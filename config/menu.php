<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2019-03-11 17:54:46
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-03-13 18:36:28
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'items' => [
        // 'MAIN NAVIGATION',
        [
            'text' => 'Dashboard',
            'url' => '/home',
            'icon' => 'dashboard',
            'roles' => [
                'super admin', 'scheme manager', 'management'
            ],
        ],
        [
            'text' => 'Scheme Manager',
            'url' => '/scheme-manager',
            'icon' => 'users',
            'roles' => [
                'super admin', 'scheme manager'
            ],

        ],
        [
            'text' => 'Operation Manager',
            'url' => '/operation-manager',
            'icon' => 'user',
            'roles' => [
                'super admin', 'scheme manager', 'operation manager'
            ],
        ],
        [
            'text' => 'Scheme Coordinator',
            'url' => '/scheme-coordinator',
            'icon' => 'users',
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator'
            ],
        ],
        [
            'text' => 'Operation Coordinator',
            'url' => '/operation-coordinator',
            'icon' => 'user',
            'roles' => [
                'super admin', 'scheme manager', 'operation manager', 'operation coordinator'
            ],
        ],
        [
            'text' => 'Auditors',
            'url' => 'auditors',
            'icon' => 'user-circle',
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator', 'auditor', 'management'
            ],

        ],

        [
            'text' => 'Technical Experts',
            'url' => '/technical-expert',
            'icon' => 'user-circle',
            // 'can' => ['show_operation_coordinator', 'scheme_manager'],
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator', 'management'
            ],

        ],

        [
            'text' => 'Company',
            'url' => 'company',
            'icon' => 'file-text',
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator', 'auditor', 'management'
            ],
        ],
        [
            'text' => 'Data Entry',
            'url' => 'data-entry',
            'icon' => 'pencil-square-o',
            'roles' => [
                'super admin', 'scheme manager', 'data entry operator'
            ],
        ],

        [
            'text' => 'Notifications',
            'url' => '#',
            'icon' => 'pencil-square-o',
            'submenu' => [
                [
                    'text' => 'Sent',
                    'url' => '#',
                    'icon' => 'pencil-square-o',
                    'roles' => [
                        'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                    ],
                    'submenu' => [
                        [
                            'text' => 'Others',
                            'url' => '#',
                            'icon' => 'pencil-square-o',
                            'roles' => [
                                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                            ],
                        ],
                        [
                            'text' => 'Post Audit',
                            'url' => '#',
                            'icon' => 'pencil-square-o',
                            'roles' => [
                                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                            ],
                        ],
                        [
                            'text' => 'Post Audit Approved',
                            'url' => '#',
                            'icon' => 'pencil-square-o',
                            'roles' => [
                                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                            ],
                        ]
                    ]
                ], [
                    'text' => 'Received',
                    'url' => '#',
                    'icon' => 'pencil-square-o',
                    'roles' => [
                        'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                    ], 'submenu' => [
                        [
                            'text' => 'Others',
                            'url' => '#',
                            'icon' => 'pencil-square-o',
                            'roles' => [
                                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                            ],
                        ],
                        [
                            'text' => 'Post Audit',
                            'url' => '#',
                            'icon' => 'pencil-square-o',
                            'roles' => [
                                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                            ],
                        ],
                        [
                            'text' => 'Post Audit Approved',
                            'url' => '#',
                            'icon' => 'pencil-square-o',
                            'roles' => [
                                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
                            ],
                        ]
                    ]
                ]
            ],
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator'
            ],

        ],
        [
            'text' => 'Outsource Request',
            'url' => '/outsource-request',
            'icon' => 'qrcode',
            'roles' => [
                'super admin', 'scheme manager', 'operation manager', 'operation coordinator'
            ],
        ],

        [
            'text' => 'Reporting',
            'url' => '/reports',
            'icon' => 'file-text',
            'roles' => [
                'super admin', 'scheme manager', 'operation manager', 'operation coordinator','scheme coordinator','management'
//                'scheme coordinator', 'operation manager', 'operation coordinator'
            ],
        ],
        [
            'text' => 'Manday Calculator',
            'url' => '/man-day-calculator',
            'icon' => 'qrcode',
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator', 'auditor', 'marketing','management'
            ],
        ],
        [
            'text' => 'Documents',
            'url' => '/documents',
            'icon' => 'file-text',
            'roles' => [
                'super admin', 'scheme manager', 'scheme coordinator', 'operation manager', 'operation coordinator', 'auditor', 'marketing'
            ],
        ],
//        [
//            'text' => 'Roles',
//            'url' => '/roles',
//            'icon' => 'file-text',
//            'roles' => [
//                'super admin'
//            ],
//        ],
        [
            'text' => 'Permissions',
            'url' => '/permissions',
            'icon' => 'file-text',
            'roles' => [
                'super admin'
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        App\Menu\Filters\HrefFilter::class,
        App\Menu\Filters\ActiveFilter::class,
        App\Menu\Filters\SubmenuFilter::class,
        App\Menu\Filters\ClassesFilter::class,
        App\Menu\Filters\GateFilter::class,
    ]
];
