@extends('layouts.master')
@section('title', "Operation Coordinator")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <form action="" id="main-form">
            <section class="content">
                <div class="container-fluid dashboard_tabs">
                    <div class="row mb-2 mrgn">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-dark dashboard_heading">VIEW <span>OPERATION COORDINATOR</span></h1>

                        </div><!-- /.col -->
                        <!-- /.col -->
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Full Name"
                                                   name="fullname" value="{{ $user->fullname() }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Enter Email"
                                                   name="email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth *</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                        </div>
                                        <input type="text" name="dob" id="dob"
                                               placeholder="dd//mm/yyyy" value="{{date('d-m-Y', strtotime($user->dob)) }}"
                                               class="float-right active form-control dob">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country *</label>
                                        <select name="country_id" class="form-control countries">
                                            <option value="">Select Country</option>
                                            @if(!empty($countries))
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" {{ ($user->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City *</label>
                                        <select name="city_id" class="form-control cities">
                                            @if(!empty($cities))
                                                @foreach($cities as $city)
                                                    <option value="{{ $city->id }}" {{ ($city->id == $user->city_id) ? 'selected' : ''}}>{{ $city->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cell Number *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="tel" class="form-control popFld phone number_field" name="cell_no"
                                                   value="{{ $user->country->phone_code . ltrim($user->phone_number, '0') }}" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Landline Number *</label>
                                        <div class="input-group mb-3">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Landline"
                                                   name="landline" value="{{ $user->operation_coordinator->landline }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Joining Date *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                            </div>
                                            <input type="text" name="joining_date" id="joining_date"
                                                   placeholder="dd//mm/yyyy"
                                                   value="{{ date('d-m-Y', strtotime($user->operation_coordinator->joining_date)) }}"
                                                   class="float-right active form-control joining_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Address *</label>
                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter Address"
                                               name="address" value="{{ $user->address }}">
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Account Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username *</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"
                                                                          aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="username"
                                               value="{{ $user->username }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Status *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="active" {{ $user->status == 'active' ? 'checked' : ''}}>
                                            <label class="job_status">Active</label>
                                        </div>
{{--                                        <div class="col-md-4 floting">--}}
{{--                                            <input type="radio" name="r3" class="icheck-blue"--}}
{{--                                                   value="suspended" {{ $user->status == 'suspended' ? 'checked' : ''}}>--}}
{{--                                            <label class="job_status">Suspended</label>--}}
{{--                                        </div>--}}
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="withdrawal" {{ $user->status == 'withdrawal' ? 'checked' : ''}}>
                                            <label class="job_status">WithDrawal</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Operation Manager *</label>
                                        <select name="operation_manager_id" class="form-control"
                                                id="operation_manager_id">
                                            <option value="" disabled>Select an option</option>
                                            @if(!empty($operation_managers))
                                                @foreach($operation_managers as $operation_manager)
                                                    <option value="{{ $operation_manager->operation_manager['id'] }}" {{($operation_manager->operation_manager['id'] == $user_operation_manager->id) ?'selected' :''}}>{{ $operation_manager->operation_manager->full_name() }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="operation_manager_detail">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="om_regions">Regions</label>
                                        <select id="om_regions" class="form-control js-example-basic-multiple" disabled
                                                name="om_regions[]" multiple="multiple" >
                                            <option value="{{$user_operation_manager->user->region->id}}"
                                                    selected>{{$user_operation_manager->user->region->title}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="om_countries">Countries</label>
                                        <select id="om_countries" class="form-control js-example-basic-multiple"
                                                name="om_countries[]" multiple="multiple"
                                                disabled>

                                            @foreach($user_operation_manager->user->region->countries as $country)
                                                <option value="{{$country->id}}" selected>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="om_zones">Zones</label>
                                        <select id="om_zones" class="form-control js-example-basic-multiple"
                                                name="om_zones[]" multiple="multiple"  disabled>
                                            @foreach($user_operation_manager->user->region->countries as $country)
                                                @foreach(\App\Zone::where('country_id',$country->id)->get() as $zone)
                                                    <option value="{{$zone->id}}" selected>{{$zone->name}}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="om_cities">Cities</label>
                                        <select id="om_cities" class="form-control js-example-basic-multiple" disabled
                                                name="om_cities[]" multiple="multiple" style="">
                                            @foreach($user_operation_manager->user->region->countries as $country)
                                                @foreach($country->cities as $city)
                                                    <option value="{{$city->id}}"
                                                    @if(!empty($regions_cities))
                                                        @foreach($regions_cities as $cities)
                                                            {{ ($cities->city_id == $city->id) ? 'selected' : '' }}
                                                                @endforeach
                                                            @endif
                                                    >{{$city->name}}({{$country->iso}})
                                                    </option>
                                                @endforeach
                                            @endforeach
                                            @foreach($user_operation_manager->user->region->countries as $country)
                                                @foreach(\App\Zone::where('country_id',$country->id)->get() as $zones)
                                                    @foreach($zones->cities as $city)
                                                        <option value="{{$city->id}}"
                                                        @if(!empty($regions_cities))
                                                            @foreach($regions_cities as $cities)
                                                                {{ ($cities->city_id == $city->id) ? 'selected' : '' }}
                                                                    @endforeach
                                                                @endif
                                                        >{{$city->name}}({{$country->iso}})</option>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Remarks *</label>
                                        <textarea name="remarks" rows="5"
                                                  class="form-control">{!! $user->remarks !!}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <label>Signature Image *</label>

                                        <div class="image form-group">
                                            <img src="{{ ($user->operation_coordinator->signature_image) ? asset('/uploads/operation_coordinator/'.$user->operation_coordinator->signature_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld1">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Profile Image</label>

                                        <div class="image form-group">
                                            <img src="{{ ($user->operation_coordinator->profile_image) ? asset('/uploads/operation_coordinator/'.$user->operation_coordinator->profile_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld2">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Additional Files</label>

                                        <div class="image form-group">
                                            @if($user->operation_coordinator->additional_files)
                                                <h6>Selected File: <span><a
                                                                href="#">{{ $user->operation_coordinator->additional_files }}</a></span>
                                                </h6>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-primary mrgn">
                                <div class="card-header cardNewHeader">
                                    <h3 class="card-title">Role & Permission</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body card_cutom">
                                    <table class="table table-responsive table-light">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Role</th>
                                            <th>Guard Name</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                @if($role->id == 5)
                                                <tr>
                                                    <td><input type="checkbox" class="icheck-blue" name="roles[]"
                                                               value="{{ $role->id }}"
                                                               @foreach ($user->roles as $u_role)
                                                               @if($u_role->id == $role->id)
                                                               checked
                                                                @endif
                                                                @endforeach
                                                        ></td>
                                                    <td>{{ ucwords($role->name) }}</td>
                                                    <td>{{ $role->guard_name }}</td>
                                                    <td>
                                                        @if($role->permissions->isNotEmpty())
                                                            @foreach($role->permissions as $permission)
                                                                @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                                    ,
                                                                @endif
                                                                {{ ucwords($permission->heading) }}
                                                            @endforeach
                                                        @else
                                                            no permissions
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

{{--                        <div class="col-sm-5">--}}
{{--                            <div class="card card-primary mrgn">--}}
{{--                                <div class="card-header cardNewHeader">--}}
{{--                                    <h3 class="card-title">Permissions</h3>--}}
{{--                                </div>--}}
{{--                                <!-- /.card-header -->--}}
{{--                                <div class="card-body card_cutom">--}}
{{--                                    <table class="table table-responsive table-light">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th>Assign</th>--}}
{{--                                            <th>Role</th>--}}
{{--                                            <th>Guard Name</th>--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                        @if(!empty($permissions))--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                <tr>--}}
{{--                                                    <td><input type="checkbox" class="icheck-blue" name="permissions[]"--}}
{{--                                                               value="{{ $permission->id }}"--}}
{{--                                                               @foreach ($user->permissions as $u_permission)--}}
{{--                                                               @if($u_permission->id == $permission->id)--}}
{{--                                                               checked--}}
{{--                                                                @endif--}}
{{--                                                                @endforeach--}}
{{--                                                        ></td>--}}
{{--                                                    <td>{{ ucwords($permission->name) }}</td>--}}
{{--                                                    <td>{{ $permission->guard_name }}</td>--}}
{{--                                                </tr>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}

{{--                                </div>--}}
{{--                                <!-- /.card-body -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-3 floting">
                                <a href="{{ route('operation-coordinator.index') }}"
                                   class="btn btn-block btn-danger  btn_cancel">Cancel
                                </a>
                            </div>
                            <div class="col-md-3 floting no_padding">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- /.content -->
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('.joining_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {

            var joininDateArray = this.value.split('-');
            var newJoiningDate = joininDateArray[2] + '-' + joininDateArray[1] + '-' + joininDateArray[0];
            var dobArray = $('.dob').val().split('-');
            var newdobDate = dobArray[2] + '-' + dobArray[1] + '-' + dobArray[0];


            var dob = moment(newdobDate).format('YYYY-MM-DD');
            var joiningDate = moment(newJoiningDate).format('YYYY-MM-DD');

            if (joiningDate > dob) {
            } else {

                toastr['error']('Joining Date always greater than DOB');
                $('#joining_date').val('');
            }


        });
        $("#main-form :input").prop("disabled", true);
        $("#om_regions").select2().trigger('change');
        $("#om_countries").select2().trigger('change');
        $("#om_zones").select2().trigger('change');
        $("#om_cities").select2().trigger('change');
    </script>
    <style>
        .select2-container--default .select2-selection--multiple {
            /*height: 38px;*/
            height: 105px;
            overflow-y: scroll;
        }
    </style>
@endpush