@extends('layouts.master')
@section('title', "Documents")

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark dashboard_heading">Documents</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('documents') }}
                    </div>
                    <!-- /.col -->
                </div>
                @can('create_documents')
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Upload Documents</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="" method="post" enctype="multipart/form-data" id="uploadDocumentForm">
                            @csrf
                            <div class="card-body card_cutom">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>File Name <span style="color: red !important;">*</span></label>
                                        <input type="text" class="form-control" name="file_name" required autofocus>
                                        <span style="color: red ; margin-top: 5px">Uploaded file shouldn't contain Hash (#)</span>
                                    </div>
                                    <div class="col-md-2" style="margin-top: 30px">
                                        <label class="upload_documt"> Add Document <span style="color: red !important;">*</span>
                                            <input type="file" name="document_file" id="document_file" required>
                                            <img src="{{asset('img/new.png')}}" alt="img"></label>
                                        <p id="selectedFileName"></p>
                                    </div>
                                    <div class="col-md-7">
                                        <label>Choose Category</label><br>

                                        @foreach($documentCategories as $category)
                                            <div class="col-md-3 floting" style="margin-top: 10px">
                                                <input type="radio" name="category" value="{{$category}}"
                                                       class="icheck-blue">&nbsp;
                                                <label>{{ucfirst($category)}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-success btn_save">Save</button>
                                    </div>
                                </div>
{{--                                <div class="col-md-2 pull-right mrgn">--}}
{{--                                    <button type="submit" class="btn btn-success btn_save edit_btn ">Save </button>--}}
{{--                                    //edit_btn--}}
{{--                                </div>--}}
                            </div>
                            <!-- /.card-body -->

                        </form>

                    </div>
                @endcan

                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Download Documents</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="#" method="post">

                        <!--<div class="col-md-3 col-sm-6 col-12 floting">
                           <a href="#"><div class="card-body cardDocument">
                    <div class="info-box info_box_grey">
                      <div class="info-box-content">
                        <h3 class="info-box-text">All</h3>
                      </div>
                    </div>
                  </div></a>
                        </div>-->
                        <div class="clearfix"></div>

                        <!-- /.card-body -->

                    </form>


                    @can('show_documents')
                        <div class="mrgn downloadNav">
                            <ul class="nav nav-pills ml-auto p-2">

                                @foreach($documentByCategory as $key => $category)
                                    <li class="nav-item"><a class="nav-link {{$key == 'auditor' ? 'active show': ''}}"
                                                            href="#{{$key}}"
                                                            data-toggle="tab">{{ucfirst($key)}}</a></li>
                                @endforeach


                            </ul>
                            <div class="tab-content">
                                @foreach($documentByCategory as $key => $category)
                                    <div class="tab-pane {{$key == 'auditor' ? 'active': ''}}" id="{{$key}}">
                                        <h2>{{ucfirst($key)}}:</h2>
                                        <div class="row mrgn">


                                            @foreach($category as $key => $document)
                                                <div class="col-md-12 mrgn">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2 floting">
                                                            <i class="fa fa-file text-lg" aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-8 floting">
                                                            <h6>{{$document->file_name}}</h6>
                                                            <a href="{{asset('/uploads/documents/'.$document->file)  }}"
                                                               download>Download</a>
                                                        </div>
                                                        <div class="col-md-2 floting">

                                                            @can('delete_documents')
                                                                <a href="javascript:void(0)"
                                                                   onclick="removeMedia({{$document->id}})"
                                                                   class="btn btn-sm btn-danger fontIconMedia"><i
                                                                            class="fa fa-remove"></i>
                                                                </a>
                                                            @endcan

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    @endcan

                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#document_file').change(function (e) {
                var fileName = e.target.files[0].name;
                $("#selectedFileName").html(fileName);

                // show active tab on reload
                if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

                // remember the hash in the URL without jumping
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    if (history.pushState) {
                        history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
                    } else {
                        location.hash = '#' + $(e.target).attr('href').substr(1);
                    }
                });
            });

        });

        $('#uploadDocumentForm').submit(function (e) {
            e.preventDefault();
            var form = $('#uploadDocumentForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('document.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#uploadDocumentForm")[0].reset();
                        $("#selectedFileName").html('');
                        $('input').iCheck('uncheck');

                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function removeMedia(id) {

            var request = "id=" + id;


            $.ajax({
                url: "{{ route('documents.media.remove') }}",
                method: 'POST',
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    toastr['success'](response.message);
                    setTimeout(function (){
                        window.location.reload();
                    },1000)


                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    </script>
@endpush

