@extends('layouts.master')
@section('title', "New Permission")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Permission</h1>
                        <h6 class="text-center dashboard_heading2">Create
                            Permission{{--  - if you want to learn how to follow these steps - <a href="#">Click Here</a> --}}</h6>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->

            <div class="card card-primary mrgn comp_det_view ">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">New Permission </h3>
                </div>

                <div class="card-body card_cutom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="aud_info">
                            <form action="{{ route('permissions.store') }}" method="POST">
                                @csrf
                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="role_id">Roles: <span style="color: red">*</span></label>
                                            <select class="form-control @if($errors->has('role_id'))  is-invalid @endif" name="role_id" id="role_id">

                                                <option value="">Select Role</option>
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}">
                                                        {{ ucwords($role->name) }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('role_id'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('role_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    {{-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Permission:</label>
                                            <input class="form-control" name="permission" type="text" value="{{old('permission')}}">
                                        </div>
                                    </div> --}}

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="permissions.0">Permissions: <span style="color: red">*</span></label>
                                            <select class="form-control multipermissions  @if($errors->has('permissions.0'))  is-invalid @endif" id="permissions.0" name="permissions[]"
                                                    multiple="multiple">

{{--                                                <option value="show_scheme_manager">View Scheme Manager</option>--}}
{{--                                                <option value="create_scheme_manager">Create Scheme Manager</option>--}}
{{--                                                <option value="edit_scheme_manager">Edit Scheme Manager</option>--}}
{{--                                                <option value="delete_scheme_manager">Delete Scheme Manager</option>--}}

{{--                                                <option value="show_scheme_coordinator">View Scheme Coordinator</option>--}}
{{--                                                <option value="create_scheme_coordinator">Create Scheme Coordinator--}}
{{--                                                </option>--}}
{{--                                                <option value="edit_scheme_coordinator">Edit Scheme Coordinator</option>--}}
{{--                                                <option value="delete_scheme_coordinator">Delete Scheme Coordinator--}}
{{--                                                </option>--}}

{{--                                                <option value="show_operation_manager">View Operation Manager</option>--}}
{{--                                                <option value="create_operation_manager">Create Operation Manager--}}
{{--                                                </option>--}}
{{--                                                <option value="edit_operation_manager">Edit Operation Manager</option>--}}
{{--                                                <option value="delete_operation_manager">Delete Operation Manager--}}
{{--                                                </option>--}}

{{--                                                <option value="show_operation_coordinator">View Operation Coordinator--}}
{{--                                                </option>--}}
{{--                                                <option value="create_operation_coordinator">Create Operation--}}
{{--                                                    Coordinator--}}
{{--                                                </option>--}}
{{--                                                <option value="edit_operation_coordinator">Edit Operation Coordinator--}}
{{--                                                </option>--}}
{{--                                                <option value="delete_operation_coordinator">Delete Operation--}}
{{--                                                    Coordinator--}}
{{--                                                </option>--}}

{{--                                                <option value="show_auditors">View Auditors</option>--}}
{{--                                                <option value="create_auditors">Create Auditors</option>--}}
{{--                                                <option value="edit_auditors">Edit Auditors</option>--}}
{{--                                                <option value="delete_auditors">Delete Auditors</option>--}}

{{--                                                <option value="show_technical_experts">View Technical Experts</option>--}}
{{--                                                <option value="create_technical_experts">Create Technical Experts--}}
{{--                                                </option>--}}
{{--                                                <option value="edit_technical_experts">Edit Technical Experts</option>--}}
{{--                                                <option value="delete_technical_experts">Delete Technical Experts--}}
{{--                                                </option>--}}

{{--                                                <option value="show_company">View Company</option>--}}
{{--                                                <option value="create_company">Create Company</option>--}}
{{--                                                <option value="edit_company">Edit Company</option>--}}
{{--                                                <option value="delete_company">Delete Company</option>--}}

{{--                                                <option value="show_company_standard">View Company Standard</option>--}}
{{--                                                <option value="create_company_standard">Create Company Standard</option>--}}
{{--                                                <option value="edit_company_standard">Edit Company Standard</option>--}}
{{--                                                <option value="delete_company_standard">Delete Company Standard</option>--}}


{{--                                                <option value="show_aj">View Audit Justification</option>--}}
{{--                                                <option value="create_aj">Create Audit Justification</option>--}}
{{--                                                <option value="edit_aj">Edit Audit Justification</option>--}}
{{--                                                <option value="delete_aj">Delete Audit Justification</option>--}}

{{--                                                <option value="show_outsource_request">View Outsource Request</option>--}}
{{--                                                <option value="create_outsource_request">Create Outsource Request--}}
{{--                                                </option>--}}
{{--                                                <option value="edit_outsource_request">Edit Outsource Request</option>--}}
{{--                                                <option value="delete_outsource_request">Delete Outsource Request--}}
{{--                                                </option>--}}

{{--                                                <option value="show_data_entry">View Data Entry</option>--}}
{{--                                                <option value="create_data_entry">Create Data Entry</option>--}}
{{--                                                <option value="edit_data_entry">Edit Data Entry</option>--}}
{{--                                                <option value="delete_data_entry">Delete Data Entry</option>--}}

{{--                                                <option value="show_documents">View Documents</option>--}}
{{--                                                <option value="create_documents">Create Documents</option>--}}
{{--                                                <option value="edit_documents">Edit Documents</option>--}}
{{--                                                <option value="delete_documents">Delete Documents</option>--}}

{{--                                                <option value="show_notification">View Notification</option>--}}
{{--                                                <option value="create_notification">Create Notification</option>--}}
{{--                                                <option value="edit_notification">Edit Notification</option>--}}
{{--                                                <option value="delete_notification">Delete Notification</option>--}}

                                            </select>

                                            @if ($errors->has('permissions.0'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('permissions.0') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-success btn_save">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $('.multipermissions').select2({
                placeholder: "Select Permissions"
            });
        });

        $('#role_id').on('change', function () {
            var request = {
                "role_id": $(this).val(),
            };
            if ($(this).val() == '') {
                toastr['error']("Please Select Role");
            } else {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.get.remaining.permissions') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = '';
                            if(response.data.length > 0){
                                response.data.forEach(function (data, index) {
                                    html += '<option value="' + data.name + '">' + data.heading + '</option>';

                                });
                                $('.multipermissions').html(html);
                            }

                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }

        });
    </script>
@endpush