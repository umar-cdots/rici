@extends('layouts.master')
@section('title', "Edit Permission")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Permission</h1>
                        <h6 class="text-center dashboard_heading2">Edit
                            Permission{{--  - if you want to learn how to follow these steps - <a href="#">Click Here</a> --}}</h6>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->

            <div class="card card-primary mrgn comp_det_view ">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">Edit Permission </h3>
                </div>

                <div class="card-body card_cutom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="aud_info">
                            <form action="{{ route('permissions.update', $permission->id) }}" method="POST">
                                @csrf
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Permission:</label>
                                            <input class="form-control" name="permission" type="text" value="{{ $permission->name }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <button class="btn btn-secondary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection