@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-4 floting">
                            <h1>Permission</h1>
                        </div>
                        <div class="col-sm-6 floting">
                            <a href="{{ route('permissions.create') }}" class="add_comp">
                                <label class="text-capitalize"><i class="fa fa-plus-circle text-lg"
                                                                  aria-hidden="true"></i> Add Permission</label>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('permissions') }}
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">


            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Roles</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <table class="table table-light">
                                <thead>
                                <tr>
                                    <th>Assign</th>
                                    <th>Role</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($roles))
                                    @foreach($roles as $role)
                                        <tr>
                                            <td><input type="radio" name="roles[]" id="radioRole{{$role->id}}"
                                                       value="{{ $role->id }}"
                                                       {{ ($role->id === 1) ? 'checked' : '' }} onchange="hideshowpermission('{{$role->id}}')">
                                            </td>
                                            <td>{{ ucwords($role->name) }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Permissions</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom permissionsScroll">
                            @if(!empty($roles))
                                @foreach($roles as $role)
                                    <table id="permission{{$role->id}}"
                                           class="table table-light permission"
                                           style="display: {{$role->id==1 ? 'inline-table':'none'}};">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody id="permissiontbody{{$role->id}}">
                                        @if(!empty($role->permissions))
                                            @foreach($role->permissions as $permission)
                                                <tr>
                                                    <td><input type="checkbox" name="permissions[]"
                                                               value="{{ $permission->id }}" checked
                                                               onclick="removePermission('{{$permission->id}}','{{$role->id}}')">
                                                    </td>
                                                    <td>{{ ucfirst($permission->heading) }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                @endforeach
                            @endif
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#tabularData').dataTable();
        });

        function hideshowpermission(roleId) {
            if ($("#radioRole" + roleId + "").is(':checked')) {
                $(".permission").css('display', 'none');
                $("#permission" + roleId + "").css('display', 'block');
            } else {
                $("#permission" + roleId + "").css('display', 'none');
            }
        }

        function removePermission(permissionId, roleId) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.deletepermission') }}",
                data: {roleId: roleId, permissionId: permissionId},
                success: function (response) {
                    if (response.status == "success") {
                        $("#permissiontbody" + roleId + "").empty();

                        var html = '';
                        $.each(response.data, function (key, value) {

                            html += '<tr>';
                            html += '<td><input type="checkbox" name="permissions[]" value="' + value['pivot']['permission_id'] + '" checked onclick="removePermission(' + value['pivot']['permission_id'] + ',' + roleId + ')"></td>'
                            html += '<td>' + value['heading'] + '</td>'
                            html += '</tr>'
                        });
                        $("#permissiontbody" + roleId + "").html(html);
                    }
                },
                error: function (error) {
                    toastr['error']('something went wrong');
                }
            });
        }

        function confirmDelete() {
            var r = confirm("Are you sure you want to perform this action");
            if (r === true) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush