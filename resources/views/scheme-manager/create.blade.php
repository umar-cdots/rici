@extends('layouts.master')
@section('title', "Scheme Manager")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <form action="" id="main-form">
            <section class="content">
                <div class="container-fluid dashboard_tabs">
                    <div class="row mb-2 mrgn">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-dark dashboard_heading">ADD <span>SCHEME MANAGER</span></h1>

                        </div><!-- /.col -->
                        <!-- /.col -->
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name *</label>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Full Name"
                                                   name="fullname">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend" style="margin-right: -2px;">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Enter Email"
                                                   name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth *</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                          </span>
                                        </div>
                                        <input type="text" name="dob" id="dob"
                                               placeholder="dd//mm/yyyy"
                                               class="float-right active form-control dob">
                                        {{--                                        <input type="date" class="float-right active form-control" name="dob">--}}
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label>Country *</label>
                                        <select name="country_id" class="form-control countries">
                                            <option value="">Select Country</option>
                                            @if(!empty($countries))
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label>City *</label>
                                        <select name="city_id" class="form-control cities">
                                            <option value="">Select City</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Cell Number *</label>
                                    <div class="row">

                                        <div class="col-md-2" style="padding-right: 0;">
                                            <input type="text" class="form-control" value="" id="country_code"
                                                   name="country_code" readonly>
                                        </div>
                                        <div class="col-md-10" style="padding-left: 0;">
                                            <div class="form-group mb-0">

                                                <input type="text" class="form-control popFld phone" name="cell_no"
                                                       onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>

                            </div>
                            <div class="row ">
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label>Landline Number *</label>
                                        <input type="tel" class="form-control " name="landline">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label>Joining Date *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                            </div>
                                            <input type="text" name="joining_date" id="joining_date"
                                                   placeholder="dd//mm/yyyy"
                                                   class="float-right active form-control joining_date">
                                            {{--                                            <input type="date" class="float-right active form-control"--}}
                                            {{--                                                   name="joining_date">--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Address *</label>
                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter Address"
                                               name="address">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Account Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Status *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="r3" class="icheck-blue" value="active" checked>
                                            <label class="job_status">Active</label>
                                        </div>
                                        {{--                                        <div class="col-md-4 floting">--}}
                                        {{--                                            <input type="radio" name="r3" class="icheck-blue" value="suspended">--}}
                                        {{--                                            <label class="job_status">Suspended</label>--}}
                                        {{--                                        </div>--}}
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="r3" class="icheck-blue" value="withdrawal">
                                            <label class="job_status">Withdrawal</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label for="standard_id">Standard Family *</label>
                                        <select id="standard_family_id" class="form-control js-example-basic-multiple"
                                                name="standard_family_id[]" multiple="multiple" style="width: 200px;">
                                            @if(!empty($standard_families))
                                                @foreach($standard_families as $standard_family)
                                                    <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="standard_id">List of standards *</label>
                                        <select id="standard_id" class="form-control js-example-basic-multiple"
                                                name="standard_id[]" multiple="multiple" style="width: 200px;">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username *</label>
                                    <div class="input-group mb-0">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"
                                                                          aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="username">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Password *</label>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-unlock-alt"
                                                                              aria-hidden="true"></i></span>
                                            </div>
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Confirm Password *</label>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-unlock-alt"
                                                                              aria-hidden="true"></i></span>
                                            </div>
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <div class="form-group mb-0">
                                        <label>Remarks *</label>
                                        <textarea name="remarks" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <label>Signature Image * <span>Max 10mb</span></label>
                                        <div class="form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input form-control"
                                                       name="signature_file"
                                                       id="signature_file" onchange="readURLMany(this,1)">
                                                <label class="custom-file-label" for="signature_file">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>

                                        </div>


                                        <div class="image form-group">
                                            <img src="{{ asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld1">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Profile Image <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="profile_image"
                                                       id="exampleInputFile2" onchange="readURLMany(this,2)">
                                                <label class="custom-file-label" for="exampleInputFile2">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld2">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Additional Files <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="additional_files"
                                                       id="exampleInputFile3" onchange="readURLMany(this,3)">
                                                <label class="custom-file-label" for="exampleInputFile3">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group" id="file">

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-primary mrgn">
                                <div class="card-header cardNewHeader">
                                    <h3 class="card-title">Role & Permissions</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body card_cutom">
                                    <table class="table table-responsive table-light">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Role</th>
                                            <th width="10%">Guard Name</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                @if($role->id === 2)
                                                    <tr>
                                                        <td><input type="radio" class="icheck-blue" name="roles[]"
                                                                   value="{{ $role->id }}" {{ ($role->id === 2) ? 'checked' : '' }}>
                                                        </td>
                                                        <td>{{ ucwords($role->name) }}</td>
                                                        <td>{{ $role->guard_name }}</td>
                                                        <td>
                                                            @if($role->permissions->isNotEmpty())
                                                                @foreach($role->permissions as $permission)
                                                                    @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                                        ,
                                                                    @endif
                                                                    {{ ucwords($permission->heading) }}
                                                                @endforeach
                                                            @else
                                                                no permissions
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>


                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-3 floting"></div>

                            <div class="col-md-3 floting">
                                <a href="{{ route('scheme-manager.index') }}"
                                   class="btn btn-block btn-danger  btn_cancel">Cancel
                                </a>
                            </div>
                            <div class="col-md-3 floting no_padding">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success btn_save">Save
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- /.content -->
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">
    <style>
        .select2-container {
            box-sizing: border-box;
            display: inline !important;
            margin: 0;
            position: relative;
            vertical-align: middle;
        }

    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('.joining_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {

            var joininDateArray = this.value.split('-');
            var newJoiningDate = joininDateArray[2] + '-' + joininDateArray[1] + '-' + joininDateArray[0];
            var dobArray = $('.dob').val().split('-');
            var newdobDate = dobArray[2] + '-' + dobArray[1] + '-' + dobArray[0];


            var dob = moment(newdobDate).format('YYYY-MM-DD');
            var joiningDate = moment(newJoiningDate).format('YYYY-MM-DD');

            if (joiningDate > dob) {
            } else {

                toastr['error']('Joining Date always greater than DOB');
                $('#joining_date').val('');
            }


        });


        function readURLMany(input, id = null) {
            if (input.id == 'exampleInputFile3') {
                var filename = input.files[0].name;
                var html = '';
                if (input.files && input.files[0]) {
                    html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
                }
                $('#file').html(html);
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#upld' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            $('#country_code').val(response.data.country.phone_code);
                            var html = "";
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        $("#region_id").change(function () {
            var region_id = $(this).val();
            var request = "region_id=" + region_id;
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.regionCountriesCities') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        console.log(response);
                        var html = "";
                        var cities = "";
                        $.each(response.data.region_countries, function (i, obj) {
                            html += obj.country.name;
                            if (i >= 0 && i < response.data.region_countries.length - 1) {
                                html += ', ';
                            }
                        });
                        $.each(response.data.country_cities, function (j, k) {
                            cities += k;

                            if (j >= 0 && j < response.data.country_cities.length - 1) {
                                cities += ', ';
                            }
                        });
                        $("#region-countries").html(html);
                        $("#region-cities").html(cities);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('#main-form').submit(function (e) {
            e.preventDefault();
            var form = $('#main-form')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('scheme-manager.store') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);

                    if (response.status == "success" || response.status == '200') {
                        setTimeout(function () {
                            window.location.href = '{{ route('scheme-manager.index') }}';
                        }, 2000);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('.js-example-basic-multiple').select2();

        $("#standard_family_id").change(function () {
            var selected_standard_families = [];
            $("#standard_id").attr('disabled', false);

            $.each($("#standard_family_id option:selected"), function () {
                selected_standard_families.push($(this).val());
            });

            if (selected_standard_families.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.families.standards') }}",
                    data: {standard_families: selected_standard_families},
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.standards, function (i, obj) {
                                html += '<option value="' + obj.id + '" selected>' + obj.name + '</option>';
                            });
                            $("#standard_id").html(html);
                            $("#standard_id").select2().trigger('change');
                            $("#standard_id").attr('disabled', true);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $("#standard_id").html('<option value=""></option>');
                $("#standard_id").select2().trigger('change');
                $("#standard_id").attr('disabled', true);

            }

        });


    </script>
@endpush

