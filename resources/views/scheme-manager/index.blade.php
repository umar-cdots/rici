@extends('layouts.master')
@section('title', "Scheme Manager List")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <div class="col-sm-5 floting">
                            <h1>Scheme Manager</h1>
                        </div>
                            @can('create_scheme_manager')
                                <div class="col-sm-6 floting">
                                    <a href="{{route('scheme-manager.create')}}" class="add_comp"><label
                                                class="text-capitalize"><i class="fa fa-plus-circle text-lg"
                                                                           aria-hidden="true"></i> Add New Scheme
                                            Manager</label></a>
                                </div>
                            @endcan
                    </div>

                    <div class="col-sm-4">
                        {{ Breadcrumbs::render('schemeManager') }}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of Scheme Manager</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <table id="tabularData" class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                <tr>
                                    <th width="8%;">Sr. #</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($scheme_managers as $scheme_manager)

                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $scheme_manager->fullName() }}</td>
                                        <td>{{ $scheme_manager->username }}</td>
                                        <td>{{ $scheme_manager->email }}</td>
                                        <td>{{ ucfirst($scheme_manager->status) }}</td>
                                        <td>
                                            <ul class="data_list">
                                                    @can('show_scheme_manager')
                                                        <li>
                                                            <a href="{{route('scheme-manager.show', $scheme_manager->id)}}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </li>
                                                    @endcan
                                                    @can('edit_scheme_manager')
                                                        <li>

                                                            <a href="{{route('scheme-manager.edit', $scheme_manager->id)}}"><i
                                                                        class="fa fa-pencil"></i></a>
                                                        </li>
                                                    @endcan
                                                    @can('delete_scheme_manager')
                                                        <li>

                                                            <form action="{{ route('scheme-manager.destroy', $scheme_manager->id) }}"
                                                                  id="delete_{{$scheme_manager->id}}" method="POST">
                                                                {{ method_field('DELETE') }}
                                                                @csrf
                                                                <a style="margin-left:10px;" href="javascript:void(0)"
                                                                   onclick="if(confirmDelete()){ document.getElementById('delete_{{$scheme_manager->id}}').submit(); }">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </form>
                                                        </li>
                                                    @endcan


                                            </ul>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#tabularData').dataTable();
        });

        function confirmDelete() {
            var r = confirm("Are you sure you want to perform this action");
            if (r === true) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush