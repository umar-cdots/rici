@extends('layouts.master')
@section('title', "Scheme Manager")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <form action="" id="main-form">
            <section class="content">

                <div class="container-fluid dashboard_tabs">
                    <div class="row mb-2 mrgn">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-dark dashboard_heading">EDIT <span>SCHEME MANAGER</span></h1>

                        </div><!-- /.col -->
                        <!-- /.col -->
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name *</label>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Full Name"
                                                   name="fullname" value="{{ $user->fullname() }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Enter Email"
                                                   id="email"
                                                   name="email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="dob">Date of Birth *</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                        </div>


                                        <input type="text" name="dob" id="dob"
                                               placeholder="dd//mm/yyyy"
                                               value="{{date('d-m-Y', strtotime($user->dob)) }}"
                                               class="float-right active form-control dob">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label for="country_id">Country *</label>
                                        <select name="country_id" id="country_id" class="form-control countries">
                                            <option value="">Select Country</option>
                                            @if(!empty($countries))
                                                @foreach($countries as $country)

                                                    <option value="{{ $country->id }}" {{ ($user->country_id == $country->id) ? 'selected' : '' }}>
                                                        {{ $country->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label for="city_id">City *</label>
                                        <select name="city_id" class="form-control cities" id="city_id">
                                            @if(!empty($cities))
                                                @foreach($cities as $city)
                                                    <option value="{{ $city->id }}" {{ ($city->id == $user->city_id) ? 'selected' : ''}}>
                                                        {{ $city->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Cell Number *</label>
                                    <div class="row">

                                        <div class="col-md-2" style="padding-right: 0;">
                                            <input type="text" class="form-control"
                                                   value="{{ $user->country->phone_code ?? '-'}}" id="country_code"
                                                   name="country_code" readonly>
                                        </div>
                                        <div class="col-md-10" style="padding-left: 0;">
                                            <div class="form-group mb-0">

                                                <input type="text" class="form-control popFld phone"
                                                       value="{{ ltrim($user->phone_number, '0') }}" name="cell_no"
                                                       onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label>landline Number *</label>
                                        <input type="tel" class="form-control"
                                               name="landline" value="{{ $user->scheme_manager->landline }}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        <label>Joining Date *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                            </div>


                                            <input type="text" name="joining_date" id="joining_date"
                                                   placeholder="dd//mm/yyyy"
                                                   value="{{ date('d-m-Y', strtotime($user->scheme_manager->joining_date)) }}"
                                                   class="float-right active form-control joining_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Address *</label>
                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter Address"
                                               name="address" value="{{ $user->address }}">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Account Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username *</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"
                                                                          aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="username"
                                               value="{{ $user->username }}">
                                    </div>
                                </div>
                                @if(auth()->user()->user_type == 'admin')
                                    <div class="col-md-4">
                                        <label for="password">Password</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-unlock-alt"
                                                                                  aria-hidden="true"></i></span>
                                            </div>
                                            <input type="password" class="form-control" name="password" id="password"
                                                   value="">
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-4">
                                    <label>Status *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="active" {{ $user->status == 'active' ? 'checked' : ''}}>
                                            <label class="job_status">Active</label>
                                        </div>
                                        {{--                                        <div class="col-md-4 floting">--}}
                                        {{--                                            <input type="radio" name="r3" class="icheck-blue"--}}
                                        {{--                                                   value="suspended" {{ $user->status == 'suspended' ? 'checked' : ''}}>--}}
                                        {{--                                            <label class="job_status">Suspended</label>--}}
                                        {{--                                        </div>--}}
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="withdrawal" {{ $user->status == 'withdrawal' ? 'checked' : ''}}>
                                            <label class="job_status">WithDrawal</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-0">

                                        <label for="standard_id">Standard Family*</label>
                                        <select id="standard_family_id" class="form-control js-example-basic-multiple "
                                                name="standard_family_id[]" multiple="multiple" style="width: 200px;">
                                            @foreach($scheme_manager_families as $scheme_manage_family)
                                                <option value="{{ $scheme_manage_family->standard_family->id }}"
                                                        selected>{{ $scheme_manage_family->standard_family->name }}</option>
                                            @endforeach
                                            @if(!empty($standard_families))
                                                @foreach($standard_families as $standard_family)
                                                    <option value="{{ $standard_family->id }}">
                                                        {{ $standard_family->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-0">
                                        <label for="standard_id">List of standards *</label>
                                        <select id="standard_id" class="form-control js-example-basic-multiple"
                                                name="standard_id[]" multiple="multiple" style="width: 200px;" disabled>
                                            @foreach($scheme_manager_families as $scheme_manage_family)
                                                @foreach($scheme_manage_family->standard_family->standards as $standard)
                                                    <option value="{{ $standard->id }}"
                                                            selected>{{ $standard->name }}</option>
                                                @endforeach
                                            @endforeach
                                            @if(!empty($standard_families))
                                                @foreach($standard_families as $standard_family)
                                                    @foreach($standard_family->standards as $standard)
                                                        <option value="{{ $standard->id }}">
                                                            {{ $standard->name }}
                                                        </option>
                                                    @endforeach

                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <div class="form-group mb-0">
                                        <label>Remarks *</label>
                                        <textarea name="remarks" rows="5"
                                                  class="form-control">{!! $user->remarks !!}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <label>Signature Image * <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="signature_file"
                                                       id="exampleInputFile1" onchange="readURLMany(this,1)">
                                                <label class="custom-file-label" for="exampleInputFile1">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ ($user->scheme_manager->signature_image) ? asset('/uploads/scheme_managers/'.$user->scheme_manager->signature_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld1">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Profile Image <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="profile_image"
                                                       id="exampleInputFile2" onchange="readURLMany(this,2)">
                                                <label class="custom-file-label" for="exampleInputFile2">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ ($user->scheme_manager->profile_image) ? asset('/uploads/scheme_managers/'.$user->scheme_manager->profile_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld2">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Additional Files <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="additional_files"
                                                       id="exampleInputFile3" onchange="readURLMany(this,3)">
                                                <label class="custom-file-label" for="exampleInputFile3">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            @if($user->scheme_manager->additional_files)
                                                <div class="image form-group" id="file">
                                                    <h6>Selected File: <span><a
                                                                    href="{{ asset('/uploads/scheme_managers/'.$user->scheme_manager->additional_files .'') }}"
                                                                    target="_blank">{{ $user->scheme_manager->additional_files }}</a>
                                                            <span><i
                                                                        class="fa fa-remove" style="float: right"
                                                                        onclick="removeAdditionalFiles('{{ $user->scheme_manager->user_id }}')"></i></span></span>
                                                    </h6>
                                                </div>
                                            @else
                                                <div class="image form-group" id="file">

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-primary mrgn">
                                <div class="card-header cardNewHeader">
                                    <h3 class="card-title">Role & Permissions</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body card_cutom">
                                    <table class="table table-responsive table-light">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Role</th>
                                            <th>Guard Name</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                @if($role->id == 2)
                                                    <tr>
                                                        <td><input type="radio" class="icheck-blue" name="roles[]"
                                                                   value="{{ $role->id }}"
                                                                   @foreach ($user->roles as $u_role)
                                                                   @if($u_role->id == $role->id)
                                                                   checked
                                                                    @endif
                                                                    @endforeach
                                                            ></td>
                                                        <td>{{ ucwords($role->name) }}</td>
                                                        <td>{{ $role->guard_name }}</td>
                                                        <td>
                                                            @if($role->permissions->isNotEmpty())
                                                                @foreach($role->permissions as $permission)
                                                                    @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                                        ,
                                                                    @endif
                                                                    {{ ucwords($permission->heading) }}
                                                                @endforeach
                                                            @else
                                                                no permissions
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-3 floting"></div>

                            <div class="col-md-3 floting">
                                <a href="{{ route('scheme-manager.index') }}"
                                   class="btn btn-block btn-danger  btn_cancel">Cancel
                                </a>
                            </div>
                            <div class="col-md-3 floting no_padding">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success btn_save">Save
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- /.content -->
@endsection


@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">
    <style>
        .select2-container {
            box-sizing: border-box;
            display: inline !important;
            margin: 0;
            position: relative;
            vertical-align: middle;
        }

    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>


    <script>
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('.joining_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {

            var joininDateArray = this.value.split('-');
            var newJoiningDate = joininDateArray[2] + '-' + joininDateArray[1] + '-' + joininDateArray[0];
            var dobArray = $('.dob').val().split('-');
            var newdobDate = dobArray[2] + '-' + dobArray[1] + '-' + dobArray[0];


            var dob = moment(newdobDate).format('YYYY-MM-DD');
            var joiningDate = moment(newJoiningDate).format('YYYY-MM-DD');

            if (joiningDate > dob) {
            } else {

                toastr['error']('Joining Date always greater than DOB');
                $('#joining_date').val('');
            }


        });
        $(document).ready(function () {
            (function standardCodes() {
                var standard_names = [];
                var standarad_ids = [];
                $.each($(".js-example-basic-multiple option:selected"), function () {
                    standard_names.push($(this).text());
                    standarad_ids.push($(this).val());
                });
                var html = '';
                standard_names.forEach(function (m, n) {
                    html += '<tr id="node-' + n + '"><td>' + standard_names[n] + '</td><input type="hidden" name="standard_codes[' + m + ']" value="' + m + '"> </tr>'
                });

                $("#team-view-stage2").html(html);
                $(".standardCodesTable").css('display', 'block');
            })();


        });


        function readURLMany(input, id = null) {
            if (input.id == 'exampleInputFile3') {
                var filename = input.files[0].name;
                var html = '';
                if (input.files && input.files[0]) {
                    html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
                }
                $('#file').html(html);
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#upld' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        $("#region_id").change(function () {
            var region_id = $(this).val();
            var request = "region_id=" + region_id;
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.regionCountriesCities') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        console.log(response);
                        var html = "";
                        var cities = "";
                        $.each(response.data.region_countries, function (i, obj) {
                            html += obj.country.name;
                            if (i >= 0 && i < response.data.region_countries.length - 1) {
                                html += ', ';
                            }
                        });
                        $.each(response.data.country_cities, function (j, k) {
                            cities += k;

                            if (j >= 0 && j < response.data.country_cities.length - 1) {
                                cities += ', ';
                            }
                        });
                        $("#region-countries").html(html);
                        $("#region-cities").html(cities);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('#main-form').submit(function (e) {
            e.preventDefault();
            var form = $('#main-form')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('scheme-manager.update') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);


                    if (response.status == "success" || response.status == '200') {
                        setTimeout(function () {
                            window.location.href = '{{ route('scheme-manager.index') }}';
                        }, 2000);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('.js-example-basic-multiple').select2();

        $("#standard_family_id").change(function () {
            var selected_standard_families = [];
            $("#standard_id").attr('disabled', false);

            $.each($("#standard_family_id option:selected"), function () {
                selected_standard_families.push($(this).val());
            });

            if (selected_standard_families.length > 0) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.families.standards') }}",
                    data: {standard_families: selected_standard_families},
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.standards, function (i, obj) {
                                html += '<option value="' + obj.id + '" selected>' + obj.name + '</option>';
                            });
                            $("#standard_id").html(html);
                            $("#standard_id").select2().trigger('change');
                            $("#standard_id").attr('disabled', true);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $("#standard_id").html('<option value=""></option>');
                $("#standard_id").select2().trigger('change');
                $("#standard_id").attr('disabled', true);

            }

        });

        function removeAdditionalFiles(id) {
            var request = {
                'user_id': parseInt(id),
                'user_type': 'scheme_manager'
            }
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.remove.additionalfiles') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {

                    if (response.status == "success" || response.status == '200') {
                        toastr['success']("File Removed Successfully.");
                        window.location.reload();

                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }
    </script>
@endpush