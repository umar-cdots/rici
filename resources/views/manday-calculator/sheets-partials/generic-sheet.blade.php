@if(!empty('sheets'))
    @foreach($sheets as $sheet)
        <tr>
            <td>{{ $sheet->standard_number }}</td>
            <td>{{ $sheet->complexity }}</td>
            <td>{{ $sheet->frequency }}</td>
            <td>{{ $sheet->effective_employes_on }}</td>
            <td>{{ $sheet->effective_employes_off }}</td>
            <td>{{ $sheet->stage1_on }}</td>
            <td>{{ $sheet->stage1_off }}</td>
            <td>{{ $sheet->stage2_on }}</td>
            <td>{{ $sheet->stage2_off }}</td>
            <td>{{ ($sheet->frequency == 'annual' && $sheet->surveillance_on > 2) ? $sheet->surveillance_on/2 : $sheet->surveillance_on }}</td>
            <td>{{ $sheet->surveillance_off }}</td>
            <td>{{ $sheet->reaudit_on }}</td>
            <td>{{ $sheet->reaudit_off }}</td>
        </tr>
    @endforeach
@endif