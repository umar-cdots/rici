@if(!empty($sheets))
    @foreach($sheets as $sheet)
        <tr>
            <td>{{ $sheet->standard->name }}</td>
            <td>{{ $sheet->food_category->code }}</td>
            <td>{{ $sheet->TD }}</td>
            <td>{{ $sheet->TH }}</td>
            <td>{{ $sheet->TMS }}</td>
            <td>{{ $sheet->TFTE_from }}</td>
            <td>{{ $sheet->TFTE_to }}</td>
            <td>{{ $sheet->TFTE_value }}</td>
        </tr>

    @endforeach
@endif
