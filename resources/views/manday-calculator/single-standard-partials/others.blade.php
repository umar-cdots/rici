
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="cal_table">
            <div class="col-md-6 floting"><label>Calculator for <span
                            id="standard_number">{{ $row->standard->name }}</span></label>
            </div>
            <div class="col-md-6 print_table floting">

                <a href="{{ asset('storage/uploads/documents/others.pdf') }}" target="_blank"><i
                            class="fa fa-print"></i></a>
                &nbsp;&nbsp;<a href="{{ asset('storage/uploads/documents/others2.pdf') }}" target="_blank"><i class="fa fa-print">2</i></a>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-10 floting customFullBtn">
                <button id="calculate-single-button" type="submit" class="btn btn-block btn-success btn_save btn_cal">
                    CALCULATE
                </button>
            </div>
            <div class="col-md-2 floting customFullBtn">
                <button type="button" class="btn btn-block btn-success btn_save btn_cal" onclick="refreshSingle()">
                    <i class="fa fa-refresh"></i>
                </button>
            </div>

            &nbsp;
            &nbsp;
            <div id="single-calc-table">
                @if(!empty($row))
                    <table class="table table-bordered text-center cal_tbl_div">
                        <tbody>
                        <tr>
                            <th></th>
                            <th>Onsite</th>
                            <th>Offsite</th>
                            <th>Total</th>
                        </tr>
                        <tr>
                            <td>Stage 1</td>
                            <td>{{ $stage1_on = $row->stage1_on }}</td>
                            <td>{{ $stage1_off = $row->stage1_off }}</td>
                            <td>{{ $total_stage1 = $stage1_on + $stage1_off }}</td>
                        </tr>
                        <tr>
                            <td>Stage 2</td>
                            <td>{{ $stage2_on = $row->stage2_on }}</td>
                            <td>{{ $stage2_off = $row->stage2_off }}</td>
                            <td>{{ $total_stage2 = $stage2_on + $stage2_off }}</td>
                        </tr>
                        <tr>
                            <td>Surveillance 1</td>
                            <td>{{ $surveillance1_on = $row->surveillance_on }}</td>
                            <td>{{ $surveillance1_off = $row->surveillance_off }}</td>
                            <td>{{ $total_surveillance1 = $surveillance1_on + $surveillance1_off }}</td>
                        </tr>
                        @if($frequency == 'biannual')
                            <tr>
                                <td>Surveillance 2</td>
                                <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                            </tr>
                            <tr>
                                <td>Surveillance 3</td>
                                <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                            </tr>
                            <tr>
                                <td>Surveillance 4</td>
                                <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                            </tr>
                            <tr>
                                <td>Surveillance 5</td>
                                <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                            </tr>

                            <tr>
                                <td><strong>TOTAL</strong></td>
                                <td>
                                    <strong>{{ ($stage1_on + $stage2_on + $surveillance1_on + ($surveillance2_on * 4))  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $stage1_off + $stage2_off + $surveillance1_off + ($surveillance2_off * 4)  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $total_stage1 + $total_stage2 + $total_surveillance1 + ($total_surveillance2 * 4) }}</strong>
                                </td>
                            </tr>

                        @endif
                        @if($frequency == 'annual')
                            <tr>
                                <td>Surveillance 2</td>
                                <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                            </tr>

                            <tr>
                                <td><strong>TOTAL</strong></td>
                                <td>
                                    <strong>{{ $stage1_on + $stage2_on + $surveillance1_on + $surveillance2_on  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $stage1_off + $stage2_off + $surveillance1_off + $surveillance2_off  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $total_stage1 + $total_stage2 + $total_surveillance1 + $total_surveillance2 }}</strong>
                                </td>
                            </tr>
                        @endif

                        <tr>
                            <td>Re-Audit</td>
                            <td>{{ $row->reaudit_on }}</td>
                            <td>{{ $row->reaudit_off }}</td>
                            <td>{{ $row->reaudit_on + $row->reaudit_off }}</td>
                        </tr>
                        </tbody>
                    </table>

                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>
    <div class="clearfix"></div>
</div>

