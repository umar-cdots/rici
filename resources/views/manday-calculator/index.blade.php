@extends('layouts.master')
@section('title', "Man Day Calculator")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark dashboard_heading">Man-Day Calculator</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('man-day-calculator') }}
                    </div>
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Man Day Calculator</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body card_cutom">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Audit Type *</label>
                                <div class="form-group">

                                    <div class="col-md-4 floting no_padding">
                                        <input type="radio" name="audit" class="flat-red audit_radio" value="single"
                                               id="refresh-single" onchange="refresh('single')"
                                               checked>
                                        <label class="job_status">Single Audit</label>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <input type="radio" name="audit" class="flat-red audit_radio" value="ims"
                                               id="refresh-ims" onchange="refresh('ims')">
                                        <label class="job_status">IMS</label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <form id="single-calc" method="post" accept-charset="utf-8">
                            <div class="singleAudit CustomTabs auditType" id="single">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Company Name *</label>
                                            <input type="text" class="form-control" placeholder="Enter Company Name"
                                                   name="company_name"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group full_lbl">
                                            <label>Standard *</label>
                                            <select name="single_standard" class="form-control" id="standard_type"
                                                    required>
                                                <option value="" selected>Select Standard</option>
                                                @if(!empty($standards))
                                                    @foreach($standards as $standard)
                                                        <option value="{{ $standard->id }}"
                                                                @if($standard->standardFamily->name == 'Food')
                                                                data-id="food"
                                                                @elseif ($standard->standardFamily->name == 'Energy Management')
                                                                data-id="energy"
                                                                @else
                                                                data-id="none"
                                                                @endif

                                                        >{{ $standard->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Effective Employees *</label>
                                            <input type="number" class="form-control"
                                                   placeholder="Enter Effective Employees" name="effective_employees"
                                                   min="0" required
                                                   onkeypress="return isNumberKey(event)">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Frequency *</label>
                                            <select class="form-control" name="frequency" required>
                                                <option value="">Select Frequency</option>
                                                @foreach($surveillance_frequencies as $rec)
                                                    <option value="{{ $rec }}" {{ ($rec === 'annual') ? 'selected' : '' }}>{{ $rec }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row" id="food" style="display: none">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>HACCP Plan</label>
                                            <input type="text" class="form-control food_inputs" placeholder="HACCP Plan"
                                                   name="haccp">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Food Category</label>
                                            <select name="food_category" class="form-control"
                                                    id="standarad-food-category">
                                                {{--                                                <option value="">Select Food Category</option>--}}
                                                {{--                                                @if(!empty($food_categories))--}}
                                                {{--                                                    @foreach($food_categories as $food_category)--}}
                                                {{--                                                        <option value="{{ $food_category->id }}" {{ ($food_category->code) === 'C' ? 'selected' : '' }}>{{ $food_category->code }}</option>--}}
                                                {{--                                                    @endforeach--}}
                                                {{--                                                @endif--}}
                                            </select>
                                        </div>
                                    </div>
{{--                                    <div class="col-md-3">--}}
{{--                                        <label>Certified to other Management System?</label>--}}
{{--                                        <div class="form-group">--}}

{{--                                            <div class="col-md-4 floting no_padding">--}}
{{--                                                <input type="radio" name="management" value="yes">--}}
{{--                                                <label class="job_status">Yes</label>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-4 floting">--}}
{{--                                                <input type="radio" name="management" value="no" checked>--}}
{{--                                                <label class="job_status">No</label>--}}
{{--                                            </div>--}}
{{--                                            <div class="clearfix"></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Number of sites</label>
                                            <input type="text" class="form-control food_inputs"
                                                   placeholder="Enter Number of Sites"
                                                   name="no_of_sites">
                                        </div>
                                    </div>
                                    <input type="hidden" name="management" value="Yes">

                                    <div class="clearfix"></div>
                                </div>
                                <div class="row" id="energy" style="display: none">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Annual energy consumption in TJ</label>
                                            <input type="text" class="form-control energy_inputs" name="annual_energy"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Number of energy sources</label>
                                            <input type="text" class="form-control energy_inputs" name="energy_source"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Number of significant energy uses (SEUs)</label>
                                            <input type="text" class="form-control energy_inputs" name="energy_use">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="complexity_container" style="display: none">

                                    <div class="col-md-6">
                                        <label>Complexity *</label>
                                        <div class="form-group energy">

                                            <div class="col-md-4 floting no_padding">
                                                <input type="radio" name="complexity" value="low" checked>
                                                <label class="job_status">Low</label>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <input type="radio" name="complexity"
                                                       value="medium">
                                                <label class="job_status">Medium</label>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <input type="radio" name="complexity" value="high">
                                                <label class="job_status">High</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div id="manday_table_container">


                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">

                                            <div class="cal_table">
                                                <div class="col-md-6 floting"><label>Calculator for <span
                                                                id="standard_number">ISO 9001</span></label>
                                                </div>
                                                {{-- <dix    --}}
                                                <div class="clearfix"></div>
                                                <button id="calculate-single-button" type="submit"
                                                        class="btn btn-block btn-success btn_save btn_cal">
                                                    CALCULATE
                                                </button>
                                                &nbsp;


                                                <div id="single-calc-table">
                                                    <table class="table table-bordered text-center cal_tbl_div">
                                                        <tbody>
                                                        <tr>
                                                            <th></th>
                                                            <th>Onsite</th>
                                                            <th>Offsite</th>
                                                            <th>Total</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Stage 1</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Stage 2</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Surveillance 1</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Surveillance 2</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>TOTAL</strong></td>
                                                            <td><strong>0</strong></td>
                                                            <td><strong>0</strong></td>
                                                            <td><strong>0</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Re-Audit</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                        <div cmanday_table_containerlass="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <form action="" id="multi-calc">
                            <div class="ims_sec CustomTabs auditType" id="ims">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Company Name *</label>
                                            <input type="text" class="form-control" placeholder="Enter Company Name"
                                                   name="company_name"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group full_lbl">
                                            <label>Standard *</label>
                                            <select name="standards[]" class="form-control select2" id="standards"
                                                    multiple
                                                    required>
                                                @if(!empty($single_standards))
                                                    @foreach($single_standards as $standard)
                                                        @if(strtolower($standard->standardFamily->name) == "quality" ||
                                                            strtolower($standard->standardFamily->name) == "environment" || $standard->standardFamily->name == 'Occupational Health and Safety')
                                                            <option value="{{ $standard->id }}">{{ $standard->name }}</option>

                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Effective Employees *</label>
                                            <input type="number" class="form-control" name="effective_employees"
                                                   placeholder="Enter Effective Employees"
                                                   min="0" required
                                                   onkeypress="return isNumberKey(event)">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Frequency *</label>
                                            <select name="frequency" class="form-control" required>
                                                <option value="">Select Frequency</option>
                                                @foreach($surveillance_frequencies as $rec)
                                                    <option value="{{ $rec }}" {{ ($rec === 'annual') ? 'selected' :''  }}>{{ $rec }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="all_calcs" id="all-calcs">
                                    <div class="row mrgn" id="total-calcs">

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">

                                            <div class="cal_table">
                                                <div class="col-md-6 floting"><label>Total Man-Days Calculation</label>
                                                </div>
                                                <div class="col-md-6 print_table floting">
                                                    <a href="#"><i class="fa fa-print"></i></a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <button type="submit"
                                                        class="btn btn-block btn-success btn_save btn_cal">
                                                    CALCULATE
                                                </button>
                                                &nbsp;
                                                <table class="table table-bordered text-center cal_tbl_div">
                                                    <tbody>
                                                    <tr>
                                                        <th></th>
                                                        <th>Onsite</th>
                                                        <th>Offsite</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Stage 1</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Stage 2</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Surveillance 1</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Surveillance 2</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>TOTAL</strong></td>
                                                        <td><strong>0</strong></td>
                                                        <td><strong>0</strong></td>
                                                        <td><strong>0</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Re-Audit</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                        <td>0</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
    <script>

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                width: '100%'
            });
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });


            $('.flat-complexity').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        function myFunction() {
            var checkBox = document.getElementById("chkbox");
            if (checkBox.checked == true) {
                $('#disble_fld').prop("disabled", false);
            } else {
                $('#disble_fld').prop("disabled", true);
            }
        }

        $(document).ready(function () {
            $('#add_address').click(function () {
                var $copyDiv = $('.address_main_div').clone();
                $('.address_add_div').html($copyDiv);
            });
        });

        function checkFunction() {
            var checkText = document.getElementById("check_textarea");
            if (checkText.checked == true) {
                $('#textarea_dis').prop("disabled", false);
            } else {
                $('#textarea_dis').prop("disabled", true);
            }

        }

        $(document).ready(function () {
            $('#add_det').click(function () {
                var $divCopy = $('.cont_det').clone();
                $('.add_cont_det').html($divCopy);
            });
        });

        function stndFunction() {
            var standardCheckbox = document.getElementById("standards_check");
            if (standardCheckbox.checked == true) {
                $('#myModal').modal('toggle');
            } else {

            }
        }

        function showFields() {
            var fieldsShow = document.getElementById("show_flds");
            if (fieldsShow.checked == true) {
                $('#yes_fields').show();
            } else {
                $('#yes_fields').hide();
            }
        }

        $(document).ready(function () {
            $('#phonePop2').click(function () {
                $('#contact_modal2').modal('toggle');
            });
        });

        $('#singleAudit_div').hide();
        $('#ims').hide();

        $('input.audit_radio').on('ifChanged', function () {
            var val = $(this).val();
            $('.auditType').hide();
            $('#' + val).show();
        });

        $('#single').on('ifChanged', function () {
            var val = $(this).val();
            $('.auditType').hide();
            $('#' + val).show();
        });


        $('.dfsfd').change(function () {
            if ($('#singleAuditor_radio').attr('checked')) {
                // alert('checked');
            } else {
                // alert('not checked');
            }
        });


        $('#twtwth').hide();
        $('#fvezroone').hide();
        $('#standard_type').change(function (e) {
            //console.log($(this).find(':selected').attr('data-id'));
            var standard_type = $(this).find(':selected').attr('data-id');

            //
            switch (standard_type) {

                case 'none':
                    $('#food').hide();
                    $('#energy').hide();
                    $("#complexity_container").fadeIn();
                    $("#generate_complexity").hide();


                    $(".food_inputs").each(function () {
                        $(this).removeAttr('required');
                    });

                    $(".energy_inputs").each(function () {
                        $(this).removeAttr('required')
                    });

                    break;
                case 'energy':
                    $('#food').hide();
                    $('#energy').show();
                    $("#complexity_container").fadeOut();

                    $(".food_inputs").each(function () {
                        $(this).removeAttr('required');
                    });

                    $(".energy_inputs").each(function () {
                        $(this).attr('required', 'true')
                    });
                    break;
                case'food':
                    var request = "standard_id=" + $('#standard_type').val();
                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.mandayCalculatorStandardBasedFoodCode') }}",
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        cache: false,
                        data: request,
                        success: function (response) {
                            var html = "";
                            html += '<option value="">Select Food Category</option>';
                            $.each(response.data.foodCategory, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.code + '</option>';
                            });

                            $('#standarad-food-category').html(html);
                            $('#food').show();
                            $('#energy').hide();
                            $("#complexity_container").fadeOut();
                            $("#generate_complexity").hide();

                            $(".food_inputs").each(function () {
                                $(this).attr('required', 'true');
                            });

                            $(".energy_inputs").each(function () {
                                $(this).removeAttr('required')
                            });
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                    break;
            }
        });

        function printTable() {
            var divToPrint = document.getElementById('single-calc-table');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);
        }

        $("#standard_type").change(function () {
            // $('.energy').css('display','block');
            $("#standard_number").text($("#standard_type option:selected").html());
        });


        $("#single-calc").submit(function (e) {
            e.preventDefault();
            var form = $('#single-calc')[0];
            var formData = new FormData(form);


            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('calculate.single') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {

                    if (typeof response == 'string') {
                        toastr['success']("Manday Calculator has been generated.");
                        $("#manday_table_container").html(response);
                    } else {
                        toastr['error']("No data found from sheets.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#standards").change(function () {
            var standards = [];
            //remove the existing tables
            $("#total-calcs").empty();
            $("#standards option:selected").each(function () {
                standards.push($(this).html());
            });
            var unique_standards = standards.filter(function (elem, index, self) {
                return index === self.indexOf(elem);
            });
            if (unique_standards.length > 0) {
                for (var i = 0; i <= unique_standards.length - 1; i++) {
                    $("#total-calcs").append('<div class="col-md-4">\n' +
                        '                                        <label>' + unique_standards[i] + '</label>\n' +
                        '                                        <div class="form-group">\n' +
                        '\n' +
                        '                                            <div class="col-md-4 floting no_padding">\n' +
                        '                                                <input type="radio" name="complexity_' + (i + 1) + '" class="flat-red" value="low" checked>\n' +
                        '                                                <label class="job_status">Low</label>\n' +
                        '                                            </div>\n' +
                        '                                            <div class="col-md-4 floting">\n' +
                        '                                                <input type="radio" name="complexity_' + (i + 1) + '" class="flat-red" value="medium">\n' +
                        '                                                <label class="job_status">Medium</label>\n' +
                        '                                            </div>\n' +
                        '                                            <div class="col-md-4 floting">\n' +
                        '                                                <input type="radio" name="complexity_' + (i + 1) + '" class="flat-red" value="high">\n' +
                        '                                                <label class="job_status">High</label>\n' +
                        '                                            </div>\n' +
                        '                                            <div class="clearfix"></div>\n' +
                        '                                        </div>\n' +
                        '\n' +
                        '                                        <div class="cal_table">\n' +
                        '\n' +
                        '                                            <div class="text-center"><label>Mandays ' + unique_standards[i] + '</label></div>\n' +
                        '\n' +
                        '                                            <div class="clearfix"></div>\n' +
                        '                                            <!--                  \t\t\t<button type="submit" class="btn btn-block btn-success btn_save btn_cal">CALCULATE</button>-->\n' +
                        '\n' +
                        '                                            <!--                  \t\t\t<h5>Total Man-Days</h5>-->\n' +
                        '                                            &nbsp;\n' +
                        '                                            <table class="table table-bordered text-center cal_tbl_div">\n' +
                        '                                                <tbody><tr>\n' +
                        '                                                    <th></th>\n' +
                        '                                                    <th>Onsite</th>\n' +
                        '                                                    <th>Offsite</th>\n' +
                        '                                                    <th>Total</th>\n' +
                        '                                                </tr>\n' +
                        '                                                <tr>\n' +
                        '                                                    <td>Stage 1</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                </tr>\n' +
                        '                                                <tr>\n' +
                        '                                                    <td>Stage 2</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                </tr>\n' +
                        '                                                <tr>\n' +
                        '                                                    <td>Surveillance  1</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                </tr>\n' +
                        '                                                <tr>\n' +
                        '                                                    <td>Surveillance  2</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                </tr>\n' +
                        '                                                <tr>\n' +
                        '                                                    <td><strong>TOTAL</strong></td>\n' +
                        '                                                    <td><strong>0</strong></td>\n' +
                        '                                                    <td><strong>0</strong></td>\n' +
                        '                                                    <td><strong>0</strong></td>\n' +
                        '                                                </tr>\n' +
                        '                                                <tr>\n' +
                        '                                                    <td>Re-Audit</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                    <td>0</td>\n' +
                        '                                                </tr>\n' +
                        '                                                </tbody></table>\n' +
                        '                                        </div>\n' +
                        '                                    </div>');
                }
            }
        });

        $("#multi-calc").submit(function (e) {
            e.preventDefault(e);
            var form = $('#multi-calc')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('calculate.multiple') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    if (typeof response == 'string') {
                        toastr['success']("Manday Calculator has been generated.");
                        $("#all-calcs").html(response);
                    } else {
                        toastr['error']("No data found from sheets.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('#refresh-ims').on('ifChanged', function () {
            refreshSingle();
        });


        $('#refresh-single').on('ifChanged', function () {
            refreshIMS();
        });

        function refreshIMS() {
            $('.select2').val('').trigger('change');
            $("#calculate_btn").removeAttr('disabled');
            $("#multi-calc")[0].reset();
            var html = '<tbody>\n' +
                '                                                <tr>\n' +
                '                                                    <th></th>\n' +
                '                                                    <th>Onsite</th>\n' +
                '                                                    <th>Offsite</th>\n' +
                '                                                    <th>Total</th>\n' +
                '                                                </tr>\n' +
                '                                                <tr>\n' +
                '                                                    <td>Stage 1</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                </tr>\n' +
                '                                                <tr>\n' +
                '                                                    <td>Stage 2</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                </tr>\n' +
                '                                                <tr>\n' +
                '                                                    <td>Surveillance 1</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                </tr>\n' +
                '                                                <tr>\n' +
                '                                                    <td>Surveillance 2</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                </tr>\n' +
                '                                                <tr>\n' +
                '                                                    <td><strong>TOTAL</strong></td>\n' +
                '                                                    <td><strong>0</strong></td>\n' +
                '                                                    <td><strong>0</strong></td>\n' +
                '                                                    <td><strong>0</strong></td>\n' +
                '                                                </tr>\n' +
                '                                                <tr>\n' +
                '                                                    <td>Re-Audit</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                    <td>0</td>\n' +
                '                                                </tr>\n' +
                '                                                </tbody>';
            $("#total_manday_table").html(html);
        }

        function refreshSingle() {
            $("#single-calc")[0].reset();
            $("#food").css('display', 'none');
            $("#energy").css('display', 'none');

            var html = '<table class="table table-bordered text-center cal_tbl_div">\n' +
                '                                                    <tbody>\n' +
                '                                                    <tr>\n' +
                '                                                        <th></th>\n' +
                '                                                        <th>Onsite</th>\n' +
                '                                                        <th>Offsite</th>\n' +
                '                                                        <th>Total</th>\n' +
                '                                                    </tr>\n' +
                '                                                    <tr>\n' +
                '                                                        <td>Stage 1</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                    </tr>\n' +
                '                                                    <tr>\n' +
                '                                                        <td>Stage 2</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                    </tr>\n' +
                '                                                    <tr>\n' +
                '                                                        <td>Surveillance 1</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                    </tr>\n' +
                '                                                    <tr>\n' +
                '                                                        <td>Surveillance 2</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                    </tr>\n' +
                '                                                    <tr>\n' +
                '                                                        <td><strong>TOTAL</strong></td>\n' +
                '                                                        <td><strong>0</strong></td>\n' +
                '                                                        <td><strong>0</strong></td>\n' +
                '                                                        <td><strong>0</strong></td>\n' +
                '                                                    </tr>\n' +
                '                                                    <tr>\n' +
                '                                                        <td>Re-Audit</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                        <td>0</td>\n' +
                '                                                    </tr>\n' +
                '                                                    </tbody>\n' +
                '                                                </table>';
            $("#single-calc-table").html(html);
        }

        function downloadSingleStandard() {


            var form = $('#single-calc')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('download.single.standard') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: request,
                success: function (response) {

                    if (typeof response == 'success') {
                        toastr['success']("Manday Calculator has been generated.");
                        $("#manday_table_container").html(response);
                    } else {
                        toastr['error']("No data found from sheets.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }


    </script>
@endpush