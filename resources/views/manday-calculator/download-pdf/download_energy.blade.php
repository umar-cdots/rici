<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        td {
            font-size: 12px;
        }

        tr:nth-child(even) {
            background-color: #e5f7fd;
        }

        h2 {
            color: #4583c4;
            padding-top: 20px;
        }

        h2 span {
            color: black;
        }

        label {
            color: #4583c4;
            font-size: 15px;
        }

        .tableOuter {
            padding: 0 45px;
            width: 60%;
            margin: 0 auto;
            margin-top: 125px;
            border: 1px solid #dddddd;
        }

        .pageHeader {
            padding: 25px 0;
            margin-bottom: 75px;
        }

        .pageHeader img {
            width: 100%;
        }

        .pageHeader h1 {
            font-size: 25px;
            text-align: center;
            width: 50%;
            margin: 0 auto;
        }

        .riciPage {
            background-color: #f4f6f9;
            color: #000;
            font-family: "Times New Roman";

        }

        .tableHeaderOne {
            margin: 25px 0;
        }

        .tableHeader {
            padding: 7px 0;
        }
    </style>
</head>

<body>
@php
    $image_path = '/img/logo.png';
@endphp
<div class="pageHeader">
    <div class="col-md-2 col-xs-2">
        <img src="{{ public_path() . $image_path }}" alt="">
    </div>
    <div class="col-md-10 col-xs-10">
        {{--        <h1>Manday Calculator </h1>--}}
    </div>
</div>
<div class="clearfix"></div>
<div class="tableHeader">


    <div class="tableHeaderOne">
        <div class="col-md-3 col-xs-3">
            <label for="">Company Name </label>
            <p>{{ ucfirst($company_name) }}</p>
        </div>

        <div class="col-md-3 col-xs-3">
            <label for="">Standard </label>
            <p>{{ $standard_name}}</p>
        </div>

        <div class="col-md-3 col-xs-3">
            <label for="">Effective Employees </label>
            <p>{{ $effective_employess }}</p>
        </div>

    </div>
    <div class="clearfix"></div>
    <div>

        <div class="col-md-3 col-xs-3">
            <label for="">Frequency </label>
            <p>{{ ucfirst($frequency) }}</p>
        </div>
        <div class="col-md-3 col-xs-3">
            <label for="">Complexity</label>
            <p>{{ ucfirst($complexity) }}</p>
        </div>
        <div class="col-md-6 col-xs-6">
            <label for="">Annual Energy Consumption in TJ: </label>
            <p>{{ $c1 }}</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <div>

        <div class="col-md-4 col-xs-4">
            <label for="">Number of energy sources: </label>
            <p>{{$c2 }}</p>
        </div>
        <div class="col-md-6 col-xs-6">
            <label for="">Number of significant evergy uses (SEUs):</label>
            <p>{{ $c3}}</p>
        </div>
    </div>

</div>
<div class="tableOuter" style="padding-top: 10px">
    <h2 style="font-size: 15px;">Manday Calculator for <span>{{$standard_name}}</span></h2>

    @if(!empty($row))
        <table class="table table-bordered text-center cal_tbl_div">
            <tbody>
            <tr>
                <th></th>
                <th>Onsite</th>
                <th>Offsite</th>
                <th>Total</th>
            </tr>
            <tr>
                <td>Stage 1</td>
                <td>{{ $stage1_on = $row->stage1_on }}</td>
                <td>{{ $stage1_off = $row->stage1_off }}</td>
                <td>{{ $total_stage1 = $stage1_on + $stage1_off }}</td>
            </tr>
            <tr>
                <td>Stage 2</td>
                <td>{{ $stage2_on = $row->stage2_on }}</td>
                <td>{{ $stage2_off = $row->stage2_off }}</td>
                <td>{{ $total_stage2 = $stage2_on + $stage2_off }}</td>
            </tr>
            <tr>
                <td>Surveillance 1</td>
                <td>{{ $surveillance1_on = $row->surveillance_on }}</td>
                <td>{{ $surveillance1_off = $row->surveillance_off }}</td>
                <td>{{ $total_surveillance1 = $surveillance1_on + $surveillance1_off }}</td>
            </tr>
            @if($frequency == 'biannual')
                <tr>
                    <td>Surveillance 2</td>
                    <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                    <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                    <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                </tr>

                <tr>
                    <td>Surveillance 3</td>
                    <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                    <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                    <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                </tr>

                <tr>
                    <td>Surveillance 4</td>
                    <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                    <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                    <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                </tr>

                <tr>
                    <td>Surveillance 5</td>
                    <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                    <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                    <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                </tr>

                <tr>
                    <td><strong>TOTAL</strong></td>
                    <td><strong>{{ ($stage1_on + $stage2_on + $surveillance1_on + ($surveillance2_on * 4))  }}</strong>
                    </td>
                    <td>
                        <strong>{{ $stage1_off + $stage2_off + $surveillance1_off + ($surveillance2_off * 4)  }}</strong>
                    </td>
                    <td>
                        <strong>{{ $total_stage1 + $total_stage2 + $total_surveillance1 + ($total_surveillance2 * 4) }}</strong>
                    </td>
                </tr>
            @endif
            @if($frequency == 'annual')
                <tr>
                    <td>Surveillance 2</td>
                    <td>{{ $surveillance2_on = $row->surveillance_on }}</td>
                    <td>{{ $surveillance2_off = $row->surveillance_off }}</td>
                    <td>{{ $total_surveillance2 = $surveillance2_on + $surveillance2_off }}</td>
                </tr>

                <tr>
                    <td><strong>TOTAL</strong></td>
                    <td><strong>{{ $stage1_on + $stage2_on + $surveillance1_on + $surveillance2_on }}</strong></td>
                    <td><strong>{{ $stage1_off + $stage2_off + $surveillance1_off + $surveillance2_off  }}</strong></td>
                    <td>
                        <strong>{{ $total_stage1 + $total_stage2 + $total_surveillance1 + $total_surveillance2 }}</strong>
                    </td>
                </tr>
            @endif

            <tr>
                <td>Re-Audit</td>
                <td>{{ $row->reaudit_on }}</td>
                <td>{{ $row->reaudit_off }}</td>
                <td>{{ $row->reaudit_on + $row->reaudit_off }}</td>
            </tr>
            </tbody>
        </table>

    @endif
</div>
</body>

</html>




