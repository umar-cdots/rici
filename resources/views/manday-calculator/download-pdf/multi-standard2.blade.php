<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        td {
            font-size: 12px;
        }

        tr:nth-child(even) {
            background-color: #e5f7fd;
        }

        h2 {
            color: #4583c4;
            padding-top: 20px;
        }

        h2 span {
            color: black;
        }

        label {
            color: #4583c4;
            font-size: 15px;
        }

        .tableOuter {
            padding: 0 45px;
            width: 60%;
            margin: 0 auto;
            margin-top: 125px;
            border: 1px solid #dddddd;
        }

        .pageHeader {
            padding: 25px 0;
            margin-bottom: 75px;
        }

        .pageHeader img {
            width: 100%;
        }

        .pageHeader h1 {
            font-size: 25px;
            text-align: center;
            width: 50%;
            margin: 0 auto;
        }

        .riciPage {
            background-color: #f4f6f9;
            color: #000;
            font-family: "Times New Roman";

        }

        .tableHeaderOne {
            margin: 25px 0;
        }

        .tableHeader {
            padding: 7px 0;
        }
    </style>
</head>

<body>
@php
    $image_path = '/img/logo.png';
@endphp
<div class="pageHeader">
    <div class="col-md-2 col-xs-2">
        <img src="{{ public_path() . $image_path }}" alt="">
    </div>
    <div class="col-md-10 col-xs-10">
        {{--        <h1>Manday Calculator </h1>--}}
    </div>
</div>
@php
    $total_stage1_on = 0;
    $total_stage1_off = 0;
    $total_stage2_on = 0;
    $total_stage2_off = 0;
    $total_surveillance1_on = 0;
    $total_surveillance1_off = 0;
    $total_surveillance2_on = 0;
    $total_surveillance2_off = 0;
    $total_reaudit_on = 0;
    $total_reaudit_off = 0;

    $grand_total_on = 0;
    $grand_total_off = 0;
@endphp
<div class="clearfix"></div>
<div class="tableHeader">


    <div class="tableHeaderOne">
        <div class="col-md-3 col-xs-3">
            <label for="">Company Name </label>
            <p>{{ ucfirst($company_name) }}</p>
        </div>
        <div class="col-md-3 col-xs-3">
            <label for="">Employees </label>
            <p>{{ $effective_employees }}</p>
        </div>

    </div>
    <div class="clearfix"></div>
    <div>
        <div class="col-md-3 col-xs-9">
            <label for="">Standard </label>
            <p>
                @foreach($rows as $sheet)
                    @php
                        $total_stage1_on +=  $sheet->stage1_on;
                        $total_stage1_off +=  $sheet->stage1_off;
                        $total_stage2_on +=  $sheet->stage2_on;
                        $total_stage2_off +=  $sheet->stage2_off;
                        $total_surveillance1_on +=  $sheet->surveillance_on;
                        $total_surveillance1_off +=  $sheet->surveillance_off;
                        if($frequency == 'biannual'){
                          $total_surveillance2_on +=  $sheet->surveillance_on;
                          $total_surveillance2_off +=  $sheet->surveillance_off;
                        }else{
                             $total_surveillance2_on += $sheet->surveillance_on;
                             $total_surveillance2_off += $sheet->surveillance_off;
                        }
                        $total_reaudit_on += $sheet->reaudit_on;
                        $total_reaudit_off += $sheet->reaudit_off;

                       // $grand_total_on += $total_stage1_on + $total_stage2_on + $total_surveillance1_on + $total_surveillance2_on;
                        //$grand_total_off += $total_stage1_off + $total_stage2_off + $total_surveillance1_off + $total_surveillance2_off;
                    @endphp
                    {{ ucfirst($sheet->standard->name) }},
                @endforeach
            </p>


        </div>
        <div class="col-md-3 col-xs-3"></div>
    </div>


</div>
<div class="tableOuter" style="padding-top: 10px">
    <h2 style="font-size: 15px;">Total Manday Calculations</h2>


    @if(!empty($rows))
        @php

            $total_stage1_stage2 = $total_stage1_on + $total_stage1_off + $total_stage2_on + $total_stage2_off;
            $total_surveillance = 0;
            if($frequency == 'biannual'){
                 $total_surveillance1_on_off = $total_surveillance1_on + $total_surveillance1_off;
                 $total_surveillance2_on_off = $total_surveillance2_on + ($total_surveillance2_off);
            }
             if($frequency == 'annual'){
                 $total_surveillance1_on_off = $total_surveillance1_on + $total_surveillance1_off ;
                 $total_surveillance2_on_off = $total_surveillance2_on + $total_surveillance2_off;
            }


        @endphp
        <table class="table table-bordered text-center cal_tbl_div" id="total_manday_table">
            <tbody>
            <tr>
                <th></th>
                <th>Total</th>
            </tr>
            <tr>
                <td>Stage 1 & 2</td>
                <td>{{ $total_stage1_stage2 }}</td>
            </tr>
            @if($frequency == 'annual')
                <tr>
                    <td>Surveillance 1</td>
                    <td>{{$total_surveillance1_on_off}}</td>

                </tr>
                <tr>
                    <td>Surveillance 2</td>
                    <td>{{$total_surveillance2_on_off}}</td>
                </tr>
            @else
                <tr>
                    <td>Surveillance 1</td>
                    <td>{{$total_surveillance1_on_off}}</td>
                </tr>
                <tr>
                    <td>Surveillance 2</td>
                    <td>{{$total_surveillance2_on_off}}</td>
                </tr>
                <tr>
                    <td>Surveillance 3</td>
                    <td>{{$total_surveillance2_on_off}}</td>
                </tr>
                <tr>
                    <td>Surveillance 4</td>
                    <td>{{$total_surveillance2_on_off}}</td>
                </tr>
                <tr>
                    <td>Surveillance 5</td>
                    <td>{{$total_surveillance2_on_off}}</td>
                </tr>
            @endif
            </tbody>
        </table>

    @endif
</div>
</body>

</html>


