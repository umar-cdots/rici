@if(!empty($rows))
    <div class="row mrgn" id="total-calcs">
        @php
            $total_stage1_on = 0;
            $total_stage1_off = 0;
            $total_stage2_on = 0;
            $total_stage2_off = 0;
            $total_surveillance1_on = 0;
            $total_surveillance1_off = 0;
            $total_surveillance2_on = 0;
            $total_surveillance2_off = 0;
            $total_reaudit_on = 0;
            $total_reaudit_off = 0;

            $grand_total_on = 0;
            $grand_total_off = 0;
        @endphp
        @foreach($rows as $sheet)
            @php
                $total_stage1_on +=  $sheet->stage1_on;
                $total_stage1_off +=  $sheet->stage1_off;
                $total_stage2_on +=  $sheet->stage2_on;
                $total_stage2_off +=  $sheet->stage2_off;
                $total_surveillance1_on +=  $sheet->surveillance_on;
                $total_surveillance1_off +=  $sheet->surveillance_off;
                if($frequency == 'biannual'){
                  $total_surveillance2_on +=  $sheet->surveillance_on;
                  $total_surveillance2_off +=  $sheet->surveillance_off;
                }else{
                     $total_surveillance2_on += $sheet->surveillance_on;
                     $total_surveillance2_off += $sheet->surveillance_off;
                }
                $total_reaudit_on += $sheet->reaudit_on;
                $total_reaudit_off += $sheet->reaudit_off;

               // $grand_total_on += $total_stage1_on + $total_stage2_on + $total_surveillance1_on + $total_surveillance2_on;
                //$grand_total_off += $total_stage1_off + $total_stage2_off + $total_surveillance1_off + $total_surveillance2_off;
            @endphp
            <div class="col-md-4">
                <label>{{ $sheet->standard->name }}</label>
                <div class="form-group">
                    <div class="col-md-4 floting no_padding">
                        <input type="radio" name="complexity{{$loop->iteration}}"
                               value="low" {{ ($complexities[$loop->index] == 'low') ? 'checked' : '' }}>
                        <label class="job_status">Low</label>
                    </div>
                    <div class="col-md-4 floting">
                        <input type="radio" name="complexity{{$loop->iteration}}"
                               value="medium" {{ ($complexities[$loop->index] == 'medium') ? 'checked' : '' }}>
                        <label class="job_status">Medium</label>
                    </div>
                    <div class="col-md-4 floting">
                        <input type="radio" name="complexity{{$loop->iteration}}"
                               value="high" {{ ($complexities[$loop->index] == 'high') ? 'checked' : '' }}>
                        <label class="job_status">High</label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="cal_table">

                    <div class="text-center"><label>{{ $sheet->standard->name }}</label></div>

                    <div class="clearfix"></div>
                    &nbsp;
                    <table class="table table-bordered text-center cal_tbl_div">
                        <tbody>
                        <tr>
                            <th></th>
                            <th>Onsite</th>
                            <th>Offsite</th>
                            <th>Total</th>
                        </tr>
                        <tr>
                            <td>Stage 1</td>
                            <td>{{ $sheet->stage1_on }}</td>
                            <td>{{ $sheet->stage1_off }}</td>
                            <td>{{ $total_stage1 = $sheet->stage1_on + $sheet->stage1_off }}</td>
                        </tr>
                        <tr>
                            <td>Stage 2</td>
                            <td>{{ $sheet->stage2_on }}</td>
                            <td>{{ $sheet->stage2_off }}</td>
                            <td>{{ $total_stage2 = $sheet->stage2_on + $sheet->stage2_off }}</td>
                        </tr>
                        <tr>
                            <td>Surveillance 1</td>
                            <td>{{ $sheet->surveillance_on }}</td>
                            <td>{{ $sheet->surveillance_off }}</td>
                            <td>{{ $total_surveillance1 = $sheet->surveillance_on + $sheet->surveillance_off }}</td>
                        </tr>
                        @if($frequency == 'biannual')
                            <tr>
                                <td>Surveillance 2</td>
                                <td>{{ $surveillance2_on = $sheet->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $sheet->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $sheet->surveillance_on + $sheet->surveillance_off }}</td>
                            </tr>

                            <tr>
                                <td>Surveillance 3</td>
                                <td>{{ $surveillance2_on = $sheet->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $sheet->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $sheet->surveillance_on + $sheet->surveillance_off }}</td>
                            </tr>

                            <tr>
                                <td>Surveillance 4</td>
                                <td>{{ $surveillance2_on = $sheet->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $sheet->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $sheet->surveillance_on + $sheet->surveillance_off }}</td>
                            </tr>

                            <tr>
                                <td>Surveillance 5</td>
                                <td>{{ $surveillance2_on = $sheet->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $sheet->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $sheet->surveillance_on + $sheet->surveillance_off }}</td>
                            </tr>

                            <tr>
                                <td><strong>TOTAL</strong></td>
                                <td>
                                    <strong>{{ $sheet->stage1_on + $sheet->stage2_on + $sheet->surveillance_on + ($sheet->surveillance_on * 4)  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $sheet->stage1_off + $sheet->stage2_off + $sheet->surveillance_off + ($sheet->surveillance_off * 4)  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $total_stage1 + $total_stage2 + $total_surveillance1 + ($total_surveillance2 * 4) }}</strong>
                                </td>
                            </tr>
                        @endif
                        @if($frequency == 'annual')
                            <tr>
                                <td>Surveillance 2</td>
                                <td>{{ $surveillance2_on = $sheet->surveillance_on }}</td>
                                <td>{{ $surveillance2_off = $sheet->surveillance_off }}</td>
                                <td>{{ $total_surveillance2 = $sheet->surveillance_on + $sheet->surveillance_off }}</td>
                            </tr>

                            <tr>
                                <td><strong>TOTAL</strong></td>
                                <td>
                                    <strong>{{ $sheet->stage1_on + $sheet->stage2_on + $sheet->surveillance_on + $surveillance2_on  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $sheet->stage1_off + $sheet->stage2_off + $sheet->surveillance_off + $surveillance2_off  }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $total_stage1 + $total_stage2 + $total_surveillance1 + $total_surveillance2 }}</strong>
                                </td>
                            </tr>
                        @endif

                        <tr>
                            <td>Re-Audit</td>
                            <td>{{ $sheet->reaudit_on }}</td>
                            <td>{{ $sheet->reaudit_off }}</td>
                            <td>{{ $sheet->reaudit_on + $sheet->reaudit_off }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">

            <div class="cal_table">
                <div class="col-md-6 floting"><label>Total Man-Days Calculation</label></div>
                <div class="col-md-6 print_table floting">
                    <a href="{{ asset('storage/uploads/documents/multi_standard.pdf') }}" target="_blank"><i
                                class="fa fa-print"></i></a>
                    &nbsp;&nbsp;<a href="{{ asset('storage/uploads/documents/multi_standard2.pdf') }}" target="_blank"><i class="fa fa-print">2</i></a>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-10 floting customFullBtn">
                    <button type="submit" disabled id="calculate_btn"
                            class="btn btn-block btn-success btn_save btn_cal">
                        CALCULATE
                    </button>
                </div>
                <div class="col-md-2 floting customFullBtn">
                    <button type="button" class="btn btn-block btn-success btn_save btn_cal" onclick="refreshIMS()">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
                &nbsp;
                <table class="table table-bordered text-center cal_tbl_div" id="total_manday_table">
                    <tbody>
                    <tr>
                        <th></th>
                        <th>Onsite</th>
                        <th>Offsite</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <td>Stage 1</td>
                        <td>{{ $total_stage1_on }}</td>
                        <td>{{ $total_stage1_off }}</td>
                        <td>{{ $total_stage_1 = $total_stage1_on + $total_stage1_off }}</td>
                    </tr>
                    <tr>
                        <td>Stage 2</td>
                        <td>{{ $total_stage2_on }}</td>
                        <td>{{ $total_stage2_off }}</td>
                        <td>{{ $total_stage_2 = $total_stage2_on + $total_stage2_off }}</td>
                    </tr>
                    <tr>
                        <td>Surveillance 1</td>
                        <td>{{ $total_surveillance1_on }}</td>
                        <td>{{ $total_surveillance1_off }}</td>
                        <td>{{ $total_surveillance1 = $total_surveillance1_on + $total_surveillance1_off }}</td>
                    </tr>
                    @if($frequency == 'biannual')
                        <tr>
                            <td>Surveillance 2</td>
                            <td>{{ $total_surveillance2_on }}</td>
                            <td>{{ $total_surveillance2_off }}</td>
                            <td>{{ $total_surveillance2 = $total_surveillance2_on + $total_surveillance2_off }}</td>
                        </tr>

                        <tr>
                            <td>Surveillance 3</td>
                            <td>{{ $total_surveillance2_on }}</td>
                            <td>{{ $total_surveillance2_off }}</td>
                            <td>{{ $total_surveillance2 = $total_surveillance2_on + $total_surveillance2_off }}</td>
                        </tr>

                        <tr>
                            <td>Surveillance 4</td>
                            <td>{{ $total_surveillance2_on }}</td>
                            <td>{{ $total_surveillance2_off }}</td>
                            <td>{{ $total_surveillance2 = $total_surveillance2_on + $total_surveillance2_off }}</td>
                        </tr>

                        <tr>
                            <td>Surveillance 5</td>
                            <td>{{ $total_surveillance2_on }}</td>
                            <td>{{ $total_surveillance2_off }}</td>
                            <td>{{ $total_surveillance2 = $total_surveillance2_on + $total_surveillance2_off }}</td>
                        </tr>

                        <tr>
                            <td><strong>TOTAL</strong></td>
                            <td>
                                <strong>{{ $total_stage1_on + $total_stage2_on + $total_surveillance1_on + ($total_surveillance2_on * 4) }}</strong>
                            </td>
                            <td>
                                <strong>{{ $total_stage1_off + $total_stage2_off + $total_surveillance1_off + ($total_surveillance2_off * 4) }}</strong>
                            </td>
                            <td>
                                <strong>{{ $total_stage_1 + $total_stage_2 + $total_surveillance1 + ($total_surveillance2 * 4) }}</strong>
                            </td>
                        </tr>
                    @endif
                    @if($frequency == 'annual')
                        <tr>
                            <td>Surveillance 2</td>
                            <td>{{ $total_surveillance2_on }}</td>
                            <td>{{ $total_surveillance2_off }}</td>
                            <td>{{ $total_surveillance2 = $total_surveillance2_on + $total_surveillance2_off }}</td>
                        </tr>

                        <tr>
                            <td><strong>TOTAL</strong></td>
                            <td>
                                <strong>{{ $total_stage1_on + $total_stage2_on + $total_surveillance1_on + $total_surveillance2_on }}</strong>
                            </td>
                            <td>
                                <strong>{{ $total_stage1_off + $total_stage2_off + $total_surveillance1_off + $total_surveillance2_off }}</strong>
                            </td>
                            <td>
                                <strong>{{ $total_stage_1 + $total_stage_2 + $total_surveillance1 + $total_surveillance2 }}</strong>
                            </td>
                        </tr>
                    @endif

                    <tr>
                        <td>Re-Audit</td>
                        <td>{{ $total_reaudit_on }}</td>
                        <td>{{ $total_reaudit_off }}</td>
                        <td>{{ $total_reaudit_on + $total_reaudit_off }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
@endif
