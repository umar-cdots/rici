@extends('layouts.master')
@section('title', "Audits Conducted")


@push('css')
    <style>
        #number_of_auditor_standard_wise_wrapper, #number_of_technical_expert_standard_wise_wrapper, #auditor_evaluations_due_standard_wise_wrapper {
            overflow-y: scroll;
        }
    </style>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
{{--                        <div class="col-sm-4 floting">--}}
{{--                            <h1>Certified Per Year</h1>--}}
{{--                        </div>--}}
                        <div class="col-sm-6 floting">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('audit-conducted') }}
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Audits Conducted</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="scrollableTable">
                                <table id="number_of_auditor_standard_wise" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Year(s)</th>
                                        <th>Total</th>
                                        <th>Stage I</th>
                                        <th>Stage II</th>
                                        <th>Surveillance </th>
                                        <th>Re-audits</th>
                                    </tr>


                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @if(!empty($years) && count($years) > 0)
                                        @foreach($years as $year)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>
                                                    <a href="#"
                                                       onclick="show_standard_graph('{{$year->year}}')">{{ $year->year }}</a>
                                                </td>
                                                @php
                                                    $PostAuditCountStageWise = \App\AJStandardStage::whereHas('postAudit',function ($query) use ($year){
                                                        $query->whereYear('actual_audit_date',$year->year);
                                                    })->whereIn('audit_type',['surveillance_1','surveillance_2','surveillance_3','surveillance_4','surveillance_5','stage_1','stage_2','reaudit'])->get();
                                                @endphp
                                                <td>{{ (count($PostAuditCountStageWise) > 0) ?  count($PostAuditCountStageWise) : 0 }}</td>
                                                <td>
                                                    @php
                                                    $PostAuditCountStageWise = \App\AJStandardStage::whereHas('postAudit',function ($query) use ($year){
                                                        $query->whereYear('actual_audit_date',$year->year);
                                                    })->where('audit_type','stage_1')->get();
                                                    @endphp
                                                    {{ (count($PostAuditCountStageWise) > 0) ?  count($PostAuditCountStageWise) : 0 }}
                                                    </td>
                                                    <td>
                                                    @php
                                                    $PostAuditCountStageWise = \App\AJStandardStage::whereHas('postAudit',function ($query) use ($year){
                                                        $query->whereYear('actual_audit_date',$year->year);
                                                    })->where('audit_type','stage_2')->get();
                                                    @endphp
                                                    {{ (count($PostAuditCountStageWise) > 0) ?  count($PostAuditCountStageWise) : 0 }}
                                                    </td>
                                                    <td>
                                                        @php
                                                        $PostAuditCountStageWise = \App\AJStandardStage::whereHas('postAudit',function ($query) use ($year){
                                                            $query->whereYear('actual_audit_date',$year->year);
                                                            })->whereIn('audit_type',['surveillance_1','surveillance_2','surveillance_3','surveillance_4','surveillance_5'])->get();
                                                            @endphp
                                                            {{ (count($PostAuditCountStageWise) > 0) ?  count($PostAuditCountStageWise) : 0 }}
                                                        </td>
                                                        <td>
                                                                @php
                                                                $PostAuditCountStageWise = \App\AJStandardStage::whereHas('postAudit',function ($query) use ($year){
                                                                    $query->whereYear('actual_audit_date',$year->year);
                                                                })->where('audit_type','reaudit')->get();
                                                                @endphp
                                                                {{ (count($PostAuditCountStageWise) > 0) ?  count($PostAuditCountStageWise) : 0 }}
                                                        </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    <style type="text/css">
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

    <script>
        function show_standard_graph(year) {
            var dataPoints = [];
            var standardName = '';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('no-of-audit-conducted-graph') }}",
                data: {year: year},
                success: function (response) {

                    var chart = new CanvasJS.Chart("chartContainer", {
                        animationEnabled: true,
                        theme: 'theme4',
                        title: {text: ''},
                        axisY: {title: year},
                        axisX: {
                            interval: 1,
                            labelAngle: -70,
                            title: 'Audit Conducted'
                        },
                        exportEnabled: true,
                        data: [{
                            type: 'column',
                            indexLabel: '{y}',
                            indexLabelPlacement: 'outside',
                            indexLabelOrientation: 'horizontal',
                            indexLabel: '{y}',
                            dataPoints: dataPoints,
                        }]
                    });


                    $.each(response.graph_data_stage, function (key, value) {
                        dataPoints.push({label: value, y: parseInt(response.graph_data_count[key])});
                    });
                    chart.render();
                    jQuery('html, body').animate({
                        scrollTop: jQuery('#chartContainer').offset().top
                    }, 2000);
                    $(".canvasjs-chart-credit").hide();
                },
                error: function (error) {
                    toastr['error']('something went wrong');
                }
            });

        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#number_of_auditor_standard_wise').dataTable({
                "order": [[0, "asc"]],
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ]
            })

        });
    </script>
@endpush