@extends('layouts.master')
@section('title', "Scheme Coordinator")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <form action="" id="main-form">
            <section class="content">
                <div class="container-fluid dashboard_tabs">
                    <div class="row mb-2 mrgn">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-dark dashboard_heading">ADD <span>SCHEME COORDINATOR</span></h1>

                        </div><!-- /.col -->
                        <!-- /.col -->
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Full Name"
                                                   name="fullname">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Enter Email"
                                                   name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth *</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                        </div>
                                        <input type="text" name="dob" id="dob"
                                               placeholder="dd//mm/yyyy"
                                               class="float-right active form-control dob">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country *</label>
                                        <select name="country_id" class="form-control countries">
                                            <option value="">Select Country</option>
                                            @if(!empty($countries))
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City *</label>
                                        <select name="city_id" class="form-control cities">
                                            <option value="">Select City</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Cell Number *</label>
                                    <div class="row">

                                        <div class="col-md-2" style="padding-right: 0;">
                                            <input type="text" class="form-control" value="" id="country_code"
                                                   name="country_code" readonly>
                                        </div>
                                        <div class="col-md-10" style="padding-left: 0;">
                                            <div class="form-group mb-0">

                                                <input type="text" class="form-control popFld phone" name="cell_no"
                                                       onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>landline Number *</label>
                                        <input type="tel" class="form-control popFld landline" name="landline">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Joining Date *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                            </div>
                                            <input type="text" name="joining_date" id="joining_date"
                                                   placeholder="dd//mm/yyyy"
                                                   class="float-right active form-control joining_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Address *</label>
                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter Address"
                                               name="address">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Account Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Status *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="r3" class="icheck-blue" value="active" checked>
                                            <label class="job_status">Active</label>
                                        </div>
{{--                                        <div class="col-md-4 floting">--}}
{{--                                            <input type="radio" name="r3" class="icheck-blue" value="suspended">--}}
{{--                                            <label class="job_status">Suspended</label>--}}
{{--                                        </div>--}}
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="r3" class="icheck-blue" value="withdrawal">
                                            <label class="job_status">Withdrawal</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Scheme Manager *</label>
                                        <select name="scheme_manager_id" class="form-control" id="manager_id">
                                            <option value="0" selected>
                                                Select Scheme Manager
                                            </option>
                                            @if(!empty($scheme_managers))
                                                @foreach($scheme_managers as $scheme_manager)
                                                    <option value="{{ $scheme_manager->scheme_manager['id'] }}">{{ $scheme_manager->scheme_manager->full_name() }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label>Scheme Manager Standards</label>
                                        <select id="scheme_manager_standards" class="js-example-basic-multiple"
                                                name="standard_id[]" multiple="multiple" style="width: 400px;">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username *</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"
                                                                          aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="username">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Password *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-unlock-alt"
                                                                              aria-hidden="true"></i></span>
                                            </div>
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Confirm Password *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-unlock-alt"
                                                                              aria-hidden="true"></i></span>
                                            </div>
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Remarks *</label>
                                        <textarea name="remarks" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <label>Signature Image * <span>Max 10mb</span></label>
                                        <div class="form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input form-control"
                                                       name="signature_file"
                                                       id="exampleInputFile1" onchange="readURLMany(this,1)">
                                                <label class="custom-file-label" for="exampleInputFile1">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld1">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Profile Image <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="profile_image"
                                                       id="exampleInputFile2" onchange="readURLMany(this,2)">
                                                <label class="custom-file-label" for="exampleInputFile2">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld2">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Additional Files <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="additional_files"
                                                       id="exampleInputFile3" onchange="readURLMany(this,3)">
                                                <label class="custom-file-label" for="exampleInputFile3">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group" id="file">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-primary mrgn">
                                <div class="card-header cardNewHeader">
                                    <h3 class="card-title">Role & Permission</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body card_cutom">
                                    <table class="table table-responsive table-light">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Role</th>
                                            <th>Guard Name</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                @if($role->id == 3)
                                                    <tr>
                                                        <td><input type="radio" class="icheck-blue" name="roles[]"
                                                                   value="{{ $role->id }}" {{ ($role->id === 3) ? 'checked' : '' }}>
                                                        </td>
                                                        <td>{{ ucwords($role->name) }}</td>
                                                        <td>{{ $role->guard_name }}</td>
                                                        <td>
                                                            @if($role->permissions->isNotEmpty())
                                                                @foreach($role->permissions as $permission)
                                                                    @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                                        ,
                                                                    @endif
                                                                    {{ ucwords($permission->heading) }}
                                                                @endforeach
                                                            @else
                                                                no permissions
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

{{--                        <div class="col-sm-5">--}}
{{--                            <div class="card card-primary mrgn">--}}
{{--                                <div class="card-header cardNewHeader">--}}
{{--                                    <h3 class="card-title">Permissions</h3>--}}
{{--                                </div>--}}
{{--                                <!-- /.card-header -->--}}
{{--                                <div class="card-body card_cutom">--}}
{{--                                    <table class="table table-responsive table-light">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th>Assign</th>--}}
{{--                                            <th>Role</th>--}}
{{--                                            <th>Guard Name</th>--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                        @if(!empty($permissions))--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                <tr>--}}
{{--                                                    <td><input type="checkbox" class="icheck-blue" name="permissions[]"--}}
{{--                                                               value="{{ $permission->id }}"></td>--}}
{{--                                                    <td>{{ ucwords($permission->name) }}</td>--}}
{{--                                                    <td>{{ $permission->guard_name }}</td>--}}
{{--                                                </tr>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                </div>--}}
{{--                                <!-- /.card-body -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-3 floting"></div>
                            <div class="col-md-3 floting">
                                <a href="{{ route('scheme-coordinator.index') }}"
                                   class="btn btn-block btn-danger  btn_cancel">Cancel
                                </a>
                            </div>
                            <div class="col-md-3 floting no_padding">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success btn_save">Save
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- /.content -->
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('.joining_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {

            var joininDateArray = this.value.split('-');
            var newJoiningDate = joininDateArray[2] + '-' + joininDateArray[1] + '-' + joininDateArray[0];
            var dobArray = $('.dob').val().split('-');
            var newdobDate = dobArray[2] + '-' + dobArray[1] + '-' + dobArray[0];


            var dob = moment(newdobDate).format('YYYY-MM-DD');
            var joiningDate = moment(newJoiningDate).format('YYYY-MM-DD');

            if (joiningDate > dob) {
            } else {

                toastr['error']('Joining Date always greater than DOB');
                $('#joining_date').val('');
            }


        });

        {{--var intlTelInputConfig = {--}}
        {{--    initialCountry: "auto",--}}
        {{--    geoIpLookup: function (callback) {--}}
        {{--        $.get("{{ route('ajax.ipInfo') }}", function () {--}}
        {{--        }, "jsonp").always(function (resp) {--}}
        {{--            var countryCode = (resp && resp.country) ? resp.country : "";--}}
        {{--            callback(countryCode);--}}
        {{--        });--}}
        {{--    },--}}
        {{--    utilsScript: "{{ asset('plugins/intlTelInput/js/utils.js') }}", // just for formatting/placeholders etc--}}
        {{--    blur: function () {--}}
        {{--        console.log($(this));--}}
        {{--    }--}}
        {{--};--}}
        {{--$(".phone").intlTelInput(intlTelInputConfig);--}}
        {{--$(".landline").intlTelInput(intlTelInputConfig);--}}
        {{--$('.landline,.phone').on('keyup blur', function () {--}}
        {{--    var number = $(this).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);--}}
        {{--    if (number == '' || !$(this).intlTelInput("isValidNumber")) {--}}
        {{--        has_error = true;--}}
        {{--        $(".btn_save").attr("disabled", true);--}}
        {{--        $(this).closest('.form-group').addClass('text-danger');--}}
        {{--        $(this).addClass('is-invalid');--}}
        {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
        {{--        $(this).closest('.form-group').append('<small class="text-danger" style="float:left">Invalid Contact Number.</small>');--}}
        {{--    } else {--}}
        {{--        $(this).val(number);--}}
        {{--        $(".btn_save").attr("disabled", false);--}}
        {{--        $(this).closest('.form-group').removeClass('text-danger');--}}
        {{--        $(this).removeClass('is-invalid');--}}
        {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
        {{--    }--}}
        {{--});--}}

        function readURLMany(input, id = null) {
            if (input.id == 'exampleInputFile3') {
                var filename = input.files[0].name;
                var html = '';
                if (input.files && input.files[0]) {
                    html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
                }
                $('#file').html(html);
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#upld' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $('.js-example-basic-multiple').select2();

        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        $('#main-form').submit(function (e) {
            e.preventDefault();
            var form = $('#main-form')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('scheme-coordinator.store') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        setTimeout(function () {
                            window.location.href = '{{ route('scheme-coordinator.index') }}';
                        }, 2000);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#manager_id").change(function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ route('get.scheme.manager.standard') }}",
                method: "POST",
                data: {id: id},
                cache: true,
                success: function (response) {
                    var html = '';
                    $(response.data).each(function (index, value) {
                        html += '<option selected>' + value.standard_family.name + '</option>';
                    });

                    $("#scheme_manager_standards").html(html);
                    $("#scheme_manager_standards").attr('disabled', true);
                    $("#scheme_manager_standards").select2().trigger('change');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

    </script>
@endpush