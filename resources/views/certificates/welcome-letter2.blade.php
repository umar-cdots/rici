<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Welcome Letter</title>
@if (isset($pathToFile))
    <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    @endif
    <style type="text/css">
        body {
            /*font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana, " sans-serif";*/
        }

        .header {
            width: 80%;
            margin: 0 auto;
        }

        .content {
            width: 80%;
            margin: 0 auto;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        p {
            font-size: 17px;
            margin-bottom: 5px;
        }

        ul {
            padding-left: 15px;
        }

        ul li {
            font-size: 15px;
            line-height: 22px;
        }

        .footer {
            border-top: #ccc solid 1px;
            margin-top: 70px;
            padding-top: 20px;
        }

        @page {
            size: 35cm ;
        }
    </style>
</head>

<body>
@php
    $logo = '/img/logo.png';

@endphp
<div class="header">
    <table width="100%">
        <tr>
            <td width="20%">
                <img src="{{ public_path() . $logo }}" alt="" >
            </td>
            <td style="text-align: center; font-size: 30px; font-weight: 700;">{{$riciName}}
            </td>
        </tr>
    </table>
</div>
<div class="content">
    <div class="text-right">

        @php
            $postAuditApprovalDate = \App\PostAudit::where('company_id',$companyStandard->company_id)->where('aj_standard_stage_id',$current_stage_1->id)->where('standard_id',$companyStandard->standard_id)->with('postAuditSchemeInfo')->first();

        @endphp

        <p>{{($postAuditApprovalDate && $postAuditApprovalDate->postAuditSchemeInfo) ? date("d F Y",strtotime($postAuditApprovalDate->postAuditSchemeInfo->approval_date)) : ''}}</p>
    </div>
    <div class="text-left">
        <p></p>
        <p>{{$companyStandard->company->primaryContact->title}}
            . {{ucfirst($companyStandard->company->primaryContact->name)}} </p>
        <p>{{$companyStandard->company->primaryContact->position}}</p>
        <p>{{$companyStandard->company->name}}</p>
        <p>{{$companyStandard->company->address}}</p>
        <p>{{$companyStandard->company->city->name}},&nbsp;{{$companyStandard->company->country->name}}.</p>
    </div>
    <div class="text-center">
        <p><strong>CONTINUATION OF CERTIFICATION</strong></p>
    </div>
    <div class="text-left">
        <p>Dear {{$companyStandard->company->primaryContact->title}}
            . {{ucfirst($companyStandard->company->primaryContact->name)}},</p>
        <p>I hope this letter finds you in best of health and business.</p>
        <p>We are pleased to inform you that audit of your organization has successfully concluded and decision
            for continuaton of certification has been taken for:-</p>
        <ul>
            @php
                $ims_heading=[];
              $company = \App\Company::where('id', $companyStandard->company_id)->first();
              $companyStandard = \App\CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
              $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
              if($companyStandard->is_ims == false){
                     array_push($ims_heading,$companyStandard->standard->name);
              }else{
                    array_push($ims_heading,$companyStandard->standard->name);
                 if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                  foreach ($imsCompanyStandards as $imsCompanyStandard) {
                      if ($companyStandard->id == $imsCompanyStandard->id) {
                          continue;
                      } else {
                         array_push($ims_heading,$imsCompanyStandard->standard->name);

                      }
                  }
              } else {

              }
              }
            @endphp
            @foreach($ims_heading as $std)
                <li>{{$std}}</li>
            @endforeach
        </ul>

        <p>The name of your organization remains in our online directory of certified clients, which could be verified
            by scanning QR code on the certificate or through track your certificate option our website <a href="#">www.ricionline.com</a>
            .</p>
        <p>Your next audit activity is "{{str_replace("_"," ",ucfirst($ajStandardStage->audit_type))}}" which is due in
            the month of "{{date("F Y",strtotime($ajStandardStage->excepted_date))}}". You are kindly requested to abide
            by all certification rules and regulations as communicated in “{{$doc_one}}”.</p>
        <p>In-case you have any query or issue during the certification cycle, please feel free to contact the
            undersigned or our country office representative. </p>
        <p>We are pleased to have you on our client list and are looking forward towards a long-term relationship with
            your organization.</p>
        <p>Kind regards,</p>
        <img style="margin-top: 5px"
             src="{{ public_path() . "/uploads/scheme_managers/". $companyStandard->standard->scheme_manager[0]->signature_image}}" alt=""
             width="80px">
        <p>{{$companyStandard->standard->scheme_manager[0]->full_name}}</p>
        <p>Scheme Manager </p>
        <p>{{$riciName}}</p>
    </div>
    <div class="footer text-center">
        <p>{{$riciAddress}}</p>
        <p>Email: <a href="mailto:scheme@ricionline.com">scheme@ricionline.com</a></p>
    </div>
</div>
</body>
</html>
