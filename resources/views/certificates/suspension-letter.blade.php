<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Suspension Letter</title>
<style type="text/css">
	body {
		/*font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana, " sans-serif";*/
	}

	.header {
		width: 80%;
		margin: 0 auto;
	}

	.content {
		width: 80%;
		margin: 0 auto;
	}

	.text-right {
		text-align: right;
	}

	.text-center {
		text-align: center;
	}

	p {
		font-size: 17px;
		margin-bottom: 5px;
	}

	ul {
		padding-left: 15px;
	}

	ul li {
		font-size: 15px;
		line-height: 22px;
	}

	.footer {
		border-top: #ccc solid 1px;
		margin-top: 70px;
		padding-top: 20px;
	}

	@page {
		size: 36cm ;
	}
</style>
</head>

<body>
	@php
		$logo = '/img/logo.png';
	
	@endphp
<div class="header">
	<table width="100%">
		<tr>
			<td width="20%">
				<img src="{{ public_path() . $logo }}" alt="" >
			</td>
			<td style="text-align: center; font-size: 30px; font-weight: 700;">{{$riciName}}
			</td>
		</tr>
	</table>
</div>
<div class="content">
	<div class="text-right">
		
		<p>{{$companyStandard->certificates && $companyStandard->certificateSuspension ? date("d F Y",strtotime($companyStandard->certificateSuspension->certificate_date)) : ''}}</p>
	</div>
	<div class="text-left">
		<p></p>
		<p>{{$companyStandard->company->primaryContact->title}}. {{ucfirst($companyStandard->company->primaryContact->name)}} </p>
		<p>{{$companyStandard->company->primaryContact->position}}</p>
		<p>{{$companyStandard->company->name}}</p>
		<p>{{$companyStandard->company->address}}</p>
		<p>{{$companyStandard->company->city->name}},&nbsp;{{$companyStandard->company->country->name}}.</p>
	</div>
	<div class="text-center">
		<p><strong>SUSPENSION OF CERTIFICATION.</strong></p>
	</div>
	<div class="text-left">
		<p>Dear {{$companyStandard->company->primaryContact->title}}. {{ucfirst($companyStandard->company->primaryContact->name)}},</p>
		<p>I hope this letter finds you in best of health and business. As you are aware your organization is certified with {{$riciName}} for:</p>
		<ul>
			@php
				$ims_heading=[];
              $company = \App\Company::where('id', $companyStandard->company_id)->first();
              $companyStandard = \App\CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
              $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
              if($companyStandard->is_ims == false){
                     array_push($ims_heading,$companyStandard->standard->name);
              }else{
                    array_push($ims_heading,$companyStandard->standard->name);
                 if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                  foreach ($imsCompanyStandards as $imsCompanyStandard) {
                      if ($companyStandard->id == $imsCompanyStandard->id) {
                          continue;
                      } else {
                         array_push($ims_heading,$imsCompanyStandard->standard->name);

                      }
                  }
              } else {

              }
              }
			@endphp
			@foreach($ims_heading as $std)
				<li>{{$std}}</li>
			@endforeach
		</ul>
		<p>One of the important conditions for continuation of certification is timely execution of the due audits. Our operations team has been in contact with your organization to arrange for the {{str_replace("_"," ",ucfirst($ajStandardStage->audit_type))}}  activity which is overdue since {{date("d/m/Y",strtotime($ajStandardStage->excepted_date))}} . </p>
		<p>As we have not been able to get the audit conducted on time, we would like to inform you that your organization’s certification is being suspended. The online directory of RICI available on <a href="#">www.ricionline.com</a> has been updated for this change of certification status.</p>
		<p>As per the suspension rules, please be advised you not allowed to claim “Active certification” or use the RICI or Accreditation logos on your websites, stationery or in any other advertising claim in a way which implies your certification is active. </p>
		<p>In-case you are interested in re-instatement of the certification please contact our local office or undersigned within 5 working days. We would be pleased to have you back on our client list. If we do not get a positive written reply from your side within the time frame above. We would be considering you are no longer interested in continuing the certification and the certification status of your organization will changed to that of withdrawn in our system. You would be required to send back the original certificate to our local office as the certificate issued is the property of RICI and refrain from making any future claims regarding certification with RICI.</p>
		<p>Looking forward towards your positive response.</p>
		<p>Kind regards,</p>
		<img style="margin-top: 5px"
			 src="{{ public_path() . "/uploads/scheme_managers/". $companyStandard->standard->scheme_manager[0]->signature_image}}" alt=""
			 width="80px">
		<p>{{$companyStandard->standard->scheme_manager[0]->full_name}}</p>
		<p>Scheme Manager </p>
		<p>{{$riciName}}</p>
	</div>
	<div class="footer text-center">
		<p>{{$riciAddress}}</p>
		<p>Email: <a href="mailto:scheme@ricionline.com">scheme@ricionline.com</a></p>
	</div>
</div>
</body>
</html>
