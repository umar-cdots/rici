<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style type="text/css">
	body {
		/*font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana, " sans-serif";*/
	}

	.header {
		width: 80%;
		margin: 0 auto;
	}

	.content {
		width: 80%;
		margin: 0 auto;
	}

	.text-right {
		text-align: right;
	}

	.text-center {
		text-align: center;
	}

	p {
		font-size: 18px;
		margin-bottom: 5px;
	}

	ul {
		padding-left: 15px;
	}

	ul li {
		font-size: 15px;
		line-height: 22px;
	}

	.footer {
		border-top: #ccc solid 1px;
		margin-top: 30px;
		padding-top: 10px;
	}

	@page {
		size: 35cm ;
	}
</style>
</head>

<body>
	@php
		$logo = '/img/logo.png';
		$sign = '/img/wasifSign.jpg';
	@endphp
<div class="main">
	<div class="addressArea">
		<p>The management system of</p>
		<h3>Ramsis Engineering Co. W.L.L. (Company Name)</h3>
		<p>Yard 1305, Road 5136, Askar 951, East Riffa, Kingdom of Bahrain (HO Address, City, Country). </p>
		<p>Office 332, Road 5136, West Moharaq, Kingdom of Bahrain (Address 2, City, Country) </p>
		<p>Scrap Yard Road 5136, Sheikh Salman Highway , Kingdom of Bahrain (Address 2, City, Country)</p>
		<p>has been assessed and certified as meeting with the requirements of</p>
		<h2>ISO 9001:2015 (Standard) </h2>
	</div>
	<div class="scopeArea">
		<h5>Under the Scope:</h5>
		<p>“Design, Manufacturing, Maintenance & Repair of Pressure Vessels, Heat Exchangers, Columns, Static Equipment, Storage Tanks, Piping, Pipe Line, Christmas Trees, Well Heads and Drill Through Equipment, Pup Joints, Drill Stem Subs, Threading of Casing, Tubing and Rotary Shoulder Connections for the Oil and Gas Industry. Repair of Valves, Pumps, Compressor and Rotating Equipment, also Prviding the Shutdown, NDT, and Associated Civil, Electrical, Instrumentation, Installation Works, Supply of Skilled Manpower and Equipment.” (Scope to be Certified)</p>
		<table style="text-align: left; font-size: 12px;">
			<tr>
				<th>Certificate Number</th>
				<td>: BH190001 (Job Number)</td>
			</tr>
			<tr>
				<th>Issue date</th>
				<td>: 26th March 2019.(Original issue date / stage II approval date)</td>
			</tr>
			<tr>
				<th>Valid till</th>
				<td>: 25th March 2020. (New Expiry Date)</td>
			</tr>
		</table>
		<img src="{{ public_path() . $sign }}" alt="" width="30px">
		<p>Authorized Signature</p>
		<p>Resource Inspections Canada Incorporated Co. <br> <a href="#">www.ricionline.com</a></p>
		<small>*This certificate remains valid subject to successful surveillance audits and maintenance of management system as per certification rules and procedures of Resource Inspections Canada Incorporated Co.</small> 
		<br>
		<small>*Further clarification regarding scope of this certificate and applicability of ISO 9001:2015 requirements may be obtained from the organization.</small>
		<br>
		<small>*This certificate remains property of Resource Inspections Canada Incorporated Co., to whom it must be returned upon request.</small>
		<br>
		<small>* Unique Certificate Identifier Number: MSCB-104-25751 (UCI #)</small>
		<br>
		<small>Page 1 of 1</small>
	</div>
</div>
<div class="footer">
	<p>Office 44, Building 2126, Road 1529, Block 115, Hidd, Kingdom of Bahrain </p>
	<p>Email: <a href="#">scheme@ricionline.com</a> </p>
</div>
</body>
</html>
