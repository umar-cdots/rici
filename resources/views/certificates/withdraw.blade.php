<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Withdrawal Letter</title>
    <style type="text/css">
        body {
            font-family: Calibri;
        }

        .header {
            width: 80%;
            margin: 0 auto;
        }

        .content {
            width: 80%;
            margin: 0 auto;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        p {
            font-size: 17px;
            margin-bottom: 5px;
        }

        ul {
            padding-left: 15px;
        }

        ul li {
            font-size: 15px;
            line-height: 22px;
        }

        .footer {
            border-top: #ccc solid 1px;
            margin-top: 110px;
            padding-top: 20px;
        }

        @page {
            size: 35cm ;
        }
    </style>
</head>

<body>
@php
    $logo = '/img/logo.png';

@endphp
<div class="header">
    <table width="100%">
        <tr>
            <td width="20%">
                <img src="{{ public_path() . $logo }}" alt="">
            </td>
            <td style="text-align: center; font-size: 30px; font-weight: 700;">{{$riciName}}
            </td>
        </tr>
    </table>
</div>

<div class="content">
    <div class="text-right">
        <p>{{$companyStandard->certificates ? date("d F Y",strtotime($companyStandard->certificateWithdrawn->certificate_date)) : ''}}</p>
    </div>
    <div class="text-left">
        <p></p>
        <p>{{$companyStandard->company->primaryContact->title}}
            . {{ucfirst($companyStandard->company->primaryContact->name)}} </p>
        <p>{{$companyStandard->company->primaryContact->position}}</p>
        <p>{{$companyStandard->company->name}}</p>
        <p>{{$companyStandard->company->address}}</p>
        <p>{{$companyStandard->company->city->name}},&nbsp;{{$companyStandard->company->country->name}}.</p>
    </div>
    <div class="text-center">
        <p><strong>WITHDRAWAL OF CERTIFICATION.</strong></p>
    </div>
    <div class="text-left">
        <p>Dear {{$companyStandard->company->primaryContact->title}}
            . {{ucfirst($companyStandard->company->primaryContact->name)}}, </p>
        <p>I hope this letter finds you in best of health and business. This is with reference to our earlier letter
            dated {{$companyStandard->certificateSuspension ? date("d/m/Y",strtotime($companyStandard->certificateSuspension->certificate_date)) : ''}}
            , as you are aware your organization’s certification with {{$riciName}} is
            suspended due to non-execution of the overdue {{str_replace("_"," ",ucfirst($ajStandardStage->audit_type))}}
            for:</p>
        <ul>
            @php
                $ims_heading=[];
              $company = \App\Company::where('id', $companyStandard->company_id)->first();
              $companyStandard = \App\CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
              $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
              if($companyStandard->is_ims == false){
                     array_push($ims_heading,$companyStandard->standard->name);
              }else{
                    array_push($ims_heading,$companyStandard->standard->name);
                 if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                  foreach ($imsCompanyStandards as $imsCompanyStandard) {
                      if ($companyStandard->id == $imsCompanyStandard->id) {
                          continue;
                      } else {
                         array_push($ims_heading,$imsCompanyStandard->standard->name);

                      }
                  }
              } else {

              }
              }
            @endphp
            @foreach($ims_heading as $std)
                <li>{{$std}}</li>
            @endforeach
        </ul>
        <p>As we have not received any positive response with confirmed audit date within the communicated time frame,
            we would like to inform you that your organization’s certification is being withdrawn. The online directory
            of RICI available on <a href="#">www.ricionline</a>.com has been updated for this change of certification
            status. </p>
        <p>You are required to send back the original certificate to our local office as the certificate issued is
            property of RICI and refrain from making any future claims regarding certification with RICI. Please remover
            all references to certification or use of the RICI or Accreditation logos from your websites, stationery or
            in any other advertising claim.</p>
        <p>Kind regards,</p>
        <img style="margin-top: 5px"
             src="{{ public_path() . "/uploads/scheme_managers/". $companyStandard->standard->scheme_manager[0]->signature_image}}"
             alt=""
             width="80px">
        <p>{{$companyStandard->standard->scheme_manager[0]->full_name}}</p>
        <p>Scheme Manager </p>
        <p>{{$riciName}}</p>
    </div>
    <div class="footer text-center">
        <p>{{$riciAddress}}</p>
        <p>Email: <a href="mailto:scheme@ricionline.com">scheme@ricionline.com</a></p>
    </div>
</div>
</body>
</html>
