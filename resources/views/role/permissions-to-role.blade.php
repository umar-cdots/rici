@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-4 floting">
                            <h1>Roles with Permissions</h1>
                        </div>
                        <div class="col-sm-6 floting">
                            <a href="{{ route('permissions.create') }}" class="add_comp">
                                <label class="text-capitalize"><i class="fa fa-plus-circle text-lg" aria-hidden="true"></i> Add Permissions to roles</label>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('permission-to-role') }}
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Roles with permissions</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tabularData" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Sr. #</th>
                                    <th>Role</th>
                                    <th>Permissions</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            @if(!empty($role->permissions))
                                                @foreach($role->permissions as $permission)
                                                    @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                        ,
                                                    @endif
                                                    {{ $permission->name }}
                                                @endforeach
                                            @else
                                                No permission granted
                                            @endif
                                        </td>
                                        <td>
                                            <ul class="data_list">
                                                <li>
                                                    <a href="#" title="View"><i class="fa fa-eye"></i>
                                                    </a>
                                                </li>
                                                 <li>
                                                    <a href="{{ route('permissions.role.edit', $role->id) }}" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </li>
                                                <li>
                                                    <form class="inline-block remove" action="{{ route('permissions.destroy', $role->id) }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="rm-inline-button">
                                                            <i class="fa fa-close"></i>
                                                        </button>
                                                    </form>
                                                    {{-- <a href="" title="Remove"><i class="fa fa-close"></i></a> --}}
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection