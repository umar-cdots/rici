@extends('layouts.master')
@section('title', "Edit Permissions of role")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Permissions and Roles</h1>
                        <h6 class="text-center dashboard_heading2">Edit Permissions to role{{--  - if you want to learn how to follow these steps - <a href="#">Click Here</a> --}}</h6>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->

            <div class="card card-primary mrgn comp_det_view ">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">Permissions to role </h3>
                </div>

                <div class="card-body card_cutom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="aud_info">
                            <form action="{{ route('permissions.role.store') }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Select Role:</label>
                                            <select name="role" class="form-control" disabled="disabled">
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Select Permissions:</label>
                                            <select name="permissions[]" class="form-control" multiple>
                                                @if(!empty($permissions))
                                                    @foreach($permissions as $permission)
                                                        <option value="{{ $permission->id }}"
                                                            @foreach ($role->permissions as $r_permission)
                                                                @if($r_permission->id == $permission->id)
                                                                    selected
                                                                @endif
                                                            @endforeach
                                                        >{{ $permission->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <button class="btn btn-secondary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection