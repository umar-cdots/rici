@extends('layouts.master')
@section('title', "Auditors List")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-4 floting">
                            <h1>Auditor</h1>
                        </div>
                        <div class="col-sm-6 floting">

                            @can('create_auditors')
                                <a href="{{route('auditors.create')}}" class="add_comp"><label
                                            class="text-capitalize"><i class="fa fa-plus-circle text-lg"
                                                                       aria-hidden="true"></i> Add New
                                        Auditor</label></a>
                            @endcan

                        </div>
                    </div>

                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('auditors') }}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of Auditors</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="8%;" data-nosort='Y'>Sr. #</th>
                                    <th>Auditor Name</th>
                                    <th>Standard(s)</th>
                                    <th>Country</th>
                                    <th>Profile Status</th>
                                    <th>Completion Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($auditors as $key => $auditor)
                                    <tr>

                                        <td data-nosort='Y'>{{$key + 1}}</td>
                                        <td>{{$auditor->fullName()}}</td>
                                        <td>

                                            
                                            @foreach($auditorStandards[$key] as $auditorStandard)
                                                {!! $auditorStandard->standard->name !!}
                                                @foreach(\App\AuditorStandardGrade::where('auditor_standard_id',$auditorStandard->id)->where('status','current')->get() as $auditorStandardGrade)
                                                    ({!! $auditorStandardGrade->grade->short_name !!} ,
                                                    <span> {{ucfirst($auditorStandard->status)}}</span>)
                                                @endforeach


                                                <br>


                                            @endforeach
                                        </td>
                                        <td>{{ $auditor->country->name }}</td>
                                        <td>{{ ucfirst($auditor->auditor_status) }}</td>
                                        <td>{{ $auditor->form_progress }}%</td>
                                        <td>
                                            <ul class="data_list">

                                                @can('show_auditors')
                                                    <li>

                                                        <a href="{{route('auditors.show', $auditor->id)}}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>

                                                    </li>
                                                @endcan
                                                @can( 'edit_auditors')
                                                    <li>

                                                        <a href="{{route('auditors.edit', $auditor->id)}}"><i
                                                                    class="fa fa-pencil"></i></a>

                                                    </li>
                                                @endcan
                                                @can('delete_auditors')
                                                    <li>

                                                        <a href="{{route('auditors.delete', $auditor->id)}}"
                                                           onclick="return confirm('Are you sure you want to delete Auditor?')">
                                                            <i class="fa fa-close"></i>
                                                        </a>

                                                    </li>
                                                @endcan
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {


            $('#example1').dataTable();


            // $('#FormalEducationTableCodes').dataTable();
        });
    </script>
@endpush