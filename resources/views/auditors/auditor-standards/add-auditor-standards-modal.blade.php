<style>
    .disabledDiv {
        pointer-events: none;
        opacity: 1.4;
    }
</style>
<div class="modal fade" id="addAuditorStandardsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 60%">
        <form action="" method="post" id="addAuditorStandardsForm">
            <input type="hidden" name="auditor_id" class="auditor-id" value="{{$auditorId}}">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Standards</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    @php
                        $standards = [];
                    @endphp
                    @foreach($standards_families as $standards_family)
                        <div class="card-header">
                            <h3 class="card-title">{{ $standards_family->name }}</h3>
                        </div>
                        @forelse($standards_family->standards->chunk(6) as $chunk)
                            <div class="col-md-12 mrgn text-center">
                                @foreach($chunk as $standard)


                                        @php
                                            $standards[] 	= $standard;
                                            $isChecked = false;
                                            $deleteAllow = false;

                                            foreach ($auditorStandards as $auditorStandard){

                                                 if ($standard->id == $auditorStandard->standard_id){
                                                    $isChecked = true;
                                                 }
                                            }

                                            if(auth()->user()->user_type == 'scheme_manager'){
                                                $deleteAllow = true;
                                            }


                                        @endphp
                                        <div class="col-md-2 floting standard_selection" data-standard-number="{{ $standard->number }}">

                                            <input

                                                    type="checkbox" name="standard_ids[]" value="{{ $standard->id }}"
                                                   class="standard_check icheck-blue @if($isChecked) @if(!$deleteAllow) disabledDiv @endif  @endif" id="auditorStandard{{$standard->id}}" {{$isChecked ==
                                                       'true' ? 'checked': ''}} {{ ($standard->status === 0) ? 'disabled' : '' }}>
                                            &nbsp;<label title="{{ $standard->name }}"> {{ $standard->name }}

                                            </label>
                                        </div>

                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        @empty
                            <div class="col-md-12">
                                <p>No Standard Got Added Yet.</p>
                            </div>
                        @endforelse
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function readURLMany(input,id) {
        var filename = input.files[0].name;
        var html='';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">'+ filename +'</a></span></h6>';
        }
        $('#upld'+id).html(html);
    }

    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });

    $('input').on('ifUnchecked', function (event) {
        confirm('Are you sure you want to remove standard?');
    });

    $('input').on('ifChecked', function (event) {
        $(this).attr('disabled', true);
    });
</script>


<script>
    $('#addAuditorStandardsForm').submit(function (e) {
        e.preventDefault();
        var form = $('#addAuditorStandardsForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.standards.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    getAuditorStandardsTabs(auditorId);
                    $("#addAuditorStandardsModal").modal('hide');
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
</script>


