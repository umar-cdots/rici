<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-training-course-form{{$auditor_standard_id}}">
        <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}" class="auditor_standard_id">
        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th width="10%">Year</th>
                <th width="25%">Certificate / Award</th>
                <th>Certificate #</th>
                <th>Awarded body</th>
                <th width="20%">Upload Certificate</th>
                <th class="action-column">Action</th>
            </tr>
            </thead>
            <tbody id="BodyStandardTrainingCourses_1">


               @foreach($auditorStandardsTrainingCourses as $auditorStandardsTrainingCourse)
                   <tr>
                       <td>{{$auditorStandardsTrainingCourse->training_year}}</td>
                       <td>{{$auditorStandardsTrainingCourse->certificate_name}}</td>
                       <td>{{$auditorStandardsTrainingCourse->certificate_number}}</td>
                       <td>{{$auditorStandardsTrainingCourse->awarded_body}}</td>
                       <td><a href="{{ asset('/uploads/training_document/'.$auditorStandardsTrainingCourse->training_document)}}" target="_blank"
                              title="{{$auditorStandardsTrainingCourse->training_document }}">
                               {{ $auditorStandardsTrainingCourse->training_document }}</a></td>
{{--                       <td><a href="{{$auditorStandardsTrainingCourse->getFirstMedia()->getFullUrl()}}" target="_blank" title="{{$auditorStandardsTrainingCourse->getFirstMedia()->name}}">--}}
{{--                               {{str_limit($auditorStandardsTrainingCourse->getFirstMedia()->name,'15','...')}}</a></td>--}}
                       <td align="center" class="action-column-data">
                           <ul class="data_list">
                               <li>
                                   <a href="javascript:void(0)" onclick="getEditTrainingCourseModal({{$auditorStandardsTrainingCourse->id}})">
                                       <i class="fa fa-pencil"></i>
                                   </a>
                               </li>
                               <li>
                                   <a href="javascript:void(0)" onclick="deleteAuditorStandardTrainingCourse({{$auditorStandardsTrainingCourse->id}});">
                                       <i class="fa fa-close"></i>
                                   </a>
                               </li>
                           </ul>

                       </td>
                   </tr>
               @endforeach


            </tbody>
            <tfoot>
            <tr>
                <td>
                    <div class="col-md-12">
                        <input type="text" class="year_sm form-control yearfield"
                               name="training_year"
                               placeholder="Year">
                    </div>
                </td>
                <td>
                    <div class="col-md-12">
                        <input type="text" name="certificate_name"
                               class="form-control"
                               placeholder="Certificate, Award">
                    </div>
                </td>
                <td>
                    <div class="col-md-12">
                        <input type="text" name="certificate_number"
                               class="form-control"
                               placeholder="Cert. #">
                    </div>
                </td>
                <td>
                    <div class="col-md-12">
                        <input type="text" name="awarded_body" class="form-control"
                              placeholder="Awarded body">
                    </div>
                </td>
                <td>
                    <div class="col-md-6 floting">
                        <label class="upload_documt">Add Document (10MB)
                            <input type="file" name="document_file" onchange="readURLMany(this,6)">
                            <img src="{{asset('img/new.png')}}" alt="img"></label>
                        <div id="upld6"></div>
                    </div>
                </td>
                <td>
                    <div class="col-md-5 floting">
                        <input type="button" value="Add New"
                               class="add-standard-training-course-btn{{$auditor_standard_id}} btn btn-primary"
                               onclick="addStandardTrainingCourse({{$auditor_standard_id}})">
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>


<script>

    function readURLMany(input,id) {
        var filename = input.files[0].name;
        var html='';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">'+ filename +'</a></span></h6>';
        }
        $('#upld'+id).html(html);
    }
    var d = new Date();
    var n = d.getFullYear();
    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
        endDate:n.toString()
    });

    function addStandardTrainingCourse(auditorStandardId)
    {

        var form = $("#auditor-standard-training-course-form"+auditorStandardId)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('auditors.standards.training.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    auditorStandardTrainingCoursesView(response.data.auditor_standard_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function deleteAuditorStandardTrainingCourse(audStdTrainingCourseId) {
        var url = "{{ route('auditors.standards.training.delete', ['TRAINING_ID' => "TRAINING_ID"]) }}".replace("TRAINING_ID", audStdTrainingCourseId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                    auditorStandardTrainingCoursesView(response.data.auditor_standard_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditTrainingCourseModal(audStdTrainingCourseId) {
        var url = "{{ route('auditors.standards.training.edit.modal', ['TRAINING_ID' => "TRAINING_ID"]) }}".replace("TRAINING_ID", audStdTrainingCourseId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-auditor-standard-training-course-container").html(response);
                $("#editTrainingCourseModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }



</script>
