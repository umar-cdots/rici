<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-energy-codes{{$auditor_standard_id}}">
        <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}" class="auditor_standard_id">
        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Description</th>
                <th>Code</th>
                <th>Accreditation</th>
                <th width="10%" class="action-column">Action</th>
            </tr>
            </thead>
            <tbody>


            @foreach($auditorStandardEnergyCodes as $key => $auditorStandardEnergyCode)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$auditorStandardEnergyCode->energycode->name}}</td>
                    <td>{{$auditorStandardEnergyCode->energycode->code}}</td>
                    <td>{!! Helper::getAccreditations($auditorStandardEnergyCode->accreditation_id) !!}</td>
                    <td align="center" class="action-column-data">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="getEditEnergyCodesModal({{$auditorStandardEnergyCode->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="deleteAuditorStandardEnergyCode({{$auditorStandardEnergyCode->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>

        </table>
        <div class="col-md-12">
            <button type="button" class="btn btn-primary edit_btn pull-right"
                    data-toggle="modal" data-target="#addStandardEnergyCodesModal{{$auditor_standard_id}}"  onclick="getEnergyAccreditations('{{$auditor_standard_id}}')">Add Energy
                Code
            </button>
        </div>
    </form>
</div>


<div id="addStandardEnergyCodesModal{{$auditor_standard_id}}" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="addStandardEnergyCodesForm{{$auditor_standard_id}}">
                <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}">
                <div class="modal-header">
                    <h4>Energy Codes</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Accreditations</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control energy_accreditations"
                                            name="energy_accreditation_id">
                                        <option value="">Select Accreditation</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Energy Code</h3>
                                </div>

                                <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <select class="form-control energy_codes" name="energy_code" id="energy_codes">
                                            <option value="">Select Energy Code</option>

                                        </select>
                                    </div>

                            </div>

                        </div>
                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary"
                                        onclick="addStandardEnergyCodeSave({{$auditor_standard_id}})
                                                ">Submit
                                </button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel" data-dismiss="modal">Cancel</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>


    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });



    function getEnergyAccreditations(auditor_standard_id) {




        var form = $(this).closest('form');
        var node = $(this);
        var node_to_modify = '.energy_accreditations';
        var request = "auditor_standard_id=" + auditor_standard_id;

        if (auditor_standard_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.accreditationByAuditorStandardId') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.accreditations, function (i, obj) {

                            html += '<option  value="' + obj.id + '">' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);

                        $(node_to_modify).prepend("<option value='' selected>Select Accreditation</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Accreditation</option>");
        }

    }

    $('.energy_accreditations').change(function () {
        var form = $(this).closest('form');
        var node = $(this);
        var node_to_modify = '.energy_codes';
        var accreditation_id = $(this).val();
        var request = "accreditation_id=" + accreditation_id;

        if (accreditation_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.energyByAccreditation') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.energy_codes, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        $(node_to_modify).prepend("<option value='' selected>Select Energy Code</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Energy Code</option>");
        }

    });
    function addStandardEnergyCodeSave(auditorStandardId) {

        var form = $("#addStandardEnergyCodesForm" + auditorStandardId)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('auditors.standards.energy.code.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                if (response.status === 'exist') {
                    toastr['error']("Energy Code Already exist");
                } else {
                    ajaxResponseHandler(response, form);

                    if (response.status == "success" || response.status == '200') {
                        $("#addStandardEnergyCodesModal" + response.data.auditor_standard_id).modal('hide');

                        setTimeout(function () {
                            auditorStandardEnergyCodesView(response.data.auditor_standard_id);
                        }, 1000);
                    }
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }


    function deleteAuditorStandardEnergyCode(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('auditors.standards.energy.code.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        auditorStandardEnergyCodesView(response.data.auditor_standard_id);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditEnergyCodesModal(modelId) {
        var url = "{{ route('auditors.standards.energy.code.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-auditor-standard-container").html(response);
                $("#editStandardEnergyCodesModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    enable_cb();
    disable_cb();


    $('.Non-Accredited').click(enable_cb);
    $('.acceditations').click(disable_cb);

    function enable_cb() {

        if ($('.Non-Accredited').is(':checked')) {
            $('.acceditations').attr('disabled', true).removeAttr('checked');
        } else {
            $('.acceditations').removeAttr('disabled');
        }
    }

    function disable_cb() {
        if ($('.acceditations').is(':checked')) {
            $('.Non-Accredited').attr('disabled', true).removeAttr('checked');
        } else {
            $('.Non-Accredited').removeAttr('disabled');
        }
    }


</script>
