
<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-witness-eval-form{{$auditor_standard_id}}">
        <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}" class="auditor_standard_id">
        <input type="hidden" name="auditor_id" value="{{$auditor_id}}" class="auditor_id">
        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th width="10%"> S.No</th>
                <th>Due Date</th>
                <th>Actual Dates</th>
                <th>Evaluation Reports</th>
                <th>Satisfactory</th>
                <th>Remarks</th>
                <th class="action-column">Action</th>
            </tr>
            </thead>
            <tbody id="BodyStandardTrainingCourses_1">


            @foreach($auditorStandardWitnessEvals as $key => $auditorStandardWitnessEval)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>
                        @if(!is_null($auditorStandardWitnessEval->due_date))
                            @php
                                $due_date = new DateTime($auditorStandardWitnessEval->due_date);
                            @endphp
                            {{ $due_date->format('d/m/Y') }}
                        @else

                        @endif
                    </td>

                    <td>
                        @if(!is_null($auditorStandardWitnessEval->actual_date))
                            @php
                                $actual_date = new DateTime($auditorStandardWitnessEval->actual_date);
                            @endphp
                            {{ $actual_date->format('d/m/Y') }}
                        @else

                        @endif
                    </td>

                    <td><a href="{{ asset('/uploads/witness_document/'.$auditorStandardWitnessEval->witness_document)}}"
                           target="_blank"
                           title="{{$auditorStandardWitnessEval->witness_document }}">
                            {{ $auditorStandardWitnessEval->witness_document }}</a></td>
                    {{--                       <td><a href="{{$auditorStandardWitnessEval->getFirstMedia()->getFullUrl()}}" target="_blank" title="{{$auditorStandardWitnessEval->getFirstMedia()->name}}">--}}
                    {{--                               {{str_limit($auditorStandardWitnessEval->getFirstMedia()->name,'15','...')}}</a></td>--}}
                    <td>
                        {{ucfirst($auditorStandardWitnessEval->satisfactory)}}
                    </td>
                    <td>
                        {{ucfirst($auditorStandardWitnessEval->remarks)}}
                    </td>
                    <td align="center" class="action-column-data">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="getEditAuditorStandardWitnessEvalModal({{$auditorStandardWitnessEval->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="deleteAuditorStandardWitnessEval({{$auditorStandardWitnessEval->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td>
                    <div class="col-md-12">
                        <input type="text" name="due_date" class="form-control due_date" id="due_date"
                               {{--                               data-provide="datepicker"--}}
                               placeholder="Due Date">
                    </div>
                </td>
                <td class="{{ ((auth()->user()->getRoleNames()[0] == 'super admin') || (auth()->user()->getRoleNames()[0] == 'scheme manager')) ? '' : 'disabledDiv' }}">
                    <div class="col-md-12">
                        <input type="text" name="actual_date" class="form-control actual_date" id="actual_date"
                               {{--                               data-provide="datepicker"--}}
                               placeholder="Actual Date">
                    </div>
                </td>

                <td class="{{ ((auth()->user()->getRoleNames()[0] == 'super admin') || (auth()->user()->getRoleNames()[0] == 'scheme manager')) ? '' : 'disabledDiv' }}">
                    <div class="col-md-12 floting document_file disabledDiv">
                        <label class="upload_documt">Add Document (10MB)
                            <input type="file" name="document_file" onchange="readURLMany(this,3)">
                            <img src="{{asset('img/new.png')}}" alt="img"></label>
                        <div id="upld3"></div>
                    </div>
                </td>
                <td class="{{ ((auth()->user()->getRoleNames()[0] == 'super admin') || (auth()->user()->getRoleNames()[0] == 'scheme manager')) ? '' : 'disabledDiv' }}">
                    <select class="form-control satisfactory disabledDiv" name="satisfactory">
                        @foreach($satisfactory as $item)
                            <option value="{{$item}}" {{ ($item == 'not applicable') ? 'selected' : '' }}>{{ucfirst($item)}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <div class="col-md-12 floting">
                        <textarea name="remarks"
                                  class="form-control"></textarea>
                    </div>
                </td>
                <td>
                    <div class="col-md-5 floting">
                        <input type="button" value="Add New"
                               class="add-standard-training-course-btn{{$auditor_standard_id}} btn btn-primary"
                               onclick="addStandardWitnessEval({{$auditor_standard_id}})">
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>

<style>
    .disabledDiv {
        pointer-events: none;
        opacity: 1.4;
    }
    /*.bootstrap-datetimepicker-widget {*/

    /*    width: 100% !important;*/
    /*    height: 400px !important;*/
    /*}*/
</style>
<script>
    $(function() {

        $('.actual_date').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('.due_date').datepicker({
            format: 'dd-mm-yyyy'

        });
    });
    $('.actual_date').change(function (e) {
        e.preventDefault();
        if ($(this).val() === '' || $(this).val() === undefined || $(this).val() === null) {
            $('.document_file').addClass('disabledDiv');
            $('.satisfactory').addClass('disabledDiv');
        } else {
            $('.document_file').removeClass('disabledDiv');
            $('.satisfactory').removeClass('disabledDiv');
        }
    });

    function readURLMany(input, id) {
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#upld' + id).html(html);
    }

    function addStandardWitnessEval(auditorStandardId) {

        var form = $("#auditor-standard-witness-eval-form" + auditorStandardId)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('auditors.standards.witness.eval.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    auditorStandardWitnessEvalView(response.data.auditor_standard_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function deleteAuditorStandardWitnessEval(audStdWitnessId) {
        var url = "{{ route('auditors.standards.witness.eval.delete', ['WITNESS_ID' => "WITNESS_ID"]) }}".replace("WITNESS_ID", audStdWitnessId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                    auditorStandardWitnessEvalView(response.data.auditor_standard_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditAuditorStandardWitnessEvalModal(audStdWitnessId) {
        var url = "{{ route('auditors.standards.witness.eval.edit.modal', ['WITNESS_ID' => "WITNESS_ID"]) }}".replace("WITNESS_ID", audStdWitnessId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-auditor-standard-witness-eval-container").html(response);
                $("#editStandardWitnessEvalModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }


</script>
