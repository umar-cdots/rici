<div class="modal fade codesModal" id="editTrainingCourseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editTrainingCourseForm" enctype="multipart/form-data">
            <input type="hidden" name="training_course_id" value="{{$auditorStandardTraining->id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Auditor Standard Training Course</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Training Year</label>
                            <input type="text" name="training_year" id="training_year" class="year_sm form-control yearfield"
                                   placeholder="Training Year" value="{{$auditorStandardTraining->training_year}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Certificate Name</label>
                            <input type="text" name="certificate_name" id="certificate_name" placeholder="Institution"
                                   class="form-control" value="{{$auditorStandardTraining->certificate_name}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Certificate Number</label>
                            <input type="text" name="certificate_number" id="certificate_number" placeholder="Certificate #"
                                   class="form-control" value="{{$auditorStandardTraining->certificate_number}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Awarded Body</label>
                            <input type="text" name="awarded_body" id="awarded_body" placeholder="Awarded Body"
                                   class="form-control" value="{{$auditorStandardTraining->awarded_body}}">
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Document Existing File</label>
                            <br>
{{--                            <img src="{{$auditorStandardTraining->getFirstMedia()->getFullUrl()}}" alt="" width="50" height="50">--}}
                            <img src="{{asset('/uploads/training_document/'.$auditorStandardTraining->training_document)}}" alt="" width="50" height="50">
                        </div>
                        <label class="upload_documt"> Add Document  (10MB)
                            <input type="file" name="document_file" id="document_file" onchange="readURLMany(this,12)">
                            <img src="{{asset('img/new.png')}}" alt="img"></label>
                        <div id="upld12"></div>
                        <br>
                        <small>
                            Upload certificate to override the existing file
                        </small>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
                    <button type="button" class="btn btn-primary updateDataBtn">Update Data</button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    function readURLMany(input,id) {
        var filename = input.files[0].name;
        var html='';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">'+ filename +'</a></span></h6>';
        }
        $('#upld'+id).html(html);
    }

    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editTrainingCourseForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.standards.training.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    auditorStandardTrainingCoursesView(response.data.auditor_standard_id);
                    $("#editTrainingCourseModal").modal('hide');
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
</script>