<div id="editStandardEnergyCodesModal" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="editStandardEnergyCodesForm">
                <input type="hidden" name="auditor_standard_energy_code_id" value="{{$auditorStandardEnergyCode->id}}">
                <div class="modal-header">
                    <h4>Edit Energy Code</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Accreditations</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control energy_accreditations" name="energy_accreditation_id">
                                        <option value="">Select Accreditation</option>
                                        @foreach($allAccreditations as $accreditations)
                                            <option value="{{$accreditations->id}}" {{$accreditations->id ==$auditorStandardEnergyCode->accreditation_id ? 'selected': ''}}>{{ $accreditations->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Energy Code</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control" name="energy_code">
                                        <option value="">Select Energy Code</option>
                                        @foreach($energyCodes as $energyCode)
                                            <option value="{{$energyCode->id}}" {{$energyCode->id ==
                                            $auditorStandardEnergyCode->energy_code ? 'selected': ''}}>
                                                {{$energyCode->code}} | {{$energyCode->name}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary updateDataBtn">Submit</button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel" data-dismiss="modal">Cancel</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });



    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardEnergyCodesForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.standards.energy.code.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    auditorStandardEnergyCodesView(response.data.auditor_standard_id);
                    $("#editStandardEnergyCodesModal").modal('hide');
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    enable_cb();
    disable_cb();
    $('.Non-Accredited').click(enable_cb);
    $('.acceditations').click(disable_cb);

    function enable_cb() {

        if ($('.Non-Accredited').is(':checked')) {
            $('.acceditations').attr('disabled', true).removeAttr('checked');
        } else {
            $('.acceditations').removeAttr('disabled');
        }
    }

    function disable_cb() {
        if ($('.acceditations').is(':checked')) {
            $('.Non-Accredited').attr('disabled', true).removeAttr('checked');
        } else {
            $('.Non-Accredited').removeAttr('disabled');
        }
    }

</script>