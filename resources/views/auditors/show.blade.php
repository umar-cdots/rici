@extends('layouts.master')
@section('title')
    {{$auditor->fullName()}} Auditor

@endsection
@push('styles')
    <style>
        tfoot {
            display: none;
        }

        .edit_btn {
            display: none;
        }

        .action-column-data {
            display: none;
        }

        .action-column {
            display: none;
        }
    </style>
@endpush

@section('content')
    <input type="hidden" class="auditor-id" value="{{$auditor->id}}">

    <div class="content-wrapper custom_cont_wrapper">

        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Auditor <span>Profile</span></h1>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>

                <div class="row text-center mrgn">
                    <div class="col-md-4 text-left"><h3>{{$auditor->fullname()}}</h3></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-1">

                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2"></div>
                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')

                        <div class="col-md-2 text-right">

                            <input type="checkbox"
                                   {{$auditor->auditor_status == 'active' ? 'checked': ''}} data-toggle="toggle"
                                   data-on="Active"
                                   data-off="Inactive"
                                   data-onstyle="success"
                                   data-offstyle="danger" onchange="updateAuditorStatus({{$auditor->id}})">
                        </div>

                    @endif
                    <div class="clearfix"></div>
                </div>

                <div class="card card-primary mrgn comp_det_view ">

                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3">Auditor Profile </h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item"><a class="nav-link active show" href="#pro_info" data-toggle="tab">Basic
                                    Info</a></li>
                            <li class="nav-item"><a class="nav-link" href="#edu" data-toggle="tab">Education</a></li>
                            <li class="nav-item"><a class="nav-link" href="#othr" data-toggle="tab">Others</a></li>
                            <li class="nav-item"><a class="nav-link" href="#standardsTab"
                                                    data-toggle="tab">Standards</a></li>
                        </ul>
                    </div>


                    <div class="card-body card_cutom">
                        <div class="tab-content">
                            <div class="tab-pane active" id="pro_info">
                                <div class="row">
                                    <div class="col-md-2">

                                        <div class="form-group">
                                            <img id="upld" src="{{ asset('/uploads/auditors/'.$auditor->profile_pic)}}"
                                                 alt="Upload Image"
                                                 height="100%">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <p class="results_answer">{{$auditor->first_name}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <p class="results_answer">{{$auditor->last_name}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <p class="results_answer">{{$auditor->email}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <p class="results_answer">{{ $auditor->country->phone_code . ltrim($auditor->phone, '0') }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Landline Number</label>
                                                    <p class="results_answer">{{$auditor->landline}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 floting">
                                                <label>Postal Address</label>
                                                <p class="results_answer">{{$auditor->postal_address}}</p>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Working Region</label>
                                                    <p class="results_answer">{{$auditor->region->title}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Resident Country</label>
                                                    <p class="results_answer">{{$auditor->country->name}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Resident City</label>
                                                    <p class="results_answer">{{$auditor->city->name}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 floting">
                                                <label>Date of Birth</label>
                                                <div class="form-group">
                                                    <p class="results_answer">{{ date('d-m-Y', strtotime($auditor->dob))}}</p>
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Language:</label>
                                                    <p class="results_answer">
                                                        @foreach($auditor_language as $language)
                                                            @foreach($language as $data)
                                                                {{$data->name}}<br>
                                                            @endforeach
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Overall Work Exp. (Years):</label>
                                                    <p class="results_answer">{{$auditor->working_experience}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Main Education:</label>
                                                    <p class="results_answer">{{$auditor->main_education}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Nationality:</label>
                                                    <p class="results_answer">{{$auditor->nationality->name}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <label>Job status</label>
                                                <div class="form-group">
                                                    <p class="results_answer">{{$auditor->jobStatusString()}}</p>

                                                </div>
                                            </div>

                                        </div>


                                        <div class="clearfix"></div>
                                    </div>


                                    <div class="col-md-12">
                                        <label>Profile Remarks</label>
                                        <p class="results_answer">{{$auditor->profile_remarks}}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save cont">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="edu">
                                <div class="row">
                                    <h4>Formal Education</h4>
                                    <div class="tables">
                                        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
                                               class="table table-striped table-bordered table-bordered2">
                                            <thead>
                                            <tr>
                                                <th width="8%">Year</th>
                                                <th>Institution</th>
                                                <th>Degree / Diploma</th>
                                                <th>Major Subject(s)</th>
                                                <th>Certificate</th>
                                            </tr>
                                            </thead>
                                            <tbody id="BodyFormalEducationTable">


                                            @foreach($auditor->education as $auditorEducation)
                                                <tr>
                                                    <td>{{$auditorEducation->year_passing}}</td>
                                                    <td>{{$auditorEducation->institution}}</td>
                                                    <td>{{$auditorEducation->degree_diploma}}</td>
                                                    <td>{{$auditorEducation->major_subjects}}</td>
                                                    <td>
                                                        <a href="{{ asset('/uploads/auditor_education_certificate/'.$auditorEducation->education_document)}}"
                                                           target="_blank"
                                                           title="{{$auditorEducation->education_document }}">
                                                            {{ $auditorEducation->education_document }}</a></td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <h4>Other Documents</h4>

                                    <div class="tables">
                                        <table id="auditorDocumentsTable" cellspacing="20" cellpadding="0" border="0"
                                               class="table table-striped table-bordered table-bordered2">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Name</th>
                                                <th>Documents</th>
                                            </tr>
                                            </thead>
                                            <tbody id="BodyFormalEducationTable">


                                            @foreach($auditor->documents as $auditorDocument)
                                                <tr>
                                                    <td colspan="2">{{$auditorDocument->document_name}}</td>
                                                    <td>
                                                        <a href="{{ asset('/uploads/auditor_document/'.$auditorDocument->auditor_document)}}"
                                                           target="_blank"
                                                           title="{{$auditorDocument->auditor_document }}">
                                                            {{ $auditorDocument->auditor_document }}</a></td>
                                                </tr>

                                            @endforeach


                                            </tbody>

                                        </table>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save cont">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="othr">
                                <div class="row">
                                    <h4>Confidentality Agreement / Joining Date</h4>
                                    <div class="tables">
                                        <table id="ConfidentalityAgreementTable" cellspacing="20" cellpadding="0"
                                               border="0"
                                               class="table table-striped table-bordered table-bordered2 table4">
                                            <thead>
                                            <tr>
                                                <th width="8%">S.No</th>
                                                <th width="25%">Date</th>
                                                <th width="40%">Agreement</th>
                                            </tr>
                                            </thead>
                                            <tbody id="BodyFormalEducationTable">


                                            @foreach($auditor->confidentialityAgreements as $key =>  $auditorAgreement)
                                                <tr>
                                                    <td>{{$key + 1}}</td>
                                                    <td>
{{--                                                        {{ date('d-m-Y', strtotime($auditorAgreement->date_signed))}} --}}
                                                        {{ $auditorAgreement->date_signed }}</td>
                                                    <td>

                                                
                                                        <a href="{{ asset('/uploads/auditor_confidentially_agreement/'.$auditorAgreement->agreement_document)}}"
                                                           target="_blank"
                                                           title="{{$auditorAgreement->agreement_document }}">
                                                            {{ $auditorAgreement->agreement_document }}</a></td>
                                                    {{--                                                    <td><a href="{{$auditorAgreement->getFirstMedia()->getFullUrl()}}" target="_blank"--}}
                                                    {{--                                                           title="{{$auditorAgreement->getFirstMedia()->name}}">--}}
                                                    {{--                                                            {{str_limit($auditorAgreement->getFirstMedia()->name,'15','...')}}</a></td>--}}

                                                </tr>
                                            @endforeach


                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <h4>Employment History</h4>
                                    <div class="tables">
                                        <table id="EmployementHistoryTable" cellspacing="20" cellpadding="0" border="0"
                                               class="table table-striped table-bordered table-bordered2 table5">
                                            <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Position Held</th>
                                                <th colspan="2" style="text-align: center; padding: 0;">Duration <br>
                                                    <div class="tbl_brdr"></div>
                                                    <div class="col-md-6 border_right from_div floting">From</div>
                                                    <div class="col-md-6 to_div floting">To</div>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="BodyFormalEducationTable">


                                            @foreach($auditor->employmentHistories as $key =>  $auditorEmpHistory)
                                                <tr>
                                                    <td>{{$auditorEmpHistory->company_name}}</td>
                                                    <td>{{$auditorEmpHistory->position_held}}</td>
                                                    <td style="text-align: center;">{{$auditorEmpHistory->employed_from}}</td>
                                                    <td style="text-align: center;">{{$auditorEmpHistory->employed_to}}</td>
                                                </tr>
                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save cont">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="standardsTab">

                                <h3 class="text-center card-header">Standards
                                    {{-- <a href="#" class="edit_comp" id="addStd" onclick="getAddAuditorStandardsModal()">
                                         <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                         Add New
                                     </a>--}}
                                </h3>

                                <div id="addAuditorStandardModalContainer">

                                </div>
                                <div class="tab-pane active" id="auditor-standards-tabs">

                                </div>

                                <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save cont">
                                                        Basic Info
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>


@endsection

@push('scripts')

@endpush

@push('styles')


@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            // show active tab on reload
            if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

            // remember the hash in the URL without jumping
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if (history.pushState) {
                    history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
                } else {
                    location.hash = '#' + $(e.target).attr('href').substr(1);
                }
            });
        });
    </script>

    <script>
        $('.cont').click(function () {
            var nextId = $(this).parents('.tab-pane').next().attr("id") || 'pro_info';

            $('[href="#' + nextId + '"]').tab('show');
        });

        $('.prev').click(function () {
            var nextId = $(this).parents('.tab-pane').prev().attr("id") || 'pro_info';

            $('[href="#' + nextId + '"]').tab('show');
        });


    </script>

    <script>

        $(document).ready(function () {
            var auditorId = $(".auditor-id").val();

            if (auditorId !== '') {
                getAuditorStandardsTabs(auditorId);
            }

        });

        function updateAuditorStatus(auditorId) {
            var url = "{{ route('auditors.update.status', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function getAuditorStandardsTabs(auditorId) {
            var url = "{{ route('auditors.standards.view', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#auditor-standards-tabs").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    </script>




@endpush