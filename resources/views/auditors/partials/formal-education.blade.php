<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-education-form">
        <input type="hidden" name="auditor_id" value="{{$auditor_id}}">
        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th width="8%">Year</th>
                <th>Institution</th>
                <th>Degree / Diploma</th>
                <th>Major Subject(s)</th>
                <th>Certificate</th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody id="BodyFormalEducationTable">


            @foreach($auditorEducations as $auditorEducation)
                <tr>
                    <td>{{$auditorEducation->year_passing}}</td>
                    <td>{{$auditorEducation->institution}}</td>
                    <td>{{$auditorEducation->degree_diploma}}</td>
                    <td>{{$auditorEducation->major_subjects}}</td>
                    <td><a href="{{ asset('/uploads/auditor_education_certificate/'.$auditorEducation->education_document)}}" target="_blank"
                           title="{{$auditorEducation->education_document }}">
                            {{ $auditorEducation->education_document }}</a></td>
{{--                    <td><a href="{{$auditorEducation->getFirstMedia()->getFullUrl()}}" target="_blank" title="{{$auditorEducation->getFirstMedia()->name}}">--}}
{{--                            {{str_limit($auditorEducation->getFirstMedia()->name,'15','...')}}</a></td>--}}
                    <td align="center">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)" onclick="getEditEducationModal({{$auditorEducation->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" onclick="deleteAuditorEducation({{$auditorEducation->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>
            <tfoot>
            <tr>
                <td>
                    <input type="text" name="year_passing" id="year_passing" class="year_sm form-control yearfield"
                           placeholder="Year">

                </td>
                <td>

                    <input type="text" name="institution" id="institution" placeholder="Institution"
                           class="form-control">
                </td>
                <td>

                    <input type="text" name="degree_diploma" id="degree_diploma" placeholder="Degree, Diploma"
                           class="form-control">

                </td>
                <td>

                    <input type="text" name="major_subjects" id="major_subjects" placeholder="Major Subject(s)"
                           class="form-control">

                </td>
                <td>

                    <label class="upload_documt"> Add Document  (10MB)
                        <input type="file" name="certificate" id="certificate" onchange="readURLMany(this, 1)">
                        <img src="{{asset('img/new.png')}}" alt="img">
                    </label>
                    <div id="upld1"></div>
                </td>
                <td>

                    <input type="submit" value="Add New" class="btn btn-primary" id="addFormalEducation">

                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>



<script>

    var auditorId = $(".auditor-id").val();
    var d = new Date();
    var n = d.getFullYear();
    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
        endDate:n.toString()
    });

    $('#auditor-education-form').submit(function (e) {
        e.preventDefault();
        var form = $('#auditor-education-form')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.education.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    $('#otherTabDis').removeClass('disabledDiv');
                    $( "#aaaa" ).removeClass( "educationform" ).addClass( "cont" );
                    getAuditorFormalEducationView(response.data.auditor_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    function deleteAuditorEducation(auditorEducationId){
        var url 	= "{{ route('auditors.education.delete', ['EDUCATION_ID' => "EDUCATION_ID"]) }}".replace("EDUCATION_ID", auditorEducationId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                    getAuditorFormalEducationView(auditorId);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditEducationModal(auditor_education_id){
        var url 	= "{{ route('auditors.education.edit.modal', ['education_id' => "EDUCATION_ID"]) }}".replace("EDUCATION_ID", auditor_education_id);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-education-modal-container").html(response);
                $("#editAuditorEducationModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }



</script>
