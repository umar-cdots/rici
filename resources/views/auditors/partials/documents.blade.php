
<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-document-form">
        <input type="hidden" name="auditor_id" value="{{$auditor_id}}">
        <table id="auditorDocumentsTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th colspan="2">Name</th>
                <th>Upload Documents</th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody id="BodyFormalEducationTable">


            @foreach($auditorDocuments as $auditorDocument)
                <tr>
                    <td colspan="2">{{$auditorDocument->document_name}}</td>
                    <td><a href="{{ asset('/uploads/auditor_document/'.$auditorDocument->auditor_document)}}" target="_blank"
                           title="{{$auditorDocument->auditor_document }}">
                            {{ $auditorDocument->auditor_document }}</a></td>
{{--                    <td><a href="{{$auditorDocument->getFirstMedia()->getFullUrl()}}" target="_blank" title="{{$auditorDocument->getFirstMedia()->name}}">--}}
{{--                            {{str_limit($auditorDocument->getFirstMedia()->name,'15','...')}}</a></td>--}}
                    <td align="center">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)" onclick="getEditAuditorDocumentModal({{$auditorDocument->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" onclick="deleteAuditorDocument({{$auditorDocument->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>

                <div class="modal fade codesModal" id="editAuditorEducationModal{{$auditorDocument->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <form action="#" method="post" id="editAuditorEducationForm" enctype="multipart/form-data">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Edit Auditor Document</h4>
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                                class="sr-only">Close</span></button>
                                </div>
                                <div class="modal-body">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Document Name</label>
                                            <input type="text" name="year_passing" id="year_passing" class="year_sm form-control yearfield"
                                                   placeholder="Year" value="{{$auditorDocument->document_name}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
                                    <button type="submit" class="btn btn-primary">Update Data</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            @endforeach


            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"><input type="text" name="document_name" id="document_name" placeholder="Name"
                                       class="form-control"></td>
                <td width="30%">
                    <div class="col-md-6 floting">
                        <label class="upload_documt"> Add Document (10MB)
                            <input type="file" name="document_file" id="document_file" onchange="readURLMany(this,2)">
                            <img src="{{asset('img/new.png')}}" alt="img">
                        </label>
                        <div id="upld2"></div>
                    </div>
                </td>
                <td>
                    <div class="col-md-6 floting">
                        <input type="submit" value="Add New" class="btn btn-primary" id="addOtherCertificate">
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>

<script>
    var auditorId = $(".auditor-id").val();
    $('#auditor-document-form').submit(function (e) {
        e.preventDefault();
        var form = $('#auditor-document-form')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.documents.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    getAuditorDocumentsView(response.data.auditor_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    function deleteAuditorDocument(auditorDocumentId){
        var url 	= "{{ route('auditors.document.delete', ['DOCUMENT_ID' => "DOCUMENT_ID"]) }}".replace("DOCUMENT_ID", auditorDocumentId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                    getAuditorDocumentsView(auditorId);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditAuditorDocumentModal(auditorDocumentId){
        var url 	= "{{ route('auditors.document.edit.modal', ['DOCUMENT_ID' => "DOCUMENT_ID"]) }}".replace("DOCUMENT_ID", auditorDocumentId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-document-modal-container").html(response);
                $("#editAuditorDocumentModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

</script>
