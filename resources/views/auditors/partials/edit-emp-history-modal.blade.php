<div class="modal fade codesModal" id="editAuditorEmpHistoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editAuditorEmpHistoryForm" enctype="multipart/form-data">
            <input type="hidden" name="auditor_emp_history_id" value="{{$auditorEmpHistory->id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Auditor Education</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="company_name">Company Name</label>
                            <input type="text" name="company_name" id="company_name" placeholder="Company Name"
                                   class="form-control" value="{{$auditorEmpHistory->company_name}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="company_name">Position Held</label>
                            <input type="text" name="position_held" id="position_held" placeholder="Position Held"
                                   class="form-control" value="{{$auditorEmpHistory->position_held}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">From</label>
                            <input type="text" name="employed_from" id="employed_from" class="year_sm form-control yearfield"
                                   placeholder="Position Held From" value="{{$auditorEmpHistory->employed_from}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">To</label>
                            <input type="text" name="employed_to" id="employed_to" class="year_sm form-control yearfield"
                                   placeholder="Position Held Till" value="{{$auditorEmpHistory->employed_to}}">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
                    <button type="submit" class="btn btn-primary updateDataBtn">Update Data</button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(".yearfield").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    $('#editAuditorEmpHistoryForm').submit(function (e) {
       e.preventDefault();
        var form = $('#editAuditorEmpHistoryForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.emp.history.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);

                if (response.status == "success" || response.status == '200') {
                    getAuditorEmpHistoryView(auditorId);
                    $("#editAuditorEmpHistoryModal").modal('hide');

                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
</script>