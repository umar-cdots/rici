<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-agreements-form">
        <input type="hidden" name="auditor_id" value="{{$auditor_id}}">
        <table id="ConfidentalityAgreementTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2 table4">
            <thead>
            <tr>
                <th width="8%">S.No</th>
                <th width="25%">Date</th>
                <th width="40%">Agreement</th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody id="BodyFormalEducationTable">


            @foreach($auditorAgreements as $key =>  $auditorAgreement)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{ $auditorAgreement->date_signed}}</td>

                    
 
                    <td><a href="{{ asset('/uploads/auditor_confidentially_agreement/'.$auditorAgreement->agreement_document)}}" target="_blank"
                           title="{{$auditorAgreement->agreement_document }}">
                            {{ $auditorAgreement->agreement_document }}</a></td>
{{--                    <td><a href="{{$auditorAgreement->getFirstMedia()->getFullUrl()}}" target="_blank" title="{{$auditorAgreement->getFirstMedia()->name}}">--}}
{{--                            {{str_limit($auditorAgreement->getFirstMedia()->name,'15','...')}}</a></td>--}}
                    <td align="center">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)" onclick="getEditAgreementModal({{$auditorAgreement->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" onclick="deleteAuditorAgreement({{$auditorAgreement->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>
            <tfoot>
            <tr>
                <td>
                    <div class="col-md-12">
                        <!--                    <input type="text" name="ac_serial" id="ac_serial" placeholder="Serial" class="form-control">-->
                    </div>
                </td>
                <td>
                    <div class="col-md-12">
                        <input type="text" name="date_signed" id="date_signed" placeholder="Signed on"
                                class="form-control datepicker">
                    </div>
                </td>
                <td width="30%">
                    <div class="col-md-6 floting">
                        <label class="upload_documt"> Add Document  (10MB)
                            <input type="file" name="document_file" id="document_file" onchange="fileUpload(this,1)">
                            <img src="{{asset('img/new.png')}}"></label>
                            <div id="img1"></div>
                    </div>
                </td>
                <td>
                    <div class="col-md-6 floting">
                        <input type="submit" value="Add New" id="addConfidentalityAgreement"
                               class="btn btn-primary">
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>



<script>
    $(document).ready(function () {
        // $(".datepicker").datepicker({
        //     endDate: new Date()
        // });

        $(document).ready(function() {
            $("#date_signed").datepicker({ endDate: "today", format: 'dd/mm/yyyy' });
        });
    });
    function fileUpload(input, id){
        var filename = input.files[0].name;
        var html='';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">'+ filename +'</a></span></h6>';
        }
        $('#img'+id).html(html);
    }
    var auditorId = $(".auditor-id").val();
    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    $('#auditor-agreements-form').submit(function (e) {
        e.preventDefault();
        var form = $('#auditor-agreements-form')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.agreement.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.data.status == "success" || response.status == '200') {
                    $('#standardTabDis').removeClass('disabledDiv');
                    $("#ccc").removeClass("confidentilyform").addClass("cont");
                    getAuditorAgreementView(auditorId);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    function deleteAuditorAgreement(auditorAgreementId){
        var url 	= "{{ route('auditors.agreement.delete', ['AGREEMENT_ID' => "AGREEMENT_ID"]) }}".replace("AGREEMENT_ID", auditorAgreementId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                    getAuditorAgreementView(auditorId);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditAgreementModal(auditor_agreement_id){
        var url 	= "{{ route('auditors.agreement.edit.modal', ['AGREEMENT_ID' => "AGREEMENT_ID"]) }}".replace("AGREEMENT_ID",
            auditor_agreement_id);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-agreement-modal-container").html(response);
                $("#editAuditorAgreementModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }



</script>
