<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-emp-history-form">
        <input type="hidden" name="auditor_id" value="{{$auditor_id}}">
        <table id="EmployementHistoryTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2 table5">
            <thead>
            <tr>
                <th>Company Name</th>
                <th>Position Held</th>
                <th colspan="2" style="text-align: center; padding: 0;">Duration <br>
                    <div class="tbl_brdr"></div>
                    <div class="col-md-6 border_right from_div floting">From</div>
                    <div class="col-md-6 to_div floting">To</div>
                </th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody id="BodyFormalEducationTable">


            @foreach($auditorEmpHistories as $key =>  $auditorEmpHistory)
                <tr>
                    <td>{{$auditorEmpHistory->company_name}}</td>
                    <td>{{$auditorEmpHistory->position_held}}</td>
                    <td style="text-align: center;">{{$auditorEmpHistory->employed_from}}</td>
                    <td style="text-align: center;">{{$auditorEmpHistory->employed_to}}</td>

                    <td align="center">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)" onclick="getEditAuditorEmpHistoryModal({{$auditorEmpHistory->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" onclick="deleteAuditorEmpHistory({{$auditorEmpHistory->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>
            <tfoot>
            <tr>
                <td>
                    <div class="col-md-12">
                        <input type="text" name="company_name" id="company_name"
                               placeholder="Company Name"
                               class="form-control">
                    </div>
                </td>
                <td width="30%">
                    <div class="col-md-12">
                        <input type="text" name="position_held" id="position_held"
                               placeholder="Position Holder" class="form-control">
                    </div>
                </td>
                <td>
                    <div class="col-md-12 floting col-sm-4">
                        <input type="text" name="employed_from" id="employed_from" placeholder="From"
                               class="form-control yearfield" >
                    </div>
                </td>
                <td>
                    <div class="col-md-12 floting col-sm-4">
                        <input type="text" name="employed_to" id="employed_to" placeholder="To" class="form-control yearfield" >
                    </div>
                </td>
                <td>
                    <div class="col-md-2 floting col-sm-4">
                        <input type="submit" value="Add New" id="addEmployementHistory"
                               class="btn btn-primary">
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>



<script>
    var auditorId = $(".auditor-id").val();

    var d = new Date();
    var n = d.getFullYear();
    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
        endDate:n.toString()
    });

    $('#auditor-emp-history-form').submit(function (e) {
        e.preventDefault();
        var form = $('#auditor-emp-history-form')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.emp.history.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    getAuditorEmpHistoryView(auditorId);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    function deleteAuditorEmpHistory(auditorEmpHistoryId){
        var url 	= "{{ route('auditors.emp.history.delete', ['EMP_HISTORY_ID' => "EMP_HISTORY_ID"]) }}".replace("EMP_HISTORY_ID", auditorEmpHistoryId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                    getAuditorEmpHistoryView(auditorId);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditAuditorEmpHistoryModal(auditor_emp_id){
        var url 	= "{{ route('auditors.emp.history.edit.modal', ['EMPLOYMENT_ID' => "EMPLOYMENT_ID"]) }}".replace("EMPLOYMENT_ID", auditor_emp_id);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-emp-history-modal-container").html(response);
                $("#editAuditorEmpHistoryModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }



</script>
