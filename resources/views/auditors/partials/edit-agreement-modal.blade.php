<div class="modal fade codesModal" id="editAuditorAgreementModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editAuditorAgreementForm" enctype="multipart/form-data">
            <input type="hidden" name="auditor_agreement_id" value="{{$auditorAgreement->id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Auditor Confidentiality Agreement</h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">


                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="date_signed">Signed On</label>
                            <input type="text" name="date_signed" id="date_signed" placeholder="Agreement Signed On"
                                   class="form-control datefield" value="{{$auditorAgreement->date_signed}}">
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Document Existing File</label>
                            <br>
                            {{--                            <img src="{{$auditorAgreement->getFirstMedia()->getFullUrl()}}" alt="" width="50" height="50">--}}
                            <img src="{{asset('/uploads/auditor_confidentially_agreement/'.$auditorAgreement->agreement_document)}}"
                                 alt="" width="50" height="50">
                        </div>
                        <label class="upload_documt"> Add Document (10MB)
                            <input type="file" name="document_file" id="document_file" onchange="readURLMany(this,11)">
                            <img src="{{asset('img/new.png')}}" alt="img"></label>

                        <div id="upld11"></div>
                        <br>
                        <small>
                            Upload document to override the existing file
                        </small>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
                    <button type="submit" class="btn btn-primary updateDataBtn">Update Data</button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function readURLMany(input, id) {
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#upld' + id).html(html);
    }

    $(document).ready(function () {
        $(".datefield").datepicker({
            endDate: "today",
            format: 'dd/mm/yyyy'
        });
    });

    $('#editAuditorAgreementForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editAuditorAgreementForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('auditors.agreement.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);

                if (response.status == "success" || response.status == '200') {
                    getAuditorAgreementView(auditorId);
                    $("#editAuditorAgreementModal").modal('hide');

                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
</script>