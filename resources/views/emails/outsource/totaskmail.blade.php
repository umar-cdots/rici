<!DOCTYPE html>
<html>
<head>
    <title>Outsource Request Task Email</title>
</head>
<body>

<h1>Requested To Remarks</h1>
<div class="card-body card_cutom">
    <div class="row">
        @if(!empty($outsource_request->outsource_request_remarks))
            <div class="col-md-12">
                <div class="form-group">
                    <label>Comments Box</label>
                    <div class="card-body chat_box" style="display: block;">
                        <div class="direct-chat-messages">

                            @foreach($outsource_request->outsource_request_remarks as $key=>$remarks)

                                @if((($outsource_request->operation_manager_id) || ($outsource_request->operation_coordinator_id)) && $remarks->operations_remarks != NULL)
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name">Requested By:</span>
                                        </div>
                                        <span class="direct-chat-timestamp">{{ $remarks->operations_remarks }}</span><br>
                                        <span class="direct-chat-timestamp">{{ $remarks->created_at->diffForHumans() }}</span>
                                    </div>

                                @endif
                                @if( $outsource_request->operation_approval_manager_id && $remarks->manager_remarks)
                                    <div class="direct-chat-msg text-right">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name">Requested To:</span>
                                        </div>
                                        <span class="direct-chat-timestamp">  {{ $remarks->manager_remarks }}</span><br>
                                        <span class="direct-chat-timestamp">  {{ $remarks->created_at->diffForHumans() }}</span>
                                    </div>
                                @endif
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        @endif

    </div>
</div>
<button><a href="{{ url('/outsource-request/'.$outsource_request->id.'/show') }}">View</a></button>
<p>Thank you for Approval or rejection</p>

</body>

</html>