<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
<style>
    table {
        @import url('https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i');
    }
</style>
<table style="width:100%;background-color:#f2f2f2;font-family:'Muli', sans-serif; font-size: 14px; padding: 0 15px; "
       width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
    <tr>
        <td style="height: 15px;">

        </td>
    </tr>
    <tr>
        <td>
            <table style="width:600px; background-color: #ffffff; box-shadow: 0 2px 5px #d5d5d5; margin: 0 auto;"
                   width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="15" border="0"
                               style="background-color: #0c2250;" align="center">
                            <tbody>
                            <tr>
                                <td style="text-align:center;">
                                    <a href="#" style="color: #ffffff; text-decoration: none;"><img
                                                src="{{ asset('img/loginLogo.png') }}"
                                                height="30" alt="PGN"
                                                style="vertical-align: bottom; width: 15%; height: 15%"/></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="15" border="0" align="center">
                            <tbody>

                            <tr>
                                <td style="font-size: 12px;">
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px; font-size: 14px;">
                                        <span>Dear {{$invoice['contact_name']}},</span>
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px; ">
                                        <span>This is with reference to the due audit activity with details as below:</span>
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:0px; ">
                                        <span>Audit Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$invoice['audit_type']}}</span>
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:0px; ">
                                        <span>Audit Due Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{date('d/m/Y',strtotime($invoice['due_date']))}}</span>
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px; ">
                                        <span>Standard(s) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$invoice['standard'] === 'ISO 27001:2013' ? 'ISO 27001:2022' : $invoice['standard']}}</span>
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px; ">
                                        <span>Please find attached with this email below documents for your review and action.</span>
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px; ">
                                        <span>- Audit Confirmation Letter @if ($invoice['is_attached_audit_plan'] === '1' || $invoice['is_attached_audit_plan'] === 1)<br>- Audit Invoice @endif  <br>- Audit Plan</span>

                                    </p>

                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px; ">
                                        <span>If there is any change in your certificate data - like change in the name, scope, address or sites you are requested to communicate in writing back to us at least a week before the planned audit date. All changes require prior approval, please note no onsite change request will be entertained by the auditors</span>
                                    </p>


                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:15px;">
                                        <span>
                                             Please give confirmation of the audit date in writing to undesigned or {{$settings['other_user_name']}} – {{$settings['other_user_designation']}}, Email: {{$settings['other_user_email']}}, Mobile {{$settings['other_user_phone_number']}}.
                                        </span>
                                    </p>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 12px;">
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0;margin-bottom:15px; font-weight: 700;">
                                        Kind Regards
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:0px;font-weight: 700;">
                                        {{$user['first_name']}} {{$user['middle_name']}} <br>Operations Manager
                                    </p>
                                    <p style="margin-top:0; margin-left: 0; margin-right: 0; margin-bottom:0px;">
                                        {{$settings['other_details_name']}} <br> Email: <a
                                                href="javascript:void(0)">{{$user['email']}}</a> <br>Mobile:
                                        {{$user['phone_number']}}</p>
                                    <p style="margin-top:15px; margin-left: 0; margin-right: 0; margin-bottom:15px;">
                                        <span style="color: red"><b>This is a system generated email, please do not reply back to this email address (ricicmsalerts@gmail.com)</b></span>
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 15px;">

        </td>
    </tr>
    <tr>
        <td>
            <table style="width:600px; margin: 0 auto; font-size: 10px;" width="600" cellspacing="0" cellpadding="0"
                   border="0" align="center">
                <tbody>
                <tr>
                    <td colspan="2" style="color: #666666;">
                        <p style="margin-top: 0; margin-right: 0; margin-bottom:15px; margin-left: 0;">
                            <strong>Disclaimer : </strong>This email and any files transmitted with it are confidential
                            and intended solely for the use of the individual or entity to whom they are addressed. If
                            you have received this email in error please notify the author and delete the email.
                            Finally, the recipient should check this email and any attachments for the presence of
                            viruses. RICI accepts no liability for any damage caused by any virus transmitted by this
                            email.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="50%" style="color: #666666;">Copyrights {{date('Y')}} | All rights reserved</td>
                    {{--                    <td width="50%" align="right" style="color: #666666;">--}}
                    {{--                        <a style=" text-decoration: none;" target="_blank" href="#">Help</a>--}}
                    {{--                        &nbsp; &nbsp;--}}
                    {{--                        <a style=" text-decoration: none;" target="_blank" href="#">Privacy Policy</a>--}}
                    {{--                        &nbsp; &nbsp;--}}
                    {{--                        <a style=" text-decoration: none;" target="_blank" href="#">Terms &amp;--}}
                    {{--                            Condition</a>--}}
                    {{--                    </td>--}}
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 15px;">

        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
