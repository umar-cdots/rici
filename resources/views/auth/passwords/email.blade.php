

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .loginPage
{
	width: 100%;
    margin: 50px auto;
    box-shadow: 1px 1px 10px #ccc;
}
.loginBg
{
    /*background: url('img/loginBg.png') no-repeat;*/
    min-height: 550px;
    padding: 120px 0 25px 0;
    text-align: center;
    background-size: cover;
    background-color: #21409a;
}
.copyright
{
    position: absolute;
    bottom: 0;
    color: #fff;
    left: 25%;
}
.loginFields
{
    padding: 100px 80px;
}
.loginFields label
{
    color: #21409a;
    font-weight: 600;
}
.loginFields input
{
    background: none;
    border: #21409a solid 1.5px;
}
.loginFields h4
{
    color: #21409a;
    font-size: 30px;
    font-weight: 700;
    margin-bottom: 20px;
}
.loginFields button
{
    background: #21409a;
    width: 100%;
    border-radius: 8px;
    color: #fff;
    border: #21409a solid 1px;
    padding: 8px 0;
    font-weight: 600;
}
.loginFields a{
    color: #21409a;
    font-weight: 600;
}
.noPadd
{
    padding: 0;
}
    </style>
</head>
<body>
<div id="app">
    {{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav> --}}

    {{-- <main class="py-4">
        <div class="container">
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- /.card -->
                            <div class="card">
                                <div class="card-header"></div>

                                <div class="card-body">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form method="POST" action="{{ route('password.email') }}"
                                          aria-label="{{ __('Reset Password') }}">
                                        @csrf

                                        <div class="form-group row">
                                          

                                            <div class="col-md-6">
                                              
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                   
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>

    </main> --}}
    <div class="container">
        <div class="loginPage">
            <div class="row">
                <div class="col-md-6 col-sm 6 col-xs-12 floting noPadd">
                    <div class="loginBg">
                        <img src="{{ asset('img/loginLogo.png') }}" alt="">
                        <div class="copyright">
                            <p>Copyright 2018 all right reserved Creative Dots</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm 6 col-xs-12 floting noPadd">
                    <div class="loginFields">
                        <h4>{{ __('Reset Password') }}</h4>
                        <div class="loginFormArea">
                            <form action="{{ route('password.email') }}"
                            aria-label="{{ __('Reset Password') }}" method="POST">
                                @csrf
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="email"
                                    class="col-form-label">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit"> {{ __('Send Password Reset Link') }}</button>
                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

