
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .loginPage
        {
            width: 100%;
            margin: 50px auto;
            box-shadow: 1px 1px 10px #ccc;
        }
        .loginBg
        {
{{--            background: url('{{ asset('img/loginLogo.png') }}') no-repeat;--}}
            min-height: 550px;
            padding: 120px 0 25px 0;
            text-align: center;
            background-size: cover;
            background-color: #21409a;
        }
        .copyright
        {
            position: absolute;
            bottom: 0;
            color: #fff;
            left: 25%;
        }
        .loginFields
        {
            padding: 100px 80px;
        }
        .loginFields label
        {
            color: #21409a;
            font-weight: 600;
        }
        .loginFields input
        {
            background: none;
            border: #21409a solid 1.5px;
        }
        .loginFields h4
        {
            color: #21409a;
            font-size: 30px;
            font-weight: 700;
            margin-bottom: 20px;
        }
        .loginFields button
        {
            background: #21409a;
            width: 100%;
            border-radius: 8px;
            color: #fff;
            border: #21409a solid 1px;
            padding: 8px 0;
            font-weight: 600;
        }
        .loginFields a{
            color: #21409a;
            font-weight: 600;
        }
        .noPadd
        {
            padding: 0;
        }
    </style>
</head>
<body>
<div id="app">
    <div class="container">
        <div class="loginPage">
            <div class="row">
                <div class="col-md-6 col-sm 6 col-xs-12 floting noPadd">
                    <div class="loginBg">
                        <img src="{{ asset('img/loginLogo.png') }}" alt="">
                        <div class="copyright">
                            <p>Copyright 2018 all right reserved Creative Dots</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm 6 col-xs-12 floting noPadd">
                    <div class="loginFields">
                        <h4>{{ __('Reset Password') }}</h4>
                        <div class="loginFormArea">
                            <form method="POST" action="{{ route('password.request') }}"
                                  aria-label="{{ __('Reset Password') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="email"
                                           class="col-form-label">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-form-label">{{ __('Password') }}</label>
                                        <input id="password" type="password"
                                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm"
                                           class="col-form-label">{{ __('Confirm Password') }}</label>


                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" required>
                                    </div>

                                <div class="form-group">
                                    <button type="submit">  {{ __('Reset Password') }}</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

