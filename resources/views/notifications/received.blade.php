@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-4 floting">
                            <h1>Notifications</h1>
                        </div>
                        <div class="col-sm-6 floting">
                            <a href="#" class="add_comp">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('notifications') }}
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="accordion notificationActive" id="faq">
                        <div class="card">
                            <div class="card-header" id="faqhead3">
                                <h3 class="card-title"><a href="#" class="btn btn-header-link"
                                                          data-toggle="collapse" data-target="#faq3"
                                                          aria-expanded="true" aria-controls="faq3">Received </a>
                                </h3>
                            </div>
                            <div id="faq3" class="collapse show" aria-labelledby="faqhead3" data-parent="#faq">
                                <div class="card-body">
                                    <ul class="nav nav-tabs tabs-up" id="friends">
                                        <li><a href="#received_others" data-target="#received_others"
                                               class="btn btn-primary active span" id="received_others_tab"
                                               data-toggle="tabajax"
                                               rel="tooltip"> Others </a></li>
                                        <li><a href="#received_post_audit" data-target="#received_post_audit"
                                               style="margin-left: 10px"
                                               class="btn btn-primary span" id="received_post_audit_tab"
                                               data-toggle="tabajax"
                                               rel="tooltip"> Post Audit In Process</a></li>
                                        @if(auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator')
                                            <li><a href="#received_post_audit_approved"
                                                   data-target="#received_post_audit_approved"
                                                   style="margin-left: 10px"
                                                   class="btn btn-primary span" id="received_post_audit_approved_tab"
                                                   data-toggle="tabajax"
                                                   rel="tooltip"> Post Audit Approved</a></li>
                                        @endif
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="received_others">
                                            <table id="tabularData" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th style="width: 29px !important;">Sr. #</th>
                                                    <th style="width: 42px !important;">Read</th>
                                                    <th style="width: 37px !important;">Action</th>
                                                    <th style="width: 80px !important;">Task Date</th>
                                                    <th>Category</th>
                                                    <th>Audit Type</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Sent By</th>


                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($receivedNotifications as $notification)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ ($notification->read() == 'no') ? 'N0' : 'YES'  }}</td>
                                                        <td>
                                                            <ul class="data_list">
                                                                <li>
                                                                    @can('show_notification')
                                                                        <a href="javascript:void(0)"
                                                                           onclick="isRead('{{$notification->id}}','{{$notification->url}}')">
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>
                                                                    @endcan
                                                                </li>

                                                                <li>
                                                                    @can('delete_notification')
                                                                        <form action="{{ route('notification.destroy', $notification->id) }}"
                                                                              id="delete_{{$notification->id}}"
                                                                              method="POST">
                                                                            {{ method_field('DELETE') }}
                                                                            @csrf
                                                                            <a style="margin-left:10px;"
                                                                               href="javascript:void(0)"
                                                                               onclick="if(confirmDelete()){ document.getElementById('delete_{{$notification->id}}').submit(); }">
                                                                                <i class="fa fa-close"></i>
                                                                            </a>
                                                                        </form>
                                                                    @endcan
                                                                </li>
                                                            </ul>

                                                        </td>
                                                        <td>{{ $notification->created_at}}
                                                            <br>
                                                            {{  date('d-m-Y', strtotime($notification->updated_at)) }}
                                                        </td>
                                                        <td>
                                                            @if($notification->type == 'post_audit')
                                                                Report Review
                                                            @elseif($notification->type == 'audit_justification')
                                                                ATJF Approval
                                                            @elseif($notification->type == 'draft_for_printing')
                                                                {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                            @elseif($notification->type == 'auditor')
                                                                {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                Approval
                                                            @elseif($notification->type == 'outsource_request')
                                                                {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                Approval
                                                            @elseif($notification->type == 'technical_expert')
                                                                {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                Approval
                                                            @endif

                                                        </td>
                                                        <td>
                                                            @if($notification->type == 'post_audit' || $notification->type == 'draft_for_printing' || $notification->type == 'audit_justification')
                                                                @php
                                                                    $id = 0;
                                                                    if($notification->type == 'audit_justification'){
                                                                        $id = $notification->request_id;
                                                                    }
                                                                    else{
                                                                        $id = $notification->type_id;
                                                                    }

                                                                    $audit_type = App\AJStandardStage::where('id',$id)->first();
                                                                @endphp
                                                                @if(!is_null($audit_type))
                                                                    {{ ucfirst(str_replace('_',' ',$audit_type->audit_type)) }}
                                                                @else
                                                                    --
                                                                @endif
                                                            @else
                                                                --
                                                            @endif
                                                        </td>
                                                        <td>{{ $notification->sent_message }}</td>
                                                        <td>{{ ($notification->status == 'resent') ? 'Unapproved' : ucfirst($notification->status) }}</td>
                                                        <td>{{ $notification->sentByUser->username }}</td>


                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="received_post_audit">
                                            <table id="tabularData1" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th style="width: 29px !important;">Sr. #</th>
                                                    <th style="width: 42px !important;">Read</th>
                                                    <th style="width: 37px !important;">Action</th>
                                                    <th style="width: 80px !important;">Task Date</th>
                                                    <th>Category</th>
                                                    <th>Audit Type</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Sent By</th>


                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($receivedPostAuditNotifications as $notification)


                                                    @php
                                                        $approvedPostAudit = \App\Notification::where('request_id',$notification->request_id)->where('type' ,'post_audit')->get()->last();

                                                    @endphp

                                                    @if($approvedPostAudit->status != 'approved' && $notification->request_id == $approvedPostAudit->request_id )

                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ ($notification->read() == 'no') ? 'NO' : 'YES'  }}</td>
                                                            <td>
                                                                <ul class="data_list">
                                                                    <li>
                                                                        @can('show_notification')
                                                                            <a href="javascript:void(0)"
                                                                               onclick="isRead('{{$notification->id}}','{{$notification->url}}')">
                                                                                <i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endcan
                                                                    </li>

                                                                    <li>
                                                                        @can('delete_notification')
                                                                            <form action="{{ route('notification.destroy', $notification->id) }}"
                                                                                  id="delete_{{$notification->id}}"
                                                                                  method="POST">
                                                                                {{ method_field('DELETE') }}
                                                                                @csrf
                                                                                <a style="margin-left:10px;"
                                                                                   href="javascript:void(0)"
                                                                                   onclick="if(confirmDelete()){ document.getElementById('delete_{{$notification->id}}').submit(); }">
                                                                                    <i class="fa fa-close"></i>
                                                                                </a>
                                                                            </form>
                                                                        @endcan
                                                                    </li>
                                                                </ul>

                                                            </td>
                                                            <td>{{ $notification->created_at}}
                                                                <br>
                                                                {{  date('d-m-Y', strtotime($notification->updated_at)) }}


                                                            </td>
                                                            <td>
                                                                @if($notification->type == 'post_audit')
                                                                    Report Review
                                                                @elseif($notification->type == 'audit_justification')
                                                                    ATJF Approval
                                                                @elseif($notification->type == 'draft_for_printing')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                @elseif($notification->type == 'auditor')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                    Approval
                                                                @elseif($notification->type == 'outsource_request')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                    Approval
                                                                @elseif($notification->type == 'technical_expert')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                    Approval
                                                                @endif

                                                            </td>
                                                            <td>
                                                                @if($notification->type == 'post_audit' || $notification->type == 'draft_for_printing' || $notification->type == 'audit_justification')
                                                                    @php
                                                                        $id = 0;
                                                                        if($notification->type == 'audit_justification'){
                                                                            $id = $notification->request_id;
                                                                        }
                                                                        else{
                                                                            $id = $notification->type_id;
                                                                        }

                                                                        $audit_type = App\AJStandardStage::where('id',$id)->first();
                                                                    @endphp
                                                                    @if(!is_null($audit_type))
                                                                        {{ ucfirst(str_replace('_',' ',$audit_type->audit_type)) }}
                                                                    @else
                                                                        --
                                                                    @endif
                                                                @else
                                                                    --
                                                                @endif
                                                            </td>
                                                            <td>{{ $notification->sent_message }}</td>
                                                            <td>{{ ($notification->status == 'resent') ? 'Unapproved' : ucfirst($notification->status) }}</td>
                                                            <td>{{ $notification->sentByUser->username }}</td>


                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @if(auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator')
                                            <div class="tab-pane" id="received_post_audit_approved">
                                                <table id="tabularDatareceived_post_audit_approved"
                                                       class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 29px !important;">Sr. #</th>
                                                        <th style="width: 42px !important;">Read</th>
                                                        <th style="width: 80px !important;">Task Date</th>
                                                        <th>Category</th>
                                                        <th>Audit Type</th>
                                                        <th>Description</th>
                                                        <th>Status</th>
                                                        <th>Sent By</th>


                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($receivedPostAuditApprovedNotifications as $notification)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ ($notification->read() == 'no') ? 'N0' : 'YES'  }}</td>
                                                            <td>{{ $notification->created_at}}
                                                                <br>
                                                                {{ date('d-m-Y', strtotime( $notification->created_at)) }}
                                                            </td>
                                                            <td>
                                                                @if($notification->type == 'post_audit')
                                                                    Report Review
                                                                @elseif($notification->type == 'audit_justification')
                                                                    ATJF Approval
                                                                @elseif($notification->type == 'draft_for_printing')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                @elseif($notification->type == 'auditor')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                    Approval
                                                                @elseif($notification->type == 'outsource_request')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                    Approval
                                                                @elseif($notification->type == 'technical_expert')
                                                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                                                    Approval
                                                                @endif

                                                            </td>
                                                            <td>
                                                                @if($notification->type == 'post_audit' || $notification->type == 'draft_for_printing' || $notification->type == 'audit_justification')
                                                                    @php
                                                                        $id = 0;
                                                                        if($notification->type == 'audit_justification'){
                                                                            $id = $notification->request_id;
                                                                        }
                                                                        else{
                                                                            $id = $notification->type_id;
                                                                        }

                                                                        $audit_type = App\AJStandardStage::where('id',$id)->first();
                                                                    @endphp
                                                                    @if(!is_null($audit_type))
                                                                        {{ ucfirst(str_replace('_',' ',$audit_type->audit_type)) }}
                                                                    @else
                                                                        --
                                                                    @endif
                                                                @else
                                                                    --
                                                                @endif
                                                            </td>
                                                            <td>{{ $notification->sent_message }}</td>
                                                            <td>{{ ($notification->status == 'resent') ? 'Unapproved' : ucfirst($notification->status) }}</td>
                                                            <td>{{ $notification->sentByUser->username }}</td>


                                                        </tr>

                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif

                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#tabularData').dataTable();
            $('#tabularData1').dataTable();
            $('#tabularDatareceived_post_audit_approved').dataTable();
            $('[data-toggle="tabajax"]').click(function (e) {
                var $this = $(this),
                    loadurl = $this.attr('href'),
                    targ = $this.attr('data-target');


                $this.tab('show');
                return false;
            });
        });

        function isRead(id, url) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.notification.is-read') }}",
                data: {notification_id: id},
                success: function (response) {
                    if (response.status == "success" && response.action == "true") {
                        window.open(url, '_blank');
                    } else if (response.status == "success" && response.action == "false") {
                        window.open(url, '_blank');

                    }
                },
                error: function (error) {
                    toastr['error']('something went wrong');
                }
            });
        }

        function confirmDelete() {
            var r = confirm("Are you sure you want to Delete");
            if (r === true) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush
