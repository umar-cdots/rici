@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="col-sm-4 floting">
                        <h1>Users</h1>
                    </div>
                    <div class="col-sm-6 floting">
                        <a href="#" class="add_comp">
                            <label class="text-capitalize"><i class="fa fa-plus-circle text-lg" aria-hidden="true"></i> Add New User</label>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    {{ Breadcrumbs::render('users') }}
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            <div class="col-12">
                  <!-- /.card -->
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Users</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="tabularData" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Sr. #</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    <th>Specific Permissions</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $user->first_name }}</td>
                                        <td>{{ $user->last_name }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @if($user->roles->isNotEmpty())
                                                @foreach($user->roles as $role)
                                                    @if($loop->index > 0 && $loop->index < count($user->roles))
                                                        ,
                                                    @endif
                                                    {{ $role->name }}
                                                @endforeach
                                            @else
                                                    No role assigned yet
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->permissions->isNotEmpty())
                                                @foreach($user->permissions as $permission)
                                                    @if($loop->index > 0 && $loop->index < count($user->permissions))
                                                        ,
                                                    @endif
                                                    {{ $permission->name }}
                                                @endforeach
                                            @else
                                                No specific permissions
                                            @endif
                                        </td>
                                         <td>
                                             <ul class="data_list">
                                                 <li>
                                                     <a href="#" title="View"><i class="fa fa-eye"></i>
                                                     </a>
                                                 </li>
                                                 <li>
                                                     <a href="{{ route('users.assign.roles', $user->id) }}" title="Roles"><i class="fa fa-edit"></i></a>
                                                 </li>
                                                 {{-- <li>
                                                     <a href="{{ route('company.edit', $company->id) }}" title="Edit"><i class="fa fa-pencil"></i></a>
                                                 </li> --}}
                                                 <li>
                                                     <form class="inline-block remove" action="#" method="post">
                                                          @csrf
                                                          @method('delete')
                                                          <button type="submit" class="rm-inline-button">
                                                              <i class="fa fa-close"></i>
                                                          </button>
                                                    </form> 
                                                     {{-- <a href="" title="Remove"><i class="fa fa-close"></i></a> --}}
                                                 </li>
                                             </ul>
                                         </td>                
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection