@extends('layouts.master')
@section('title', "New permissions and roles")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Permissions and Roles to user</h1>
                        <h6 class="text-center dashboard_heading2">Add Permissions and roles to user{{--  - if you want to learn how to follow these steps - <a href="#">Click Here</a> --}}</h6>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->

            <div class="card card-primary mrgn comp_det_view ">
                <div class="card-header d-flex p-0">
                    <h3 class="card-title p-3">Permissions and roles to user </h3>
                </div>

                <div class="card-body card_cutom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="aud_info">
                            <form action="{{ route('users.assign.roles.store') }}" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="row">
                                    <h2>Roles</h2>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            @if(!empty($roles))
                                                @foreach($roles as $role)
                                                    <input type="checkbox" name="roles[]" class="icheck-blue"
                                                       @foreach ($user->roles as $u_role)
                                                           @if($u_role->id == $role->id)
                                                                checked
                                                           @endif
                                                        @endforeach
                                                    >
                                                    <label>{{ $role->name }}</label><br>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <h2>Permissions</h2>
                                    <div class="col-sm-12">
                                        @if(!empty($permissions))
                                            @php
                                                $total_permissions = count($permissions);
                                                $permissions_per_cols = round($total_permissions / 4);
                                            @endphp
                                            @foreach($permissions->chunk($permissions_per_cols) as $c_permissions)
                                                <div class="col-sm-3">
                                                    @foreach($c_permissions as $permission)
                                                        <input type="checkbox" name="permissions[]" class="icheck-blue"
                                                               @foreach ($user->permissions as $u_permission)
                                                                   @if($u_permission->id == $permission->id)
                                                                        checked
                                                                    @endif
                                                                @endforeach
                                                        >
                                                        <label>{{ $permission->name }}</label><br>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <button class="btn btn-secondary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection