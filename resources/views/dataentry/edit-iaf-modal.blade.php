
<div class="modal codesModal" id="editIAFModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editIAFForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit IAF Code</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Standard(s) Family</label>
                                <select name="standards_family" id="standard-family-iaf-edit-iaf" class="form-control" onchange="updateStandards('standard-family-iaf-edit-iaf', 'standard-iaf-edit-iaf', 'accreditation-iaf-edit-iaf')">
                                    <option value="">--Select Standard Family--</option>
                                    @foreach($standard_families as $standard_family)
                                        <option value="{{ $standard_family->id }}" {{ $standard_family->id === $iaf->standards_family_id ? 'selected' : '' }}>{{ $standard_family->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Standard(s)</label>
                                <select name="standard" id="standard-iaf-edit-iaf" class="form-control" onchange="updateAccreditations('standard-iaf-edit-iaf', 'standard-family-iaf-edit-iaf', 'accreditation-iaf-edit-iaf')">
                                    <option value="">--Select Standard--</option>
                                    @foreach($standards as $standard)
                                        <option style="display: none" value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Accreditation</label>
                                <select name="accreditation" id="accreditation-iaf-edit-iaf" class="form-control">
                                    <option value="">--Select Accreditation--</option>
                                    @foreach($accreditations as $accreditation)
                                        <option style="display: none"
                                                value="{{ $accreditation->id }}"
                                                data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                data-standard-id="{{$accreditation->standard_id}}">
                                            {{ $accreditation->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

{{--                        <div class="row">--}}
{{--                            <div class="col-md-12 ">--}}
{{--                                <div class="form-group check-iaf">--}}
{{--                                    <label>Select Title of IAF Code</label>--}}
{{--                                    <select name="name[]" id="name" class="form-control iaf-select-dropdown-edit"--}}
{{--                                            multiple="multiple" required>--}}
{{--                                        <option value="">--Select IAF--</option>--}}
{{--                                        @foreach($iafs_nullable as $iaf_data)--}}
{{--                                            <option style="display: none" value="{{ $iaf_data->id }}" {{ ($iaf_data->id == $iaf->id) ? 'selected' : '' }}>--}}
{{--                                                {{ $iaf_data->code }} . {{ $iaf_data->name }}--}}
{{--                                            </option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}


                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Enter Title of IAF Code</label>
                                <input type="text" name="name" class="form-control" placeholder="Food Products" value="{{$iaf->name}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Enter IAF Code</label>
                                <input type="text" class="form-control" placeholder="03" name="code" value="{{$iaf->code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="iaf_id" value="{{$iaf->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        IAF Code </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });


    $(document).ready(function () {
        $('.iaf-select-dropdown-edit').select2({
            placeholder: '--Select IAF--',
            width: "100%"
        });
       $('#standard-family-iaf-edit-iaf').trigger('change');

        setTimeout(function(){
            $('#standard-iaf-edit-iaf').val({{ $iaf->standard_id }}).trigger('change');
            setTimeout(function(){
                $('#accreditation-iaf-edit-iaf').val({{ $iaf->accreditation_id }})
            }, 200);
        }, 200);
    });


    $('#editIAFForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editIAFForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.iaf.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editIAFModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>