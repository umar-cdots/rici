
<div class="modal codesModal" id="editEnergyCodeModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editEnergyCodeForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Energy Category</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Standard(s) Family</label>
                                <select name="standards_family" id="standard-family-energy-code-edit-energy-code" class="form-control" onchange="updateStandards('standard-family-energy-code-edit-energy-code', 'standard-energy-code-edit-energy-code', 'accreditation-energy-code-edit-energy-code', 'iaf-energy-code-edit-energy-code', 'ias-energy-code-edit-energy-code')">
                                    <option value="">--Select Standard Family--</option>
                                    @foreach($standard_families as $standard_family)
                                        <option value="{{ $standard_family->id }}" {{ $standard_family->id === $energyCode->standards_family_id ? 'selected' : '' }}>{{ $standard_family->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Standard(s)</label>
                                <select name="standard" id="standard-energy-code-edit-energy-code" class="form-control" onchange="updateAccreditations('standard-energy-code-edit-energy-code', 'standard-family-energy-code-edit-energy-code', 'accreditation-energy-code-edit-energy-code', 'iaf-energy-code-edit-energy-code', 'ias-energy-code-edit-energy-code')">
                                    <option value="">--Select Standard--</option>
                                    @foreach($standards as $standard)
                                        <option style="display: none" value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Accreditation</label>
                                <select name="accreditation" id="accreditation-energy-code-edit-energy-code" class="form-control" onchange="updateIAF('standard-family-energy-code-edit-energy-code', 'standard-energy-code-edit-energy-code', 'accreditation-energy-code-edit-energy-code', 'iaf-energy-code-edit-energy-code', 'ias-energy-code-edit-energy-code')">
                                    <option value="">--Select Accreditation--</option>
                                    @foreach($accreditations as $accreditation)
                                        <option style="display: none"
                                                value="{{ $accreditation->id }}"
                                                data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                data-standard-id="{{$accreditation->standard_id}}">
                                            {{ $accreditation->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>IAF Code</label>--}}
{{--                                <select name="iaf" class="form-control" id="iaf-energy-code-edit-energy-code" onchange="updateIAS('standard-family-energy-code-edit-energy-code', 'standard-energy-code-edit-energy-code', 'accreditation-energy-code-edit-energy-code','iaf-energy-code-edit-energy-code', 'ias-energy-code-edit-energy-code')">--}}
{{--                                    <option value="">--Select IAF Code--</option>--}}
{{--                                    @foreach($iafs as $iaf)--}}
{{--                                        <option style="display: none"--}}
{{--                                                value="{{$iaf->id}}"--}}
{{--                                                data-standard-family-id="{{$iaf->standards_family_id}}"--}}
{{--                                                data-standard-id="{{$iaf->standard_id}}"--}}
{{--                                                data-accreditation-id="{{$iaf->accreditation_id}}">--}}
{{--                                            {{$iaf->code}}. {{$iaf->name}}--}}
{{--                                        </option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>IAS Code</label>--}}
{{--                                <select name="ias" class="form-control" id="ias-energy-code-edit-energy-code">--}}
{{--                                    <option value="">--Select IAS Code--</option>--}}
{{--                                    @foreach($iass as $ias)--}}
{{--                                        <option style="display: none"--}}
{{--                                                value="{{$ias->id}}"--}}
{{--                                                data-standard-family-id="{{$ias->standards_family_id}}"--}}
{{--                                                data-standard-id="{{$ias->standard_id}}"--}}
{{--                                                data-accreditation-id="{{$ias->accreditation_id}}"--}}
{{--                                                data-iaf-id="{{$ias->iaf_id}}">--}}
{{--                                            {{$ias->code}}. {{$ias->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}

{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Technical Area Title</label>
                                <input type="text" name="name" class="form-control" placeholder="Building Complexes" value="{{$energyCode->name}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Technical Area Code</label>
                                <input type="text" class="form-control" placeholder="VIII" name="code" value="{{$energyCode->code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="energy_code_id" value="{{$energyCode->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Energy Code </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('#standard-family-energy-code-edit-energy-code').trigger('change');
        setTimeout(function(){
            $('#standard-energy-code-edit-energy-code').val({{ $energyCode->standard_id }}).trigger('change');
            setTimeout(function(){
                $('#accreditation-energy-code-edit-energy-code').val({{ $energyCode->accreditation_id }}).trigger('change');
                {{--setTimeout(function(){--}}
                {{--    $('#iaf-edit-energy-code').val({{ $energyCode->iaf_id }}).trigger('change');--}}
                {{--    setTimeout(function(){--}}
                {{--        $('#iaf-energy-code-edit-energy-code').val({{ $energyCode->iaf_id }}).trigger('change');--}}
                {{--        setTimeout(function(){--}}
                {{--            $('#ias-energy-code-edit-energy-code').val({{ $energyCode->ias_id }})--}}
                {{--        }, 100);--}}
                {{--    }, 100);--}}
                {{--}, 100);--}}
            }, 100);
        }, 100);
    });


    $('#editEnergyCodeForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editEnergyCodeForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.energy.code.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editEnergyCodeModal").modal('hide');
                  window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>