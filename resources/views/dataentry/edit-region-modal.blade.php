
<div class="modal codesModal" id="editRegionModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editRegionForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Region</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Region Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Region Name" value="{{$region->title}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Country</label>
                                <select name="country_id[]" class="form-control countries" multiple="multiple" required>
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}"
                                                {{$region->regionscountries->contains($country->id) == 'true' ? 'selected': ''}}>
                                            {{$country->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <input type="hidden" name="region_id" value="{{$region->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Region </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $('.countries').select2({
        placeholder: 'Select country(s)',
        width: "100%"
    });

    $('#editRegionForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editRegionForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.region.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editRegionModal").modal('hide');
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>