
<div class="modal codesModal" id="editFoodCategoryModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editFoodCategoryForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Food Category</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Standard(s) Family</label>
                                <select name="standards_family" id="standard-family-food-category-edit-food-category" class="form-control" onchange="updateStandards('standard-family-food-category-edit-food-category', 'standard-food-category-edit-food-category', 'accreditation-food-category-edit-food-category', 'iaf-food-category-edit-food-category', 'ias-food-category-edit-food-category')">
                                    <option value="">--Select Standard Family--</option>
                                    @foreach($standard_families as $standard_family)
                                        <option value="{{ $standard_family->id }}" {{ $standard_family->id === $foodCategory->standards_family_id ? 'selected' : '' }}>{{ $standard_family->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Standard(s)</label>
                                <select name="standard" id="standard-food-category-edit-food-category" class="form-control" onchange="updateAccreditations('standard-food-category-edit-food-category', 'standard-family-food-category-edit-food-category', 'accreditation-food-category-edit-food-category', 'iaf-food-category-edit-food-category', 'ias-food-category-edit-food-category')">
                                    <option value="">--Select Standard--</option>
                                    @foreach($standards as $standard)
                                        <option style="display: none" value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Accreditation</label>
                                <select name="accreditation" id="accreditation-food-category-edit-food-category" class="form-control" onchange="updateIAF('standard-family-food-category-edit-food-category', 'standard-food-category-edit-food-category', 'accreditation-food-category-edit-food-category', 'iaf-food-category-edit-food-category', 'ias-food-category-edit-food-category')">
                                    <option value="">--Select Accreditation--</option>
                                    @foreach($accreditations as $accreditation)
                                        <option style="display: none"
                                                value="{{ $accreditation->id }}"
                                                data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                data-standard-id="{{$accreditation->standard_id}}">
                                            {{ $accreditation->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>IAF Code</label>--}}
{{--                                <select name="iaf" class="form-control" id="iaf-food-category-edit-food-category" onchange="updateIAS('standard-family-food-category-edit-food-category', 'standard-food-category-edit-food-category', 'accreditation-food-category-edit-food-category','iaf-food-category-edit-food-category', 'ias-food-category-edit-food-category')">--}}
{{--                                    <option value="">--Select IAF Code--</option>--}}
{{--                                    @foreach($iafs as $iaf)--}}
{{--                                        <option style="display: none"--}}
{{--                                                value="{{$iaf->id}}"--}}
{{--                                                data-standard-family-id="{{$iaf->standards_family_id}}"--}}
{{--                                                data-standard-id="{{$iaf->standard_id}}"--}}
{{--                                                data-accreditation-id="{{$iaf->accreditation_id}}">--}}
{{--                                            {{$iaf->code}}. {{$iaf->name}}--}}
{{--                                        </option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>IAS Code</label>--}}
{{--                                <select name="ias" class="form-control" id="ias-food-category-edit-food-category">--}}
{{--                                    <option value="">--Select IAS Code--</option>--}}
{{--                                    @foreach($iass as $ias)--}}
{{--                                        <option style="display: none"--}}
{{--                                                value="{{$ias->id}}"--}}
{{--                                                data-standard-family-id="{{$ias->standards_family_id}}"--}}
{{--                                                data-standard-id="{{$ias->standard_id}}"--}}
{{--                                                data-accreditation-id="{{$ias->accreditation_id}}"--}}
{{--                                                data-iaf-id="{{$ias->iaf_id}}">--}}
{{--                                            {{$ias->code}}. {{$ias->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}

{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Food Category</label>
                                <input type="text" name="name" class="form-control" placeholder="Food Manufacturing" value="{{$foodCategory->name}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Category Code</label>
                                <input type="text" class="form-control" placeholder="C" name="code" value="{{$foodCategory->code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="food_category_id" value="{{$foodCategory->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Food Category Code </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('#standard-family-food-category-edit-food-category').trigger('change');
        setTimeout(function(){
            $('#standard-food-category-edit-food-category').val({{ $foodCategory->standard_id }}).trigger('change');
            setTimeout(function(){
                $('#accreditation-food-category-edit-food-category').val({{ $foodCategory->accreditation_id }}).trigger('change');
                setTimeout(function(){
                    $('#iaf-edit-food-category').val({{ $foodCategory->iaf_id }}).trigger('change');
                    setTimeout(function(){
                        $('#iaf-food-category-edit-food-category').val({{ $foodCategory->iaf_id }}).trigger('change');
                        setTimeout(function(){
                            $('#ias-food-category-edit-food-category').val({{ $foodCategory->ias_id }})
                        }, 100);
                    }, 100);
                }, 100);
            }, 100);
        }, 100);
    });

    $('#editFoodCategoryForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editFoodCategoryForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.food.category.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    $("#editFoodCategoryModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>