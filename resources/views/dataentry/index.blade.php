@extends('layouts.master')
@section('title', "Data Entry")

@push('css')
    <style>
        .check-iaf span.select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
        }


    </style>
@endpush

@section('content')

    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark dashboard_heading"> Data Entry</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('data-entry') }}
                    </div>
                    <!-- /.col -->
                </div>
                <div class="accordion" id="faq">
                    <div class="card">
                        <div class="card-header" id="faqhead1">
                            <h3 class="card-title"><a href="#" class="btn btn-header-link" data-toggle="collapse"
                                                      data-target="#faq1"
                                                      aria-expanded="true" aria-controls="faq1">Standards & Codes</a>
                            </h3>

                        </div>
                        <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                            <div class="card-body">
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_blue">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Standard(s) <span>Family</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getStandardFamilyListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-toggle="modal"
                                                               data-target="#addStandardFamilyModal"><i class="far
                                                        fa-edit"></i>CREATE</a></li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_green">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Standard(s) <span>&nbsp</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getStandardsListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addStandardsModal"
                                                               data-toggle="modal"><i class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Accreditation's <span>&nbsp</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getAccreditationListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addAccreditationModal"
                                                               data-toggle="modal"><i class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_blue">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">IAF <span>Codes</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('create_data_entry')
                                                        <li><a href="#" onclick="uploadIAFfile()"><i
                                                                        class="fas fa-upload"></i>Add Code</a></li>
                                                    @endcan
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getIAFListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addIAFModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_green">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">IAS <span>Codes</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('create_data_entry')
                                                        <li><a href="#" onclick="uploadIASfile()"><i
                                                                        class="fas fa-upload"></i>Add Code</a></li>
                                                    @endcan
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getIASListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a>
                                                        </li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addIASModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a></li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_green">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Food <span>Category</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getFoodCategoryListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addFoodCategoryModal"
                                                               data-toggle="modal"><i class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Food <span>Sub Category</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getFoodSubCategoryListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addFoodSubCategoryModal"
                                                               data-toggle="modal"><i class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_blue">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Energy <span>Codes</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getEnergyCodesListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan

                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addEnergyCodeModal"
                                                               data-toggle="modal"><i class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="faqhead2">
                            <h3 class="card-title"><a href="#" class="btn btn-header-link collapsed"
                                                      data-toggle="collapse" data-target="#faq2">Locations</a></h3>
                        </div>
                        <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                            <div class="card-body">
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_green">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Country <span>&nbsp</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getCountriesListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan

                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addCountryModal"
                                                               data-toggle="modal"><i class="far
                                                    fa-edit"></i>CREATE</a></li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Region <span>&nbsp</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getRegionsListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan

                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addRegionModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a></li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_green">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Zones <span>&nbsp</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getZonesListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan

                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addZoneModal"
                                                               data-toggle="modal"><i class="far
                                                    fa-edit"></i>CREATE</a></li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_blue">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">City <span>&nbsp</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" onclick="getCitesListView()"><i
                                                                        class="far fa-eye"></i>VIEW</a></li>
                                                    @endcan

                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#addCityModal" data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a></li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="faqhead3">
                            <h3 class="card-title"><a href="#" class="btn btn-header-link collapsed"
                                                      data-toggle="collapse" data-target="#faq3"
                                                      aria-expanded="true" aria-controls="faq3">Manday Tables </a></h3>
                        </div>
                        <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                            <div class="card-body">
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Man-day's <span>Table</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('show_data_entry')
                                                        <li><a href="#" data-target="#createManDayModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Effective <span>Employees Factors</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#createEffectiveEmployeeModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>

                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Letters <span> Names</span></h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#createLetterNamesModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>

                                <div class="col-md-4 col-sm-6 col-12 floting">
                                    <div class="info-box info_box_orange">
                                        <div class="info-box-content">
                                            <h3 class="info-box-text">Invoice</h3>
                                            <div class="info-box-links">
                                                <ul>
                                                    @can('create_data_entry')
                                                        <li><a href="#" data-target="#createInvoiceModal"
                                                               data-toggle="modal"><i
                                                                        class="far fa-edit"></i>CREATE</a>
                                                        </li>
                                                    @endcan
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <div class="card card-primary mrgn">

                <!-- form start -->
                <form role="form" action="#" method="post">
                    <div class="card-body card_cutom">


                        {{-- <div class="col-md-4 col-sm-6 col-12 floting">
                             <div class="info-box info_box_green">
                                 <div class="info-box-content">
                                     <h3 class="info-box-text">Standard's <span>Group</span></h3>
                                     <div class="info-box-links">
                                         <ul>
                                             <li><a href="#"><i class="far fa-eye"></i>VIEW</a></li>
                                             <li><a href="#"><i class="far fa-edit"></i>CREATE</a></li>
                                         </ul>
                                     </div>
                                 </div>
                                 <!-- /.info-box-content -->
                             </div>
                             <!-- /.info-box -->
                         </div>--}}
                    </div>


                </form>

            </div>


        </section>

    </div>

    <div id="ajaxModalContainer">

    </div>

    <!-- Modals START-->

    <div class="modal fade codesModal" id="addStandardFamilyModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="#" method="post" id="addStandardFamilyForm" enctype="multipart/form-data">

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Standard Family</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Enter Standard Family Name"
                                       class="form-control" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    <input type="hidden" name="is_ims_enabled" value="0">
                                    <input type="checkbox" name="is_ims_enabled"
                                           class="icheck-blue" value="1"> &nbsp; IMS Enabled
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getStandardFamilyListView()">VIEW LIST
                            </button>
                        </div>


                        {{--                        <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">V</button>--}}

                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="addStandardsModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addStandardForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Assign Standard(s)</h3>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family_id" class="form-control" required>
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{$standard_family->id}}">{{$standard_family->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-primary btn-sm add-btn addBtnResponsive"
                                        onclick="addStandardFamily('standard-family')">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Standard(s) Name</label>
                                    <input type="text" name="standard_name" class="form-control"
                                           placeholder="Type Standard's Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Year / Version</label>
                                    <input type="text" class="form-control"
                                           placeholder="Type year/version i.e 9001:2008" name="standard_number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><br><br>
                                        <input type="hidden" name="status" value="0">
                                        <input type="checkbox" name="status"
                                               class="icheck-blue" value="1"> &nbsp; Active/Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getStandardsListView()">VIEW LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addAccreditationModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addAccreditationForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Accreditation</h3>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family_id" id="standard-family-accreditation"
                                            class="form-control"
                                            onchange="updateStandards('standard-family-accreditation', 'standard-accreditation')">
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard_id" id="standard-accreditation" class="form-control">
                                        <option value="">--Select Standard--</option>
                                        @foreach($standards as $standard)
                                            <option style="display: none" value="{{ $standard->id }}"
                                                    data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Enter Complete Name</label>
                                    <input type="text" name="full_name" class="form-control"
                                           placeholder="International Accreditation Services">
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Enter Code</label>
                                    <input type="text" class="form-control" placeholder="Enter Code i.e IAS"
                                           name="name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getAccreditationListView()">VIEW
                                LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addIAFModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addIAFForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add IAF Code</h3>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family" id="standard-family-iaf" class="form-control"
                                            onchange="updateStandards('standard-family-iaf', 'standard-iaf', 'accreditation-iaf')">
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard" id="standard-iaf" class="form-control"
                                            onchange="updateAccreditations('standard-iaf', 'standard-family-iaf', 'accreditation-iaf')">
                                        <option value="">--Select Standard--</option>
                                        @foreach($standards as $standard)
                                            <option style="display: none" value="{{ $standard->id }}"
                                                    data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Accreditation</label>
                                    <select name="accreditation" id="accreditation-iaf" class="form-control">
                                        <option value="">--Select Accreditation--</option>
                                        @foreach($accreditations as $accreditation)
                                            <option style="display: none"
                                                    value="{{ $accreditation->id }}"
                                                    data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                    data-standard-id="{{$accreditation->standard_id}}">
                                                {{ $accreditation->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            {{--                            <div class="col-md-8">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>Enter Title of IAF Code</label>--}}
                            {{--                                    <input type="text" name="name" class="form-control" placeholder="Food Products">--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="col-md-4">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>Enter IAF Code</label>--}}
                            {{--                                    <input type="text" class="form-control" placeholder="03" name="code">--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group check-iaf">
                                    <label>Select Title of IAF Code</label>
                                    <select name="name[]" id="name" class="form-control iaf-select-dropdown"
                                            multiple="multiple">
                                        <option value="">--Select IAF--</option>
                                        @foreach($iafs_nullable as $iaf_data)
                                            <option style="display: none" value="{{ $iaf_data->id }}">
                                                {{ $iaf_data->code }} . {{ $iaf_data->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getIAFListView()">VIEW LIST</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="uploadIAFModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="{{ route('ajax.data.entry.iaf.save') }}" method="post" id="upload-iaf-file"
                      enctype="multipart/form-data">
                    @csrf

                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fas fa-plus"></i>ADD Code </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        {{--                                                <div class="row">--}}
                        {{--                                                    <div class="col-md-6"><h3>Upload IAF Code File</h3></div>--}}
                        {{--                                                    <div class="col-md-6"><a class="btn btn-block btn_search" style="width: 10%; float: right"--}}
                        {{--                                                                             target="_blank" download--}}
                        {{--                                                                             href="{{ asset('/uploads/sheet_documents/iaf_file.xlsx') }}"--}}
                        {{--                                                        ><i class="fas fa-download"></i></a></div>--}}
                        {{--                                                </div>--}}
                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Enter Title of IAF Code</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Food Products">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter IAF Code</label>
                                    <input type="text" class="form-control" placeholder="03" name="code">
                                </div>
                            </div>
                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label></label>--}}
                            {{--                                    <input type="file" class="form-control" name="file">--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        </div>

                        @include('dataentry.codes.iaf-codes-list')
                    </div>
                    <div class="modal-footer">


                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>


                    </div>
                </form>


            </div>

        </div>
    </div>

    <div id="addIASModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addIASForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add IAS Code</h3>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family" id="standard-family-ias" class="form-control"
                                            onchange="updateStandards('standard-family-ias', 'standard-ias', 'accreditation-ias', 'iaf')">
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard" id="standard-ias" class="form-control"
                                            onchange="updateAccreditations('standard-ias', 'standard-family-ias', 'accreditation-ias', 'iaf')">
                                        <option value="">--Select Standard--</option>
                                        @foreach($standards as $standard)
                                            <option style="display: none" value="{{ $standard->id }}"
                                                    data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Accreditation</label>
                                    <select name="accreditation" id="accreditation-ias" class="form-control"
                                            {{--                                            onchange="updateIAF('standard-family-ias', 'standard-ias', 'accreditation-ias', 'iaf')"--}}
                                    >
                                        <option value="">--Select Accreditation--</option>
                                        @foreach($accreditations as $accreditation)
                                            <option style="display: none"
                                                    value="{{ $accreditation->id }}"
                                                    data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                    data-standard-id="{{$accreditation->standard_id}}">
                                                {{ $accreditation->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>IAF Code</label>
                                    <select name="iaf_id" class="form-control" id="iaf">
                                        <option value="">--Select IAF Code--</option>
                                        {{--                                        @foreach($iafs as $iaf)--}}
                                        {{--                                            <option style="display: none"--}}
                                        {{--                                                    value="{{$iaf->id}}"--}}
                                        {{--                                                    data-standard-family-id="{{$iaf->standards_family_id}}"--}}
                                        {{--                                                    data-standard-id="{{$iaf->standard_id}}"--}}
                                        {{--                                                    data-accreditation-id="{{$iaf->accreditation_id}}">--}}
                                        {{--                                                {{$iaf->code}}. {{$iaf->name}}--}}
                                        {{--                                            </option>--}}
                                        {{--                                        @endforeach--}}
                                    </select>

                                </div>
                            </div>
                            {{--                            <div class="col-md-1">--}}
                            {{--                                <button class="btn btn-primary btn-sm add-btn" onclick="addIAFCode('iaf-code')">--}}
                            {{--                                    <i class="fa fa-plus" aria-hidden="true"></i>--}}
                            {{--                                </button>--}}
                            {{--                            </div>--}}

                        </div>
                        {{--                        <div class="row">--}}

                        {{--                            <div class="col-md-8">--}}
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label>Enter Title of IAS Code</label>--}}
                        {{--                                    <input type="text" name="name" class="form-control" placeholder="Food Products">--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-md-4">--}}
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label>Enter IAS Code</label>--}}
                        {{--                                    <input type="text" class="form-control" placeholder="03" name="code">--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}

                        {{--                        </div>--}}


                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group check-iaf">
                                    <label>Select Title of IAS Code</label>
                                    <select name="name[]" id="name" class="form-control ias-select-dropdown"
                                            multiple="multiple">
                                        <option value="">--Select IAS--</option>
                                        @foreach($ias_nullable as $ias_data)
                                            <option style="display: none" value="{{ $ias_data->id }}">
                                                {{ $ias_data->code }} . {{ $ias_data->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getIASListView()">VIEW LIST</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="uploadIASModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="{{ route('ajax.data.entry.ias.save') }}" method="post" id="upload-ias-file"
                      enctype="multipart/form-data">
                    @csrf

                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fas fa-plus"></i>ADD Code </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Enter Title of IAS Code</label>
                                    <input type="text" name="name" class="form-control" placeholder="Food Products">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter IAS Code</label>
                                    <input type="text" class="form-control" placeholder="03" name="code">
                                </div>
                            </div>

                        </div>
                        @include('dataentry.dataentry-codes.ias-codes-list')
                    </div>
                    <div class="modal-footer">


                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>


                    </div>
                </form>


            </div>

        </div>
    </div>

    <div id="addFoodCategoryModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addFoodCategoryForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Food Category Code</h3>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family" id="standard-family-food-category"
                                            class="form-control"
                                            onchange="updateStandards('standard-family-food-category', 'standard-food-category', 'accreditation-food-category', 'iaf-food-category', 'ias-food-category')">
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard" id="standard-food-category" class="form-control"
                                            onchange="updateAccreditations('standard-food-category', 'standard-family-food-category', 'accreditation-food-category', 'iaf-food-category', 'ias-food-category')">
                                        <option value="">--Select Standard--</option>
                                        @foreach($standards as $standard)
                                            <option style="display: none" value="{{ $standard->id }}"
                                                    data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Accreditation</label>
                                    <select name="accreditation" id="accreditation-food-category" class="form-control"
                                            onchange="updateIAF('standard-family-food-category', 'standard-food-category', 'accreditation-food-category', 'iaf-food-category', 'ias-food-category')">
                                        <option value="">--Select Accreditation--</option>
                                        @foreach($accreditations as $accreditation)
                                            <option style="display: none"
                                                    value="{{ $accreditation->id }}"
                                                    data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                    data-standard-id="{{$accreditation->standard_id}}">
                                                {{ $accreditation->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>IAF Code</label>--}}
                            {{--                                    <select name="iaf" class="form-control" id="iaf-food-category" onchange="updateIAS('standard-family-food-category', 'standard-food-category', 'accreditation-food-category','iaf-food-category', 'ias-food-category')">--}}
                            {{--                                        <option value="">--Select IAF Code--</option>--}}
                            {{--                                        @foreach($iafs as $iaf)--}}
                            {{--                                            <option style="display: none"--}}
                            {{--                                                    value="{{$iaf->id}}"--}}
                            {{--                                                    data-standard-family-id="{{$iaf->standards_family_id}}"--}}
                            {{--                                                    data-standard-id="{{$iaf->standard_id}}"--}}
                            {{--                                                    data-accreditation-id="{{$iaf->accreditation_id}}">--}}
                            {{--                                                {{$iaf->code}}. {{$iaf->name}}--}}
                            {{--                                            </option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>IAS Code</label>--}}
                            {{--                                    <select name="ias" class="form-control" id="ias-food-category">--}}
                            {{--                                        <option value="">--Select IAS Code--</option>--}}
                            {{--                                        @foreach($iass as $ias)--}}
                            {{--                                            <option style="display: none"--}}
                            {{--                                                    value="{{$ias->id}}"--}}
                            {{--                                                    data-standard-family-id="{{$ias->standards_family_id}}"--}}
                            {{--                                                    data-standard-id="{{$ias->standard_id}}"--}}
                            {{--                                                    data-accreditation-id="{{$ias->accreditation_id}}"--}}
                            {{--                                                    data-iaf-id="{{$ias->iaf_id}}">--}}
                            {{--                                                {{$ias->code}}. {{$ias->name}}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Food Category</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Food Manufacturing">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Category Code</label>
                                    <input type="text" class="form-control" placeholder="C" name="code">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getFoodCategoryListView()">VIEW LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addFoodSubCategoryModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addFoodSubCategoryForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Food Sub Category Code</h3>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family" id="standard-family-food-sub-category"
                                            class="form-control"
                                            onchange="updateStandards('standard-family-food-sub-category', 'standard-food-sub-category', 'accreditation-food-sub-category', 'iaf-food-sub-category', 'ias-food-sub-category', 'food-category-food-sub-category')">
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard" id="standard-food-sub-category" class="form-control"
                                            onchange="updateAccreditations('standard-food-sub-category', 'standard-family-food-sub-category', 'accreditation-food-sub-category')">
                                        <option value="">--Select Standard--</option>
                                        @foreach($standards as $standard)
                                            <option style="display: none" value="{{ $standard->id }}"
                                                    data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Accreditation</label>
                                    <select name="accreditation" id="accreditation-food-sub-category"
                                            class="form-control"
                                            {{--                                            onchange="updateIAF('standard-family-food-sub-category', 'standard-food-sub-category', 'accreditation-food-sub-category', 'iaf-food-sub-category', 'ias-food-sub-category')"--}}
                                            onchange="updateFoodCategory('standard-family-food-sub-category', 'standard-food-sub-category', 'accreditation-food-sub-category', 'food-category-food-sub-category')"
                                    >
                                        <option value="">--Select Accreditation--</option>
                                        @foreach($accreditations as $accreditation)
                                            <option style="display: none"
                                                    value="{{ $accreditation->id }}"
                                                    data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                    data-standard-id="{{$accreditation->standard_id}}">
                                                {{ $accreditation->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>IAF Code</label>--}}
                            {{--                                    <select name="iaf" class="form-control" id="iaf-food-sub-category" onchange="updateIAS('standard-family-food-sub-category', 'standard-food-sub-category', 'accreditation-food-sub-category','iaf-food-sub-category', 'ias-food-sub-category', 'food-category-food-sub-category')">--}}
                            {{--                                        <option value="">--Select IAF Code--</option>--}}
                            {{--                                        @foreach($iafs as $iaf)--}}
                            {{--                                            <option style="display: none"--}}
                            {{--                                                    value="{{$iaf->id}}"--}}
                            {{--                                                    data-standard-family-id="{{$iaf->standards_family_id}}"--}}
                            {{--                                                    data-standard-id="{{$iaf->standard_id}}"--}}
                            {{--                                                    data-accreditation-id="{{$iaf->accreditation_id}}">--}}
                            {{--                                                {{$iaf->code}}. {{$iaf->name}}--}}
                            {{--                                            </option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>IAS Code</label>--}}
                            {{--                                    <select name="ias" class="form-control" id="ias-food-sub-category" onchange="updateFoodCategory('standard-family-food-sub-category', 'standard-food-sub-category', 'accreditation-food-sub-category','iaf-food-sub-category', 'ias-food-sub-category', 'food-category-food-sub-category')">--}}
                            {{--                                        <option value="">--Select IAS Code--</option>--}}
                            {{--                                        @foreach($iass as $ias)--}}
                            {{--                                            <option style="display: none"--}}
                            {{--                                                    value="{{$ias->id}}"--}}
                            {{--                                                    data-standard-family-id="{{$ias->standards_family_id}}"--}}
                            {{--                                                    data-standard-id="{{$ias->standard_id}}"--}}
                            {{--                                                    data-accreditation-id="{{$ias->accreditation_id}}"--}}
                            {{--                                                    data-iaf-id="{{$ias->iaf_id}}">--}}
                            {{--                                                {{$ias->code}}. {{$ias->name}}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Food Category Code</label>
                                    <select name="food_category_id" class="form-control"
                                            id="food-category-food-sub-category">
                                        <option value="">--Select Category--</option>
                                        @foreach($foodCategories as $foodCategory)
                                            <option style="display: none" value="{{$foodCategory->id}}"
                                                    data-standard-family-id="{{$foodCategory->standards_family_id}}"
                                                    data-standard-id="{{$foodCategory->standard_id}}"
                                                    data-accreditation-id="{{$foodCategory->accreditation_id}}"
                                            >
                                                {{ $foodCategory->code }}
                                                . {{$foodCategory->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            {{--                            <div class="col-md-1">--}}
                            {{--                                <button type="button" class="btn btn-primary btn-sm add-btn"--}}
                            {{--                                        onclick="addFoodCategory('food-category')">--}}
                            {{--                                    <i class="fa fa-plus" aria-hidden="true"></i>--}}
                            {{--                                </button>--}}
                            {{--                            </div>--}}
                        </div>
                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Food Sub Category Name</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Processing of Animal Products">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Sub Category Code</label>
                                    <input type="text" class="form-control" placeholder="CIII" name="code">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getFoodSubCategoryListView()">VIEW
                                LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addEnergyCodeModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addEnergyCodeForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Energy Code</h3>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard(s) Family</label>
                                    <select name="standards_family" id="standard-family-energy" class="form-control"
                                            onchange="updateStandards('standard-family-energy', 'standard-energy', 'accreditation-energy', 'iaf-energy', 'ias-energy')">
                                        <option value="">--Select Standard Family--</option>
                                        @foreach($standard_families as $standard_family)
                                            <option value="{{ $standard_family->id }}">{{ $standard_family->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard" id="standard-energy" class="form-control"
                                            onchange="updateAccreditations('standard-energy', 'standard-family-energy', 'accreditation-energy')">
                                        <option value="">--Select Standard--</option>
                                        @foreach($standards as $standard)
                                            <option style="display: none" value="{{ $standard->id }}"
                                                    data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Accreditation</label>
                                    <select name="accreditation" id="accreditation-energy" class="form-control"
                                            onchange="updateIAF('standard-family-energy', 'standard-energy', 'accreditation-energy', 'iaf-energy')">
                                        <option value="">--Select Accreditation--</option>
                                        @foreach($accreditations as $accreditation)
                                            <option style="display: none"
                                                    value="{{ $accreditation->id }}"
                                                    data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                    data-standard-id="{{$accreditation->standard_id}}">
                                                {{ $accreditation->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>IAF Code</label>--}}
                            {{--                                    <select name="iaf" class="form-control" id="iaf-energy"--}}
                            {{--                                            onchange="updateIAS('standard-family-energy', 'standard-energy', 'accreditation-energy','iaf-energy', 'ias-energy')">--}}
                            {{--                                        <option value="">--Select IAF Code--</option>--}}
                            {{--                                        @foreach($iafs as $iaf)--}}
                            {{--                                            <option style="display: none"--}}
                            {{--                                                    value="{{$iaf->id}}"--}}
                            {{--                                                    data-standard-family-id="{{$iaf->standards_family_id}}"--}}
                            {{--                                                    data-standard-id="{{$iaf->standard_id}}"--}}
                            {{--                                                    data-accreditation-id="{{$iaf->accreditation_id}}">--}}
                            {{--                                                {{$iaf->code}}. {{$iaf->name}}--}}
                            {{--                                            </option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                            <div class="col-md-6">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label>IAS Code</label>--}}
                            {{--                                    <select name="ias" class="form-control" id="ias-energy">--}}
                            {{--                                        <option value="">--Select IAS Code--</option>--}}
                            {{--                                        @foreach($iass as $ias)--}}
                            {{--                                            <option style="display: none"--}}
                            {{--                                                    value="{{$ias->id}}"--}}
                            {{--                                                    data-standard-family-id="{{$ias->standards_family_id}}"--}}
                            {{--                                                    data-standard-id="{{$ias->standard_id}}"--}}
                            {{--                                                    data-accreditation-id="{{$ias->accreditation_id}}"--}}
                            {{--                                                    data-iaf-id="{{$ias->iaf_id}}">--}}
                            {{--                                                {{$ias->code}}. {{$ias->name}}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Technical Area Title</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Building Complexes">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Technical Area Code</label>
                                    <input type="text" class="form-control" placeholder="VIII" name="code">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getEnergyCodesListView()">VIEW LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addCountryModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addCountryForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Country</h3>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Country Name</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Enter Country Name">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Country Code</label>
                                    <input type="text" name="code" class="form-control"
                                           placeholder="Enter Country Code">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone Code</label>
                                    <input type="text" name="phone_code" class="form-control"
                                           placeholder="Enter Phone Code  i.e: +92">
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Currency Name</label>
                                    <input type="text" name="currency_name" class="form-control"
                                           placeholder="i.e: Rupees"
                                           value="">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Currency Code</label>
                                    <input type="text" name="currency_code" class="form-control" placeholder="i.e: PKR"
                                           value="">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getCountriesListView()">VIEW LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addCityModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addCityForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add City</h3>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Zone/Country</label>
                                    <select name="type" class="form-control type" required>
                                        <option value="">Select an option</option>
                                        <option value="zone">Zone</option>
                                        <option value="country">Country</option>

                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-11">
                                <div class="form-group" style="display: none" id="selectZone">
                                    <label>Zone</label>
                                    <select name="zone_id" class="form-control">
                                        <option value="">Select Zone</option>
                                        @foreach($zones as $zone)
                                            <option value="{{$zone->id}}">{{$zone->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group" style="display:none;" id="selectCountry">
                                    <label>Country</label>
                                    <select name="country_id" class="form-control">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>

                                </div>


                            </div>

                            <div class="col-md-1" style="display: none" id="addCountry">
                                <button class="btn btn-primary btn-sm add-btn addBtnResponsive"
                                        onclick="addCountry('city')">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>


                            <div class="col-md-1" style="display: none" id="addZone">
                                <button class="btn btn-primary btn-sm add-btn addBtnResponsive"
                                        onclick="addZone('city')">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>City Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter City Name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>City Post Code</label>
                                    <input type="text" name="post_code" class="form-control"
                                           placeholder="Enter Post Code">
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getCitesListView()">VIEW
                                LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addZoneModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addZoneForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Zone</h3>

                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select name="country_id" class="form-control">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <span class="btn btn-primary btn-sm add-btn addBtnResponsive"
                                      onclick="addCountry('zone')">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </span>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Zone Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter Zone Name">
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getZonesListView()">VIEW
                                LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="addRegionModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form action="#" method="post" id="addRegionForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="far fa-edit"></i>CREATE </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h3>Add Region</h3>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Region Name</label>
                                    <input type="text" name="title" class="form-control"
                                           placeholder="Enter Region Name">
                                </div>
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-11">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select name="country_id[]" class="form-control countries" multiple="multiple"
                                            required>
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <button class="btn btn-primary add-btn addBtnResponsive" onclick="addCountry('region')">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE</button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn_search" onclick="getRegionsListView()">VIEW LIST
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="createManDayModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">MAN-DAY TABLE FOR DATA ENTRY</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body mayDayModal">
                    <ul class="nav nav-pills ml-auto p-2">
                        <li class="nav-item">
                            <a class="nav-link active" href="#genericFamily" data-toggle="tab">Generic Family</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#foodFamily" data-toggle="tab">Food Family</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#energyFamily" data-toggle="tab">Energy Family</a>
                        </li>
                    </ul>
                    <div class="tab-content p-0">
                        <div class="chart tab-pane active" id="genericFamily">
                            <form action="" id="excel-sheet-form">
                                @can('create_data_entry')
                                    <div class="col-md-3 floting">
                                        <label>Upload Table</label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" name="file" class="custom-file-input"
                                                       id="exampleInputFile30" onchange="readURLMany(this,30)">
                                                <label class="custom-file-label" for="exampleInputFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="image form-group" id="file30">

                                        </div>
                                    </div>
                                    <div class="col-md-2 floting">
                                        <label>&nbsp</label>
                                        <div class="form-group">
                                            <button class="btn btn-primary uploadPopupBtn" style="margin-left: 5px;">
                                                Upload
                                            </button>
                                        </div>
                                    </div>
                                @endcan

                                <div class="col-md-3 floting">
                                    <label>&nbsp;</label>
                                    <div class="form-group">
                                        <a class="btn btn-block btn_search" target="_blank" download id="generic_family"
                                           href="{{ asset('/uploads/sheet_documents/'.\App\StandardFamilySheet::where('name','generic_family')->first()->file) }}"
                                        >DOWNLOAD</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>

                            <div class="clearfix"></div>
                            <div class="row" id="refresh">
                                <div class="col-md-3">
                                    <label>View Filters</label>
                                    <div class="input-group form-group">
                                        <select class="form-control" id="standard">
                                            <option value="">Select Standards</option>
                                            @if(!empty($standards))
                                                @foreach($standards as $standard)
                                                    <option value="{{ $standard->id }}">{{ $standard->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>From</label>
                                    <div class=" form-group">
                                        <input type="number" class="form-control" id="from" min="1">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>To</label>
                                    <div class=" form-group">
                                        <input type="number" class="form-control" id="to" min="1">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Complexity</label>
                                    <div class="input-group form-group">
                                        <select class="form-control" id="complexity">
                                            <option value="">Select Complexity</option>
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>&nbsp;</label>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="filter">Filter</button>
                                        <i class="fa fa-refresh" onclick="refresh('refresh')"
                                           style="padding-left: 1rem;"></i>
                                    </div>
                                </div>

                            </div>
                            <div class="foodFamilyTable">
                                <table id="genericSheet" class="table table-bordered availableAuditorTable">
                                    <thead id="genericThead">
                                    <tr>
                                        <th rowspan="2">Standards</th>
                                        <th rowspan="2">Complexity</th>
                                        <th rowspan="2">Frequency</th>
                                        <th colspan="2">Effective Employees</th>
                                        <th colspan="2">Stage 1</th>
                                        <th colspan="2">Stage 2</th>
                                        <th colspan="2">Surveillance</th>
                                        <th colspan="2">Re-audit</th>
                                        {{--                                            <th rowspan="2">Actions</th>--}}
                                    </tr>
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                    </tr>
                                    </thead>
                                    <tbody id="generic_family_sheet">
                                    @if(!empty($generic_sheets))
                                        @foreach($generic_sheets as $sheet)
                                            <tr>
                                                <td>{{ $sheet->standard_number }}</td>
                                                <td>{{ $sheet->complexity }}</td>
                                                <td>{{ $sheet->frequency }}</td>
                                                <td>{{ $sheet->effective_employes_on }}</td>
                                                <td>{{ $sheet->effective_employes_off }}</td>
                                                <td>{{ $sheet->stage1_on }}</td>
                                                <td>{{ $sheet->stage1_off }}</td>
                                                <td>{{ $sheet->stage2_on }}</td>
                                                <td>{{ $sheet->stage2_off }}</td>
                                                <td>{{ ($sheet->frequency == 'annual' && $sheet->surveillance_on > 2) ? $sheet->surveillance_on/2 : $sheet->surveillance_on }}</td>
                                                <td>{{ $sheet->surveillance_off }}</td>
                                                <td>{{ $sheet->reaudit_on }}</td>
                                                <td>{{ $sheet->reaudit_off }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                        <div class="chart tab-pane" id="foodFamily">
                            <form action="" id="food_family_form">

                                @can('create_data_entry')
                                    <div class="col-md-3 floting">
                                        <label>Upload Table</label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" name="file" class="custom-file-input"
                                                       id="exampleInputFile31" onchange="readURLMany(this,31)">
                                                <label class="custom-file-label" for="exampleInputFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="image form-group" id="file31">

                                        </div>
                                    </div>
                                    <div class="col-md-2 floting">
                                        <label>&nbsp</label>
                                        <div class="form-group">
                                            <button class="btn btn-primary uploadPopupBtn" style="margin-left: 5px;">
                                                Upload
                                            </button>
                                        </div>
                                    </div>
                                @endcan
                                <div class="col-md-3 floting">
                                    <label>&nbsp;</label>
                                    <div class="form-group">
                                        <a class="btn btn-block btn_search" target="_blank" download id="food_family"
                                           href="{{ asset('/uploads/sheet_documents/'.\App\StandardFamilySheet::where('name','food_family')->first()->file) }}"
                                        >DOWNLOAD</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>

                            <div class="foodFamilyTable">
                                <table class="table table-bordered availableAuditorTable">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">Standard</th>
                                        <th rowspan="2">CATEGORY</th>
                                        <th rowspan="2">TD</th>
                                        <th rowspan="2">TH</th>
                                        <th rowspan="2">TMS</th>
                                        <th colspan="3">TFTE</th>
                                    </tr>
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody id="food_standard_sheet">
                                    @foreach($food_sheets as $sheet)
                                        <tr>
                                            <td>{{ $sheet->standard->name }}</td>
                                            <td>{{ $sheet->food_category->code }}</td>
                                            <td>{{ $sheet->TD }}</td>
                                            <td>{{ $sheet->TH }}</td>
                                            <td>{{ $sheet->TMS }}</td>
                                            <td>{{ $sheet->TFTE_from }}</td>
                                            <td>{{ $sheet->TFTE_to }}</td>
                                            <td>{{ $sheet->TFTE_value }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>

                            </div>

                        </div>
                        <div class="chart tab-pane" id="energyFamily">
                            <form action="" id="energy_family_form">
                                @can('create_data_entry')
                                    <div class="col-md-3 floting form-group">
                                        <label>Upload Table</label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" name="file" class="custom-file-input form-control"
                                                       id="exampleInputFile32" onchange="readURLMany(this,32)">
                                                <label class="custom-file-label" for="exampleInputFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="image form-group" id="file32">

                                        </div>
                                    </div>

                                    <div class="col-md-2 floting">
                                        <label>&nbsp</label>
                                        <div class="form-group">
                                            <button class="btn btn-primary" style="margin-left: 5px;">Upload
                                            </button>
                                        </div>
                                    </div>
                                @endcan

                                <div class="col-md-3 floting">
                                    <label>&nbsp;</label>
                                    <div class="form-group">
                                        <a class="btn btn-block btn_search" target="_blank" download id="energy_family"
                                           href="{{ asset('/uploads/sheet_documents/'.\App\StandardFamilySheet::where('name','energy_family')->first()->file) }}"
                                        >DOWNLOAD</a>
                                    </div>
                                </div>
                            </form>

                            <form id="weightage-form">
                                <div class="col-md-5 floting"></div>
                                <div class="foodFamilyTable">
                                    <table class="table table-bordered availableAuditorTable">
                                        <thead>
                                        <tr>
                                            <th rowspan="2">CONSIDERATION</th>
                                            <th rowspan="2">CODE</th>
                                            <th colspan="2">VALUE OF CONSIDERATION</th>
                                            <th rowspan="2">COMPLEXITY FACTOR</th>
                                        </tr>
                                        <tr>
                                            <th>FROM</th>
                                            <th>TO</th>
                                        </tr>
                                        </thead>

                                        <tbody id="energy_family_sheet">
                                        @if(!empty($energy_sheets))
                                            @foreach($energy_sheets as $energy_sheet)
                                                <tr>
                                                    <td>{{ $energy_sheet->considerations }}</td>
                                                    {{--                                                    <td>{{ $energy_sheet->energy_code->code }}</td>--}}
                                                    <td>{{ $energy_sheet->energy_code_id }}</td>
                                                    <td>{{ $energy_sheet->from }}</td>
                                                    <td>{{ $energy_sheet->to }}</td>
                                                    <td>{{ $energy_sheet->complexity }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="card card-primary mrgn">
                                    <div class="card-header cardNewHeader">
                                        <h3 class="card-title">WEIGHTAGE % DETERMINATION TABLE</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <div class="card-body card_cutom">

                                        <div class="row form-group">
                                            <div class="col-md-4 floting">
                                                <label><strong>Consideration</strong></label>
                                                <p>Annual energy consumption in TJ</p>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <label><strong>Code</strong></label>
                                                <p>W_EC</p>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="col-md-12">
                                                    <label><strong>Weight</strong></label>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <input type="text" class="form-control" name="ec" id="ae"
                                                           value="{{ $energy_weightage[0]->weightage }}">
                                                </div>
                                                <div class="col-md-6 floting">
                                                    %
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-4 floting">
                                                <p>No. of energy sources</p>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <p>W_ES</p>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="col-md-12">
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <input type="text" class="form-control" name="es" id="es"
                                                           value="{{ $energy_weightage[1]->weightage }}">
                                                </div>
                                                <div class="col-md-6 floting">
                                                    %
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-4 floting">
                                                <p>Number of significant energy uses (SEUs)</p>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <p>W_SEU</p>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="col-md-12">
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <input type="text" class="form-control" name="seu" id="seu"
                                                           value="{{ $energy_weightage[2]->weightage }}">
                                                </div>
                                                <div class="col-md-6 floting">
                                                    %
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->


                                </div>
                                <div class="card card-primary mrgn">
                                    <div class="card-header cardNewHeader">
                                        <h3 class="card-title">DETERMINING LEVEL OF ENERGY COMPLEXITY</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->

                                    <input type="hidden" name="standard_id" id="standard_id_input" value="">
                                    <div class="card-body card_cutom">
                                        <div class="col-md-10">
                                            <div class="col-md-6 floting">
                                                <label><strong>Value Range of Consideration</strong></label>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label><strong>From</strong></label>
                                                        <input type="text" class="form-control" name="from1" id="from1"
                                                               value="{{ $energy_enms[0]->from  }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label><strong>To</strong></label>
                                                        <input type="text" class="form-control" name="to1" id="to1"
                                                               value="{{ $energy_enms[0]->to  }}">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label><strong>From</strong></label>
                                                        <input type="text" class="form-control" name="from2" id="from2"
                                                               value="{{ $energy_enms[1]->from  }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label><strong>To</strong></label>
                                                        <input type="text" class="form-control" name="to2" id="to2"
                                                               value="{{ $energy_enms[1]->to  }}">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label><strong>From</strong></label>
                                                        <input type="text" class="form-control" name="from3" id="from3"
                                                               value="{{ $energy_enms[2]->from  }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label><strong>To</strong></label>
                                                        <input type="text" class="form-control" name="to3" id="to3"
                                                               value="{{ $energy_enms[1]->to  }}">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 floting">
                                                <label><strong>Complexity</strong></label>
                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <label for="">&nbsp;</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <p class="results_answer">Low</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <label for="">&nbsp;</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <p class="results_answer">Medium</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <label for="">&nbsp;</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <p class="results_answer">High</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <div class="col-md-12 mrgn no_padding">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting no_padding">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success btn_save">Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div id="createEffectiveEmployeeModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Effective Employee Calculation FOR DATA ENTRY</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="effective_employee_form" method="POST">
                        <div class="form-group">
                            <label>Factor Part Time</label>
                            <input class="form-control" name="factor_part_time" type="number"
                                   value="{{ $effective_employee_formula->factor_part_time ?? 00 }}" step="0.01"
                                   placeholder="0.55">
                        </div>

                        <div class="form-group">
                            <label>Factor Similar</label>
                            <input class="form-control" name="factor_similar" type="number"
                                   value="{{ $effective_employee_formula->factor_similar ?? 00}}" step="0.01"
                                   placeholder="0.75">
                        </div>

                        <div class="form-group">
                            <label>Factor =((Total-S-PT)+(F_S*S+F_PT*PT))</label>
                        </div>


                        <div class="modal-footer">

                            <div class="col-md-3">
                                <button type="submit" class="btn btn-block btn-success btn_save">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div id="createLetterNamesModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Letters Name</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="letter_names_form" method="POST">
                        <div class="form-group">
                            <label>Rici Name</label>
                            <input class="form-control" name="rici_name" type="text"
                                   value="{{ $letters->rici_name ?? 'RICI CO W.L.L' }}"
                                   placeholder="RICI Name">
                        </div>

                        <div class="form-group">
                            <label>Rici Address</label>
                            <input class="form-control" name="rici_address" type="text"
                                   value="{{ $letters->rici_address ?? '7067, Salman Al Farisi 3002, Al Khalidiyah Al Janubiyah Dist., Dammam 32225, Kingdom of Saudi Arabia'}}"
                                   placeholder="Rici Address">
                        </div>
                        <div class="form-group">
                            <label>Doc Name 1</label>
                            <input class="form-control" name="doc_name_one" type="text"
                                   value="{{ $letters->doc_name_one ?? 'Certification Rules and Regulations, - Doc 01'}}"
                                   placeholder="Doc Name 1">
                        </div>
                        <div class="form-group">
                            <label>Doc Name 2</label>
                            <input class="form-control" name="doc_name_two" type="text"
                                   value="{{ $letters->doc_name_two ?? 'Rules for use of Certification and Accreditation Marks, Doc 02'}}"
                                   placeholder="Doc Name 2">
                        </div>


                        <div class="modal-footer">

                            <div class="col-md-3">
                                <button type="submit" class="btn btn-block btn-success btn_save">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div id="createInvoiceModal" class="modal fade codesModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Invoice</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="invoice_form" method="POST">
                        <div class="form-group">
                            <label>Tax Authorities (Entry should be comma separated)</label>
                            <input class="form-control" name="tax_authorities" type="text"
                                   value="{{ $invoice->tax_authorities ?? 'P.R.A,S.R.B,K.P.K.A' }}"
                                   placeholder="Tax Authorities">
                        </div>
                        <div class="form-group">
                            <label>Tax Percentages (Entry should be comma separated)</label>
                            <input class="form-control" name="tax_percentage" type="text"
                                   value="{{ $invoice->tax_percentage ?? '16,20,30' }}"
                                   placeholder="Tax Percentages">
                        </div>
                        <div class="form-group">
                            <label>Inflation Percentages (Entry should be comma separated)</label>
                            <input class="form-control" name="inflation_percentage" type="text"
                                   value="{{ $invoice->inflation_percentage ?? '0,10,15' }}"
                                   placeholder="Inflation Percentages">
                        </div>

                        <div class="form-group">
                            <label>Invoice Remarks One</label>
                            <textarea class="form-control" name="invoice_remarks_one" rows="1"
                                      placeholder="Invoice Remarks One">{!! $invoice->invoice_remarks_one ?? 'As per the Punjab Government Sales Tax regulation for Active taxpayers companies, sales tax of RICI cannot be withheld.' !!}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label>Invoice Remarks Two</label>
                            <textarea class="form-control" name="invoice_remarks_two" type="text" rows="2"
                                      placeholder="Invoice Remarks Two">{!! $invoice->invoice_remarks_two ?? 'As per new Income Tax Rule 153(1) (b) withholding for Advance/Income tax for certification/Testing/Inspection/Training Services is changed to 3% from 8 %.' !!}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label>Coordinator user name</label>
                            <input class="form-control" name="other_user_name" type="text"
                                   value="{{ $invoice->other_user_name ?? 'Faizan Ahmed'}}"
                                   placeholder="Other user name">
                        </div>
                        <div class="form-group">
                            <label>Coordinator user designation</label>
                            <input class="form-control" name="other_user_designation" type="text"
                                   value="{{ $invoice->other_user_designation ?? 'Operations Coordinator'}}"
                                   placeholder="Other user designation">
                        </div>
                        <div class="form-group">
                            <label>Coordinator user email</label>
                            <input class="form-control" name="other_user_email" type="text"
                                   value="{{ $invoice->other_user_email ?? 'Coordination2@ricipakistan.com'}}"
                                   placeholder="Other user email">
                        </div>
                        <div class="form-group">
                            <label>Coordinator user phone number</label>
                            <input class="form-control" name="other_user_phone_number" type="text"
                                   value="{{ $invoice->other_user_phone_number ?? '0346-4497125'}}"
                                   placeholder="Other user phone number">
                        </div>

                        <div class="form-group">
                            <label>Company name</label>
                            <input class="form-control" name="other_details_name" type="text"
                                   value="{{ $invoice->other_details_name ?? 'RICI Pakistan (Pvt.) Ltd.'}}"
                                   placeholder="Other details name">
                        </div>
                        <div class="form-group">
                            <label>Company address</label>
                            <input class="form-control" name="other_details_address" type="text"
                                   value="{{ $invoice->other_details_address ?? '181-A, M BLOCK GULBERG III Lahore – Pakistan '}}"
                                   placeholder="Other details address">
                        </div>
                        <div class="form-group">
                            <label>Company email</label>
                            <input class="form-control" name="other_details_email" type="text"
                                   value="{{ $invoice->other_details_email ?? 'operations@ricipakistan.com'}}"
                                   placeholder="Other details email">
                        </div>
                        <div class="form-group">
                            <label>Company phone numbers</label>
                            <input class="form-control" name="other_details_phone_number" type="text"
                                   value="{{ $invoice->other_details_phone_number ?? '+92-344-4442484'}}"
                                   placeholder="Other details phone numbers">
                        </div>

                        <div class="modal-footer">

                            <div class="col-md-3">
                                <button type="submit" class="btn btn-block btn-success btn_save">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
          integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">

    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">
    <style>
        .select2-container--default .select2-selection--single {
            height: 40px;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

    <script>
        $('.iaf-select-dropdown').select2({
            placeholder: '--Select IAF--',
            width: "100%"
        });

        $('.ias-select-dropdown').select2({
            placeholder: '--Select IAS--',
            width: "100%"
        });
        $("#effective_employee_form").submit(function (e) {
            e.preventDefault();
            var form = $("#effective_employee_form")[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('effective.employees.save') }}",
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        $("#createEffectiveEmployeeModal").modal('hide');
                        $("#effective_employee_form")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#letter_names_form").submit(function (e) {
            e.preventDefault();
            var form = $("#letter_names_form")[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('letter.names.save') }}",
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        $("#createInvoiceModal").modal('hide');
                        $("#letter_names_form")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $("#invoice_form").submit(function (e) {
            e.preventDefault();
            var form = $("#invoice_form")[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('invoice.settings.save') }}",
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        $("#createLetterNamesModal").modal('hide');
                        $("#invoice_form")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        // function cbDropdown(column) {
        //     return $('<ul>', {
        //         'class': 'cb-dropdown'
        //     }).appendTo($('<div>', {
        //         'class': 'cb-dropdown-wrap'
        //     }).appendTo(column));
        // }
        // var $genericesheet = $('#genericSheet').dataTable({
        //
        //     // dom: 'Bfrtip',
        //     // buttons: [
        //     //     'print','csv'
        //     // ],
        //     "lengthMenu": [[20, 40, 100, -1], [20, 40, 100, "All"]],
        //     // initComplete: function() {
        //     //     this.api().columns().every(function() {
        //     //         var column = this;
        //     //         var ddmenu = cbDropdown($(column.header()))
        //     //             .on('change', ':checkbox', function() {
        //     //                 var active;
        //     //                 var vals = $(':checked', ddmenu).map(function(index, element) {
        //     //                     active = true;
        //     //                     return $.fn.dataTable.util.escapeRegex($(element).val());
        //     //                 }).toArray().join('|');
        //     //
        //     //                 column
        //     //                     .search(vals.length > 0 ? '^(' + vals + ')$' : '', true, false)
        //     //                     .draw();
        //     //
        //     //                 // Highlight the current item if selected.
        //     //                 if (this.checked) {
        //     //                     $(this).closest('li').addClass('active');
        //     //                 } else {
        //     //                     $(this).closest('li').removeClass('active');
        //     //                 }
        //     //
        //     //                 // Highlight the current filter if selected.
        //     //                 var active2 = ddmenu.parent().is('.active');
        //     //                 if (active && !active2) {
        //     //                     ddmenu.parent().addClass('active');
        //     //                 } else if (!active && active2) {
        //     //                     ddmenu.parent().removeClass('active');
        //     //                 }
        //     //             });
        //     //
        //     //         column.data().unique().sort().each(function(d, j) {
        //     //             var // wrapped
        //     //                 $label = $('<label>'),
        //     //                 $text = $('<span>', {
        //     //                     text: d
        //     //                 }),
        //     //                 $cb = $('<input>', {
        //     //                     type: 'checkbox',
        //     //                     value: d
        //     //                 });
        //     //
        //     //             $text.appendTo($label);
        //     //             $cb.appendTo($label);
        //     //
        //     //             ddmenu.append($('<li>').append($label));
        //     //         });
        //     //     });
        //     // }
        // });
        $('#energy_standard_id').change(function () {
            let standard_value = $(this).val();
            $("#standard_id_input").val(standard_value);
        });

        $('.datefield').datepicker();
        $('.countries').select2({
            placeholder: 'Select country(s)',
            width: "100%"
        });
        $('#iaf').select2({
            width: "100%"
        });
        $("#filter").click(function () {
            var standard_id = $("#standard").val();
            var from = $("#from").val();
            var to = $("#to").val();
            var complexity = $("#complexity").val();

            // if (standard_id != '' && from != '' && to != '' && complexity != '') {
            $.ajax({
                type: "POST",
                url: '{{ route('mandaytable.filter') }}',
                data: {standard: standard_id, from: from, to: to, complexity: complexity},
                cache: false,
                success: function (response) {
                    if (typeof response == 'string') {
                        $("#generic_family_sheet").html(response);
                    } else {
                        // alert(response.message);
                    }
                },
                error: function (response) {
                    alert('Something went wrong.');
                }
            });
            // } else {
            //     alert('Please select all fields of filters');
            // }
        });

        /*showing confirm cancel popup box*/
        function showConfirmDelete() {
            return confirm("Are You Sure You Want To Delete This Data?");
        }

        function getStandardFamilyListView() {
            $("#addStandardFamilyModal").modal('hide');
            var url = "{{ route('data.entry.standard.family.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);

                    $('#ConfidentalityAgreementTable').dataTable();
                    $("#listStandardFamilyModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function getStandardsListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.standards.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#standardListTable").dataTable();
                    $("#listStandardsModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addStandardFamilyForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addStandardFamilyForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.standard.family.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addStandardFamilyModal").modal('hide');
                        $("#addStandardFamilyForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('#addStandardForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addStandardForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.standard.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addStandardsModal").modal('hide');
                        $("#addStandardForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        // update Standards
        function updateStandards(standardFamily, standard, accreditation = null, iaf = null, ias = null, foodCategory = null) {

            $("#" + standard).children('option').not(':first').hide();
            $("#" + accreditation).children('option').not(':first').hide();
            $("#" + iaf).children('option').not(':first').hide();
            $("#" + ias).children('option').not(':first').hide();
            $("#" + foodCategory).children('option').not(':first').hide();

            $("#" + standard).prop("selectedIndex", 0);
            $("#" + standard + " option[data-standard-family-id=" + $("#" + standardFamily).val() + "]").show();

            if (standard) {
                $("#" + standard).prop("selectedIndex", 0);
            }

            if (accreditation) {
                $("#" + accreditation).prop("selectedIndex", 0);
            }

            if (iaf) {
                $("#" + iaf).prop("selectedIndex", 0);
            }

            if (ias) {
                $("#" + ias).prop("selectedIndex", 0);
            }

            if (foodCategory) {
                $("#" + foodCategory).prop("selectedIndex", 0);
            }
        }

        function updateAccreditations(standard, selectedStandardFamilyId, accreditation, iaf = null, ias = null, foodCategory = null) {

            let selectStandardFamily = $("#" + selectedStandardFamilyId).val();
            let selectedStandard = $("#" + standard).val();

            if (selectStandardFamily && selectedStandard) {

                $("#" + iaf).children('option').not(':first').hide();
                $("#" + ias).children('option').not(':first').hide();
                $("#" + foodCategory).children('option').not(':first').hide();

                if (iaf) {
                    $("#" + iaf).prop("selectedIndex", 0);
                }

                if (ias) {
                    $("#" + ias).prop("selectedIndex", 0);
                }

                if (foodCategory) {
                    $("#" + foodCategory).prop("selectedIndex", 0);
                }

                $("#" + accreditation).children('option').not(':first').hide();
                $("#" + accreditation).prop("selectedIndex", 0);
                $("#" + accreditation + " option[data-standard-family-id=" + selectStandardFamily + "][data-standard-id=" + selectedStandard + "]").show();
            }

        }

        function updateIAF(standardFamily, standard, accreditation, iaf, ias = null, foodCategory = null) {

            let standardFamilyId = $("#" + standardFamily).val();
            let standardId = $("#" + standard).val();
            let accreditationId = $("#" + accreditation).val();

            $("#" + ias).children('option').not(':first').hide();
            $("#" + ias).prop("selectedIndex", 0);

            $("#" + foodCategory).children('option').not(':first').hide();
            $("#" + foodCategory).prop("selectedIndex", 0);

            if (standardFamilyId && standardId && accreditationId) {

                $("#" + iaf).children('option').not(':first').hide();
                $("#" + iaf).prop("selectedIndex", 0);
                $("#" + iaf + " option[data-standard-family-id=" + standardFamilyId + "][data-standard-id=" + standardId + "][data-accreditation-id=" + accreditationId + "]").show();
            }

        }

        function updateIAS(standardFamily, standard, accreditation, iaf, ias, foodCategory = null) {

            let standardFamilyId = $("#" + standardFamily).val();
            let standardId = $("#" + standard).val();
            let accreditationId = $("#" + accreditation).val();
            let iafId = $("#" + iaf).val();

            $("#" + foodCategory).children('option').not(':first').hide();
            $("#" + foodCategory).prop("selectedIndex", 0);

            if (standardFamilyId && standardId && accreditationId && iafId) {

                $("#" + ias).children('option').not(':first').hide();
                $("#" + ias).prop("selectedIndex", 0);
                $("#" + ias + " option[data-standard-family-id=" + standardFamilyId + "][data-standard-id=" + standardId + "][data-accreditation-id=" + accreditationId + "][data-iaf-id=" + iafId + "]").show();
            }

        }

        function updateFoodCategory(standardFamily, standard, accreditation, foodCategory) {

            // function updateFoodCategory(standardFamily, standard, accreditation, iaf, ias, foodCategory){

            // if (standardFamily && standard && accreditation && iaf && ias) {
            if (standardFamily && standard && accreditation) {

                let standardFamilyId = $("#" + standardFamily).val();
                let standardId = $("#" + standard).val();
                let accreditationId = $("#" + accreditation).val();
                // let iafId               = $("#"+iaf).val();
                // let iasId               = $("#"+ias).val();

                $("#" + foodCategory).children('option').not(':first').hide();
                $("#" + foodCategory).prop("selectedIndex", 0);
                // $("#"+foodCategory + " option[data-standard-family-id=" + standardFamilyId + "][data-standard-id=" + standardId +"][data-accreditation-id=" + accreditationId +"][data-iaf-id=" + iafId +"][data-ias-id=" + iasId +"]").show();
                $("#" + foodCategory + " option[data-standard-family-id=" + standardFamilyId + "][data-standard-id=" + standardId + "][data-accreditation-id=" + accreditationId + "]").show();
            }

        }

        $('#addAccreditationForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addAccreditationForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.accreditation.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addAccreditationModal").modal('hide');
                        $("#addAccreditationForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getAccreditationListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.accreditation.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $('#accreditationListTable').dataTable();
                    $("#listAccreditationModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addIAFForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addIAFForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.iaf.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addIAFModal").modal('hide');
                        $("#addIAFForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getIAFListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.iaf.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $('#iafListTable').dataTable();
                    $("#listIAFModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addIASForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addIASForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.ias.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addIASModal").modal('hide');
                        $("#addIASForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function uploadIASfile() {

            $("#uploadIASModal").modal('show');
        }


        function uploadIAFfile() {

            $("#uploadIAFModal").modal('show');
        }

        function getIASListView() {

            $(".modal").modal('hide');
            var url = "{{ route('data.entry.ias.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);

                    $("#listIASModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addFoodCategoryForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addFoodCategoryForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.food.category.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addFoodCategoryModal").modal('hide');
                        $("#addFoodCategoryForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getFoodCategoryListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.food.category.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#foodCategoryListTable").dataTable();
                    $("#listFoodCategoryModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addFoodSubCategoryForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addFoodSubCategoryForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.food.sub.category.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addFoodSubCategoryModal").modal('hide');
                        $("#addFoodSubCategoryForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getFoodSubCategoryListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.food.sub.category.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#foodSubCategoryListTable").dataTable();
                    $("#listFoodSubCategoryModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addEnergyCodeForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addEnergyCodeForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.energy.code.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addEnergyCodeModal").modal('hide');
                        $("#addEnergyCodeForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getEnergyCodesListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.energy.code.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#energyListTable").dataTable();
                    $("#listEnergyCodesModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addCountryForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addCountryForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.country.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addCountryModal").modal('hide');
                        $("#addCountryForm")[0].reset();
                        location.reload();
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getCitesListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.cities.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#citiesModal").modal('show');
                    $('#citiesData').dataTable();
                    // $('#genericSheet').dataTable();
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function getZonesListView() {

            $(".modal").modal('hide');
            var url = "{{ route('data.entry.zones.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#zonesModal").modal('show');
                    $('#zonesData').dataTable();
                    // $('#genericSheet').dataTable();
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }


        function getCountriesListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.countries.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#listCountriesModal").modal('show');
                    $('#tabularData').dataTable();
                    // $('#genericSheet').dataTable();
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('#addCityForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addCityForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.city.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addCityModal").modal('hide');
                        $("#addCityForm")[0].reset();
                        location.reload();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('#addZoneForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addZoneForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.zone.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addZoneModal").modal('hide');
                        $("#addZoneForm")[0].reset();
                        location.reload();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('#addRegionForm').submit(function (e) {
            e.preventDefault();
            var form = $('#addRegionForm')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('data.entry.region.save') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {

                        $("#addRegionModal").modal('hide');
                        $("#addRegionForm")[0].reset();
                        location.reload();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getRegionsListView() {
            $(".modal").modal('hide');
            var url = "{{ route('data.entry.regions.index')}}";
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#ajaxModalContainer").html(response);
                    $("#regionListTable").dataTable();
                    $("#regionsModal").modal('show');

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $("#excel-sheet-form").submit(function (e) {
            e.preventDefault();
            var form = $('#excel-sheet-form')[0];
            var formData = new FormData(form);
            var myUrl = '{{ route('mandaytable.excel') }}';
            $("#loader").show();
            axios.post(myUrl, formData, {
                headers: {
                    'csrftoken': '{{ csrf_token() }}',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                // other configuration there
            }).then(function (response) {

                if (response.data.status == '200') {
                    $("#generic_family").attr("href", "/uploads/sheet_documents/" + response.data.generic_sheet_family);
                    var html = '';
                    $(response.data.records).each(function (index, value) {

                        html += '<tr>';
                        html += '<td>' + value.standard_number + '</td>';
                        html += '<td>' + value.complexity + '</td>';
                        html += '<td>' + value.frequency + '</td>';
                        html += '<td>' + value.effective_employes_on + '</td>';
                        html += '<td>' + value.effective_employes_off + '</td>';
                        html += '<td>' + value.stage1_on + '</td>';
                        html += '<td>' + value.stage1_off + '</td>';
                        html += '<td>' + value.stage2_on + '</td>';
                        html += '<td>' + value.stage2_off + '</td>';
                        if (value.frequency === 'annual' && value.surveillance_on > 2) {
                            html += '<td>' + value.surveillance_on / 2 + '</td>';
                        } else {
                            html += '<td>' + value.surveillance_on + '</td>';
                        }
                        html += '<td>' + value.surveillance_off + '</td>';
                        html += '<td>' + value.reaudit_on + '</td>';
                        html += '<td>' + value.reaudit_off + '</td>';
                        html += '</tr>';


                    });

                    $("#generic_family_sheet").html(html);
                    if (response.data.records.length > 0) {
                        setTimeout(function () {
                            $("#loader").hide();
                            toastr['success']("Sheet Uploaded Successfully.");
                        }, 20000);
                    }
                } else {
                    $("#generic_family_sheet").empty();
                }
            }).catch(function (error) {
                toastr['error']("Something Went Wrong.");
            });

        });


        $('#food_family_form').submit(function (e) {
            e.preventDefault();
            var form = $('#food_family_form')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('foodfamily.excel') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    //
                    // ajaxResponseHandler(response, form);
                    // if (response.status == "success" || response.status == '200') {
                    //     console.log('Food sheet has been uploaded.');
                    // }
                    if (response.status == '200') {
                        toastr['success']("Sheet Uploaded Successfully.");

                        $("#food_family").attr("href", "/uploads/sheet_documents/" + response.food_sheet_family);
                        $("#food_standard_sheet").html(response.records);
                    } else {
                        $("#food_standard_sheet").empty();
                    }
                    //
                    // $("#food_family").attr("href", "/uploads/sheet_documents/"+response.food_sheet_family);
                    // $("#food_standard_sheet").html(response.records);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $('#energy_family_form').submit(function (e) {
            e.preventDefault();
            var form = $('#energy_family_form')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('energyfamily.excel') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {

                    // ajaxResponseHandler(response, form);
                    if (response.status == '200') {
                        toastr['success']("Sheet Uploaded Successfully.");
                        $("#energy_family").attr("href", "/uploads/sheet_documents/" + response.energy_sheet_family);
                        $("#energy_family_sheet").html(response.records);
                    } else {
                        $("#energy_family_sheet").empty();
                    }

                    // if (response.status == "success" || response.status == '200') {
                    //     console.log('Food sheet has been uploaded.');
                    // }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#weightage-form").submit(function (e) {

            e.preventDefault();
            // if ($("#standard_id_input").val() != '' && $("#standard_id_input").val() != 0) {
            var form = $('#weightage-form')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('weightage.store') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        console.log('Weight % has been saved.');
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
            // } else {
            //     alert('Please Select the Standard.');
            // }

        });

        function readURLMany(input, id) {
            if (input.id == 'exampleInputFile' + id) {
                var filename = input.files[0].name;
                var html = '';
                if (input.files && input.files[0]) {
                    html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
                }
                $('#file' + id).html(html);
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#upld' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        function refresh(divElement) {
            var standard_id = $("#standard").val('');
            var from = $("#from").val('');
            var to = $("#to").val('');
            var complexity = $("#complexity").val('');
        }

        $("#food_standard").change(function () {

            var standard_id = $(this).val();

            $.ajax({
                type: "POST",
                url: "{{ route('dataentry.foodfamily.filter') }}",
                data: {id: standard_id},
                cache: true,
                success: function (response) {
                    var html = '';
                    if (response.status == 'success') {
                        if (response.data.length > 0) {
                            ajaxResponseHandler(response);
                            $(response.data).each(function (i, d) {
                                html += '<tr>\n' +
                                    '       <td>' + d.standard.number + '</td>\n' +
                                    '       <td>' + d.food_category.code + '</td>\n' +
                                    '       <td>' + d.TD + '</td>\n' +
                                    '       <td>' + d.TH + '</td>\n' +
                                    '       <td>' + d.TMS + '</td>\n' +
                                    '       <td>' + d.TFTE_from + '</td>\n' +
                                    '       <td>' + d.TFTE_to + '</td>\n' +
                                    '       <td>' + d.TFTE_value + '</td>\n' +
                                    '    </tr>';
                            });

                            $("#food_standard_sheet").html(html);
                        } else {
                            toastr['error']("No data found.");
                        }
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#energy_standard_id").change(function () {
            var standard_id = $(this).val();

            {{--$.ajax({--}}
            {{--type: "POST",--}}
            {{--url: "{{ route('dataentry.energyfamily.filter') }}",--}}
            {{--data: { id: standard_id },--}}
            {{--cache: true,--}}
            {{--success: function(response){--}}
            {{--var html = '';--}}
            {{--if(response.status == 'success'){--}}
            {{--if(response.sheets.length > 0){--}}
            {{--ajaxResponseHandler(response);--}}
            {{--$(response.data).each( function(i,d) {--}}
            {{--sheets += '<tr>\n' +--}}
            {{--'       <td>'+ d.standard.number +'</td>\n' +--}}
            {{--'       <td>'+ d.food_category.code +'</td>\n' +--}}
            {{--'       <td>'+ d.TD +'</td>\n' +--}}
            {{--'       <td>'+ d.TH +'</td>\n' +--}}
            {{--'       <td>'+ d.TMS +'</td>\n' +--}}
            {{--'     </tr>';--}}
            {{--});--}}

            {{--$("#food_standard_sheet").html(sheets);--}}
            {{--}--}}

            {{--else{--}}
            {{--toastr['error']("No data found.");--}}
            {{--}--}}
            {{--}--}}
            {{--},--}}
            {{--error: function () {--}}
            {{--toastr['error']("Something Went Wrong.");--}}
            {{--}--}}
            {{--});--}}

        });

        $("select.type").change(function () {
            var selectedType = $(this).children("option:selected").val();
            if (selectedType === 'zone') {
                $('#selectZone').css('display', 'block');
                $('#addZone').css('display', 'block');
                $("#selectZone select").prop('required', true);

                $('#selectCountry').css('display', 'none');
                $('#addCountry').css('display', 'none');
                $('#selectCountry select').prop('required', false);
            } else if (selectedType === 'country') {

                $('#selectCountry').css('display', 'block');
                $('#addCountry').css('display', 'block');
                $('#selectCountry select').prop('required', true);

                $('#selectZone').css('display', 'none');
                $('#addZone').css('display', 'none');
                $("#selectZone select").prop('required', false);
            } else {
                $('#selectCountry').css('display', 'none');
                $('#addCountry').css('display', 'none');
                $('#selectCountry select').prop('required', false);
                $('#selectZone').css('display', 'none');
                $('#addZone').css('display', 'none');
                $("#selectZone select").prop('required', false);
            }
        });

    </script>


    {{--
        Start section
        Add Funciton for modal changing if records not exist while adding another on the basis of other modal
    --}}
    <script>

        function addCountry(type) {
            if (type === 'zone') {
                $("#addZoneModal").modal('hide');
            } else if (type === 'region') {
                $("#addRegionModal").modal('hide');
            } else if (type === 'city') {
                $("#addCityModal").modal('hide')
            }

            $("#addCountryModal").modal('show');

        }

        function addZone(type) {
            if (type === 'city') {
                $("#addCityModal").modal('hide');
            }

            $("#addZoneModal").modal('show');
        }

        function addFoodCategory(type) {
            if (type === 'food-category') {
                $("#addFoodSubCategoryModal").modal('hide');
            }
            $("#addFoodCategoryModal").modal('show');
        }

        function addIAFCode(type) {
            if (type === 'iaf-code') {
                $("#addIASModal").modal('hide')
            }
            $("#addIAFModal").modal('show');
        }

        function addStandardFamily(type) {
            if (type === 'standard-family') {
                $("#addStandardsModal").modal('hide')
            }
            $("#addStandardFamilyModal").modal('show')
        }


        $(document).ready(function () {
            $("#iafDataEntryListTable").dataTable({
                "order": [[1, "asc"]],
            });
        });

        function deleteDataEntryIAF(modelId) {
            var val = showConfirmDelete();
            if (val) {
                var url = "{{ route('ajax.data.entry.iaf.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                    modelId);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response) {
                        successResponseHandler(response);
                        if (response.status == "success" || response.status == '200') {
                            $("#uploadIAFModal").modal('hide');
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }

        function getEditDataEntryIAFModal(modelId) {
            var url = "{{ route('ajax.data.entry.iaf.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {

                    $("#uploadIAFModal").modal("hide");
                    $("#ajaxModalContainer").html(response);
                    $('#editDataEntryIAFModal').modal({
                        backdrop: 'static'//to disable click close
                    });

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }


        $(document).ready(function () {
            $("#iasDataEntryListTable").dataTable({
                "order": [[1, "asc"]],
            });
        });

        function deleteDataEntryIAS(modelId) {
            var val = showConfirmDelete();
            if (val) {
                var url = "{{ route('ajax.data.entry.ias.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                    modelId);
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (response) {
                        successResponseHandler(response);
                        if (response.status == "success" || response.status == '200') {
                            $("#uploadIASModal").modal('hide');
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }

        function getEditDataEntryIASModal(modelId) {
            var url = "{{ route('ajax.data.entry.ias.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {

                    $("#uploadIASModal").modal("hide");
                    $("#ajaxModalContainer").html(response);
                    $('#editDataEntryIASModal').modal({
                        backdrop: 'static'//to disable click close
                    });

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

    </script>


    <script>
        $('#accreditation-ias').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#iaf';
            var accreditation_id = $(this).val();
            var request = "accreditation_id=" + accreditation_id;

            if (accreditation_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.companyIafByAccreditation') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.iafs, function (i, obj) {

                                html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            // sortMe($(node_to_modify).find('option'));
                            // $(node_to_modify).prepend("<option value='' selected>Select IAF</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select IAF</option>");
            }

        });
    </script>

    {{--
         End section
         Add Funciton for modal changing if records not exist while adding another on the basis of other modal
    --}}
@endpush


