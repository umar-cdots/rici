<div class="modal codesModal" id="citiesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cites List</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="tables">
                            <table cellspacing="20" cellpadding="0" border="0"
                                   class="table table-striped table-bordered table-bordered2 table4" id="citiesData">
                                <thead>
                                <tr>
                                    <th width="8%">S.No</th>
                                    <th width="30%">Name</th>
                                    <th width="30%">Post Code</th>
                                    <th width="25%">Country</th>
                                    <th width="30%">Zone</th>
                                    {{--                                    <th width="25%">Region</th>--}}
                                    <th align="center">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($cities as $key =>  $item)
                                    <tr id="rowData{{$item->id}}">
                                        <td>{{  $loop->index +1}}</td>
                                        <td>{{ $item->name}}</td>
                                        <td>{{ $item->post_code}}</td>
                                        <td>
                                            @if($item->zoneable_type == 'App\Zone')
                                                {{ $item->zoneable->country->name }}

                                            @else
                                                @if($item->zoneable_type == 'App\Countries')
                                                    {{ $item->zoneable->name }}
                                                @else
                                                    'N/A'
                                                @endif
                                            @endif
                                        </td>
                                        <td>{{ ($item->zoneable_type == 'App\Zone') ? $item->zoneable->name : 'N/A'}}</td>
                                        {{--                                        <td>{{($item->zoneable_type == 'App\Zone') ? $item->zone : ($item->zoneable_type == 'App\Countries') ? $item->zoneable->name : '---'}}</td>--}}
                                        {{--                                        @if($item->zoneable_type == 'App\Zone')--}}
                                        {{--                                            <td>Zone</td>--}}
                                        {{--                                        @else--}}
                                        {{--                                            <td>Country</td>--}}
                                        {{--                                        @endif--}}

                                        {{--                                        <td>{{$item->zoneable->country->region}}</td>--}}

                                        <td align="center">
                                            <ul class="data_list">
                                                @can('edit_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="getEditModal({{$item->id}})">
                                                            <i class="fa fa-pencil-square-o"
                                                               aria-hidden="true"></i>

                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('delete_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="deleteItem({{$item->id}});">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>

                                        </td>
                                    </tr>


                                @endforeach
                                </tbody>

                            </table>
                            {{--                            {{$cities->links()}}--}}
                        </div>


                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">Close</button>
                @can('create_data_entry')
                    <button type="button" class="btn btn-block btn-success btn_save" onclick="cityListModal()"
                            style="width: 175px;"><i class="far
                                                    fa-edit"></i>Add City
                    </button>
                @endcan

            </div>
        </div>

    </div>
</div>


<script>
    function cityListModal() {
        $("#citiesModal").modal('hide');
        $("#addCityModal").modal('show');
    }

    function showConfirmDelete() {
        return confirm("Are You Sure You Want To Delete This Data?");
    }


    function deleteItem(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('data.entry.city.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        $("#rowData" + modelId).remove();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditModal(modelId) {
        var url = "{{ route('data.entry.city.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
            modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                $("#ajaxModalContainer").html(response);
                $('#editCityModal').modal({
                    backdrop: 'static'//to disable click close
                });

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

</script>