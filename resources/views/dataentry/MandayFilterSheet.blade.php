@foreach( $filter_records as $rec)
    <tr>
        <td>{{ $rec->standard_number }}</td>
        <td>{{ $rec->complexity }}</td>
        <td>{{ $rec->frequency }}</td>
        <td>{{ $rec->effective_employes_on }}</td>
        <td>{{ $rec->effective_employes_off }}</td>
        <td>{{ $rec->stage1_on }}</td>
        <td>{{ $rec->stage1_off }}</td>
        <td>{{ $rec->stage2_on }}</td>
        <td>{{ $rec->stage2_off }}</td>
        <td>{{ ($rec->frequency == 'annual') ? $rec->surveillance_on : $rec->surveillance_on }}</td>
        <td>{{ $rec->surveillance_off }}</td>
        <td>{{ $rec->reaudit_on }}</td>
        <td>{{ $rec->reaudit_off }}</td>
    </tr>
@endforeach