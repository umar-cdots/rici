<div class="modal codesModal" id="editCityModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editCityForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit City</h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-8">

                            <div class="form-group">
                                @if($city->zoneable_type == "App\Zone")
                                    <input type="hidden" name="type" value="zone">
                                    <label>Zone</label>
                                    <select name="zone_id" class="form-control">

                                        @foreach($zones as $zone)
                                            <option value="{{$zone->id}}" {{$city->zoneable_id == $zone->id ? 'selected': ''}}>
                                                {{$zone->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                @else
                                    <input type="hidden" name="type" value="country">
                                    <label>Country</label>
                                    <select name="country_id" class="form-control">

                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{$city->zoneable_id == $country->id ? 'selected': ''}}>
                                                {{$country->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>City Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter City Name"
                                       value="{{$city->name}}">
                            </div>
                            <div class="form-group">
                                <label>Post Code</label>
                                <input type="text" name="post_code" class="form-control" placeholder="Enter Post Code"
                                       value="{{$city->post_code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="city_id" value="{{$city->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal" id="cityModalDismiss">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        City
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $('#editCityForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editCityForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.city.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    $("#editCityModal").modal('hide');
                    location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
    $('#cityModalDismiss').click(function(){
        $('.modal-backdrop').hide();
    });


</script>