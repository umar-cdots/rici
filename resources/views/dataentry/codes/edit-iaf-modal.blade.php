
<div class="modal codesModal" id="editDataEntryIAFModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editIAFDataEntryForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit IAF Code</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Enter Title of IAF Code</label>
                                <input type="text" name="name" class="form-control" placeholder="Food Products" value="{{$iaf->name}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Enter IAF Code</label>
                                <input type="text" class="form-control" placeholder="03" name="code" value="{{$iaf->code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="iaf_id" value="{{$iaf->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        IAF Code </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });


    $('#editIAFDataEntryForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editIAFDataEntryForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('ajax.data.entry.iaf.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editDataEntryIAFModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>