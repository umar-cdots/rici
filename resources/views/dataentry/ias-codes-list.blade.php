<div class="modal codesModal" id="listIASModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">IAS Codes List</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        {{--                        <label for="aifCodes"> IAF Codes</label>--}}
                        {{--                        <select name="aif_code" id="aifCodes" class="form-control" onchange="getIASCodes(this.value)">--}}
                        {{--                            <option value="">--Select IAF Code--</option>--}}
                        {{--                            @foreach($iafs as $iaf)--}}
                        {{--                                <option value="{{$iaf->id}}">{{$iaf->code}}. {{$iaf->name}}</option>--}}
                        {{--                            @endforeach--}}
                        {{--                        </select>--}}
                        {{--                        <br>--}}
                        {{--                        <div id="iasCodesListTable">--}}

                        {{--                        </div>--}}

                        <div class="tables">
                            <table cellspacing="20" cellpadding="0" border="0" id="iasListTable"
                                   class="table table-striped table-bordered table-bordered2 table4">
                                <thead>
                                <tr>
                                    <th width="5%">S.No</th>
                                    <th width="20%">Standard</th>
                                    <th width="5%">Accreditation</th>
                                    <th width="25%">IAF Code</th>
                                    <th width="50%">Name</th>
                                    <th width="5%">Code</th>
                                    <th align="center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="BodyFormalEducationTable">


                                @foreach($iafs as $key =>  $item)
                                    <tr id="iasCodesRow{{$item->id}}">
                                        <td>{{$key + 1}}</td>
                                        <td>{{$item->standard->name ?? '--'}}</td>
                                        <td>{{$item->accreditation->name ?? '--'}}</td>
                                        <td>{{$item->iaf->name ?? '--'}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->code}}</td>
                                        <td align="center">
                                            <ul class="data_list">
                                                {{--                                                <li>--}}
                                                {{--                                                    <a href="javascript:void(0)"--}}
                                                {{--                                                       onclick="getEditIASModal({{$item->id}})">--}}
                                                {{--                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>--}}

                                                {{--                                                    </a>--}}
                                                {{--                                                </li>--}}
                                                @can('delete_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="deleteIAS({{$item->id}});">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>

                                        </td>
                                    </tr>


                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">Close</button>
                @can('create_data_entry')
                    <button type="button" class="btn btn-block btn-success btn_save" onclick="iasCodeListModal()"
                            style="width: 180px;"><i class="far
                                                    fa-edit"></i>Add IASCode
                    </button>
                @endcan
                {{--                <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>--}}

            </div>
        </div>

    </div>
</div>


<script>


    $(document).ready(function () {
        $('#iasListTable').dataTable();
    });


    function iasCodeListModal() {
        $("#listIASModal").modal('hide');
        $("#addIASModal").modal('show');
    }

    function deleteIAS(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('data.entry.ias.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        $('#iasCodesRow' + modelId).remove();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditIASModal(modelId) {
        var url = "{{ route('data.entry.ias.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
            modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#listIASModal").modal("hide");
                $("#ajaxModalContainer").html(response);
                $('#editIASModal').modal({
                    backdrop: 'static'//to disable click close
                });

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getIASCodes(iafModel) {

        var url = "{{ route('data.entry.ias.codes.list', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
            iafModel);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#iasCodesListTable").html(response);
                $("#iasListTable").dataTable();
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });

    }

</script>

