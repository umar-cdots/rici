
<div class="modal codesModal" id="editStandardFamilyModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editStandardFamilyForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Standard Family</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" placeholder="Enter Standard Family Name"
                                   class="form-control" value="{{$standardFamily->name}}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>IMS Enabled <br>
                                <input type="hidden" name="is_ims_enabled" value="0">
                                <input type="checkbox" name="is_ims_enabled"
                                       {{$standardFamily->is_ims_enabled == 1 ? 'checked': ''}}
                                       class="icheck-blue" value="1">
                            </label>
                        </div>
                    </div>

                </div>

                <input type="hidden" name="standard_family_id" value="{{$standardFamily->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Standard Family</button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });

    $('#editStandardFamilyForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editStandardFamilyForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.standard.family.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editStandardFamilyModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>