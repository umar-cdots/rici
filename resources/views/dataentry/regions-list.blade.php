<div class="modal codesModal" id="regionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Regions List</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="tables">
                            <table cellspacing="20" cellpadding="0" border="0" id="regionListTable"
                                   class="table table-striped table-bordered table-bordered2 table4">
                                <thead>
                                <tr>
                                    <th width="8%">S.No</th>
                                    <th width="30%">Name</th>
                                    <th width="40%">Countries</th>
                                    <th align="center">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($regions as $key =>  $item)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{!! $item->regionscountries->pluck('name')->implode('<br>')  !!}</td>
                                        <td align="center">
                                            <ul class="data_list">
                                                @can('edit_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="getEditModal({{$item->id}})">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('delete_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="deleteItem({{$item->id}});">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>

                                        </td>
                                    </tr>


                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">Close</button>
                @can('create_data_entry')
                    <button type="button" class="btn btn-block btn-success btn_save" onclick="regionListModal()"
                            style="width: 175px;"><i class="far
                                                    fa-edit"></i>Add Region
                    </button>
                @endcan

            </div>
        </div>

    </div>
</div>


<script>
    function regionListModal() {
        $("#regionsModal").modal('hide');
        $("#addRegionModal").modal('show');
    }

    function deleteItem(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('data.entry.region.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        $("#regionsModal").modal('hide');
                        getRegionsListView();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditModal(modelId) {

        var url = "{{ route('data.entry.region.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                $("#regionsModal").modal("hide");
                $("#ajaxModalContainer").html(response);
                $('#editRegionModal').modal({
                    backdrop: 'static'//to disable click close
                });

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

</script>