<div class="modal codesModal" id="listAccreditationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Accreditations List</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="tables">
                            <table cellspacing="20" cellpadding="0" border="0" id="accreditationListTable"
                                   class="table table-striped table-bordered table-bordered2 table4">
                                <thead>
                                <tr>
                                    <th width="8%">S.No</th>
                                    <th width="20%">Standard Name</th>
                                    <th width="25%">Full Name</th>
                                    <th width="25%">Short Code</th>

                                    <th align="center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="BodyFormalEducationTable">


                                @foreach($accreditations as $key =>  $accreditation)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{ $accreditation->standard->name ?? '--' }}</td>
                                        <td>{{ $accreditation->full_name}}</td>
                                        <td>{{$accreditation->name}}</td>

                                        <td align="center">
                                            <ul class="data_list">
                                                @can('edit_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="getEditAccreditationModal({{$accreditation->id}})">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('delete_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="deleteAccreditation({{$accreditation->id}});">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>

                                        </td>
                                    </tr>


                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">Close</button>

                @can('create_data_entry')
                    <button type="button" class="btn btn-block btn-success btn_save" onclick="accreditationListModal()"
                            style="width: 180px;"><i class="far
                                                    fa-edit"></i>Add Accreditation
                    </button>
                @endcan

            </div>
        </div>

    </div>
</div>


<script>

    function accreditationListModal() {
        $("#listAccreditationModal").modal('hide');
        $("#addAccreditationModal").modal('show');
    }

    function deleteAccreditation(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('data.entry.accreditation.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        $("#listAccreditationModal").modal('hide');
                        getAccreditationListView();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditAccreditationModal(modelId) {
        var url = "{{ route('data.entry.accreditation.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
            modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                $("#listAccreditationModal").modal("hide");
                $("#ajaxModalContainer").html(response);
                $('#editAccreditationModal').modal({
                    backdrop: 'static'//to disable click close
                });

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

</script>