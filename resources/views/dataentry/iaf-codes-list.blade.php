<div class="modal codesModal" id="listIAFModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">IAF Codes List</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="tables">
                            <table cellspacing="20" cellpadding="0" border="0" id="iafListTable"
                                   class="table table-striped table-bordered table-bordered2 table4">
                                <thead>
                                <tr>
                                    <th width="8%">S.No</th>
                                    <th width="8%">Standard</th>
                                    <th width="8%">Accreditation</th>
                                    <th width="45%">Name</th>
                                    <th width="25%">Code</th>
                                    <th align="center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="BodyFormalEducationTable">


                                @foreach($iafs as $key =>  $iaf)
                                    <tr>
                                        <td>{{ $key + 1}}</td>
                                        <td>{{ $iaf->standard->name ?? '--'}}</td>
                                        <td>{{ $iaf->accreditation->name ?? '--'}}</td>
                                        <td>{{ $iaf->name}}</td>
                                        <td>{{ $iaf->code}}</td>

                                        <td align="center">
                                            <ul class="data_list">
                                                {{--                                                <li>--}}
                                                {{--                                                    <a href="javascript:void(0)"--}}
                                                {{--                                                       onclick="getEditIAFModal({{$iaf->id}})">--}}
                                                {{--                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>--}}

                                                {{--                                                    </a>--}}
                                                {{--                                                </li>--}}
                                                @if(Auth::user()->user_type === "scheme_manager")
                                                    <li>
                                                        <input type="checkbox"
                                                               style="width: 15px !important; height: 15px !important;"
                                                               data-iaf-code-id="{{ $iaf->id }}"
                                                               {{ $iaf->iaf_code_status == 1 ? 'checked' : '' }} value="{{ $iaf->iaf_code_status == 1 ? 1 : 0 }}"
                                                               name="iaf_code_status" id="iaf_code_status"
                                                               autocomplete="off">
                                                    </li>
                                                @endif
                                                @can('delete_data_entry')

                                                    <li>
                                                        <a href="javascript:void(0)" onclick="deleteIAF({{$iaf->id}});">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>

                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">Close</button>
                @can('create_data_entry')
                    <button type="button" class="btn btn-block btn-success btn_save" onclick="iafCodeListModal()"
                            style="width: 180px;"><i class="far
                                                    fa-edit"></i>Add IAFCode
                    </button>
                @endcan

            </div>
        </div>

    </div>
</div>


<script>

    function iafCodeListModal() {
        $("#listIAFModal").modal('hide');
        $("#addIAFModal").modal('show');
    }

    function deleteIAF(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('data.entry.iaf.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
                modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        $("#listIAFModal").modal('hide');
                        getIAFListView();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    $(document).on('change', '#iaf_code_status', function () {
        var status = 0;
        if ($(this).is(':checked') === true) {
            $(this).val(1);
            status = 1;
        } else {
            $(this).val(0);
            status = 0;
        }
        var id = $(this).data('iaf-code-id');

        $.ajax({
            type: "GET",
            url: '{{ route('ajax.data.entry.iaf.status') }}',
            data: {
                status: status,
                iaf_id: id
            },
            success: function (response) {
                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    })

    function getEditIAFModal(modelId) {
        var url = "{{ route('data.entry.iaf.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID",
            modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                $("#listIAFModal").modal("hide");
                $("#ajaxModalContainer").html(response);
                $('#editIAFModal').modal({
                    backdrop: 'static'//to disable click close
                });

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

</script>
