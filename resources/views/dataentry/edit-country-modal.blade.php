<div class="modal codesModal" id="editCountryModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editCountryForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Country</h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country Name</label>
                                <input type="text" name="name" class="form-control" placeholder="United Kingdom"
                                       value="{{$country->name}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country Code</label>
                                <input type="text" name="code" class="form-control" placeholder="UK"
                                       value="{{$country->iso}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone Code</label>
                                <input type="text" name="phone_code" class="form-control" placeholder="i.e: +92"
                                       value="{{$country->phone_code}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Currency Name</label>
                                <input type="text" name="currency_name" class="form-control" placeholder="i.e: Rupees"
                                       value="{{$country->currency_name}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Currency Code</label>
                                <input type="text" name="currency_code" class="form-control" placeholder="i.e: PKR"
                                       value="{{$country->currency_code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="country_id" value="{{$country->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Country
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>


    $('#editCountryForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editCountryForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.country.update')}}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    $("#editCountryModal").modal('hide');
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>