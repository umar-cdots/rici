<div class="modal codesModal" id="listStandardFamilyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Standard Family List</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="tables">
                            <table id="ConfidentalityAgreementTable" cellspacing="20" cellpadding="0" border="0"
                                   class="table table-striped table-bordered table-bordered2 table4">
                                <thead>
                                <tr>
                                    <th width="8%">S.No</th>
                                    <th width="">Name</th>
                                    <th width="">IMS Enabled</th>
                                    <th align="center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="BodyFormalEducationTable">


                                @foreach($standardFamilies as $key =>  $standardFamily)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$standardFamily->name}}</td>
                                        <td>@if($standardFamily->is_ims_enabled == 1)
                                                <span class="badge badge-success">Yes</span>
                                            @else
                                                <span class="badge badge-warning">No</span>
                                            @endif
                                        </td>
                                        <td align="center">
                                            <ul class="data_list">
                                                @can('edit_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="getEditStandardFamilyModal({{$standardFamily->id}})">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('delete_data_entry')
                                                    <li>
                                                        <a href="javascript:void(0)"
                                                           onclick="deleteStandardFamily({{$standardFamily->id}});">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                            </ul>

                                        </td>
                                    </tr>


                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>


            </div>
            <div class="modal-footer">

                <button type="button" class="btn info_box_blue" style="color: #fff" data-dismiss="modal">Close</button>
                @can('create_data_entry')
                    <button type="button" class="btn btn-block btn-success btn_save" onclick="standardfamilyModal()"
                            style="width: 180px;"><i class="far
                                                    fa-edit"></i>Add Standard Family
                    </button>
                @endcan

            </div>
        </div>

    </div>
</div>


<script>

    function standardfamilyModal() {
        $("#listStandardFamilyModal").modal('hide');
        $("#addStandardFamilyModal").modal('show');

    }

    function deleteStandardFamily(standardFamilyId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('data.entry.standard.family.delete', ['STANDARD_FAMILY_ID' => "STANDARD_FAMILY_ID"]) }}".replace("STANDARD_FAMILY_ID",
                standardFamilyId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        $("#listStandardFamilyModal").modal('hide');
                        getStandardFamilyListView();
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditStandardFamilyModal(standardFamilyId) {
        var url = "{{ route('data.entry.standard.family.edit.modal', ['STANDARD_FAMILY_ID' => "STANDARD_FAMILY_ID"]) }}".replace("STANDARD_FAMILY_ID",
            standardFamilyId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                $("#listStandardFamilyModal").modal("hide");
                $("#ajaxModalContainer").html(response);
                $('#editStandardFamilyModal').modal({
                    backdrop: 'static'//to disable click close
                });

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

</script>