<div class="row">
    <div class="col-md-12">

        <div class="tables">
            <table cellspacing="20" cellpadding="0" border="0" id="iasDataEntryListTable"
                   class="table table-striped table-bordered table-bordered2 table4">
                <thead>
                <tr>
                    {{--                    <th width="8%">S.No</th>--}}
                    <th width="45%">Name</th>
                    <th width="25%">Code</th>
                    <th align="center">Action</th>
                </tr>
                </thead>
                <tbody id="BodyFormalEducationTable">


                @foreach($ias_nullable as $key =>  $ias)
                    <tr>
                        {{--                        <td>{{$key + 1}}</td>--}}
                        <td>{{ $ias->name}}</td>
                        <td>{{$ias->code}}</td>

                        <td align="center">
                            <ul class="data_list">
                                @can('edit_data_entry')
                                    <li>
                                        <a href="javascript:void(0)" onclick="getEditDataEntryIASModal({{$ias->id}})">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                        </a>
                                    </li>
                                @endcan
                                @can('delete_data_entry')
                                    <li>
                                        <a href="javascript:void(0)" onclick="deleteDataEntryIAS({{$ias->id}});">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </li>
                                @endcan
                            </ul>

                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>

        </div>


    </div>
</div>

