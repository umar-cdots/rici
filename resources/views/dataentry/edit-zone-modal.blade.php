
<div class="modal codesModal" id="editZoneModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editZoneForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Zone</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Country</label>
                                <select name="country_id" class="form-control">

                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" {{$zone->country_id == $country->id ? 'selected': ''}}>
                                            {{$country->name}}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Zone Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Zone Name" value="{{$zone->name}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="zone_id" value="{{$zone->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal" id="zoneModalDismiss">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Zone </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $('#editZoneForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editZoneForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.zone.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editZoneModal").modal('hide');
                  location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
    $('#zoneModalDismiss').click(function(){
        $('.modal-backdrop').hide();
    });

</script>