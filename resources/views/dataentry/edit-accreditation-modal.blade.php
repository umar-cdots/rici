
<div class="modal codesModal" id="editAccreditationModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editAccreditationForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Accreditation</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Standard(s) Family</label>
                                <select name="standards_family_id" id="standard-family-accreditation-edit-accreditation" class="form-control" onchange="updateStandards('standard-family-accreditation-edit-accreditation', 'standard-accreditation-edit-accreditation')">
                                    <option value="">--Select Standard Family--</option>
                                    @foreach($standard_families as $standard_family)
                                        <option value="{{ $standard_family->id }}" {{ $standards_family_id === $standard_family->id ? 'selected' : ''}}>{{ $standard_family->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Standard(s)</label>
                                <select name="standard_id" id="standard-accreditation-edit-accreditation" class="form-control">
                                    <option value="">--Select Standard--</option>
                                    @foreach($standards as $standard)
                                        <option style="display: none" value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Enter Complete Name</label>
                                <input type="text" name="full_name" class="form-control" placeholder="International Accreditation Services"
                                       value="{{$accreditation->full_name}}">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Enter Code</label>
                                <input type="text" class="form-control" placeholder="Enter Code i.e IAS" name="name"  value="{{$accreditation->name}}">
                            </div>
                        </div>
                    </div>

                </div>

                <input type="hidden" name="accreditation_id" value="{{$accreditation->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        Accreditation </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('#standard-family-accreditation-edit-accreditation').trigger('change');

        setTimeout(function(){
            $('#standard-accreditation-edit-accreditation').val({{ $accreditation->standard_id }}).trigger('change')
        }, 200);
    });

    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });



    $('#editAccreditationForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editAccreditationForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.accreditation.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editAccreditationModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>