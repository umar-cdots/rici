
<div class="modal codesModal" id="editIASModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editIASForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit IAS</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Standard(s) Family</label>
                                <select name="standards_family" id="standard-family-ias-edit-ias" class="form-control" onchange="updateStandards('standard-family-ias-edit-ias', 'standard-ias-edit-ias', 'accreditation-ias-edit-ias', 'iaf-edit-ias')">
                                    <option value="">--Select Standard Family--</option>
                                    @foreach($standard_families as $standard_family)
                                        <option value="{{ $standard_family->id }}" {{ $standard_family->id === $ias->standards_family_id ? 'selected' : '' }}>{{ $standard_family->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Standard(s)</label>
                                <select name="standard" id="standard-ias-edit-ias" class="form-control" onchange="updateAccreditations('standard-ias-edit-ias', 'standard-family-ias-edit-ias', 'accreditation-ias-edit-ias', 'iaf-edit-ias')">
                                    <option value="">--Select Standard--</option>
                                    @foreach($standards as $standard)
                                        <option style="display: none" value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Accreditation</label>
                                <select name="accreditation" id="accreditation-ias-edit-ias" class="form-control" onchange="updateIAF('standard-family-ias-edit-ias', 'standard-ias-edit-ias', 'accreditation-ias-edit-ias', 'iaf-edit-ias')">
                                    <option value="">--Select Accreditation--</option>
                                    @foreach($accreditations as $accreditation)
                                        <option style="display: none"
                                                value="{{ $accreditation->id }}"
                                                data-standard-family-id="{{$accreditation->standards_family_id}}"
                                                data-standard-id="{{$accreditation->standard_id}}">
                                            {{ $accreditation->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>IAF Code</label>
                                <select name="iaf_id" class="form-control" id="iaf-edit-ias">
                                    <option value="">--Select IAF Code--</option>
                                    @foreach($iafs as $iaf)
                                        <option style="display: none"
                                                value="{{$iaf->id}}"
                                                data-standard-family-id="{{$iaf->standards_family_id}}"
                                                data-standard-id="{{$iaf->standard_id}}"
                                                data-accreditation-id="{{$iaf->accreditation_id}}">
                                            {{$iaf->code}}. {{$iaf->name}}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Enter Title of IAS Code</label>
                                <input type="text" name="name" class="form-control" placeholder="Food Products" value="{{$ias->name}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Enter IAS Code</label>
                                <input type="text" class="form-control" placeholder="03" name="code" value="{{$ias->code}}">
                            </div>
                        </div>

                    </div>

                </div>

                <input type="hidden" name="ias_id" value="{{$ias->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save">Edit
                        IAS Code </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });

    $(document).ready(function () {
        $('#standard-family-ias-edit-ias').trigger('change');

        setTimeout(function(){
            $('#standard-ias-edit-ias').val({{ $ias->standard_id }}).trigger('change');
            setTimeout(function(){
                $('#accreditation-ias-edit-ias').val({{ $ias->accreditation_id }}).trigger('change');
                setTimeout(function(){
                    $('#iaf-edit-ias').val({{ $ias->iaf_id }})
                }, 100);
            }, 100);
        }, 100);
    });


    $('#editIASForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editIASForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.ias.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                  $("#editIASModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>