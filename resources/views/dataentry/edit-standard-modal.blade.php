<div class="modal codesModal" id="editStandardModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editStandardForm"
              enctype="multipart/form-data">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Standard</h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Standard(s) Family</label>
                                <select name="standards_family_id" class="form-control" required>
                                    <option value="">--Select Standard Family--</option>

                                    @foreach($standard_families as $standard_family)
                                        <option value="{{$standard_family->id}}"
                                                {{$standard->standards_family_id == $standard_family->id ? 'selected': '' }}>
                                            {{$standard_family->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Standard(s) Name</label>
                                <input type="text" name="standard_name" class="form-control"
                                       placeholder="Type Standard's Name"
                                       value="{{$standard->name}}">
                            </div>
                        </div>
                        {{--   <div class="col-md-6">
                               <div class="form-group">
                                   <label>Standard(s) Group</label>
                                   <select name="srndrd_grp" class="form-control">
                                       <option value="">Select</option>
                                   </select>
                               </div>
                           </div>--}}

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year / Version</label>
                                <input type="text" class="form-control" placeholder="Type year/version i.e 9001:2008"
                                       name="standard_number"
                                       value="{{$standard->number}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Active if Checked <br>
                                    <input type="hidden" name="status" value="0">
                                    <input type="checkbox" name="status"
                                           {{$standard->status == 1 ? 'checked': ''}}
                                           class="icheck-blue" value="1">
                                </label>
                            </div>
                        </div>
                    </div>

                </div>

                <input type="hidden" name="standard_id" value="{{$standard->id}}">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary info_box_blue" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn_save"> Update
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });

    $('#editStandardForm').submit(function (e) {
        e.preventDefault();
        var form = $('#editStandardForm')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('data.entry.standard.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    $("#editStandardModal").modal('hide');
                    window.location.reload();
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>
