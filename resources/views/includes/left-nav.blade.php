<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                {{--          <a href=""><img src="{{ asset('img/logo.png') }}" alt="logo" width="100px"></a>--}}
                {{--          <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">--}}
            </div>
            {{--        <div class="info">--}}
            {{--          <a href="#" class="d-block">--}}
            {{--            @if(Auth::check())--}}
            {{--              {{ Auth()->user()->username }}--}}
            {{--              @else--}}
            {{--                username--}}
            {{--            @endif--}}
            {{--          </a>--}}
            {{--        </div>--}}
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @each('includes.nav-item', $menu->menu(), 'item')

                {{-- @include('includes.nav-item') --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<!-- / .Main Sidebar Container -->