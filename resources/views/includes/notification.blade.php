<a class="nav-link" data-toggle="dropdown" href="#">
    <i class="fa fa-bell"></i>
    <span class="badge badge-warning navbar-badge" id="notificationCount">{{$notificationsCount}}</span>
</a>
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <span class="dropdown-item dropdown-header"><span id="notificationCount">{{$notificationsCount}}</span> Notifications</span>
    <div class="scrollNoti">
        @if(isset($notifications))
            @foreach($notifications as $notification)
                <div id="notification{{$notification->id}}">
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void(0)"
                       onclick="isRead('{{$notification->id}}','{{$notification->url}}')"
                       class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                    <span class="float-right text-sm text-muted"><i
                                                class="fa fa-envelope mr-2"></i></span>

                                </h3>
                                <p class="text-sm">{{ $notification->getShortBody() }}</p>
                                <p class="text-sm text-muted"><i
                                            class="fa fa-clock-o mr-1"></i>{{ $notification->created_at }}
                                </p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                </div>

            @endforeach
        @endif
    </div>
    {{--                <a href="{{ route('notifications.index') }}" class="dropdown-item dropdown-footer">See All--}}
    {{--                    Notifications</a>--}}

</div>
