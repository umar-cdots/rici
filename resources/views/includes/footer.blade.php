<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2017-{{ date('Y') }} <a href="{{ config('app.APP_URL') }}">RiciOnline.com</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
    </div>
</footer>
@stack('modals')
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
    document.addEventListener("DOMContentLoaded", function(){
        window.addEventListener('scroll', function() {
            if (window.scrollY > 50) {
                document.getElementById('navbar_top').classList.add('fixed-top');
                // add padding top to show content behind navbar
                navbar_height = document.querySelector('.navbar').offsetHeight;
                document.body.style.paddingTop = navbar_height + 'px';
            } else {
                document.getElementById('navbar_top').classList.remove('fixed-top');
                // remove padding top from body
                document.body.style.paddingTop = '0';
            }
        });
    });
</script>


<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>


{{--Validation --}}
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>


<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->

<!-- Slimscroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('js/toastr.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.js') }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/demo.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'csrftoken': '{{ csrf_token() }}',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("form :input").attr("autocomplete", "off");
    $(document).ajaxStart(function (e) {
        $('#loader').show();
    });

    $(document).ajaxStop(function () {
        $('#loader').hide();
    });

    $('form').submit(function () {
        $('.text-danger').removeClass('text-danger');
        $('.is-invalid').removeClass('is-invalid');
        $('small.text-danger').remove();
    });

    function ajaxResponseHandler(response, form) {
        if (response.status == "success" || response.status == '200') {
            $(form).parent().find('.text-danger').removeClass('text-danger');
            $(form).parent().find('.is-invalid').removeClass('is-invalid');
            $(form).parent().find('small.ajax-error-text').remove();
            if (response.message != '')
                toastr['success'](response.message);
        } else {
            $(form).parent().find('.text-danger').removeClass('text-danger');
            $(form).parent().find('.is-invalid').removeClass('is-invalid');
            $(form).parent().find('small.ajax-error-text').remove();
            if (response.message != '')
                toastr['error'](response.message);
            console.log(response.data);
            $.each(response.data, function (elm, errors) {
                $.each(errors, function (k, v) {
                    var splited = elm.split('.');
                    if (splited.length > 1) {
                        if (parseInt(splited[1]) >= 0 && typeof splited[2] != 'undefined') {
                            var node_index = parseInt(splited[1]);
                            var node = $(form).find('[name="' + splited[0] + '[][' + splited[2] + ']"]')[node_index];
                        } else if (parseInt(splited[2]) >= 0) {
                            var node_index = parseInt(splited[2]);
                            var node = $(form).find('[name="' + splited[0] + '[' + splited[1] + '][]"]')[node_index];
                        } else {
                            var node_index = parseInt(splited[1]);
                            var node = $(form).find('[name="' + splited[0] + '[]"]')[node_index];
                        }
                    } else {
                        var node = $(form).find('[name="' + elm + '"]');
                    }
                    $(node).prev().addClass('text-danger');
                    $(node).addClass('is-invalid');
                    $(node).find('small.ajax-error-text').remove();
                    // $(node).parent().children('<small class="text-danger ajax-error-text">' + v + '</small>');
                });
            });
        }
    }


    function successResponseHandler(response) {
        if (response.status == "success" || response.status == '200') {
            if (response.message != '')
                toastr['success'](response.message);
        } else {
            if (response.message != '')
                toastr['error'](response.message);
        }
    }

    function sortMe(element) {
        var options = $(element);
        var arr = options.map(function (_, o) {
            return {t: $(o).text(), v: o.value};
        }).get();
        arr.sort(function (o1, o2) {
            return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
        });
        options.each(function (i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
        });
    }

    if (typeof toastr != "undefined") {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }
    @if(session('flash_status') && session('flash_message'))
        toastr['{{ session('flash_status') }}']("{{ session('flash_message') }}");
    @endif
            @if(session()->has('success'))
        toastr["{{ session()->get('success') }}"]("{{ session()->get('success') }}");
    @endif
            @if(session()->has('status'))
        toastr["{{ session()->get('status') }}"]("{{ session()->get('message') }}");
    @endif
    $(document).ready(function () {
        $('.icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '15%' // optional
        });
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upld').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLMany(input, id) {
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#upld' + id).html(html);
    }

    $(".propic").change(function () {
        readURL(this);
    });

    /*showing confirm cancel popup box*/
    function showConfirmDelete() {
        return confirm("Are You Sure You Want To Delete This Data?");
    }
</script>
@stack('scripts')
