<a class="nav-link" data-toggle="dropdown" href="#">
    Packs Inprocess <i class="fa fa-bell"></i>
    <span class="badge badge-warning navbar-badge"
          id="notificationCount"></span>
</a>
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <div class="scrollNoti">
        @if(isset($sentPostAuditNotifications))
            @foreach($sentPostAuditNotifications as $notification)
                @php
                    $approvedPostAudit = \App\Notification::where('request_id',$notification->request_id)->where('type' ,'post_audit')->get()->last();

                @endphp

                @if($approvedPostAudit->status != 'approved' && $notification->request_id == $approvedPostAudit->request_id )
                <div id="notification{{$notification->id}}">
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void(0)"
                       onclick="isRead('{{$notification->id}}','{{$notification->url}}')"
                       class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    {{ ucfirst(str_replace('_',' ',$notification->type)) }}
                                    <span class="float-right text-sm text-muted"><i
                                                class="fa fa-envelope mr-2"></i></span>

                                </h3>
                                <p class="text-sm">{{ $notification->getShortBody() }}</p>
                                <p class="text-sm text-muted"><i
                                            class="fa fa-clock-o mr-1"></i>{{ $notification->created_at }}
                                </p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                </div>

                @endif
            @endforeach
        @endif
    </div>

</div>
