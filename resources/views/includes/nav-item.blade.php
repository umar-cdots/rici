@if (is_array($item) && !empty($item['text']))
    @if($item['text'] == 'Notifications')
        @if(isset($item['submenu']))
            <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-others')) ? 'active' : '' }} {{ (request()->is('sent-notifications-post-audit')) ? 'active' : '' }} {{ (request()->is('sent-notifications-post-audit-approved')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                <a href="{{ route('notifications.sent') }}"
                   class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-others')) ? 'active' : '' }} {{ (request()->is('sent-notifications-post-audit')) ? 'active' : '' }} {{ (request()->is('sent-notifications-post-audit-approved')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                    <i class="nav-icon fa fa-circle-o"></i>
                    <p>Tasks Sent</p>
                    @if (isset($item['submenu']))
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    @endif
                </a>
                @if(isset($item['submenu']))
                    <ul class="nav nav-treeview {{ $item['submenu_class'] ?? '' }}"
                        style="margin-left: 10px;display: {{ (request()->is('sent-notifications-others')) ? 'block' : '' }} {{ (request()->is('sent-notifications-post-audit')) ? 'block' : '' }} {{ (request()->is('sent-notifications-post-audit-approved')) ? 'block' : '' }}">
                        @if(auth()->user()->user_type != 'scheme_coordinator')
                            <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-others')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                                <a href="{{ route('notifications.sent.others') }}"
                                   class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-others')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                                    <i class="nav-icon fa fa-bell"></i>
                                    <p>AJs</p>
                                </a>
                            </li>
                        @endif


                        <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-post-audit')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                            <a href="{{ route('notifications.sent.post.audit') }}"
                               class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-post-audit')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                                <i class="nav-icon fa fa-bell"></i>
                                <p>Audit Packs InProcess</p>
                            </a>
                        </li>
                        @if(auth()->user()->user_type == 'scheme_manager')
                            <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-post-audit-approved')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                                <a href="{{ route('notifications.sent.post.audit.approved') }}"
                                   class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('sent-notifications-post-audit-approved')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                                    <i class="nav-icon fa fa-bell"></i>
                                    <p>Audit Packs Approved</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                @endif
            </li>
            <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-others')) ? 'active' : '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }} {{ (request()->is('received-notifications-post-audit-approved')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                <a href="{{ route('notifications.received') }}"
                   class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-others')) ? 'active' : '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }} {{ (request()->is('received-notifications-post-audit-approved')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                    <i class="nav-icon fa fa-circle-o"></i>
                    <p>Tasks Received</p>
                    @if (isset($item['submenu']))
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    @endif
                </a>
                @if(isset($item['submenu']))
                    <ul class="nav nav-treeview {{ $item['submenu_class'] ?? '' }}"
                        style="margin-left: 10px; display: {{ (request()->is('received-notifications-others')) ? 'block' : '' }} {{ (request()->is('received-notifications-post-audit')) ? 'block' : '' }} {{ (request()->is('received-notifications-post-audit-approved')) ? 'block' : '' }}">
                        <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-others')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                            <a href="{{ route('notifications.received.others') }}"
                               class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-others')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                                <i class="nav-icon fa fa-bell"></i>
                                @if(auth()->user()->user_type == 'scheme_coordinator')
                                    <p>Drafts</p>
                                @else
                                    <p>AJs</p>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                            <a href="{{ route('notifications.received.post.audit') }}"
                               class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                                <i class="nav-icon fa fa-bell"></i>
                                <p>Audit Packs InProcess</p>
                            </a>
                        </li>
                        @if(auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator')
                            <li class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit-approved')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                                <a href="{{ route('notifications.received.post.audit.approved') }}"
                                   class="nav-link {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit-approved')) ? 'active' : '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                                    <i class="nav-icon fa fa-bell"></i>
                                    <p>Audit Packs Approved</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                @endif
            </li>
        @endif
    @else
        <li class="nav-item {{ $item['class'] ?? '' }} has-treeview {{-- menu-open --}}">
            <a href="{{ $item['href'] ?? $item['url'] ?? '#' }}"
               class="nav-link {{ $item['class'] ?? '' }}" {{ isset($item['target']) ? 'target="'. $item['target'] .'"' : '' }}>
                <i class="nav-icon fa fa-{{ isset($item['icon']) ? $item['icon'] : 'circle-o' }}"></i>
                <p>{{ $item['text'] ?? '' }}</p>
                @if (isset($item['label']))
                    <span class="pull-right-container">
<span class="label label-{{ isset($item['label_color']) ? $item['label_color'] : 'primary' }} pull-right">{{ $item['label'] }}</span>
</span>
                @elseif (isset($item['submenu']))
                    <span class="pull-right-container">
<i class="fa fa-angle-left pull-right"></i>
</span>
                @endif
            </a>
        </li>


    @endif

@endif

