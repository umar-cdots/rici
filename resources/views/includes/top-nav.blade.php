<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom" id="navbar_top">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li>
            <a href=""><img src="{{ asset('img/logo.png') }}" alt="logo" width="100px"></a>
{{--            <h1 style="color: red">Testing Server</h1>--}}
        </li>

    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        @if(auth()->user()->user_type == 'scheme_manager')
            <li id="bellNotificationsAJ"
                class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-others')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">

                @include('includes.notification-aj')
            </li>
            <li id="bellNotificationsPack"
                class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                @include('includes.notification-pack')
            </li>
            <li id="bellNotificationsDraft"
                class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                @include('includes.notification-draft-operations')
            </li>
            <li id="bellNotificationsDraft"
                class="nav-item {{ $item['class'] ?? '' }} {{ (request()->is('received-notifications-post-audit')) ? 'active' : '' }} has-treeview {{-- menu-open --}}">
                @include('includes.notification-pack-inprocess-operations')
            </li>


        @else
            <li class="nav-item dropdown" id="bellNotifications">
                @include('includes.notification')
            </li>
        @endif


        <li class="dropdown user user-menu userProfileSettings open">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                @if(auth()->user()->user_type == 'operation_manager')
                    <img src="{{ (auth()->user()->operation_manager) ? asset('/uploads/operation_manager/'.auth()->user()->operation_manager->profile_image): asset('img/user2-160x160.jpg') }}"
                         class="user-image" alt="User Image"
                         style="width: 2.1rem; height: auto;     border-radius: 50%;">
                @elseif(auth()->user()->user_type == 'operation_coordinator')
                    <img src="{{ (auth()->user()->operation_coordinator) ? asset('/uploads/operation_coordinator/'.auth()->user()->operation_coordinator->profile_image): asset('img/user2-160x160.jpg') }}"
                         class="user-image" alt="User Image"
                         style="width: 2.1rem; height: auto;     border-radius: 50%;">
                @elseif(auth()->user()->user_type == 'scheme_manager')
                    <img src="{{ (auth()->user()->scheme_manager) ? asset('/uploads/scheme_managers/'.auth()->user()->scheme_manager->profile_image): asset('img/user2-160x160.jpg') }}"
                         class="user-image" alt="User Image"
                         style="width: 2.1rem; height: auto;     border-radius: 50%;">
                @elseif(auth()->user()->user_type == 'scheme_coordinator')
                    <img src="{{ (auth()->user()->scheme_coordinator) ? asset('/uploads/scheme_coordinator/'.auth()->user()->scheme_coordinator->profile_image): asset('img/user2-160x160.jpg') }}"
                         class="user-image" alt="User Image"
                         style="width: 2.1rem; height: auto;border-radius: 50%;">
                @elseif(auth()->user()->user_type == 'auditor')
                    <img src="{{ (auth()->user()->auditor) ? asset('/uploads/auditors/'.auth()->user()->auditor->profile_pic): asset('img/user2-160x160.jpg') }}"
                         class="user-image" alt="User Image"
                         style="width: 2.1rem; height: auto;border-radius: 50%;">
                @elseif(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'data_entry' || auth()->user()->user_type == 'marketing' || auth()->user()->user_type == 'management')
                    <img src="{{ asset('img/user2-160x160.jpg') }}" class="user-image" alt="User Image"
                         style="width: 2.1rem; height: auto;    border-radius: 50%;">
                @else
                @endif

                <span class="hidden-xs">{{ auth()->user()->username }}</span>
            </a>
            <ul class="dropdown-menu">
                <!-- Menu Footer-->
                <li>
                    <a href="{{ route('profile.index') }}" class="" style="border-radius: 60px ;    cursor: pointer;">Profile</a>
                </li>
                <li>
                    <form action="{{ route('logout') }}" method="post" style=" padding-top: 7px;">
                        @csrf
                        <button class="" style="border-radius: 60px ;    cursor: pointer;">Logout</button>
                    </form>
                </li>

            </ul>
        </li>


        <li>

        </li>
    </ul>
</nav>
<!-- /.navbar -->


<script>

    function isRead(id, url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('ajax.notification.is-read') }}",
            data: {notification_id: id},
            success: function (response) {
                if (response.status == "success" && response.action == "true") {
                    var notificationCount = parseInt($('#notificationCount').text());
                    $('#notificationCount').text(notificationCount - 1);
                    $('#notification' + response.data).remove();
                    window.location.href = url;
                } else if (response.status == "success" && response.action == "false") {
                    window.location.href = url;

                }
            },
            error: function (error) {
                toastr['error']('something went wrong');
            }
        });
    }

    function updateNotificationBell() {
        $('#loader').hide();
        $.ajax({
            type: "GET",
            url: "{{ route('ajax.update.notification.bell') }}",
            beforeSend: function () {
                console.log('Ajax call started');
                $('#loader').hide();
            },
            success: function (response) {
                if (response.status == "success") {
                    $('#bellNotifications').html(response.data.html);
                }

            },
            error: function (error) {
                toastr['error']('something went wrong');
            }
        });
    }

    function updateNotificationBellAJ() {
        $('#loader').hide();
        $.ajax({
            type: "GET",
            url: "{{ route('ajax.update.notification.bell.aj') }}",
            beforeSend: function () {
                console.log('Ajax call started');
                $('#loader').hide();
            },
            success: function (response) {
                if (response.status == "success") {
                    $('#bellNotificationsAJ').html(response.data.html);
                }

            },
            error: function (error) {
                toastr['error']('something went wrong');
            }
        });
    }

    function updateNotificationBellPostAudit() {
        $('#loader').hide();
        $.ajax({
            type: "GET",
            url: "{{ route('ajax.update.notification.bell.post.audit') }}",
            beforeSend: function () {
                console.log('Ajax call started');
                $('#loader').hide();
            },
            success: function (response) {
                if (response.status == "success") {
                    $('#bellNotificationsPack').html(response.data.html);
                }

            },
            error: function (error) {
                toastr['error']('something went wrong');
            }
        });
    }

    setInterval(function () {
        @if(auth()->user()->user_type == 'scheme_manager')
        updateNotificationBellAJ();
        updateNotificationBellPostAudit();
        @else
        updateNotificationBell();
        @endif
    }, 20000);
</script>
