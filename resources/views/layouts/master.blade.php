@extends('layouts.app')

@section('pre_content')
	@include('includes.top-nav')
	@include('includes.left-nav')
@endsection

@section('main_content')
	@yield('content')
@endsection

@section('post_content')
	@include('includes.footer')
@endsection