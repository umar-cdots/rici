@extends('layouts.master')
@if($letter_type === 'letter1')
    @section('title', "Intimation Letter 1 & Invoice Page")
@elseif($letter_type === 'letter2')
    @section('title', "Intimation Letter 2 & Invoice Page")
@else
    @section('title', "Intimation Letter 3 & Invoice Page")
@endif

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }


    .disabledDiv {
        pointer-events: none;
        opacity: 1.4;
    }

    #invoice_paid {
        width: 10%;
        height: 40%;
    }
</style>
<style>
    .largerCheckbox{
        width: 40px;
        height: 40px;
    }
</style>
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Intimation @if($letter_type === 'letter1')
                            Letter 1
                        @elseif($letter_type === 'letter2')
                            Letter 2
                        @else
                            Letter 3
                        @endif & Invoice Page</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('invoice.letter.store',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id,$letter_type]) }}"
                          method="POST"
                          id="letterSubmit"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="invoice_number">Invoice Number</label>
                                        <input type="text" class="form-control" placeholder="Enter Invoice Number"
                                               value="{{ substr($aj->job_number,4) }}/{{date('d/m/y')}}"
                                               id="invoice_number" name="invoice_number">

                                    </div>
                                </div>
                                <div class="col-md-4 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="audit_type">Audit type</label>
                                        <input type="text" class="form-control" placeholder="Enter Audit Type"
                                               value="{{ str_replace('_',' ',ucfirst($aj->audit_type) )}}"
                                               id="audit_type" name="audit_type">
                                    </div>
                                </div>
                                @php
                                    if($companyStandard->is_ims == true) {
                                        $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                        $company = \App\Company::where('id', $company->id)->first();
                                        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                        $ims_heading = $companyStandard1->standard->name;
                                          if($aj->audit_type === 'reaudit'){
                                              $invoiceAmount = $companyStandard->re_audit_amount;
                                          } else{
                                              $invoiceAmount = $companyStandard->surveillance_amount;
                                          }
                                          if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                              foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                  if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                  continue;
                                                  } else {
                                                  $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                      if($aj->audit_type === 'reaudit'){
                                                         $invoiceAmount  = $invoiceAmount + $imsCompanyStandard->re_audit_amount;
                                                      }else{
                                                          $invoiceAmount = $invoiceAmount + $imsCompanyStandard->surveillance_amount;
                                                      }
                                                  }
                                              }
                                          }

                                        $standardName = $ims_heading;
                                    } else{
                                        $standardName = $companyStandard->standard->name;
                                        if($aj->audit_type === 'reaudit'){
                                              $invoiceAmount = $companyStandard->re_audit_amount;
                                        } else {
                                              $invoiceAmount = $companyStandard->surveillance_amount;
                                        }
                                    }
                                @endphp
                                <div class="col-md-4 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="standard">Standard</label>
                                        <input type="text" class="form-control" placeholder="Enter Standard"
                                               value="{{$standardName}}"
                                               id="standard" name="standard">
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="company_name">Company Name</label>
                                        <input type="text" class="form-control" placeholder="Enter Company Name"
                                               value="{{ $company->name }}"
                                               id="company_name" name="company_name">
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="city">City *</label>
                                        <input type="text" class="form-control" placeholder="Enter City" required
                                               value="{{ $company->city ? $company->city->name : '' }}"
                                               id="city" name="city">

                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="contact_name">Contact Name *</label>
                                        @php
                                            $titles = ['Mr','Mrs','Ms','Dr'];
                                        @endphp
                                        <div class="row">
                                            <div class="col-md-3">
                                                <select class="form-control" id="title" name="title" required>

                                                    @foreach($titles as $title)
                                                        <option value="{{$title}}" {{$company->primaryContact &&  $company->primaryContact->title === $title  ? 'selected' : ''}}>{{$title}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" placeholder="Enter Contact Name"
                                                       required
                                                       value="{{ $company->primaryContact ? $company->primaryContact->name : '' }}"
                                                       id="contact_name" name="contact_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="designation">Designation *</label>
                                        <input type="text" class="form-control" placeholder="Enter Designation" required
                                               value="{{ $company->primaryContact ? $company->primaryContact->position : '' }}"
                                               id="designation" name="designation">

                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="phone_number">Phone number *</label>
                                        <input type="text" class="form-control" placeholder="Enter Phone number"
                                               required
                                               value="{{ $company->primaryContact ? $company->primaryContact->contact : '' }}"
                                               id="phone_number" name="phone_number">

                                    </div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input type="text" class="form-control" placeholder="Enter Email" required
                                               value="{{ $company->primaryContact ? $company->primaryContact->email : '' }}"
                                               id="email" name="email">

                                    </div>
                                </div>
                                <input type="hidden" value="{{ $companyStandard->surveillance_currency }}"
                                       name="invoice_unit">
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="invoice_amount">Invoice Amount
                                            ({{ $companyStandard->surveillance_currency }}) *</label>
                                        <input type="text" class="form-control" placeholder="Enter Invoice Amount"
                                               required
                                               value="{{$invoiceAmount}}"
                                               id="invoice_amount" name="invoice_amount">

                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2 floting">
                                    <div class="form-group">
                                        <label for="tax_authority">Tax Authority *</label>
                                        <select class="form-control" id="tax_authority" name="tax_authority" required>
                                            <option value="">Select Tax Authority</option>
                                            @foreach($taxAuthorities as $taxAuthority)
                                                <option value="{{$taxAuthority}}">{{$taxAuthority}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <label for="tax_percentage">Tax % *</label>
                                        <select class="form-control" id="tax_percentage" name="tax_percentage" required>
                                            <option value="">Select Tax %</option>
                                            @foreach($taxPercentages as $taxPercentage)
                                                <option value="{{$taxPercentage}}">{{$taxPercentage}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <label for="inflation_percentage">Inflation % *</label>
                                        <select class="form-control" id="inflation_percentage" name="inflation_percentage" required>
                                            <option value="">Select Inflation %</option>
                                            @foreach($inflationPercentages as $inflationPercentage)
                                                <option value="{{$inflationPercentage}}">{{$inflationPercentage}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="invoice_date">Invoice Date *</label>
                                        <input type="text" name="invoice_date"
                                               class="form-control invoice_date"
                                               id="invoice_date"
                                               value="{{ date('d-m-Y') }}"
                                               placeholder="Enter Invoice Date">
                                    </div>
                                </div>
                                <div class="col-md-3 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="due_date">Audit Due Date</label>
                                        <input type="text" name="due_date"
                                               class="form-control due_date"
                                               id="due_date"
                                               value="{{ date('d-m-Y',strtotime($aj->excepted_date)) }}"
                                               placeholder="Enter Due Date">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 floting">
                                <label>Attachment File <span>Max 500kb (.docx or .pdf file formats only)</span></label>
                                <div class="input-group form-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="attached_file" required
                                               id="exampleInputFile3" onchange="readURLMany(this,3)">
                                        <label class="custom-file-label" for="exampleInputFile3">Choose
                                            file</label>
                                    </div>
                                    <div class="input-group-append">
                                    </div>
                                </div>
                                <div class="image form-group" id="file">

                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <label for="certificate_date">Attached Audit Invoice ?</label>
                                <div class="input-group">
                                    <input type="checkbox" class="float-right largerCheckbox" style="width: 30px !important; height: 30px !important;" checked value="1"
                                           name="is_attached_audit_plan" id="is_attached_audit_plan"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-8"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate Invoice
                                    </button>

                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <a href="{{ route('invoice.letter.index',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id]) }}"
                                       class="btn btn-block btn-danger  step1Savebtn">
                                        Cancel
                                    </a>

                                </div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <style>
        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top {
            width: 25% !important;
            margin: 0 auto !important;
        }
    </style>
@endpush

@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $('#invoice_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('#due_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        function readURLMany(input, id = null) {
            if (input.id == 'exampleInputFile3') {
                var filename = input.files[0].name;
                var html = '';
                if (input.files && input.files[0]) {
                    html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
                }
                $('#file').html(html);
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#upld' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }
        $(document).on('change', '#is_attached_audit_plan', function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
        })
    </script>

@endpush


