@extends('layouts.master')
@section('title', "Advance Intimation Letters & Invoices")

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }


    .disabledDiv {
        pointer-events: none;
        opacity: 1.4;
    }

    #invoice_paid {
        width: 10%;
        height: 40%;
    }

    .cross-icon {
        display: inline-block;
        float: right;
        font-weight: bold;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Advance Intimation Letters & Invoices
                        <a class="cross-icon"
                           href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit') }}">X</a>
                    </h3>

                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('invoice.letter.paid',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id]) }}"
                          method="POST"
                          id="letterSubmit"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-4 @if(!is_null($invoice) && $invoice->invoice_status === 1) disabledDiv @endif">
                                <div class="form-group">
                                    @if(!is_null($invoice) && $invoice->letter_one_print === 1)
                                        <a href="{{ route('invoice.letter.edit', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, 'letter1', $invoice->id, 'edit']) }}"
                                           class="btn btn-block btn-primary">
                                            Letter 1
                                        </a>
                                    @else
                                        <a href="{{ route('invoice.letter.create',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id,'letter1']) }}"
                                           class="btn btn-block btn-primary">
                                            Letter 1
                                        </a>
                                    @endif

                                </div>
                            </div>
                            <div class="col-md-4 @if(!is_null($invoice) && $invoice->invoice_status === 1) disabledDiv @elseif(!is_null($invoice) && $invoice->letter_one_print === 0) disabledDiv @elseif(!is_null($invoice) && $invoice->letter_one_print === 1) @elseif(is_null($invoice)) disabledDiv  @endif">
                                <div class="form-group">
                                    @if(!is_null($invoice))

                                        <a href="{{ route('invoice.letter.edit', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, 'letter2', $invoice->id, 'edit']) }}"
                                           class="btn btn-block btn-primary">
                                            Letter 2
                                        </a>
                                    @else
                                        <a href="javascript:void(0)"
                                           class="btn btn-block btn-primary">
                                            Letter 2
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 @if(!is_null($invoice) && $invoice->invoice_status === 1) disabledDiv @elseif(!is_null($invoice) && $invoice->letter_two_print === 0) disabledDiv @elseif(!is_null($invoice) && $invoice->letter_two_print === 1) @elseif(is_null($invoice)) disabledDiv  @endif">
                                <div class="form-group">
                                    @if(!is_null($invoice))
                                        <a href="{{ route('invoice.letter.edit', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, 'letter3', $invoice->id, 'edit']) }}"
                                           class="btn btn-block btn-primary">
                                            Letter 3
                                        </a>
                                    @else
                                        <a href="javascript:void(0)"
                                           class="btn btn-block btn-primary">
                                            Letter 3
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row" id="chboxes">

                            <div class="col-md-3 floting @if(!is_null($invoice) && $invoice->invoice_status === 1) disabledDiv @endif">
                                <input type="checkbox" class=""
                                       name="invoice_paid" id="invoice_paid"
                                       value="@if(!is_null($invoice) && $invoice->invoice_status === 1) 1 @else 0 @endif"
                                       @if(!is_null($invoice) && $invoice->invoice_status === 1) checked @endif>
                                <label style="font-size: 20px;">Invoice Paid</label>
                            </div>
                            <div id="ifInvoiceYes"
                                 style="display:  @if(!is_null($invoice) && $invoice->invoice_status === 0) none @elseif(!is_null($invoice) && $invoice->invoice_status === 1) @elseif(is_null($invoice)) none @endif;width: 100%">
                                @if(!is_null($invoice) && $invoice->invoice_status === 0)
                                    <div class="col-md-2 floting">
                                        <div class="form-group">

                                            <button type="button"
                                                    class="btn btn-block btn-secondary btn_save step1Savebtn">
                                                Save
                                            </button>

                                        </div>
                                    </div>
                                    <div class="col-md-2 floting">
                                        <div class="form-group">

                                            <a href="{{ route('invoice.letter.index',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id]) }}"
                                               class="btn btn-block btn-danger">
                                                Cancel
                                            </a>

                                        </div>
                                    </div>
                                @elseif(!is_null($invoice) && $invoice->invoice_status === 1)
                                @elseif(is_null($invoice))
                                    <div class="col-md-2 floting">
                                        <div class="form-group">

                                            <button type="button"
                                                    class="btn btn-block btn-secondary btn_save step1Savebtn">
                                                Save
                                            </button>

                                        </div>
                                    </div>
                                    <div class="col-md-2 floting">
                                        <div class="form-group">

                                            <a href="{{ route('invoice.letter.index',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id]) }}"
                                               class="btn btn-block btn-danger">
                                                Cancel
                                            </a>

                                        </div>
                                    </div>
                                @endif

                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>

</style>
@push('scripts')

    <script>

        $('#invoice_paid').on('change', function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
                $('#ifInvoiceYes').show();
            } else {
                $(this).val(0);
                $('#ifInvoiceYes').hide();
            }
        })
        $(document).on('click', '.step1Savebtn', function () {
            var r = confirm("Are you sure you want to save invoice as paid. This action can't be undone once saved and all letters will be disabled");
            if (r === true) {
                $('form#letterSubmit').submit();
            } else {
                return false;
            }
        })

    </script>
@endpush


