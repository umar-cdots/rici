@extends('layouts.master')
@if($letter_type === 'letter1')
    @section('title', "Intimation Letter 1 & Invoice Page")
@elseif($letter_type === 'letter2')
    @section('title', "Intimation Letter 2 & Invoice Page")
@else
    @section('title', "Intimation Letter 3 & Invoice Page")
@endif

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }


    .disabledDiv {
        pointer-events: none;
        opacity: 1.4;
    }

    #invoice_paid {
        width: 10%;
        height: 40%;
    }

    .direct-chat-name {
        font-weight: 500 !important;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Intimation @if($letter_type === 'letter1')
                            Letter 1
                        @elseif($letter_type === 'letter2')
                            Letter 2
                        @else
                            Letter 3
                        @endif & Invoice Page</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('invoice.letter.update',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id,$letter_type,$invoice->id]) }}"
                          method="POST"
                          id="letterSubmit"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 disabledDiv">
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="invoice_number">Invoice Number</label>
                                        <input type="text" class="form-control" placeholder="Enter Invoice Number"
                                               value="{{$invoice->invoice_number}}"
                                               id="invoice_number" name="invoice_number">
                                    </div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="audit_type">Audit type</label>
                                        <input type="text" class="form-control" placeholder="Enter Audit Type"
                                               value="{{$invoice->audit_type}}"
                                               id="audit_type" name="audit_type">
                                    </div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="standard">Standard</label>
                                        <input type="text" class="form-control" placeholder="Enter Standard"
                                               value="{{$invoice->standard}}"
                                               id="standard" name="standard">
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 disabledDiv">
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="company_name">Company Name</label>
                                        <input type="text" class="form-control" placeholder="Enter Company Name"
                                               value="{{$invoice->company_name}}"
                                               id="company_name" name="company_name">
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="city">City *</label>
                                        <input type="text" class="form-control" placeholder="Enter City" required
                                               value="{{ $company->city ? $company->city->name : '' }}"
                                               id="city" name="city">

                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="contact_name">Contact Name</label>
                                        <input type="text" class="form-control" placeholder="Enter Contact Name"
                                               value="{{$invoice->contact_name}}"
                                               id="contact_name" name="contact_name">
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="designation">Designation</label>
                                        <input type="text" class="form-control" placeholder="Enter Designation"
                                               value="{{$invoice->designation}}"
                                               id="designation" name="designation">
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="phone_number">Phone number</label>
                                        <input type="text" class="form-control" placeholder="Enter Phone number"
                                               value="{{$invoice->phone_number}}"
                                               id="phone_number" name="phone_number">
                                    </div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input type="text" class="form-control" placeholder="Enter Email"
                                               value="{{$invoice->email}}"
                                               id="email" name="email">
                                    </div>
                                </div>
                                <div class="col-md-4 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="invoice_amount">Invoice Amount
                                            ({{ $companyStandard->surveillance_currency }})</label>
                                        <input type="text" class="form-control" placeholder="Enter Invoice Amount"
                                               value="{{$invoice->invoice_amount}}"
                                               id="invoice_amount" name="invoice_amount">
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="tax_authority">Tax Authority</label>
                                        <select class="form-control" id="tax_authority" name="tax_authority" required>
                                            <option value="">Select Tax Authority</option>
                                            @foreach($taxAuthorities as $taxAuthority)
                                                <option value="{{$taxAuthority}}" {{ $invoice->tax_authority === $taxAuthority ? 'selected' : '' }}>{{$taxAuthority}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 floting disabledDiv">
                                    <div class="form-group">
                                        <label for="tax_percentage">Tax %</label>
                                        <select class="form-control" id="tax_percentage" name="tax_percentage" required>
                                            <option value="">Select Tax %</option>
                                            @foreach($taxPercentages as $taxPercentage)
                                                <option value="{{$taxPercentage}}" {{ (int)$invoice->tax_percentage === (int)$taxPercentage ? 'selected' : '' }}>{{$taxPercentage}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 floting disabledDiv">
                                    <div class="form-group">

                                        <label for="inflation_percentage">Inflation % *</label>
                                        <select class="form-control" id="inflation_percentage" name="inflation_percentage" required>
                                            <option value="">Select Inflation %</option>
                                            @foreach($inflationPercentages as $inflationPercentage)
                                                <option value="{{$inflationPercentage}}" {{ (int)$invoice->inflation_percentage === (int)$inflationPercentage ? 'selected' : '' }}>{{$inflationPercentage}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                @if($letter_type === 'letter1')
                                    <div class="col-md-3 floting disabledDiv">
                                        <div class="form-group">
                                            <label for="invoice_date">Invoice Date</label>
                                            <input type="text" name="invoice_date"
                                                   class="form-control invoice_date"
                                                   id="invoice_date"
                                                   value="{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}"
                                                   placeholder="Enter Invoice Date">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting disabledDiv">
                                        <div class="form-group">
                                            <label for="due_date">Audit Due Date</label>
                                            <input type="text" name="due_date"
                                                   class="form-control due_date"
                                                   id="due_date"
                                                   value="{{ date('d-m-Y',strtotime($invoice->due_date)) }}"
                                                   placeholder="Enter Due Date">
                                        </div>
                                    </div>
                                @elseif($letter_type === 'letter2')
                                    <div class="col-md-2 floting disabledDiv">
                                        <div class="form-group">
                                            <label for="invoice_date">Letter 1 Date</label>
                                            <input type="text" name="invoice_date"
                                                   class="form-control invoice_date"
                                                   id="invoice_date"
                                                   value="{{ date('d-m-Y',strtotime($invoice->letter_one_date)) }}"
                                                   placeholder="Enter Invoice Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2 floting @if($invoice->letter_two_print === 1) disabledDiv @endif">
                                        <div class="form-group">
                                            <label for="letter_two_date">Letter 2
                                                Date @if($invoice->letter_two_print === 0)
                                                    *
                                                @endif</label>
                                            <input type="text" name="letter_two_date"
                                                   class="form-control letter_two_date"
                                                   id="letter_two_date" required
                                                   value="{{ $invoice->letter_two_print === 0 ? date('d-m-Y',strtotime($invoice->letter_one_date)) : date('d-m-Y',strtotime($invoice->letter_two_date))}}"
                                                   placeholder="Enter Letter 2 Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2 floting disabledDiv">
                                        <div class="form-group">
                                            <label for="due_date">Audit Due Date</label>
                                            <input type="text" name="due_date"
                                                   class="form-control due_date"
                                                   id="due_date"
                                                   value="{{ date('d-m-Y',strtotime($invoice->due_date)) }}"
                                                   placeholder="Enter Due Date">
                                        </div>
                                    </div>
                                @elseif($letter_type === 'letter3')
                                    <div class="col-md-2 floting disabledDiv">
                                        <div class="form-group">
                                            <label for="invoice_date">Letter 2 Date</label>
                                            <input type="text" name="invoice_date"
                                                   class="form-control invoice_date"
                                                   id="invoice_date"
                                                   value="{{ date('d-m-Y',strtotime($invoice->letter_two_date)) }}"
                                                   placeholder="Enter Invoice Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2 floting @if($invoice->letter_three_print === 1) disabledDiv @endif">
                                        <div class="form-group">
                                            <label for="letter_three_date">Letter 3
                                                Date @if($invoice->letter_three_print === 0)
                                                    *
                                                @endif</label>
                                            <input type="text" name="letter_three_date"
                                                   class="form-control letter_three_date"
                                                   id="letter_three_date" required
                                                   value="{{ $invoice->letter_three_print === 0 ? date('d-m-Y',strtotime($invoice->letter_two_date)) : date('d-m-Y',strtotime($invoice->letter_three_date)) }}"
                                                   placeholder="Enter Letter 3 Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2 floting disabledDiv">
                                        <div class="form-group">
                                            <label for="due_date">Audit Due Date</label>
                                            <input type="text" name="due_date"
                                                   class="form-control due_date"
                                                   id="due_date"
                                                   value="{{ date('d-m-Y',strtotime($invoice->due_date)) }}"
                                                   placeholder="Enter Due Date">
                                        </div>
                                    </div>
                                @endif


                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-2 floting">
                                <label for="certificate_date">Attached Audit Invoice ?</label>
                                <div class="input-group">
                                    <input type="checkbox" class="float-right largerCheckbox" style="width: 30px !important; height: 30px !important;"
                                           {{$invoice->is_attached_audit_plan == '1' || $invoice->is_attached_audit_plan == 1 ? 'checked' : ''}}
                                           value="{{$invoice->is_attached_audit_plan == '1' || $invoice->is_attached_audit_plan == 1 ? 1 : 0}}"
                                           name="is_attached_audit_plan" id="is_attached_audit_plan"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>
                        @if($invoice->letter_two_print === 0 && $letter_type === 'letter2')
                            <div class="row">

                                <div class="col-md-8"></div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                            Generate Letter 2
                                        </button>

                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <a href="{{ route('invoice.letter.index',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id]) }}"
                                           class="btn btn-block btn-danger  step1Savebtn">
                                            Cancel
                                        </a>

                                    </div>
                                </div>

                            </div>
                        @elseif($invoice->letter_three_print === 0 && $letter_type === 'letter3')
                            <div class="row">

                                <div class="col-md-8"></div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                            Generate Letter 3
                                        </button>

                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <a href="{{ route('invoice.letter.index',[$company_id,$company_standard_id,$aj_standard_id,$aj_standard_stage_id]) }}"
                                           class="btn btn-block btn-danger  step1Savebtn">
                                            Cancel
                                        </a>

                                    </div>
                                </div>

                            </div>
                        @else
                            <div class="row">

                                <div class="col-md-6"></div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <a href="{{ route('invoice.letter.print', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice->id]) }}"
                                           class="btn btn-block btn-secondary btn_save step1Savebtn">
                                            Print @if($letter_type === 'letter1')
                                                Letter 1
                                            @elseif($letter_type === 'letter2')
                                                Letter 2
                                            @else
                                                Letter 3
                                            @endif
                                        </a>

                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <a href="{{ route('invoice.letter.print', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, 'invoice', $invoice->id]) }}"
                                           class="btn btn-block btn-secondary btn_save step1Savebtn">

                                            Print Invoice
                                        </a>

                                    </div>
                                </div>

                                @if(is_null($checkMail))
                                    <div class="col-md-2 floting">
                                        <div class="form-group">

                                            <button type="button" class="btn btn-block btn-secondary btn_save sendMail"
                                                    data-invoice-id="{{$invoice->id}}" data-email-type="send">
                                                Email to Client
                                            </button>

                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-2 floting">
                                        <div class="form-group">

                                            <button type="button" class="btn btn-block btn-danger sendReMail"
                                                    data-invoice-id="{{$invoice->id}}" data-email-type="resend">
                                                Re-send Email
                                            </button>

                                        </div>
                                    </div>
                                @endif

                            </div>
                        @endif

                        @if(!empty($notifications) && count($notifications) > 0)
                            <div class="row">

                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email History</label>
                                        <div class="card-body chat_box" style="display: block;">
                                            <div class="direct-chat-messages">
                                                @foreach($notifications as $notification)
                                                    <div class="direct-chat-msg">
                                                        <div class="direct-chat-info clearfix">
                                                            <span class="direct-chat-name">{{$notification->message}} {{ date('d-m-Y h:i:s A',strtotime($notification->created_at)) }} </span>
                                                        </div>
                                                    </div>
                                                @endforeach


                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4"></div>


                            </div>
                        @endif
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <style>
        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top {
            width: 25% !important;
            margin: 0 auto !important;
        }
    </style>
@endpush

@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>

    <script>
        $('#invoice_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('#due_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $(document).on('click', '.sendMail', function () {
            var invoiceId = $(this).data('invoice-id');
            var emailType = $(this).data('email-type');
            var request = "invoice_id=" + invoiceId + "&email_type=" + emailType + "&letter_type={{$letter_type}}" + "&email=" + $('#email').val();
            mailAjaxSubmit(request);


        })
        $(document).on('click', '.sendReMail', function () {
            var invoiceId = $(this).data('invoice-id');
            var emailType = $(this).data('email-type');
            var request = "invoice_id=" + invoiceId + "&email_type=" + emailType + "&letter_type={{$letter_type}}" + "&email=" + $('#email').val();
            mailAjaxSubmit(request);
        })

        function mailAjaxSubmit(request) {
            $('#loader').show();
            $.ajax({
                type: "POST",
                url: "{{ route('invoice.letter.email.sent') }}",
                data: request,
                dataType: "json",
                cache: true,
                global: false,
                success: function (response) {
                    if (response.status === "success") {
                        toastr['success'](response.message);
                        $('#loader').hide();
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }


        var letterDate;
        var letterType = '{{$letter_type}}'
        @if($letter_type === 'letter2')
            letterDate = '{{$invoice->letter_one_date}}';
        $('.letter_two_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {
            var dateAr = this.value.split('-');
            var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
            var newLetterDate = moment(letterDate).format('YYYY-MM-DD');
            var letterTwoDate = moment(newDate).format('YYYY-MM-DD');
            if (letterTwoDate < newLetterDate) {
                toastr['error']('Letter 2 Date always greater than Letter 1 Date');
                $('#letter_two_date').val(moment(letterDate).format('DD-MM-YYYY'));
            } else {
            }
        });
        @else
            letterDate = '{{$invoice->letter_two_date}}';
        $('.letter_three_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {
            var dateAr = this.value.split('-');
            var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
            var newLetterDate = moment(letterDate).format('YYYY-MM-DD');
            var letterTwoDate = moment(newDate).format('YYYY-MM-DD');
            if (letterTwoDate < newLetterDate) {
                toastr['error']('Letter 3 Date always greater than Letter 2 Date');
                $('#letter_three_date').val(moment(letterDate).format('DD-MM-YYYY'));
            } else {
            }
        });
        @endif

        $(document).on('change', '#is_attached_audit_plan', function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
        })
    </script>
@endpush


