@extends('layouts.master')
@section('title', "Reports List")


<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.auditor-evaluation-schedule') }}" method="get" autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <h4 class="modal-title">Auditor Evaluation Schedule </h4>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Auditor Name</label>
                                        <select class="form-control" id="auditor" name="auditor">
                                            <option value="">Please Select</option>
                                            @if(!empty($auditors))
                                                @foreach($auditors as $auditor)
                                                    <option value="{{ $auditor->id }}">{{ $auditor->fullName() }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Standard</label>
                                        <select class="form-control" id="standard" name="standard">
                                            <option value="">Please Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Profile Status</label>
                                        <select class="form-control" name="status">
                                            <option value="">Please Select</option>
                                            <option value="active">Active</option>
                                            <option value="inactive">In-Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="due_date">Due Date</label>
                                        <input type="text" class="form-control" name="due_date" id="due_date"
                                               placeholder="mm/dd/yyyy">

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="actual_date">Actual Date</label>
                                        <input type="text" class="form-control" name="actual_date" id="actual_date"
                                               placeholder="mm/dd/yyy">

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row" id="chboxes">

                            <div class="col-md-2 floting">
                                <input type="checkbox" class=""
                                       name="region" id="region" value="region"
                                       onchange="hideItems(this, 'region_field')">
                                <label>Region</label>
                            </div>
                            <div class="col-md-2 floting">
                                <input type="checkbox" class="" value="employment"
                                       onchange="hideItems(this, 'employement_field')"
                                       name="employment" id="employment">
                                <label>Employeement</label>
                            </div>
                            <div class="col-md-2 floting">
                                <input type="checkbox" class="" onchange="hideItems(this, 'agreement_field')"
                                       name="agreement" id="agreement" value="agreement">
                                <label>Agreement date</label>
                            </div>


                            <div class="col-md-2 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.auditor-evaluation-schedule') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">

                        <div class="col-md-12 print_table ">
                            <a href="{{ asset('storage/uploads/documents/list-of-auditor-evaluation.pdf') }}"
                               target="_blank"><i class="fa fa-print"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Standard</th>
                                <th id="column3_search">Role</th>
                                <th id="column3_search">Standard Status</th>
                                <th id="column3_search">IAF Codes</th>
                                <th id="column3_search">Due Date</th>
                                <th id="column3_search">Actual Date</th>
                                <th class="employement_field" style="display: none">Employment Type</th>
                                <th class="agreement_field" style="display: none">Agreement Date</th>
                                <th class="region_field" style="display: none">Region</th>
                                <th>Country</th>
                                <th>Profile Status</th>

                            </tr>
                            </thead>
                            @php
                                $index = 0;
                            @endphp
                            <tbody id="myTable">
                            @if(!empty($auditors))
                                @foreach($auditors as $key=>$auditor)

                                @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)

                                    @php
                                        $rowspan =0;
                                            if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0){
                                              foreach($auditor->auditorStandards as $key=>$a_standard){
                                                  if(is_null($a_standard->deleted_at)){
                                                           $rowspan = $rowspan +  1;
                                                  }
                                              }
                                            }

                                    @endphp
                                    <tr>


                                        <td data-datatable-multi-row-rowspan="{{$rowspan}}">
                                            {{$auditor->fullName()}}
                                            <script type="x/template" class="extra-row-content">
                                                @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)
                                                @foreach($auditor->auditorStandards as $key=>$a_standard)
                                                @if(is_null($a_standard->deleted_at))


                                                @if($key == $index)
                                                @else
                                                <tr role="row">
                                                    <td>{{ $a_standard->standard->name }}

                                                        <a href="#"
                                                           class="ml-5 float-right"
                                                           onclick="auditorStandardInformation('{{$a_standard->id}}','{{$a_standard->name}}','evaluation-schedule')">View </a>
                                                    </td>
                                                    <td>
                                                        @foreach($auditor_standard_grades = \App\AuditorStandardGrade::where('auditor_standard_id', $a_standard->id)->where('status','current')->get() as $auditor_standard_grade)
                                                            <span id="auditorStandardGrade{{$a_standard->id}}">{{ $auditor_standard_grade->grade->name }}</span>
                                                        @endforeach
                                                    </td>

                                                    <td>{{ ucfirst($a_standard->status) }}</td>
                                                    @php
                                                        $auditor_standard_codes = \App\AuditorStandardCode::where('auditor_standard_id', $a_standard->id)->groupBy('iaf_id')->get();

                                                        if((!is_null($actual_date)) && (!is_null($due_date))){
                                                             $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->whereBetween('actual_date', [$actual_date1->format('Y-m-d'),$actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'),$due_date2->format('Y-m-d')])->get();

                                                        }else{
                                                            if((!is_null($actual_date))){
                                                              $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->whereBetween('actual_date', [$actual_date1->format('Y-m-d'),$actual_date2->format('Y-m-d')])->get();

                                                            }
                                                            else if((!is_null($due_date))){
                                                                $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->whereBetween('due_date', [$due_date1->format('Y-m-d'),$due_date2->format('Y-m-d')])->get();

                                                            } else{
                                                                 $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->get();

                                                            }
                                                        }


                                                    @endphp
                                                    <td>
                                                        @if(!empty($auditor_standard_codes) && count($auditor_standard_codes) > 0)
                                                            @foreach($auditor_standard_codes as $auditorStandardCode)
                                                                {{ $auditorStandardCode->iaf->code }}
                                                                @if($rowspan >1)
                                                                    ,
                                                                @endif

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($auditor_withness_evaluation) && count($auditor_withness_evaluation) >0)

                                                            @foreach($auditor_withness_evaluation as $witnessEvaluation)
                                                                @if(!is_null($witnessEvaluation->due_date))
                                                                    @php
                                                                        $due_date_one = new DateTime($witnessEvaluation->due_date);
                                                                    @endphp
                                                                    {{ $due_date_one->format('d/m/Y') }}
                                                                @else
                                                                    {{ '~' }}
                                                                @endif
                                                                @if($loop->index >= 0 && $loop->index < count($auditor_withness_evaluation)-1)
                                                                    <br>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            ~
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($auditor_withness_evaluation) && count($auditor_withness_evaluation) >0)

                                                            @foreach($auditor_withness_evaluation as $witnessEvaluation)
                                                                {{--                                                                {{ $witnessEvaluation->actual_date  ?? 'N/A'}}--}}

                                                                @if(!is_null($witnessEvaluation->actual_date))
                                                                    @php
                                                                        $actual_date_one = new DateTime($witnessEvaluation->actual_date);
                                                                    @endphp
                                                                    {{ $actual_date_one->format('d/m/Y') }}
                                                                @else
                                                                    {{ '---' }}
                                                                @endif
                                                                @if($loop->index >= 0 && $loop->index < count($auditor_withness_evaluation)-1)
                                                                    <br>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            ---
                                                        @endif
                                                    </td>

                                                </tr>
                                                @endif


                                                @endif
                                                @endforeach
                                                @endif
                                            </script>
                                        </td>
                                        @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)
                                            @foreach($auditor->auditorStandards as $key=>$a_standard)
                                                @if(is_null($a_standard->deleted_at))
                                                    @php
                                                        $index =$key;
                                                    @endphp

                                                    <td>{{ $a_standard->standard->name }}
                                                        <a href="#" class="ml-5 float-right"
                                                           onclick="auditorStandardInformation('{{$a_standard->id}}','{{$a_standard->name}}','evaluation-schedule')">View </a>
                                                    </td>
                                                    <td>
                                                        @foreach($auditor_standard_grades = \App\AuditorStandardGrade::where('auditor_standard_id', $a_standard->id)->where('status','current')->get() as $auditor_standard_grade)
                                                            <span id="auditorStandardGrade{{$a_standard->id}}"
                                                                  style="display: block">{{ $auditor_standard_grade->grade->name }}</span>
                                                        @endforeach
                                                    </td>
                                                    <td>{{ ucfirst($a_standard->status) }}</td>

                                                    @break
                                                @endif
                                            @endforeach
                                        @else
                                            <td>N/A</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                        @endif
                                        @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)
                                            @foreach($auditor->auditorStandards as $key=>$a_standard)
                                                @if(is_null($a_standard->deleted_at))

                                                    @php


                                                        $auditor_standard_codes = \App\AuditorStandardCode::where('auditor_standard_id', $a_standard->id)->groupBy('iaf_id')->get();
                                                         if((!is_null($actual_date)) && (!is_null($due_date))){
                                                             $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->whereBetween('actual_date', [$actual_date1->format('Y-m-d'),$actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'),$due_date2->format('Y-m-d')])->get();

                                                        }else{
                                                            if((!is_null($actual_date))){
                                                              $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->whereBetween('actual_date', [$actual_date1->format('Y-m-d'),$actual_date2->format('Y-m-d')])->get();

                                                            }
                                                            else if((!is_null($due_date))){
                                                                $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->whereBetween('due_date', [$due_date1->format('Y-m-d'),$due_date2->format('Y-m-d')])->get();

                                                            } else{
                                                                 $auditor_withness_evaluation = \App\AuditorStandardWitnessEvaluation::where('auditor_standard_id', $a_standard->id)->get();

                                                            }
                                                        }



                                                    @endphp
                                                    <td>
                                                        @if(!empty($auditor_standard_codes) && count($auditor_standard_codes) > 0)
                                                            @foreach($auditor_standard_codes as $auditorStandardCode)
                                                                {{ $auditorStandardCode->iaf->code }}@if($rowspan >0 )
                                                                    ,@endif
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($auditor_withness_evaluation) && count($auditor_withness_evaluation) >0)

                                                            @foreach($auditor_withness_evaluation as $witnessEvaluation)
                                                                @if(!is_null($witnessEvaluation->due_date))
                                                                    @php
                                                                        $due_date_one = new DateTime($witnessEvaluation->due_date);
                                                                    @endphp
                                                                    {{ $due_date_one->format('d/m/Y') }}
                                                                @else
                                                                    {{ '~' }}
                                                                @endif
                                                                @if($loop->index >= 0 && $loop->index < count($auditor_withness_evaluation)-1)
                                                                    <br>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            ~
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($auditor_withness_evaluation) && count($auditor_withness_evaluation) >0)

                                                            @foreach($auditor_withness_evaluation as $witnessEvaluation)
                                                                {{--                                                                {{ $witnessEvaluation->actual_date  ?? 'N/A'}}--}}

                                                                @if(!is_null($witnessEvaluation->actual_date))
                                                                    @php
                                                                        $actual_date_one = new DateTime($witnessEvaluation->actual_date);
                                                                    @endphp
                                                                    {{ $actual_date_one->format('d/m/Y') }}
                                                                @else
                                                                    {{ '---' }}
                                                                @endif
                                                                @if($loop->index >= 0 && $loop->index < count($auditor_withness_evaluation)-1)
                                                                    <br>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            ---
                                                        @endif
                                                    </td>

                                                    @break
                                                @endif

                                            @endforeach
                                        @else
                                            <td>N/A</td>
                                            <td>~</td>
                                            <td>---</td>
                                        @endif

                                        <td class="employement_field" style="display: none"
                                            data-datatable-multi-row-rowspan="{{$rowspan}}">{{ str_replace('_', ' ',ucwords($auditor->job_status)) }}</td>
                                        <td class="agreement_field" style="display: none"
                                            data-datatable-multi-row-rowspan="{{$rowspan}}">
                                            @foreach($auditor->confidentialityAgreements as $agreement)
{{--                                                @php--}}
{{--                                                    $agreement_date = new DateTime($agreement->date_signed);--}}
{{--                                                @endphp--}}
{{--                                                {{ $agreement_date->format('d/m/Y') }}--}}
                                                {{ $agreement->date_signed }}
                                            @endforeach
                                        </td>
                                        <td class="region_field" style="display: none"
                                            data-datatable-multi-row-rowspan="{{$rowspan}}">{{ $auditor->region->title }}</td>
                                        <td data-datatable-multi-row-rowspan="{{$rowspan}}">{{ $auditor->country->name }}</td>
                                        <td data-datatable-multi-row-rowspan="{{$rowspan}}">{{ ucfirst($auditor->auditor_status) }}</td>


                                    </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--       <div class="col-md-3"><button type="submit" class="btn btn-block btn-success btn_save">SAVE</button></div>-->
                <!--       <div class="col-md-3"><button type="button" class="btn btn_search">VIEW LIST</button></div>-->
            </div>
        </div>
    </div>

@endsection
@push('modals')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span class="auditor_standard_name"></span> <span
                                class="auditor_standard_grade"></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body" id="appendData">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endpush
<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }
</style>
@push('scripts')


    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
    <style link="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css"></style>


    <script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>



    <script>
        $(document).ready(function () {

            //Date range picker
            $('#actual_date').daterangepicker({
                format: "DD-MM-YYYY",
            });
            $('#due_date').daterangepicker({
                format: "DD-MM-YYYY",
            });


            // $('#actual_date').datetimepicker({
            //     format: "DD-MM-YYYY",
            //     icons: {
            //         time: 'fa fa-clock',
            //         date: 'fa fa-calendar',
            //         up: 'fa fa-chevron-up',
            //         down: 'fa fa-chevron-down',
            //         previous: 'fa fa-chevron-left',
            //         next: 'fa fa-chevron-right',
            //         today: 'fa fa-check',
            //         clear: 'fa fa-trash',
            //         close: 'fa fa-times'
            //     }
            // });
            // $('#due_date').datetimepicker({
            //     format: "DD-MM-YYYY",
            //     icons: {
            //         time: 'fa fa-clock',
            //         date: 'fa fa-calendar',
            //         up: 'fa fa-chevron-up',
            //         down: 'fa fa-chevron-down',
            //         previous: 'fa fa-chevron-left',
            //         next: 'fa fa-chevron-right',
            //         today: 'fa fa-check',
            //         clear: 'fa fa-trash',
            //         close: 'fa fa-times'
            //     }
            // });


            var standardNames = $('#standardName').text();
            standardNames = standardNames.replace(/\s/g, '');
            $('#standardName').text(standardNames);


            // IAS Codes
            var count_of_tr_arrays = $("#example1 > tbody > tr").length;
            for (var i = 0; i < count_of_tr_arrays; i++) {
                var ias_codes = $('#ias-codes' + i).text();
                ias_codes = ias_codes.replace(/\s/g, '');
                let ias_codes_array = ias_codes.split(',');
                var final_ias_codes_array = unique(ias_codes_array);
                console.log(final_ias_codes_array);
                $('#ias-codes' + i).text(final_ias_codes_array.toString());
            }


            // Accrediation
            var count_of_tr_arrays_acc = $("#example1 > tbody > tr").length;
            for (var i = 0; i < count_of_tr_arrays; i++) {
                var acc_codes = $('#acc-codes' + i).text();
                acc_codes = acc_codes.replace(/\s/g, '');
                let acc_codes_array = acc_codes.split(',');
                var final_acc_codes_array = unique(acc_codes_array);
                console.log(final_acc_codes_array);
                $('#acc-codes' + i).text(final_acc_codes_array.toString());
            }


            // $('#example1').DataTable({
            //     "columns": [
            //         {
            //             "className":      'details-control',
            //             "orderable":      false,
            //             "data":           null,
            //             "defaultContent": ''
            //         },
            //     ],
            // });
        });

        var table = $('.datatable').DataTable({

            "fnDrawCallback": function () {

                $table = $(this);

                // only apply this to specific tables
                if ($table.closest(".datatable-multi-row").length) {

                    // for each row in the table body...
                    $table.find("tbody>tr").each(function () {
                        var $tr = $(this);

                        // get the "extra row" content from the <script> tag.
                        // note, this could be any DOM object in the row.
                        var extra_row = $tr.find(".extra-row-content").html();

                        // in case draw() fires multiple times,
                        // we only want to add new rows once.
                        if (!$tr.next().hasClass('dt-added')) {
                            $tr.after(extra_row);
                            $tr.find("td").each(function () {

                                // for each cell in the top row,
                                // set the "rowspan" according to the data value.
                                var $td = $(this);
                                var rowspan = parseInt($td.data("datatable-multi-row-rowspan"), 10);
                                if (rowspan) {
                                    $td.attr('rowspan', rowspan);
                                }
                            });
                        }

                    });

                } // end if the table has the proper class
            } // end fnDrawCallback()
        });
    </script>


    <script type="text/javascript">
        $('#region_id').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#country_id';
            var region_id = $(this).val();
            var request = "region_id=" + region_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.regionCountries') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.region_countries, function (i, obj) {
                            html += '<option value="' + obj.country.id + '">' + obj.country.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>Select Country</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#auditor").change(function () {
            var id = $(this).val();
            var node_to_modify = '#standard';
            $.ajax({
                type: "GET",
                url: "{{ route('get.auditor.standard') }}",
                data: {id: id},
                dataType: "json",
                cache: true,
                success: function (response) {

                    // ajaxResponseHandler(response);
                    if (response.status == "success" && response.data.length > 0) {
                        var html = "";
                        $(response.data).each(function (i, d) {
                            if (d.deleted_at == null) {
                                html += '<option value="' + d.standard.id + '">' + d.standard.name + '</option>';
                            }


                        });

                        $(node_to_modify).html(html);
                        sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>--Select--</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }


        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }


        function auditorStandardInformation(auditorStandardId, auditorStandardName, type) {

            var request = {
                "auditor_standard_id": auditorStandardId,
                "type": type,
            };
            $('.auditor_standard_name').text(auditorStandardName);
            $('.auditor_standard_grade').text($('#auditorStandardGrade' + auditorStandardId).text());
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.auditorStandaradInformation') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    if (response.status == "success") {
                        $('#appendData').html(response.data.html);
                        $('#myModal').modal('show');
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    </script>
@endpush
