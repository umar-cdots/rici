<div>
    <div class="row">
        <div class="col-md-12">
            <label><b>Last Evaluation:&nbsp;</b></label>
            <span>
                @foreach($auditorStandardWithnessEvaluation as $witnessEvaluation)
                    {{ $witnessEvaluation->actual_date }}
                    @if($loop->index >= 0 && $loop->index < count($auditorStandardWithnessEvaluation)-1)
                        ,
                    @endif
                @endforeach
            </span>
        </div>
        <div class="col-md-12">
            <label><b>Next Due:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></label>
            <span>
                @foreach($auditorStandardWithnessEvaluation as $witnessEvaluation)
                    {{ $witnessEvaluation->due_date }}
                    @if($loop->index >= 0 && $loop->index < count($auditorStandardWithnessEvaluation)-1)
                        ,
                    @endif
                @endforeach
            </span>
        </div>
    </div>
</div>

<table id="FormalEducationTableCodes" cellspacing="20" cellpadding="0" border="0"
       class="table table-striped table-bordered table-bordered2">
    <thead>
    <tr>
        <th>S.No</th>
        <th>Accreditation</th>
        <th>IAF Code</th>
        <th>IAS Code</th>
        <th>Status</th>
        <th>Remarks</th>
    </tr>
    </thead>
    <tbody>

    @if(!empty($auditorStandards->auditorStandardCodes) && count($auditorStandards->auditorStandardCodes) > 0)
        @foreach($auditorStandards->auditorStandardCodes as $key => $auditorStandardCode)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{!! $auditorStandardCode->accreditation->name !!}</td>
                <td>{{$auditorStandardCode->iaf->code}}</td>
                <td>{{$auditorStandardCode->ias->code}}</td>
                <td>{{($auditorStandardCode->status)  ? ucfirst($auditorStandardCode->status)  : 'N/A'}}</td>
                <td>{{($auditorStandardCode->remarks) ? ucfirst($auditorStandardCode->remarks) : 'N/A'}}</td>
            </tr>
        @endforeach
    @else

        <tr>
            <td colspan="6"> Record Not Found</td>
        </tr>

    @endif


    </tbody>

</table>