@extends('layouts.master')
@section('title', "Invoices & Letters Report")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Invoices & Letters Report</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.invoice.and.letter.index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="audit_done">Audit Done</label>
                                    <select class="form-control" id="audit_done" name="audit_done">
                                        <option value="">Select an option</option>
                                        <option value="Yes" {{ isset($_REQUEST['audit_done']) && old('audit_done',$_REQUEST['audit_done']) === 'Yes'  ? 'selected' : '' }}>Yes
                                        </option>
                                        <option value="No" {{ isset($_REQUEST['audit_done']) && old('audit_done',$_REQUEST['audit_done']) === 'No'  ? 'selected' : '' }}>No
                                        </option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="invoice_status">Paid Status</label>
                                    <select class="form-control" id="invoice_status" name="invoice_status">
                                        <option value="">Select an option</option>
                                        <option value="1" {{ isset($_REQUEST['invoice_status']) && old('invoice_status',$_REQUEST['invoice_status']) === '1'  ? 'selected' : '' }}>Yes
                                        </option>
                                        <option value="0" {{ isset($_REQUEST['invoice_status']) && old('invoice_status',$_REQUEST['invoice_status']) === '0' ? 'selected' : '' }}>No
                                        </option>

                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="invoice_date">Invoice Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="invoice_date" id="invoice_date" value=""
                                               class="form-control float-right active invoice_date"
                                               placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="due_date">Due Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="due_date" id="due_date" value=""
                                               class="form-control float-right active due_date"
                                               placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="chboxes">
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-9 floting"></div>
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.invoice.and.letter.index') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th>Company Name</th>
                                <th class="city" style="display: none">City</th>
                                <th>Standard(s)</th>
                                <th>Audit Type</th>
                                <th>Due Date</th>
                                <th>Invoice Unit</th>
                                <th>Invoice Amount</th>
                                <th>Invoice Date</th>
                                <th>Letter 2 Date</th>
                                <th>Letter 3 Date</th>
                                <th>Last Email Date</th>
                                <th>Audit Done / Date</th>
                                <th>Paid Status</th>
                            </thead>

                            <tbody id="myTable">
                            @if(!empty($invoices) && count($invoices) > 0)
                                @foreach($invoices as $invoice)
                                    @php
                                        $postAudit = \App\PostAudit::where(['company_id' => $invoice->company_id, 'aj_standard_stage_id' => $invoice->aj_standard_stage_id])->first(['actual_audit_date']);
                                    @endphp
                                    @if( isset($_REQUEST['audit_done']) && !is_null($_REQUEST['audit_done']) && $_REQUEST['audit_done'] != "" && $_REQUEST['audit_done'] === 'No' && is_null($postAudit))
                                        <tr>
                                            <td>{{ $invoice->company_name }}</td>
                                            <td class="city"
                                                style="display: none">{{ ucfirst($invoice->company ? $invoice->company->city->name : 'N/A')}}</td>
                                            <td>{{ $invoice->standard }}</td>
                                            <td>{{ $invoice->audit_type }}</td>
                                            <td>{{ $invoice->due_date ? date('d-m-Y',strtotime($invoice->due_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->invoice_unit }}</td>
                                            <td>{{ $invoice->invoice_amount }}</td>
                                            <td>{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</td>
                                            <td>{{ $invoice->letter_two_date ? date('d-m-Y',strtotime($invoice->letter_two_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->letter_three_date ? date('d-m-Y',strtotime($invoice->letter_three_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->last_email_date ? date('d-m-Y',strtotime($invoice->last_email_date)) : 'N/A' }}</td>
                                            <td>
                                                @if(!is_null($postAudit) && !is_null($postAudit->actual_audit_date))
                                                    Yes {{ $postAudit->actual_audit_date ? date('d-m-Y',strtotime($postAudit->actual_audit_date)) : 'N/A' }}
                                                @else
                                                    No
                                                @endif

                                            </td>
                                            <td>{{ $invoice->invoice_status === 1 ? 'Yes' : 'No' }}</td>
                                        </tr>
                                    @elseif( isset($_REQUEST['audit_done']) && !is_null($_REQUEST['audit_done']) && $_REQUEST['audit_done'] != "" && $_REQUEST['audit_done'] === 'No' && !is_null($postAudit))
                                    @elseif( isset($_REQUEST['audit_done']) && !is_null($_REQUEST['audit_done']) && $_REQUEST['audit_done'] != "" && $_REQUEST['audit_done'] === 'Yes' && is_null($postAudit))
                                    @elseif( isset($_REQUEST['audit_done']) && !is_null($_REQUEST['audit_done']) && $_REQUEST['audit_done'] != "" && $_REQUEST['audit_done'] === 'Yes' && !is_null($postAudit))

                                        <tr>
                                            <td>{{ $invoice->company_name }}</td>
                                            <td class="city"
                                                style="display: none">{{ ucfirst($invoice->company ? $invoice->company->city->name : 'N/A')}}</td>
                                            <td>{{ $invoice->standard }}</td>
                                            <td>{{ $invoice->audit_type }}</td>
                                            <td>{{ $invoice->due_date ? date('d-m-Y',strtotime($invoice->due_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->invoice_unit }}</td>
                                            <td>{{ $invoice->invoice_amount }}</td>
                                            <td>{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</td>
                                            <td>{{ $invoice->letter_two_date ? date('d-m-Y',strtotime($invoice->letter_two_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->letter_three_date ? date('d-m-Y',strtotime($invoice->letter_three_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->last_email_date ? date('d-m-Y',strtotime($invoice->last_email_date)) : 'N/A' }}</td>
                                            <td>
                                                @if(!is_null($postAudit) && !is_null($postAudit->actual_audit_date))
                                                    Yes {{ $postAudit->actual_audit_date ? date('d-m-Y',strtotime($postAudit->actual_audit_date)) : 'N/A' }}
                                                @else
                                                    No
                                                @endif

                                            </td>
                                            <td>{{ $invoice->invoice_status === 1 ? 'Yes' : 'No' }}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td>{{ $invoice->company_name }}</td>
                                            <td class="city"
                                                style="display: none">{{ ucfirst($invoice->company ? $invoice->company->city->name : 'N/A')}}</td>
                                            <td>{{ $invoice->standard }}</td>
                                            <td>{{ $invoice->audit_type }}</td>
                                            <td>{{ $invoice->due_date ? date('d-m-Y',strtotime($invoice->due_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->invoice_unit }}</td>
                                            <td>{{ $invoice->invoice_amount }}</td>
                                            <td>{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</td>
                                            <td>{{ $invoice->letter_two_date ? date('d-m-Y',strtotime($invoice->letter_two_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->letter_three_date ? date('d-m-Y',strtotime($invoice->letter_three_date)) : 'N/A' }}</td>
                                            <td>{{ $invoice->last_email_date ? date('d-m-Y',strtotime($invoice->last_email_date)) : 'N/A' }}</td>
                                            <td>
                                                @if(!is_null($postAudit) && !is_null($postAudit->actual_audit_date))
                                                    Yes {{ $postAudit->actual_audit_date ? date('d-m-Y',strtotime($postAudit->actual_audit_date)) : 'N/A' }}
                                                @else
                                                    No
                                                @endif

                                            </td>
                                            <td>{{ $invoice->invoice_status === 1 ? 'Yes' : 'No' }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }

    .daterangepicker .ranges .input-mini {
        font-size: 20px !important;
        height: 41px !important;
        width: 120px !important;
    }

    .daterangepicker .ranges .range_inputs > div:nth-child(2) {
        padding-left: 0px !important;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        cursor: pointer !important;
    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>

        //Date range picker
        $('#invoice_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        //Date range picker
        $('#due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }
        $('.datatable').DataTable({
            dom: 'Bfrtip',
            "pageLength": 600,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "Invoices & Letters Report",
                    filename: "Invoices & Letters Report",
                    "createEmptyCells": true,
                    sheetName: "Invoices & Letters Report",

                }


            ],
        });
    </script>
@endpush


