@extends('layouts.master')
@section('title', "Reports List")

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Standards</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.list-of-generic-company-standard-index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <select class="form-control" id="name" name="name">
                                        <option value="">Select Company Name</option>
                                        @if(!empty($get_companies) && count($get_companies) > 0)
                                            @foreach($get_companies as $company)
                                                <option value="{{ $company->id }}">{{ ucfirst($company->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard</label>
                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>
                                        <option value="" disabled>Select Standard</option>
                                        @if(!empty($get_standards) && count($get_standards) > 0)
                                            @foreach($get_standards as $standard)
                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Region</label>
                                    <select class="form-control" id="region_id" name="region_id[]" multiple>
                                        <option value="" disabled>Select Region</option>
                                        @if(!empty($get_regions) && count($get_regions) > 0)
                                            @foreach($get_regions as $region)
                                                <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="form-control" name="country_id[]" id="country_id" multiple>
                                        <option value="" disabled>Select Country</option>
                                        @if(!empty($get_countries) && count($get_countries) > 0)
                                            @foreach($get_countries as $country)
                                                <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="unique_codes">Accreditation & Codes</label>
                                    <select class="form-control" id="unique_codes" name="unique_codes[]" multiple>
                                        <option value="" disabled>Accreditation & Codes</option>
                                        @if(!empty($get_company_standard_unique_codes) && count($get_company_standard_unique_codes) > 0)
                                            @foreach($get_company_standard_unique_codes as $unique_code)
                                                <option value="{{ $unique_code->unique_code }}">{{ ucfirst($unique_code->unique_code) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status">Certification Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="">Select Certification Status</option>
                                        <option value="not_certified">Not Certified</option>
                                        <option value="certified">Certified</option>
                                        <option value="suspended">Suspended</option>
                                        <option value="withdrawn">Withdrawn</option>
                                        <option value="closed">Closed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="chboxes">

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="unique_code" id="unique_code" value="unique_code"
                                       onchange="hideItems(this, 'unique_code')">
                                <label>Accreditation & Codes</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="certificationstatus" id="certificationstatus" value="certificationstatus"
                                       onchange="hideItems(this, 'certificationstatus')">
                                <label>Certification Status</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="job_number" id="job_number" value="job_number"
                                       onchange="hideItems(this, 'job_number')">
                                <label>Job Number</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="scope" id="scope" value="scope"
                                       onchange="hideItems(this, 'scope')">
                                <label>Scope</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="frequency" id="frequency" value="frequency"
                                       onchange="hideItems(this, 'frequency')">
                                <label>Frequency</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="stage_two" id="stage_two" value="stage_two"
                                       onchange="hideItems(this, 'stage_two')">
                                <label>Stage II Actual Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="addess" id="addess" value="addess"
                                       onchange="hideItems(this, 'addess')">
                                <label>Address</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="contact_person" id="contact_person" value="contact_person"
                                       onchange="hideItems(this, 'contact_person')">
                                <label>Contact Person</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_md" id="surveillance_md" value="surveillance_md"
                                       onchange="hideItems(this, 'surveillance_md')">
                                <label>Surveillance MD</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_md" id="re_audit_md" value="re_audit_md"
                                       onchange="hideItems(this, 're_audit_md')">
                                <label>Re-audit MD</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_price" id="surveillance_price" value="surveillance_price"
                                       onchange="hideItems(this, 'surveillance_price')">
                                <label>Surveillance Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_price" id="re_audit_price" value="re_audit_price"
                                       onchange="hideItems(this, 're_audit_price')">
                                <label>Re-audit Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="intial_certification_date" id="intial_certification_date"
                                       value="intial_certification_date"
                                       onchange="hideItems(this, 'intial_certification_date')">
                                <label>Original Issue Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="reaudit_actual_date" id="reaudit_actual_date"
                                       value="reaudit_actual_date"
                                       onchange="hideItems(this, 'reaudit_actual_date')">
                                <label>Reaudit Actual Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="reaudit_planned_date" id="reaudit_planned_date"
                                       value="reaudit_planned_date"
                                       onchange="hideItems(this, 'reaudit_planned_date')">
                                <label>Reaudit Due Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="withdrawn_date" id="withdrawn_date" value="withdrawn_date"
                                       onchange="hideItems(this, 'withdrawn_date')">
                                <label>Withdrawn Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="suspension_date" id="suspension_date" value="suspension_date"
                                       onchange="hideItems(this, 'suspension_date')">
                                <label>Suspension Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="days_since_suspension" id="days_since_suspension"
                                       value="days_since_suspension"
                                       onchange="hideItems(this, 'days_since_suspension')">
                                <label>Days Since Suspension</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="directory_updated" id="directory_updated"
                                       value="directory_updated"
                                       onchange="hideItems(this, 'directory_updated')">
                                <label>Directory Updated Status</label>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.list-of-generic-company-standard-index') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh "></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th rowspan="2">Sr No</th>
                                <th rowspan="2" class="company_name">Name of Company</th>
                                <th rowspan="2" class="standard">Standard(s)</th>
                                <th rowspan="2" class="unique_code" style="display: none">Accreditation & Codes</th>
                                <th rowspan="2" class="status">Status</th>
                                <th rowspan="2" class="region_field">Region</th>
                                <th rowspan="2" class="certificationstatus" style="display: none">Certification Status
                                </th>
                                <th rowspan="2" class="job_number" style="display: none; width: 25% !important;">Job
                                    Number
                                </th>
                                <th rowspan="2" class="scope" style="display: none">Scope</th>
                                <th rowspan="2" class="frequency" style="display: none">Frequency</th>
                                <th rowspan="2" class="stage_two" style="display: none">Stage II Actual Date</th>
                                <th rowspan="2" class="due_month">Stage II Month</th>
                                <th rowspan="2" class="addess" style="display: none">Address</th>
                                <th rowspan="2" class="country">Country</th>
                                <th rowspan="2" class="city" style="display: none">City</th>
                                <th colspan="4" class="contact_person" style="display: none">Contact Person</th>
                                <th colspan="2" class="surveillance_md" style="display: none">Surveillance MD</th>
                                <th colspan="2" class="re_audit_md" style="display: none">Re-audit MD</th>
                                <th colspan="2" class="surveillance_price" style="display: none">Surveillance Price</th>
                                <th colspan="2" class="re_audit_price" style="display: none">Re-audit Price</th>
                                <th rowspan="2" class="intial_certification_date" style="display: none">Original
                                    Issue Date
                                </th>
                                <th rowspan="2" class="reaudit_actual_date" style="display: none">Reaudit
                                    Actual Date
                                </th>
                                <th rowspan="2" class="reaudit_planned_date" style="display: none">Reaudit
                                    Due Date
                                </th>
                                <th rowspan="2" class="withdrawn_date" style="display: none">Withdrawn Date</th>
                                <th rowspan="2" class="suspension_date" style="display: none">Suspension Date</th>
                                <th rowspan="2" class="days_since_suspension" style="display: none">Days Since
                                    Suspension
                                </th>
                                <th rowspan="2" class="directory_updated" style="display: none">Directory Updated Status
                                </th>

                            </tr>

                            <tr>
                                <th class="contact_person" style="display: none">Title</th>
                                <th class="contact_person" style="display: none">Name</th>
                                <th class="contact_person" style="display: none">Contact No</th>
                                <th class="contact_person" style="display: none">Email</th>
                                <th class="surveillance_md" style="display: none">On-Site</th>
                                <th class="surveillance_md" style="display: none">Off-Site</th>
                                <th class="re_audit_md" style="display: none">On-Site</th>
                                <th class="re_audit_md" style="display: none">Off-Site</th>
                                <th class="surveillance_price" style="display: none">Unit</th>
                                <th class="surveillance_price" style="display: none">Value</th>
                                <th class="re_audit_price" style="display: none">Unit</th>
                                <th class="re_audit_price" style="display: none">Value</th>
                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                @foreach($companyStandard->companyStandardCodes as $key => $companyStandardCode)
                                                    @if($key == 0)

                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                            <td class="standard">{{ ucfirst($companyStandardCode->standard->name) }}</td>
                                                            <td class="unique_code" style="display: none">
                                                                @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                    {{ $companyStandardCode->unique_code }} ,
                                                                @endforeach
                                                            </td>
                                                            <td class="status">{{ ($companyStandardCode->standard->status  == 1) ? 'Active' : 'Inactive' }}</td>
                                                            <td class="region_field">{{ ucfirst($company->region->title) }}</td>
                                                            <td class="certificationstatus" style="display: none">

                                                                {{ isset($companyStandard->certificates) ? ucfirst($companyStandard->certificates->certificate_status) : 'Not Certified' }}

                                                            </td>
                                                            <td class="job_number" style="display: none">
                                                                @if(isset($companyStandard->standard->ajStandard) && !empty($companyStandard->standard->ajStandard->ajStandardStages) && count($companyStandard->standard->ajStandard->ajStandardStages) >0)
                                                                    @foreach($companyStandard->standard->ajStandard->ajStandardStages as $ajStandardStage)
                                                                        @if(isset($ajStandardStage->job_number))
                                                                            {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                            : {{ $ajStandardStage->job_number }}<br>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="scope"
                                                                style="display: none">{{ $company->scope_to_certify ?? 'N/A'}}</td>
                                                            <td class="frequency"
                                                                style="display: none">{{ ucfirst($companyStandard->surveillance_frequency) }}</td>
                                                            <td class="stage_two" style="display: none">
                                                                @php
                                                                    $post_audit_stage2_date = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q){
                                                                    $q->where('audit_type','stage_2');  
                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                    $Certification_date = App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use($companyStandard){
                                                                        $q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);  
                                                                        })->latest()->first();
                                                                @endphp
                                                                @if(isset($post_audit_stage2_date))
                                                                    {{date('d-m-Y',strtotime($post_audit_stage2_date->actual_audit_date))}} -
                                                                    {{date('d-m-Y',strtotime($post_audit_stage2_date->actual_audit_date_to))}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="due_month">
                                                                @if(isset($post_audit_stage2_date))
                                                                    {{date('F',strtotime($post_audit_stage2_date->actual_audit_date))}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="addess"
                                                                style="display: none">{{ ucfirst($company->address) }}</td>
                                                            <td class="country">{{ ucfirst($company->country->name) }}</td>
                                                            <td class="city"
                                                                style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                            <td class="contact_person"
                                                                style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->title) : '' }}</td>
                                                            <td class="contact_person"
                                                                style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->name) : '' }}</td>
                                                            <td class="contact_person"
                                                                style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->contact) : '' }}</td>
                                                            <td class="contact_person"
                                                                style="display: none">{{ $company->primaryContact ? $company->primaryContact->email : '' }}</td>
                                                            <td class="surveillance_md" style="display: none">

                                                                @php
                                                                    $onsite = 0;
                                                                    $offsite = 0;
                                                                    $reaudit_onsite = 0;
                                                                    $reaudit_offsite = 0;
                                                                   if(isset($companyStandard->standard->ajStandard) && !empty($companyStandard->standard->ajStandard->ajStandardStages) && count($companyStandard->standard->ajStandard->ajStandardStages) >0){
                                                                        foreach($companyStandard->standard->ajStandard->ajStandardStages as $ajStandardStage){
                                                                            if(!empty($company) && count($company->auditJustifications) > 0){
                                                                                   if($companyStandard->surveillance_frequency == 'annual'){
                                                                                   if($ajStandardStage->audit_type == 'surveillance_1' || $ajStandardStage->audit_type == 'surveillance_2'){
                                                                                       $onsite = $onsite + $ajStandardStage->onsite;
                                                                                       $offsite = $offsite + $ajStandardStage->offsite;
                                                                                   }elseif($ajStandardStage->audit_type == 'reaudit'){
                                                                                       $reaudit_onsite = $reaudit_onsite + $ajStandardStage->onsite;
                                                                                       $reaudit_offsite = $reaudit_offsite + $ajStandardStage->offsite;
                                                                                   }
                                                                               }else{
                                                                                    if($ajStandardStage->audit_type == 'surveillance_1' || $ajStandardStage->audit_type == 'surveillance_2' || $ajStandardStage->audit_type == 'surveillance_3' || $ajStandardStage->audit_type == 'surveillance_4' || $ajStandardStage->audit_type == 'surveillance_5'){
                                                                                       $onsite = $onsite + $ajStandardStage->onsite;
                                                                                       $offsite = $offsite + $ajStandardStage->offsite;
                                                                                    }elseif($ajStandardStage->audit_type == 'reaudit'){
                                                                                       $reaudit_onsite = $reaudit_onsite + $ajStandardStage->onsite;
                                                                                       $reaudit_offsite = $reaudit_offsite + $ajStandardStage->offsite;
                                                                                   }
                                                                               }
                                                                            }

                                                                        }
                                                                   }

                                                                @endphp
                                                                {{ $onsite }}


                                                            </td>
                                                            <td class="surveillance_md"
                                                                style="display: none">{{ $offsite }}</td>
                                                            <td class="re_audit_md"
                                                                style="display: none">{{$reaudit_onsite}}</td>
                                                            <td class="re_audit_md"
                                                                style="display: none">{{$reaudit_offsite}}</td>
                                                            <td class="surveillance_price"
                                                                style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                            <td class="surveillance_price"
                                                                style="display: none">{{ $companyStandard->surveillance_amount }}</td>
                                                            <td class="re_audit_price"
                                                                style="display: none">{{ ucfirst($companyStandard->re_audit_currency) }}</td>
                                                            <td class="re_audit_price"
                                                                style="display: none">{{ $companyStandard->re_audit_amount }}</td>
                                                            <td class="intial_certification_date" style="display: none">
                                                                @if(isset($Certification_date))
                                                                    {{date('d-m-Y',strtotime($Certification_date->original_issue_date))}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="reaudit_actual_date"
                                                                style="display: none">
                                                                @php
                                                                    $reaudit_actual_date = App\PostAudit::whereHas('AjStandardStage',function($q){
                                                                    $q->where('audit_type','reaudit');
                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->with('AjStandardStage')->first();

                                                                @endphp

                                                                @if(isset($reaudit_actual_date))
                                                                    {{date('d-m-Y',strtotime($reaudit_actual_date->actual_audit_date))}} -
                                                                    {{date('d-m-Y',strtotime($reaudit_actual_date->actual_audit_date))}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="reaudit_planned_date"
                                                                style="display: none">
                                                                @if(isset($reaudit_actual_date))
                                                                    {{date('d-m-Y',strtotime($reaudit_actual_date->AjStandardStage->excepted_date))}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="withdrawn_date" style="display: none">
                                                                @if(isset($companyStandard->certificateWithdrawn))
                                                                    {{date('d-m-Y',strtotime($companyStandard->certificateWithdrawn->certificate_date))}}
                                                                @else
                                                                    N/A
                                                                @endif

                                                            </td>
                                                            <td class="suspension_date" style="display: none">
                                                                @if(isset($companyStandard->certificateSuspension))
                                                                    {{date('d-m-Y',strtotime($companyStandard->certificateSuspension->certificate_date))}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="days_since_suspension" style="display: none">

                                                                @if(isset($companyStandard->certificateSuspension))
                                                                    @php

                                                                        $endDate = Carbon\Carbon::parse($companyStandard->certificateSuspension->certificate_date);
                                                                        $now = Carbon\Carbon::now();
                                                                        $diff = $endDate->diffInDays($now);
                                                                    @endphp

                                                                    @if($endDate < $now)
                                                                        <i class="fa fa-minus"
                                                                           aria-hidden="true"
                                                                           style="font-size: 10px;"></i> {{ $diff }} Day
                                                                    @else
                                                                        <i class="fa fa-plus"
                                                                           aria-hidden="true"
                                                                           style="font-size: 10px;"></i>{{ $diff }} Day
                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif


                                                            </td>
                                                            <td class="directory_updated" style="display: none">
                                                                {{ ucwords($companyStandard->directory_updated) }}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')




    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>
        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#unique_codes").select2();
        $("#status").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "Generic Company Standard Report",
                    filename: "Generic Company Standard Report",
                    "createEmptyCells": true,
                    sheetName: "Generic Company Standard Report",

                }


            ],
        });
    </script>
@endpush


