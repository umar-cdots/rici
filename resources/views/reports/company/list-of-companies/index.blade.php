@extends('layouts.master')
@section('title', "Reports List")

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Companies</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.list-of-companies-index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <select class="form-control" id="name" name="name">
                                        <option value="">Select Company Name</option>
                                        @if(!empty($get_companies) && count($get_companies) > 0)
                                            @foreach($get_companies as $company)
                                                <option value="{{ $company->id }}">{{ ucfirst($company->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard</label>
                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>
                                        <option value="" disabled>Select Standard</option>
                                        @if(!empty($get_standards) && count($get_standards) > 0)
                                            @foreach($get_standards as $standard)
                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Region</label>
                                    <select class="form-control" id="region_id" name="region_id[]" multiple>
                                        <option value="" disabled>Select Region</option>
                                        @if(!empty($get_regions) && count($get_regions) > 0)
                                            @foreach($get_regions as $region)
                                                <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="form-control" name="country_id[]" id="country_id" multiple>
                                        <option value="" disabled>Select Country</option>
                                        @if(!empty($get_countries) && count($get_countries) > 0)
                                            @foreach($get_countries as $country)
                                                <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="standard_status">Standard Status</label>
                                    <select class="form-control" name="standard_status" id="standard_status">
                                        <option value="" >Select Standard Status</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="chboxes">

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="region_field" id="region_field" value="region_field"
                                       onchange="hideItems(this, 'region_field')">
                                <label>Region</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="country_field" id="country_field" value="country_field"
                                       onchange="hideItems(this, 'country_field')">
                                <label>Country</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="addess" id="addess" value="addess"
                                       onchange="hideItems(this, 'addess')">
                                <label>Address</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="scope" id="scope" value="scope"
                                       onchange="hideItems(this, 'scope')">
                                <label>Scope</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="contact_person" id="contact_person" value="contact_person"
                                       onchange="hideItems(this, 'contact_person')">
                                <label>Contact Person</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="standardcount" id="standardcount" value="standardcount"
                                       onchange="hideItems(this, 'standardcount')">
                                <label>Standard Count</label>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.list-of-companies-index') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th rowspan="2">Sr No</th>
                                <th rowspan="2" class="company_name">Name of Company</th>
                                <th rowspan="2" class="standard">Standard(s)</th>
                                <th rowspan="2" class="standardcount" style="display: none">Standard Count</th>
                                <th rowspan="2" class="region_field" style="display: none">Region</th>
                                <th rowspan="2" class="country_field" style="display: none">Country</th>
                                <th rowspan="2" class="city" style="display: none">City</th>
                                <th rowspan="2" class="addess" style="display: none">Address</th>
                                <th rowspan="2" class="scope" style="display: none">Scope</th>
                                <th colspan="4" class="contact_person" style="display: none">Contact Person</th>

                            </tr>

                            <tr>
                                <th class="contact_person" style="display: none">Title</th>
                                <th class="contact_person" style="display: none">Name</th>
                                <th class="contact_person" style="display: none">Contact No</th>
                                <th class="contact_person" style="display: none">Email</th>
                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)

                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td class="company_name">{{ ucfirst($company->name) }}</td>
                                        <td class="standard">
                                            {{--Certification Status pending to show--}}
                                            @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                                @foreach($company->companyStandards as $companyStandard)
                                                    @if(!is_null($companyStandard->standard))
                                                        {{ ucfirst($companyStandard->standard->name) }}
                                                        ({{ ($companyStandard->standard->status == true) ? 'Active' : 'Inactive' }})
                                                        @if($loop->index >= 0 && $loop->index < count($company->companyStandards) - 1)
                                                            ,
                                                        @endif
                                                    @endif

                                                @endforeach
                                            @endif


                                        </td>
                                        <td class="standardcount" style="display: none">
                                            @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                                {{ count($company->companyStandards) }}
                                            @else
                                                N/A
                                            @endif

                                        </td>
                                        <td class="region_field"
                                            style="display: none">{{ ucfirst($company->region->title) }}</td>
                                        <td class="country_field"
                                            style="display: none">{{ ucfirst($company->country->name) }}</td>
                                        <td class="city"
                                            style="display: none">{{ ucfirst($company->city->name)}}</td>
                                        <td class="addess"
                                            style="display: none">{{ ucfirst($company->address) }}</td>
                                        <td class="scope"
                                            style="display: none">{{ $company->scope_to_certify ?? 'N/A'}}</td>
                                        <td class="contact_person"
                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->title) : '' }}</td>
                                        <td class="contact_person"
                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->name) : '' }}</td>
                                        <td class="contact_person"
                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->contact) : '' }}</td>
                                        <td class="contact_person"
                                            style="display: none">{{ $company->primaryContact ? $company->primaryContact->email : '' }}</td>

                                    </tr>
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')




    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>
        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#status").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "List Of Companies Report",
                    filename: "List Of Companies Report",
                    "createEmptyCells": true,
                    sheetName: "List Of Companies Report",

                }


            ],
        });
    </script>
@endpush


