@extends('layouts.master')
@section('title', "List of Certificates Not Uploaded")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Certificates Not Uploaded</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th style="width: 7% !important;">Sr No</th>
                                <th class="company_name" style="width: 15% !important;">Name of Company</th>
                                <th class="standard" style="width: 15% !important;">Standard(s)</th>
                                <th class="audit_type" style="width: 10% !important;">Audit Type</th>
                                <th class="pack_status_and_date" style="width: 10% !important;">Pack Status & Date</th>
                                <th class="print_status" style="width: 10% !important;">Print Status</th>

                            </tr>

                            </thead>

                            <tbody id="myTable">
                            @php
                                $i=1;
                            @endphp
                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td class="company_name">{{ ucfirst($company->name) }}</td>
                                        <td class="standard">


                                            @if($company->is_ims == true)
                                                @php
                                                    $companyStandard1 = \App\CompanyStandards::where('id', $company->company_standard_id)->first();
                                                    $companyRecord = \App\Company::where('id', $company->company_id)->first();
                                                    $imsCompanyStandards = $companyRecord->companyStandards()->where('is_ims', true)->get();
                                                    $ims_heading = $companyStandard1->standard->name;
                                                    if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                        foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                            if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                            continue;
                                                            } else {
                                                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                            }
                                                        }
                                                    }
                                                @endphp
                                                {{ ($ims_heading) }}(IMS)
                                            @else
                                                @php
                                                    $companyStandard = \App\CompanyStandards::where('id', $company->company_standard_id)->first();
                                                @endphp
                                                {{ $companyStandard->standard->name }}
                                            @endif


                                        </td>
                                        <td class="audit_type">
                                            {!! str_replace('_',' ',ucwords($company->audit_type)) !!}
                                        </td>
                                        <td class="pack_status_and_date">
                                            @if($company->status == 'save')
                                                @php
                                                    $status = '';
                                                    $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$company->id)->where('question_id',3)->first();
                                                     $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                     if(is_null($file)){
                                                         $status = 'no';
                                                     }else{
                                                         $status = '';
                                                     }
                                                @endphp
                                                @if(!is_null($company->actual_audit_date) && $status == 'no')
                                                    Pack Not Uploaded
                                                @else
                                                    Pack Not Sent
                                                @endif
                                            @elseif($company->status == 'unapproved')
                                                Submitted
                                            @elseif($company->status == 'rejected')
                                                Rejected {{ date('d-m-Y',strtotime($company->updated_at)) }}
                                            @elseif($company->status == 'resent')
                                                Resent
                                            @elseif($company->status == 'accepted')
                                                Forwarded
                                            @elseif($company->status == 'approved')
                                                Approved {{ date('d-m-Y',strtotime($company->approval_date)) }}
                                            @endif

                                        </td>
                                        <td class="print_status">
                                            @if($company->print_required == 'YES')
                                                @php
                                                    $certificate = \App\CertificateFile::where('post_audit_id',$company->id)->count();

                                                @endphp
                                                @if($certificate>0)
                                                    Printed
                                                @else
                                                    Not Uploaded
                                                @endif
                                                <br>
                                            @else
                                                N/A
                                                <br>
                                            @endif

                                        </td>

                                    </tr>

                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>

        //Date range picker
        $('#issue_from').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#unique_codes").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 500,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "List of Certificates Not Uploaded",
                    filename: "List of Certificates Not Uploaded",
                    "createEmptyCells": true,
                    sheetName: "List of Certificates Not Uploaded",

                }


            ],
        });
    </script>
@endpush


