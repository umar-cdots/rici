@extends('layouts.master')
@section('title', "Audits Planning Sheet Report")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Audits Planning Sheet Report</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.audit-planning-sheet-index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="region_id">Region</label>
                                    <select class="form-control" id="region_id" name="region_id[]" multiple>
                                        <option value="" disabled>Select Region</option>
                                        @if(!empty($get_regions) && count($get_regions) > 0)
                                            @foreach($get_regions as $region)
                                                <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="form-control" name="country_id[]" id="country_id" multiple>
                                        <option value="" disabled>Select Country</option>
                                        @if(!empty($get_countries) && count($get_countries) > 0)
                                            @foreach($get_countries as $country)
                                                <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="standard_id">Standard</label>
                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>
                                        <option value="" disabled>Select Standard</option>
                                        @if(!empty($get_standards) && count($get_standards) > 0)
                                            @foreach($get_standards as $standard)
                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="audit_type">Audit Type</label>
                                    <select class="form-control" id="audit_type" name="audit_type">
                                        <option value="">Select Audit Type</option>
                                        <option value="stage_2">Stage 2</option>
                                        <option value="surveillance_1">Surveillance 1</option>
                                        <option value="surveillance_2">Surveillance 2</option>
                                        <option value="surveillance_3">Surveillance 3</option>
                                        <option value="surveillance_4">Surveillance 4</option>
                                        <option value="surveillance_5">Surveillance 5</option>
                                        <option value="reaudit">Reaudit</option>


                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status">Certification Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="">Select Certification Status</option>
                                        <option value="certified">Certified</option>
                                        <option value="suspended">Suspended</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">Approval / Issue Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="issue_from" id="issue_from" value=""
                                               class="form-control float-right active issue_from"
                                               placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="aj_status">AJ Status</label>
                                    <select class="form-control" name="aj_status" id="aj_status">
                                        <option value="">Select AJ Status</option>
                                        <option value="approved">Approved</option>
                                        <option value="rejected">Rejected</option>
                                        <option value="applied">Applied</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">AJ Due Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="aj_due_date" id="aj_due_date" value=""
                                               class="form-control float-right active aj_due_date"
                                               placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row" id="chboxes">
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="unique_code" id="unique_code" value="unique_code"
                                       onchange="hideItems(this, 'unique_code')">
                                <label>Accreditation & Codes</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="certificationstatus" id="certificationstatus" value="certificationstatus"
                                       onchange="hideItems(this, 'certificationstatus')">
                                <label>Certification Status</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="job_number" id="job_number" value="job_number"
                                       onchange="hideItems(this, 'job_number')">
                                <label>Job Number</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="scope" id="scope" value="scope"
                                       onchange="hideItems(this, 'scope')">
                                <label>Scope</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="frequency" id="frequency" value="frequency"
                                       onchange="hideItems(this, 'frequency')">
                                <label>Frequency</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="stage_two" id="stage_two" value="stage_two"
                                       onchange="hideItems(this, 'stage_two')">
                                <label>Stage II Actual Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="due_month" id="due_month" value="due_month"
                                       onchange="hideItems(this, 'due_month')">
                                <label>Due month</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="addess" id="addess" value="addess"
                                       onchange="hideItems(this, 'addess')">
                                <label>Address</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="contact_person" id="contact_person" value="contact_person"
                                       onchange="hideItems(this, 'contact_person')">
                                <label>Contact Person</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_md" id="surveillance_md" value="surveillance_md"
                                       onchange="hideItems(this, 'surveillance_md')">
                                <label>Surveillance MD</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_md" id="re_audit_md" value="re_audit_md"
                                       onchange="hideItems(this, 're_audit_md')">
                                <label>Re-audit MD</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_price" id="surveillance_price" value="surveillance_price"
                                       onchange="hideItems(this, 'surveillance_price')">
                                <label>Surveillance Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_price" id="re_audit_price" value="re_audit_price"
                                       onchange="hideItems(this, 're_audit_price')">
                                <label>Re-audit Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="intial_certification_date" id="intial_certification_date"
                                       value="intial_certification_date"
                                       onchange="hideItems(this, 'intial_certification_date')">
                                <label>Original Issue Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="reaudit_actual_date" id="reaudit_actual_date"
                                       value="reaudit_actual_date"
                                       onchange="hideItems(this, 'reaudit_actual_date')">
                                <label>Reaudit Actual Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="reaudit_planned_date" id="reaudit_planned_date"
                                       value="reaudit_planned_date"
                                       onchange="hideItems(this, 'reaudit_planned_date')">
                                <label>Reaudit Due Date</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="suspension_date" id="suspension_date" value="suspension_date"
                                       onchange="hideItems(this, 'suspension_date')">
                                <label>Suspension Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="days_since_suspension" id="days_since_suspension"
                                       value="days_since_suspension"
                                       onchange="hideItems(this, 'days_since_suspension')">
                                <label>Days Since Suspension</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="directory_updated" id="directory_updated"
                                       value="directory_updated"
                                       onchange="hideItems(this, 'directory_updated')">
                                <label>Directory Updated Status</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="car_status" id="car_status"
                                       value="car_status"
                                       onchange="hideItems(this, 'car_status')">
                                <label>Car Status</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="aj_status_and_date" id="aj_status_and_date"
                                       value="aj_status_and_date"
                                       onchange="hideItems(this, 'aj_status_and_date')">
                                <label>Aj Status & Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="pack_status_and_date" id="pack_status_and_date"
                                       value="pack_status_and_date"
                                       onchange="hideItems(this, 'pack_status_and_date')">
                                <label>Pack Status & Date</label>
                            </div>

                            {{--                            <div class="col-md-3 floting">--}}
                            {{--                                <input type="checkbox" class=""--}}
                            {{--                                       name="print_status" id="print_status"--}}
                            {{--                                       value="print_status"--}}
                            {{--                                       onchange="hideItems(this, 'print_status')">--}}
                            {{--                                <label>Print Status</label>--}}
                            {{--                            </div>--}}
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.audit-planning-sheet-index') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th rowspan="2">Sr No</th>
                                <th rowspan="2" class="company_name">Name of Company</th>
                                <th rowspan="2" class="standard">Standard(s)</th>
                                <th rowspan="2" class="audit_type" style="width: 10% !important;">Audit Type</th>
                                <th rowspan="2" class="expected_due_date" style="width: 10% !important;">Due Date</th>
                                <th rowspan="2" class="actual_audit_date" style="width: 16% !important;">Actual Audit
                                    Date
                                </th>
                                <th rowspan="2" class="unique_code" style="display: none">Accreditation & Codes</th>
                                {{--                                <th rowspan="2" class="status">Status</th>--}}
                                <th rowspan="2" class="region_field">Region</th>
                                <th rowspan="2" class="certificationstatus" style="display: none">Certification Status
                                </th>
                                <th rowspan="2" class="job_number" style="display: none; width: 12px !important;">Job
                                    Number
                                </th>
                                <th rowspan="2" class="scope" style="display: none">Scope</th>
                                <th rowspan="2" class="frequency" style="display: none">Frequency</th>
                                <th rowspan="2" class="stage_two" style="display: none">Stage II Actual Date</th>
                                <th rowspan="2" class="due_month">Stage II Month</th>
                                <th rowspan="2" class="addess" style="display: none">Address</th>
                                <th rowspan="2" class="country">Country</th>
                                <th rowspan="2" class="city" style="display: none">City</th>
                                <th rowspan="2" class="days_overdue">Days OverDue</th>
                                <th colspan="4" class="contact_person" style="display: none">Contact Person</th>
                                <th colspan="2" class="surveillance_md" style="display: none">Surveillance MD</th>
                                <th colspan="2" class="re_audit_md" style="display: none">Re-audit MD</th>
                                <th colspan="2" class="surveillance_price" style="display: none">Surveillance Price</th>
                                <th colspan="2" class="re_audit_price" style="display: none">Re-audit Price</th>
                                <th rowspan="2" class="intial_certification_date" style="display: none">Original
                                    Issue Date
                                </th>
                                <th rowspan="2" class="reaudit_actual_date" style="display: none">Reaudit
                                    Actual Date
                                </th>
                                <th rowspan="2" class="reaudit_planned_date" style="display: none">Reaudit
                                    Due Date
                                </th>
                                <th rowspan="2" class="suspension_date" style="display: none">Suspension Date</th>
                                <th rowspan="2" class="days_since_suspension" style="display: none">Days Since
                                    Suspension
                                </th>
                                <th rowspan="2" class="directory_updated" style="display: none">Directory Updated Status
                                </th>
                                <th rowspan="2" class="car_status" style="display: none">Car Status
                                </th>
                                <th rowspan="2" class="aj_status_and_date" style="display: none">Aj Status & Date
                                </th>
                                <th rowspan="2" class="pack_status_and_date" style="display: none">Pack Status & Date
                                </th>
                                {{--                                <th rowspan="2" class="print_status" style="display: none">Print Status--}}
                                {{--                                </th>--}}

                            </tr>
                            <tr>
                                <th class="contact_person" style="display: none">Title</th>
                                <th class="contact_person" style="display: none">Name</th>
                                <th class="contact_person" style="display: none">Contact No</th>
                                <th class="contact_person" style="display: none">Email</th>
                                <th class="surveillance_md" style="display: none">On-Site</th>
                                <th class="surveillance_md" style="display: none">Off-Site</th>
                                <th class="re_audit_md" style="display: none">On-Site</th>
                                <th class="re_audit_md" style="display: none">Off-Site</th>
                                <th class="surveillance_price" style="display: none">Unit</th>
                                <th class="surveillance_price" style="display: none">Value</th>
                                <th class="re_audit_price" style="display: none">Unit</th>
                                <th class="re_audit_price" style="display: none">Value</th>
                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp
                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                @foreach($companyStandard->companyStandardCodes as $key => $companyStandardCode)
                                                    @if($key == 0)

                                                        @if( isset($_REQUEST['audit_type']) && !is_null($_REQUEST['audit_type']) && $_REQUEST['audit_type'] != "")

                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                @php
                                                                    $check =false;
                                                                @endphp
                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                        @php
                                                                            $check =true;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if($check == true)

                                                                    <tr>
                                                                        <td>{{ $i++ }}</td>
                                                                        <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                        <td class="standard">
                                                                            @if($companyStandard->is_ims == true)
                                                                                @php
                                                                                    $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                    $company = \App\Company::where('id', $company->id)->first();
                                                                                    $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                    $ims_heading = $companyStandard1->standard->name;


                                                                                    if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                        foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                            if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                            continue;
                                                                                            } else {
                                                                                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                @endphp
                                                                                {{ ($ims_heading) }}(IMS)
                                                                            @else
                                                                                {{ $companyStandardCode->standard->name }}
                                                                            @endif


                                                                        </td>
                                                                        <td class="audit_type">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="exptected_due_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="actual_due_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if(isset($actual_audit_date_each_stage))
                                                                                            {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}
                                                                                            -
                                                                                            {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date_to))}}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="unique_code" style="display: none">
                                                                            @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                                {{ $companyStandardCode->unique_code }}
                                                                                ,
                                                                            @endforeach
                                                                        </td>
                                                                        {{--                                                                        <td class="status">{{ ($companyStandardCode->standard->status  == 1) ? 'Active' : 'Inactive' }}</td>--}}
                                                                        <td class="region_field">{{ ucfirst($company->region->title) }}</td>
                                                                        <td class="certificationstatus"
                                                                            style="display: none">

                                                                            {{ isset($companyStandard->certificates) ? ucfirst($companyStandard->certificates->certificate_status) : 'Not Certified' }}

                                                                        </td>
                                                                        <td class="job_number" style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(isset($ajStandardStage->job_number))
                                                                                            {{ $ajStandardStage->job_number }}
                                                                                            <br>
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="scope"
                                                                            style="display: none">{{ $company->scope_to_certify ?? 'N/A'}}</td>
                                                                        <td class="frequency"
                                                                            style="display: none">{{ ucfirst($companyStandard->surveillance_frequency) }}</td>
                                                                        <td class="stage_two" style="display: none">
                                                                            @php
                                                                                $post_audit_stage2_date = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q){
                                                                                $q->where('audit_type','stage_2');
                                                                                })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                $Certification_date = App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use($companyStandard){
                                                                                    $q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);
                                                                                    })->latest()->first();
                                                                            @endphp
                                                                            @if(isset($post_audit_stage2_date))
                                                                                {{date('d-m-Y',strtotime($post_audit_stage2_date->actual_audit_date))}}
                                                                                -
                                                                                {{date('d-m-Y',strtotime($post_audit_stage2_date->actual_audit_date_to))}}
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="due_month">

                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                                        <br>
                                                                                    @endif

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="addess"
                                                                            style="display: none">{{ ucfirst($company->address) }}</td>
                                                                        <td class="country">{{ ucfirst($company->country->name) }}</td>
                                                                        <td class="city"
                                                                            style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                        <td class="days_overdue">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if(isset($actual_audit_date_each_stage))
                                                                                            @php

                                                                                                $endDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                                                $now = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                                $diff = $endDate->diffInDays($now);
                                                                                            @endphp

                                                                                            @if($endDate < $now)
                                                                                                <i class="fa fa-plus"
                                                                                                   aria-hidden="true"
                                                                                                   style="font-size: 10px;"></i> {{ $diff }}
                                                                                                Day
                                                                                            @else
                                                                                                <i class="fa fa-minus"
                                                                                                   aria-hidden="true"
                                                                                                   style="font-size: 10px;"></i>{{ $diff }}
                                                                                                Day
                                                                                            @endif
                                                                                        @else
                                                                                            @php

                                                                                                $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                                $now = Carbon\Carbon::now();
                                                                                                $diff = $endDate->diffInDays($now);
                                                                                            @endphp

                                                                                            @if($endDate < $now)
                                                                                                <i class="fa fa-plus"
                                                                                                   aria-hidden="true"
                                                                                                   style="font-size: 10px;"></i> {{ $diff }}
                                                                                                Day
                                                                                            @else
                                                                                                <i class="fa fa-minus"
                                                                                                   aria-hidden="true"
                                                                                                   style="font-size: 10px;"></i>{{ $diff }}
                                                                                                Day
                                                                                            @endif

                                                                                        @endif
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ ucfirst($company->primaryContact->title) }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ ucfirst($company->primaryContact->name) }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ ucfirst($company->primaryContact->contact) }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact->email }}</td>
                                                                        <td class="surveillance_md"
                                                                            style="display: none">

                                                                            @php
                                                                                $onsite = 0;
                                                                                $offsite = 0;
                                                                                $reaudit_onsite = 0;
                                                                                $reaudit_offsite = 0;
                                                                               if(isset($companyStandard->standard->ajStandard) && !empty($companyStandard->standard->ajStandard->ajStandardStages) && count($companyStandard->standard->ajStandard->ajStandardStages) >0){
                                                                                    foreach($companyStandard->standard->ajStandard->ajStandardStages as $ajStandardStage){
                                                                                          if($ajStandardStage->audit_type == $_REQUEST['audit_type']){
                                                                                             if(!empty($company) && count($company->auditJustifications) > 0){
                                                                                               if($companyStandard->surveillance_frequency == 'annual'){
                                                                                               if($ajStandardStage->audit_type == 'surveillance_1' || $ajStandardStage->audit_type == 'surveillance_2'){
                                                                                                   $onsite = $onsite + $ajStandardStage->onsite;
                                                                                                   $offsite = $offsite + $ajStandardStage->offsite;
                                                                                               }elseif($ajStandardStage->audit_type == 'reaudit'){
                                                                                                   $reaudit_onsite = $reaudit_onsite + $ajStandardStage->onsite;
                                                                                                   $reaudit_offsite = $reaudit_offsite + $ajStandardStage->offsite;
                                                                                               }
                                                                                           }else{
                                                                                                if($ajStandardStage->audit_type == 'surveillance_1' || $ajStandardStage->audit_type == 'surveillance_2' || $ajStandardStage->audit_type == 'surveillance_3' || $ajStandardStage->audit_type == 'surveillance_4' || $ajStandardStage->audit_type == 'surveillance_5'){
                                                                                                   $onsite = $onsite + $ajStandardStage->onsite;
                                                                                                   $offsite = $offsite + $ajStandardStage->offsite;
                                                                                                }elseif($ajStandardStage->audit_type == 'reaudit'){
                                                                                                   $reaudit_onsite = $reaudit_onsite + $ajStandardStage->onsite;
                                                                                                   $reaudit_offsite = $reaudit_offsite + $ajStandardStage->offsite;
                                                                                               }
                                                                                           }
                                                                                        }
                                                                                          }
                                                                                    }
                                                                               }

                                                                            @endphp
                                                                            {{ $onsite }}


                                                                        </td>
                                                                        <td class="surveillance_md"
                                                                            style="display: none">{{ $offsite }}</td>
                                                                        <td class="re_audit_md"
                                                                            style="display: none">{{$reaudit_onsite}}</td>
                                                                        <td class="re_audit_md"
                                                                            style="display: none">{{$reaudit_offsite}}</td>
                                                                        <td class="surveillance_price"
                                                                            style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                                        <td class="surveillance_price"
                                                                            style="display: none">{{ $companyStandard->surveillance_amount }}</td>
                                                                        <td class="re_audit_price"
                                                                            style="display: none">{{ ucfirst($companyStandard->re_audit_currency) }}</td>
                                                                        <td class="re_audit_price"
                                                                            style="display: none">{{ $companyStandard->re_audit_amount }}</td>
                                                                        <td class="intial_certification_date"
                                                                            style="display: none">
                                                                            @if(isset($Certification_date))
                                                                                {{date('d-m-Y',strtotime($Certification_date->original_issue_date))}}
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="reaudit_actual_date"
                                                                            style="display: none">
                                                                            @php
                                                                                $reaudit_actual_date = App\PostAudit::whereHas('AjStandardStage',function($q){
                                                                                $q->where('audit_type','reaudit');
                                                                                })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->with('AjStandardStage')->first();

                                                                            @endphp

                                                                            @if(isset($reaudit_actual_date))
                                                                                {{date('d-m-Y',strtotime($reaudit_actual_date->actual_audit_date))}}
                                                                                -
                                                                                {{date('d-m-Y',strtotime($reaudit_actual_date->actual_audit_date_to))}}
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="reaudit_planned_date"
                                                                            style="display: none">
                                                                            @if(isset($reaudit_actual_date))
                                                                                {{date('d-m-Y',strtotime($reaudit_actual_date->AjStandardStage->excepted_date))}}
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="suspension_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->certificateSuspension))
                                                                                {{date('d-m-Y',strtotime($companyStandard->certificateSuspension->certificate_date))}}
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="days_since_suspension"
                                                                            style="display: none">

                                                                            @if(isset($companyStandard->certificateSuspension))
                                                                                @php

                                                                                    $endDate = Carbon\Carbon::parse($companyStandard->certificateSuspension->certificate_date);
                                                                                    $now = Carbon\Carbon::now();
                                                                                    $diff = $endDate->diffInDays($now);
                                                                                @endphp

                                                                                @if($endDate < $now)
                                                                                    <i class="fa fa-minus"
                                                                                       aria-hidden="true"
                                                                                       style="font-size: 10px;"></i> {{ $diff }}
                                                                                    Day
                                                                                @else
                                                                                    <i class="fa fa-plus"
                                                                                       aria-hidden="true"
                                                                                       style="font-size: 10px;"></i>{{ $diff }}
                                                                                    Day
                                                                                @endif
                                                                            @else
                                                                                N/A
                                                                            @endif


                                                                        </td>
                                                                        <td class="directory_updated"
                                                                            style="display: none">
                                                                            {{ ucwords($companyStandard->directory_updated) }}
                                                                        </td>
                                                                        <td class="car_status" style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(!is_null($ajStandardStage->postAudit))
                                                                                            @php
                                                                                                $postAuditQuestionOne = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',1)->first();
                                                                                                $postAuditQuestionTwo = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',2)->first();

                                                                                            @endphp
                                                                                            @if(!is_null($postAuditQuestionOne) && !is_null($postAuditQuestionTwo))
                                                                                                @if($postAuditQuestionOne->scheme_value == 'NO' || is_null($postAuditQuestionOne->scheme_value))
                                                                                                    Cat
                                                                                                    1
                                                                                                    Open
                                                                                                @else
                                                                                                    Cat
                                                                                                    1
                                                                                                    Closed
                                                                                                @endif
                                                                                                <br>
                                                                                                @if($postAuditQuestionTwo->scheme_value == 'NO' || is_null($postAuditQuestionTwo->scheme_value))
                                                                                                    Cat
                                                                                                    2
                                                                                                    Open
                                                                                                @else
                                                                                                    Cat
                                                                                                    2
                                                                                                    Closed
                                                                                                @endif
                                                                                                <hr>
                                                                                            @else
                                                                                                N/A
                                                                                            @endif
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="aj_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if($ajStandardStage->status == 'approved')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                        @elseif($ajStandardStage->status == 'rejected')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                        @else
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @endif
                                                                                        <hr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="pack_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(!is_null($ajStandardStage->postAudit))
                                                                                            @if($ajStandardStage->postAudit->status == 'save')
                                                                                                @php
                                                                                                    $status = '';
                                                                                                    $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                     $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                     if(is_null($file)){
                                                                                                         $status = 'no';
                                                                                                     }else{
                                                                                                         $status = '';
                                                                                                     }


                                                                                                @endphp
                                                                                                @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                    Pack
                                                                                                    Not
                                                                                                    Uploaded
                                                                                                @else
                                                                                                    Pack
                                                                                                    Not
                                                                                                    Sent
                                                                                                @endif
                                                                                            @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                                Submitted
                                                                                            @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                                Rejected
                                                                                                <br>
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                            @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                                Resent
                                                                                            @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                                Forwarded
                                                                                            @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                                Approved
                                                                                                <br>
                                                                                                {{ $ajStandardStage->postAudit && $ajStandardStage->postAudit->postAuditSchemeInfo ? date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) : '' }}
                                                                                            @endif
                                                                                        @else
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        {{--                                                                        <td class="print_status" style="display: none">--}}
                                                                        {{--                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)--}}
                                                                        {{--                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)--}}
                                                                        {{--                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])--}}
                                                                        {{--                                                                                        @if(!is_null($ajStandardStage->postAudit))--}}
                                                                        {{--                                                                                            @if(!is_null($ajStandardStage->postAudit->postAuditSchemeInfo) && $ajStandardStage->postAudit->postAuditSchemeInfo->print_required == 'YES')--}}
                                                                        {{--                                                                                                @if(!is_null($ajStandardStage->postAudit->postAuditCertificateFile) && !empty($ajStandardStage->postAudit->postAuditCertificateFile) && count($ajStandardStage->postAudit->postAuditCertificateFile)>0)--}}
                                                                        {{--                                                                                                    Printed--}}
                                                                        {{--                                                                                                @else--}}
                                                                        {{--                                                                                                    In--}}
                                                                        {{--                                                                                                    Process--}}

                                                                        {{--                                                                                                @endif--}}
                                                                        {{--                                                                                                <hr/>--}}
                                                                        {{--                                                                                            @else--}}
                                                                        {{--                                                                                                N/A--}}
                                                                        {{--                                                                                            @endif--}}
                                                                        {{--                                                                                        @endif--}}
                                                                        {{--                                                                                    @endif--}}
                                                                        {{--                                                                                @endforeach--}}
                                                                        {{--                                                                            @endif--}}
                                                                        {{--                                                                        </td>--}}
                                                                    </tr>
                                                                @endif

                                                            @endif
                                                        @else

                                                            <tr>
                                                                <td>{{ $i++ }}</td>
                                                                <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                <td class="standard">


                                                                    @if($companyStandard->is_ims == true)
                                                                        @php
                                                                            $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                            $company = \App\Company::where('id', $company->id)->first();
                                                                            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                            $ims_heading = $companyStandard1->standard->name;


                                                                            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                    if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                    continue;
                                                                                    } else {
                                                                                    $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                    }
                                                                                }
                                                                            }
                                                                        @endphp
                                                                        {{ ($ims_heading) }}(IMS)
                                                                    @else
                                                                        {{ $companyStandardCode->standard->name }}
                                                                    @endif


                                                                </td>
                                                                <td class="audit_type">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                            <br>

                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif

                                                                </td>
                                                                <td class="exptected_due_date">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                            <br>

                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif

                                                                </td>
                                                                <td class="actual_due_date">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @php
                                                                                $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                            @endphp
                                                                            @if(isset($actual_audit_date_each_stage))
                                                                                {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}
                                                                                -
                                                                                {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date_to))}}
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                            <br>

                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif

                                                                </td>
                                                                <td class="unique_code" style="display: none">
                                                                    @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                        {{ $companyStandardCode->unique_code }} ,
                                                                    @endforeach
                                                                </td>
                                                                {{--                                                                <td class="status">{{ ($companyStandardCode->standard->status  == 1) ? 'Active' : 'Inactive' }}</td>--}}
                                                                <td class="region_field">{{ ucfirst($company->region->title) }}</td>
                                                                <td class="certificationstatus" style="display: none">

                                                                    {{ isset($companyStandard->certificates) ? ucfirst($companyStandard->certificates->certificate_status) : 'Not Certified' }}

                                                                </td>
                                                                <td class="job_number" style="display: none">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @if(isset($ajStandardStage->job_number))
                                                                                {{ $ajStandardStage->job_number }}
                                                                                <br>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="scope"
                                                                    style="display: none">{{ $company->scope_to_certify ?? 'N/A'}}</td>
                                                                <td class="frequency"
                                                                    style="display: none">{{ ucfirst($companyStandard->surveillance_frequency) }}</td>
                                                                <td class="stage_two" style="display: none">
                                                                    @php
                                                                        $post_audit_stage2_date = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q){
                                                                        $q->where('audit_type','stage_2');
                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                        $Certification_date = App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use($companyStandard){
                                                                            $q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);
                                                                            })->latest()->first();
                                                                    @endphp
                                                                    @if(isset($post_audit_stage2_date))
                                                                        {{date('d-m-Y',strtotime($post_audit_stage2_date->actual_audit_date))}}
                                                                        -
                                                                        {{date('d-m-Y',strtotime($post_audit_stage2_date->actual_audit_date_to))}}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="due_month">

                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                            <br>

                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="addess"
                                                                    style="display: none">{{ ucfirst($company->address) }}</td>
                                                                <td class="country">{{ ucfirst($company->country->name) }}</td>
                                                                <td class="city"
                                                                    style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                <td class="days_overdue">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @php
                                                                                $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                            @endphp
                                                                            @if(isset($actual_audit_date_each_stage))
                                                                                @php

                                                                                    $endDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                                    $now = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                    $diff = $endDate->diffInDays($now);
                                                                                @endphp

                                                                                @if($endDate < $now)
                                                                                    <i class="fa fa-plus"
                                                                                       aria-hidden="true"
                                                                                       style="font-size: 10px;"></i> {{ $diff }}
                                                                                    Day
                                                                                @else
                                                                                    <i class="fa fa-minus"
                                                                                       aria-hidden="true"
                                                                                       style="font-size: 10px;"></i>{{ $diff }}
                                                                                    Day
                                                                                @endif

                                                                            @else
                                                                                @php

                                                                                    $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                    $now = Carbon\Carbon::now();
                                                                                    $diff = $endDate->diffInDays($now);
                                                                                @endphp

                                                                                @if($endDate < $now)
                                                                                    <i class="fa fa-plus"
                                                                                       aria-hidden="true"
                                                                                       style="font-size: 10px;"></i> {{ $diff }}
                                                                                    Day
                                                                                @else
                                                                                    <i class="fa fa-minus"
                                                                                       aria-hidden="true"
                                                                                       style="font-size: 10px;"></i>{{ $diff }}
                                                                                    Day
                                                                                @endif

                                                                            @endif
                                                                            <br>

                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif

                                                                </td>
                                                                <td class="contact_person"
                                                                    style="display: none">{{ ucfirst($company->primaryContact->title) }}</td>
                                                                <td class="contact_person"
                                                                    style="display: none">{{ ucfirst($company->primaryContact->name) }}</td>
                                                                <td class="contact_person"
                                                                    style="display: none">{{ ucfirst($company->primaryContact->contact) }}</td>
                                                                <td class="contact_person"
                                                                    style="display: none">{{ $company->primaryContact->email }}</td>
                                                                <td class="surveillance_md" style="display: none">

                                                                    @php
                                                                        $onsite = 0;
                                                                        $offsite = 0;
                                                                        $reaudit_onsite = 0;
                                                                        $reaudit_offsite = 0;
                                                                       if(isset($companyStandard->standard->ajStandard) && !empty($companyStandard->standard->ajStandard->ajStandardStages) && count($companyStandard->standard->ajStandard->ajStandardStages) >0){
                                                                            foreach($companyStandard->standard->ajStandard->ajStandardStages as $ajStandardStage){
                                                                                if(!empty($company) && count($company->auditJustifications) > 0){
                                                                                       if($companyStandard->surveillance_frequency == 'annual'){
                                                                                       if($ajStandardStage->audit_type == 'surveillance_1' || $ajStandardStage->audit_type == 'surveillance_2'){
                                                                                           $onsite = $onsite + $ajStandardStage->onsite;
                                                                                           $offsite = $offsite + $ajStandardStage->offsite;
                                                                                       }elseif($ajStandardStage->audit_type == 'reaudit'){
                                                                                           $reaudit_onsite = $reaudit_onsite + $ajStandardStage->onsite;
                                                                                           $reaudit_offsite = $reaudit_offsite + $ajStandardStage->offsite;
                                                                                       }
                                                                                   }else{
                                                                                        if($ajStandardStage->audit_type == 'surveillance_1' || $ajStandardStage->audit_type == 'surveillance_2' || $ajStandardStage->audit_type == 'surveillance_3' || $ajStandardStage->audit_type == 'surveillance_4' || $ajStandardStage->audit_type == 'surveillance_5'){
                                                                                           $onsite = $onsite + $ajStandardStage->onsite;
                                                                                           $offsite = $offsite + $ajStandardStage->offsite;
                                                                                        }elseif($ajStandardStage->audit_type == 'reaudit'){
                                                                                           $reaudit_onsite = $reaudit_onsite + $ajStandardStage->onsite;
                                                                                           $reaudit_offsite = $reaudit_offsite + $ajStandardStage->offsite;
                                                                                       }
                                                                                   }
                                                                                }

                                                                            }
                                                                       }

                                                                    @endphp
                                                                    {{ $onsite }}


                                                                </td>
                                                                <td class="surveillance_md"
                                                                    style="display: none">{{ $offsite }}</td>
                                                                <td class="re_audit_md"
                                                                    style="display: none">{{$reaudit_onsite}}</td>
                                                                <td class="re_audit_md"
                                                                    style="display: none">{{$reaudit_offsite}}</td>
                                                                <td class="surveillance_price"
                                                                    style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                                <td class="surveillance_price"
                                                                    style="display: none">{{ $companyStandard->surveillance_amount }}</td>
                                                                <td class="re_audit_price"
                                                                    style="display: none">{{ ucfirst($companyStandard->re_audit_currency) }}</td>
                                                                <td class="re_audit_price"
                                                                    style="display: none">{{ $companyStandard->re_audit_amount }}</td>
                                                                <td class="intial_certification_date"
                                                                    style="display: none">
                                                                    @if(isset($Certification_date))
                                                                        {{date('d-m-Y',strtotime($Certification_date->original_issue_date))}}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="reaudit_actual_date"
                                                                    style="display: none">
                                                                    @php
                                                                        $reaudit_actual_date = App\PostAudit::whereHas('AjStandardStage',function($q){
                                                                        $q->where('audit_type','reaudit');
                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->with('AjStandardStage')->first();

                                                                    @endphp

                                                                    @if(isset($reaudit_actual_date))
                                                                        {{date('d-m-Y',strtotime($reaudit_actual_date->actual_audit_date))}}
                                                                        -
                                                                        {{date('d-m-Y',strtotime($reaudit_actual_date->actual_audit_date_to))}}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="reaudit_planned_date"
                                                                    style="display: none">
                                                                    @if(isset($reaudit_actual_date))
                                                                        {{date('d-m-Y',strtotime($reaudit_actual_date->AjStandardStage->excepted_date))}}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="suspension_date" style="display: none">
                                                                    @if(isset($companyStandard->certificateSuspension))
                                                                        {{date('d-m-Y',strtotime($companyStandard->certificateSuspension->certificate_date))}}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td class="days_since_suspension" style="display: none">

                                                                    @if(isset($companyStandard->certificateSuspension))
                                                                        @php

                                                                            $endDate = Carbon\Carbon::parse($companyStandard->certificateSuspension->certificate_date);
                                                                            $now = Carbon\Carbon::now();
                                                                            $diff = $endDate->diffInDays($now);
                                                                        @endphp

                                                                        @if($endDate < $now)
                                                                            <i class="fa fa-minus"
                                                                               aria-hidden="true"
                                                                               style="font-size: 10px;"></i> {{ $diff }}
                                                                            Day
                                                                        @else
                                                                            <i class="fa fa-plus"
                                                                               aria-hidden="true"
                                                                               style="font-size: 10px;"></i>{{ $diff }}
                                                                            Day
                                                                        @endif
                                                                    @else
                                                                        N/A
                                                                    @endif


                                                                </td>
                                                                <td class="directory_updated" style="display: none">
                                                                    {{ ucwords($companyStandard->directory_updated) }}
                                                                </td>
                                                                <td class="car_status" style="display: none">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @if(!is_null($ajStandardStage->postAudit))
                                                                                @php
                                                                                    $postAuditQuestionOne = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',1)->first();
                                                                                    $postAuditQuestionTwo = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',2)->first();

                                                                                @endphp
                                                                                @if(!is_null($postAuditQuestionOne) && !is_null($postAuditQuestionTwo))
                                                                                    @if($postAuditQuestionOne->scheme_value == 'NO' || is_null($postAuditQuestionOne->scheme_value))
                                                                                        Cat
                                                                                        1
                                                                                        Open
                                                                                    @else
                                                                                        Cat
                                                                                        1
                                                                                        Closed
                                                                                    @endif
                                                                                    <br>
                                                                                    @if($postAuditQuestionTwo->scheme_value == 'NO' || is_null($postAuditQuestionTwo->scheme_value))
                                                                                        Cat
                                                                                        2
                                                                                        Open
                                                                                    @else
                                                                                        Cat
                                                                                        2
                                                                                        Closed
                                                                                    @endif
                                                                                    <hr>
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                                <td class="aj_status_and_date" style="display: none">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @if($ajStandardStage->status == 'approved')
                                                                                {{ ucfirst($ajStandardStage->status) }}
                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                            @elseif($ajStandardStage->status == 'rejected')
                                                                                {{ ucfirst($ajStandardStage->status) }}
                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                            @else
                                                                                {{ ucfirst($ajStandardStage->status) }}
                                                                            @endif
                                                                            <hr>
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                                <td class="pack_status_and_date" style="display: none">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @if(!is_null($ajStandardStage->postAudit))
                                                                                @if($ajStandardStage->postAudit->status == 'save')
                                                                                    @php
                                                                                        $status = '';
                                                                                        $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                         $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                         if(is_null($file)){
                                                                                             $status = 'no';
                                                                                         }else{
                                                                                             $status = '';
                                                                                         }


                                                                                    @endphp
                                                                                    @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                        Pack
                                                                                        Not
                                                                                        Uploaded
                                                                                    @else
                                                                                        Pack
                                                                                        Not
                                                                                        Sent
                                                                                    @endif
                                                                                @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                    Submitted
                                                                                @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                    Rejected
                                                                                    <br>
                                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                    Resent
                                                                                @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                    Forwarded
                                                                                @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                    Approved
                                                                                    <br>
                                                                                    {{ $ajStandardStage->postAudit && $ajStandardStage->postAudit->postAuditSchemeInfo ? date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) : '' }}

                                                                                @endif
                                                                            @else
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                                {{--                                                                <td class="print_status" style="display: none">--}}
                                                                {{--                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)--}}
                                                                {{--                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)--}}
                                                                {{--                                                                            @if(!is_null($ajStandardStage->postAudit))--}}
                                                                {{--                                                                                @if(!is_null($ajStandardStage->postAudit->postAuditSchemeInfo) && $ajStandardStage->postAudit->postAuditSchemeInfo->print_required == 'YES')--}}
                                                                {{--                                                                                    @if(count($ajStandardStage->postAudit->postAuditCertificateFile)>0)--}}
                                                                {{--                                                                                        Printed--}}
                                                                {{--                                                                                    @else--}}
                                                                {{--                                                                                        In--}}
                                                                {{--                                                                                        Process--}}

                                                                {{--                                                                                    @endif--}}
                                                                {{--                                                                                    <hr/>--}}
                                                                {{--                                                                                @else--}}
                                                                {{--                                                                                    N/A--}}
                                                                {{--                                                                                @endif--}}
                                                                {{--                                                                            @endif--}}
                                                                {{--                                                                        @endforeach--}}
                                                                {{--                                                                    @endif--}}
                                                                {{--                                                                </td>--}}
                                                            </tr>

                                                        @endif



                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')



    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>

        //Date range picker
        $('#issue_from').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#unique_codes").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 400,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "Audits Planning Sheet Report",
                    filename: "Audits Planning Sheet Report",
                    "createEmptyCells": true,
                    sheetName: "Audits Planning Sheet Report",

                }


            ],
        });
    </script>
@endpush


