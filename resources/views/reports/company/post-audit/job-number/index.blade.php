@extends('layouts.master')
@section('title', "List of Job Numbers for Duplication Check")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Job Numbers for Duplication Check Report</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.company-job-number-index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="region_id">Region</label>
                                    <select class="form-control" id="region_id" name="region_id[]" multiple>
                                        <option value="" disabled>Select Region</option>
                                        @if(!empty($get_regions) && count($get_regions) > 0)
                                            @foreach($get_regions as $region)
                                                <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="form-control" name="country_id[]" id="country_id" multiple>
                                        <option value="" disabled>Select Country</option>
                                        @if(!empty($get_countries) && count($get_countries) > 0)
                                            @foreach($get_countries as $country)
                                                <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="standard_id">Standard</label>
                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>
                                        <option value="" disabled>Select Standard</option>
                                        @if(!empty($get_standards) && count($get_standards) > 0)
                                            @foreach($get_standards as $standard)
                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status">Certification Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="">Select Certification Status</option>
                                        <option value="certified">Certified</option>
                                        <option value="suspended">Suspended</option>
                                    </select>
                                </div>
                            </div>



                        </div>
                        <div class="row" id="chboxes">
                            <br> <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.company-job-number-index') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th class="company_name">Name of Company</th>
                                <th class="company_name">Country</th>
                                <th class="standard">Standard(s)</th>
                                <th class="audit_type" style="width: 20% !important;">Audit Type</th>
                                <th class="job_number" style=" width: 20px !important;">Job Number</th>


                            </tr>

                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp
                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                @foreach($companyStandard->companyStandardCodes as $key => $companyStandardCode)
                                                    @if($key == 0)

                                                        @if( isset($_REQUEST['audit_type']) && !is_null($_REQUEST['audit_type']) && $_REQUEST['audit_type'] != "")

                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                @php
                                                                    $check =false;
                                                                @endphp
                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                        @php
                                                                            $check =true;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if($check == true)

                                                                    <tr>
                                                                        <td>{{ $i++ }}</td>
                                                                        <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                        <td class="company_country">{{ $company->country ? ucfirst($company->country->name) : 'N/A' }}</td>
                                                                        <td class="standard">
                                                                            @if($companyStandard->is_ims == true)
                                                                                @php
                                                                                    $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                    $company = \App\Company::where('id', $company->id)->first();
                                                                                    $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                    $ims_heading = $companyStandard1->standard->name;


                                                                                    if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                        foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                            if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                            continue;
                                                                                            } else {
                                                                                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                @endphp
                                                                                {{ ($ims_heading) }}(IMS)
                                                                            @else
                                                                                {{ $companyStandardCode->standard->name }}
                                                                            @endif


                                                                        </td>
                                                                        <td class="audit_type">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="job_number" style="">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(isset($ajStandardStage->job_number))
                                                                                            {{ $ajStandardStage->job_number }}
                                                                                            <br>
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                    </tr>

                                                                @endif

                                                            @endif
                                                        @else
                                                            <tr>
                                                                <td>{{ $i++ }}</td>
                                                                <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                <td class="company_country">{{ $company->country ? ucfirst($company->country->name) : 'N/A' }}</td>
                                                                <td class="standard">


                                                                    @if($companyStandard->is_ims == true)
                                                                        @php
                                                                            $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                            $company = \App\Company::where('id', $company->id)->first();
                                                                            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                            $ims_heading = $companyStandard1->standard->name;


                                                                            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                    if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                    continue;
                                                                                    } else {
                                                                                    $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                    }
                                                                                }
                                                                            }
                                                                        @endphp
                                                                        {{ ($ims_heading) }}(IMS)
                                                                    @else
                                                                        {{ $companyStandardCode->standard->name }}
                                                                    @endif


                                                                </td>
                                                                <td class="audit_type">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                            <br>

                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif

                                                                </td>
                                                                <td class="job_number" style="">
                                                                    @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                        @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                            @if(isset($ajStandardStage->job_number))
                                                                                {{ $ajStandardStage->job_number }}
                                                                                <br>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')



    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>

        //Date range picker
        $('#issue_from').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#unique_codes").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 500,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "List of Job Numbers for Duplication Check",
                    filename: "List of Job Numbers for Duplication Check",
                    "createEmptyCells": true,
                    sheetName: "List of Job Numbers for Duplication Check",

                }


            ],
        });
    </script>
@endpush


