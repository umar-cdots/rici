@extends('layouts.master')
@section('title', "List of Non Certified Clients/Initial Audits InProcess")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Non Certified Clients/Initial Audits InProcess</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                        <form action="{{ route('reports.audit-justification-not-certified-clients') }}" method="get"
                              id="step1Savebtn"
                              autocomplete="off"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="region_id">Region</label>
                                        <select class="form-control" id="region_id" name="region_id[]" multiple>
                                            <option value="" disabled>Select Region</option>
                                            @if(!empty($get_regions) && count($get_regions) > 0)
                                                @foreach($get_regions as $region)
                                                    <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country_id">Country</label>
                                        <select class="form-control" name="country_id[]" id="country_id" multiple>
                                            <option value="" disabled>Select Country</option>
                                            @if(!empty($get_countries) && count($get_countries) > 0)
                                                @foreach($get_countries as $country)
                                                    <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="standard_id">Standard</label>
                                        <select class="form-control" id="standard_id"
                                                name="standard_id[]" multiple>
                                            <option value="" disabled>Select Standard</option>
                                            @if(!empty($get_standards) && count($get_standards) > 0)
                                                @foreach($get_standards as $standard)
                                                    <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="audit_type">Audit Type</label>
                                        <select class="form-control" id="audit_type" name="audit_type">
                                            <option value="">Select Audit Type</option>
                                            <option value="stage_1">Stage 1</option>
                                            <option value="stage_2">Stage 2</option>


                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="issue_from">AJ Due Date </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            </div>
                                            <input type="text" name="aj_due_date" id="aj_due_date" value=""
                                                   class="form-control float-right active aj_due_date"
                                                   placeholder="dd//mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="issue_from">AJ Due Date less than Current Date </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            </div>
                                            <input type="text" name="date_less_than_aj_due_date"
                                                   id="date_less_than_aj_due_date" value=""
                                                   class="form-control float-right date_less_than_aj_due_date"
                                                   placeholder="dd-mm-yyyy">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row" id="chboxes">
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="region_field" id="region_field" value="region_field"
                                           onchange="hideItems(this, 'region_field')">
                                    <label>Region</label>
                                </div>

                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="country" id="country" value="country"
                                           onchange="hideItems(this, 'country')">
                                    <label>Country</label>
                                </div>
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="city" id="city" value="city"
                                           onchange="hideItems(this, 'city')">
                                    <label>City</label>
                                </div>
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="aj_status_and_date" id="aj_status_and_date"
                                           value="aj_status_and_date"
                                           onchange="hideItems(this, 'aj_status_and_date')">
                                    <label>Aj Status & Date</label>
                                </div>
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="planning_remarks" id="planning_remarks" value="planning_remarks"
                                           onchange="hideItems(this, 'planning_remarks')">
                                    <label>Planning Remarks</label>
                                </div>

                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="report_od_by_days" id="report_od_by_days" value="report_od_by_days"
                                           onchange="hideItems(this, 'report_od_by_days')">
                                    <label>Report OD by (days)</label>
                                </div>

                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="aj_ageing_days" id="aj_ageing_days" value="aj_ageing_days"
                                           onchange="hideItems(this, 'aj_ageing_days')">
                                    <label>AJ Ageing (days)</label>
                                </div>
                                <br>
                                <br>
                                <div class="col-md-8 floting"></div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">
                                        <a href="{{ route('reports.audit-justification-not-certified-clients') }}"
                                           class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                    class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="row" id="chboxes">
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="report_od_by_days" id="report_od_by_days" value="report_od_by_days"
                                       onchange="hideItems(this, 'report_od_by_days')">
                                <label>Report OD by (days)</label>
                            </div>
                        </div>
                    @endif

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th class="company_name">Name of Company</th>
                                <th class="standard">Standard(s)</th>
                                <th class="audit_type" style="width: 10% !important;">Audit Type</th>
                                <th class="expected_due_date" style="width: 10% !important;">Actual Audit Date</th>
                                <th class="report_od_by_days" style="display: none">Report OD by (days)</th>
                                @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                    <th class="due_month">Actual Audit Date Month</th>
                                    <th class="region_field" style="display: none">Region</th>
                                    <th class="country" style="display: none">Country</th>
                                    <th class="city" style="display: none">City</th>
                                    <th class="aj_status_and_date" style="width: 143px; display: none">Aj Status &
                                        Date
                                    </th>
                                @else
                                    <th class="aj_status_and_date" style="width: 143px;">Aj Status & Date</th>
                                @endif
                                <th class="aj_ageing_days" style="display: none">AJ Ageing (days)</th>
                                @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                    <th class="pack_status_and_date">Pack Status</th>
                                    <th class="pack_status_and_date">Pack Date</th>

                                @else
                                    <th class="pack_status_and_date" style="width: 143px;">Pack Status</th>
                                @endif
                                <th class="planning_remarks" style="width: 30% !important;">Planning
                                    Remarks
                                </th>
                                <th class="noExport" style="width: 3% !important;">Edit</th>
                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(is_null($companyStandard->certificates))
                                                @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                    @foreach($companyStandard->companyStandardCodes as $key => $companyStandardCode)
                                                        @if($key == 0)

                                                            @if( isset($_REQUEST['audit_type']) && !is_null($_REQUEST['audit_type']) && $_REQUEST['audit_type'] != "")
                                                                <tr>
                                                                    <td>{{ $i++ }}</td>
                                                                    <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                    <td class="standard">
                                                                        @if($companyStandard->is_ims == true)
                                                                            @php
                                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                $ims_heading = $companyStandard1->standard->name;


                                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                        continue;
                                                                                        } else {
                                                                                        $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            @endphp
                                                                            {{ ($ims_heading) }}(IMS)
                                                                        @else
                                                                            {{ $companyStandardCode->standard->name }}
                                                                        @endif


                                                                    </td>
                                                                    <td class="audit_type">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                    {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                                @endif
                                                                                <br>

                                                                            @endforeach
                                                                        @else
                                                                            N/A
                                                                        @endif

                                                                    </td>
                                                                    <td class="exptected_due_date">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                    @php
                                                                                        $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                        $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                    @endphp
                                                                                    @if(isset($actual_audit_date_each_stage))
                                                                                        {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                @endif
                                                                                <br>

                                                                            @endforeach
                                                                        @else
                                                                            N/A
                                                                        @endif

                                                                    </td>
                                                                    <td class="report_od_by_days" style="display: none">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)

                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if($ajStandardStage->postAudit->status == 'approved')
                                                                                            N/A
                                                                                        @else
                                                                                            @if(isset($actual_audit_date_each_stage))

                                                                                                @php

                                                                                                    $endDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                                                    $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                                    $diff = $endDate->diffInDays($now);
                                                                                                @endphp

                                                                                                @if($endDate < $now)
                                                                                                    +{{ $diff }}

                                                                                                @else
                                                                                                    -{{ $diff }}

                                                                                                @endif

                                                                                            @else
                                                                                                N/A
                                                                                            @endif
                                                                                        @endif
                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>
                                                                                @endif

                                                                            @endforeach
                                                                        @else
                                                                            N/A
                                                                        @endif

                                                                    </td>
                                                                    @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                                                        <td class="due_month">

                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if(isset($actual_audit_date_each_stage))
                                                                                            {{date('F',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="region_field"
                                                                            style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                                        <td class="country"
                                                                            style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                                        <td class="city"
                                                                            style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                        <td class="aj_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if($ajStandardStage->status == 'approved')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            ({{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                            )
                                                                                        @elseif($ajStandardStage->status == 'rejected')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            ({{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                            )
                                                                                        @else
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                Not Applied
                                                                            @endif
                                                                        </td>
                                                                    @else
                                                                        <td class="aj_status_and_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if($ajStandardStage->status == 'approved')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            ({{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                            )
                                                                                        @elseif($ajStandardStage->status == 'rejected')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            ({{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                            )
                                                                                        @else
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                Not Applied
                                                                            @endif
                                                                        </td>
                                                                    @endif
                                                                    <td class="aj_ageing_days" style="display: none">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)

                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                    @if($ajStandardStage->status == 'approved')

                                                                                        @php

                                                                                            $endDate = Carbon\Carbon::parse($ajStandardStage->approval_date);
                                                                                            $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                            $diff = $endDate->diffInDays($now);
                                                                                        @endphp

                                                                                        @if($endDate < $now)
                                                                                            +{{ $diff }}

                                                                                        @else
                                                                                            -{{ $diff }}
                                                                                        @endif
                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            Not Applied
                                                                        @endif
                                                                    </td>
                                                                    <td class="pack_status_and_date">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @if($ajStandardStage->postAudit->status == 'save')
                                                                                            @php
                                                                                                $status = '';
                                                                                                $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                 $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                 if(is_null($file)){
                                                                                                     $status = 'no';
                                                                                                 }else{
                                                                                                     $status = '';
                                                                                                 }


                                                                                            @endphp
                                                                                            @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                Pack Not Uploaded
                                                                                            @else
                                                                                                Pack Not Sent
                                                                                            @endif
                                                                                        @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                            Submitted
                                                                                        @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                            Rejected
                                                                                        @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                            Resent
                                                                                        @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                            Forwarded
                                                                                        @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                            Approved
                                                                                        @endif
                                                                                    @else
                                                                                    @endif
                                                                                @endif
                                                                                <br>
                                                                            @endforeach
                                                                        @endif
                                                                    </td>
                                                                    @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                                                        <td class="pack_status_and_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(!is_null($ajStandardStage->postAudit))
                                                                                            @if($ajStandardStage->postAudit->status == 'save')
                                                                                                @php
                                                                                                    $status = '';
                                                                                                    $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                     $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                     if(is_null($file)){
                                                                                                         $status = 'no';
                                                                                                     }else{
                                                                                                         $status = '';
                                                                                                     }


                                                                                                @endphp
                                                                                                @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                    Pack Not Uploaded
                                                                                                @else
                                                                                                    Pack Not Sent
                                                                                                @endif
                                                                                            @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                            @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                            @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                                Resent
                                                                                            @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                                Forwarded
                                                                                            @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                                {{ $ajStandardStage->postAudit && $ajStandardStage->postAudit->postAuditSchemeInfo ? date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) : '' }}

                                                                                            @endif
                                                                                        @else
                                                                                        @endif
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                    @endif
                                                                    <td class="planning_remarks">{{$companyStandard->main_remarks ?? 'N/A'}}
                                                                    </td>
                                                                    <td>
                                                                        <a href="javascript:void(0)"
                                                                           onclick="companyStandardRemarks('{{ $companyStandard->id }}')"><i
                                                                                    class="fa fa-edit"></i></a>
                                                                        <a href="{{ route('company.show', $company->id) }}"
                                                                           target="_blank"
                                                                           style="margin-left: 10px;"
                                                                           title="View"><i
                                                                                    class="fa fa-eye"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{ $i++ }}</td>
                                                                    <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                    <td class="standard">


                                                                        @if($companyStandard->is_ims == true)
                                                                            @php
                                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                $ims_heading = $companyStandard1->standard->name;


                                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                        continue;
                                                                                        } else {
                                                                                        $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            @endphp
                                                                            {{ ($ims_heading) }}(IMS)
                                                                        @else
                                                                            {{ $companyStandardCode->standard->name }}
                                                                        @endif


                                                                    </td>
                                                                    <td class="audit_type">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                                <br>

                                                                            @endforeach
                                                                        @else
                                                                            N/A
                                                                        @endif

                                                                    </td>
                                                                    <td class="exptected_due_date">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @php
                                                                                    $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                    $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                @endphp
                                                                                @if(isset($actual_audit_date_each_stage))
                                                                                    {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                                <br>

                                                                            @endforeach
                                                                        @else
                                                                            N/A
                                                                        @endif

                                                                    </td>
                                                                    <td class="report_od_by_days" style="display: none">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                                    @php
                                                                                        $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                        $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                    @endphp
                                                                                    @if($ajStandardStage->postAudit->status == 'approved')
                                                                                        N/A
                                                                                    @else
                                                                                        @if(isset($actual_audit_date_each_stage))

                                                                                            @php

                                                                                                $endDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                                                $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                                $diff = $endDate->diffInDays($now);
                                                                                            @endphp

                                                                                            @if($endDate < $now)
                                                                                                +{{ $diff }}

                                                                                            @else
                                                                                                -{{ $diff }}

                                                                                            @endif

                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                    @endif
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                                <br>

                                                                            @endforeach
                                                                        @else
                                                                            N/A
                                                                        @endif

                                                                    </td>
                                                                    @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                                                        <td class="due_month">

                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @php
                                                                                        $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                        $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                    @endphp
                                                                                    @if(isset($actual_audit_date_each_stage))
                                                                                        {{date('F',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>

                                                                        <td class="region_field"
                                                                            style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                                        <td class="country"
                                                                            style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                                        <td class="city"
                                                                            style="display: none">{{ ucfirst($company->city->name)}}</td>

                                                                        <td class="aj_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->status == 'approved')
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                        ({{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                        )
                                                                                    @elseif($ajStandardStage->status == 'rejected')
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                        ({{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                        )
                                                                                    @else
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @else
                                                                                Not Applied
                                                                            @endif
                                                                        </td>
                                                                    @else
                                                                        <td class="aj_status_and_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->status == 'approved')
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                        ({{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                        )
                                                                                    @elseif($ajStandardStage->status == 'rejected')
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                        ({{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                        )
                                                                                    @else
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @else
                                                                                Not Applied
                                                                            @endif
                                                                        </td>
                                                                    @endif
                                                                    <td class="aj_ageing_days" style="display: none">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if($ajStandardStage->status == 'approved')

                                                                                    @php

                                                                                        $endDate = Carbon\Carbon::parse($ajStandardStage->approval_date);
                                                                                        $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                        $diff = $endDate->diffInDays($now);
                                                                                    @endphp

                                                                                    @if($endDate < $now)
                                                                                        +{{ $diff }}

                                                                                    @else
                                                                                        -{{ $diff }}
                                                                                    @endif
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                                <br>
                                                                            @endforeach
                                                                        @else
                                                                            Not Applied
                                                                        @endif
                                                                    </td>
                                                                    <td class="pack_status_and_date">
                                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                                    @if($ajStandardStage->postAudit->status == 'save')
                                                                                        @php
                                                                                            $status = '';
                                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                             $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                             if(is_null($file)){
                                                                                                 $status = 'no';
                                                                                             }else{
                                                                                                 $status = '';
                                                                                             }


                                                                                        @endphp
                                                                                        @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                            Pack Not Uploaded
                                                                                        @else
                                                                                            Pack Not Sent
                                                                                        @endif
                                                                                    @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                        Submitted
                                                                                    @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                        Rejected
                                                                                    @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                        Resent
                                                                                    @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                        Forwarded
                                                                                    @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                        Approved
                                                                                    @endif
                                                                                @else
                                                                                @endif
                                                                                <br>
                                                                            @endforeach
                                                                        @endif
                                                                    </td>
                                                                    @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                                                        <td class="pack_status_and_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @if($ajStandardStage->postAudit->status == 'save')
                                                                                            @php
                                                                                                $status = '';
                                                                                                $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                 $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                 if(is_null($file)){
                                                                                                     $status = 'no';
                                                                                                 }else{
                                                                                                     $status = '';
                                                                                                 }


                                                                                            @endphp
                                                                                            @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                Pack Not Uploaded
                                                                                            @else
                                                                                                Pack Not Sent
                                                                                            @endif
                                                                                        @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                        @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                        @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                            Resent
                                                                                        @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                            Forwarded
                                                                                        @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                            {{ $ajStandardStage->postAudit && $ajStandardStage->postAudit->postAuditSchemeInfo ? date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) : '' }}

                                                                                        @endif
                                                                                    @else
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                    @endif
                                                                    <td class="planning_remarks">{{$companyStandard->main_remarks ?? 'N/A'}}
                                                                    </td>
                                                                    <td>
                                                                        <a href="javascript:void(0)"
                                                                           onclick="companyStandardRemarks('{{ $companyStandard->id }}')"><i
                                                                                    class="fa fa-edit"></i></a>
                                                                        <a href="{{ route('company.show', $company->id) }}"
                                                                           target="_blank"
                                                                           style="margin-left: 10px;"
                                                                           title="View"><i
                                                                                    class="fa fa-eye"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('modals')
    <div id="company_standard_remarks_html"></div>
@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }

    .daterangepicker .ranges .input-mini {
        font-size: 20px !important;
        height: 41px !important;
        width: 120px !important;
    }

    .daterangepicker .ranges .range_inputs > div:nth-child(2) {
        padding-left: 0px !important;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        cursor: pointer !important;
        @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
            display: none;
    @endif




    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>


        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#date_less_than_aj_due_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            // Show length rows menu
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 100,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "List of Non Certified Clients/Initial Audits InProcess",
                    filename: "List of Non Certified Clients/Initial Audits InProcess",
                    "createEmptyCells": true,
                    sheetName: "List of Non Certified Clients/Initial Audits InProcess",

                }


            ],
        });

        function companyStandardRemarks(companyStandardId) {
            var request = {"company_standard_id": companyStandardId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.company_standard_remarks')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#company_standard_remarks_html").html(response.data.html);
                        $('#companyStandardRemarksModal').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardRemarksSubmit(e, company_standard_id) {

            e.preventDefault();
            var form = $('#company-standard-remarks-store')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.company_standard_remarks.store') }}",
                enctype: 'multipart/form-data',
                // processData: false,
                // contentType: false,
                // cache: false,
                data: {
                    planning_remarks: $('#remarks' + company_standard_id).val(),
                    scheme_manager_remarks: '',
                    company_standard_id: company_standard_id
                },
                success: function (response) {
                    if (response.status == "success") {
                        $('#companyStandardRemarksModal').modal('hide');
                        toastr['success']("Remarks has been added Successfully.");
                        // setTimeout(function () {
                        //     window.location.reload();
                        // }, 1000);

                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    </script>
@endpush


