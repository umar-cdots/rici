@extends('layouts.master')
@section('title', "Audits Conducted Report")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Audits Conducted Report</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit-conducted-report') }}"
                          method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="region_id">Region</label>
                                    <select class="form-control" id="region_id" name="region_id[]" multiple>
                                        <option value="" disabled>Select Region</option>
                                        @if(!empty($get_regions) && count($get_regions) > 0)
                                            @foreach($get_regions as $region)
                                                <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="form-control" name="country_id[]" id="country_id" multiple>
                                        <option value="" disabled>Select Country</option>
                                        @if(!empty($get_countries) && count($get_countries) > 0)
                                            @foreach($get_countries as $country)
                                                <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            {{--                            <div class="col-md-4">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="standard_id">Standard</label>--}}
                            {{--                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>--}}
                            {{--                                        <option value="" disabled>Select Standard</option>--}}
                            {{--                                        @if(!empty($get_standards) && count($get_standards) > 0)--}}
                            {{--                                            @foreach($get_standards as $standard)--}}
                            {{--                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>--}}
                            {{--                                            @endforeach--}}
                            {{--                                        @endif--}}

                            {{--                                    </select>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="audit_type">Audit Type</label>
                                    <select class="form-control" id="audit_type" name="audit_type">
                                        <option value="">Select Audit Type</option>
                                        <option value="stage_1">Stage 1</option>
                                        <option value="stage_2">Stage 2</option>
                                        <option value="surveillance_1">Surveillance 1</option>
                                        <option value="surveillance_2">Surveillance 2</option>
                                        <option value="surveillance_3">Surveillance 3</option>
                                        <option value="surveillance_4">Surveillance 4</option>
                                        <option value="surveillance_5">Surveillance 5</option>
                                        <option value="reaudit">Reaudit</option>


                                    </select>
                                </div>
                            </div>
                            {{--                            <div class="col-md-4">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="certificate_status">Certification Status</label>--}}

                            {{--                                    <select name="certificate_status" id="certificate_status" class="form-control">--}}
                            {{--                                        <option value="">Select Certification Status</option>--}}
                            {{--                                        <option value="certified">Certified</option>--}}
                            {{--                                        <option value="suspended">Suspended</option>--}}
                            {{--                                        <option value="withdrawn">Withdrawn</option>--}}
                            {{--                                        <option value="closed">Closed</option>--}}
                            {{--                                        <option value="not_certified" selected>Not Certified</option>--}}
                            {{--                                    </select>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="aj_approved_status">Pack Approval Status / Checkbox</label>

                                    <select name="aj_approved_status" id="aj_approved_status" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="true">Checked</option>
                                        <option value="false">UnChecked</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">Actual Audit Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="aj_due_date" id="aj_due_date" value=""
                                               class="form-control float-right active aj_due_date"
                                               placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">AJ Due Date less than Current Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="date_less_than_aj_due_date"
                                               id="date_less_than_aj_due_date" value=""
                                               class="form-control float-right date_less_than_aj_due_date"
                                               placeholder="dd-mm-yyyy">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row" id="chboxes">
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="region_field" id="region_field" value="region_field"
                                       onchange="hideItems(this, 'region_field')">
                                <label>Region</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="country" id="country" value="country"
                                       onchange="hideItems(this, 'country')">
                                <label>Country</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="pack_status_and_date" id="pack_status_and_date"
                                       value="pack_status_and_date"
                                       onchange="hideItems(this, 'pack_status_and_date')">
                                <label>Pack Status & Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="days_overdue" id="days_overdue"
                                       value="days_overdue"
                                       onchange="hideItems(this, 'days_overdue')">
                                <label>Days OverDue</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="initial_certification_amount" id="initial_certification_amount"
                                       value="initial_certification_amount"
                                       onchange="hideItems(this, 'initial_certification_amount')">
                                <label>Stage 1 & 2 Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_price" id="surveillance_price" value="surveillance_price"
                                       onchange="hideItems(this, 'surveillance_price')">
                                <label>Surveillance Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_price" id="re_audit_price" value="re_audit_price"
                                       onchange="hideItems(this, 're_audit_price')">
                                <label>Re-audit Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="standard_status" id="standard_status" value="standard_status"
                                       onchange="hideItems(this, 'standard_status')">
                                <label>Standard Status</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="job_number" id="job_number" value="job_number"
                                       onchange="hideItems(this, 'job_number')">
                                <label>Job Number</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="onsite" id="onsite" value="onsite"
                                       onchange="hideItems(this, 'onsite')">
                                <label>Onsite Mandays</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="offsite" id="offsite" value="offsite"
                                       onchange="hideItems(this, 'offsite')">
                                <label>Offsite Mandays</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="question_one" id="question_one" value="question_one"
                                       onchange="hideItems(this, 'question_one')">
                                <label>Question 1</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="question_two" id="question_two" value="question_two"
                                       onchange="hideItems(this, 'question_two')">
                                <label>Question 2</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="out_of_country_region_name" id="out_of_country_region_name"
                                       value="out_of_country_region_name"
                                       onchange="hideItems(this, 'out_of_country_region_name')">
                                <label>Country # 2</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="key_change_remarks" id="key_change_remarks"
                                       value="key_change_remarks"
                                       onchange="hideItems(this, 'key_change_remarks')">
                                <label>Key Change Remarks</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="pack_review_remarks" id="pack_review_remarks"
                                       value="pack_review_remarks"
                                       onchange="hideItems(this, 'pack_review_remarks')">
                                <label>Pack Review Remarks</label>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit-conducted-report') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th data-is-export="0" data-comment-field="false" rowspan="2">Sr No</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="company_name">Name of Company</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="standard">Standard(s)</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="audit_type noExport" style="width: 10% !important;">Audit Type
                                </th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Audit Type</th> <!--For export-->

                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="job_number" style="width: 3% !important;display: none">Job
                                    Number
                                </th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="expected_due_date noExport" style="width: 10% !important;">Due
                                    Date
                                </th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Due Date</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="due_month noExport">Due Month</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Due Month</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="days_overdue noExport" style="display: none">Days OverDue</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Days OverDue</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="actual_audit_date noExport" style="width: 9% !important;">Audit Start Date
                                </th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Audit Start Date
                                </th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="region_field" style="display: none">Region</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="country" style="display: none">Country</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="city" style="display: none">City</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="aj_status_and_date noExport" style="width: 135px;">Aj Status</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Aj Status</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="aj_status_and_date noExport" style="width: 135px;">Aj Date</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Aj Date</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="pack_status_and_date noExport" style="display: none">Pack
                                    Status
                                </th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Pack Status</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="pack_status_and_date noExport" style="display: none">Pack Date
                                </th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Pack Date</th> <!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="onsite noExport" style="display: none">Onsite Mandays</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Onsite Mandays</th><!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="offsite noExport"  style="display: none">Offsite Mandays</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Offsite Mandays</th><!--For export-->
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="standard_status" style="display: none">Standard Status
                                </th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="question_one noExport"  style="display: none">Q1</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Q1</th><!--For export-->

                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="question_two noExport"  style="display: none">Q2</th>
                                <th data-is-export="1" data-comment-field="false" rowspan="2" style="display: none">Q2</th><!--For export-->
                                <th data-is-export="1" data-comment-field="false" rowspan="2" class="out_of_country_region_name" style="display: none">Country # 2</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="key_change_remarks noExport" style="display: none">Key Change Remarks</th>
                                <th data-is-export="1" data-comment-field="true" rowspan="2"  style="display: none">Key Change Remarks</th>
                                <th data-is-export="0" data-comment-field="false" rowspan="2" class="pack_review_remarks noExport" style="display: none">Pack Review Remarks</th>
                                <th data-is-export="1" data-comment-field="true" rowspan="2"  style="display: none">Pack Review Remarks</th>
                                <th data-is-export="0" data-comment-field="false" colspan="2" class="initial_certification_amount" style="display: none">Stage 1 & 2 Price
                                </th>
                                <th data-is-export="0" data-comment-field="false" colspan="1" class="surveillance_price" style="display: none">Surveillance Price</th>
                                <th data-is-export="0" data-comment-field="false" colspan="1" class="re_audit_price" style="display: none">Re-audit Price</th>


                            </tr>
                            <tr>
                                <th data-is-export="0" data-comment-field="false" class="initial_certification_amount" style="display: none">Unit</th>
                                <th data-is-export="0" data-comment-field="false" class="initial_certification_amount" style="display: none">Value</th>
                                <th data-is-export="0" data-comment-field="false" class="surveillance_price" style="display: none">Value</th>
                                <th data-is-export="0" data-comment-field="false" class="re_audit_price" style="display: none">Value</th>
                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                <tr>
                                                    <td data-is-export="0" data-comment-field="false" >{{ $i++ }}</td>
                                                    <td data-is-export="0" data-comment-field="false"  class="company_name">{{ ucfirst($company->name) }}</td>
                                                    <td data-is-export="0" data-comment-field="false"  class="standard">


                                                        @if($companyStandard->is_ims == true)
                                                            @php
                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                $ims_heading = $companyStandard1->standard->name;
                                                                $surveillance_amount = (int)$companyStandard1->surveillance_amount;
                                                                $re_audit_amount = (int)$companyStandard1->re_audit_amount;
                                                                $initial_certification_amount = (int)$companyStandard->initial_certification_amount;



                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                        continue;
                                                                        } else {
                                                                         $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                         $surveillance_amount += (int)$imsCompanyStandard->surveillance_amount;
                                                                         $re_audit_amount += (int)$imsCompanyStandard->re_audit_amount;
                                                                         $initial_certification_amount += (int)$imsCompanyStandard->initial_certification_amount;

                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                            {{ ($ims_heading) }}(IMS)
                                                        @else
                                                            @php
                                                                $initial_certification_amount = (int)$companyStandard->initial_certification_amount;
                                                                $surveillance_amount = $companyStandard->surveillance_amount;
                                                                $re_audit_amount =  $companyStandard->re_audit_amount;
                                                            @endphp
                                                            {{ $companyStandard->standard->name }}
                                                        @endif


                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false"  class="audit_type">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                <br>

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                $
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>

                                                    <td data-is-export="0" data-comment-field="false" class="job_number" style="display: none;">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{$ajStandardStage->job_number}}
                                                                @break

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="exptected_due_date">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                <br>

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                $
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="due_month">

                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                <br>

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display:none;">

                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                $

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="days_overdue"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)

                                                                @php
                                                                    $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                    $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                @endphp

                                                                @if(isset($actual_audit_date_each_stage))
                                                                    @php
                                                                        $startDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                        $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                        $now = Carbon\Carbon::now();
                                                                        $diff = $endDate->diffInDays($startDate);
                                                                    @endphp
                                                                    @if($endDate < $startDate)
                                                                        +{{ $diff }}

                                                                    @else
                                                                        -{{ $diff }}

                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif

                                                                <br>

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)

                                                                @php
                                                                    $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                    $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                @endphp

                                                                @if(isset($actual_audit_date_each_stage))
                                                                    @php
                                                                        $startDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                        $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                        $now = Carbon\Carbon::now();
                                                                        $diff = $endDate->diffInDays($startDate);
                                                                    @endphp
                                                                    @if($endDate < $startDate)
                                                                        +{{ $diff }}

                                                                    @else
                                                                        -{{ $diff }}

                                                                    @endif
                                                                    $
                                                                @else
                                                                    N/A $
                                                                @endif


                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="actual_due_date">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @php
                                                                    $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                    $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                @endphp
                                                                @if(isset($actual_audit_date_each_stage))
                                                                    {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                @else
                                                                    N/A
                                                                @endif
                                                                <br>

                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display:none;">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @php
                                                                    $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                    $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                    })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                @endphp
                                                                @if(isset($actual_audit_date_each_stage))
                                                                    {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}} $

                                                                @else
                                                                    N/A $
                                                                @endif


                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="region_field"
                                                        style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                    <td data-is-export="0" data-comment-field="false" class="country"
                                                        style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                    <td data-is-export="2" data-comment-field="false" class="city"
                                                        style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                    <td data-is-export="0" data-comment-field="false" class="aj_status_and_date">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if($ajStandardStage->status == 'approved')
                                                                    {{ ucfirst($ajStandardStage->status) }}
                                                                @elseif($ajStandardStage->status == 'rejected')
                                                                    {{ ucfirst($ajStandardStage->status) }}
                                                                @else
                                                                    {{ ucfirst($ajStandardStage->status) }}
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if($ajStandardStage->status == 'approved')
                                                                    {{ ucfirst($ajStandardStage->status) }} $
                                                                @elseif($ajStandardStage->status == 'rejected')
                                                                    {{ ucfirst($ajStandardStage->status) }} $
                                                                @else
                                                                    {{ ucfirst($ajStandardStage->status) }} $
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="aj_status_and_date">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if($ajStandardStage->status == 'approved')
                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}

                                                                @elseif($ajStandardStage->status == 'rejected')
                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                @else
                                                                    {{ ucfirst($ajStandardStage->status) }}
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if($ajStandardStage->status == 'approved')
                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}$

                                                                @elseif($ajStandardStage->status == 'rejected')
                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}$
                                                                @else
                                                                    {{ ucfirst($ajStandardStage->status) }}$
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="pack_status_and_date"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if($ajStandardStage->postAudit->status == 'save')
                                                                        @php
                                                                            $status = '';
                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                             $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                             if(is_null($file)){
                                                                                 $status = 'no';
                                                                             }else{
                                                                                 $status = '';
                                                                             }


                                                                        @endphp
                                                                        @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                            Pack Not Uploaded
                                                                        @else
                                                                            Pack Not Sent
                                                                        @endif
                                                                    @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                        Submitted
                                                                    @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                        Rejected
                                                                    @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                        Resent
                                                                    @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                        Forwarded
                                                                    @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                        Approved
                                                                    @endif
                                                                @else
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if($ajStandardStage->postAudit->status == 'save')
                                                                        @php
                                                                            $status = '';
                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                             $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                             if(is_null($file)){
                                                                                 $status = 'no';
                                                                             }else{
                                                                                 $status = '';
                                                                             }


                                                                        @endphp
                                                                        @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                            Pack Not Uploaded$
                                                                        @else
                                                                            Pack Not Sent$
                                                                        @endif
                                                                    @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                        Submitted$
                                                                    @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                        Rejected$
                                                                    @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                        Resent$
                                                                    @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                        Forwarded$
                                                                    @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                        Approved$
                                                                    @endif
                                                                @else
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="pack_status_and_date"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if($ajStandardStage->postAudit->status == 'save')
                                                                        @php
                                                                            $status = '';
                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                             $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                             if(is_null($file)){
                                                                                 $status = 'no';
                                                                             }else{
                                                                                 $status = '';
                                                                             }


                                                                        @endphp
                                                                        @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                            Pack Not Uploaded
                                                                        @else
                                                                            Pack Not Sent
                                                                        @endif
                                                                    @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                        Submitted
                                                                    @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                        {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                    @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                        Resent
                                                                    @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                        Forwarded
                                                                    @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                        @if(!is_null($ajStandardStage->postAudit->postAuditSchemeInfo))
                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) }}
                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    @endif
                                                                @else
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if($ajStandardStage->postAudit->status == 'save')
                                                                        @php
                                                                            $status = '';
                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                             $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                             if(is_null($file)){
                                                                                 $status = 'no';
                                                                             }else{
                                                                                 $status = '';
                                                                             }


                                                                        @endphp
                                                                        @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                            Pack Not Uploaded$
                                                                        @else
                                                                            Pack Not Sent$
                                                                        @endif
                                                                    @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                        Submitted$
                                                                    @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                        {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}$
                                                                    @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                        Resent$
                                                                    @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                        Forwarded$
                                                                    @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                        @if(!is_null($ajStandardStage->postAudit->postAuditSchemeInfo))
                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) }}$
                                                                        @else
                                                                            N/A$
                                                                        @endif
                                                                    @endif
                                                                @else
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="onsite" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{ $ajStandardStage->onsite }}
                                                                <br>
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{ $ajStandardStage->onsite }}
                                                                $
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="offsite" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{ $ajStandardStage->offsite }}
                                                                <br>
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                {{ $ajStandardStage->offsite }}
                                                                $
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="standard_status"
                                                        style="display: none">{{ $companyStandard->certificates ? ucfirst($companyStandard->certificates->certificate_status) : 'Not Certified' }}</td>

                                                    <td data-is-export="0" data-comment-field="false" class="question_one"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @php
                                                                        $status = '';
                                                                        $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',1)->first();
                                                                    @endphp
                                                                    @if(!is_null($postAuditQuestions))
                                                                        {{ $postAuditQuestions->value }}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @php
                                                                        $status = '';
                                                                        $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',1)->first();
                                                                    @endphp
                                                                    @if(!is_null($postAuditQuestions))
                                                                        {{ $postAuditQuestions->value }}$
                                                                    @else
                                                                        N/A$
                                                                    @endif
                                                                @else
                                                                    N/A$
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>

                                                    <td data-is-export="0" data-comment-field="false" class="question_two"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @php
                                                                        $status = '';
                                                                        $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',2)->first();
                                                                    @endphp
                                                                    @if(!is_null($postAuditQuestions))
                                                                        {{ $postAuditQuestions->value }}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @php
                                                                        $status = '';
                                                                        $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',2)->first();
                                                                    @endphp
                                                                    @if(!is_null($postAuditQuestions))
                                                                        {{ $postAuditQuestions->value }}$
                                                                    @else
                                                                        N/A$
                                                                    @endif
                                                                @else
                                                                    N/A$
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="false" class="out_of_country_region_name" style="display: none">{{ $company->out_of_country_region_name ?? 'N/A' }}</td>
                                                    <td data-is-export="0" data-comment-field="false" class="key_change_remarks"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if(!is_null($ajStandardStage->postAudit) && isset($ajStandardStage->postAudit->key_change) && $ajStandardStage->postAudit->key_change === 1 || $ajStandardStage->postAudit->key_change === '1')
                                                                        {{ $ajStandardStage->postAudit->key_remarks ?? 'N/A' }}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="true" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if(!is_null($ajStandardStage->postAudit) && isset($ajStandardStage->postAudit->key_change) && $ajStandardStage->postAudit->key_change === 1 || $ajStandardStage->postAudit->key_change === '1')
                                                                        {{ $ajStandardStage->postAudit->key_remarks ?? 'N/A' }}$
                                                                    @else
                                                                        N/A$
                                                                    @endif
                                                                @else
                                                                    N/A$
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="pack_review_remarks"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if(!is_null($ajStandardStage->postAudit) && isset($ajStandardStage->postAudit->comments))
                                                                        {{ $ajStandardStage->postAudit->comments ?? 'N/A' }}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif
                                                                <br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td data-is-export="1" data-comment-field="true" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(!is_null($ajStandardStage->postAudit))
                                                                    @if(!is_null($ajStandardStage->postAudit) && isset($ajStandardStage->postAudit->comments))
                                                                        {{ $ajStandardStage->postAudit->comments ?? 'N/A' }}$
                                                                    @else
                                                                        N/A$
                                                                    @endif
                                                                @else
                                                                    N/A$
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </td>


                                                    <td data-is-export="0" data-comment-field="false" class="initial_certification_amount"
                                                        style="display: none">{{ ucfirst($companyStandard->initial_certification_currency) }}</td>
                                                    <td data-is-export="0" data-comment-field="false" class="initial_certification_amount"
                                                        style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if($ajStandardStage->audit_type === 'stage_1' || $ajStandardStage->audit_type === 'stage_2')
                                                                    @if($ajStandardStage->audit_type === 'stage_1')
                                                                        {{ $initial_certification_amount }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                @else
                                                                    @break
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td data-is-export="0" data-comment-field="false" class="surveillance_price"
                                                        style="display: none">{{ $surveillance_amount }}</td>
                                                    <td data-is-export="0" data-comment-field="false" class="re_audit_price"
                                                        style="display: none">{{ $re_audit_amount }}</td>


                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
    .daterangepicker .ranges .input-mini {
        font-size: 20px !important;
        height: 41px !important;
        width: 120px !important;
    }
    .daterangepicker .ranges .range_inputs>div:nth-child(2) {
        padding-left: 0px !important;
    }
    button.dt-button.buttons-excel.buttons-html5 {
        cursor: pointer !important;
    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>


        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#date_less_than_aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        // $('#date_less_than_aj_due_date').datepicker({
        //     format: 'dd-mm-yyyy',
        // });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        var fixNewLine = {
            exportOptions: {
                format: {
                    body: function (data, rowidx, colidx, node) {
                        if ($(node).data('is-export') === 1) {
                           if($(node).data('comment-field') === "true" || $(node).data('comment-field') === true){
                               return data.replace(/\$\s+/g, "$"); // Remove spaces after $ if followed by whitespace
                           }else{
                               return data.replace(/(\r\n|\n|\r|<br>| )/gm, "");
                           }
                        }else if ($(node).data('is-export') === 2) {
                            return data.replace(/(\r\n|\n|\r|<br>|&nbsp;| )/gm, "");
                        }else{
                            return data;
                        }

                    }
                }
            }
        };
        $('.datatable').DataTable({
            // Show length rows menu
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 600,

            "buttons": [
                $.extend(true, {}, fixNewLine, {
                    extend: 'excelHtml5',
                    title: "Audits Conducted Report",
                    filename: "Audits Conducted Report",
                    "createEmptyCells": true,
                    sheetName: "Audits Conducted Report",
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                })


            ],
        });
    </script>
@endpush


