@extends('layouts.master')
@section('title', "List of Withdrawn Clients")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Withdrawn Clients</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.surveillance-and-reaudit-due-withdrawn-with-actual-audit') }}"
                          method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="region_id">Region</label>
                                        <select class="form-control" id="region_id" name="region_id[]" multiple>
                                            <option value="" disabled>Select Region</option>
                                            @if(!empty($get_regions) && count($get_regions) > 0)
                                                @foreach($get_regions as $region)
                                                    <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country_id">Country</label>
                                        <select class="form-control" name="country_id[]" id="country_id" multiple>
                                            <option value="" disabled>Select Country</option>
                                            @if(!empty($get_countries) && count($get_countries) > 0)
                                                @foreach($get_countries as $country)
                                                    <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="withdrawn_due_date">Withdrawn Date </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            </div>
                                            <input type="text" name="withdrawn_due_date" id="withdrawn_due_date"
                                                   value=""
                                                   class="form-control float-right active withdrawn_due_date"
                                                   placeholder="dd//mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                            @endif


                        </div>
                        <div class="row" id="chboxes">

                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="region_field" id="region_field" value="region_field"
                                           onchange="hideItems(this, 'region_field')">
                                    <label>Region</label>
                                </div>
                            @endif

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="country" id="country" value="country"
                                       onchange="hideItems(this, 'country')">
                                <label>Country</label>
                            </div>

                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="surveillance_price" id="surveillance_price" value="surveillance_price"
                                           onchange="hideItems(this, 'surveillance_price')">
                                    <label>Surveillance Price</label>
                                </div>
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="re_audit_price" id="re_audit_price" value="re_audit_price"
                                           onchange="hideItems(this, 're_audit_price')">
                                    <label>Re-audit Price</label>
                                </div>
                            @endif


                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="planning_remarks" id="planning_remarks" value="planning_remarks"
                                       onchange="hideItems(this, 'planning_remarks')">
                                <label>Planning Remarks</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="standard_count" id="standard_count" value="standard_count"
                                       onchange="hideItems(this, 'standard_count')">
                                <label>Standard Count</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="job_number" id="job_number" value="job_number"
                                       onchange="hideItems(this, 'job_number')">
                                <label>Job Number</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="due_date" id="due_date" value="due_date"
                                       onchange="hideItems(this, 'due_date')">
                                <label>AJ Due Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="suspension_reason" id="suspension_reason" value="suspension_reason"
                                       onchange="hideItems(this, 'suspension_reason')">
                                <label>Suspension Stage</label>
                            </div>


                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.surveillance-and-reaudit-due-withdrawn-with-actual-audit') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th style="width: 3%">Sr No</th>
                                <th class="company_name" style="width: 15%">Name of Company</th>
                                <th class="standard" style="width: 10%;">Standard(s)</th>
                                <th class="job_number" style="display: none;">Job Number</th>
                                <th class="standard_count" style="display: none">Standard(s) Count</th>
                                <th class="due_date" style="display: none;width: 10%;">AJ Due Date</th>
                                <th class="suspension_reason" style="display: none">Suspension Stage</th>
                                <th class="suspension_date">Suspension Date</th>
                                <th class="withdrawn_date">Withdrawn Date</th>
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')
                                    <th class="region_field" style="display: none">Region</th>
                                @endif
                                <th class="country" style="display: none">Country</th>
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')
                                    <th class="surveillance_price" style="display: none">Unit</th>
                                    <th class="surveillance_price" style="display: none"> Surveillance Price</th>
                                    <th class="re_audit_price" style="display: none"> Re-audit Price</th>
                                @endif


                                <th class="planning_remarks" style="width: 30% !important;display: none">Planning
                                    Remarks
                                </th>

                                <th class="noExport" style="width: 3% !important;">Edit</th>

                            </tr>

                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)

                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td class="company_name"
                                                    style="width: 15%;">{{ ucfirst($company->name) }}</td>
                                                <td class="standard">


                                                    @php
                                                        $standardCount = 1;
                                                    @endphp
                                                    @if($companyStandard->is_ims == true)
                                                        @php
                                                            $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                            $company = \App\Company::where('id', $company->id)->first();
                                                            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                            $ims_heading = $companyStandard1->standard->name;
                                                            $surveillance_amount = (int)$companyStandard1->surveillance_amount;
                                                            $re_audit_amount = (int)$companyStandard1->re_audit_amount;


                                                            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                    if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                    continue;
                                                                    } else {
                                                                        $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                        $standardCount = $standardCount + 1;
                                                                        $surveillance_amount += (int)$imsCompanyStandard->surveillance_amount;
                                                                        $re_audit_amount += (int)$imsCompanyStandard->re_audit_amount;

                                                                    }
                                                                }
                                                            }
                                                        @endphp
                                                        {{ ($ims_heading) }}(IMS)
                                                    @else
                                                        @php
                                                            $surveillance_amount = $companyStandard->surveillance_amount;
                                                            $re_audit_amount =  $companyStandard->re_audit_amount;
                                                        @endphp
                                                        {{ $companyStandard->standard->name }}
                                                    @endif


                                                </td>
                                                <td class="job_number" style="display: none">
                                                    @php
                                                        $auditJustificationId = $company->auditJustifications[0]->id;
                                                        $ajStandard = \App\AJStandard::where('audit_justification_id',$auditJustificationId)->where('standard_id',$companyStandard->standard_id)->first(['id']);
                                                        $ajStandardStage = \App\AJStandardStage::where('aj_standard_id',$ajStandard->id)->first(['job_number']);
                                                    @endphp
                                                    {{ $ajStandardStage->job_number }}

                                                </td>
                                                <td class="standard_count" style="display: none">{{$standardCount}}</td>
                                                <td class="due_date" style="display: none;width: 10%;">
                                                    @if(isset($companyStandard->certificateSuspension) && !is_null($companyStandard->certificateSuspension) && !is_null($companyStandard->certificateSuspension->ajStandardStage))
                                                        {{ $companyStandard->certificateSuspension->ajStandardStage->excepted_date ? date('d-m-Y',strtotime($companyStandard->certificateSuspension->ajStandardStage->excepted_date)) : 'N/A'  }}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>

                                                <td class="suspension_reason" style="display: none">
                                                    @if(isset($companyStandard->certificateSuspension) && !is_null($companyStandard->certificateSuspension) && !is_null($companyStandard->certificateSuspension->ajStandardStage))
                                                        {{ ucfirst(str_replace('_',' ',$companyStandard->certificateSuspension->ajStandardStage->audit_type)) ?? 'N/A' }}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>

                                                <td class="suspension_date">
                                                    @if(isset($companyStandard->certificateSuspension) && !is_null($companyStandard->certificateSuspension))
                                                        {{ date('d-m-Y',strtotime($companyStandard->certificateSuspension->certificate_date)) }}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>

                                                <td class="withdrawn_date">
                                                    @if(isset($companyStandard->certificates) && !is_null($companyStandard->certificates))
                                                        {{ date('d-m-Y',strtotime($companyStandard->certificates->certificate_date)) }}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')

                                                    <td class="region_field"
                                                        style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                @endif
                                                <td class="country"
                                                    style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')
                                                    <td class="surveillance_price"
                                                        style="display: none">{{ ucfirst($companyStandard->initial_certification_currency) }}</td>
                                                    <td class="surveillance_price"
                                                        style="display: none">{{ $surveillance_amount }}</td>
                                                    <td class="re_audit_price"
                                                        style="display: none">{{ $re_audit_amount }}</td>
                                                @endif


                                                <td class="planning_remarks"
                                                    style="display: none;width: 30%">{{$companyStandard->main_remarks ?? 'N/A'}}
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)"
                                                       onclick="companyStandardRemarks('{{ $companyStandard->id }}')"><i
                                                                class="fa fa-edit"></i></a>
                                                    <a href="{{ route('company.show', $company->id) }}"
                                                       target="_blank" style="margin-left: 10px;"
                                                       title="View"><i
                                                                class="fa fa-eye"></i>
                                                    </a>
                                                </td>

                                            </tr>

                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')
    <div id="company_standard_remarks_html"></div>
@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }

    .daterangepicker .ranges .range_inputs > div:nth-child(2) {
        padding-left: 1px !important;
    }

    .daterangepicker .ranges .input-mini {
        border: 1px solid #ccc;
        border-radius: 4px;
        color: #555;
        display: block;
        font-size: 17px !important;
        height: 37px !important;
        line-height: 30px;
        vertical-align: middle;
        margin: 0 0 10px 0;
        padding: 0 6px;
        width: 160px !important;
    }

    .daterangepicker .daterangepicker_start_input label, .daterangepicker .daterangepicker_end_input label {
        font-size: 16px !important;

    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>


        $('#withdrawn_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });


        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
// Show length rows menu
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 400,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "List of Withdrawn Clients",
                    filename: "List of Withdrawn Clients",
                    "createEmptyCells": true,
                    sheetName: "List of Withdrawn Clients",
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }

                }


            ],
        });

        function companyStandardRemarks(companyStandardId) {
            var request = {"company_standard_id": companyStandardId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.company_standard_remarks')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#company_standard_remarks_html").html(response.data.html);
                        $('#companyStandardRemarksModal').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardRemarksSubmit(e, company_standard_id) {

            e.preventDefault();
            var form = $('#company-standard-remarks-store')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.company_standard_remarks.store') }}",
                enctype: 'multipart/form-data',
                // processData: false,
                // contentType: false,
                // cache: false,
                data: {
                    planning_remarks: $('#remarks' + company_standard_id).val(),
                    scheme_manager_remarks: '',
                    company_standard_id: company_standard_id
                },
                success: function (response) {
                    if (response.status == "success") {
                        $('#companyStandardRemarksModal').modal('hide');
                        toastr['success']("Remarks has been added Successfully.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);

                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    </script>
@endpush


