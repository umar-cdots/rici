@extends('layouts.master')
@section('title', "Surveillance and Reaudit Due Report")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')

    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Surveillance and Reaudit Due Report</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit') }}"
                          method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="region_id">Region</label>
                                        <select class="form-control" id="region_id" name="region_id[]" multiple>
                                            <option value="" disabled>Select Region</option>
                                            @if(!empty($get_regions) && count($get_regions) > 0)
                                                @foreach($get_regions as $region)
                                                    <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country_id">Country</label>
                                        <select class="form-control" name="country_id[]" id="country_id" multiple>
                                            <option value="" disabled>Select Country</option>
                                            @if(!empty($get_countries) && count($get_countries) > 0)
                                                @foreach($get_countries as $country)
                                                    <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                {{--                            <div class="col-md-4">--}}
                                {{--                                <div class="form-group">--}}
                                {{--                                    <label for="standard_id">Standard</label>--}}
                                {{--                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>--}}
                                {{--                                        <option value="" disabled>Select Standard</option>--}}
                                {{--                                        @if(!empty($get_standards) && count($get_standards) > 0)--}}
                                {{--                                            @foreach($get_standards as $standard)--}}
                                {{--                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>--}}
                                {{--                                            @endforeach--}}
                                {{--                                        @endif--}}

                                {{--                                    </select>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="audit_type">Audit Type</label>
                                        <select class="form-control" id="audit_type" name="audit_type">
                                            <option value="">Select Audit Type</option>
                                            <option value="surveillance_1">Surveillance 1</option>
                                            <option value="surveillance_2">Surveillance 2</option>
                                            <option value="surveillance_3">Surveillance 3</option>
                                            <option value="surveillance_4">Surveillance 4</option>
                                            <option value="surveillance_5">Surveillance 5</option>
                                            <option value="reaudit">Reaudit</option>


                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="issue_from">AJ Due Date </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            </div>
                                            <input type="text" name="aj_due_date" id="aj_due_date" value=""
                                                   class="form-control float-right active aj_due_date"
                                                   placeholder="dd//mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">AJ Due Date less than Current Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="date_less_than_aj_due_date"
                                               id="date_less_than_aj_due_date" value=""
                                               class="form-control float-right date_less_than_aj_due_date"
                                               placeholder="dd-mm-yyyy">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row" id="chboxes">
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="days_overdue" id="days_overdue"
                                       value="days_overdue"
                                       onchange="hideItems(this, 'days_overdue')">
                                <label>Audit OD By</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="audit_days_overdue" id="audit_days_overdue"
                                       value="audit_days_overdue"
                                       onchange="hideItems(this, 'audit_days_overdue')">
                                <label>Rep. OD By</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="pack_status_and_date" id="pack_status_and_date"
                                       value="pack_status_and_date"
                                       onchange="hideItems(this, 'pack_status_and_date')">
                                <label>Pack Status & Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="standard_status" id="standard_status" value="standard_status"
                                       onchange="hideItems(this, 'standard_status')">
                                <label>Standard Status</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="aj_status_and_date" id="aj_status_and_date"
                                       value="aj_status_and_date"
                                       onchange="hideItems(this, 'aj_status_and_date')">
                                <label>AJ Status & Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="exptected_start_date" id="exptected_start_date"
                                       value="exptected_start_date"
                                       onchange="hideItems(this, 'exptected_start_date')">
                                <label>Expected Start Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="certificate_expiry_date" id="certificate_expiry_date"
                                       value="certificate_expiry_date"
                                       onchange="hideItems(this, 'certificate_expiry_date')">
                                <label>Certificate Expiry Date</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="onsite" id="onsite" value="onsite"
                                       onchange="hideItems(this, 'onsite')">
                                <label>Onsite Mandays</label>
                            </div>
                            @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="certificate_reason" id="certificate_reason" value="certificate_reason"
                                           onchange="hideItems(this, 'certificate_reason')">
                                    <label>Certification Status Reason</label>
                                </div>
                            @endif
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="due_month_in_digit" id="due_month_in_digit" value="due_month_in_digit"
                                           onchange="hideItems(this, 'due_month_in_digit')">
                                    <label>Due Month in Digits</label>
                                </div>
                            @endif
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="planning_remarks" id="planning_remarks" value="planning_remarks"
                                       onchange="hideItems(this, 'planning_remarks')">
                                <label>Planning Remarks</label>
                            </div>
                            @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="due_month" id="due_month"
                                           value="due_month"
                                           onchange="hideItems(this, 'due_month')">
                                    <label>Days Month</label>
                                </div>
                            @endif
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_price" id="surveillance_price" value="surveillance_price"
                                       onchange="hideItems(this, 'surveillance_price')">
                                <label>Surveillance Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_price" id="re_audit_price" value="re_audit_price"
                                       onchange="hideItems(this, 're_audit_price')">
                                <label>Re-audit Price</label>
                            </div>
                            @if (auth()->user()->user_type === 'scheme_manager')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="offsite" id="offsite" value="offsite"
                                           onchange="hideItems(this, 'offsite')">
                                    <label>Offsite Mandays</label>
                                </div>
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="grades" id="grades"
                                           value="grades"
                                           onchange="hideItems(this, 'grades')">
                                    <label>Grades</label>
                                </div>
                            @endif
                            {{--                            @if(auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator')--}}
                            {{--                                <div class="col-md-3 floting">--}}
                            {{--                                    <input type="checkbox" class=""--}}
                            {{--                                           name="cs_code" id="cs_code" value="cs_code"--}}
                            {{--                                           onchange="hideItems(this, 'cs_code')">--}}
                            {{--                                    <label>Code</label>--}}
                            {{--                                </div>--}}
                            {{--                            @endif--}}
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="region_field" id="region_field" value="region_field"
                                           onchange="hideItems(this, 'region_field')">
                                    <label>Region</label>
                                </div>
                            @endif

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="country" id="country" value="country"
                                       onchange="hideItems(this, 'country')">
                                <label>Country</label>
                            </div>
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="city" id="city" value="city"
                                           onchange="hideItems(this, 'city')">
                                    <label>City</label>
                                </div>
                            @endif
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="question_one" id="question_one" value="question_one"
                                       onchange="hideItems(this, 'question_one')">
                                <label>CAT I CAR</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="question_two" id="question_two" value="question_two"
                                       onchange="hideItems(this, 'question_two')">
                                <label>CAT II CAR</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="out_of_country_region_name" id="out_of_country_region_name"
                                       value="out_of_country_region_name"
                                       onchange="hideItems(this, 'out_of_country_region_name')">
                                <label>Country # 2</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="contact_person" id="contact_person" value="contact_person"
                                       onchange="hideItems(this, 'contact_person')">
                                <label>Contact Person</label>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th style="width: 3%">Sr No</th>
                                {{--                                <th class="cs_code" style="display: none">Code</th>--}}
                                <th class="company_name" style="width: 10%">Name of Company</th>
                                <th class="standard" style="width: 10%;">Standard(s)</th>
                                <th class="audit_type" style="width: 10% !important;">Audit Type</th>

                                <th class="expected_due_date" style="width: 10% !important;">Due Date</th>
                                <th class="certificate_expiry_date" style="display: none">Certificate Expiry Date</th>
                                <th class="due_month" style="display:none;">Due Month</th>
                                <th class="exptected_start_date" style="display: none">Expected Start Date</th>
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                    <th class="due_month_in_digit" style="display: none">Due Month In Digit</th>
                                @endif
                                <th class="days_overdue" style="display: none">Audit OD By</th>
                                <th class="actual_audit_date" style="width: 9% !important;">Audit Date
                                </th>
                                <th class="audit_days_overdue" style="display: none">Rep. OD By
                                </th>
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                    <th class="region_field" style="display: none">Region</th>
                                @endif
                                <th class="country" style="display: none">Country</th>
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                    <th class="city" style="display: none">City</th>
                                @endif
                                <th class="aj_status_and_date" style="width: 135px; display: none;">Aj Status</th>
                                <th class="aj_status_and_date" style="width: 135px;display: none;">Aj Date</th>
                                <th class="pack_status_and_date" style="display: none">Pack Status
                                </th>
                                <th class="pack_status_and_date" style="display: none">Pack Date
                                </th>
                                <th class="standard_status" style="display: none">Status
                                </th>
                                <th class="certificate_reason" style="display: none">Certification Status Reason
                                </th>
                                <th class="surveillance_price" style="display: none">Surveillance Unit</th>
                                <th class="surveillance_price" style="display: none"> Surveillance Price</th>
                                <th class="re_audit_price" style="display: none"> Re-audit Price</th>

                                <th class="planning_remarks" style="width: 30% !important;">Planning Remarks
                                </th>
                                <th data-is-export="0" class="onsite" style="display: none">Onsite Mandays</th>
                                @if (auth()->user()->user_type === 'scheme_manager')
                                    <th data-is-export="0" class="offsite" style="display: none">Offsite Mandays</th>
                                    <th class="grades" style="display: none">Grades</th>
                                @endif
                                <th class="question_one" style="display: none">CAT I CAR</th>
                                <th class="question_two" style="display: none">CAT II CAR</th>
                                <th class="out_of_country_region_name" style="display: none">Country # 2</th>
                                <th class="contact_person" style="display: none">Name</th>
                                <th class="contact_person" style="display: none">Designation</th>
                                <th class="contact_person" style="display: none">Contact No</th>
                                <th class="contact_person" style="display: none">Landline No</th>
                                <th class="contact_person" style="display: none">Email</th>
                                @if (auth()->user()->user_type === 'operation_manager' && auth()->user()->region->title === 'Pakistan Region')
                                    <th class="noExport" style="width: 3% !important;">Print</th>
                                @endif
                                <th class="noExport" style="width: 3% !important;">Edit</th>

                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                @foreach($companyStandard->companyStandardCodes as $key => $companyStandardCode)
                                                    @if($key == 0)

                                                        @if( isset($_REQUEST['audit_type']) && !is_null($_REQUEST['audit_type']) && $_REQUEST['audit_type'] != "")

                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                @php
                                                                    $check =false;
                                                                    $auditCheck =false;
                                                                @endphp
                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                        @php
                                                                            $check =true;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if($check == true)

                                                                    <tr>
                                                                        <td>{{ $i++ }}</td>
                                                                        {{--                                                                        <td class="cs_code"--}}
                                                                        {{--                                                                            style="display: none">{{ $companyStandard->id }}</td>--}}
                                                                        <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                        <td class="standard">
                                                                            @if($companyStandard->is_ims == true)
                                                                                @php
                                                                                    $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                    $company = \App\Company::where('id', $company->id)->first();
                                                                                    $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                    $ims_heading = $companyStandard1->standard->name;


                                                                                    if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                        foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                            if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                            continue;
                                                                                            } else {
                                                                                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                @endphp
                                                                                {{ ($ims_heading) }}(IMS)
                                                                            @else
                                                                                {{ $companyStandardCode->standard->name }}
                                                                            @endif


                                                                        </td>
                                                                        <td class="audit_type">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}} @if($ajStandardStage->audit_type === 'surveillance_1' && is_null($ajStandardStage->recycle))
                                                                                            -I
                                                                                        @endif
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="exptected_due_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="certificate_expiry_date"
                                                                            style="display: none">
                                                                            @php
                                                                                $latest_dates = \App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use ($companyStandard){
                                                                                        $q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);
                                                                                })->latest()->first(['new_expiry_date']);
                                                                            @endphp
                                                                            {{ (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) ? date('d-m-Y',strtotime(($latest_dates->new_expiry_date))) : '' }}

                                                                        </td>
                                                                        <td class="due_month" style="display: none;">

                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="exptected_start_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'] && !is_null($ajStandardStage->actual_expected_date))
                                                                                        {{date('d-m-Y',strtotime($ajStandardStage->actual_expected_date))}}
                                                                                        <br>
                                                                                    @endif

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                                                            <td class="due_month_in_digit"
                                                                                style="display: none">

                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{date('m',strtotime($ajStandardStage->excepted_date))}}
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                        @endif
                                                                        <td class="days_overdue"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @php

                                                                                            $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                            $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                            $diff = $endDate->diffInDays($now);
                                                                                        @endphp

                                                                                        @if($endDate < $now)
                                                                                            +{{ $diff }}

                                                                                        @else
                                                                                            -{{ $diff }}

                                                                                        @endif
                                                                                    @endif

                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="actual_due_date"
                                                                            style="width: 57px;">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if(isset($actual_audit_date_each_stage))
                                                                                            {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="audit_days_overdue"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if(isset($actual_audit_date_each_stage))

                                                                                            @php

                                                                                                $endDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                                                $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                                $diff = $endDate->diffInDays($now);
                                                                                            @endphp

                                                                                            @if($endDate < $now)
                                                                                                +{{ $diff }}

                                                                                            @else
                                                                                                -{{ $diff }}

                                                                                            @endif
                                                                                            {{--                                                                                            {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}--}}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                                                            <td class="region_field"
                                                                                style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                                        @endif
                                                                        <td class="country"
                                                                            style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                                                            <td class="city"
                                                                                style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                        @endif
                                                                        <td class="aj_status_and_date"
                                                                            style="display: none;">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if($ajStandardStage->status == 'approved')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @elseif($ajStandardStage->status == 'rejected')
                                                                                            {{ ucfirst($ajStandardStage->status) }}

                                                                                        @else
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="aj_status_and_date"
                                                                            style="display: none;">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if($ajStandardStage->status == 'approved')
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}

                                                                                        @elseif($ajStandardStage->status == 'rejected')
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}

                                                                                        @else
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @endif
                                                                                        <br>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="pack_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(!is_null($ajStandardStage->postAudit))
                                                                                            @if($ajStandardStage->postAudit->status == 'save')
                                                                                                @php
                                                                                                    $status = '';
                                                                                                    $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                     $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                     if(is_null($file)){
                                                                                                         $status = 'no';
                                                                                                     }else{
                                                                                                         $status = '';
                                                                                                     }


                                                                                                @endphp
                                                                                                @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                    Pack
                                                                                                    Not
                                                                                                    Uploaded
                                                                                                @else
                                                                                                    Pack
                                                                                                    Not
                                                                                                    Sent
                                                                                                @endif
                                                                                            @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                                Submitted
                                                                                            @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                                Rejected

                                                                                            @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                                Resent
                                                                                            @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                                Forwarded
                                                                                            @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                                Approved
                                                                                            @endif
                                                                                        @else
                                                                                        @endif
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="pack_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                        @if(!is_null($ajStandardStage->postAudit))
                                                                                            @if($ajStandardStage->postAudit->status == 'save')
                                                                                                @php
                                                                                                    $status = '';
                                                                                                    $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                     $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                     if(is_null($file)){
                                                                                                         $status = 'no';
                                                                                                     }else{
                                                                                                         $status = '';
                                                                                                     }


                                                                                                @endphp
                                                                                                @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                    Pack Not Uploaded
                                                                                                @else
                                                                                                    Pack Not Sent
                                                                                                @endif
                                                                                            @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                            @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                            @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                                Resent
                                                                                            @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                                Forwarded
                                                                                            @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                                {{ $ajStandardStage->postAudit && $ajStandardStage->postAudit->postAuditSchemeInfo ? date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) : '' }}

                                                                                            @endif
                                                                                        @else
                                                                                        @endif
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="standard_status"
                                                                            style="display: none">

                                                                            @if($companyStandard->certificates)
                                                                                @if($companyStandard->certificates->certificate_status == 'certified')
                                                                                    Active
                                                                                @else
                                                                                    {{ ucfirst($companyStandard->certificates->certificate_status) }} {{ date('d-m-Y',strtotime($companyStandard->certificates->certificate_date)) }}
                                                                                @endif
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                            {{--                                                                            {{ $companyStandard->certificates ? ucfirst($companyStandard->certificates->certificate_status) : 'N/A' }}--}}
                                                                        </td>
                                                                        <td class="certificate_reason"
                                                                            style="display: none">

                                                                            @if($companyStandard->certificates)
                                                                                @if($companyStandard->certificates->certificate_status == 'suspended')
                                                                                    {{!is_null($companyStandard->certificates->suspension_reason) ? ucfirst(str_replace('_',' ',$companyStandard->certificates->suspension_reason)) : 'N/A'}}
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>

                                                                        @if($companyStandard->is_ims == true)
                                                                            @php
                                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                $ims_heading = $companyStandard1->standard->name;
                                                                                $surveillance_amount = (int)$companyStandard1->surveillance_amount;
                                                                                $re_audit_amount = (int)$companyStandard1->re_audit_amount;



                                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                        continue;
                                                                                        } else {

                                                                                        $surveillance_amount += (int)$imsCompanyStandard->surveillance_amount;
                                                                                        $re_audit_amount += (int)$imsCompanyStandard->re_audit_amount;

                                                                                        }
                                                                                    }
                                                                                }
                                                                            @endphp
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ $surveillance_amount}}</td>
                                                                            <td class="re_audit_price"
                                                                                style="display: none">{{ $re_audit_amount }}</td>
                                                                        @else
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ $companyStandard->surveillance_amount }}</td>

                                                                            <td class="re_audit_price"
                                                                                style="display: none">{{ $companyStandard->re_audit_amount }}</td>
                                                                        @endif
                                                                        <td class="planning_remarks">{{$companyStandard->main_remarks ?? 'N/A'}}
                                                                        </td>
                                                                        <td data-is-export="0" class="onsite"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    {{ $ajStandardStage->onsite }}
                                                                                    <br>
                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        @if (auth()->user()->user_type === 'scheme_manager')
                                                                            <td data-is-export="0" class="offsite"
                                                                                style="display: none">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{ $ajStandardStage->offsite }}
                                                                                        <br>
                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="grades" style="display: none">
                                                                                @if(!is_null($companyStandard->grade))
                                                                                    {{ $companyStandard->grade }}
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                        @endif

                                                                        <td class="question_one"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @php
                                                                                            $status = '';
                                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',1)->first();
                                                                                        @endphp
                                                                                        @if(!is_null($postAuditQuestions))
                                                                                            {{ $postAuditQuestions->value }}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="question_two"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @php
                                                                                            $status = '';
                                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',2)->first();
                                                                                        @endphp
                                                                                        @if(!is_null($postAuditQuestions))
                                                                                            {{ $postAuditQuestions->value }}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="out_of_country_region_name" style="display: none">{{ $company->out_of_country_region_name ?? 'N/A' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->title) : '' }} {{ $company->primaryContact ? ucfirst($company->primaryContact->name) : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->position) : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->contact) : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? $company->primaryContact->landline ?? '-' : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? $company->primaryContact->email : '' }}</td>

                                                                        @if (auth()->user()->user_type === 'operation_manager' && auth()->user()->region->title === 'Pakistan Region')
                                                                            <td>
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            <a href="{{ route('invoice.letter.index',[$company->id,$companyStandard->id,$ajStandardStage->aj_standard_id,$ajStandardStage->id]) }}">
                                                                                                <i class="fa fa-print"></i>
                                                                                            </a>
                                                                                        @endif
                                                                                        <br>
                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                        @endif
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                               onclick="companyStandardRemarks('{{ $companyStandard->id }}')"><i
                                                                                        class="fa fa-edit"></i></a>
                                                                            <a href="{{ route('company.show', $company->id) }}"
                                                                               target="_blank"
                                                                               style="margin-left: 10px;"
                                                                               title="View"><i
                                                                                        class="fa fa-eye"></i>
                                                                            </a>
                                                                        </td>


                                                                    </tr>

                                                                @endif

                                                            @endif
                                                        @else
                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                @php
                                                                    $check =false;
                                                                    $auditCheck =false;
                                                                @endphp
                                                                @if(!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0)
                                                                    @php
                                                                        $check =true;
                                                                    @endphp
                                                                @endif
                                                                @if($check == true)

                                                                    <tr>
                                                                        <td>{{ $i++ }}</td>
                                                                        {{--                                                                        <td class="cs_code"--}}
                                                                        {{--                                                                            style="display: none">{{ $companyStandard->id }}</td>--}}
                                                                        <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                        <td class="standard">


                                                                            @if($companyStandard->is_ims == true)
                                                                                @php
                                                                                    $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                    $company = \App\Company::where('id', $company->id)->first();
                                                                                    $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                    $ims_heading = $companyStandard1->standard->name;


                                                                                    if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                        foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                            if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                            continue;
                                                                                            } else {
                                                                                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                @endphp
                                                                                {{ ($ims_heading) }}(IMS)
                                                                            @else
                                                                                {{ $companyStandardCode->standard->name }}
                                                                            @endif


                                                                        </td>
                                                                        <td class="audit_type">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}@if($ajStandardStage->audit_type === 'surveillance_1' && is_null($ajStandardStage->recycle))
                                                                                        -I
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="exptected_due_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="certificate_expiry_date"
                                                                            style="display: none">
                                                                            @php
                                                                                $latest_dates = \App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use ($companyStandard){
                                                                                        $q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);
                                                                                })->latest()->first(['new_expiry_date']);
                                                                            @endphp
                                                                            {{ (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) ? date('d-m-Y',strtotime(($latest_dates->new_expiry_date))) : '' }}

                                                                        </td>
                                                                        <td class="due_month" style="display:none;">

                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        <td class="exptected_start_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->actual_expected_date))
                                                                                        {{date('d-m-Y',strtotime($ajStandardStage->actual_expected_date))}}
                                                                                        <br>
                                                                                    @endif

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                                                            <td class="due_month_in_digit"
                                                                                style="display: none">

                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{date('m',strtotime($ajStandardStage->excepted_date))}}
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                        @endif
                                                                        <td class="days_overdue"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)

                                                                                    @php

                                                                                        $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                        $now = Carbon\Carbon::now();
                                                                                        $diff = $endDate->diffInDays($now);
                                                                                    @endphp

                                                                                    @if($endDate < $now)
                                                                                        +{{ $diff }}

                                                                                    @else
                                                                                        -{{ $diff }}

                                                                                    @endif

                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="actual_due_date">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @php
                                                                                        $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                        $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                    @endphp
                                                                                    @if(isset($actual_audit_date_each_stage))
                                                                                        {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        <td class="audit_days_overdue"
                                                                            style="display: none">

                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @php
                                                                                        $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                        $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                        })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                    @endphp
                                                                                    @if(isset($actual_audit_date_each_stage))
                                                                                        @php

                                                                                            $endDate = Carbon\Carbon::parse($actual_audit_date_each_stage->actual_audit_date);
                                                                                            $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                            $diff = $endDate->diffInDays($now);
                                                                                        @endphp

                                                                                        @if($endDate < $now)
                                                                                            +{{ $diff }}

                                                                                        @else
                                                                                            -{{ $diff }}

                                                                                        @endif
                                                                                        {{--                                                                                        {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}--}}

                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>

                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif


                                                                        </td>
                                                                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                                                            <td class="region_field"
                                                                                style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                                        @endif
                                                                        <td class="country"
                                                                            style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                                                            <td class="city"
                                                                                style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                        @endif
                                                                        <td class="aj_status_and_date"
                                                                            style="display: none;">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->status == 'approved')
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                    @elseif($ajStandardStage->status == 'rejected')
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                    @else
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="aj_status_and_date"
                                                                            style="display: none;">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if($ajStandardStage->status == 'approved')
                                                                                        {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}

                                                                                    @elseif($ajStandardStage->status == 'rejected')
                                                                                        {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                    @else
                                                                                        {{ ucfirst($ajStandardStage->status) }}
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="pack_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @if($ajStandardStage->postAudit->status == 'save')
                                                                                            @php
                                                                                                $status = '';
                                                                                                $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                 $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                 if(is_null($file)){
                                                                                                     $status = 'no';
                                                                                                 }else{
                                                                                                     $status = '';
                                                                                                 }


                                                                                            @endphp
                                                                                            @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                Pack Not Uploaded
                                                                                            @else
                                                                                                Pack Not Sent
                                                                                            @endif
                                                                                        @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                            Submitted
                                                                                        @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                            Rejected
                                                                                        @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                            Resent
                                                                                        @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                            Forwarded
                                                                                        @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                            Approved
                                                                                        @endif
                                                                                    @else
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="pack_status_and_date"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @if($ajStandardStage->postAudit->status == 'save')
                                                                                            @php
                                                                                                $status = '';
                                                                                                $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                 $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                 if(is_null($file)){
                                                                                                     $status = 'no';
                                                                                                 }else{
                                                                                                     $status = '';
                                                                                                 }


                                                                                            @endphp
                                                                                            @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                Pack Not Uploaded
                                                                                            @else
                                                                                                Pack Not Sent
                                                                                            @endif
                                                                                        @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                        @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                        @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                            Resent
                                                                                        @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                            Forwarded
                                                                                        @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                            {{ $ajStandardStage->postAudit && $ajStandardStage->postAudit->postAuditSchemeInfo ? date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) : '' }}

                                                                                        @endif
                                                                                    @else
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="standard_status"
                                                                            style="display: none">

                                                                            @if($companyStandard->certificates)
                                                                                @if($companyStandard->certificates->certificate_status == 'certified')
                                                                                    Active
                                                                                @else
                                                                                    {{ ucfirst($companyStandard->certificates->certificate_status) }} {{ date('d-m-Y',strtotime($companyStandard->certificates->certificate_date)) }}
                                                                                @endif
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                            {{--                                                                            {{ $companyStandard->certificates ? ucfirst($companyStandard->certificates->certificate_status) : 'N/A' }}--}}
                                                                        </td>
                                                                        <td class="certificate_reason"
                                                                            style="display: none">

                                                                            @if($companyStandard->certificates)
                                                                                @if($companyStandard->certificates->certificate_status == 'suspended')
                                                                                    {{!is_null($companyStandard->certificates->suspension_reason) ? ucfirst(str_replace('_',' ',$companyStandard->certificates->suspension_reason)) : 'N/A'}}
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            @else
                                                                                N/A
                                                                            @endif
                                                                        </td>
                                                                        @if($companyStandard->is_ims == true)
                                                                            @php
                                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                $ims_heading = $companyStandard1->standard->name;
                                                                                $surveillance_amount = (int)$companyStandard1->surveillance_amount;
                                                                                $re_audit_amount = (int)$companyStandard1->re_audit_amount;



                                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                        continue;
                                                                                        } else {

                                                                                        $surveillance_amount += (int)$imsCompanyStandard->surveillance_amount;
                                                                                        $re_audit_amount += (int)$imsCompanyStandard->re_audit_amount;

                                                                                        }
                                                                                    }
                                                                                }
                                                                            @endphp
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ $surveillance_amount }}</td>

                                                                            <td class="re_audit_price"
                                                                                style="display: none">{{ $re_audit_amount }}</td>
                                                                        @else
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ ucfirst($companyStandard->surveillance_currency) }}</td>
                                                                            <td class="surveillance_price"
                                                                                style="display: none">{{ $companyStandard->surveillance_amount }}</td>
                                                                            <td class="re_audit_price"
                                                                                style="display: none">{{ $companyStandard->re_audit_amount }}</td>
                                                                        @endif
                                                                        <td class="planning_remarks">{{$companyStandard->main_remarks ?? 'N/A'}}
                                                                        </td>
                                                                        <td data-is-export="0" class="onsite"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    {{ $ajStandardStage->onsite }}
                                                                                    <br>
                                                                                @endforeach
                                                                            @else
                                                                                N/A
                                                                            @endif

                                                                        </td>
                                                                        @if (auth()->user()->user_type === 'scheme_manager')
                                                                            <td data-is-export="0" class="offsite"
                                                                                style="display: none">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{ $ajStandardStage->offsite }}
                                                                                        <br>
                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="grades" style="display: none">
                                                                                @if(!is_null($companyStandard->grade))
                                                                                    {{ $companyStandard->grade }}
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                        @endif
                                                                        <td class="question_one"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @php
                                                                                            $status = '';
                                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',1)->first();
                                                                                        @endphp
                                                                                        @if(!is_null($postAuditQuestions))
                                                                                            {{ $postAuditQuestions->value }}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="question_two"
                                                                            style="display: none">
                                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                    @if(!is_null($ajStandardStage->postAudit))
                                                                                        @php
                                                                                            $status = '';
                                                                                            $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',2)->first();
                                                                                        @endphp
                                                                                        @if(!is_null($postAuditQuestions))
                                                                                            {{ $postAuditQuestions->value }}
                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                    @else
                                                                                        N/A
                                                                                    @endif
                                                                                    <br>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td class="out_of_country_region_name" style="display: none">{{ $company->out_of_country_region_name ?? 'N/A' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->title) : '' }} {{ $company->primaryContact ? ucfirst($company->primaryContact->name) : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->position) : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->contact) : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? $company->primaryContact->landline ?? '-' : '' }}</td>
                                                                        <td class="contact_person"
                                                                            style="display: none">{{ $company->primaryContact ? $company->primaryContact->email : '' }}</td>
                                                                        @if (auth()->user()->user_type === 'operation_manager' && auth()->user()->region->title === 'Pakistan Region')
                                                                            <td>
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @php
                                                                                            $invoice = \App\Invoice::where(['company_id' => $company->id, 'company_standard_id' => $companyStandard->id, 'aj_standard_id' => $ajStandardStage->aj_standard_id, 'aj_standard_stage_id' => $ajStandardStage->id])->first();
                                                                                        @endphp
                                                                                        @if(!is_null($invoice))
                                                                                            @if($invoice->letter_one_print  === 1)
                                                                                                L1,&nbsp;
                                                                                            @endif
                                                                                            @if($invoice->letter_two_print  === 1)
                                                                                                L2,&nbsp;
                                                                                            @endif
                                                                                            @if($invoice->letter_three_print  === 1)
                                                                                                L3,&nbsp;
                                                                                            @endif
                                                                                            @if($invoice->invoice_status  === 1)
                                                                                                P
                                                                                            @else
                                                                                                <a href="{{ route('invoice.letter.index',[$company->id,$companyStandard->id,$ajStandardStage->aj_standard_id,$ajStandardStage->id]) }}">
                                                                                                    <i class="fa fa-print"></i>
                                                                                                </a>
                                                                                            @endif
                                                                                        @else
                                                                                            <a href="{{ route('invoice.letter.index',[$company->id,$companyStandard->id,$ajStandardStage->aj_standard_id,$ajStandardStage->id]) }}">
                                                                                                <i class="fa fa-print"></i>
                                                                                            </a>
                                                                                        @endif

                                                                                        <br>
                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                        @endif
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                               onclick="companyStandardRemarks('{{ $companyStandard->id }}')"><i
                                                                                        class="fa fa-edit"></i></a>
                                                                            <a href="{{ route('company.show', $company->id) }}"
                                                                               target="_blank"
                                                                               style="margin-left: 10px;"
                                                                               title="View"><i
                                                                                        class="fa fa-eye"></i>
                                                                            </a>
                                                                        </td>


                                                                    </tr>

                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')
    <div id="company_standard_remarks_html"></div>
@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }

    .daterangepicker .ranges .input-mini {
        font-size: 20px !important;
        height: 41px !important;
        width: 120px !important;
    }

    .daterangepicker .ranges .range_inputs > div:nth-child(2) {
        padding-left: 0px !important;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        cursor: pointer !important;
    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>


        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#date_less_than_aj_due_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
// Show length rows menu
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 400,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: null,
                    filename: "Surveillance and Reaudit Due Report",
                    "createEmptyCells": true,
                    sheetName: "Surveillance and Reaudit Due Report",
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }

                }


            ],
        });

        function companyStandardRemarks(companyStandardId) {
            var request = {"company_standard_id": companyStandardId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.company_standard_remarks')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#company_standard_remarks_html").html(response.data.html);
                        $('#companyStandardRemarksModal').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardRemarksSubmit(e, company_standard_id) {

            e.preventDefault();
            var form = $('#company-standard-remarks-store')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.company_standard_remarks.store') }}",
                enctype: 'multipart/form-data',
// processData: false,
// contentType: false,
// cache: false,
                data: {
                    planning_remarks: $('#remarks' + company_standard_id).val(),
                    scheme_manager_remarks: '',
                    company_standard_id: company_standard_id
                },
                success: function (response) {
                    if (response.status == "success") {
                        $('#companyStandardRemarksModal').modal('hide');
                        toastr['success']("Remarks has been added Successfully.");
// setTimeout(function () {
//     window.location.reload();
// }, 1000);

                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    </script>
@endpush


