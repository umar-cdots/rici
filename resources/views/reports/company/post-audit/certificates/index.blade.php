@extends('layouts.master')
@section('title', "List of Certified Clients")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Certified Clients</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.list-of-certificates-index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="region_id">Region</label>
                                        <select class="form-control" id="region_id" name="region_id[]" multiple>
                                            <option value="" disabled>Select Region</option>
                                            @if(!empty($get_regions) && count($get_regions) > 0)
                                                @foreach($get_regions as $region)
                                                    <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country_id">Country</label>
                                        <select class="form-control" name="country_id[]" id="country_id" multiple>
                                            <option value="" disabled>Select Country</option>
                                            @if(!empty($get_countries) && count($get_countries) > 0)
                                                @foreach($get_countries as $country)
                                                    <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="standard_id">Standard</label>
                                        <select class="form-control" id="standard_id" name="standard_id[]" multiple>
                                            <option value="" disabled>Select Standard</option>
                                            @if(!empty($get_standards) && count($get_standards) > 0)
                                                @foreach($get_standards as $standard)
                                                    <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="status">Certification Status</label>
                                        <select class="form-control" name="status" id="status">
                                            <option value="">Select Certification Status</option>
                                            {{--                                        <option value="not_certified">Not Certified</option>--}}
                                            <option value="certified">Certified</option>
                                            <option value="suspended">Suspended</option>
                                            <option value="withdrawn">Withdrawn</option>
                                            {{--                                        <option value="withdrawn">Withdrawn</option>--}}
                                            {{--                                        <option value="closed">Closed</option>--}}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="issue_from">Approval / Issue Date </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            </div>
                                            <input type="text" name="issue_from" id="issue_from" value=""
                                                   class="form-control float-right active issue_from"
                                                   placeholder="dd//mm/yyyy">
                                        </div>
                                    </div>
                                </div>


                            </div>
                        @endif
                        <div class="row" id="chboxes">

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="unique_code" id="unique_code" value="unique_code"
                                       onchange="hideItems(this, 'unique_code')">
                                <label>Accreditation & Codes</label>
                            </div>
                            @if(auth()->user()->user_type === 'scheme_manager')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="all_accreditation" id="all_accreditation" value="all_accreditation"
                                           onchange="hideItems(this, 'all_accreditation')">
                                    <label>All Accreditation</label>
                                </div>
                            @endif

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="job_number" id="job_number" value="job_number"
                                       onchange="hideItems(this, 'job_number')">
                                <label>Job Number</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="post_code" id="post_code" value="post_code"
                                       onchange="hideItems(this, 'post_code')">
                                <label>Post Code</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="addess" id="addess" value="addess"
                                       onchange="hideItems(this, 'addess')">
                                <label>Address</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="scope" id="scope" value="scope"
                                       onchange="hideItems(this, 'scope')">
                                <label>Scope</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="contact_person" id="contact_person" value="contact_person"
                                       onchange="hideItems(this, 'contact_person')">
                                <label>Contact Person</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="original_issue_date" id="original_issue_date" value="original_issue_date"
                                       onchange="hideItems(this, 'original_issue_date')">
                                <label>Original I. Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="expiry_date" id="expiry_date" value="expiry_date"
                                       onchange="hideItems(this, 'expiry_date')">
                                <label>Expiry Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="last_certificate_issue_date" id="last_certificate_issue_date"
                                       value="last_certificate_issue_date"
                                       onchange="hideItems(this, 'last_certificate_issue_date')">
                                <label>Last Certificate I. Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="withdrawn_date" id="withdrawn_date" value="withdrawn_date"
                                       onchange="hideItems(this, 'withdrawn_date')">
                                <label>Certification Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="transfer_case" id="transfer_case" value="transfer_case"
                                       onchange="hideItems(this, 'transfer_case')">
                                <label>Transfer Case</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="initial_certification_amount" id="initial_certification_amount"
                                       value="initial_certification_amount"
                                       onchange="hideItems(this, 'initial_certification_amount')">
                                <label>Initial Certification Amount</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_price" id="surveillance_price" value="surveillance_price"
                                       onchange="hideItems(this, 'surveillance_price')">
                                <label>Surveillance Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="re_audit_price" id="re_audit_price" value="re_audit_price"
                                       onchange="hideItems(this, 're_audit_price')">
                                <label>Re-audit Price</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="actual_audit_start_date" id="actual_audit_start_date"
                                       value="actual_audit_start_date"
                                       onchange="hideItems(this, 'actual_audit_start_date')">
                                <label>Stage 2 Start Date</label>
                            </div>
                            @if (auth()->user()->user_type === 'scheme_manager')
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="grades" id="grades"
                                           value="grades"
                                           onchange="hideItems(this, 'grades')">
                                    <label>Grades</label>
                                </div>
                                <div class="col-md-3 floting">
                                    <input type="checkbox" class=""
                                           name="multiple_sides" id="multiple_sides"
                                           value="multiple_sides"
                                           onchange="hideItems(this, 'multiple_sides')">
                                    <label>Total Sites (Manual)</label>
                                </div>
                            @endif
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="out_of_country_region_name" id="out_of_country_region_name"
                                       value="out_of_country_region_name"
                                       onchange="hideItems(this, 'out_of_country_region_name')">
                                <label>Country # 2</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="locations" id="locations"
                                       value="locations"
                                       onchange="hideItems(this, 'locations')">
                                <label>Total Locations</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="shifts" id="shifts"
                                       value="shifts"
                                       onchange="hideItems(this, 'shifts')">
                                <label>Shift #</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="standard_status_date" id="standard_status_date"
                                       value="standard_status_date"
                                       onchange="hideItems(this, 'standard_status_date')">
                                <label>Standard Status Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="proposed_complexity" id="proposed_complexity"
                                       value="proposed_complexity"
                                       onchange="hideItems(this, 'proposed_complexity')">
                                <label>Proposed Complexity</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="general_remarks" id="general_remarks"
                                       value="proposed_complexity"
                                       onchange="hideItems(this, 'general_remarks')">
                                <label>Complexity Remarks</label>
                            </div>
                            <div class="col-md-9 floting">
                            </div>
                            <br>
                            <br>
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                <div class="col-md-8 floting"></div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">
                                        <a href="{{ route('reports.list-of-certificates-index') }}"
                                           class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                    class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th class="company_name">Name of Company</th>
                                <th class="standard">Standard(s)</th>
                                <th class="proposed_complexity" style="display: none">Proposed Complexity</th>
                                <th class="general_remarks" style="display: none">Complexity Remarks</th>
                                <th class="count_standard">Certificate Count</th>
                                <th class="certificate_status">Certificate Status</th>
                                <th class="withdrawn_date" style="display: none">Status Date</th>
                                <th class="unique_code" style="display: none">Accreditation</th>
                                @if(auth()->user()->user_type === 'scheme_manager')
                                    <th class="all_accreditation" style="display: none">All Accreditation</th>
                                @endif
                                <th class="unique_code" style="display: none">Codes</th>
                                <th class="job_number" style="display: none; width: 25% !important;">Job Number</th>
                                <th class="region_field">Region</th>
                                <th class="country">Country</th>
                                <th class="city" style="display: none">City</th>
                                <th class="post_code" style="display: none">Post Code</th>
                                <th class="addess" style="display: none">Address</th>
                                <th class="scope" style="display: none">Scope</th>
                                <th class="contact_person" style="display: none">Name</th>
                                <th class="contact_person" style="display: none">Designation</th>
                                <th class="contact_person" style="display: none">Contact No</th>
                                <th class="contact_person" style="display: none">Landline No</th>
                                <th class="contact_person" style="display: none">Email</th>
                                <th class="original_issue_date" style="display: none">Original I. Date</th>
                                <th class="expiry_date" style="display: none">Expiry Date</th>
                                <th class="last_certificate_issue_date" style="display: none">Last Certificate I. Date
                                </th>
                                <th class="transfer_case" style="display: none">Transfer Case</th>

                                <th class="initial_certification_amount" style="display: none">Currency</th>
                                <th class="initial_certification_amount" style="display: none">Initial Price</th>

                                <th class="surveillance_price" style="display: none">Surveillance Price</th>
                                <th class="re_audit_price" style="display: none">Re-audit Price</th>
                                <th class="actual_audit_start_date" style="display: none">Stage 2 Start Date</th>
                                @if (auth()->user()->user_type === 'scheme_manager')
                                    <th class="grades" style="display: none">Grades</th>

                                    <th class="multiple_sides" style="display: none">Total Sites (Manual)</th>
                                @endif
                                <th class="out_of_country_region_name" style="display: none">Country # 2</th>
                                <th class="locations" style="display: none">Total Locations</th>
                                <th class="shifts" style="display: none">Shift #</th>
                                <th class="standard_status_date" style="display: none">Standard Status Date</th>
                                <th class="noExport" style="width: 3% !important;">Edit</th>

                            </tr>

                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0)

                                                @php
                                                    $companyStandardCodesAccreditation = '';
                                                    $companyStandardCodesAccreditationAll = '';
                                                    $companyStandardCodesIAF = '';
                                                    $companyStandardCodesAccreditationArray =[];
                                                    $companyStandardCodesAccreditationArrayAll =[];
                                                    $companyStandardCodesIAFArray =[];
                                                    $companyStandardProposedComplexityArray =[];
                                                    $ims =false;

                                                @endphp
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                    <td class="standard">
                                                        @if($companyStandard->is_ims == true)
                                                            @php
                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                $ims_heading = $companyStandard1->standard->name;
                                                                $proposedComplexity = $companyStandard1->proposed_complexity;
                                                                $generalRemarks = $companyStandard1->general_remarks ?? 'N/A';
                                                                $ims = true;


                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                         if(!empty($imsCompanyStandard->companyStandardCodes) && count($imsCompanyStandard->companyStandardCodes) > 0){
                                                                             foreach ($imsCompanyStandard->companyStandardCodes as $companyStandardCode){
                                                                                 array_push($companyStandardCodesAccreditationArray,explode('/',$companyStandardCode->unique_code)[0]);
                                                                                 array_push($companyStandardCodesAccreditationArrayAll,explode('/',$companyStandardCode->unique_code)[0]);
                                                                                 array_push($companyStandardCodesIAFArray,explode('/',$companyStandardCode->unique_code)[1]);
                                                                             }
                                                                         }
                                                                         $companyStandardCodesAccreditationAll = implode(',',$companyStandardCodesAccreditationArrayAll);
                                                                         $companyStandardCodesAccreditationArray = array_values(array_unique($companyStandardCodesAccreditationArray));
                                                                         $companyStandardCodesAccreditation = implode(',',$companyStandardCodesAccreditationArray);
                                                                         $companyStandardCodesIAFArray = array_values(array_unique($companyStandardCodesIAFArray));
                                                                         $companyStandardCodesIAF = implode('/',$companyStandardCodesIAFArray);
                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                            continue;
                                                                        } else {
                                                                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                            $proposedComplexity .= ', ' .$imsCompanyStandard->proposed_complexity;
                                                                            $generalRemarks .= ', ' .$imsCompanyStandard->general_remarks ?? 'N/A';
                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                            {{ ($ims_heading) }}(IMS)
                                                        @else
                                                            @php
                                                                $proposedComplexity = $companyStandard->proposed_complexity;
                                                                $generalRemarks = $companyStandard->general_remarks;
                                                            @endphp
                                                            {{ $companyStandard->standard->name }}
                                                        @endif
                                                    </td>
                                                    <td class="proposed_complexity" style="display: none">{{ $proposedComplexity }}</td>
                                                    <td class="general_remarks" style="display: none">{{ $generalRemarks ?? 'N/A' }}</td>
                                                    <td class="count_standard">
                                                        @php
                                                            $count =1;
                                                        @endphp
                                                        @if($companyStandard->is_ims == true)
                                                            @php
                                                                $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                $company = \App\Company::where('id', $company->id)->first();
                                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                        continue;
                                                                        } else {
                                                                        $count++;
                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                            {{ $count }}
                                                        @else
                                                            {{ $count }}
                                                        @endif
                                                    </td>
                                                    <td class="certificate_status">{{ ucfirst($companyStandard->certificates->certificate_status) }}</td>
                                                    <td class="withdrawn_date" style="display: none">
                                                        @if(isset($companyStandard->certificates) && !is_null($companyStandard->certificates))
                                                            {{ date('d-m-Y',strtotime($companyStandard->certificates->certificate_date)) }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td class="unique_code" style="display: none">
                                                        @if($ims === true)
                                                            {{ $companyStandardCodesAccreditation }}
                                                        @else
                                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                                @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                    @php
                                                                        array_push($companyStandardCodesAccreditationArray,explode('/',$companyStandardCode->unique_code)[0]);
                                                                    @endphp
                                                                @endforeach
                                                                @php
                                                                    $companyStandardCodesAccreditationArray = array_values(array_unique($companyStandardCodesAccreditationArray));
                                                                    $companyStandardCodesAccreditation = implode(',',$companyStandardCodesAccreditationArray);
                                                                @endphp
                                                                {{$companyStandardCodesAccreditation}}
                                                            @else
                                                                N/A
                                                            @endif
                                                        @endif
                                                    </td>
                                                    @if(auth()->user()->user_type === 'scheme_manager')
                                                        <td class="all_accreditation" style="display: none">
                                                            @if($ims === true)
                                                                {{ $companyStandardCodesAccreditationAll }}
                                                            @else
                                                                @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                                    @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                        @php
                                                                            array_push($companyStandardCodesAccreditationArrayAll,explode('/',$companyStandardCode->unique_code)[0]);
                                                                        @endphp
                                                                    @endforeach
                                                                    @php
                                                                        $companyStandardCodesAccreditationAll = implode(',',$companyStandardCodesAccreditationArrayAll);
                                                                    @endphp
                                                                    {{$companyStandardCodesAccreditationAll}}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            @endif
                                                        </td>
                                                    @endif

                                                    <td class="unique_code" style="display: none">
                                                        @if($ims === true)
                                                            {{ $companyStandardCodesIAF }}
                                                        @else
                                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                                @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                    @php
                                                                        array_push($companyStandardCodesIAFArray,explode('/',$companyStandardCode->unique_code)[1]);
                                                                    @endphp
                                                                @endforeach
                                                                @php

                                                                    $companyStandardCodesIAFArray = array_values(array_unique($companyStandardCodesIAFArray));
                                                                    $companyStandardCodesIAF = implode('/',$companyStandardCodesIAFArray);
                                                                @endphp
                                                                {{$companyStandardCodesIAF}}
                                                            @else
                                                                N/A
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td class="job_number" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if(isset($ajStandardStage->job_number) && $ajStandardStage->audit_type == 'stage_2')
                                                                    {{ $ajStandardStage->job_number }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            N/A
                                                        @endif

                                                    </td>
                                                    <td class="region_field">{{ ucfirst($company->region->title) }}</td>
                                                    <td class="country">{{ ucfirst($company->country->name) }}</td>
                                                    <td class="city"
                                                        style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                    <td class="post_code"
                                                        style="display: none">{{ ucfirst($company->city->post_code ?? 'N/A')}}</td>
                                                    <td class="addess"
                                                        style="display: none">{{ ucfirst($company->address) }}</td>
                                                    <td class="scope"
                                                        style="display: none">{{ $company->scope_to_certify ?? 'N/A'}}</td>
                                                    <td class="contact_person"
                                                        style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->title) : '' }} {{ $company->primaryContact ? ucfirst($company->primaryContact->name) : '' }}</td>
                                                    <td class="contact_person"
                                                        style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->position) : '' }}</td>
                                                    <td class="contact_person"
                                                        style="display: none">{{ $company->primaryContact ? ucfirst($company->primaryContact->contact) : '' }}</td>
                                                    <td class="contact_person"
                                                        style="display: none">{{ $company->primaryContact ? $company->primaryContact->landline ?? '-' : '' }}</td>
                                                    <td class="contact_person"
                                                        style="display: none">{{ $company->primaryContact ? $company->primaryContact->email : '' }}</td>

                                                    @php
                                                        $latest_dates = \App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use ($companyStandard){
                                                                $q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);
                                                        })->latest()->first();




                                                    @endphp
                                                    <td class="original_issue_date"
                                                        style="display: none">{{ (isset($latest_dates)) ? date('d-m-Y',strtotime(($latest_dates->original_issue_date))) : 'N/A' }}</td>
                                                    <td class="expiry_date"
                                                        style="display: none">{{ (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) ? date('d-m-Y',strtotime(($latest_dates->new_expiry_date))) : 'N/A' }}</td>
                                                    <td class="last_certificate_issue_date"
                                                        style="display: none">{{(isset($companyStandard) && $companyStandard->last_certificate_issue_date) ?  date("d-m-Y", strtotime($companyStandard->last_certificate_issue_date)):'N/A' }}</td>
                                                    <td class="transfer_case"
                                                        style="display: none">{{ $companyStandard->is_transfer_standard == true ? $companyStandard->transfer_year : 'N/A'  }}</td>

                                                    <td class="initial_certification_amount"
                                                        style="display: none">{{ ucfirst($companyStandard->initial_certification_currency) }}</td>

                                                    @if($companyStandard->is_ims == true)

                                                        @php
                                                            $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                            $company = \App\Company::where('id', $company->id)->first();
                                                            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                            $initial_certification_amount = (int)$companyStandard1->initial_certification_amount;
                                                            $surveillance_amount = (int)$companyStandard1->surveillance_amount;
                                                            $re_audit_amount = (int)$companyStandard1->re_audit_amount;

                                                            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                    if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                    continue;
                                                                    } else {

                                                                    $initial_certification_amount += (int)$imsCompanyStandard->initial_certification_amount;
                                                                    $surveillance_amount += (int)$imsCompanyStandard->surveillance_amount;
                                                                    $re_audit_amount += (int)$imsCompanyStandard->re_audit_amount;

                                                                    }
                                                                }
                                                            }
                                                        @endphp

                                                        <td class="initial_certification_amount"
                                                            style="display: none">{{ $initial_certification_amount }}</td>
                                                        <td class="surveillance_price"
                                                            style="display: none">{{ $surveillance_amount }}</td>
                                                        <td class="re_audit_price"
                                                            style="display: none">{{ $re_audit_amount }}</td>
                                                    @else
                                                        <td class="initial_certification_amount"
                                                            style="display: none">{{ $companyStandard->initial_certification_amount }}</td>
                                                        <td class="surveillance_price"
                                                            style="display: none">{{ $companyStandard->surveillance_amount }}</td>
                                                        <td class="re_audit_price"
                                                            style="display: none">{{ $companyStandard->re_audit_amount }}</td>
                                                    @endif
                                                    <td class="actual_audit_start_date" style="display: none">
                                                        @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                            @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                @if($ajStandardStage->audit_type === 'stage_2')
                                                                    @php
                                                                        $postAuditInfo  = \App\PostAudit::where(['company_id'=>$company->id,'standard_id'=>$companyStandard->standard_id,'aj_standard_stage_id'=>$ajStandardStage->id])->first(['actual_audit_date']);
                                                                    @endphp
                                                                    {{ (isset($postAuditInfo) && !is_null($postAuditInfo)) ? date('d-m-Y',strtotime(($postAuditInfo->actual_audit_date))) : 'N/A' }}
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    @if (auth()->user()->user_type === 'scheme_manager')
                                                        <td class="grades" style="display: none">
                                                            @if(!is_null($companyStandard->grade))
                                                                {{ $companyStandard->grade }}
                                                            @else
                                                                N/A
                                                            @endif
                                                        </td>

                                                        <td class="multiple_sides" style="display: none">
                                                            {{ $company->multiple_sides ?? 1 }}
                                                        </td>
                                                    @endif
                                                    <td class="out_of_country_region_name"
                                                        style="display: none">{{ $company->out_of_country_region_name ?? 'N/A' }}</td>
                                                    <td class="locations"
                                                        style="display: none">{{ 1 + $company->childCompanies->count() }}</td>
                                                    <td class="shifts"
                                                        style="display: none">{{ $company->shift ?? 0}}</td>
                                                    <td class="standard_status_date"  style="display: none">{{ (isset($companyStandard->certificates)) ? date('d-m-Y',strtotime(($companyStandard->certificates->certificate_date))) : '' }}</td>
                                                    <td>
                                                        <a href="{{ route('company.show', $company->id) }}"
                                                           target="_blank"
                                                           style="margin-left: 10px;"
                                                           title="View"><i
                                                                    class="fa fa-eye"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }

    .daterangepicker .ranges .input-mini {
        font-size: 20px !important;
        height: 41px !important;
        width: 120px !important;
    }

    .daterangepicker .ranges .range_inputs > div:nth-child(2) {
        padding-left: 0px !important;
    }

    button.dt-button.buttons-excel.buttons-html5 {
        cursor: pointer !important;
    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>

        //Date range picker
        $('#issue_from').daterangepicker({
            format: "DD-MM-YYYY",
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#unique_codes").select2();


        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 800,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: null,
                    filename: "List of Certified Clients",
                    "createEmptyCells": true,
                    sheetName: "List of Certified Clients",

                }


            ],
        });
    </script>
@endpush


