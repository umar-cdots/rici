@extends('layouts.master')
@section('title', "Surveillance and Reaudit Due Report")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Surveillance and Reaudit Due Report</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.surveillance-and-reaudit-due-index') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="region_id">Region</label>
                                    <select class="form-control" id="region_id" name="region_id[]" multiple>
                                        <option value="" disabled>Select Region</option>
                                        @if(!empty($get_regions) && count($get_regions) > 0)
                                            @foreach($get_regions as $region)
                                                <option value="{{ $region->id }}">{{ ucfirst($region->title) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="form-control" name="country_id[]" id="country_id" multiple>
                                        <option value="" disabled>Select Country</option>
                                        @if(!empty($get_countries) && count($get_countries) > 0)
                                            @foreach($get_countries as $country)
                                                <option value="{{ $country->id }}">{{ ucfirst($country->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            {{--                            <div class="col-md-4">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="standard_id">Standard</label>--}}
                            {{--                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>--}}
                            {{--                                        <option value="" disabled>Select Standard</option>--}}
                            {{--                                        @if(!empty($get_standards) && count($get_standards) > 0)--}}
                            {{--                                            @foreach($get_standards as $standard)--}}
                            {{--                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>--}}
                            {{--                                            @endforeach--}}
                            {{--                                        @endif--}}

                            {{--                                    </select>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="audit_type">Audit Type</label>
                                    <select class="form-control" id="audit_type" name="audit_type">
                                        <option value="">Select Audit Type</option>
                                        <option value="surveillance_1">Surveillance 1</option>
                                        <option value="surveillance_2">Surveillance 2</option>
                                        <option value="surveillance_3">Surveillance 3</option>
                                        <option value="surveillance_4">Surveillance 4</option>
                                        <option value="surveillance_5">Surveillance 5</option>
                                        <option value="reaudit">Reaudit</option>


                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">AJ Due Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="aj_due_date" id="aj_due_date" value=""
                                               class="form-control float-right active aj_due_date"
                                               placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">AJ Due Date less than Current Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="date_less_than_aj_due_date"
                                               id="date_less_than_aj_due_date" value=""
                                               class="form-control float-right date_less_than_aj_due_date"
                                               placeholder="dd-mm-yyyy">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row" id="chboxes">
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="region_field" id="region_field" value="region_field"
                                       onchange="hideItems(this, 'region_field')">
                                <label>Region</label>
                            </div>

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="country" id="country" value="country"
                                       onchange="hideItems(this, 'country')">
                                <label>Country</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="city" id="city" value="city"
                                       onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="pack_status_and_date" id="pack_status_and_date"
                                       value="pack_status_and_date"
                                       onchange="hideItems(this, 'pack_status_and_date')">
                                <label>Pack Status & Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="days_overdue" id="days_overdue"
                                       value="days_overdue"
                                       onchange="hideItems(this, 'days_overdue')">
                                <label>Days OverDue</label>
                            </div>

                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.surveillance-and-reaudit-due-index') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th class="company_name">Name of Company</th>
                                <th class="standard">Standard(s)</th>
                                <th class="audit_type" style="width: 10% !important;">Audit Type</th>
                                <th class="expected_due_date" style="width: 10% !important;">Due Date</th>
                                <th class="due_month">Due Month</th>
                                <th class="days_overdue" style="display: none">Days OverDue</th>
                                <th class="actual_audit_date" style="width: 16% !important;">Actual Audit Start Date
                                </th>
                                <th class="region_field" style="display: none">Region</th>
                                <th class="country" style="display: none">Country</th>
                                <th class="city" style="display: none">City</th>
                                <th class="aj_status_and_date">Aj Status & Date</th>
                                <th class="pack_status_and_date" style="display: none">Pack Status & Date
                                </th>

                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                @foreach($companyStandard->companyStandardCodes as $key => $companyStandardCode)
                                                    @if($key == 0)

                                                        @if( isset($_REQUEST['audit_type']) && !is_null($_REQUEST['audit_type']) && $_REQUEST['audit_type'] != "")

                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                @php
                                                                    $check =false;
                                                                    $auditCheck =false;
                                                                @endphp
                                                                @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                    @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                        @php
                                                                            $check =true;
                                                                        @endphp

                                                                        @php
                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                            $q->where('audit_type',$ajStandardStage->audit_type);
                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();
                                                                            if(is_null($actual_audit_date_each_stage)){
                                                                                $auditCheck = true;
                                                                            }
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if($check == true)

                                                                    @if($auditCheck == true)
                                                                        <tr>
                                                                            <td>{{ $i++ }}</td>
                                                                            <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                            <td class="standard">
                                                                                @if($companyStandard->is_ims == true)
                                                                                    @php
                                                                                        $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                        $company = \App\Company::where('id', $company->id)->first();
                                                                                        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                        $ims_heading = $companyStandard1->standard->name;


                                                                                        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                                if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                                continue;
                                                                                                } else {
                                                                                                $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    @endphp
                                                                                    {{ ($ims_heading) }}(IMS)
                                                                                @else
                                                                                    {{ $companyStandardCode->standard->name }}
                                                                                @endif


                                                                            </td>
                                                                            <td class="audit_type">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                                        @endif
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="exptected_due_date">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                                        @endif
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="due_month">

                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                            <td class="days_overdue"
                                                                                style="display: none">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            @php

                                                                                                $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                                $now = Carbon\Carbon::now();//19-09-2021 -13-09-2021
                                                                                                $diff = $endDate->diffInDays($now);
                                                                                            @endphp

                                                                                            @if($endDate < $now)
                                                                                                +{{ $diff }}

                                                                                            @else
                                                                                                -{{ $diff }}

                                                                                            @endif
                                                                                            <hr>
                                                                                        @endif

                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="actual_due_date">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            @php
                                                                                                $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                                $q->where('audit_type',$ajStandardStage->audit_type);
                                                                                                })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                            @endphp
                                                                                            @if(isset($actual_audit_date_each_stage))
                                                                                                {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}
                                                                                            @else
                                                                                                N/A
                                                                                            @endif
                                                                                            <br>
                                                                                        @endif

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="region_field"
                                                                                style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                                            <td class="country"
                                                                                style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                                            <td class="city"
                                                                                style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                            <td class="aj_status_and_date">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            @if($ajStandardStage->status == 'approved')
                                                                                                {{ ucfirst($ajStandardStage->status) }}
                                                                                                <br>
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                            @elseif($ajStandardStage->status == 'rejected')
                                                                                                {{ ucfirst($ajStandardStage->status) }}
                                                                                                <br>
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                            @else
                                                                                                {{ ucfirst($ajStandardStage->status) }}
                                                                                            @endif
                                                                                            <hr>
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            </td>
                                                                            <td class="pack_status_and_date"
                                                                                style="display: none">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->audit_type == $_REQUEST['audit_type'])
                                                                                            @if(!is_null($ajStandardStage->postAudit))
                                                                                                @if($ajStandardStage->postAudit->status == 'save')
                                                                                                    @php
                                                                                                        $status = '';
                                                                                                        $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                         $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                         if(is_null($file)){
                                                                                                             $status = 'no';
                                                                                                         }else{
                                                                                                             $status = '';
                                                                                                         }


                                                                                                    @endphp
                                                                                                    @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                        Pack
                                                                                                        Not
                                                                                                        Uploaded
                                                                                                    @else
                                                                                                        Pack
                                                                                                        Not
                                                                                                        Sent
                                                                                                    @endif
                                                                                                @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                                    Submitted
                                                                                                @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                                    Rejected
                                                                                                    <br>
                                                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                                @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                                    Resent
                                                                                                @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                                    Forwarded
                                                                                                @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                                    Approved
                                                                                                    <br>
                                                                                                    {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) }}
                                                                                                @endif
                                                                                            @else
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            </td>

                                                                        </tr>
                                                                    @endif
                                                                @endif

                                                            @endif
                                                        @else
                                                            @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                @php
                                                                    $check =false;
                                                                    $auditCheck =false;
                                                                @endphp
                                                                @if(!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0)
                                                                    @php
                                                                        $check =true;
                                                                    @endphp
                                                                @endif
                                                                @if($check == true)
                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                        @php
                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                            $q->where('audit_type',$ajStandardStage->audit_type);
                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();
                                                                            if(is_null($actual_audit_date_each_stage)){
                                                                                $auditCheck = true;
                                                                            }
                                                                        @endphp
                                                                        @break
                                                                    @endforeach
                                                                    @if($auditCheck == true)
                                                                        <tr>
                                                                            <td>{{ $i++ }}</td>
                                                                            <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                                            <td class="standard">


                                                                                @if($companyStandard->is_ims == true)
                                                                                    @php
                                                                                        $companyStandard1 = \App\CompanyStandards::where('id', $companyStandard->id)->first();
                                                                                        $company = \App\Company::where('id', $company->id)->first();
                                                                                        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                                                                                        $ims_heading = $companyStandard1->standard->name;


                                                                                        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                                                            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                                                                if ($companyStandard1->id == $imsCompanyStandard->id) {
                                                                                                continue;
                                                                                                } else {
                                                                                                $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    @endphp
                                                                                    {{ ($ims_heading) }}(IMS)
                                                                                @else
                                                                                    {{ $companyStandardCode->standard->name }}
                                                                                @endif


                                                                            </td>
                                                                            <td class="audit_type">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="exptected_due_date">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{date('d-m-Y',strtotime($ajStandardStage->excepted_date))}}
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="due_month">

                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        {{date('F',strtotime($ajStandardStage->excepted_date))}}
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif
                                                                            </td>
                                                                            <td class="days_overdue"
                                                                                style="display: none">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)

                                                                                        @php

                                                                                            $endDate = Carbon\Carbon::parse($ajStandardStage->excepted_date);
                                                                                            $now = Carbon\Carbon::now();
                                                                                            $diff = $endDate->diffInDays($now);
                                                                                        @endphp

                                                                                        @if($endDate < $now)
                                                                                            +{{ $diff }}

                                                                                        @else
                                                                                            -{{ $diff }}

                                                                                        @endif
                                                                                        <hr>

                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="actual_due_date">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @php
                                                                                            $actual_audit_date_each_stage = App\PostAudit::with('postAuditSchemeInfoLatest')->whereHas('AjStandardStage',function($q) use ($ajStandardStage){
                                                                                            $q->where('audit_type',$ajStandardStage->audit_type)->where('recycle',$ajStandardStage->recycle);
                                                                                            })->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id)->first();

                                                                                        @endphp
                                                                                        @if(isset($actual_audit_date_each_stage))
                                                                                            {{date('d-m-Y',strtotime($actual_audit_date_each_stage->actual_audit_date))}}

                                                                                        @else
                                                                                            N/A
                                                                                        @endif
                                                                                        <br>

                                                                                    @endforeach
                                                                                @else
                                                                                    N/A
                                                                                @endif

                                                                            </td>
                                                                            <td class="region_field"
                                                                                style="display: none">{{ ucfirst($company->region->title) }}</td>
                                                                            <td class="country"
                                                                                style="display: none">{{ ucfirst($company->country->name) }}</td>
                                                                            <td class="city"
                                                                                style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                                            <td class="aj_status_and_date">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if($ajStandardStage->status == 'approved')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            <br>
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->approval_date)) }}
                                                                                        @elseif($ajStandardStage->status == 'rejected')
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                            <br>
                                                                                            {{ date('d-m-Y',strtotime($ajStandardStage->updated_at)) }}
                                                                                        @else
                                                                                            {{ ucfirst($ajStandardStage->status) }}
                                                                                        @endif
                                                                                        <hr>
                                                                                    @endforeach
                                                                                @endif
                                                                            </td>
                                                                            <td class="pack_status_and_date"
                                                                                style="display: none">
                                                                                @if(isset($companyStandard->ajStandardStage) && !empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) >0)
                                                                                    @foreach($companyStandard->ajStandardStage as $ajStandardStage)
                                                                                        @if(!is_null($ajStandardStage->postAudit))
                                                                                            @if($ajStandardStage->postAudit->status == 'save')
                                                                                                @php
                                                                                                    $status = '';
                                                                                                    $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$ajStandardStage->postAudit->id)->where('question_id',3)->first();
                                                                                                     $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
                                                                                                     if(is_null($file)){
                                                                                                         $status = 'no';
                                                                                                     }else{
                                                                                                         $status = '';
                                                                                                     }


                                                                                                @endphp
                                                                                                @if(!is_null($ajStandardStage->postAudit->actual_audit_date) && $status == 'no')
                                                                                                    Pack
                                                                                                    Not
                                                                                                    Uploaded
                                                                                                @else
                                                                                                    Pack
                                                                                                    Not
                                                                                                    Sent
                                                                                                @endif
                                                                                            @elseif($ajStandardStage->postAudit->status == 'unapproved')
                                                                                                Submitted
                                                                                            @elseif($ajStandardStage->postAudit->status == 'rejected')
                                                                                                Rejected
                                                                                                <br>
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->updated_at)) }}
                                                                                            @elseif($ajStandardStage->postAudit->status == 'resent')
                                                                                                Resent
                                                                                            @elseif($ajStandardStage->postAudit->status == 'accepted')
                                                                                                Forwarded
                                                                                            @elseif($ajStandardStage->postAudit->status == 'approved')
                                                                                                Approved
                                                                                                <br>
                                                                                                {{ date('d-m-Y',strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)) }}
                                                                                            @endif
                                                                                        @else
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            </td>

                                                                        </tr>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')



    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>


        $('#aj_due_date').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $('#date_less_than_aj_due_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            // Show length rows menu
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 100,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "Surveillance and Reaudit Due Report",
                    filename: "Surveillance and Reaudit Due Report",
                    "createEmptyCells": true,
                    sheetName: "Surveillance and Reaudit Due Report",

                }


            ],
        });
    </script>
@endpush


