<table id="FormalEducationTableCodes" cellspacing="20" cellpadding="0" border="0"
       class="table table-striped table-bordered table-bordered2">
    <thead>
    <tr>
        <th>S.No</th>
        <th>Accreditation</th>
        <th>IAF Code</th>
        <th>IAS Code</th>
        <th>Status</th>
        <th>Remarks</th>
    </tr>
    </thead>
    <tbody>

    @if(!empty($auditorStandards->auditorStandardCodes) && count($auditorStandards->auditorStandardCodes) > 0)
        @foreach($auditorStandards->auditorStandardCodes as $key => $auditorStandardCode)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{!! $auditorStandardCode->accreditation->name !!}</td>
                <td>{{$auditorStandardCode->iaf->code}}</td>
                <td>{{$auditorStandardCode->ias->code}}</td>
                <td>{{($auditorStandardCode->status)  ? ucfirst($auditorStandardCode->status)  : 'N/A'}}</td>
                <td>{{($auditorStandardCode->remarks) ? ucfirst($auditorStandardCode->remarks) : 'N/A'}}</td>
            </tr>
        @endforeach
    @else

        <tr>
            <td colspan="6"> Record Not Found</td>
        </tr>

    @endif


    </tbody>

</table>