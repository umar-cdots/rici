@extends('layouts.master')
@section('title', "List of Technical Experts")

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Technical Experts</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.list-of-technical-expert-with-approval-date') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Region</label>
                                    <select class="form-control" id="region_id" name="region_id">
                                        <option value="">Select Region</option>
                                        @foreach($regions as $region)
                                            <option value="{{ $region->id }}" {{ old('region_id') === $region->id ? 'selected' : '' }}>{{ $region->title }}</>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="country_id">Country *</label>
                                    <select class="form-control" name="country_id" id="country_id">
                                        <option value="">Select Country</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Profile Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="">Please Select</option>
                                        <option value="active" {{ old('status') === 'active' ? 'selected' : '' }}>Active</option>
                                        <option value="inactive" {{ old('status') === 'inactive' ? 'selected' : '' }}>InActive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="chboxes">

                            <div class="col-md-2 floting">
                                <input type="checkbox" class=""
                                       name="region" id="region" value="region"
                                       onchange="hideItems(this, 'region_field')">
                                <label>Region</label>
                            </div>
                            <div class="col-md-2 floting">
                                <input type="checkbox" class="" value="employment"
                                       onchange="hideItems(this, 'employement_field')"
                                       name="employment" id="employment">
                                <label>Employeement</label>
                            </div>
                            <div class="col-md-2 floting">
                                <input type="checkbox" class="" onchange="hideItems(this, 'agreement_field')"
                                       name="agreement" id="agreement" value="agreement">
                                <label>Agreement date</label>
                            </div>
                            <div class="col-md-2 floting">
                                <input type="checkbox" class="" onchange="hideItems(this, 'grade_approval_date')"
                                       name="grade_approval_date" id="grade_approval_date" value="grade_approval_date">
                                <label>Grade Approval Date</label>
                            </div>
                            <div class="col-md-2 floting"></div>
                            <div class="col-md-2 floting"></div>
                            <div class="col-md-9 floting"></div>
                            <div class="col-md-1 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.list-of-technical-expert-with-approval-date') }}" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">

                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row">

                        <div class="reportsTable reportsTablePop">


                            <table id="example1"
                                   class="availableAuditorTable datatable datatable-multi-row"
                                   role="grid"
                                   aria-describedby="example1_info">

                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Standard</th>
                                    <th id="column3_search">Standard Status</th>
                                    <th id="column3_search">IAF Codes</th>
                                    <th class="employement_field" style="display: none">Employment Type</th>
                                    <th class="agreement_field" style="display: none">Agreement Date</th>
                                    <th class="grade_approval_date" style="display: none">Grade Approval Date</th>
                                    <th class="region_field" style="display: none">Region</th>
                                    <th>Country</th>
                                    <th>Profile Status</th>

                                </tr>
                                </thead>
                                @php
                                    $index = 0;
                                @endphp
                                <tbody id="myTable">
                                @if(!empty($auditors))
                                    @foreach($auditors as $key=>$auditor)
                                        @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)
                                            @foreach($auditor->auditorStandards as $index=>$a_standard)
                                                @php
                                                    $iafCodesArray = [];
                                                    $auditor_standard_codes = \App\AuditorStandardCode::where('auditor_standard_id', $a_standard->id)->groupBy('iaf_id')->get(['id','iaf_id']);
                                                    $auditor_standard_grade = \App\AuditorStandardGrade::where('auditor_standard_id', $a_standard->id)->where('status','current')->first(['approval_date']);
                                                @endphp
                                                <tr>
                                                    <td>{{ $auditor->fullName() }}</td>
                                                    <td id="column3_search">{{$a_standard->standard->name}}</td>
                                                    <td id="column3_search">{{ ucfirst($a_standard->status) }}</td>
                                                    <td>
                                                        @if(!empty($auditor_standard_codes) && count($auditor_standard_codes) > 0)
                                                            @foreach($auditor_standard_codes as $auditorStandardCode)
                                                                @php
                                                                    array_push($iafCodesArray,$auditorStandardCode->iaf->code);
                                                                @endphp
                                                            @endforeach
                                                            @php
                                                                $iafCodes = array_unique($iafCodesArray);
                                                            @endphp
                                                            {{ implode(',',$iafCodes) }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>

                                                    <td class="employement_field"
                                                        style="display: none">{{ str_replace('_', ' ',ucwords($auditor->job_status)) }}</td>
                                                    <td class="agreement_field" style="display: none">
                                                        @foreach($auditor->confidentialityAgreements as $agreement)
                                                            {{ $agreement->date_signed }}
                                                        @endforeach
                                                    </td>
                                                    <td class="grade_approval_date" style="display: none">
                                                       {{ !is_null($auditor_standard_grade) ? $auditor_standard_grade->approval_date : 'N/A' }}
                                                    </td>
                                                    <td class="region_field"
                                                        style="display: none">{{ $auditor->region->title }}</td>
                                                    <td>{{ $auditor->country->name }}</td>
                                                    <td>{{ ucfirst($auditor->auditor_status) }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>


                            </table>
                            <div id="pagination">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }
</style>
@push('scripts')

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.datatable').DataTable({
                "order": [[0, "asc"]],
                dom: 'Bfrtip',
                "pageLength": 600,
                "buttons": [
                    {
                        extend: 'excelHtml5',
                        title: null,
                        filename: "List of Technical Experts",
                        "createEmptyCells": true,
                        sheetName: "List of Technical Experts",

                    }


                ],
            });
        });


    </script>


    <script type="text/javascript">
        $('#region_id').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#country_id';
            var region_id = $(this).val();
            var request = "region_id=" + region_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.regionCountries') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.region_countries, function (i, obj) {
                            html += '<option value="' + obj.country.id + '">' + obj.country.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>Select Country</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

    </script>
@endpush
