@extends('layouts.master')
@section('title', "IAF CertSearch Success Log")
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">IAF CertSearch Success Log</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.statistics.success') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')

                            <div class="row">


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="issue_from">Date </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            </div>
                                            <input type="date" name="date" id="date" value=""
                                                   class="form-control float-right active issue_from"
                                                   placeholder="dd//mm/yyyy">
                                        </div>
                                    </div>
                                </div>


                            </div>
                        @endif
                        <div class="row" id="chboxes">


                            <br>
                            <br>
                            @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator')

                                <div class="col-md-8 floting"></div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">
                                        <a href="{{ route('reports.statistics.success') }}"
                                           class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                    class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-2 floting">
                                    <div class="form-group">

                                        <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th style="width: 4%">Sr No.</th>
                                <th style="width: 30% !important;">Company</th>
                                <th style="width: 14% !important; ">Standard</th>
                                <th style="width: 14% !important;">Saved Date</th>
                                <th style="width: 20% !important;">Upload Date & Time</th>


                            </tr>
                            </thead>

                            <tbody id="myTable">

                            @if(!empty($records) && count($records)>0)
                                @foreach($records as $key=>$record)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{$record->company ? ucfirst($record->company->name) : '--'}}</td>
                                        <td>{{$record->standard ? ucfirst($record->standard->name) : '--'}}</td>
                                        <td>{{ date('d-m-Y',strtotime($record->upload_date))  }}</td>
                                        <td>{{ date('d-m-Y h:i A',strtotime($record->updated_at))  }}</td>

                                    </tr>
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>


        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "pageLength": 600,
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "IAF CertSearch Success Log",
                    filename: "IAF CertSearch Success Log",
                    "createEmptyCells": true,
                    sheetName: "IAF CertSearch Success Log",

                }


            ],
        });
    </script>
@endpush


