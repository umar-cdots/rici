@extends('layouts.master')
@section('title', "Auditor Logs")

<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }

    .btnRefresh {
        height: 39px !important;
        width: 50% !important;
        float: right !important;
    }

    .overflowY {
        overflow-y: scroll;
    }

    th.job_number.sorting {
        width: 25% !important;
    }

</style>

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of Standards</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.auditor-log') }}" method="get"
                          id="step1Savebtn"
                          autocomplete="off"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Name <span style="color: red;">*</span></label>
                                    <select class="form-control" id="name" name="name" required>
                                        <option value="">Select Auditor Name</option>
                                        @if(!empty($get_auditors) && count($get_auditors) > 0)
                                            @foreach($get_auditors as $auditor)
                                                <option value="{{ $auditor->id }}">{{ ucfirst($auditor->fullName()) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Standard</label>
                                    <select class="form-control" id="standard_id" name="standard_id[]" multiple>
                                        <option value="" disabled>Select Standard</option>
                                        @if(!empty($get_standards) && count($get_standards) > 0)
                                            @foreach($get_standards as $standard)
                                                <option value="{{ $standard->id }}">{{ ucfirst($standard->name) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="unique_codes">Accreditation & Codes</label>
                                    <select class="form-control" id="unique_codes" name="unique_codes[]" multiple>
                                        <option value="" disabled>Accreditation & Codes</option>
                                        @if(!empty($get_company_standard_unique_codes) && count($get_company_standard_unique_codes) > 0)
                                            @foreach($get_company_standard_unique_codes as $unique_code)
                                                <option value="{{ $unique_code->unique_code }}">{{ ucfirst($unique_code->unique_code) }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="issue_from">Actual Audit Date </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="issue_from" id="issue_from" value=""
                                            class="form-control float-right active issue_from"
                                            placeholder="dd//mm/yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="chboxes">

                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                    name="unique_code" id="unique_code" value="unique_code"
                                    onchange="hideItems(this, 'unique_code')">
                                <label>Accreditation & Codes</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                    name="job_number" id="job_number" value="job_number"
                                    onchange="hideItems(this, 'job_number')">
                                <label>Job Number</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                    name="scope" id="scope" value="scope"
                                    onchange="hideItems(this, 'scope')">
                                <label>Scope</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                    name="stage_two" id="stage_two" value="stage_two"
                                    onchange="hideItems(this, 'stage_two')">
                                <label>Actual Audit Date</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                    name="city" id="city" value="city"
                                    onchange="hideItems(this, 'city')">
                                <label>City</label>
                            </div>
                            <div class="col-md-3 floting">
                                <input type="checkbox" class=""
                                       name="surveillance_md" id="surveillance_md" value="surveillance_md"
                                       onchange="hideItems(this, 'surveillance_md')">
                                <label>Surveillance MD</label>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-8 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.auditor-log') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn btnRefresh customRefreshBtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-block btn-secondary btn_save step1Savebtn">
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row overflowY">
                        <table id="example1"
                               class="availableAuditorTable datatable datatable-multi-row"
                               role="grid"
                               aria-describedby="example1_info">

                            <thead>
                            <tr>
                                <th rowspan="2">Sr No</th>
                                <th rowspan="2" class="company_name">Name of Client</th>
                                <th rowspan="2" class="standard">Standard(s)</th>
                                <th rowspan="2" class="audit_type">Audit Type</th>
                                <th rowspan="2" class="role">Role</th>
                                <th rowspan="2" class="unique_code" style="display: none">Accreditation & Codes</th>
                                <th rowspan="2" class="region_field">Region</th>
                                <th rowspan="2" class="job_number" style="display: none; width: 25% !important;">Job
                                    Number
                                </th>
                                <th rowspan="2" class="scope" style="display: none">Scope</th>
                                <th rowspan="2" class="stage_two" style="display: none">Actual Audit Date</th>
                                <th rowspan="2" class="country">Country</th>
                                <th rowspan="2" class="city" style="display: none">City</th>
                                <th colspan="2" class="surveillance_md" style="display: none">Surveillance MD</th>
                            </tr>

                            <tr>
                                <th class="surveillance_md" style="display: none">On-Site</th>
                                <th class="surveillance_md" style="display: none">Off-Site</th>
                            </tr>
                            </thead>

                            <tbody id="myTable">


                            @php
                                $i=1;
                            @endphp

                            @if(!empty($companies) && count($companies) > 0)
                                @foreach($companies as $company)
                                    @if(!empty($company->companyStandards) && count($company->companyStandards) > 0)
                                        @foreach($company->companyStandards as $companyStandard)
                                            @if(!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0)
                                                @foreach($companyStandard->ajStandardStage as $key => $ajStandardStage)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td class="company_name">{{ ucfirst($company->name) }}</td>
                                                            <td class="standard">{{ ucfirst($companyStandard->standard->name) }}</td>
                                                            <td class="audit_type">{{str_replace('_',' ',ucwords($ajStandardStage->audit_type))}}</td>
                                                            <td class="role">{{str_replace('_',' ',ucwords($ajStandardStage->ajStageTeam->member_type))}}</td>
                                                            <td class="unique_code" style="display: none">
                                                                @if(!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0)
                                                                @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                                                    {{ $companyStandardCode->unique_code }} ,
                                                                @endforeach
                                                                @else
                                                                N/A
                                                                @endif
                                                            </td>
                                                            <td class="region_field">{{ ucfirst($company->region->title) }}</td>
                                                            <td class="job_number" style="display: none" >   
                                                                @if(!is_null($ajStandardStage->job_number))
                                                                {{ $ajStandardStage->job_number }}<br>
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                            <td class="scope"
                                                                style="display: none">{{ $company->scope_to_certify ?? 'N/A'}}</td>
                                                            <td class="stage_two" style="display: none">
                                                                {{ isset($ajStandardStage->postAudit) ? date('d-m-Y',strtotime($ajStandardStage->postAudit->actual_audit_date)) : 'N/A' }} -
                                                                {{ isset($ajStandardStage->postAudit) ? date('d-m-Y',strtotime($ajStandardStage->postAudit->actual_audit_date_to)) : 'N/A' }}
                                                            </td>
                                                            <td class="country">{{ ucfirst($company->country->name) }}</td>
                                                            <td class="city"
                                                                style="display: none">{{ ucfirst($company->city->name)}}</td>
                                                            <td class="surveillance_md" style="display: none">
                                                                {{ $ajStandardStage->onsite }}
                                                            </td>
                                                            <td class="surveillance_md"
                                                                style="display: none">{{ $ajStandardStage->offsite }}</td>
                                                        </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif


                            </tbody>


                        </table>
                        <div id="pagination">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('modals')

@endpush

<style>
    .availableAuditorTable tr:hover {
        transition-duration: .3s;
        background: transparent !important;
        color: #000;
        cursor: pointer;
    }

    .select2-container--default .select2-selection--single {
        height: 40px !important;
    }
</style>
@push('scripts')




    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script>

        $('#issue_from').daterangepicker({
            format: "DD-MM-YYYY",
        });
        $("#name").select2();
        $("#standard_id").select2();
        $("#region_id").select2();
        $("#country_id").select2();
        $("#unique_codes").select2();
        $("#status").select2();
        $("#country_id").select2();

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            }
        }

        $('.datatable').DataTable({
            "order": [[0, "asc"]],
            dom: 'Bfrtip',
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: "Generic Company Standard Report",
                    filename: "Generic Company Standard Report",
                    "createEmptyCells": true,
                    sheetName: "Generic Company Standard Report",

                }


            ],
        });
    </script>
@endpush


