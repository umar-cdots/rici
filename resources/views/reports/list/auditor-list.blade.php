{{--{{ dd($auditor_standard_codes) }}--}}
@if(!empty($auditors))
    @foreach($auditors as $key=>$auditor)

        <tr role="row">
            <td> {{ $auditor->id }}</td>
            <td> {{ ucfirst($auditor->first_name) }} {{ ucfirst($auditor->last_name) }}</td>
            <td>
                @foreach($auditor->auditorStandards as $standards)
                    @if(!empty($standards))
                        {{$standards->number  }}<br>
                    @else
                        -----
                    @endif
                @endforeach
            </td>
            <td>{{ $auditor_standard_grades[$key]->grade->name }}</td>
            <td>
                @foreach($auditor_standard_codes as $auditor_standard_code)
                    @if(!empty($auditor_standard_code))
                        {{ $auditor_standard_code->ias->code }} <br>
                    @endif
                @endforeach
            </td>

            <td>1</td>
            <td id="region_field">{{ $auditor->region->title }}</td>
            <td id="employement_field">{{ $auditor->job_status === 'full_time' ? 'Permanent' :'Part Time' }}</td>
            <td id="agreement_field">1</td>
            @if(!empty($accredations[$key]))
                <td id="accreditation_field">{{ $accredations[$key]->full_name }}</td>
            @else
                <td id="accreditation_field">-----</td>
            @endif
            <td id="partial_code_field">1</td>
            <td>{{ ucfirst($auditor->auditor_status)}}</td>
            <td>{{ $auditor->country->name }}</td>
        </tr>
    @endforeach

@else
    <tr role="row">
        Record Not Found
    </tr>
@endif
