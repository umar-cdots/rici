@extends('layouts.master')
@section('title', "Reports List")
<style>
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

    .demo {
        padding-top: 60px;
        padding-bottom: 60px;
    }


    .float-right {
        float: right;
    }

    #myFilter {
        width: 245px;
        height: 40px;
        margin-top: 13px;
    }

    .wrapper {
        margin: 20px;
        font-family: sans-serif;
    }

    td,
    th {
        vertical-align: top;
        border: 1px solid #ddd;
    }

    ul,
    li {
        padding-left: 5px;
        margin-left: 5px;
    }

    #example1_wrapper {
        width: 100%;
    }
</style>
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('reports.auditor-energy') }}" method="get">
                        <div class="modal-header">
                            <h4 class="modal-title">Energy Auditor Code</h4>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <h3>Search Filters:</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-4 floting">
                                    <label>Region </label>
                                    <div class="form-group">
                                        <select class="form-control" name="region_id" id="region_id">
                                            <option value="">--Select--</option>
                                            @if(!empty($regions))
                                                @foreach($regions as $region)
                                                    <option value="{{ $region->id }}">{{ $region->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 floting">
                                    <label>Country</label>
                                    <div class="form-group">
                                        <select class="form-control" name="country_id" id="country_id">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 floting">
                                    <label>Name of Auditor</label>
                                    <div class="form-group">
                                        <select name="auditor" class="form-control" id="auditor">
                                            <option value="">--Select--</option>
                                            @if(!empty($energy_auditors) &&(count($energy_auditors)>0))
                                                @foreach($energy_auditors as $energyAuditor)
                                                    <option value="{{$energyAuditor->id}}">{{ ucfirst($energyAuditor->fullName()) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mrgn">
                            {{--                            <div class="col-md-12">--}}
                            {{--                            </div>--}}
                            <div class="col-md-8">


                            </div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <a href="{{ route('reports.auditor-energy') }}"
                                       class="btn btn-block btn-secondary btn_save step1Savebtn"><i
                                                class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                            <div class="col-md-2 floting">
                                <button type="submit" class="btn btn-block btn-success btn_save">Generate</button>
                            </div>
                        </div>

                    </form>

                    @if(!is_null($auditors) && count($auditors) > 0)
                        <div class="row">
                            <div class="col-md-12 print_table floting">
                                <a href="{{ asset('storage/uploads/documents/energy-auditors.pdf') }}"
                                   target="_blank"><i class="fa fa-print"></i></a>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="reportsTable reportsTable2">

                            <table id="example1"
                                   class="availableAuditorTable datatable datatable-multi-row"
                                   role="grid"
                                   aria-describedby="example1_info">

                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Standard</th>
                                    <th>Standard Status</th>
                                    <th>Grade</th>
                                    <th>Energy Main Category</th>
                                    <th>Energy Category Description</th>
                                    <th>Country</th>

                                </tr>
                                </thead>
                                <tbody id="myTable">
                                @php
                                    $index = 0;
                                @endphp
                                @if(!is_null($auditors) && count($auditors) > 0)
                                    @foreach($auditors as $auditor)
                                        @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)

                                            @php
                                                $rowspan =0;
                                                    if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0){
                                                      foreach($auditor->auditorStandards as $key=>$a_standard){
                                                          if(is_null($a_standard->deleted_at)){
                                                                   $rowspan = $rowspan +  1;
                                                          }
                                                      }
                                                    }

                                            @endphp

                                            <td data-datatable-multi-row-rowspan="{{$rowspan}}">
                                                {{$auditor->fullName()}}
                                                <script type="x/template" class="extra-row-content">
                                                    @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)
                                                    @foreach($auditor->auditorStandards as $key=>$a_standard)
                                                    @if(is_null($a_standard->deleted_at))
                                                    @if($key == $index)
                                                    @else
                                                    <tr role="row">
                                                        <td>{{ $a_standard->standard->name }}
                                                        </td>
                                                        <td>
                                                            {{ ucfirst($a_standard->status) }}
                                                        </td>
                                                        <td>
                                                            @foreach($auditor_standard_grades = \App\AuditorStandardGrade::where('auditor_standard_id', $a_standard->id)->get() as $auditor_standard_grade)
                                                                <span id="auditorStandardGrade{{$a_standard->id}}">{{ $auditor_standard_grade->grade->name }}</span>
                                                            @endforeach
                                                        </td>

                                                        <td>
                                                            @foreach($a_standard->auditorStandardEnergyCode as $foodCode)
                                                                <span>{{ $foodCode->energycode->code }}</span>
                                                                <hr>
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach($a_standard->auditorStandardEnergyCode as $foodCode)
                                                                <span>{{ $foodCode->energycode->name }}</span>
                                                                <hr>
                                                            @endforeach
                                                        </td>


                                                    </tr>
                                                    @endif


                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </script>
                                            </td>
                                            @if(!empty($auditor->auditorStandards) && count($auditor->auditorStandards) > 0)
                                                @foreach($auditor->auditorStandards as $key=>$a_standard)
                                                    @if(is_null($a_standard->deleted_at))
                                                        @php
                                                            $index =$key;
                                                        @endphp

                                                        <td>{{ $a_standard->standard->name }}</td>
                                                        <td>
                                                            {{ ucfirst($a_standard->status) }}

                                                        </td>
                                                        <td>
                                                            @foreach($auditor_standard_grades = \App\AuditorStandardGrade::where('auditor_standard_id', $a_standard->id)->get() as $auditor_standard_grade)
                                                                <span id="auditorStandardGrade{{$a_standard->id}}">{{ $auditor_standard_grade->grade->name }}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach($a_standard->auditorStandardEnergyCode as $foodCode)
                                                                <span>{{ $foodCode->energycode->code }}</span>
                                                                @if($loop->index >= 0 && $loop->index < count($a_standard->auditorStandardEnergyCode)-1)
                                                                    <hr>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach($a_standard->auditorStandardEnergyCode as $foodCode)
                                                                <span>{{ $foodCode->energycode->name }}</span>
                                                                @if($loop->index >= 0 && $loop->index < count($a_standard->auditorStandardEnergyCode)-1)
                                                                    <hr>
                                                                @endif

                                                            @endforeach
                                                        </td>

                                                        @break
                                                    @endif
                                                @endforeach
                                            @endif

                                            <td data-datatable-multi-row-rowspan="{{$rowspan}}">{{ $auditor->country->name }}</td>


                                        @endif
                                    @endforeach
                                @endif

                                </tbody>


                            </table>

                            <div class="clearfix"></div>

                        </div>
                    </div>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>




    <script type="text/javascript">


        var table = $('.datatable').DataTable({
            "fnDrawCallback": function () {

                $table = $(this);

                // only apply this to specific tables
                if ($table.closest(".datatable-multi-row").length) {

                    // for each row in the table body...
                    $table.find("tbody>tr").each(function () {
                        var $tr = $(this);

                        // get the "extra row" content from the <script> tag.
                        // note, this could be any DOM object in the row.
                        var extra_row = $tr.find(".extra-row-content").html();

                        // in case draw() fires multiple times,
                        // we only want to add new rows once.
                        if (!$tr.next().hasClass('dt-added')) {
                            $tr.after(extra_row);
                            $tr.find("td").each(function () {

                                // for each cell in the top row,
                                // set the "rowspan" according to the data value.
                                var $td = $(this);
                                var rowspan = parseInt($td.data("datatable-multi-row-rowspan"), 10);
                                if (rowspan) {
                                    $td.attr('rowspan', rowspan);
                                }
                            });
                        }

                    });

                } // end if the table has the proper class
            } // end fnDrawCallback()
        });

        $('#region_id').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#country_id';
            var region_id = $(this).val();
            var request = "region_id=" + region_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.regionCountries') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.region_countries, function (i, obj) {
                            html += '<option value="' + obj.country.id + '">' + obj.country.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>--Select--</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function hideItems(input_id, table_row) {
            if ($(input_id).is(":checked")) {
                $('.' + table_row).each(function () {
                    $(this).fadeOut();
                });
            } else {
                $('.' + table_row).each(function () {
                    $(this).fadeIn();
                });
            }
        }

    </script>
@endpush
