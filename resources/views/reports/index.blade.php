@extends('layouts.master')
@section('title', "Reports List")

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-4 floting">
                            <h1>Reports</h1>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('reports') }}
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Reports</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tabularData" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="8%">Sr. #</th>
                                    <th>Report Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(Auth::user()->user_type === "scheme_manager")
                                    <tr>
                                        <td>0 </td>
                                        <td>
                                            <a href="{{ url('storage/iasCertificate/CMS_certificate_sheet.xlsx')}}" target="_blank">Download CMS Certificate Excel File</a>
                                        </td>
                                    </tr>
                                @endif

                                @if (Auth::user()->user_type !== "operation_manager" &&  Auth::user()->user_type !== "operation_coordinator" && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')
                                    {{--                                <tr>--}}
                                    {{--                                    <td>1</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.auditor-log') }}">Auditor Log</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}

                                    {{--                                <tr>--}}
                                    {{--                                    <td>2</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.auditor-food') }}">Auditor Food</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}
                                    {{--                                <tr>--}}
                                    {{--                                    <td>3</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.auditor-energy') }}">Auditor Energy</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <a href="{{ route('reports.list-of-index') }}">List of Auditors</a>
                                        </td>
                                    </tr>
                                    {{--                                <tr>--}}
                                    {{--                                    <td>5</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.auditor-evaluation-schedule') }}">Auditor Evaluation--}}
                                    {{--                                            Schedule</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <a href="{{ route('reports.list-of-technical-expert-index') }}">Technical
                                                Expert
                                                List</a>
                                        </td>
                                    </tr>

                                    {{--                                <tr>--}}
                                    {{--                                    <td>7</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.list-of-generic-company-standard-index') }}">Generic--}}
                                    {{--                                            Company--}}
                                    {{--                                            Standard List</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}


                                    {{--                                <tr>--}}
                                    {{--                                    <td>8</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.list-of-food-company-standard-index') }}">Food--}}
                                    {{--                                            Company--}}
                                    {{--                                            Standard List</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}


                                    {{--                                <tr>--}}
                                    {{--                                    <td>9</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.list-of-energy-company-standard-index') }}">Energy--}}
                                    {{--                                            Company--}}
                                    {{--                                            Standard List</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}
                                    {{--                                <tr>--}}
                                    {{--                                    <td>10</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.list-of-companies-index') }}">List of Companies</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}
                                @endif
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <a href="{{ route('reports.list-of-certificates-index') }}">List of
                                                Certified
                                                Clients
                                            </a>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <a href="{{ route('reports.list-of-certificates-index') }}">List of
                                                Certified
                                                Clients
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                                @if (Auth::user()->user_type !== "operation_manager" &&  Auth::user()->user_type !== "operation_coordinator" && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                    <tr>
                                        <td>4</td>
                                        <td>
                                            <a href="{{ route('reports.audit-planning-sheet-index') }}">Audit Planning
                                                Sheet</a>
                                        </td>
                                    </tr>

                                    {{--                                <tr>--}}
                                    {{--                                    <td>3</td>--}}
                                    {{--                                    <td>--}}
                                    {{--                                        <a href="{{ route('reports.surveillance-and-reaudit-due-index') }}">Surveillance and Reaudit Due Report</a>--}}
                                    {{--                                    </td>--}}
                                    {{--                                </tr>--}}

                                @endif
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                    <tr>
                                        <td>5</td>
                                        <td>
                                            <a href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit') }}">Surveillance
                                                and Reaudit Due Report</a>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <a href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit') }}">Surveillance
                                                and Reaudit Due Report</a>
                                        </td>
                                    </tr>
                                @endif
                                @if (auth()->user()->user_type !== 'operation_manager' && auth()->user()->user_type !== 'operation_coordinator' && auth()->user()->user_type !== 'scheme_coordinator' && auth()->user()->user_type !== 'management')

                                    <tr>
                                        <td>6</td>
                                        <td>
                                            <a href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit-conducted-report') }}">Audits
                                                Conducted Report</a>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>7</td>
                                        <td>
                                            <a href="{{ route('reports.audit-justification-not-certified-clients') }}">
                                                List of Non Certified Clients/Initial Audits InProcess
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>
                                            <a href="{{ route('reports.company-job-number-index') }}">
                                                List of Job Numbers for Duplication Check
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>
                                            <a href="{{ route('reports.certificate-not-uploaded-index') }}">
                                                List of Certificates Not Uploaded</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>
                                            <a href="{{ route('reports.surveillance-and-reaudit-due-withdrawn-with-actual-audit') }}">
                                                List of Withdrawn Clients
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>
                                            <a href="{{ route('reports.statistics.count') }}">IAF CertSearch Upload
                                                Log</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>
                                            <a href="{{ route('reports.statistics.error') }}">IAF CertSearch Errors
                                                Log</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>
                                            <a href="{{ route('reports.statistics.success') }}">IAF CertSearch Success
                                                Log</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>14</td>
                                        <td>
                                            <a href="{{ route('reports.surveillance-and-reaudit-due-index-with-actual-audit-conducted-special-report') }}">Audits
                                                Conducted Report Special</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>15</td>
                                        <td>
                                            <a href="{{ route('reports.list-of-technical-expert-with-approval-date') }}">List of Technical Experts Report With Excel</a>
                                        </td>
                                    </tr>

                                @else
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <a href="{{ route('reports.audit-justification-not-certified-clients') }}">
                                                List of Non Certified Clients/Initial Audits InProcess
                                            </a>
                                        </td>
                                    </tr>
                                @endif

                                @if (auth()->user()->user_type === 'operation_manager' && auth()->user()->region->title === 'Pakistan Region')
                                    <tr>
                                        <td>4</td>
                                        <td>
                                            <a href="{{ route('reports.invoice.and.letter.index') }}">
                                                Invoices & Letters Report
                                            </a>
                                        </td>
                                    </tr>
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


