<!DOCTYPE html>
<html>
<head>
    <title>List of Auditors Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>
        .borderColor {
            border-style: solid;
            border-color: #000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
@php
    $image_path = '/img/logo.png';
@endphp

<table border="1px" cellspacing="0" width="100%">
    <tr>
        <td colspan="8">
            <img src="{{ public_path() . $image_path }}" alt="">
        </td>
    </tr>
</table>
<br>
@php $check=0 @endphp
@if(!empty($auditors))
    @foreach($auditors as $key=>$auditor)
        @php $check++ @endphp
        <div class="borderColor @if( $check % 2 == 0 ) page-break @endif">
            <h3 style="text-align: center">General Information</h3>
            <table border="1px" cellspacing="1" align="center" width="100%">
                <tr>
                    <td><b>Auditor ID:</b></td>
                    <td>
                        {{$key + 1}}
                    </td>
                </tr>
                <tr>
                    <td><b>Name:</b></td>
                    <td>
                        {{ $auditor->fullName() }}
                    </td>
                </tr>
                <tr>
                    <td><b>Email:</b></td>
                    <td>
                        {{ $auditor->email}}
                    </td>
                </tr>
                <tr>
                    <td><b>Mobile Number:</b></td>
                    <td>
                        {{ $auditor->country->phone_code . ltrim($auditor->phone, '0') }}
                    </td>
                </tr>
                <tr>
                    <td><b>Employement Type:</b></td>
                    <td>
                        {{ str_replace('_', ' ',ucwords($auditor->job_status)) }}
                    </td>
                </tr>
                <tr>
                    <td><b>Agreement Date:</b></td>
                    <td>
                        @foreach($auditor->confidentialityAgreements as $agreement)
{{--                            @php--}}
{{--                                $agreement_date = new DateTime($agreement->date_signed);--}}
{{--                            @endphp--}}
{{--                            {{ $agreement_date->format('d/m/Y') }}--}}
                            {{ $agreement->date_signed }}
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td><b>Region:</b></td>
                    <td>
                        {{ $auditor->region->title }}
                    </td>
                </tr>
                <tr>
                    <td><b>Country:</b></td>
                    <td>
                        {{ $auditor->country->name  }}
                    </td>

                </tr>
                <tr>
                    <td><b>Profile Status:</b></td>
                    <td>
                        {{ ucfirst($auditor->auditor_status)  }}
                    </td>
                </tr>
            </table>
            <h3 style="text-align: center">Standard Information</h3>
            <table border="1px" cellspacing="1" align="center" width="100%">
                <tr>
                    <td colspan="{{ ((count($auditor->auditorStandards)) > 0) ? count($auditor->auditorStandards) : 1  }}">
                        <b>Standards:</b>
                    </td>
                    @if(!empty($auditor->auditorStandards))
                        @foreach($auditor->auditorStandards as $a_standard)
                            @if(is_null($a_standard->deleted_at))

                                <td> {{ $a_standard->standard->name }}
                                </td>
                            @endif
                        @endforeach
                    @endif


                </tr>
                <tr>
                    <td colspan="{{ ((count($auditor->auditorStandards)) > 0) ? count($auditor->auditorStandards) : 1  }}">
                        <b>Standard Status</b>
                    </td>
                    @if(!empty($auditor->auditorStandards))
                        @foreach($auditor->auditorStandards as $a_standard)
                            @if(is_null($a_standard->deleted_at))
                                <td> {{ ucfirst($a_standard->status) }}</td>
                            @endif
                        @endforeach
                    @endif


                </tr>
                <tr>
                    <td colspan="{{ ((count($auditor->auditorStandards)) > 0) ? (count($auditor->auditorStandards)) : 1 }}">
                        <b>Role:</b>
                    </td>
                    @if(!empty($auditor->auditorStandards))
                        @foreach($auditor->auditorStandards as $a_standard)
                            @if(is_null($a_standard->deleted_at))
                                @php
                                    $auditor_standard_grades = \App\AuditorStandardGrade::where('auditor_standard_id', $a_standard->id)->where('status','current')->get()
                                @endphp
                                @if(!empty($auditor_standard_grades) && (count($auditor_standard_grades) > 0))
                                    @foreach($auditor_standard_grades as $auditor_standard_grade)
                                        <td>
                                            {{ $auditor_standard_grade->grade->name }}
                                        </td>
                                    @endforeach
                                @else
                                    <td>N/A</td>
                                @endif
                            @endif
                        @endforeach
                    @else
                        <td> ---</td>
                    @endif
                </tr>
                <tr>
                    <td colspan="{{ ((count($auditor->auditorStandards)) > 0) ? (count($auditor->auditorStandards)) : 1 }}">
                        <b>Accreditations:</b>
                    </td>
                    @php
                        $accreditation_count = 0;
                    @endphp

                    @if(!empty($auditor->auditorStandards))
                        @foreach($auditor->auditorStandards as $a_standard)
                            @if(is_null($a_standard->deleted_at))
                                @php
                                    $auditor_standard_codes = \App\AuditorStandardCode::where('auditor_standard_id', $a_standard->id)->groupBy('accreditation_id')->get();
                                    $accreditation_count = 0;
                                @endphp
                                <td>
                                    @if(!empty($auditor_standard_codes))
                                        @foreach($auditor_standard_codes as $auditorStandardCode)
                                            @if($auditorStandardCode->accreditation_id != $accreditation_count)
                                                {{ $auditorStandardCode->accreditation->name }},
                                            @endif
                                            @php
                                                $accreditation_count =$auditorStandardCode->accreditation_id;
                                            @endphp

                                        @endforeach
                                    @endif
                                </td>
                            @endif
                        @endforeach
                    @else
                        '--'
                    @endif


                </tr>
                <tr>
                    <td colspan="{{ ((count($auditor->auditorStandards)) > 0) ? (count($auditor->auditorStandards)) : 1 }}">
                        <b>IAF Codes:</b>
                    </td>
                    @php
                        $iaf_count = 0;
                    @endphp
                    @if(!empty($auditor->auditorStandards))
                        @foreach($auditor->auditorStandards as $a_standard)
                            @if(is_null($a_standard->deleted_at))
                                @php
                                    $auditor_standard_codes = \App\AuditorStandardCode::where('auditor_standard_id', $a_standard->id)->groupBy('iaf_id')->get()
                                @endphp
                                <td>
                                    @if(!empty($auditor_standard_codes))

                                        @foreach($auditor_standard_codes as $auditorStandardCode)
                                            @if($auditorStandardCode->iaf_id != $iaf_count)
                                                {{ $auditorStandardCode->iaf->code }},
                                            @endif
                                            @php
                                                $iaf_count =$auditorStandardCode->iaf_id;
                                            @endphp
                                        @endforeach
                                    @endif
                                </td>
                            @endif
                        @endforeach
                    @else
                        '--'
                    @endif


                </tr>
            </table>
        </div>
        <br>
    @endforeach
@endif

</body>
</html>
