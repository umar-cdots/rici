@extends('layouts.master')
@section('title', "Operation Manager List")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <div class="col-sm-4 floting">
                            <h1>Operation Manager</h1>
                        </div>
                        <div class="col-sm-6 floting">

                            @can('create_operation_manager')
                                <a href="{{route('operation-manager.create')}}" class="add_comp"><label
                                            class="text-capitalize"><i class="fa fa-plus-circle text-lg"
                                                                       aria-hidden="true"></i> Add New Operation
                                        Manager</label></a>
                            @endcan


                        </div>
                    </div>

                    <div class="col-sm-4">
                        {{ Breadcrumbs::render('operationManager') }}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of Operation Manager</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="8%;">Sr. #</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($operation_managers as $operation_manager)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $operation_manager->fullName() }}</td>
                                        <td>
                                            {{ $operation_manager->email }}
                                        </td>
                                        <td>{{ $operation_manager->username }}</td>
                                        <td>{{ ucfirst($operation_manager->status) }}</td>
                                        <td>
                                            <ul class="data_list">

                                                    @can('show_operation_manager')
                                                        <li>
                                                            <a href="{{route('operation-manager.show', $operation_manager->id)}}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </li>
                                                    @endcan

                                                    @can('edit_operation_manager')
                                                        <li>
                                                            <a href="{{route('operation-manager.edit', $operation_manager->id)}}"><i
                                                                        class="fa fa-pencil"></i></a>
                                                        </li>
                                                    @endcan

                                                    @can('delete_operation_manager')
                                                        <li>
                                                            <form action="{{ route('operation-manager.destroy', $operation_manager->id) }}"
                                                                  id="delete_{{$operation_manager->id}}" method="POST">
                                                                {{ method_field('DELETE') }}
                                                                @csrf
                                                                <a style="margin-left:10px;" href="javascript:void(0)"
                                                                   onclick="if(confirmDelete()){ document.getElementById('delete_{{$operation_manager->id}}').submit(); }">

                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </form>
                                                        </li>
                                                    @endcan
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#example1').dataTable();
        });

        function confirmDelete() {
            var r = confirm("Are you sure you want to perform this action");
            if (r === true) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush