@extends('layouts.master')
@section('title', "Operation Manager")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <form action="" id="main-form">
            <section class="content">
                <div class="container-fluid dashboard_tabs">
                    <div class="row mb-2 mrgn">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-dark dashboard_heading">EDIT <span>OPERATION MANAGER</span></h1>

                        </div><!-- /.col -->
                        <!-- /.col -->
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Full Name"
                                                   name="fullname" value="{{ $user->fullname() }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="margin-right: -2px;">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Enter Email"
                                                   name="email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth *</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                        </div>
                                        <input type="text" name="dob" id="dob"
                                               placeholder="dd//mm/yyyy" value="{{date('d-m-Y', strtotime($user->dob)) }}"
                                               class="float-right active form-control dob">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="row mb-1">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country *</label>
                                        <select name="country_id" class="form-control countries">
                                            <option value="">Select Country</option>
                                            @if(!empty($resident_countries))
                                                @foreach($resident_countries as $country)
                                                    <option value="{{ $country->id }}" {{ ($user->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City *</label>
                                        <select name="city_id" class="form-control cities">
                                            @if(!empty($resident_cities))
                                                @foreach($resident_cities as $city)
                                                    <option value="{{ $city->id }}" {{ ($city->id == $user->city_id) ? 'selected' : ''}}>{{ $city->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Cell Number *</label>
                                    <div class="row">

                                        <div class="col-md-2" style="padding-right: 0;">
                                            <input type="text" class="form-control" value="{{ $user->country->phone_code ?? '-'}}" id="country_code" name="country_code" readonly>
                                        </div>
                                        <div class="col-md-10" style="padding-left: 0;">
                                            <div class="form-group mb-0">

                                                <input type="text" class="form-control popFld phone" value="{{ ltrim($user->phone_number, '0') }}" name="cell_no" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Landline Number *</label>
                                        <input type="tel" class="form-control number_field"
                                               name="landline" value="{{ $user->operation_manager->landline }}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Joining Date *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                            </div>

                                            <input type="text" name="joining_date" id="joining_date"
                                                   placeholder="dd//mm/yyyy"
                                                   value="{{ date('d-m-Y', strtotime($user->operation_manager->joining_date)) }}"
                                                   class="float-right active form-control joining_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Address *</label>
                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter Address"
                                               name="address" value="{{ $user->address }}">
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Account Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Status *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="active" {{ $user->status == 'active' ? 'checked' : ''}}>
                                            <label class="job_status">Active</label>
                                        </div>
{{--                                        <div class="col-md-4 floting">--}}
{{--                                            <input type="radio" name="r3" class="icheck-blue"--}}
{{--                                                   value="suspended" {{ $user->status == 'suspended' ? 'checked' : ''}}>--}}
{{--                                            <label class="job_status">Suspended</label>--}}
{{--                                        </div>--}}
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="withdrawal" {{ $user->status == 'withdrawal' ? 'checked' : ''}}>
                                            <label class="job_status">WithDrawal</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Region *</label>
                                    <select name="region_id" class="form-control" id="region_id">
                                        <option value="">Select Region</option>
                                        <option value="{{ $selected_region->id }}"
                                                selected>{{ $selected_region->title }}</option>
                                        @if(!empty($regions))
                                            @foreach($regions as $region)
                                                <option value="{{ $region->id }}" {{ ($region->id == $user->region_id) ? 'selected' : '' }}>{{ $region->title }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="display: grid">
                                        <label for="country_id">List of countries *</label>
                                        <select id="country_id" class="form-control js-example-basic-multiple" disabled
                                                name="country_id[]" multiple="multiple" style="width: 200px;">
                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" style="    display: grid;">
                                        <label for="zone_id">List of Zones*</label>

                                        <select id="zone_id" class="form-control js-example-basic-multiple"
                                                name="zone_id[]" multiple="multiple" style="width: 200px;">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group" style="    display: grid;">
                                        <label for="city_id">List of Cities*</label>

                                        <select id="city_id" class="form-control js-example-basic-multiple"
                                                name="city_id[]" multiple="multiple" style="width: 200px;">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username *</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"
                                                                          aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="username"
                                               value="{{ $user->username }}">
                                    </div>
                                </div>
                                @if(auth()->user()->user_type == 'admin')
                                    <div class="col-md-4">
                                        <label for="password">Password</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                                            </div>
                                            <input type="password" class="form-control" name="password" id="password" value="">
                                        </div>
                                    </div>
                                @endif
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Is Enable Post Audit *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="is_enabled" class="icheck-blue"
                                                   value="true" {{ $user->is_enabled == true ? 'checked' : ''}}>
                                            <label class="job_status">Active</label>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="is_enabled" class="icheck-blue"
                                                   value="false" {{ $user->is_enabled == false ? 'checked' : ''}}>
                                            <label class="job_status">In Active</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Remarks *</label>
                                        <textarea name="remarks" rows="5"
                                                  class="form-control">{!! $user->remarks !!}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <label>Signature Image * <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="signature_file"
                                                       id="exampleInputFile1" onchange="readURLMany(this,1)">
                                                <label class="custom-file-label" for="exampleInputFile1">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ ($user->operation_manager->signature_image) ? asset('/uploads/operation_manager/'.$user->operation_manager->signature_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld1">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Profile Image <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="profile_image"
                                                       id="exampleInputFile2" onchange="readURLMany(this,2)">
                                                <label class="custom-file-label" for="exampleInputFile2">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            <img src="{{ ($user->operation_manager->profile_image) ? asset('/uploads/operation_manager/'.$user->operation_manager->profile_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld2">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Additional Files <span>Max 10mb</span></label>
                                        <div class="input-group form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="additional_files"
                                                       id="exampleInputFile3" onchange="readURLMany(this,3)">
                                                <label class="custom-file-label" for="exampleInputFile3">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                            </div>
                                        </div>
                                        <div class="image form-group">
                                            @if($user->operation_manager->additional_files)
                                                <div class="image form-group" id="file">
                                                    <h6>Selected File: <span><a target="_blank"
                                                                    href="/uploads/operation_manager/{{$user->operation_manager->additional_files}}">{{ $user->operation_manager->additional_files }}</a><span><i class="fa fa-remove" style="float: right"
                                                                                                                                                                                                                  onclick="removeAdditionalFiles('{{ $user->operation_manager->user_id }}')"></i></span></span>
                                                    </h6>
                                                </div>
                                            @else
                                                <div class="image form-group" id="file">

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-primary mrgn">
                                <div class="card-header cardNewHeader">
                                    <h3 class="card-title">Role & Permissions</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body card_cutom">
                                    <table class="table table-responsive table-light">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Role</th>
                                            <th>Guard Name</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                @if($role->id == 4)
                                                <tr>
                                                    <td><input type="radio" class="icheck-blue" name="roles[]"
                                                               value="{{ $role->id }}"
                                                               @foreach ($user->roles as $u_role)
                                                               @if($u_role->id == $role->id)
                                                               checked
                                                                @endif
                                                                @endforeach
                                                        ></td>
                                                    <td>{{ ucwords($role->name) }}</td>
                                                    <td>{{ $role->guard_name }}</td>
                                                    <td>
                                                        @if($role->permissions->isNotEmpty())
                                                            @foreach($role->permissions as $permission)
                                                                @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                                    ,
                                                                @endif
                                                                {{ ucwords($permission->heading) }}
                                                            @endforeach
                                                        @else
                                                            no permissions
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

{{--                        <div class="col-sm-5">--}}
{{--                            <div class="card card-primary mrgn">--}}
{{--                                <div class="card-header cardNewHeader">--}}
{{--                                    <h3 class="card-title">Permissions</h3>--}}
{{--                                </div>--}}
{{--                                <!-- /.card-header -->--}}
{{--                                <div class="card-body card_cutom">--}}
{{--                                    <table class="table table-responsive table-light">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--                                            <th>Assign</th>--}}
{{--                                            <th>Role</th>--}}
{{--                                            <th>Guard Name</th>--}}
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--                                        @if(!empty($permissions))--}}
{{--                                            @foreach($permissions as $permission)--}}
{{--                                                <tr>--}}
{{--                                                    <td><input type="checkbox" class="icheck-blue" name="permissions[]"--}}
{{--                                                               value="{{ $permission->id }}"--}}
{{--                                                               @foreach ($user->permissions as $u_permission)--}}
{{--                                                               @if($u_permission->id == $permission->id)--}}
{{--                                                               checked--}}
{{--                                                                @endif--}}
{{--                                                                @endforeach--}}
{{--                                                        ></td>--}}
{{--                                                    <td> {{ $permission->heading }}</td>--}}
{{--                                                    <td>{{ $permission->guard_name }}</td>--}}
{{--                                                </tr>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                </div>--}}
{{--                                <!-- /.card-body -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-3 floting"></div>
                            <div class="col-md-3 floting">
                                <a href="{{ route('operation-manager.index') }}"
                                   class="btn btn-block btn-danger  btn_cancel">Cancel
                                </a>
                            </div>
                            <div class="col-md-3 floting no_padding">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success btn_save">Save
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- /.content -->
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">
    <style>
        span.select2.select2-container.select2-container--default {
            width: 100% !important;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('.joining_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {

            var joininDateArray = this.value.split('-');
            var newJoiningDate = joininDateArray[2] + '-' + joininDateArray[1] + '-' + joininDateArray[0];
            var dobArray = $('.dob').val().split('-');
            var newdobDate = dobArray[2] + '-' + dobArray[1] + '-' + dobArray[0];


            var dob = moment(newdobDate).format('YYYY-MM-DD');
            var joiningDate = moment(newJoiningDate).format('YYYY-MM-DD');

            if (joiningDate > dob) {
            } else {

                toastr['error']('Joining Date always greater than DOB');
                $('#joining_date').val('');
            }


        });
        $(document).ready(function () {

            (function getRegionZonesCountryCity() {
                $("#country_id").attr('disabled', false);
                $("#city_id").attr('disabled', false);


                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.regionCountriesCities') }}",
                    data: {region_id: '{{ $user->region_id }}'},
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            var cities_html = "";
                            var zones_html = "";
                            $.each(response.data.region[0].countries, function (i, obj) {
                                html += '<option value="' + obj.id + '" selected>' + obj.name + '</option>';
                                $.each(obj.cities, function (i, city) {
                                    cities_html += '<option value="' + city.id + '" selected>' + city.name + '</option>';
                                });
                                if (obj.zones.length > 0) {
                                    $.each(obj.zones, function (i, zone) {
                                        zones_html += '<option value="' + zone.id + '" selected>' + zone.name + '</option>';
                                        if (zone.cities.length > 0) {
                                            $.each(zone.cities, function (i, city) {
                                                cities_html += '<option value="' + city.id + '" selected>' + city.name + '</option>';
                                            });
                                        }
                                    });
                                }


                            });

                            $("#country_id").html(html);
                            $("#country_id").select2().trigger('change');
                            $("#country_id").attr('disabled', true);

                            $("#zone_id").html(zones_html);
                            $("#zone_id").select2().trigger('change');
                            $("#zone_id").attr('disabled', true);

                            $("#city_id").html(cities_html);
                            $("#city_id").select2().trigger('change');
                            $("#city_id").attr('disabled', true);


                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });


            })();

            {{--var intlTelInputConfig = {--}}
            {{--    initialCountry: "auto",--}}
            {{--    autoPlaceholder: false,--}}
            {{--    geoIpLookup: function (callback) {--}}
            {{--        $.get("{{ route('ajax.ipInfo') }}", function () {--}}
            {{--        }, "jsonp").always(function (resp) {--}}
            {{--            var countryCode = (resp && resp.country) ? resp.country : "";--}}
            {{--            callback(countryCode);--}}
            {{--        });--}}
            {{--    },--}}
            {{--    utilsScript: "{{ asset('plugins/intlTelInput/js/utils.js') }}", // just for formatting/placeholders etc--}}
            {{--    blur: function () {--}}
            {{--        console.log($(this));--}}
            {{--    }--}}
            {{--};--}}
            {{--$(".phone").intlTelInput(intlTelInputConfig);--}}
            {{--setTimeout(function () {--}}
            {{--    $('.phone').trigger('blur');--}}
            {{--}, 2000);--}}
            {{--$('.phone').on('keyup blur', function () {--}}

            {{--    var number = $(this).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);--}}
            {{--    if (number == '' || !$(this).intlTelInput("isValidNumber")) {--}}
            {{--        has_error = true;--}}
            {{--        $(".btn_save").attr("disabled", true);--}}
            {{--        $(this).closest('.form-group').addClass('text-danger');--}}
            {{--        $(this).addClass('is-invalid');--}}
            {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
            {{--        $(this).closest('.form-group').append('<small class="text-danger" style="float:left">Invalid Contact Number.</small>');--}}
            {{--    } else {--}}
            {{--        $(this).val(number);--}}
            {{--        $(".btn_save").attr("disabled", false);--}}
            {{--        $(this).closest('.form-group').removeClass('text-danger');--}}
            {{--        $(this).removeClass('is-invalid');--}}
            {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
            {{--    }--}}
            {{--});--}}

        });
        $('.js-example-basic-multiple').select2();

        function readURLMany(input, id = null) {
            if (input.id == 'exampleInputFile3') {
                var filename = input.files[0].name;
                var html = '';
                if (input.files && input.files[0]) {
                    html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
                }
                $('#file').html(html);
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#upld' + id).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }

        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });
        $("#region_id").change(function () {
            var selected_regions = [];
            $("#country_id").attr('disabled', false);
            $("#city_id").attr('disabled', false);

            $.each($("#region_id option:selected"), function () {
                selected_regions.push($(this).val());
            });

            if ($(this).val() != '') {
                if (selected_regions.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.regionCountriesCities') }}",
                        data: {region_id: selected_regions},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            if (response.status == "success") {
                                var html = "";
                                var cities_html = "";
                                var zones_html = "";
                                $.each(response.data.region[0].countries, function (i, obj) {
                                    html += '<option value="' + obj.id + '" selected>' + obj.name + '</option>';
                                    $.each(obj.cities, function (i, city) {
                                        cities_html += '<option value="' + city.id + '" selected>' + city.name + '</option>';
                                    });
                                    if (obj.zones.length > 0) {
                                        $.each(obj.zones, function (i, zone) {
                                            zones_html += '<option value="' + zone.id + '" selected>' + zone.name + '</option>';
                                            if (zone.cities.length > 0) {
                                                $.each(zone.cities, function (i, city) {
                                                    cities_html += '<option value="' + city.id + '" selected>' + city.name + '</option>';
                                                });
                                            }
                                        });
                                    }


                                });

                                $("#country_id").html(html);
                                $("#country_id").select2().trigger('change');
                                $("#country_id").attr('disabled', true);

                                $("#zone_id").html(zones_html);
                                $("#zone_id").select2().trigger('change');
                                $("#zone_id").attr('disabled', true);

                                $("#city_id").html(cities_html);
                                $("#city_id").select2().trigger('change');
                                $("#city_id").attr('disabled', true);


                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    $("#zone_id").html('<option value=""></option>');
                    $("#zone_id").select2().trigger('change');
                    $("#zone_id").attr('disabled', true);

                    $("#country_id").html('<option value=""></option>');
                    $("#country_id").select2().trigger('change');
                    $("#country_id").attr('disabled', true);

                    $("#city_id").html('<option value=""></option>');
                    $("#city_id").select2().trigger('change');
                    $("#city_id").attr('disabled', true);



                }
            } else {

                $("#zone_id").html('<option value=""></option>');
                $("#zone_id").select2().trigger('change');
                $("#zone_id").attr('disabled', true);

                $("#country_id").html('<option value=""></option>');
                $("#country_id").select2().trigger('change');
                $("#country_id").attr('disabled', true);

                $("#city_id").html('<option value=""></option>');
                $("#city_id").select2().trigger('change');
                $("#city_id").attr('disabled', true);
            }


        });
        $('#main-form').submit(function (e) {
            e.preventDefault();
            var form = $('#main-form')[0];
            var formData = new FormData(form);
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('operation-manager.update') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        console.log('data submited');
                        setTimeout(function () {
                            window.location.href = '{{ route('operation-manager.index') }}';
                        }, 2000);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        function removeAdditionalFiles(id) {
            var request = {
                'user_id': parseInt(id),
                'user_type': 'operation_manager'
            }
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.remove.additionalfiles') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {

                    if (response.status == "success" || response.status == '200') {
                        toastr['success']("File Removed Successfully.");
                        window.location.reload();

                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }


    </script>
@endpush
