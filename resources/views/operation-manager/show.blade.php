@extends('layouts.master')
@section('title', "Operation Manager")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <form action="" id="main-form">
            <section class="content">
                <div class="container-fluid dashboard_tabs">
                    <div class="row mb-2 mrgn">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-dark dashboard_heading">VIEW <span>OPERATION MANAGER</span></h1>

                        </div><!-- /.col -->
                        <!-- /.col -->
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Personal Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Full Name"
                                                   name="fullname" value="{{ $user->fullname() }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" style="margin-right: -2px;">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Enter Email"
                                                   name="email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Date of Birth</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                        </div>
                                        <input type="text" name="dob" id="dob"
                                               placeholder="dd//mm/yyyy"
                                               value="{{date('d-m-Y', strtotime($user->dob)) }}"
                                               class="float-right active form-control dob">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country *</label>
                                        <select name="country_id" class="form-control countries">
                                            <option value="">Select Country</option>
                                            @if(!empty($resident_countries))
                                                @foreach($resident_countries as $country)
                                                    <option value="{{ $country->id }}" {{ ($user->country_id == $country->id) ? 'selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City *</label>
                                        <select name="city_id" class="form-control cities">
                                            @if(!empty($resident_cities))
                                                @foreach($resident_cities as $city)
                                                    <option value="{{ $city->id }}" {{ ($city->id == $user->city_id) ? 'selected' : ''}}>{{ $city->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cell Number *</label>

                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="tel" class="form-control popFld phone number_field"
                                                   name="cell_no"
                                                   value="{{ $user->country->phone_code . ltrim($user->phone_number, '0') }}"
                                                   required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Landline *</label>
                                        <div class="input-group mb-3">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Enter Landline"
                                                   name="landline" value="{{ $user->operation_manager->landline }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Joining Date *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
									  <span class="input-group-text">
										<i class="fa fa-calendar"></i>
									  </span>
                                            </div>
                                            <input type="text" name="joining_date" id="joining_date"
                                                   placeholder="dd//mm/yyyy"
                                                   value="{{ date('d-m-Y', strtotime($user->operation_manager->joining_date)) }}"
                                                   class="float-right active form-control joining_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Address *</label>
                                    <div class="input-group">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter Address"
                                               name="address" value="{{ $user->address }}">
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">Account Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Status *</label>
                                    <div class="form-group">
                                        <div class="col-md-4 floting no_padding">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="active" {{ $user->status == 'active' ? 'checked' : ''}}>
                                            <label class="job_status">Active</label>
                                        </div>
                                        {{--                                        <div class="col-md-4 floting">--}}
                                        {{--                                            <input type="radio" name="r3" class="icheck-blue"--}}
                                        {{--                                                   value="applied" {{ $user->status == 'applied' ? 'applied' : ''}}>--}}
                                        {{--                                            <label class="job_status">Applied</label>--}}
                                        {{--                                        </div>--}}
                                        <div class="col-md-4 floting">
                                            <input type="radio" name="r3" class="icheck-blue"
                                                   value="withdrawal" {{ $user->status == 'withdrawal' ? 'checked' : ''}}>
                                            <label class="job_status">WithDrawal</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Region *</label>
                                    <select name="region_id" class="form-control" id="region_id">
                                        <option value="">Select Region</option>
                                        <option value="{{ $selected_region->id }}"
                                                selected>{{ $selected_region->title }}</option>
                                        @if(!empty($regions))
                                            @foreach($regions as $region)
                                                <option value="{{ $region->id }}" {{ ($region->id == $user->region_id) ? 'selected' : '' }}>{{ $region->title }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="display: grid">
                                        <label for="country_id">List of countries *</label>
                                        <select id="country_id" class="form-control js-example-basic-multiple" disabled
                                                name="country_id[]" multiple="multiple" style="width: 200px;">
                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" style="    display: grid;">
                                        <label for="zone_id">List of Zones*</label>

                                        <select id="zone_id" class="form-control js-example-basic-multiple"
                                                name="zone_id[]" multiple="multiple" style="width: 200px;">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group" style="    display: grid;">
                                        <label for="city_id">List of Cities*</label>

                                        <select id="city_id" class="form-control js-example-basic-multiple"
                                                name="city_id[]" multiple="multiple" style="width: 200px;">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"
                                                                          aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="username"
                                               value="{{ $user->username }}">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <textarea name="remarks" rows="5"
                                                  class="form-control">{!! $user->remarks !!}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <label>Signature Image </label>
                                        <div class="image form-group">
                                            <img src="{{ ($user->operation_manager->signature_image) ? asset('/uploads/operation_manager/'.$user->operation_manager->signature_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld1">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Profile Image</label>
                                        <div class="image form-group">
                                            <img src="{{ ($user->operation_manager->profile_image) ? asset('/uploads/operation_manager/'.$user->operation_manager->profile_image) : asset('img/user2-160x160.jpg') }}"
                                                 class="img-circle elevation-2"
                                                 alt="User Image" width="32" id="upld2">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <label>Additional Files</label>
                                        <div class="image form-group">
                                            @if($user->operation_manager->additional_files)
                                                <div class="image form-group" id="file">
                                                    <h6>Selected File: <span><a
                                                                    href="#">{{ $user->operation_manager->additional_files }}</a></span>
                                                    </h6>
                                                </div>
                                            @else
                                                <div class="image form-group" id="file">

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-primary mrgn">
                                <div class="card-header cardNewHeader">
                                    <h3 class="card-title">Role & Permissions</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body card_cutom">
                                    <table class="table table-responsive table-light">
                                        <thead>
                                        <tr>
                                            <th>Assign</th>
                                            <th>Role</th>
                                            <th>Guard Name</th>
                                            <th>Permissions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                @if($role->id == 4)
                                                    <tr>
                                                        <td><input type="radio" name="roles[]" value="{{ $role->id }}"
                                                                   @foreach ($user->roles as $u_role)
                                                                   @if($u_role->id == $role->id)
                                                                   checked
                                                                    @endif
                                                                    @endforeach
                                                            ></td>
                                                        <td>{{ $role->name }}</td>
                                                        <td>{{ $role->guard_name }}</td>
                                                        <td>
                                                            @if($role->permissions->isNotEmpty())
                                                                @foreach($role->permissions as $permission)
                                                                    @if($loop->index > 0 && $loop->index < count($role->permissions))
                                                                        ,
                                                                    @endif
                                                                    {{ $permission->heading }}
                                                                @endforeach
                                                            @else
                                                                no permissions
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

                        {{--                        <div class="col-sm-5">--}}
                        {{--                            <div class="card card-primary mrgn">--}}
                        {{--                                <div class="card-header cardNewHeader">--}}
                        {{--                                    <h3 class="card-title">Permissions</h3>--}}
                        {{--                                </div>--}}
                        {{--                                <!-- /.card-header -->--}}
                        {{--                                <div class="card-body card_cutom">--}}
                        {{--                                    <table class="table table-responsive table-light">--}}
                        {{--                                        <thead>--}}
                        {{--                                        <tr>--}}
                        {{--                                            <th>Assign</th>--}}
                        {{--                                            <th>Role</th>--}}
                        {{--                                            <th>Guard Name</th>--}}
                        {{--                                        </tr>--}}
                        {{--                                        </thead>--}}
                        {{--                                        <tbody>--}}
                        {{--                                        @if(!empty($permissions))--}}
                        {{--                                            @foreach($permissions as $permission)--}}
                        {{--                                                <tr>--}}
                        {{--                                                    <td><input type="checkbox" name="permissions[]"--}}
                        {{--                                                               value="{{ $permission->id }}"--}}
                        {{--                                                               @foreach ($user->permissions as $u_permission)--}}
                        {{--                                                               @if($u_permission->id == $permission->id)--}}
                        {{--                                                               checked--}}
                        {{--                                                                @endif--}}
                        {{--                                                                @endforeach--}}
                        {{--                                                        ></td>--}}
                        {{--                                                    <td>{{ $permission->name }}</td>--}}
                        {{--                                                    <td>{{ $permission->guard_name }}</td>--}}
                        {{--                                                </tr>--}}
                        {{--                                            @endforeach--}}
                        {{--                                        @endif--}}
                        {{--                                        </tbody>--}}
                        {{--                                    </table>--}}
                        {{--                                </div>--}}
                        {{--                                <!-- /.card-body -->--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-3 floting">
                                <a href="{{ route('operation-manager.index') }}"
                                   class="btn btn-block btn-danger  btn_cancel">Cancel
                                </a>
                            </div>
                            <div class="col-md-3 floting no_padding">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    <!-- /.content -->
@endsection
@push('styles')
    <style>
        span.select2.select2-container.select2-container--default {
            width: 100% !important;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('.joining_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {

            var joininDateArray = this.value.split('-');
            var newJoiningDate = joininDateArray[2] + '-' + joininDateArray[1] + '-' + joininDateArray[0];
            var dobArray = $('.dob').val().split('-');
            var newdobDate = dobArray[2] + '-' + dobArray[1] + '-' + dobArray[0];


            var dob = moment(newdobDate).format('YYYY-MM-DD');
            var joiningDate = moment(newJoiningDate).format('YYYY-MM-DD');

            if (joiningDate > dob) {
            } else {

                toastr['error']('Joining Date always greater than DOB');
                $('#joining_date').val('');
            }


        });
        $(document).ready(function () {
            (function getRegionZonesCountryCity() {
                $("#country_id").attr('disabled', false);
                $("#city_id").attr('disabled', false);


                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.regionCountriesCities') }}",
                    data: {region_id: '{{ $user->region_id }}'},
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            var cities_html = "";
                            var zones_html = "";
                            $.each(response.data.region[0].countries, function (i, obj) {
                                html += '<option value="' + obj.id + '" selected>' + obj.name + '</option>';
                                $.each(obj.cities, function (i, city) {
                                    cities_html += '<option value="' + city.id + '" selected>' + city.name + '</option>';
                                });
                                if (obj.zones.length > 0) {
                                    $.each(obj.zones, function (i, zone) {
                                        zones_html += '<option value="' + zone.id + '" selected>' + zone.name + '</option>';
                                        if (zone.cities.length > 0) {
                                            $.each(zone.cities, function (i, city) {
                                                cities_html += '<option value="' + city.id + '" selected>' + city.name + '</option>';
                                            });
                                        }
                                    });
                                }


                            });

                            $("#country_id").html(html);
                            $("#country_id").select2().trigger('change');
                            $("#country_id").attr('disabled', true);

                            $("#zone_id").html(zones_html);
                            $("#zone_id").select2().trigger('change');
                            $("#zone_id").attr('disabled', true);

                            $("#city_id").html(cities_html);
                            $("#city_id").select2().trigger('change');
                            $("#city_id").attr('disabled', true);


                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });


            })();
        });
        $('.js-example-basic-multiple').select2();
        $("#main-form :input").prop("disabled", true);
    </script>
@endpush