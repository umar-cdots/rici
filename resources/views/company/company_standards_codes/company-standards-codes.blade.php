<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-codes{{$standard_id}}">
        <input type="hidden" name="company_standard_id" value="{{$standard_id}}" class="company_standard_id">
        <input type="hidden" name="company_id" value="{{$company_id}}" class="company_id">
        <table id="FormalEducationTableCodes" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2 FormalEducationTableCodes{{$standard_id}}">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Accreditation</th>
                <th>IAF Code</th>
                <th>IAS Code</th>
                <th width="10%" class="action-column">Action</th>
            </tr>
            </thead>
            <tbody>


            @foreach($companyStandardCodes as $key => $companyStandardCode)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{!! $companyStandardCode->accreditation->name !!}</td>
                    <td>{{$companyStandardCode->iaf->code}}</td>
                    <td>{{$companyStandardCode->ias->code}}</td>

                    <td align="center" class="action-column-data">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)" class="editAuditorStandardsCode"
                                   onclick="getEditCodesModal({{$companyStandardCode->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="deleteAuditorStandardCode({{$companyStandardCode->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>

        </table>
        <div class="col-md-12" style="    margin-top: 10px;">
            <button type="button" class="btn btn-primary edit_btn pull-right"
                    data-toggle="modal" data-target="#addStandardCodesModal{{$standard_id}}"
                    onclick="getAccreditationByStandard('{{$standard_id}}')">Add Code
            </button>
        </div>
        <div class="clearfix"></div>
    </form>
</div>

<div id="addStandardCodesModal{{$standard_id}}" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="addStandardCodesForm{{$standard_id}}">
                <input type="hidden" name="company_standard_id" id="standard_id" value="{{$standard_id}}">

                <input type="hidden" name="company_standard_id_codes" id="company_standard_id_codes" value="{{$standard_id}}">
                <input type="hidden" name="company_id" value="{{$company_id}}">
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close" onclick="closeModel('{{$standard_id}}','add')">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">

                        <div class="mrgn">
                            <div class="row mrgn">


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Accreditations *</label>
                                        <select class="form-control codes_accreditations" name="accreditation_id"
                                                id="codes_accreditations-auditor-standard{{$standard_id}}">
                                            <option value="">Select Accreditation</option>

                                        </select>

                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>IAF Code *</label>
                                        <select name="iaf_id" class="form-control iaf_acceditations"
                                                id="iaf-auditor-standard{{$standard_id}}">
                                            <option value="">--Select IAF Code--</option>

                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>IAS Code *</label>
                                        <select name="ias_id[]" class="form-control ias_acceditations"
                                                id="ias-auditor-standard{{$standard_id}}" multiple="multiple">
                                        </select>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary"
                                        onclick="addStandardCodeSave({{$standard_id}})">Submit
                                </button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel"
                                        onclick="closeModel('{{$standard_id}}','add')">Cancel
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    .select2-container--default .select2-selection--single {

        height: 40px;
    }
</style>
<script>

    $('.ias_acceditations').select2({width: "100%"});

    function getAccreditationByStandard(standard_id) {

        var form = $(this).closest('form');
        var node = $(this);
        var node_to_modify = '.codes_accreditations';
        var request = "standard_id=" + standard_id;

        if (standard_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.companyAccreditationByStandardId') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.accreditations, function (i, obj) {

                            html += '<option  value="' + obj.id + '">' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);

                        $(node_to_modify).prepend("<option value='' selected>Select Accreditation</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Accreditation</option>");
        }

    }

    $('.codes_accreditations').change(function () {
        form = $(this).closest('form');
        node = $(this);
        node_to_modify = '.iaf_acceditations';
        var accreditation_id = $(this).val();
        var request = "accreditation_id=" + accreditation_id;

        if (accreditation_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.companyIafByAccreditation') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.iafs, function (i, obj) {

                            html += '<option value="' + obj.id + '" data-standard-id="' + obj.standard_id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        // sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>Select IAF</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select IAF</option>");
        }

    });
    $('.iaf_acceditations').change(function () {
        form = $(this).closest('form');
        node = $(this);
        node_to_modify = '.ias_acceditations';
        var iaf_id = $(this).val();
        var standard_id = $(this).find(':selected').data("standard-id");


        if (iaf_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.iasByIAF') }}",
                data: {iaf_id: iaf_id, standard_id: standard_id},
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.iases, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        // sortMe($(node_to_modify).find('option'));
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select IAS</option>");
        }

    });


    function addStandardCodeSave(standardID) {

        var form = $("#addStandardCodesForm" + standardID)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('company.standards.code.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                if (response.status === 'exist') {
                    toastr['error']("Code Already exist");
                } else {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        $("#addStandardCodesModal" + response.data.standard_id).modal('hide');
                        setTimeout(function () {
                            var standard_id = $('.company_standard_id').val();
                            var company_id = $('.company_id').val();
                            companyStandardCodesView(response.data.standard_id, response.data.company_id);
                        }, 1000);
                    }
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function deleteAuditorStandardCode(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('company.standards.code.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        var standard_id = $('.company_standard_id').val();
                        var company_id = $('.company_id').val();
                        companyStandardCodesView(response.data.standard_id, response.data.company_id);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditCodesModal(modelId) {
        var url = "{{ route('company.standards.code.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-auditor-standard-container").html(response);
                $("#editStandardCodesModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function closeModel(standardId, type) {
        if (type === 'add') {
            $("#addStandardCodesModal" + standardId).modal('hide');
        } else if (type === 'edit') {
            $("#editStandardCodesModal").modal('hide');
        }
    }


</script>
