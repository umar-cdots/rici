<div id="editStandardCodesModal" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="editStandardCodesForm">
                <input type="hidden" name="company_standard_code_id" value="{{$companyStandardCodes->id}}">
                <input type="hidden" name="company_standard_id" value="{{$companyStandardCodes->standard_id}}">
                <input type="hidden" name="company_id" value="{{$companyStandardCodes->company_id}}">
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close" onclick="closeModel('{{$companyStandardCodes->standard_id}}','edit')">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="mrgn">
                            <div class="row mrgn">


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Accreditations</label>
                                        <select class="form-control codes_accreditations" name="accreditation_id"
                                                id="codes_accreditations-auditor-standard{{$companyStandardCodes->standard_id}}">
                                            <option value="">Select Accreditation</option>
                                            @foreach($allAccreditations as $accreditation)
                                                <option value="{{$accreditation->id}}" {{ ($accreditation->id ==$companyStandardCodes->accreditation_id) ? 'selected' : '' }}>{{$accreditation->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>IAF Code</label>
                                        <select name="iaf_id" class="form-control iaf_acceditations"
                                                id="iaf-auditor-standard{{$companyStandardCodes->standard_id}}">
                                            <option value="">--Select IAF Code--</option>
                                            @foreach($iafs as $iaf)
                                                <option value="{{$iaf->id}}" {{ ($iaf->id ==$companyStandardCodes->iaf_id) ? 'selected' : '' }}>{{$iaf->code}} | {{$iaf->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>IAS Code</label>
                                        <select name="ias_id" class="form-control ias_acceditations"
                                                id="ias-auditor-standard{{$companyStandardCodes->standard_id}}">
                                            <option value="">--Select IAS Code--</option>
                                            @foreach($iass as $ias)
                                                <option value="{{$ias->id}}" {{ ($ias->id ==$companyStandardCodes->ias_id) ? 'selected' : '' }}>{{$ias->code}} | {{$ias->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary updateDataBtn">Submit</button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel" onclick="closeModel('{{$companyStandardCodes->standard_id}}','edit')">Cancel</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    .select2-container--default .select2-selection--single {

        height: 40px;
    }
</style>
<script>

    $('.ias_acceditations').select2({ width: "100%"});

    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardCodesForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('company.standards.code.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    $("#editStandardCodesModal").modal('hide');
                    setTimeout(function () {
                        var standard_id = $('.company_standard_id').val();
                        var company_id = $('.company_id').val();
                        companyStandardCodesView(response.data.standard_id, response.data.company_id);
                    }, 1000);


                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });


</script>