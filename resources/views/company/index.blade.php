@extends('layouts.master')
@section('title', "Company List")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-4 floting">
                            <h1>Companies</h1>
                        </div>
                        <div class="col-sm-4 floting">

                            @can('create_company')
                                <a href="{{ route('company.create') }}" class="add_comp ">
                                    <i class="fa fa-plus-circle text-lg"
                                       aria-hidden="true"></i> Add New
                                    Company
                                </a>
                            @endcan
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('company') }}
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Companies</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tabularData" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Sr. #</th>
                                    <th style="width: 27%;">Company Name</th>
                                    <th>Standard(s)</th>
                                    {{--                                    Add certification status--}}
                                    <th>Profile Status</th>
                                    <th style="width: 10%;">Country</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $key=>$company)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $company->name }}</td>
                                        <td>

                                            @foreach($companyStandards[$key] as $companyStandard)
                                                @if(!is_null($companyStandard->standard->name))
                                                    {!! $companyStandard->standard->name !!}
                                                    @if(!is_null($companyStandard->certificates))
                                                        ({{$companyStandard->certificates->certificate_status}})
                                                    @else
                                                        (N/A)
                                                    @endif

                                                    @if($companyStandard->is_ims == true)
                                                        (IMS)
                                                    @endif

                                                    <br>
                                                @endif
                                            @endforeach
                                            {{--                                            {!!  $company->companyStandards()->with('standard')->get()->pluck('standard.name')->implode('<br>')  !!}--}}

                                        </td>
                                        <td>{{ ucfirst($company->company_form_status) }}</td>
                                        <td>{{ $company->country ? ucfirst($company->country->name) :'' }}</td>
                                        <td>
                                            <ul class="data_list">


                                                @can('show_company')
                                                    <li>
                                                        <a href="{{ route('company.show', $company->id) }}"
                                                           title="View"><i
                                                                    class="fa fa-eye"></i>
                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('edit_company')

                                                    @if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator'))
                                                        @if($company->is_edit_access_to_operations === 1 || $company->is_edit_access_to_operations === '1')
                                                            <li>
                                                                <a href="{{ route('company.edit', $company->id) }}"
                                                                   title="Edit"><i
                                                                            class="fa fa-pencil"></i></a>
                                                            </li>
                                                        @endif
                                                    @else
                                                            <li>
                                                                <a href="{{ route('company.edit', $company->id) }}"
                                                                   title="Edit"><i
                                                                            class="fa fa-pencil"></i></a>
                                                            </li>
                                                    @endif

                                                @endcan
                                                @can('delete_company')
                                                    <li>

                                                        <form action="{{ route('company.destroy', $company->id) }}"
                                                              id="delete_{{$company->id}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <a style="margin-left:10px;" href="javascript:void(0)"
                                                               onclick="if(confirmDelete()){ document.getElementById('delete_{{$company->id}}').submit(); }">

                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </form>

                                                    </li>
                                                @endcan

                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }

        .clearable {
            background: #fff url(data:image/gif;base64,R0lGODlhBwAHAIAAAP///5KSkiH5BAAAAAAALAAAAAAHAAcAAAIMTICmsGrIXnLxuDMLADs=) no-repeat right -10px center;
            border: 1px solid #999;
            padding: 3px 18px 3px 4px; /* Use the same right padding (18) in jQ! */
            border-radius: 3px;
            transition: background 0.4s;
        }

        .clearable.x {
            background-position: right 5px center;
        }

        .dataTables_wrapper .dataTables_filter input {
            width: 300px !important;
        }

        .clearable.onX {
            cursor: pointer;
            width: 300px !important;
        }

        .clearable::-ms-clear {
            display: none;
            width: 0;
            height: 0;
        }

        .add_comp {
            background-color: #21409a;
            font-weight: bold;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            user-select: none;
            border: 1px solid transparent;
            padding: 0.375rem 0.5rem;
            font-size: 1rem;
            border-radius: 0.25rem;
            transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            color: #ffff;
            cursor: pointer;
            display: block;
        }

        .add_comp:hover {
            color: #ffffff !important;
            text-decoration: none !important;
            cursor: pointer !important;
        }

        .buttons-excel {
            cursor: pointer !important;
        }
    </style>
@endpush

@push('scripts')

    {{--    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{-- <script type="text/javascript" src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}

    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var rtable = $('#tabularData').DataTable({
                "order": [[0, "asc"]],
                dom: 'Blfrtip',
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "buttons": [
                    {
                        name: "Export",
                        extend: 'excelHtml5',
                        title: "Company List",
                        filename: "Company List",
                        "createEmptyCells": true,
                        sheetName: "Company List",

                    }


                ],
            });

            $('div.dataTables_filter input').addClass('clearable');

            function tog(v) {
                return v ? 'addClass' : 'removeClass';
            }

            $(document).on('div.dataTables_filter input', '.clearable', function () {

                $(this)[tog(this.value)]('x');

            }).on('mousemove', '.x', function (e) {

                $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');

            }).on('touchstart click change', '.onX', function (ev) {
                var temp = $(this).removeClass('x onX').val('');
                console.log(temp.length);

                if (temp.length == 1) {

                    rtable
                        .search('')
                        .columns().search('')
                        .draw();


                }


            });

        });

        function confirmDelete() {
            var r = confirm("Are you sure you want to delete this action");
            if (r === true) {
                return true;
            } else {
                return false;
            }
        }
    </script>
@endpush
