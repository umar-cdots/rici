<div class="tab-pane">
    <div class="sub_tabs">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills p-2 ">
                    <li class="nav-item initial_inquiry_form">
                        <a class="nav-link active show" href="#iif_tab2" data-toggle="tab">Initial Inquiry Forms</a>
                    </li>
                    <li class="nav-item proposal">
                        <a class="nav-link" href="#ppl_tab2" data-toggle="tab">Proposal</a>
                    </li>
                    <li class="nav-item contract">
                        <a class="nav-link" href="#ctt_tab2" data-toggle="tab">Contract</a></li>
                    <li class="nav-item other">
                        <a class="nav-link" href="#ods_tab2" data-toggle="tab">Other Documents</a>
                    </li>
                </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="iif_tab2">
                        @foreach($company->companyStandards as $company_standard)
                            @php
                                $date_passed = [];
                            @endphp

                            @foreach($company_standard->companyStandardDocuments()->where('document_type', "initial_inquiry_form")->get() as $documents)
                                @continue(in_array($documents->date, $date_passed))
                                @php
                                    $date_passed[] = $documents->date;
                                @endphp

                                @php
                                    $media_docs = \Spatie\MediaLibrary\Models\Media::where('id',$documents->media_id)->get();
                                @endphp
                                @if(!empty($media_docs) && count($media_docs) > 0)
                                    <div class="col-md-12">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <td>{{ $documents->date->format('d/m/Y') }}</td>
                                                <th>Standard</th>
                                                <th>{{ $documents->companyStandard->standard->name }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--											{{ dd(Helper::getStandardDocuments($company_standard->id, $documents->date, 'initial_inquiry_form')) }}--}}
                                            @foreach($media_docs as $document)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td colspan="3">
                                                        <a href="{{  asset('storage/'.$document->id.'/'.$document->file_name) }}"
                                                           target="_blank"
                                                           title="{{ $document->file_name }}">{{ $document->file_name }}</a>
                                                    </td>
                                                    {{--															<a href="{{ $document->getDocument()->getFullUrl() }}" target="_blank">{{ $document->getDocument()->name }}</a>--}}
                                                    {{--														</td>--}}
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                    </div>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                    <div class="tab-pane" id="ppl_tab2">
                        @foreach($company->companyStandards as $company_standard)
                            <div class="col-md-12">
                                @php
                                    $date_passed = [];
                                @endphp
                                @foreach($company_standard->companyStandardDocuments()->where('document_type', "proposal")->get() as $documents)
                                    @continue(in_array($documents->date, $date_passed))
                                    @php
                                        $date_passed[] = $documents->date;
                                    @endphp
                                    @php
                                        $media_docs = \Spatie\MediaLibrary\Models\Media::where('id',$documents->media_id)->get();
                                    @endphp
                                    @if(!empty($media_docs) && count($media_docs) > 0)
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <td>{{ $documents->date->format('d/m/Y') }}</td>
                                                <th>Standard</th>
                                                <th>{{ $documents->companyStandard->standard->name }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($media_docs as $document)
                                                {{--											@foreach(Helper::getStandardDocuments($company_standard->id, $documents->date, 'proposal') as $document)--}}
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td colspan="3">
                                                        <a href="{{  asset('storage/'.$document->id.'/'.$document->file_name) }}"
                                                           target="_blank"
                                                           title="{{ $document->file_name }}">{{ $document->file_name }}</a>
                                                    </td>

                                                    {{--														<a href="{{ $document->getDocument()->getFullUrl() }}" target="_blank">{{ $document->getDocument()->name }}</a>--}}
                                                    {{--													</td>--}}
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="ctt_tab2">
                        @foreach($company->companyStandards as $company_standard)
                            <div class="col-md-12">
                                @php
                                    $date_passed = [];
                                @endphp
                                @foreach($company_standard->companyStandardDocuments()->where('document_type', "contract")->get() as $documents)
                                    @continue(in_array($documents->date, $date_passed))
                                    @php
                                        $date_passed[] = $documents->date;
                                    @endphp
                                    @php
                                        $media_docs = \Spatie\MediaLibrary\Models\Media::where('id',$documents->media_id)->get();
                                    @endphp
                                    @if(!empty($media_docs) && count($media_docs) > 0)
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <td>{{ $documents->date->format('d/m/Y') }}</td>
                                                <th>Standard</th>
                                                <th>{{ $documents->companyStandard->standard->name }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($media_docs as $document)
                                                {{--											@foreach(Helper::getStandardDocuments($company_standard->id, $documents->date, 'contract') as $document)--}}
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td colspan="3">
                                                        <a href="{{  asset('storage/'.$document->id.'/'.$document->file_name) }}"
                                                           target="_blank"
                                                           title="{{ $document->file_name }}">{{ $document->file_name }}</a>
                                                    </td>
                                                {{--														<a href="{{ $document->getDocument()->getFullUrl() }}" target="_blank">{{ $document->getDocument()->name }}</a>--}}
                                                {{--													</td>--}}
                                                {{--												</tr>--}}
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane" id="ods_tab2">
                        @foreach($company->companyStandards as $company_standard)
                            <div class="col-md-12">
                                @php
                                    $date_passed = [];
                                @endphp
                                @foreach($company_standard->companyStandardDocuments()->where('document_type', "other")->get() as $documents)
                                    @continue(in_array($documents->date, $date_passed))
                                    @php
                                        $date_passed[] = $documents->date;
                                    @endphp
                                    @php
                                        $media_docs = \Spatie\MediaLibrary\Models\Media::where('id',$documents->media_id)->get();
                                    @endphp
                                    @if(!empty($media_docs) && count($media_docs) > 0)
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <td>{{ $documents->date->format('d/m/Y') }}</td>
                                                <th>Standard</th>
                                                <th>{{ $documents->companyStandard->standard->name }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($media_docs as $document)
                                                {{--											@foreach(Helper::getStandardDocuments($company_standard->id, $documents->date, 'other') as $document)--}}
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td colspan="3">
                                                        <a href="{{  asset('storage/'.$document->id.'/'.$document->file_name) }}"
                                                           target="_blank"
                                                           title="{{ $document->file_name }}">{{ $document->file_name }}</a>
                                                    </td>
                                                    {{--														<a href="{{ $document->getDocument()->getFullUrl() }}" target="_blank">{{ $document->getDocument()->name }}</a>--}}
                                                    {{--													</td>--}}
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>