<div class="card collapsed-card cardMargin uploaded_documents_card">
    <img src="{{ asset('img/closeIcon.png') }}" alt="Remove" class="iconClose remove_uploaded_documents"
         data-standard-document-ids="{{ json_encode($documents_saved) }}" style="cursor: pointer;">
    <div class="card-header cardHead">

        <h3 class="card-title">{{ $request->date }} |
            @foreach($documents as $document)
                {{ $loop->iteration }}. <a
                        href="{{ $document->getFirstMediaUrl() }}">{{ $document->getFirstMedia()->name }}</a>
                @if($loop->index >= 0 && $loop->index < count($documents)-1)
                    ,
                @endif
            @endforeach
        </h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0" style="display: none;">
        <ul class="products-list product-list-in-card pl-2 pr-2">
            <li class="item">
                <div class="col-md-12 floting text-left">
                    <label>Standards</label>
                </div>
                <div class="col-md-12 text-left">
                    {{ Helper::getStandardNumberById($request->standards) }}
                </div>
                <div class="col-md-12">
                    <label>Files</label>
                </div>
                <div class="col-md-12">
                    @foreach($documents as $document)

                        <p>{{ $loop->iteration }} <a
                                    href="{{  asset('storage/'.$document->id.'/'.$document->file_name) }}"
                                    target="_blank"
                                    title="{{ $document->file_name }}">{{ $document->file_name }}</a>
                        </p>
                        {{--              			<p>{{ $loop->iteration }}. <a href="{{ $document->getFirstMediaUrl() }}">{{ $document->getFirstMedia()->name }}</a></p>--}}
                    @endforeach
                </div>
                <div class="clearfix"></div>
            </li>
            <!-- /.item -->
        </ul>
    </div>
</div>