<div class="tab-pane standard-tab {{ $loop->index == 0 ? 'active' : '' }}"
     id="companyStandard_{{ $companyStandard->id }}">
    <div class="sub_tabs">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills p-2 ">
                    <li class="nav-item"><a class="nav-link "
                                            href="#information_{{ $companyStandard->id }}"
                                            data-toggle="tab">Details</a>
                    </li>
                    <li class="nav-item"><a class="nav-link active"
                                            href="#postAudit_{{ $companyStandard->id }}"
                                            data-toggle="tab">Post Audit</a>
                    </li>
                </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane "
                         id="information_{{ $companyStandard->id }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center stnd_lines">
                                    <label class="text-uppercase emp_lbl">{{ $companyStandard->standard->name }}</label>
                                </div>
                            </div>
                            <div class="col-md-12 mrgn text-right">
                                <div class="col-md-6 floting ">
                                    <label>New Client or Transfer From Other
                                        CB &nbsp;&nbsp; |</label>
                                </div>
                                <div class="col-md-6 floting text-left">
                                    <p class="results_ans">{{ $companyStandard->client_type == 'transfered' ? 'Yes' : 'No' }}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            @if($companyStandard->client_type == 'transfered')
                                <div class="col-md-12">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <strong>
                                                    CB Name
                                                </strong><br>{{ $companyStandard->old_cb_name }}
                                            </td>
                                            <td>
                                                <strong>
                                                    Certification Issue Date
                                                </strong><br>{{ $companyStandard->old_cb_certificate_issue_date->format('d/m/Y') }}
                                            </td>
                                            <td>
                                                <strong>
                                                    Certification Expiry
                                                </strong><br>{{ $companyStandard->old_cb_certificate_expiry_date->format('d/m/Y') }}
                                            </td>
                                            <td>
                                                <strong>
                                                    Previous Surveillance
                                                    Frequency
                                                </strong><br>{{ ucfirst($companyStandard->old_cb_surveillance_frequency) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>
                                                    Last Audit Activity
                                                </strong><br>{{ $companyStandard->oldCbAuditActivityStage->name }}
                                            </td>
                                            <td>
                                                <strong>
                                                    Last Audit Report
                                                </strong><br><a
                                                        href="{{!is_null($companyStandard->old_cb_last_audit_report_doc_media_id) ?  asset('uploads/company_audit_report/'.$companyStandard->old_cb_last_audit_report_doc_media_id) : '#'}}"
                                                        title="{{ !is_null($companyStandard->old_cb_last_audit_report_doc_media_id) ? $companyStandard->old_cb_last_audit_report_doc_media_id : '' }}"
                                                        target="_blank">{{ !is_null($companyStandard->old_cb_last_audit_report_doc_media_id) ? $companyStandard->old_cb_last_audit_report_doc_media_id : '-' }}</a>
                                            </td>
                                            <td>
                                                <strong>
                                                    Certificate Copy
                                                </strong><br><a
                                                        href="{{!is_null($companyStandard->old_cb_certificate_copy_doc_media_id) ?  asset('uploads/company_certificate_copy/'.$companyStandard->old_cb_certificate_copy_doc_media_id) : '#'}}"
                                                        title="{{ !is_null($companyStandard->old_cb_certificate_copy_doc_media_id) ? $companyStandard->old_cb_certificate_copy_doc_media_id : '' }}"
                                                        target="_blank">{{ !is_null($companyStandard->old_cb_certificate_copy_doc_media_id) ? $companyStandard->old_cb_certificate_copy_doc_media_id : '-' }}</a>
                                            </td>
                                            <td>
                                                <strong>
                                                    Other Documents
                                                </strong><br><a
                                                        href="{{!is_null($companyStandard->old_cb_others_doc_media_id) ?  asset('uploads/company_other_doc/'.$companyStandard->old_cb_others_doc_media_id) : '#'}}"
                                                        title="{{ !is_null($companyStandard->old_cb_others_doc_media_id) ? $companyStandard->old_cb_others_doc_media_id : '' }}"
                                                        target="_blank">{{ !is_null($companyStandard->old_cb_others_doc_media_id) ? $companyStandard->old_cb_others_doc_media_id : '-' }}</a>
                                            </td>
                                        </tr>
                                        @if(!empty($companyStandard->old_cb_audit_activity_dates))
                                            @foreach(collect(json_decode($companyStandard->old_cb_audit_activity_dates))->chunk(4) as $old_cb_audit_activities)
                                                <tr>
                                                    @foreach($old_cb_audit_activities as $stage_id => $old_cb_audit_activity_date)
                                                        <td>
                                                            <strong>
                                                                Last Audit
                                                                Activity
                                                                Date
                                                            </strong><br>
                                                            {{ $old_cb_audit_activity_date }}
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </div>
                            @endif

                            <div class="col-md-12">
                                <div class="popup_info">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h5>Additional Information
                                                <b>{{ $companyStandard->name }}</b>
                                            </h5>
                                        </div>
                                    </div>
                                    @foreach($companyStandard->standard->questions()->orderBy('sort')->get() as $question)
                                        <div class="col-md-12 mrgn">
                                            <div class="col-md-12">
                                                <label><strong>
                                                        {{--                                                                                                    Question--}}
                                                        {{--                                                                                                    ({{ $loop->iteration }}--}}
                                                        {{--                                                                                                    )--}}
                                                    </strong> {{ $question->question }}
                                                </label>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="results_ans">{{ ucfirst($companyStandard->companyStandardsAnswers()->where('standard_question_id', $question->id)->first()->answer ?? '-') }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-12 mrgn">
                                    <div class="col-md-3 floting">
                                        <label>Proposed Complexity</label>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <p class="results_ans">{{ ucfirst($companyStandard->proposed_complexity) }}</p>
                                    </div>
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Complexity
                                                Remarks</label>
                                            <p class="results_ans">{{ $companyStandard->general_remarks ?? ''   }}</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="text-center stnd_lines2">
                                <label class="text-uppercase emp_lbl">Contract
                                    Price / Standard
                                    @if(auth()->user()->user_type === 'operation_manager')
                                        <i style="">
                                            <a href="javascript:void(0)" class="edit_standard_prices"
                                               data-initial-certification-currency="{{ $companyStandard->initial_certification_currency }}"
                                               data-initial-certification-amount="{{ $companyStandard->initial_certification_amount }}"
                                               data-surveillance-amount="{{ $companyStandard->surveillance_amount }}"
                                               data-re_audit-amount="{{ $companyStandard->re_audit_amount }}"
                                               data-client-type="{{$companyStandard->client_type}}"
                                               data-company-standard-id="{{$companyStandard->id}}"
                                               id="edit_standard_prices" style="font-size: 22px !important;">
                                                <small>(Edit Price)</small>
                                            </a>
                                        </i>
                                    @endif
                                </label>
                            </div>
                            <div class="mrgn"></div>
                            <div class="col-md-4 floting">
                                <div class="col-md-12 text-center">
                                    <label><strong>Initial
                                            Certification</strong></label>
                                </div>
                                <div class="col-md-12 floting">
                                    <div class="col-md-6 floting">
                                        <label>Unit</label>
                                        <p class="results_ans">{{ $companyStandard->initial_certification_currency }}</p>
                                    </div>
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Amount</label>
                                            <p class="results_ans">{{ $companyStandard->initial_certification_amount }}</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="col-md-12 text-center">
                                    <label><strong>Surveillance</strong></label>
                                </div>
                                <div class="col-md-12 floting">
                                    <div class="col-md-6 floting">
                                        <label>Unit</label>
                                        <p class="results_ans">{{ $companyStandard->initial_certification_currency }}</p>
                                    </div>
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Amount</label>
                                            <p class="results_ans">{{ $companyStandard->surveillance_amount }}</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="col-md-12 text-center">
                                    <label><strong>Re-Audit</strong></label>
                                </div>
                                <div class="col-md-12 floting">
                                    <div class="col-md-6 floting">
                                        <label>Unit</label>
                                        <p class="results_ans">{{ $companyStandard->initial_certification_currency }}</p>
                                    </div>
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Amount</label>
                                            <p class="results_ans">{{ $companyStandard->re_audit_amount }}</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6 floting">
                                <div class="form-group">
                                    <label>Surveillance frequency</label>
                                    <p class="results_ans">{{ ucfirst($companyStandard->surveillance_frequency) }}</p>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="text-center stnd_lines2">
                                <label class="text-uppercase emp_lbl">IAF
                                    Codes</label>
                            </div>
                            <div class="mrgn"></div>
                            <div class="col-md-12 floting">
                                <table id="FormalEducationTableCodes"
                                       cellspacing="20" cellpadding="0"
                                       border="0"
                                       class="table table-striped table-bordered table-bordered2 FormalEducationTableCodes{{$companyStandard->id}}">
                                    <thead>
                                    <tr>
                                        <th style="color: #fff">S.No</th>
                                        <th style="color: #fff">
                                            Accreditation
                                        </th>
                                        <th style="color: #fff">IAF Code
                                        </th>
                                        <th style="color: #fff">IAS Code
                                        </th>
                                        <th style="color: #fff" width="50%"
                                            class="action-column">IAS
                                            Description
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    @foreach($companyStandard->companyStandardCodes as $companyStandardCode)
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{!! $companyStandardCode->accreditation->name !!}</td>
                                            <td>{{$companyStandardCode->iaf->code}}</td>
                                            <td>{{$companyStandardCode->ias->code}}</td>
                                            <td>{{$companyStandardCode->ias->name}}</td>
                                        </tr>
                                    @endforeach


                                    </tbody>

                                </table>


                            </div>

                            <div class="clearfix"></div>

                        </div>
                        @if($companyStandard->standard->standards_family_id == 23)
                            <div class="col-md-12">
                                <div class="text-center stnd_lines2">
                                    <label class="text-uppercase emp_lbl">Food
                                        Codes</label>
                                </div>
                                <div class="mrgn"></div>

                                <div class="col-md-12 floting">
                                    <table id="FormalEducationTable"
                                           cellspacing="20" cellpadding="0"
                                           border="0"
                                           class="table table-striped table-bordered table-bordered2">
                                        <thead>
                                        <tr>
                                            <th style="color: #fff">S.No
                                            </th>
                                            <th style="color: #fff">
                                                Accreditation
                                            </th>
                                            <th style="color: #fff">Food
                                                Code
                                            </th>
                                            <th style="color: #fff">Food
                                                Sub-Code
                                            </th>
                                            <th style="color: #fff"
                                                width="50%"
                                                class="action-column">
                                                Description of Sub-Code
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        @foreach($companyStandard->companyStandardFoodCodes  as $key => $companyStandardFoodCode)
                                            <tr>
                                                <td style="">{{$key + 1}}</td>
                                                <td>{!!$companyStandardFoodCode->accreditation->name !!}</td>
                                                <td>{{$companyStandardFoodCode->foodcategory->code}}</td>
                                                <td>{{$companyStandardFoodCode->foodsubcategory->code}}</td>
                                                <td>{{$companyStandardFoodCode->foodsubcategory->name}}</td>
                                            </tr>
                                        @endforeach


                                        </tbody>

                                    </table>
                                </div>


                                <div class="clearfix"></div>

                            </div>
                        @endif
                        @if($companyStandard->standard->standards_family_id == 20)
                            <div class="col-md-12">
                                <div class="text-center stnd_lines2">
                                    <label class="text-uppercase emp_lbl">Energy
                                        Codes</label>
                                </div>
                                <div class="mrgn"></div>

                                <div class="col-md-12 floting">
                                    <table id="FormalEducationTable"
                                           cellspacing="20" cellpadding="0"
                                           border="0"
                                           class="table table-striped table-bordered table-bordered2">
                                        <thead>
                                        <tr>
                                            <th style="color: #fff">S.No
                                            </th>
                                            <th style="color: #fff">
                                                Accreditation
                                            </th>
                                            <th style="color: #fff">Code
                                            </th>
                                            <th style="color: #fff"
                                                width="50%"
                                                class="action-column">
                                                Description
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        @foreach($companyStandard->companyStandardEnergyCodes as $key => $companyStandardEnergyCode)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$companyStandardEnergyCode->accreditation->name}}</td>
                                                <td>{{$companyStandardEnergyCode->energycode->code}}</td>
                                                <td>{{$companyStandardEnergyCode->energycode->name}}</td>
                                            </tr>
                                        @endforeach


                                        </tbody>

                                    </table>
                                </div>


                                <div class="clearfix"></div>

                            </div>
                        @endif
                        @if(!is_null($companyStandard->version_change))
                            <div class="col-md-12">
                                <div class="text-center stnd_lines2">
                                    <label class="text-uppercase emp_lbl">Transfer Case History</label>
                                </div>
                                <div class="mrgn"></div>
                                <div class="col-md-12 floting">
                                    <table id="FormalEducationTableCodes"
                                           cellspacing="20" cellpadding="0"
                                           border="0"
                                           class="table table-striped table-bordered table-bordered2">
                                        <thead>
                                        <tr>
                                            <th style="color: #fff">Audit Type</th>
                                            <th style="color: #fff">Old Standard Version</th>
                                            <th style="color: #fff">Upgraded to next Version On</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!is_null($companyStandard->version_change))
                                            @php
                                                $versionChangeHistories =  json_decode($companyStandard->version_change,true);
                                            @endphp
                                            @foreach($versionChangeHistories as $history)
                                                @php
                                                    $auditTypeHistory = \App\AJStandardStage::whereId($history['old_aj_standard_stage_id'])->first(['audit_type']);
                                                    $ajStandardHistory  = \App\AJStandard::whereId($history['old_aj_standard_id'])->with('standard:id,name')->first(['id','standard_id']);


                                                @endphp
                                                <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                    <td>{{!is_null($auditTypeHistory) ? $auditTypeHistory->getAuditType() : '--'}}</td>
                                                    <td>{{!is_null($ajStandardHistory) ? $ajStandardHistory->standard->name : '--'}}</td>
                                                    <td>{{$history['change_date'] ? date('d-m-Y',strtotime($history['change_date'])) : '--'}}</td>
                                                </tr>
                                            @endforeach

                                        @else
                                            <tr class='notfound'>
                                                <td colspan='3'>No record found</td>
                                            </tr>
                                        @endif
                                        </tbody>

                                    </table>


                                </div>

                                <div class="clearfix"></div>

                            </div>
                        @endif
                        <div class="col-md-12">
                            <div class="text-center stnd_lines2">
                                <label class="text-uppercase emp_lbl">Documents</label>
                            </div>
                            <div class="mrgn"></div>
                            <table class="table">

                                @if(!empty($standard_documents) && count($standard_documents) >0)
                                    @foreach ($standard_documents as $inqueryDocuemts)
                                        @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                            @foreach ($inqueryDocuemts as $index => $data)
                                                @if($data->company_standard_id === $companyStandard->id)
                                                    @php
                                                        $proposal_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                    @endphp
                                                    <tr id="#media_docs{{$proposal_media->id}}">

                                                        <td>
                                                            <input type="hidden"
                                                                   name="company_media_id"
                                                                   value="{{ $proposal_media->id}}"
                                                                   id="company_media_id"/>
                                                        </td>
                                                        <td>{{ucwords(str_replace('_',' ',$data->document_type))}}</td>
                                                        <td>{{ $data->date->format('d/m/Y') }}</td>
                                                        <td>{{ $data->companyStandard->standard->name}}</td>
                                                        <td>
                                                            <a href="{{  asset('storage/'.$proposal_media->id.'/'.$proposal_media->file_name) }}"
                                                               target="_blank"
                                                               title="{{ $proposal_media->file_name }}">{{ $proposal_media->file_name }}</a>
                                                        </td>
                                                        <td>
                                                            @can('delete_company_documents')
                                                                <a href="javascript:void(0)"
                                                                   onclick="removeMedia({{ $proposal_media->id}})"
                                                                   class="btn btn-sm btn-danger fontIconMedia"><i
                                                                            class="fa fa-remove"></i>
                                                                </a>
                                                            @endcan

                                                        </td>

                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No Files Available
                                        </td>
                                    </tr>
                                @endif

                            </table>
                        </div>


                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane active"
                         id="postAudit_{{ $companyStandard->id }}">
                        <div class="col-md-12 no_padding margin_div">
                            <div class="row auditJustificationTable mrgn">
                                <div class="col-md-3">
                                    <p>Standard Status:
                                        <span id="new_status">{{ (isset($companyStandard->certificates)) ? str_replace('_',' ',ucfirst($companyStandard->certificates->certificate_status)) : 'Not Certified' }}</span>&nbsp;
                                        @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                        @else
                                            <a href="#"
                                               class=" {{ (auth()->user()->user_type == 'scheme_manager') ? '' :'disabledDiv' }}"
                                               onclick="certificate('{{$companyStandard->id}}')"><i
                                                        class="fa fa-pencil"></i></a>
                                        @endif
                                    </p>
                                </div>

                                <div class="col-md-3">
                                    <p>Status Date: <span
                                                id="date_certificate">{{ (isset($companyStandard->certificates)) ? date('d-m-Y',strtotime(($companyStandard->certificates->certificate_date))) : '' }}</span>
                                    </p>
                                </div>

                                <div class="col-md-3">
                                    <p>Print letter &nbsp;

                                        @if (isset($companyStandard->certificates))
                                            @if($companyStandard->certificates->is_print_letter === 1)
                                                @if ($companyStandard->certificates->certificate_status == "withdrawn" )
                                                    <a href="{{ route('withdrawCertificate',[  $companyStandard->id]) }}"
                                                       target="_blank"
                                                       class="printAj"><i
                                                                class="fa fa-print"></i></a>
                                                @elseif($companyStandard->certificates->certificate_status == "suspended")
                                                    <a href="{{ route('suspensionCertificate',[  $companyStandard->id]) }}"
                                                       target="_blank"
                                                       class="printAj"><i
                                                                class="fa fa-print"></i></a>
                                                @endif
                                            @endif
                                        @endif


                                    </p>

                                </div>

                                <div class="col-md-3">
                                    <p>Transfer Case:
                                        {{--                                                                                    <span>{{ $companyStandard->client_type == 'transfered' ? 'Yes' : 'No' }}</span>--}}
                                        <span>{{ $companyStandard->is_transfer_standard == true ? 'Yes' : 'No' }}</span>
                                        @if($companyStandard->is_transfer_standard == true)
                                            &nbsp; Year:
                                            <span>{{ !is_null($companyStandard->transfer_year) ? $companyStandard->transfer_year : '' }}</span>

                                        @endif
                                    </p>

                                </div>


                                <div class="clearfix"></div>
                            </div>

                            @php
                                $latest_dates = \App\SchemeInfoPostAudit::whereHas('postAudit',function($q) use ($companyStandard){
$q->where('company_id',$companyStandard->company_id)->where('standard_id',$companyStandard->standard_id);
})->latest()->first();


                            @endphp
                            <div class="row auditJustificationTable mrgn">
                                <div class="col-md-3">
                                    <p>Original I. Date:
                                        @if(!is_null($latest_dates))
                                            @php
                                                $postAuditAjStandardStageId = \App\PostAudit::whereId($latest_dates->post_audit_id)->first();
$stageType = \App\AJStandardStage::whereId($postAuditAjStandardStageId->aj_standard_stage_id)->first();
                                            @endphp
                                            {{--                                            @if($stageType->audit_type !='stage_1')--}}
                                            <span>{{ (isset($latest_dates)) ? date('d-m-Y',strtotime(($latest_dates->original_issue_date))) : '' }}</span>
                                            @if(!is_null($latest_dates))
                                                @if(auth()->user()->user_type == 'scheme_manager')
                                                    <a href="#"
                                                       class=""
                                                       onclick="updateOriginalIssueDate('{{$latest_dates->id}}')"><i
                                                                class="fa fa-pencil"></i></a>
                                                @endif
                                            @endif
                                            {{--                                            @endif--}}
                                        @endif


                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>Expiry Date:
                                        <span>{{ (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) ? date('d-m-Y',strtotime(($latest_dates->new_expiry_date))) : '' }}</span>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6 floting no_padding">
                                        <p for="directory_updated">Directory
                                            Updated: </p>
                                    </div>

                                    <div class="col-md-6 floting {{ (auth()->user()->user_type == 'scheme_manager' || $companyStandard->status == 'disabled' || $companyStandard->status == 'pending') ? '' :'disabledDiv' }}">
                                        <div class="col-md-6 floting">

                                            <input type="radio"
                                                   name="directory_updated[{{$companyStandard->id}}]"
                                                   id="directory_updated_yes{{$companyStandard->id}}"
                                                   value="yes"
                                                   onclick="directoryUpdate('{{$companyStandard->id}}','yes')"
                                                   data-company-standard-id="{{$companyStandard->id}}" {{ (isset($companyStandard) && $companyStandard->directory_updated == 'yes') ? 'checked' : '' }}>
                                            Yes
                                        </div>
                                        <div class="col-md-6 floting">
                                            <input type="radio"
                                                   name="directory_updated[{{$companyStandard->id}}]"
                                                   id="directory_updated_no{{$companyStandard->id}}"
                                                   value="no"
                                                   onclick="directoryUpdate('{{$companyStandard->id}}','no')"
                                                   data-company-standard-id="{{$companyStandard->id}}" {{ (isset($companyStandard) && $companyStandard->directory_updated == 'no') ? 'checked' : '' }}>
                                            &nbsp;No
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-6 mrgn">
                                    <p>Last Certificate I. Date:
                                        <span>{{(isset($companyStandard) && $companyStandard->last_certificate_issue_date) ?  date("d-m-Y", strtotime($companyStandard->last_certificate_issue_date)):'' }}</span>

                                    </p>
                                </div>
                                @if((auth()->user()->user_type == 'scheme_manager'))
                                    <div class="col-md-6 mrgn">
                                        <div class="col-md-6 floting no_padding">
                                            <p for="directory_updated">Add
                                                Grade: </p>
                                        </div>


                                        <div class="col-md-6 floting">
                                            <div class="col-md-12 floting">

                                                <select
                                                        name="grade[{{$companyStandard->id}}]"
                                                        id="grade{{$companyStandard->id}}"
                                                        class="gradeChange form-control"
                                                        data-company-standard-id="{{$companyStandard->id}}">
                                                    <option value="">Select
                                                        Grade
                                                    </option>
                                                    <option value="N/A" {{ $companyStandard->grade === 'N/A' ? 'selected' : ''}}>
                                                        N/A
                                                    </option>
                                                    <option value="A" {{ $companyStandard->grade === 'A' ? 'selected' : ''}}>
                                                        A
                                                    </option>
                                                    <option value="B" {{ $companyStandard->grade === 'B' ? 'selected' : ''}}>
                                                        B
                                                    </option>
                                                    <option value="C" {{ $companyStandard->grade === 'C' ? 'selected' : ''}}>
                                                        C
                                                    </option>
                                                    <option value="D" {{ $companyStandard->grade === 'D' ? 'selected' : ''}}>
                                                        D
                                                    </option>
                                                </select>

                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endif
                                <br>
                                <br>
                                <div class="col-md-12">
                                    @if(auth()->user()->user_type !== 'management')
                                        <form id="company-standard-remarks-store"
                                              action="#" method="post">
                                            @csrf
                                            <input type="hidden"
                                                   name="company_standard_id"
                                                   id="company_standard_id"
                                                   value="{{$companyStandard->id}}">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="planning_remark">Planning
                                                        Remarks</label>


                                                    <textarea type="text"
                                                              name="planning_remark"
                                                              rows="2"
                                                              id="planning_remark{{ $companyStandard->id }}"
                                                              class="form-control">{{$companyStandard->main_remarks}}</textarea>
                                                </div>
                                            </div>
                                            @if(!is_null($companyStandard->SOA) && strtolower(str_replace(' ','_',$companyStandard->standard->standardFamily->name)) == 'information_security')
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="soa_version">SOA
                                                            Version</label>
                                                        <input type="text"
                                                               value="{{$companyStandard->SOA}}"
                                                               readonly
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            @endif

                                            @if(auth()->user()->user_type == 'scheme_manager')
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="scheme_manager_remark">Scheme
                                                            Manager
                                                            Remarks</label>
                                                        <textarea type="text"
                                                                  name="scheme_manager_remark"
                                                                  rows="2"
                                                                  id="scheme_manager_remark{{ $companyStandard->id }}"
                                                                  class="form-control">{{$companyStandard->scheme_manager_remarks}}</textarea>

                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-2 text-right">
                                                <button type="button"
                                                        class="btn btn-block btn-success btn_save"
                                                        data-company-standard-id="{{ $companyStandard->id }}"
                                                        onclick="companyStandardRemarksSubmit(event,{{$companyStandard->id}})">
                                                    Save
                                                </button>
                                            </div>
                                        </form>
                                    @endif

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 text-right">
                                    {{--                                                                                <a href="#"--}}
                                    {{--                                                                                   class="btn btn-info"--}}
                                    {{--                                                                                   onclick="companyStandardRemarks('{{ $companyStandard->id }}')">Planning--}}
                                    {{--                                                                                    Remarks</a>--}}
                                    @if(auth()->user()->roles[0]->name === "scheme manager")

                                        @if($company->is_send_to_ias === 0)
                                            <form action="{{ route('company.send.to.ias') }}"
                                                  method="POST">
                                                @csrf
                                                <input type="hidden"
                                                       name="type"
                                                       value="reset">
                                                <input type="hidden"
                                                       name="company_id"
                                                       value="{{$company->id}}">
                                                <button class="btn btn-success btn_save"
                                                        type="submit"
                                                        id="#manDay">Reset
                                                </button>
                                            </form>
                                            <form action="{{ route('company.send.to.ias') }}"
                                                  method="POST">
                                                @csrf
                                                <input type="hidden"
                                                       name="type"
                                                       value="send">
                                                <input type="hidden"
                                                       name="company_id"
                                                       value="{{$company->id}}">
                                                <button class="btn btn-success btn_save"
                                                        type="submit"
                                                        id="#manDay">Send To
                                                    IAS
                                                </button>
                                            </form>
                                        @endif
                                        <button class="btn btn-success btn_save"
                                                data-toggle="modal"
                                                data-target="#manDay_{{ $companyStandard->id }}"
                                                id="#manDay">View MDs
                                        </button>

                                    @endif

                                    @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                    @else
                                        @if(auth()->user()->user_type !== 'management')
                                            <a href="{{ route('justification.special', [$company->id, $companyStandard->standard->name]) }}"
                                               class="btn btn-primary tblBtn"
                                               style="color:#fff !important">
                                                Add Special AJ
                                            </a>
                                        @endif
                                    @endif

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="scroll_tbl lengthScrollTable">
                                <table cellspacing="20" cellpadding="0"
                                       border="0"
                                       class="table table-striped table-bordered table-bordered2 mycompanytbl mycompanytblCustom">
                                    <tbody>
                                    <tr class="tableHeader tableHeaderOrng">
                                        <th colspan="4">Audit Justification
                                            Task
                                        </th>
                                        <th colspan="3">Audit Task</th>
                                        <th>CARs</th>
                                        <th>Certificate Task</th>
                                    </tr>
                                    <tr class="tableSubHeader">
                                        <td>Title</td>
                                        <td>Due Date</td>
                                        <td>AJ</td>
                                        <td>AJ Status</td>

                                        <td>Acutal Audit Date</td>
                                        <td>File Upload</td>
                                        <td>Pack Status</td>

                                        <td>CAR Status</td>

                                        <td>Print Status</td>

                                    </tr>

                                    @if($company->auditJustifications->isNotEmpty())

                                        @foreach($company->auditJustifications as $key=>$audit_justification)
                                            @php
                                                $standard_id_is =$audit_justification->ajStandards()->where('standard_id', $companyStandard->standard_id)->get();
foreach($standard_id_is as $aj_standard_stage_id){
$standard_stage_man_days = App\AJStandardStage::where('aj_standard_id',$aj_standard_stage_id->id)->get();
}


                                            @endphp
                                            @if($standard_id_is->count() >0)

                                                @if($companyStandard->client_type === 'transfered')
                                                    @foreach($audit_justification->ajStandards()->where('standard_id', $companyStandard->standard_id)->get() as $index=>$aj_standard)
                                                        @foreach($aj_standard->ajStandardStages as  $index=>$standard_stage)

                                                            <tr class="tableNoBg">
                                                                <td>{{ $standard_stage->getAuditType() }}
                                                                    @if(auth()->user()->user_type == 'scheme_manager')
                                                                        @if(preg_match("/Verification/", $standard_stage->getAuditType()))
                                                                        @else
                                                                            <br>
                                                                            <span><input
                                                                                        type="checkbox"
                                                                                        id="ajApprovedStatus"
                                                                                        data-id="{{$standard_stage->id}}"
                                                                                        value="{{$standard_stage->aj_approved_status == false ? 0 : 1}}"
                                                                                        class="form-control" {{$standard_stage->aj_approved_status == false ? '': 'checked'}}></span>

                                                                        @endif
                                                                    @endif
                                                                </td>

                                                                <td>{{  $standard_stage->excepted_date != null && $standard_stage->audit_type !== 'stage_1' && $standard_stage->audit_type !== 'stage_2'  ? date('d-m-Y',strtotime($standard_stage->excepted_date)) : ''  }}</td>
                                                                @if(preg_match("/Verification/", $standard_stage->getAuditType() ) || preg_match("/Scope-Extension/", $standard_stage->getAuditType()) || preg_match("/Special-Transition/", $standard_stage->getAuditType()))
                                                                    <td class="">

                                                                        @if($standard_stage->status === 'Not Applied')
                                                                            @can('create_aj')
                                                                                <a href="{{ route('justification.transfer.add',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                   class="btn btn-primary tblBtn">
                                                                                    Create
                                                                                    New
                                                                                </a>
                                                                            @endcan
                                                                        @else
                                                                            @can('show_aj')

                                                                                @if(preg_match("/Verification/", $standard_stage->getAuditType()) || preg_match("/Scope-Extension/", $standard_stage->getAuditType()) || preg_match("/Special-Transition/", $standard_stage->getAuditType()))
                                                                                    <a class="fontSize"
                                                                                       href="{{ route('justification.show.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                       title="View"><i
                                                                                                class="fa fa-eye"></i>
                                                                                    </a>
                                                                                @endif
                                                                            @endcan


                                                                            @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                            @else
                                                                                @can('edit_aj')
                                                                                    @if(preg_match("/Verification/", $standard_stage->getAuditType()) || preg_match("/Scope-Extension/", $standard_stage->getAuditType()) || preg_match("/Special-Transition/", $standard_stage->getAuditType()))

                                                                                        @if($standard_stage->status === 'applied' || $standard_stage->status === 'created')

                                                                                            @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                <a href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                   class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                                   title="Edit"><i
                                                                                                            class="fa fa-pencil"></i>
                                                                                                </a>
                                                                                            @else
                                                                                                <a class="fontSize"
                                                                                                   href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"

                                                                                                   title="Edit"><i
                                                                                                            class="fa fa-pencil"></i>
                                                                                                </a>
                                                                                            @endif

                                                                                        @else
                                                                                            @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                <a href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                   class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                                   title="Rewise">
                                                                                                    <i class="fa fa-recycle"></i>
                                                                                                </a>
                                                                                            @else
                                                                                                <a class="fontSize"
                                                                                                   href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                   title="Rewise">
                                                                                                    <i class="fa fa-recycle"></i>
                                                                                                </a>
                                                                                            @endif

                                                                                        @endif
                                                                                    @endif
                                                                                @endcan

                                                                                @can('delete_aj')
                                                                                    @if(preg_match("/Verification/", $standard_stage->getAuditType()) || preg_match("/Scope-Extension/", $standard_stage->getAuditType()) ||preg_match("/Special-Transition/", $standard_stage->getAuditType()))

                                                                                        <a class="fontSize"
                                                                                           onclick="showConfirmForDelete('{{ route('justification.delete.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}')"
                                                                                           href="javascript:void(0)"

                                                                                           title="Delete"><i
                                                                                                    class="fa fa-close"></i>
                                                                                        </a>

                                                                                    @endif
                                                                                @endcan
                                                                            @endif
                                                                        @endif

                                                                    </td>
                                                                @else
                                                                    @php
                                                                        $disable = '';
                                                                    @endphp
                                                                    @if($index > 0)

                                                                        @if($aj_standard->ajStandardStages[$index -1]->status === 'Approved' && preg_match('/[+]/', $aj_standard->ajStandardStages[$index -1]->audit_type) && strpos($aj_standard->ajStandardStages[$index -1]->ajStandardStages[$index -1]->audit_type, 'Transfer') !== false)
                                                                            @php
                                                                                $disable = '';
                                                                            @endphp
                                                                        @else
                                                                            @if($aj_standard->ajStandardStages[$index -1]->status === 'approved'  && !is_null($aj_standard->ajStandardStages[$index -1]->postAudit) && isset($aj_standard->ajStandardStages[$index -1]->postAudit) && $aj_standard->ajStandardStages[$index -1]->postAudit->status == 'approved' && isset($aj_standard->ajStandardStages[$index -1]->postAudit->postAuditSchemeInfo) && !is_null($aj_standard->ajStandardStages[$index -1]->postAudit->postAuditSchemeInfo))
                                                                                @php
                                                                                    $disable = '';
                                                                                @endphp
                                                                            @else
                                                                                @php
                                                                                    $disable = 'disabledDiv';
                                                                                @endphp
                                                                            @endif
                                                                        @endif
                                                                        {{--                                                                                                                        {{ ($index > 0) ? () ? 'disabledDiv' : '' :''}}--}}
                                                                    @endif


                                                                    <td class="">
                                                                        @if($standard_stage->status === 'Not Applied')
                                                                            @can('create_aj')
                                                                                <a href="{{ route('justification.transfer.add',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                   class="btn btn-primary tblBtn @if($disable == 'disabledDiv') btnColorDanger @endif {{ $disable }}">
                                                                                    Create
                                                                                    New
                                                                                </a>
                                                                            @endcan
                                                                        @else
                                                                            @if(preg_match('/[+]/', $standard_stage->getAuditType()) && strpos($standard_stage->getAuditType(), 'Transfer') !== false)
                                                                                <p>
                                                                                    ---</p>
                                                                            @else
                                                                                @can('show_aj')

                                                                                    <a class="fontSize"
                                                                                       href="{{ route('justification.transfer.show',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                       title="View"><i
                                                                                                class="fa fa-eye"></i>
                                                                                    </a>

                                                                                @endcan
                                                                                @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                                @else
                                                                                    @can('edit_aj')
                                                                                        @if(auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager')
                                                                                            @if(isset($standard_stage->postAudit->actual_audit_date))
                                                                                            @else
                                                                                                @if($standard_stage->status === 'applied' || $standard_stage->status === 'created')

                                                                                                    @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                        <a href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                           class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                                           title="Rewise">
                                                                                                            <i class="fa fa-pencil"></i>
                                                                                                        </a>
                                                                                                    @else
                                                                                                        <a class="fontSize"
                                                                                                           href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                           title="Edit"><i
                                                                                                                    class="fa fa-pencil"></i>
                                                                                                        </a>
                                                                                                    @endif

                                                                                                @else

                                                                                                    @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                        <a href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                           class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                                           title="Rewise">
                                                                                                            <i class="fa fa-recycle"></i>
                                                                                                        </a>
                                                                                                    @else
                                                                                                        <a class="fontSize"
                                                                                                           href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                           title="Rewise">
                                                                                                            <i class="fa fa-recycle"></i>
                                                                                                        </a>
                                                                                                    @endif

                                                                                                @endif
                                                                                            @endif
                                                                                        @else
                                                                                            @if($standard_stage->status === 'applied' || $standard_stage->status === 'created')

                                                                                                @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                    <a href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                                       title="Rewise">
                                                                                                        <i class="fa fa-recycle"></i>
                                                                                                    </a>
                                                                                                @else
                                                                                                    <a class="fontSize"
                                                                                                       href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       title="Edit"><i
                                                                                                                class="fa fa-pencil"></i>
                                                                                                    </a>
                                                                                                @endif

                                                                                            @else
                                                                                                @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                    <a href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                                       title="Rewise">
                                                                                                        <i class="fa fa-recycle"></i>
                                                                                                    </a>
                                                                                                @else
                                                                                                    <a class="fontSize"
                                                                                                       href="{{ route('justification.transfer.edit',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       title="Rewise">
                                                                                                        <i class="fa fa-recycle"></i>
                                                                                                    </a>
                                                                                                @endif

                                                                                            @endif
                                                                                        @endif
                                                                                    @endcan
                                                                                    @can('delete_aj')
                                                                                        <a class="fontSize"
                                                                                           onclick="showConfirmForDelete('{{ route('justification.transfer.delete',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}')"
                                                                                           href="javascript:void(0)"

                                                                                           title="Delete"><i
                                                                                                    class="fa fa-close"></i>
                                                                                        </a>
                                                                                    @endcan
                                                                                @endif
                                                                            @endif
                                                                        @endif

                                                                    </td>

                                                                @endif


                                                                <td>

                                                                    @if(preg_match('/Transfer/', $standard_stage->getAuditType()))
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                    @elseif($standard_stage->status == 'approved')
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                        <br>
                                                                        {{ date('d-m-Y',strtotime($standard_stage->approval_date)) }}
                                                                    @elseif($standard_stage->status == 'rejected')
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                        <br>
                                                                        {{ date('d-m-Y',strtotime($standard_stage->updated_at)) }}
                                                                    @else
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                    @endif

                                                                </td>
                                                                <td>{{ isset($standard_stage->postAudit->actual_audit_date) ? date("d-m-Y", strtotime($standard_stage->postAudit->actual_audit_date)) : '---' }}
                                                                    - {{ isset($standard_stage->postAudit->actual_audit_date_to) ? date("d-m-Y", strtotime($standard_stage->postAudit->actual_audit_date_to)) : '---' }}</td>
                                                                <td>

                                                                    @if($standard_stage->status == 'approved')

                                                                        @if($standard_stage->audit_type === 'stage_2_verification' || $standard_stage->audit_type === 'surveillance_1_verification' || $standard_stage->audit_type === 'surveillance_2_verification' || $standard_stage->audit_type === 'surveillance_3_verification' || $standard_stage->audit_type === 'surveillance_4_verification' || $standard_stage->audit_type === 'surveillance_5_verification' || $standard_stage->audit_type === 'reaudit_verification')
                                                                        @else
                                                                            @if(!is_null($standard_stage->postAudit))

                                                                                <a href="{{ route('post.audit.show', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                   title="View Date"
                                                                                   class="fontSize"><i
                                                                                            class="fa fa-eye"></i>
                                                                                </a>

                                                                                @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                                @else
                                                                                    @if($standard_stage->postAudit->status == 'rejected' || $standard_stage->postAudit->status == 'save')

                                                                                        <a href="{{ route('post.audit.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Edit Date"
                                                                                           class="fontSize"><i
                                                                                                    class="fa fa-pencil"></i>
                                                                                        </a>

                                                                                    @endif
                                                                                    @if (auth()->user()->user_type == 'scheme_manager')

                                                                                        <a
                                                                                                onclick="showConfirmForDelete('{{ route('post.audit.delete', [$company->id, $standard_stage->postAudit->id, $companyStandard->standard->id,$standard_stage->id]) }}')"
                                                                                                href="javascript:void(0)"
                                                                                                title="Delete Date"
                                                                                                class="fontSize"><i
                                                                                                    class="fa fa-close"></i>
                                                                                        </a>
                                                                                        <br>
                                                                                        <a href="{{ route('post.audit.scheme.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Audit Activity">Audit
                                                                                            Activity
                                                                                        </a>
                                                                                    @endif
                                                                                @endif

                                                                            @else
                                                                                @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                                @else
                                                                                    @if(auth()->user()->user_type != 'scheme_coordinator')
                                                                                        <a href="{{ route('post.audit.create', [$company->id,$standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           class="btn btn-success"
                                                                                           title="Add Date">Add
                                                                                            Date</a>
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(!is_null($standard_stage->postAudit))
                                                                        @if($standard_stage->postAudit->status == 'save')
                                                                            @php
                                                                                $status = '';
       $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$standard_stage->postAudit->id)->where('question_id',3)->first();
        $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
        if(is_null($file)){
            $status = 'no';
        }else{
            $status = '';
        }


                                                                            @endphp
                                                                            @if(!is_null($standard_stage->postAudit->actual_audit_date) && $status == 'no')
                                                                                Pack
                                                                                Not
                                                                                Uploaded
                                                                            @else
                                                                                Pack
                                                                                Not
                                                                                Sent
                                                                            @endif
                                                                        @elseif($standard_stage->postAudit->status == 'unapproved')
                                                                            Submitted
                                                                            <br>
                                                                            {{ date('d-m-Y',strtotime($standard_stage->postAudit->updated_at)) }}
                                                                            - @if($standard_stage->postAudit->sentToUser->user_type == 'scheme_manager')
                                                                                SM
                                                                            @elseif($standard_stage->postAudit->sentToUser->user_type == 'scheme_coordinator')
                                                                                SC
                                                                            @endif
                                                                        @elseif($standard_stage->postAudit->status == 'rejected')
                                                                            Rejected
                                                                            <br>
                                                                            {{ date('d-m-Y',strtotime($standard_stage->postAudit->updated_at)) }}
                                                                        @elseif($standard_stage->postAudit->status == 'resent')
                                                                            Resent
                                                                        @elseif($standard_stage->postAudit->status == 'accepted')
                                                                            Forwarded
                                                                            <br>
                                                                            {{ date('d-m-Y',strtotime($standard_stage->postAudit->updated_at)) }}
                                                                        @elseif($standard_stage->postAudit->status == 'approved')
                                                                            Approved
                                                                            <br>
                                                                            @if(!is_null($standard_stage->postAudit->postAuditSchemeInfo))
                                                                                {{ date('d-m-Y',strtotime($standard_stage->postAudit->postAuditSchemeInfo->approval_date)) }}
                                                                            @endif
                                                                        @endif
                                                                    @else
                                                                    @endif


                                                                </td>
                                                                <td>
                                                                    @if(!is_null($standard_stage->postAudit))
                                                                        @php
                                                                            $postAuditQuestionOne = \App\PostAuditQueestion::where('post_audit_id',$standard_stage->postAudit->id)->where('question_id',1)->first();
   $postAuditQuestionTwo = \App\PostAuditQueestion::where('post_audit_id',$standard_stage->postAudit->id)->where('question_id',2)->first();

                                                                        @endphp
                                                                        @if(!is_null($postAuditQuestionOne) && !is_null($postAuditQuestionTwo))

                                                                            @if($postAuditQuestionOne->value == 'YES' && is_null($postAuditQuestionOne->scheme_value))
                                                                                Cat
                                                                                1
                                                                                Open
                                                                                <br>
                                                                            @endif



                                                                            @if($postAuditQuestionOne->value == 'NO' && is_null($postAuditQuestionOne->scheme_value))
                                                                                Cat
                                                                                1
                                                                                No
                                                                                CARs
                                                                                <br>
                                                                            @endif
                                                                            @if(!is_null($postAuditQuestionOne->scheme_value) && $postAuditQuestionOne->scheme_value == 'NO')
                                                                                Cat
                                                                                1
                                                                                Open
                                                                                <br>
                                                                            @endif
                                                                            @if(!is_null($postAuditQuestionOne->scheme_value) && $postAuditQuestionOne->scheme_value == 'YES')
                                                                                Cat
                                                                                1
                                                                                Closed
                                                                                <br>
                                                                            @endif
                                                                            @if(!is_null($postAuditQuestionOne->scheme_value) && $postAuditQuestionOne->scheme_value == 'N/A')
                                                                                Cat
                                                                                1
                                                                                No
                                                                                CARs
                                                                                <br>
                                                                            @endif
                                                                            @if($postAuditQuestionTwo->value == 'YES' && is_null($postAuditQuestionTwo->scheme_value))
                                                                                Cat
                                                                                2
                                                                                Open
                                                                                <br>
                                                                            @endif

                                                                            @if($postAuditQuestionTwo->value == 'NO' && is_null($postAuditQuestionTwo->scheme_value))
                                                                                Cat
                                                                                2
                                                                                No
                                                                                CARs
                                                                                <br>
                                                                            @endif

                                                                            @if(!is_null($postAuditQuestionTwo->scheme_value) && $postAuditQuestionTwo->scheme_value == 'NO')
                                                                                Cat
                                                                                2
                                                                                Open
                                                                                <br>
                                                                            @endif

                                                                            @if(!is_null($postAuditQuestionTwo->scheme_value) && $postAuditQuestionTwo->scheme_value == 'YES')
                                                                                Cat
                                                                                2
                                                                                Closed
                                                                                <br>
                                                                            @endif

                                                                            @if(!is_null($postAuditQuestionTwo->scheme_value) && $postAuditQuestionTwo->scheme_value == 'N/A')
                                                                                Cat
                                                                                2
                                                                                No
                                                                                CARs
                                                                                <br>

                                                                            @endif

                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    @endif

                                                                </td>
                                                                <td>
                                                                    @if(!is_null($standard_stage->postAudit))
                                                                        @if(!is_null($standard_stage->postAudit->postAuditSchemeInfo) && $standard_stage->postAudit->postAuditSchemeInfo->print_required == 'YES')
                                                                            @if ($standard_stage->audit_type =='stage_2')
                                                                                <a href="{{ route('welcomeCertificate', [ $companyStandard->id,$standard_stage->id]) }}"
                                                                                   title="Print Welcome Letter"
                                                                                   target="_blank"><i
                                                                                            class="fa fa-print"><br>Certificate
                                                                                        Letter</i>
                                                                                </a>

                                                                            @else
                                                                                <a href="{{ route('welcomeCertificate1', [ $companyStandard->id,$standard_stage->id]) }}"
                                                                                   title="Print Welcome Letter"
                                                                                   target="_blank"><i
                                                                                            class="fa fa-print"><br>Certificate
                                                                                        Letter</i>
                                                                                </a>

                                                                            @endif
                                                                            <br>


                                                                            @if(count($standard_stage->postAudit->postAuditCertificateFile)>0)
                                                                                @if ($standard_stage->postAudit->postAuditSchemeInfo->print_required == 'YES' && $standard_stage->special_aj_remarks == null)
                                                                                    @if ($standard_stage->audit_type =='stage_2')
                                                                                        Printed

                                                                                        {{--                                                                                                                                    <br>--}}
                                                                                        {{--                                                                                                                                    <a href="{{ route('certificate-letter', [ $companyStandard->id,$standard_stage->id]) }}"--}}
                                                                                        {{--                                                                                                                                       title="Certificate Letter">Certificate--}}
                                                                                        {{--                                                                                                                                        Letter--}}
                                                                                        {{--                                                                                                                                    </a>--}}
                                                                                    @else
                                                                                        Printed
                                                                                        <br>
                                                                                        {{--                                                                                                                                    <a href="{{ route('certificate-letter-others', [ $companyStandard->id,$standard_stage->id]) }}"--}}
                                                                                        {{--                                                                                                                                       title="Certificate Letter">Certificate--}}
                                                                                        {{--                                                                                                                                        Letter--}}
                                                                                        {{--                                                                                                                                    </a>--}}
                                                                                    @endif
                                                                                @else

                                                                                    N/A
                                                                                @endif
                                                                            @else
                                                                                In
                                                                                Process
                                                                                <br/>
                                                                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator')
                                                                                    @if($standard_stage->postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_manager')
                                                                                        <a href="{{ route('post.audit.draft.print', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Print Draft"><i
                                                                                                    class="fa fa-print"></i>
                                                                                        </a>
                                                                                    @elseif($standard_stage->postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_coordinator')
                                                                                        <a href="{{ route('post.audit.draft.print', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Print Draft"><i
                                                                                                    class="fa fa-print"></i>
                                                                                        </a>
                                                                                    @else
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    @endif
                                                                </td>

                                                            </tr>

                                                        @endforeach
                                                    @endforeach
                                                @else
                                                    @foreach($audit_justification->ajStandards()->where('standard_id', $companyStandard->standard_id)->get() as $index=>$aj_standard)

                                                        @foreach($aj_standard->ajStandardStages as  $index=>$standard_stage)

                                                            <tr class="tableNoBg">
                                                                {{--                                                                                                    {{dd($standard_stage->getAuditType())}}--}}
                                                                <td>{{ $standard_stage->getAuditType() }}
                                                                    @if(auth()->user()->user_type == 'scheme_manager')
                                                                        @if(preg_match("/Verification/", $standard_stage->getAuditType()))
                                                                        @else
                                                                            <br>
                                                                            <span><input
                                                                                        type="checkbox"
                                                                                        id="ajApprovedStatus"
                                                                                        data-id="{{$standard_stage->id}}"
                                                                                        value="{{$standard_stage->aj_approved_status == false ? 0 : 1}}"
                                                                                        class="form-control" {{$standard_stage->aj_approved_status == false ? '': 'checked'}}></span>
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td>{{  $standard_stage->excepted_date != null && $standard_stage->audit_type !== 'stage_1' && $standard_stage->audit_type !== 'stage_2'  ? date('d-m-Y',strtotime($standard_stage->excepted_date)) : ''  }}</td>

                                                            @if($standard_stage->audit_type === 'stage_2_verification' || $standard_stage->audit_type === 'surveillance_1_verification' || $standard_stage->audit_type === 'surveillance_2_verification' || $standard_stage->audit_type === 'surveillance_3_verification' || $standard_stage->audit_type === 'surveillance_4_verification' || $standard_stage->audit_type === 'surveillance_5_verification' || $standard_stage->audit_type === 'reaudit_verification' || $standard_stage->audit_type === 'stage_2_scope_extension' || $standard_stage->audit_type === 'surveillance_1_scope_extension' || $standard_stage->audit_type === 'surveillance_2_scope_extension' || $standard_stage->audit_type === 'surveillance_3_scope_extension' || $standard_stage->audit_type === 'surveillance_4_scope_extension' || $standard_stage->audit_type === 'surveillance_5_scope_extension' || $standard_stage->audit_type === 'reaudit_scope_extension' || $standard_stage->audit_type === 'stage_2_special_transition' || $standard_stage->audit_type === 'surveillance_1_special_transition' || $standard_stage->audit_type === 'surveillance_2_special_transition' || $standard_stage->audit_type === 'surveillance_3_special_transition' || $standard_stage->audit_type === 'surveillance_4_special_transition' || $standard_stage->audit_type === 'surveillance_5_special_transition' || $standard_stage->audit_type === 'reaudit_special_transition')
                                                                    <td>
                                                                        @if($standard_stage->status === 'Not Applied')
                                                                            @can('create_aj|edit_aj')
                                                                                <a href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                   class="btn btn-primary tblBtn">
                                                                                    Create
                                                                                    New
                                                                                </a>
                                                                            @endcan
                                                                        @else
                                                                            @can('show_aj')
                                                                                <a class="fontSize"
                                                                                   href="{{ route('justification.show.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                   title="View"><i
                                                                                            class="fa fa-eye"></i>
                                                                                </a>
                                                                            @endcan
                                                                            @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                            @else
                                                                                @can('edit_aj')

                                                                                    @if($standard_stage->status === 'applied' || $standard_stage->status === 'created')

                                                                                        @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                            <a href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                               class="fontSzie {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                               title="Edit">
                                                                                                <i class="fa fa-pencil"></i>
                                                                                            </a>
                                                                                        @else
                                                                                            <a class="fontSize"
                                                                                               href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                               title="Edit"><i
                                                                                                        class="fa fa-pencil"></i>
                                                                                            </a>
                                                                                        @endif
                                                                                    @else
                                                                                        @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                            <a href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                               class="fontSize {{ ($standard_stage->status === 'applied' ||  $standard_stage->status === 'approved') ? 'disabledDiv' :''  }}"
                                                                                               title="Rewise">
                                                                                                <i class="fa fa-recycle"></i>
                                                                                            </a>
                                                                                        @else
                                                                                            <a class="fontSize"
                                                                                               href="{{ route('justification.edit.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                               title="Rewise">
                                                                                                <i class="fa fa-recycle"></i>
                                                                                            </a>
                                                                                        @endif

                                                                                    @endif
                                                                                @endcan
                                                                                @can('delete_aj')
                                                                                    <a class="fontSize"
                                                                                       onclick="showConfirmForDelete('{{ route('justification.delete.special',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}')"
                                                                                       href="javascript:void(0)"

                                                                                       title="Delete"><i
                                                                                                class="fa fa-close"></i>
                                                                                    </a>
                                                                                @endcan
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                @else
                                                                    @php
                                                                        $disable = '';
                                                                    @endphp
                                                                    @if($index > 0)
                                                                        @if($aj_standard->ajStandardStages[$index -1]->status == 'approved' && $aj_standard->ajStandardStages[$index -1]->audit_type == 'stage_1')
                                                                            @php
                                                                                $disable = '';
                                                                            @endphp
                                                                        @elseif($aj_standard->ajStandardStages[$index -1]->status == 'approved' && str_contains($aj_standard->ajStandardStages[$index -1]->getAuditType(), 'Verification'))
                                                                            @php
                                                                                $disable = '';
                                                                            @endphp
                                                                        @else
                                                                            @if($aj_standard->ajStandardStages[$index -1]->status === 'approved'  && !is_null($aj_standard->ajStandardStages[$index -1]->postAudit) && isset($aj_standard->ajStandardStages[$index -1]->postAudit) && $aj_standard->ajStandardStages[$index -1]->postAudit->status == 'approved' && isset($aj_standard->ajStandardStages[$index -1]->postAudit->postAuditSchemeInfo) && !is_null($aj_standard->ajStandardStages[$index -1]->postAudit->postAuditSchemeInfo))
                                                                                @php
                                                                                    $disable = '';
                                                                                @endphp
                                                                            @else
                                                                                @php
                                                                                    $disable = 'disabledDiv';
                                                                                @endphp
                                                                            @endif
                                                                        @endif
                                                                        {{--                                                                                                                        {{ ($index > 0) ? () ? 'disabledDiv' : '' :''}}--}}
                                                                    @endif
                                                                    @php
                                                                        if(isset($companyStandard->certificates) && ($companyStandard->certificates->certificate_status == 'withdrawn' || $companyStandard->certificates->certificate_status == 'closed')){
                                                                           $disable = 'disabledDiv';
                                                                        }
                                                                    @endphp
                                                                    <td class="">

                                                                        @if($standard_stage->status === 'Not Applied')
                                                                            @can('edit_aj')
                                                                                @if($index > 0 && preg_match("/Verification/", $aj_standard->ajStandardStages[$index -1]->getAuditType()) )
                                                                                    <a href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                       class="btn btn-primary tblBtn">
                                                                                        Create
                                                                                        New
                                                                                    </a>
                                                                                @else
                                                                                    <a href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                       class="btn btn-primary tblBtn @if($disable == 'disabledDiv') btnColorDanger @endif {{ $disable }}">
                                                                                        Create
                                                                                        New
                                                                                    </a>
                                                                                @endif
                                                                            @endcan
                                                                        @else
                                                                            @can('show_aj')
                                                                                <a class="fontSize "
                                                                                   href="{{ route('justification.show', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                   title="View"><i
                                                                                            class="fa fa-eye"></i>
                                                                                </a>
                                                                            @endcan
                                                                            @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                            @else
                                                                                @can('edit_aj')
                                                                                    @if(auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager')
                                                                                        @if(isset($standard_stage->postAudit->actual_audit_date) &&  $standard_stage->status === 'approved')
                                                                                        @else
                                                                                            @if($standard_stage->status === 'applied' || $standard_stage->status === 'created')

                                                                                                @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                    <a href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       class="fontSize {{ $disable }} {{ ($standard_stage->status === 'applied' || $standard_stage->status === 'resent') ? 'disabledDiv' :''  }}"
                                                                                                       title="Edit">
                                                                                                        <i class="fa fa-pencil"></i>
                                                                                                    </a>
                                                                                                @else
                                                                                                    <a class="fontSize {{ $disable }}"
                                                                                                       href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       title="Edit"><i
                                                                                                                class="fa fa-pencil"></i>
                                                                                                    </a>
                                                                                                @endif
                                                                                            @else

                                                                                                @if(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator')
                                                                                                    <a href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       class="fontSize {{ $disable }} {{ ($standard_stage->status === 'applied'  || $standard_stage->status === 'resent') ? 'disabledDiv' :''  }}"
                                                                                                       title="Rewise">
                                                                                                        <i class="fa fa-recycle"></i>
                                                                                                    </a>
                                                                                                @else
                                                                                                    <a class="fontSize {{ $disable }}"
                                                                                                       href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                                       title="Rewise">
                                                                                                        <i class="fa fa-recycle"></i>
                                                                                                    </a>
                                                                                                @endif

                                                                                            @endif
                                                                                        @endif
                                                                                    @else
                                                                                        @if($standard_stage->status === 'applied' || $standard_stage->status === 'created')

                                                                                            <a class="fontSize "
                                                                                               href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                               title="Edit"><i
                                                                                                        class="fa fa-pencil"></i>
                                                                                            </a>

                                                                                        @else

                                                                                            <a class="fontSize {{ $disable }}"
                                                                                               href="{{ route('justification.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->name,$standard_stage->id]) }}"
                                                                                               title="Rewise">
                                                                                                <i class="fa fa-recycle"></i>
                                                                                            </a>

                                                                                        @endif
                                                                                    @endif
                                                                                @endcan

                                                                                @can('delete_aj')

                                                                                    @php
                                                                                        $disableDelete = isset($standard_stage->postAudit) && isset($standard_stage->postAudit->actual_audit_date) ? 'disabledDiv' : '';
                                                                                    @endphp
                                                                                    <a class="fontSize {{ $disable }} {{ $disableDelete }}"
                                                                                       onclick="showConfirmForDelete('{{ route('justification.delete',[$company->id, $standard_stage->audit_type, $companyStandard->standard->name,$standard_stage->id]) }}')"
                                                                                       href="javascript:void(0)"

                                                                                       title="Delete"><i
                                                                                                class="fa fa-close"></i>
                                                                                    </a>
                                                                                @endcan
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    @if($standard_stage->status == 'approved')
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                        <br>
                                                                        {{ date('d-m-Y',strtotime($standard_stage->approval_date)) }}
                                                                    @elseif($standard_stage->status == 'rejected')
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                        <br>
                                                                        {{ date('d-m-Y',strtotime($standard_stage->updated_at)) }}
                                                                    @else
                                                                        {{ ucfirst($standard_stage->status) }}
                                                                    @endif
                                                                </td>
                                                                <td>{{ isset($standard_stage->postAudit->actual_audit_date) ? date("d-m-Y", strtotime($standard_stage->postAudit->actual_audit_date)) : '---' }}
                                                                    - {{ isset($standard_stage->postAudit->actual_audit_date_to) ? date("d-m-Y", strtotime($standard_stage->postAudit->actual_audit_date_to)) : '---' }}</td>

                                                                <td>

                                                                    @if($standard_stage->status == 'approved')

                                                                        @if($standard_stage->audit_type === 'stage_2_verification' || $standard_stage->audit_type === 'surveillance_1_verification' || $standard_stage->audit_type === 'surveillance_2_verification' || $standard_stage->audit_type === 'surveillance_3_verification' || $standard_stage->audit_type === 'surveillance_4_verification' || $standard_stage->audit_type === 'surveillance_5_verification' || $standard_stage->audit_type === 'reaudit_verification')
                                                                        @else
                                                                            @if(!is_null($standard_stage->postAudit))
                                                                                <a href="{{ route('post.audit.show', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                   title="View Date"
                                                                                   class="fontSize"><i
                                                                                            class="fa fa-eye"></i>
                                                                                </a>

                                                                                @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                                @else
                                                                                    @if($standard_stage->postAudit->status == 'rejected' || $standard_stage->postAudit->status == 'save')

                                                                                        <a href="{{ route('post.audit.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Edit Date"
                                                                                           class="fontSize"><i
                                                                                                    class="fa fa-pencil"></i>
                                                                                        </a>

                                                                                    @endif
                                                                                    @if (auth()->user()->user_type == 'scheme_manager')

                                                                                        <a
                                                                                                title="Delete Date"
                                                                                                onclick="showConfirmForDelete('{{ route('post.audit.delete', [$company->id, $standard_stage->postAudit->id, $companyStandard->standard->id,$standard_stage->id]) }}')"
                                                                                                href="javascript:void(0)"
                                                                                                class="fontSize"><i
                                                                                                    class="fa fa-close"></i>
                                                                                        </a>
                                                                                        <br>
                                                                                        <a href="{{ route('post.audit.scheme.edit', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Audit Activity">Audit
                                                                                            Activity
                                                                                        </a>

                                                                                    @endif
                                                                                @endif

                                                                            @else
                                                                                @if($companyStandard->status == 'disabled' || $companyStandard->status == 'pending')
                                                                                @else
                                                                                    @if(auth()->user()->user_type != 'scheme_coordinator')
                                                                                        <a href="{{ route('post.audit.create', [$company->id,$standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           class="btn btn-success"
                                                                                           title="Add Date">Add
                                                                                            Date</a>
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(!is_null($standard_stage->postAudit))
                                                                        @if($standard_stage->postAudit->status == 'save')
                                                                            @php
                                                                                $status = '';
       $postAuditQuestions = \App\PostAuditQueestion::where('post_audit_id',$standard_stage->postAudit->id)->where('question_id',3)->first();
        $file = \App\PostAuditQuestionFile::where('post_audit_question_id',$postAuditQuestions->id)->first();
        if(is_null($file)){
            $status = 'no';
        }else{
            $status = '';
        }


                                                                            @endphp
                                                                            @if(!is_null($standard_stage->postAudit->actual_audit_date) && $status == 'no')
                                                                                Pack
                                                                                Not
                                                                                Uploaded
                                                                            @else
                                                                                Pack
                                                                                Not
                                                                                Sent
                                                                            @endif
                                                                        @elseif($standard_stage->postAudit->status == 'unapproved')
                                                                            Submitted
                                                                            <br>
                                                                            {{ date('d-m-Y',strtotime($standard_stage->postAudit->updated_at)) }}
                                                                            - @if($standard_stage->postAudit->sentToUser->user_type == 'scheme_manager')
                                                                                SM
                                                                            @elseif($standard_stage->postAudit->sentToUser->user_type == 'scheme_coordinator')
                                                                                SC
                                                                            @endif
                                                                        @elseif($standard_stage->postAudit->status == 'rejected')
                                                                            Rejected
                                                                            <br>
                                                                            {{ date('d-m-Y',strtotime($standard_stage->postAudit->updated_at)) }}
                                                                        @elseif($standard_stage->postAudit->status == 'resent')
                                                                            Resent
                                                                        @elseif($standard_stage->postAudit->status == 'accepted')
                                                                            Forwarded
                                                                            <br>
                                                                            {{ date('d-m-Y',strtotime($standard_stage->postAudit->updated_at)) }}
                                                                        @elseif($standard_stage->postAudit->status == 'approved')
                                                                            Approved
                                                                            <br>
                                                                            @if(!is_null($standard_stage->postAudit->postAuditSchemeInfo))
                                                                                {{ date('d-m-Y',strtotime($standard_stage->postAudit->postAuditSchemeInfo->approval_date)) }}
                                                                            @endif
                                                                        @endif
                                                                    @else
                                                                    @endif


                                                                </td>
                                                                <td>
                                                                    @if(!is_null($standard_stage->postAudit))
                                                                        @php
                                                                            $postAuditQuestionOne = \App\PostAuditQueestion::where('post_audit_id',$standard_stage->postAudit->id)->where('question_id',1)->first();
   $postAuditQuestionTwo = \App\PostAuditQueestion::where('post_audit_id',$standard_stage->postAudit->id)->where('question_id',2)->first();

                                                                        @endphp

                                                                        @if(!is_null($postAuditQuestionOne) && !is_null($postAuditQuestionTwo) && $standard_stage->audit_type != 'stage_1')
                                                                            @if($postAuditQuestionOne->value == 'YES' && is_null($postAuditQuestionOne->scheme_value))
                                                                                Cat
                                                                                1
                                                                                Open
                                                                                <br>
                                                                            @endif



                                                                            @if($postAuditQuestionOne->value == 'NO' && is_null($postAuditQuestionOne->scheme_value))
                                                                                Cat
                                                                                1
                                                                                No
                                                                                CARs
                                                                                <br>
                                                                            @endif
                                                                            @if(!is_null($postAuditQuestionOne->scheme_value) && $postAuditQuestionOne->scheme_value == 'NO')
                                                                                Cat
                                                                                1
                                                                                Open
                                                                                <br>
                                                                            @endif
                                                                            @if(!is_null($postAuditQuestionOne->scheme_value) && $postAuditQuestionOne->scheme_value == 'YES')
                                                                                Cat
                                                                                1
                                                                                Closed
                                                                                <br>
                                                                            @endif
                                                                            @if(!is_null($postAuditQuestionOne->scheme_value) && $postAuditQuestionOne->scheme_value == 'N/A')
                                                                                Cat
                                                                                1
                                                                                No
                                                                                CARs
                                                                                <br>
                                                                            @endif
                                                                            @if($postAuditQuestionTwo->value == 'YES' && is_null($postAuditQuestionTwo->scheme_value))
                                                                                Cat
                                                                                2
                                                                                Open
                                                                                <br>
                                                                            @endif

                                                                            @if($postAuditQuestionTwo->value == 'NO' && is_null($postAuditQuestionTwo->scheme_value))
                                                                                Cat
                                                                                2
                                                                                No
                                                                                CARs
                                                                                <br>
                                                                            @endif

                                                                            @if(!is_null($postAuditQuestionTwo->scheme_value) && $postAuditQuestionTwo->scheme_value == 'NO')
                                                                                Cat
                                                                                2
                                                                                Open
                                                                                <br>
                                                                            @endif

                                                                            @if(!is_null($postAuditQuestionTwo->scheme_value) && $postAuditQuestionTwo->scheme_value == 'YES')
                                                                                Cat
                                                                                2
                                                                                Closed
                                                                                <br>
                                                                            @endif

                                                                            @if(!is_null($postAuditQuestionTwo->scheme_value) && $postAuditQuestionTwo->scheme_value == 'N/A')
                                                                                Cat
                                                                                2
                                                                                No
                                                                                CARs
                                                                                <br>

                                                                            @endif
                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    @endif

                                                                </td>
                                                                <td>
                                                                    @if(!is_null($standard_stage->postAudit))
                                                                        @if(!is_null($standard_stage->postAudit->postAuditSchemeInfo) && $standard_stage->postAudit->postAuditSchemeInfo->print_required == 'YES')
                                                                            @if(count($standard_stage->postAudit->postAuditCertificateFile)>0)
                                                                                @if ($standard_stage->audit_type =='stage_2'   )
                                                                                    <a href="{{ route('welcomeCertificate', [ $companyStandard->id,$standard_stage->id]) }}"
                                                                                       title="Print Welcome Letter"
                                                                                       target="_blank"><i
                                                                                                class="fa fa-print"><br>Certificate
                                                                                            Letter</i>
                                                                                    </a>

                                                                                @else
                                                                                    <a href="{{ route('welcomeCertificate1', [ $companyStandard->id,$standard_stage->id]) }}"
                                                                                       title="Print Welcome Letter"
                                                                                       target="_blank"><i
                                                                                                class="fa fa-print"><br>Certificate
                                                                                            Letter</i>
                                                                                    </a>

                                                                                @endif
                                                                                <br>
                                                                                @if ($standard_stage->postAudit->postAuditSchemeInfo->print_required == 'YES' && $standard_stage->special_aj_remarks == null)
                                                                                    @if ($standard_stage->audit_type =='stage_2')
                                                                                        Printed

                                                                                        {{--                                                                                                                                    <br>--}}
                                                                                        {{--                                                                                                                                    <a href="{{ route('certificate-letter', [ $companyStandard->id,$standard_stage->id]) }}"--}}
                                                                                        {{--                                                                                                                                       title="Print Draft">Certificate--}}
                                                                                        {{--                                                                                                                                        Letter--}}
                                                                                        {{--                                                                                                                                    </a>--}}
                                                                                    @else
                                                                                        Printed
                                                                                        {{--                                                                                                                                    <br>--}}
                                                                                        {{--                                                                                                                                    <a href="{{ route('certificate-letter-others', [ $companyStandard->id,$standard_stage->id]) }}"--}}
                                                                                        {{--                                                                                                                                       title="Print Draft">Certificate--}}
                                                                                        {{--                                                                                                                                        Letter--}}
                                                                                        {{--                                                                                                                                    </a>--}}
                                                                                    @endif
                                                                                @else

                                                                                    N/A
                                                                                @endif

                                                                            @else
                                                                                In
                                                                                Process
                                                                                <br/>
                                                                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator')
                                                                                    @if($standard_stage->postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_manager')
                                                                                        <a href="{{ route('post.audit.draft.print', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Print Draft"><i
                                                                                                    class="fa fa-print"></i>
                                                                                        </a>
                                                                                    @elseif($standard_stage->postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_coordinator')
                                                                                        <a href="{{ route('post.audit.draft.print', [$company->id, $standard_stage->getAuditType(), $companyStandard->standard->id,$standard_stage->id]) }}"
                                                                                           title="Print Draft"><i
                                                                                                    class="fa fa-print"></i>
                                                                                        </a>
                                                                                    @else
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        @else
                                                                            N/A
                                                                        @endif
                                                                    @endif
                                                                </td>

                                                            </tr>

                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            @else
                                                @php

                                                    $audit_activity_stage =\App\AuditActivityStages::where('id',$companyStandard->old_cb_audit_activity_stage_id)->get()->last();
                                                @endphp



                                                @if($companyStandard->client_type === 'transfered')
                                                    <tr>
                                                        <td>{{$audit_activity_stage->name}}
                                                            + {{ucfirst($companyStandard->client_type) }}</td>
                                                        <td></td>
                                                        <td>
                                                            <a href="{{ route('justification.transfer.create', [$company->id, str_replace(' ','_',lcfirst($audit_activity_stage->name)),$companyStandard->old_cb_surveillance_frequency, $companyStandard->standard->name]) }}"
                                                               class="btn btn-primary tblBtn">
                                                                Create AJ
                                                            </a>
                                                        </td>
                                                        <td>Not Required
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                        <td>

                                                        <td></td>
                                                        <td>
                                                        </td>

                                                    </tr>
                                                @else
                                                    @if($companyStandard->standard->standardFamily->name === 'Information Security')
                                                        <tr>
                                                            <td>Stage 1</td>
                                                            <td></td>
                                                            <td>
                                                                <a href="javascript:void(0)"
                                                                   onclick="showConfirmForStage('{{ route('justification.create', [$company->id, 'stage_1', $companyStandard->standard->name]) }}')"
                                                                   class="btn btn-primary tblBtn">
                                                                    Create New
                                                                </a>
                                                            <td>Not Applied
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                            <td>

                                                            <td></td>
                                                            <td>
                                                            </td>

                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>Stage 1</td>
                                                            <td></td>
                                                            <td>
                                                                <a href="javascript:void(0)"
                                                                   onclick="showConfirmForStage('{{ route('justification.create', [$company->id, 'stage_1', $companyStandard->standard->name]) }}')"
                                                                   class="btn btn-primary tblBtn">
                                                                    Create New
                                                                </a>
                                                            <td>Not Applied
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                            <td>

                                                            <td></td>
                                                            <td>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>Stage 2</td>
                                                            <td></td>
                                                            <td>
                                                                <a href="{{ route('justification.create', [$company->id, 'stage_1|stage_2', $companyStandard->standard->name]) }}"
                                                                   class="btn btn-primary tblBtn">
                                                                    Create New
                                                                </a>
                                                            <td>Not Applied
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                            <td>

                                                            <td></td>
                                                            <td>
                                                            </td>

                                                        </tr>
                                                    @endif
                                                @endif

                                            @endif

                                        @endforeach

                                    @else

                                        @php

                                            $audit_activity_stage =\App\AuditActivityStages::where('id',$companyStandard->old_cb_audit_activity_stage_id)->get()->last();
                                        @endphp
                                        @if($companyStandard->client_type === 'transfered')
                                            <tr>
                                                <td>{{$audit_activity_stage->name}}
                                                    + {{ucfirst($companyStandard->client_type) }}</td>
                                                <td></td>
                                                <td>
                                                    <a href="{{ route('justification.transfer.create', [$company->id, str_replace(' ','_',lcfirst($audit_activity_stage->name)),$companyStandard->old_cb_surveillance_frequency, $companyStandard->standard->name]) }}"
                                                       class="btn btn-primary tblBtn">
                                                        Create AJ
                                                    </a>
                                                </td>
                                                <td>Not Required
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>

                                            </tr>
                                        @else

                                            @if($companyStandard->standard->standardFamily->name === 'Information Security')
                                                <tr>
                                                    <td>Stage 1</td>
                                                    <td></td>
                                                    <td>
                                                        <a href="javascript:void(0)"
                                                           onclick="showConfirmForStage('{{ route('justification.create', [$company->id, 'stage_1', $companyStandard->standard->name]) }}')"
                                                           class="btn btn-primary tblBtn">
                                                            Create New
                                                        </a>
                                                    </td>

                                                    <td>Not Applied
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>
                                            @else
                                                <tr>
                                                    <td>Stage 1</td>
                                                    <td></td>
                                                    <td>
                                                        <a href="javascript:void(0)"
                                                           onclick="showConfirmForStage('{{ route('justification.create', [$company->id, 'stage_1', $companyStandard->standard->name]) }}')"
                                                           class="btn btn-primary tblBtn">
                                                            Create New
                                                        </a>
                                                    </td>

                                                    <td>Not Applied
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>

                                                <tr>
                                                    <td>Stage 2</td>
                                                    <td></td>
                                                    <td>
                                                        <a href="{{ route('justification.create', [$company->id, 'stage_1|stage_2', $companyStandard->standard->name]) }}"
                                                           class="btn btn-primary tblBtn">
                                                            Create New
                                                        </a>
                                                    <td>Not Applied
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                    <td>

                                                    <td></td>
                                                    <td>
                                                    </td>

                                                </tr>
                                            @endif
                                        @endif

                                    @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
    </div>
</div>
