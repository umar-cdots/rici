{{-- Standard Questions Modal --}}
@foreach($standards as $standard)
    @php
        $company_standard = $company->companyStandards->where('standard_id', $standard->id)->first();
    @endphp
    <div class="modal fade standard_questions" id="standard_question_{{ $standard->id }}" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: auto">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#" method="post" class="standard_questionnaire" data-standard-id="{{ $standard->id }}"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">{{ $standard->name }} Additional
                            Information</h4>
                        {{--                            <button type="button" class="close"--}}
                        {{--                                    onclick="closeStandardQuestionModal('{{$standard->id}}')"><span--}}
                        {{--                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>--}}
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="step" value="4">
                        <input type="hidden" name="standard_id" value="{{ $standard->id }}">
                        <input type="hidden" name="company_standard_id" value="{{ $company_standard->id ?? '' }}">
                        <input type="hidden" name="is_ims" value="{{ $company_standard->is_ims ?? '' }}">
                        @if(!empty($company_standard))
                            <input type="hidden" name="company_standard_action" value="update">
                        @endif
                        <div class="col-md-12 text-center" style="display:none">
                            <div class="col-md-6 floting">
                                <label>Transfered from another CB?</label>
                            </div>
                            <div class="col-md-6 floting">
                                <input type="checkbox" name="client_type" value="transfered"
                                       class="transfered_customer" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? 'checked' : '') : '' }}>&nbsp;&nbsp;<span>Yes</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="col-md-6 floting">
                                <label>Transfered from another CB?</label>
                            </div>
                            <div class="col-md-6 floting">
                                <input type="checkbox" name="is_transfer_standard" value="{{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? 1 : 0) : 0 }}"
                                       class="is_transfer_standard" {{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? 'checked' : '') : '' }}>&nbsp;&nbsp;<span>Yes</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12 mrgn is_transfer_standard_fields"
                             style="display: {{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? 'unset' : 'none') : 'none' }};">
                            <div class="col-md-12">
                                <div class="col-md-3 floting">
                                    <label>Transfer Year *</label>
                                    <input type="text" name="transfer_year" class="form-control transfer_year"
                                           value="{{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? $company_standard->transfer_year : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? '' : 'disabled') : 'disabled' }}>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                        <div class="col-md-12 mrgn transfered_fields"
                             style="display: {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? 'unset' : 'none') : 'none' }};">
                            <div class="col-md-12">
                                <div class="col-md-3 floting">
                                    <label>CB Name *</label>
                                    <input type="text" name="old_cb_name" class="form-control"
                                           placeholder="Enter CB Name"
                                           value="{{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? $company_standard->old_cb_name : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                </div>
                                <div class="col-md-3 floting">
                                    <label>Certification Issue Date *</label>
                                    <input type="text" name="old_cb_certificate_issue_date"
                                           class="form-control float-right  active old_cb_certificate_issue_date"
                                           {{--                                               onchange="dateStandardBased('{{$standard->id}}')"--}}
                                           id="old_cb_certificate_issue_date{{$standard->id}}"
                                           data-standard-id="{{$standard->id}}"
                                           onkeypress="return false;"
                                           placeholder="Enter Certification Issued"
                                           value="{{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? $company_standard->old_cb_certificate_issue_date->format('m/d/Y') : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                </div>
                                <div class="col-md-3 floting">
                                    <label>Certification Expiry *</label>
                                    <input type="text" name="old_cb_certificate_expiry_date"
                                           class="form-control float-right  active"
                                           id="old_cb_certificate_expiry_date{{$standard->id}}"
                                           data-standard-id="{{$standard->id}}"
                                           placeholder="Enter Certification Expiry"
                                           onkeypress="return false;"
                                           value="{{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? $company_standard->old_cb_certificate_expiry_date->format('m/d/Y') : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label>Previous Surveillance Frequency *</label>
                                        <select class="form-control"
                                                id="previousSurveillanceFrequency{{$standard->id}}"
                                                onchange="previousFrequncy('#previousSurveillanceFrequency{{$standard->id}}',{{$standard->id}})"
                                                name="old_cb_surveillance_frequency" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                            {{--                                                <option value="">Select Surveillance Frequency</option>--}}
                                            @foreach($surveillance_frequencies as $frequency)
                                                <option value="{{ $frequency }}" {{ !empty($company_standard) ? ($frequency == $company_standard->old_cb_surveillance_frequency ? 'selected' : '') : '' }}>
                                                    {{ ucfirst($frequency) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3 floting">
                                    <label>Last Audit Activity *</label>
                                    <select class="form-control" id="old_cb_audit_activity_stage_id"
                                            name="old_cb_audit_activity_stage_id"
                                            data-standard-id="{{$standard->id}}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                        <option value="">Select Activity</option>
                                        @foreach($audit_activity_stages as $stage)
                                            @continue($stage->name == 'Stage 1')
                                            <option value="{{ $stage->id }}"
                                                    id="last-audit-activity-{{ $stage->id }}{{$standard->id}}"
                                                    {{ !empty($company_standard) ? ($stage->id == $company_standard->old_cb_audit_activity_stage_id ? 'selected' : '') : '' }}>
                                                {{ $stage->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-9 floting uploadCheck">
                                    {{-- <label>Files Upload</label> --}}
                                    <div class="form-group">
                                        <div class="col-md-4 floting">
                                            <input type="checkbox" name="old_cb_last_audit_report_attached"
                                                   value="attached" checked="" style="display: none;"><label>Last
                                                Audit Report * (10MB)</label>
                                            <div class="input-group form-group">
                                                <div class="custom-file">

                                                    <input type="file" id="old_cb_last_audit_report_doc"
                                                           name="old_cb_last_audit_report_doc"
                                                           data-standard-id="{{$standard->id}}1"
                                                           style="display:block !important;"
                                                           name="old_cb_last_audit_report_doc" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}
                                                    >

                                                </div>

                                                <div class="input-group-append"></div>
                                            </div>
                                            @if( !empty($company_standard) && !is_null($company_standard->old_cb_last_audit_report_doc_media_id))
                                                <div class="image form-group" id="upld{{$standard->id}}1">

                                                    <h6>Selected File: <span><a target="_blank"
                                                                                href="{{ asset('uploads/company_audit_report/'.$company_standard->old_cb_last_audit_report_doc_media_id) }}">{{ $company_standard->old_cb_last_audit_report_doc_media_id }}</a></span>
                                                    </h6>
                                                </div>
                                            @else
                                                <div class="image form-group" id="upld{{$standard->id}}1">
                                                </div>

                                            @endif
                                        </div>
                                        <div class="col-md-4 floting">
                                            <input type="checkbox" name="old_cb_certificate_copy_attached"
                                                   value="attached" checked="" style="display: none;"><label>Certificate
                                                Copy * (10MB)</label>
                                            <div class="input-group form-group">
                                                <div class="custom-file">


                                                    <input type="file" id="old_cb_certificate_copy_doc"
                                                           name="old_cb_certificate_copy_doc"
                                                           data-standard-id="{{$standard->id}}2"
                                                           style="display:block !important;"
                                                           name="old_cb_certificate_copy_doc" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}
                                                    >


                                                </div>
                                                <div class="input-group-append"></div>
                                            </div>
                                            @if( !empty($company_standard) && $company_standard->old_cb_certificate_copy_doc_media_id)
                                                <div class="image form-group" id="upld{{$standard->id}}2">
                                                    <h6>Selected File: <span><a target="_blank"
                                                                                href="{{ asset('uploads/company_certificate_copy/'.$company_standard->old_cb_certificate_copy_doc_media_id) }}">{{ $company_standard->old_cb_certificate_copy_doc_media_id }}</a></span>
                                                    </h6>
                                                </div>
                                            @else
                                                <div class="image form-group" id="upld{{$standard->id}}2">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-4 floting">
                                            <input type="checkbox" name="old_cb_others_attached" value="attached"
                                                   checked="" style="display: none;"><label>Other Documents
                                                (10MB)</label>
                                            <div class="input-group form-group">
                                                <div class="custom-file">


                                                    <input type="file" id="old_cb_others_doc"
                                                           data-standard-id="{{$standard->id}}3"
                                                           style="display:block !important;"
                                                           name="old_cb_others_doc" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}
                                                    >
                                                </div>
                                                <div class="input-group-append"></div>
                                            </div>
                                            @if( !empty($company_standard) && !is_null($company_standard->old_cb_others_doc_media_id))
                                                <div class="image form-group" id="upld{{$standard->id}}3">
                                                    <h6>Selected File: <span><a target="_blank"
                                                                                href="{{ asset('uploads/company_other_doc/'.$company_standard->old_cb_others_doc_media_id) }}">{{ $company_standard->old_cb_others_doc_media_id }}</a></span>
                                                    </h6>
                                                </div>
                                            @else
                                                <div class="image form-group" id="upld{{$standard->id}}3">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 previous_activities_dates">
                                <div class="col-md-6">
                                    <div class="col-md-6 floting activity_type">
                                        <label>Audit Type</label>
                                        @if(!empty($company_standard) && $company_standard->client_type == 'transfered')
                                            @foreach($audit_activity_stages as $stage)
                                                @continue($stage->name == 'Stage 1')
                                                <p>{{ $stage->name }}</p>
                                                @break($stage->id == $company_standard->old_cb_audit_activity_stage_id)
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-md-6 floting activity_date">
                                        <label>Previous Audit Date</label>
                                        @if(!empty($company_standard) && $company_standard->client_type == 'transfered')
                                            @php
                                                $activity_dates = json_decode($company_standard->old_cb_audit_activity_dates, true);
                                            @endphp
                                            @foreach($audit_activity_stages as $stage)
                                                @continue($stage->name == 'Stage 1')
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
<span class="input-group-text"><i
            class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control datefield float-right active"
                                                           id="old_cb_audit_activity_date{{ str_replace(" ", "" , $stage->name) }}{{$standard->id}}"
                                                           name="old_cb_audit_activity_date[{{ $stage->id }}]"
                                                           onkeypress="return false;"
                                                           required=""
                                                           value="{{ $activity_dates[$stage->id] ?? '' }}">
                                                </div>
                                                @break($stage->id == $company_standard->old_cb_audit_activity_stage_id)
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="border_div"></div>
                        <div class="popup_info">

                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <label class="text-uppercase emp_lbl" style="font-weight: 300;"><span
                                                    class="text-lg blue_text" style="font-weight: 700;">Additional Information</span></label>
                                    </div>
                                </div>
                                @foreach($standard->questions()->orderBy('sort')->get() as $question)
                                    @php
                                        $attribs = ' id="question'. $question->id .'" '. ($question->has_dependent_fields ? 'data-dependent-fields="'. $question->dependent_fields_ids .'"' : '') . ($question->has_dependent_fields ? 'data-right-answer="'. $question->right_answer_for_dependent_fields .'"' : '') .' minlength="'. $question->min_length .'" maxlength="'. $question->max_length .'"'. ($question->is_required ? 'required=""' : '') . (($question->is_dependent_field = '0') ? 'disabled=""' : '');
if(!empty($company_standard) && $company_standard->companyStandardsAnswers->where('standard_question_id', $question->id)->count() > 0)
{
$answer = $company_standard->companyStandardsAnswers->where('standard_question_id', $question->id)->first()->answer;
}
else
{
unset($answer);
}
                                    @endphp
                                    <div class="col-md-12">
                                        <div class="col-md-12 floting">
                                            <label>
                                                {{ $question->question }} {{ $question->is_required ? '*' : ''}}
                                            </label>
                                        </div>
                                        @if($question->field_type == 'textarea')
                                            <div class="col-md-12">
<textarea rows="1"
          class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }}"
          name="question[{{ $question->id }}]" {!! $attribs !!}
>{{ $answer ?? $question->default_answer }}</textarea>
                                            </div>
                                        @elseif($question->field_type == 'small_text' || $question->field_type == 'text' || $question->field_type == 'date' || $question->field_type == 'datetime')
                                            <div class="col-md-12 {{ $question->field_type == 'small_text' ? 'col-xs-3' : '' }}">
                                                <input type="{{ $question->field_type == 'number' ? 'number' : 'text' }}"
                                                       name="question[{{ $question->id }}]"
                                                       class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }} {{ $question->field_type == 'datefield' ? '' : '' }} {{ $question->field_type == 'datetime' ? 'datetimefield' : '' }}"
                                                       value="{{ $answer ?? '' }}" {!! $attribs !!}>
                                            </div>
                                        @elseif($question->field_type == 'number')
                                            <div class="col-md-4 {{ $question->field_type == 'small_text' ? 'col-xs-3' : '' }}">
                                                <input type="{{ $question->field_type == 'number' ? 'number' : 'text' }}"
                                                       name="question[{{ $question->id }}]"
                                                       class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }} {{ $question->field_type == 'datefield' ? '' : '' }} {{ $question->field_type == 'datetime' ? 'datetimefield' : '' }}"
                                                       value="{{ $answer ?? 1 }}" {!! $attribs !!}>
                                            </div>
                                            <dic class="clearfix"></dic>
                                        @elseif($question->field_type == 'radio' || $question->field_type == 'checkbox')
                                            @foreach(collect(json_decode($question->options))->chunk(4) as $chunk)
                                                @foreach($chunk as $option)
                                                    <div class="col-md-3 floting">
                                                        <label class="radio-inline">
                                                            <input type="{{ $question->field_type == 'radio' ? 'radio' : 'checkbox' }}"
                                                                   name="question[{{ $question->id }}]"
                                                                   value="{{ $option }}"
                                                                   class="{{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }}"
                                                                   {!! $attribs !!}
                                                                   @if(isset($answer) && $answer == $option)
                                                                       checked
                                                                   @else
                                                                       @if(($question->id == 24 || $question->id == 22  || $question->id == 20) && $option == 'no')
                                                                           checked
                                                                    @endif
                                                                    @endif
                                                            > {{ ucfirst($option) }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endforeach
                                            <div class="clearfix"></div>
                                        @elseif($question->field_type == 'dropdown' || $question->field_type == 'multi_dropdown')
                                            <div class="col-md-12">
                                                <select class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }}"
                                                        {{ $question->field_type == 'multi_dropdown' ? 'multiple' : '' }} {!! $attribs !!} name="question[{{ $question->id }}]{{ $question->field_type == 'multi_dropdown' ? '[]' : '' }}">
                                                    @php
                                                        $decoded_options = json_decode($question->options);
if(isset($answer) && $question->field_type == 'multi_dropdown')
{
$decoded_answer = json_decode($answer, true);
}
                                                    @endphp
                                                    @foreach($decoded_options as $option)
                                                        <option value="{{ $option }}" {{ $question->field_type == 'multi_dropdown' ? (isset($answer) && in_array($option, $decoded_answer) ? 'selected' : '') : (isset($answer) && $option == $answer ? 'selected' : '') }}>
                                                            {{ ucfirst($option) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @elseif($question->field_type == 'special_dropdown')
                                            <div class="col-md-12">
                                                <select class="form-control special_dropdown"
                                                        data-id="id{{ $question->id }}" {!! $attribs !!}>
                                                    <option>Select {{ $question->question }}</option>
                                                    @php
                                                        $decoded_options = json_decode($question->options);
if(isset($answer))
{
$decoded_answer = json_decode($answer, true);
}
                                                    @endphp
                                                    @foreach($decoded_options as $option)
                                                        <option value="{{ $option }}" {{ (isset($answer) && in_array($option, $decoded_answer)) ? 'selected' : '' }}>
                                                            {{ ucfirst($option) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @elseif($question->field_type == 'totally_dependent')
                                            <div class="col-md-12">
                                                <select class="form-control"
                                                        id="id{{ $question->company_standards_question_id }}"
                                                        data-question-title="{{ $question->question }}" {!! $attribs !!}>
                                                    <option>Select {{ $question->question }}</option>

                                                </select>
                                            </div>
                                            @push('scripts')
                                                <script type="text/javascript">
                                                    special_dropdown['id{{ $question->company_standards_question_id }}'] = JSON.parse('{!! $question->options !!}');
                                                </script>
                                            @endpush
                                        @elseif($question->field_type == 'special_dropdown_multiple')
                                            <div class="col-md-12">
                                                <select class="form-control special_dropdown_multiple"
                                                        data-id="id{{ $question->id }}" {!! $attribs !!} multiple=""
                                                        name="question[{{ $question->id }}][]">

                                                    @php
                                                        $decoded_options = json_decode($question->options);
if(isset($answer))
{
$decoded_answer = json_decode($answer, true);
}
                                                    @endphp
                                                    @foreach($decoded_options as $option)
                                                        <option value="{{ $option }}" {{ (isset($answer) && in_array($option, $decoded_answer)) ? 'selected' : '' }}>
                                                            {{ ucfirst($option) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @elseif($question->field_type == 'totally_dependent_multiple')
                                            <div class="col-md-12">
                                                <select class="form-control"
                                                        id="id{{ $question->company_standards_question_id }}"
                                                        data-question-title="{{ $question->question }}"
                                                        {!! $attribs !!} multiple=""
                                                        name="question[{{ $question->id }}][]">

                                                </select>
                                            </div>
                                            @push('scripts')
                                                <script type="text/javascript">
                                                    special_dropdown_multiple['id{{ $question->company_standards_question_id }}'] = JSON.parse('{!! $question->options !!}');
                                                </script>
                                            @endpush
                                        @endif
                                    </div>
                                @endforeach

                                @if($standard->standards_family_id != 20)
                                    <div class="col-md-12">
                                        <div class="col-md-6 pull-left">
                                            <div class="col-md-12">
                                                <label>
                                                    Proposed Complexity *
                                                </label>
                                            </div>
                                            @foreach($proposed_complexities as $key=>$proposed_complexity)
                                                @php
                                                    if(!empty($company_standard)){
                                                        if($proposed_complexity == $company_standard->proposed_complexity){
                                                            $checked = 'checked';
                                                        }else{
                                                            if($key === 1){
                                                                $checked = 'checked';
                                                            }else{
                                                                $checked = '';
                                                            }
                                                        }

                                                    }else{
                                                       if($key === 1){
                                                          $checked = 'checked';
                                                       }else{
                                                          $checked = '';
                                                       }
                                                    }

                                                @endphp

                                                <div class="col-md-4 floting">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="proposed_complexity"
                                                               value="{{ $proposed_complexity }}" {{ !empty($company_standard) ? ($proposed_complexity == $company_standard->proposed_complexity ? 'checked' : '') : '' }}> {{ ucfirst($proposed_complexity) }}
                                                    </label>
                                                </div>
                                            @endforeach
                                            <span id="propsoedComplexity{{$standard->id}}" style="display: none">Proposed Complexity is Required for this Standard</span>
                                        </div>
                                        <div class="col-md-6 pull-left">
                                            <div class="col-md-12">
                                                <label>
                                                    Complexity Remarks
                                                </label>
                                            </div>
                                            <div class="col-md-12">
<textarea class="form-control"
          name="general_remarks"
          rows="3">{{ $company_standard->general_remarks ?? '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <h4>Accreditations & IAF Codes *</h4>

                                    <div id="auditor-standard-codes{{$standard->id}}"
                                         data-standard_id="{{$standard->id}}"
                                         data-standard-family-id="{{$standard->standards_family_id}}"
                                         class="auditor-standard-codes{{$standard->id}}">

                                    </div>

                                </div>


                                @if($standard->standards_family_id == 23)
                                    <div class="col-md-12">
                                        <h4>Accreditations & Food Codes*</h4>

                                        <div id="auditor-standard-food-codes{{$standard->id}}"
                                             data-standard_id="{{$standard->id}}"
                                             data-standard-family-id="{{$standard->standards_family_id}}"
                                             class="auditor-standard-food-codes{{$standard->id}}">

                                        </div>

                                    </div>
                                @endif

                                @if($standard->standards_family_id == 20)
                                    <div class="col-md-12">
                                        <h4>Accreditations & Energy Codes*</h4>

                                        <div id="auditor-standard-energy-codes{{$standard->id}}"
                                             class="auditor-standard-energy-codes{{$standard->id}}"
                                             data-standard-family-id="{{$standard->standards_family_id}}"
                                             data-standard_id="{{$standard->id}}"
                                        >

                                        </div>

                                    </div>
                                @endif
                            </div>


                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <div class="form-group">
                                        <label>Surveillance Frequency *</label>
                                        <select class="form-control" name="surveillance_frequency">
                                            {{--                                                <option value="">Select Surveillance Frequency</option>--}}
                                            @foreach($surveillance_frequencies as $frequency)
                                                <option value="{{ $frequency }}"
                                                        @if(!empty($company_standard) )
                                                            @if($frequency == $company_standard->surveillance_frequency)
                                                                selected
                                                        @else
                                                        @endif
                                                        @else
                                                            @if($frequency == 'annual')
                                                                selected
                                                @else
                                                        @endif
                                                        @endif
                                                        {{--                                                            {{ !empty($company_standard) ? ($frequency == $company_standard->surveillance_frequency ? 'selected' : '') : '' }}--}}
                                                >
                                                    {{ ucfirst($frequency) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <label class="text-uppercase emp_lbl" style="font-weight: 300;"><span
                                                    class="text-lg blue_text" style="font-weight: 700;">Contract Price Per Standard</span></label>
                                    </div>
                                </div>
                                <div class="mrgn"></div>
                                <div class="col-md-12 floting">
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label>Currency *</label>
                                            <select class="form-control" name="currency_unit">
                                                <option>Select Currency</option>
                                                @foreach($currencies as $code => $name)
                                                    @if(empty($code))
                                                        @continue
                                                    @endif
                                                    <option value="{{ $code }}" {{ !empty($company_standard) ? ($company_standard->initial_certification_currency == $code ? 'selected' : '') : '' }}>
                                                        {{ $code }}
                                                        | {{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label>Initial Certification Amount *</label>

                                            <input type="text" name="initial_certification_amount"
                                                   class="form-control" placeholder="Initial Certification Amount"
                                                   pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                   {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? 'disabled' : '') : '' }}
                                                   value="{{ $company_standard->initial_certification_amount ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label>Surveillance Amount *</label>
                                            <input type="text" name="surveillance_amount" class="form-control"
                                                   placeholder="Surveillance Amount"
                                                   pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                   value="{{ $company_standard->surveillance_amount ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label>Re-Audit Amount</label>
                                            <input type="text" name="re_audit_amount" class="form-control"
                                                   placeholder="Re-Audit Amount" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                   value="{{ $company_standard->re_audit_amount ?? '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--                            <button type="button" class="btn btn-default"--}}
                        {{--                                    onclick="closeStandardQuestionModal('{{$standard->id}}')">Close--}}
                        {{--                            </button>--}}
                        <button type="submit" class="btn btn-primary save_standard_questionnaire">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endforeach
<div class="card-body">
    <div class="tab-content">


        <div id="edit-auditor-standard-container">

        </div>


        <div id="edit-energy-auditor-standard-container">

        </div>

    </div>

</div>
<div class="modal fade customModal2" id="standards_for_documents" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="save_standard_documents" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Standard(s)</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <div class="popup_info">
                        <h4>Select Standard(s)</h4>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="multiselect">
                                <div class="selectBox">
                                    <select class="form-control" multiple=""></select>
                                    <div class="overSelect"></div>
                                </div>
                                <div id="checkboxes" class="selected_standards"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="standard_document_date">Date:</label>
                            <input type="text" class="form-control datefield" id="standard_document_date"
                                   name="date">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label style="color:red;">All Fields are required *</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="btn btn-default btn-file noMarginBtmBtn">
                                Select File(s) (10MB)<input type="file" id="documents" name="documents[]"
                                                            onchange="readURLMany(this,100)" class="documents">
                            </label>

                            <label>
                                <button type="submit" class="btn btn-success">Save</button>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <div class="image form-group" id="upld100">

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade priceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: auto">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" method="post" class="standard_prices"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Contract Price Per Standard</h4>
                </div>
                <div class="modal-body">
                    <div class="popup_info">

                        <input type="hidden" name="price_company_standard_id" id="price_company_standard_id">
                        <input type="hidden" name="price_client_type" id="price_client_type">
                        <div class="col-md-12">
                            <div class="col-md-12 floting">
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label>Currency *</label>
                                        <select class="form-control" name="price_currency_unit" id="price_currency_unit">
                                            <option>Select Currency</option>
                                            @foreach($currencies as $code => $name)
                                                @if(empty($code))
                                                    @continue
                                                @endif
                                                <option value="{{ $code }}">
                                                    {{ $code }}
                                                    | {{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label>Initial Certification Amount *</label>

                                        <input type="text" name="price_initial_certification_amount" id="price_initial_certification_amount"
                                               class="form-control" placeholder="Initial Certification Amount"
                                               pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                               value="">
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label>Surveillance Amount *</label>
                                        <input type="text" name="price_surveillance_amount" class="form-control" id="price_surveillance_amount"
                                               placeholder="Surveillance Amount"
                                               pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                               value="">
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label>Re-Audit Amount</label>
                                        <input type="text" name="price_re_audit_amount" class="form-control" id="price_re_audit_amount"
                                               placeholder="Re-Audit Amount" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                               value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger close_price_modal">Close</button>
                    <button type="button" class="btn btn-primary save_standard_prices">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@if(auth()->user()->roles[0]->name === "scheme manager")
    @foreach($company->companyStandards as $index=>$companyStandard)
        @if($company->auditJustifications->isNotEmpty())
            @foreach($company->auditJustifications as $key=>$audit_justification)
                @php
                    $standard_id_is =$audit_justification->ajStandards()->where('standard_id', $companyStandard->standard_id)->get();
foreach($standard_id_is as $aj_standard_stage_id){
$standard_stage_man_days = App\AJStandardStage::where('aj_standard_id',$aj_standard_stage_id->id)->get();
}
                @endphp
                @if($standard_id_is->count() >0)
                    @foreach($audit_justification->ajStandards()->where('standard_id', $companyStandard->standard_id)->get() as $index=>$aj_standard)

                        @foreach($aj_standard->ajStandardStages->groupBy('aj_standard_id') as  $index=>$standard_stage)
                            <div class="modal fade changeStatus" id="manDay_{{ $companyStandard->id }}"
                                 tabindex="-1"
                                 role="dialog" aria-labelledby="myModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="#" method="post">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="">Man days Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal"><span
                                                            aria-hidden="true">×</span><span
                                                            class="sr-only">Close</span></button>
                                            </div>
                                            <div class="modal-body">

                                                <div class="row">

                                                    <table class="table table-bordered text-center manday_tbl">
                                                        <tbody>
                                                        <tr>
                                                            <th>Audit Type</th>
                                                            <th>Onsite</th>
                                                            <th>Offsite</th>
                                                        </tr>
                                                        @foreach($standard_stage_man_days as $man_days)
                                                            @if(!preg_match('/[+]/', $man_days->getAuditType()) && !preg_match("/Transfer/", $man_days->getAuditType()))
                                                                <tr>
                                                                    <td>{{$man_days->getAuditType() }}</td>
                                                                    <td>{{$man_days->onsite}}</td>
                                                                    <td>{{$man_days->offsite}}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach


                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Close
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        @endforeach
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
@endif


<div id="certificate_html"></div>
<div id="original_issue_date_html"></div>
<div id="company_standard_remarks_html"></div>


