<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-food-codes{{$standard_id}}">
        <input type="hidden" name="company_standard_id" value="{{$standard_id}}" class="company_standard_id">
        <input type="hidden" name="company_id" value="{{$company_id}}" class="company_id">
        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Accreditation</th>
                <th>Food Code</th>
                <th>Description of Code</th>
                <th>Food Sub-Code</th>
                <th>Description of Sub-Code</th>

                <th width="10%" class="action-column">Action</th>
            </tr>
            </thead>
            <tbody>


            @foreach($companyStandardFoodCodes as $key => $companyStandardFoodCode)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{!!$companyStandardFoodCode->accreditation->name !!}</td>
                    <td>{{$companyStandardFoodCode->foodcategory->code}}</td>
                    <td>{{$companyStandardFoodCode->foodcategory->name}}</td>
                    <td>{{$companyStandardFoodCode->foodsubcategory->code}}</td>
                    <td>{{$companyStandardFoodCode->foodsubcategory->name}}</td>
                    <td align="center" class="action-column-data">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="getEditFoodCodesModal({{$companyStandardFoodCode->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="deleteAuditorStandardFoodCode({{$companyStandardFoodCode->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>

        </table>

        <div class="col-md-12">
            <button type="button" class="btn btn-primary edit_btn pull-right"
                    data-toggle="modal" data-target="#addStandardFoodCodesModal{{$standard_id}}"
                    onclick="getAccreditationByStandardFoodCode('{{$standard_id}}','{{$company_id}}')">Add Food Code
            </button>
        </div>
        <div class="clearfix"></div>
    </form>
</div>

<div id="addStandardFoodCodesModal{{$standard_id}}" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="addStandardFoodCodesForm{{$standard_id}}">
                <input type="hidden" name="company_standard_id" value="{{$standard_id}}">
                <input type="hidden" name="company_id" value="{{$company_id}}">
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close" onclick="closeModelFood('{{$standard_id}}','add')">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Please add food codes for multiple accreditations one by one manually.</label>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Accreditations *</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food_accreditations" name="food_accreditation_id">
                                        <option value="">Select Accreditation</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Food Code *</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food-category" name="food_category_code">
                                        <option value="">Select Food Code</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Food Sub-Code *</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food-sub-category" name="food_sub_category_code">
                                        <option value="">Select Food Sub-Code</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary"
                                        onclick="addStandardFoodCodeSave({{$standard_id}})">Submit
                                </button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel"
                                        onclick="closeModelFood('{{$standard_id}}','add')">Cancel
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>


    function getAccreditationByStandardFoodCode(standard_id,companyId) {

        var form = $(this).closest('form');
        var node = $(this);
        var node_to_modify = '.food_accreditations';
        var request = {"standard_id": standardId, "company_id": companyId};

        if (standard_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.companyAccreditationByStandardIdCompanyId') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.accreditations, function (i, obj) {

                            html += '<option  value="' + obj.id + '">' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);

                        $(node_to_modify).prepend("<option value='' selected>Select Accreditation</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Accreditation</option>");
        }

    }

    $('.food_accreditations').change(function () {
        var form = $(this).closest('form');
        var node = $(this);
        var node_to_modify = '.food-category';
        var accreditation_id = $(this).val();
        var request = "accreditation_id=" + accreditation_id;

        if (accreditation_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.foodCategoryByAccreditation') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.food_category, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        $(node_to_modify).prepend("<option value='' selected>Select Food Code</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Code</option>");
        }

    });
    $('.food-category').change(function () {
        var form = $(this).closest('form');
        var node = $(this);
        var node_to_modify = '.food-sub-category';
        var food_category = $(this).val();
        var request = "food_category_id=" + food_category;

        if (food_category !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.foodSubCategoryByFoodCategory') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.foodsubcategories, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        // sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>Select Food Sub-Code</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Food Sub-Code</option>");
        }

    });

    function addStandardFoodCodeSave(standardId) {

        var form = $("#addStandardFoodCodesForm" + standardId)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('company.standards.food.code.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                if (response.status === 'exist') {
                    toastr['error']("Food Code Already exist");
                } else {
                    ajaxResponseHandler(response, form);

                    if (response.status == "success" || response.status == '200') {
                        $("#addStandardFoodCodesModal" + response.data.standard_id).modal('hide');
                        setTimeout(function () {
                            var standard_id = $('.company_standard_id').val();
                            var company_id = $('.company_id').val();
                            companyStandardFoodCodesView(response.data.standard_id, response.data.company_id);
                        }, 1000);
                    }
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function deleteAuditorStandardFoodCode(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('company.standards.food.code.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        var standard_id = $('.company_standard_id').val();
                        var company_id = $('.company_id').val();
                        companyStandardFoodCodesView(response.data.standard_id, response.data.company_id);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditFoodCodesModal(modelId) {
        var url = "{{ route('company.standards.food.code.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                $("#edit-auditor-standard-container").html(response);
                $("#editCompanyStandardFoodCodesModal").modal('show');


            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function closeModelFood(standardId, type) {
        if (type === 'add') {
            $("#addStandardFoodCodesModal" + standardId).modal('hide');
        } else if (type === 'edit') {
            $("#editCompanyStandardFoodCodesModal").modal('hide');
        }
    }


</script>
