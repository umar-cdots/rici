<div id="editCompanyStandardFoodCodesModal" class="modal fade in codesModal" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="editStandardFoodCodesForm">
                <input type="hidden" name="company_standard_food_code_id" value="{{$companyStandardFoodCodes->id}}">
                <input type="hidden" name="company_standard_id" value="{{$companyStandardFoodCodes->standard_id}}">
                <input type="hidden" name="company_id" value="{{$companyStandardFoodCodes->company_id}}">
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close"
                            onclick="closeModelFoodEdit('{{$companyStandardFoodCodes->standard_id}}','edit')">×
                    </button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Accreditations</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food_accreditations" name="food_accreditation_id">
                                        <option value="">Select Accreditation</option>
                                        @foreach($allAccreditations as $accreditations)
                                            <option value="{{$accreditations->id}}" {{$accreditations->id ==$companyStandardFoodCodes->accreditation_id ? 'selected': ''}}>{{ $accreditations->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Food Code</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food-category" name="food_category_code">
                                        <option value="">Select Food Code</option>
                                        @foreach($foodCategories as $foodCategory)
                                            <option value="{{$foodCategory->id}}" {{($foodCategory->id == $companyStandardFoodCodes->food_category_id) ? 'selected': ''}}>
                                                {{$foodCategory->code}} | {{$foodCategory->name}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row " style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Food Sub-Code</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food-sub-category" name="food_sub_category_code">
                                        @foreach($foodSubCategories as $foodSubCategory)
                                            <option value="{{$foodSubCategory->id}}" {{($foodSubCategory->id == $companyStandardFoodCodes->food_sub_category_id) ? 'selected': ''}}>
                                                {{$foodSubCategory->code}} | {{$foodSubCategory->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary updateDataBtnFoodCode">Submit</button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel"
                                        onclick="closeModelFoodEdit('{{$companyStandardFoodCodes->standard_id}}','edit')">
                                    Cancel
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    $('.updateDataBtnFoodCode').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardFoodCodesForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('company.standards.food.code.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {

                    var standard_id = $('.company_standard_id').val();
                    var company_id = $('.company_id').val();
                    companyStandardFoodCodesView(response.data.standard_id, response.data.company_id);
                    $("#editCompanyStandardFoodCodesModal").modal('hide');
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    function closeModelFoodEdit(standardId, type) {
        $("#editCompanyStandardFoodCodesModal").modal('hide');

    }


</script>