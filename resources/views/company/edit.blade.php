@extends('layouts.master')
@section('title', "Edit Company")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">@if(!empty($company->name))
                                {{ $company->name }}
                            @else @endif
                            Company <span>Profile</span></h1>
                        <h6 class="text-center dashboard_heading2">Edit Company
                            Profile{{--  - if you want to learn how to follow these steps - <a href="#">Click Here</a> --}}</h6>
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content dashboard_tabs">
            <div class="container-fluid">
                <div class="col-md-10 floting">
                    <div class="progress">
                        <div class="progress-bar bg-primary progress-bar-striped barColor" role="progressbar"
                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                             style="width: 0%"
                             id="progressBar">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 text-center floting barText">
                    <h5 id="progressValue">0%</h5>
                </div>
                <div class="clearfix"></div>
                <ul class="nav nav-tabs nav-tabs1 mrgn company-steps-nav">
                    <div class="col-md-2">
                        <li class="active pro_working">STEP 01
                            <a class="active show" data-toggle="tab" href="#stepTab1">
                                <i class="nav-icon fa fa-building-o"></i>
                            </a> Company Details
                        </li>
                    </div>
                    {{--                    <div class="col-md-2">--}}
                    {{--                        <li class="nonactive pro_pending" data-back-id="stepTab1">STEP 02--}}
                    {{--                            <a data-toggle="tab" href="#stepTab2">--}}
                    {{--                                <i class="nav-icon fa fa-file-text"></i>--}}
                    {{--                            </a>Scope & Technical Code Detail--}}
                    {{--                        </li>--}}
                    {{--                    </div>--}}
                    <div class="col-md-2">
                        <li class="nonactive pro_pending" data-back-id="stepTab1">STEP 02
                            <a data-toggle="tab" href="#stepTab3">
                                <i class="fa fa-id-card-o" aria-hidden="true"></i>
                            </a>Contact Person <br> Detail
                        </li>
                    </div>
                    <div class="col-md-2">
                        <li class="nonactive pro_pending" data-back-id="stepTab3">STEP 03
                            <a data-toggle="tab" href="#stepTab4">
                                <i class="fa fa-certificate" aria-hidden="true"></i>
                            </a>Standards
                        </li>
                    </div>
                    <div class="col-md-2">
                        <li class="nonactive pro_pending" data-back-id="stepTab4">STEP 04
                            <a data-toggle="tab" href="#stepTab5"><i class="fa fa-paperclip" aria-hidden="true"></i>
                            </a>File Upload
                        </li>
                    </div>
                    <div class="col-md-2">
                        <li class="nonactive pro_pending" data-back-id="stepTab5">STEP 05
                            <a data-toggle="tab" href="#stepTab6"><i class="fa fa-briefcase" aria-hidden="true"></i>
                            </a>View Form
                        </li>
                    </div>
                </ul>

                <div class="tab-content">
                    <div id="stepTab1" class="company-step tab-pane fade in active show">
                        <div class="row">
                            <form action="#" id="step1" method="post" autocomplete="off">
                                <input type="hidden" name="step" value="1">
                                <input type="hidden" name="company_id" id="company_id" value="{{$company->id}}">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="name">Company Name *</label>
                                            <input type="text" class="form-control" placeholder="Enter Company Name"
                                                   id="name" name="name" required="" value="{{ $company->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="region_id">Region *</label>
                                            <select class="form-control" id="region_id" name="region_id" required="">
                                                <option value="">Select Region</option>
                                                @foreach($regions as $region)
                                                    <option value="{{ $region->id }}" {{ $region->id == $company->region_id ? 'selected' : '' }}>
                                                        {{ $region->title }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">

                                        <div class="form-group">
                                            <label for="country_id">Country *</label>
                                            <select class="form-control countries" name="country_id" id="country_id">
                                                <option value="">Select Country</option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->country->id }}" {{ $country->country->id == $company->country_id ? 'selected' : '' }}>
                                                        {{ $country->country->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>sister company <span style="font-size: 12px;">(if yes check the box and enter name)</span></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
						                        <span class="input-group-text">
						                          <input type="checkbox" id="enable_sister_company_name" {{ $company->sister_company_name === ' ' ? '' : 'checked' }}>
						                        </span>
                                                </div>
                                                <input type="text" class="form-control" id="sister_company_name"
                                                       name="sister_company_name"
                                                       placeholder="Enter Sister Company Name"
                                                       {{ $company->sister_company_name === ' ' ? 'disabled' : '' }} value="{{ $company->sister_company_name }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label class="website">Website</label>
                                            <input type="text" class="form-control" id="website" name="website"
                                                   placeholder="Enter Website" value="{{ $company->website }}">
                                        </div>
                                    </div>
                                    @if(auth()->user()->user_type === 'scheme_manager')
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <label class="multiple_sides">Total Sites (Manual)</label>
                                                <input type="number" class="form-control" id="multiple_sides"
                                                       name="multiple_sides" value="{{ $company->multiple_sides ?? 1 }}"
                                                       placeholder="Enter Total Sites">
                                            </div>
                                        </div>
                                    @else
                                        <input type="hidden" class="form-control" id="multiple_sides"
                                               name="multiple_sides" value="{{ $company->multiple_sides ?? 1 }}"
                                               placeholder="Enter Total Sites">
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="address">HO / Main Address *</label>
                                            <input type="text" class="form-control" placeholder="Enter Company Address"
                                                   id="address" name="address" required=""
                                                   value="{{ $company->address }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="key_process">Key Process *</label>
                                            <input type="text" class="form-control" id="key_process" name="key_process"
                                                   required="" placeholder="Enter Key Process"
                                                   value="{{ $company->key_process }}">
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6 floting"> --}}
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="city_id">City *</label>
                                            <select class="form-control cities" id="city_id" name="city_id" required="">
                                                <option value="">Select City</option>
                                                @foreach($cities as $city)
                                                    <option value="{{ $city->id }}" {{ $city->id == $company->city_id ? 'selected' : '' }}>
                                                        {{ $city->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label class="website">Website</label>
                                            <input type="text" class="form-control" id="website" name="website"  placeholder="Enter Website">
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="total_employees">No. of Employees *</label>
                                            <input type="number" class="form-control total_employees"
                                                   id="total_employees" name="total_employees" required=""
                                                   placeholder="Enter Employees" min="1"
                                                   value="{{ $company->total_employees }}">
                                        </div>
                                    </div>
                                    {{-- </div> --}}
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    @if(auth()->user()->user_type == 'scheme_manager')
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <label>Out of Region Country Case <span style="font-size: 12px;">(if yes check the box and enter name)</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
						                        <span class="input-group-text">
						                          <input type="checkbox" id="enable_out_of_country_region_name" {{ $company->out_of_country_region_name === '' || is_null($company->out_of_country_region_name) ? '' : 'checked' }}>
						                        </span>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           id="out_of_country_region_name"
                                                           name="out_of_country_region_name"
                                                           placeholder="Enter Out of Country Region Name"
                                                           {{ $company->out_of_country_region_name === '' || is_null($company->out_of_country_region_name) ? 'disabled' : '' }} value="{{ $company->out_of_country_region_name }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <label for="city_id">Confidential Certified Entity *</label>
                                                <select class="form-control" id="confidential_certified_entity"
                                                        name="confidential_certified_entity" required="">
                                                    <option value="No" {{$company->confidential_certified_entity === 'No' ? 'selected' : '' }}>
                                                        No
                                                    </option>
                                                    <option value="Yes" {{$company->confidential_certified_entity === 'Yes' ? 'selected' : '' }}>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    @else
                                        <input type="hidden" name="out_of_country_region_name"
                                               value="{{ $company->out_of_country_region_name }}">
                                        <input type="hidden" name="confidential_certified_entity"
                                               value="{{ $company->confidential_certified_entity ?? 'No' }}">
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                                <div class="border_div"></div>
                                <div class="col-md-12">
                                    <div class="text-left">
                                        <label class="text-uppercase emp_lbl">Total Locations - <span class="text-lg"
                                                                                                      style="color: #fc5b00; font-weight: 700;"><span
                                                        id="total_locations_count">{{ 1 + $company->childCompanies->count() }}</span> Locations</span></label>
                                    </div>
                                </div>


                                <div class="address_main_div_new" id="address_main_div_new" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label class="address_count">Address *</label>
                                                <input type="text" class="form-control"
                                                       name="child_company[address][]" placeholder="Enter Address"
                                                       disabled="" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Key Process *</label>
                                                <input type="text" class="form-control"
                                                       name="child_company[key_process][]"
                                                       placeholder="Enter Key Process" disabled="" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Country *</label>
                                                <select class="form-control countries_child"
                                                        data-index=""
                                                        name="child_company[country_id][]"
                                                        disabled="">
                                                    <option value="">Select Country</option>
                                                    @if(!empty($allCountries) && count($allCountries) >0)
                                                        @foreach($allCountries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>City *</label>
                                                <select class="form-control cities_child"
                                                        name="child_company[city_id][]"
                                                        data-index=""
                                                        disabled="" required="">
                                                    <option value="">Select City</option>
                                                    {{--                                                    @foreach($cities as $city)--}}
                                                    {{--                                                        <option value="{{ $city->id }}">--}}
                                                    {{--                                                            {{ $city->name }}--}}
                                                    {{--                                                        </option>--}}
                                                    {{--                                                    @endforeach--}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>No. of Employees *</label>
                                                <input type="number" class="form-control total_employees"
                                                       name="child_company[total_employees][]"
                                                       placeholder="Enter Employees" disabled="" required=""
                                                       min="1">
                                            </div>
                                        </div>
                                        <div class="col-md-1 floting" style="margin-top: 24px;">
                                            <button type="button" class="btn btn-danger remove_other_location"
                                                    style="float: right;" title="Remove"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="address_add_div_new">
                                    @forelse($company->childCompanies as $key=>$child_company)
                                        <div class="address_main_div_new">
                                            <div class="col-md-12">
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Address *</label>
                                                        <input type="text" class="form-control"
                                                               name="child_company[address][]"
                                                               placeholder="Enter Address"
                                                               value="{{ $child_company->address }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>Key Process *</label>
                                                        <input type="text" class="form-control"
                                                               name="child_company[key_process][]"
                                                               placeholder="Enter Key Process"
                                                               value="{{ $child_company->key_process }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>Country *</label>
                                                        <select class="form-control countries_child"
                                                                data-index="{{$key+1}}"
                                                                name="child_company[country_id][]"
                                                                required="">
                                                            <option value="">Select Country</option>
                                                            @if(!empty($allCountries) && count($allCountries) >0)
                                                                @foreach($allCountries as $country)
                                                                    <option value="{{$country->id}}" {{ $country->id == $child_company->country_id ? 'selected' : '' }}>{{$country->name}}</option>
                                                                @endforeach
                                                            @endif

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>City *</label>
                                                        <select class="form-control cities_child cities_child{{$key+1}}"
                                                                data-index={{$key+1}}""
                                                                name="child_company[city_id][]" required="">
                                                            <option value="">Select City</option>
                                                            @foreach($allCities as $city)
                                                                <option value="{{ $city->id }}" {{ $city->id == $child_company->city_id ? 'selected' : '' }}>
                                                                    {{ $city->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>No. of Employees *</label>
                                                        <input type="number" class="form-control total_employees"
                                                               name="child_company[total_employees][]"
                                                               placeholder="Enter Employees"
                                                               value="{{ $child_company->total_employees }}" required=""
                                                               min="1">
                                                    </div>
                                                </div>
                                                <div class="col-md-1 floting" style="margin-top: 24px;">
                                                    <button type="button" class="btn btn-danger remove_other_location"
                                                            style="float: right;" title="Remove"><i
                                                                class="fa fa-minus"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    @empty
                                    @endforelse
                                </div>
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <a id="add_address">
                                            <label class="text-capitalize">
                                                Add More Locations
                                                <i class="fa fa-plus-circle text-lg" aria-hidden="true"></i>
                                            </label>
                                        </a>
                                    </div>
                                </div>
                                <div class="border_div"></div>
                                <div class="col-md-12">
                                    <div class="text-left">
                                        <label class="text-uppercase emp_lbl">Total Employees - <span class="text-lg"
                                                                                                      style="color: #fc5b00; font-weight: 700;"><span
                                                        id="total_employees_count">{{ $company->total_employees + $company->childCompanies->sum('total_employees') }}</span> Employees</span></label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="permanent_employees">Permanent Employees *</label>
                                            <input type="number" class="form-control" id="permanent_employees"
                                                   name="permanent_employees" placeholder="Enter Employees" min="0"
                                                   readonly
                                                   value="{{ $company->permanent_employees }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="part_time_employees">Part Time Employees *</label>
                                            <input type="number" class="form-control" id="part_time_employees"
                                                   name="part_time_employees" placeholder="Enter Employees" min="0"
                                                   value="{{ $company->part_time_employees }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="repeatative_employees">Similar / repeatative tasks *</label>
                                            <input type="number" class="form-control" id="repeatative_employees"
                                                   name="repeatative_employees" placeholder="Enter Tasks" min="0"
                                                   value="{{ $company->repeatative_employees }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label for="shift">Shifts *</label>
                                            <select class="form-control" id="shift" name="shift">
                                                <option value="">Select Shifts</option>
                                                @foreach($shifts as $key=>$shift)
                                                    <option value="{{ $shift }}"
                                                            @if($shift == $company->shift)
                                                                selected
                                                            @elseif($key === 0)
                                                                selected
                                                            @endif
                                                    >
                                                        {{ ucfirst($shift) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-left">
                                        <label class="text-uppercase emp_lbl">Effective Employees <span
                                                    class="text-lg"
                                                    style="color: #fc5b00; font-weight: 700; display: none"><span
                                                        id="effective_employees_count">- {{ $company->effective_employees }}</span> Employees</span></label>
                                        <br>
                                        <input type="number" name="effective_employees" class="form-control"
                                               id="calculate_employees"
                                               @if(auth()->user()->user_type !== 'scheme_manager') readonly @endif
                                               value="{{ $company->effective_employees }}">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Shift Remarks</label>
                                        <textarea name="shift_remarks" id="shift_remarks"
                                                  class="form-control">{{ $company->shift_remarks ?? 'N/A' }}</textarea>
                                    </div>
                                </div>
                                <div class="border_div"></div>

                                <div class="col-md-12 ">
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Scope to be Certified *</label>
                                            <textarea name="scope_to_certify" id="scope_to_certify" class="form-control"
                                                      required>{{ $company->scope_to_certify ?? '' }} </textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Any processes are outsourced ?</label>&nbsp;&nbsp;&nbsp;(
                                            <input type="checkbox" name="enable_outsourced_process_details"
                                                   id="enable_outsourced_process_details" value="1"
                                                   @if(!empty($company->outsourced_process_details)) checked @endif>&nbsp;&nbsp;<label>Yes</label>)
                                            <textarea name="outsourced_process_details" class="form-control"
                                                      @if(!empty($company->outsourced_process_details)) @else disabled
                                                      @endif
                                                      id="outsourced_process_details">{{$company->outsourced_process_details}}</textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12" style="margin-top: -10px;">
                                    <div class="col-md-12 floting">
                                        <div class="form-group">
                                            <label for="co_general_remarks">General Remarks</label>
                                            <textarea class="form-control" id="co_general_remarks"
                                                      name="general_remarks" rows="5"
                                                      placeholder="Enter General Remarks">{{ $company->general_remarks }}</textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success btn_save"
                                                    id="save_step_1">Save &
                                                Next
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-block btn-secondary btn_cncl">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="stepTab3" class="company-step tab-pane fade in">
                        <div class="row">
                            <form action="#" id="step3" method="post" autocomplete="off">
                                <input type="hidden" name="step" value="3">
                                <div class="col-md-12 main_contact_form">


                                    @php
                                        $primaryIndex = ''
                                    @endphp
                                    @if($company->contacts->count() >0)
                                        <div id="education_fields">
                                            @foreach($company->contacts as $key=>$c_contact)

                                                @if($c_contact->type === 'primary')
                                                    @php
                                                        $primaryIndex = $key;
                                                    @endphp
                                                @endif
                                                <div class="form-group removeclass{{$key}}">
                                                    <div class="col-md-12">
                                                        <div class="col-md-1 floting">
                                                            <label>Primary *</label>


                                                            <div class="form-group">
                                                                <input type="radio" class="form-control primary"
                                                                       onclick="primaryIndex('{{$key}}')"
                                                                       name="primary"
                                                                       value="{{ $c_contact->type === 'primary' ? 1 : 0 }}"
                                                                       required=""
                                                                       @if($c_contact->type === 'primary') checked @endif>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 floting">
                                                            <div class="form-group">
                                                                <label>Title *</label>
                                                                <select class="form-control" name="title[]">
                                                                    @foreach($contact_titles as $title)
                                                                        <option value="{{ $title }}"
                                                                                @if($c_contact->title === $title) selected @endif>
                                                                            {{$title }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 floting">
                                                            <div class="form-group">
                                                                <label>Name *</label>
                                                                <input type="text" name="name[]" class="form-control"
                                                                       placeholder="Enter Name"
                                                                       value="{{ $c_contact->name }}"
                                                                       required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 floting">
                                                            <div class="form-group">
                                                                <label>Position *</label>
                                                                <input type="text" name="position[]"
                                                                       class="form-control"
                                                                       placeholder="Enter Position"
                                                                       value="{{ $c_contact->position }}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 floting">
                                                            <div class="form-group">
                                                                <label>Cell Number *</label>
                                                                <input type="tel" name="contact[]" class="form-control"
                                                                       value="{{ $c_contact->contact }}" required
                                                                       placeholder="Enter Contact Number">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 floting">
                                                            <div class="form-group">
                                                                <label>Landline</label>
                                                                <input type="tel" name="landline[]" class="form-control"
                                                                       placeholder="Enter Contact Number"
                                                                       value="{{ $c_contact->landline }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 floting">
                                                            <div class="form-group">
                                                                <label>Email</label>
                                                                <input type="email" name="email[]" class="form-control"
                                                                       placeholder="Enter Email"
                                                                       value="{{ $c_contact->email }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 floting">
                                                            <div class="form-group add_remove">
                                                                <br>
                                                                <button type="button"
                                                                        class="btn btn-danger removeBtnCompany"
                                                                        onclick="remove_main_contact({{$key}});"
                                                                        style="margin-top: 4px;">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                </div>

                                            @endforeach
                                        </div>
                                        <div class="col-md-12">

                                            <div class="col-md-1 floting">
                                                <div class="form-group add_remove">
                                                    <br>
                                                    <button type="button" class="btn btn-sm btn-primary "
                                                            style="margin-top: 4px;" onclick="add_main_contact();">
                                                        <i class="fa fa-plus"></i> Add More
                                                    </button>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="clearfix"></div>
                                    @else
                                        <div class="col-md-12">
                                            <div id="education_fields">
                                            </div>
                                            <div class="col-md-1 floting">
                                                <div class="form-group add_remove">
                                                    <br>
                                                    <button type="button" class="btn btn-sm btn-primary "
                                                            style="margin-top: 4px;" onclick="add_main_contact();">
                                                        <i class="fa fa-plus"></i> Add More
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    @endif
                                    <input type="hidden" name="primary_index" id="primary_index"
                                           value="{{$primaryIndex}}">
                                </div>

                                <div class="col-md-12 mrgn">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-6 floting">
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn-success btn_back"
                                                        data-back-id="stepTab1">Back
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-success btn_save">Save &
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn-secondary btn_cncl">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="stepTab4" class="company-step tab-pane fade in">
                        <div class="row">
                            <form action="#" id="step4" method="post" autocomplete="off">
                                <input type="hidden" name="step" value="4">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <label class="text-uppercase emp_lbl" style="font-weight: 300;">Add <span
                                                    class="text-lg" style="font-weight: 700;">Standards</span></label>
                                        <h6 class="text-center dashboard_heading2">Add Each Standard Seperately as each
                                            may have any specific information</h6>
                                    </div>
                                </div>
                                @php
                                    $standards = [];
                                @endphp
                                @foreach($standards_families as $standards_family)
                                    <div class="card-header">
                                        <h3 class="card-title">{{ $standards_family->name }}</h3>
                                    </div>
                                    @forelse($standards_family->standards->chunk(6) as $chunk)
                                        <div class="col-md-12 mrgn text-center">
                                            @foreach($chunk as $standard)
                                                @php
                                                    $standards[] 	= $standard;
                                                    $auditJustification = \App\AuditJustification::where('company_id',$company->id)->whereHas('ajStandards',function($q) use ($standard){
                                                                    $q->where('standard_id',$standard->id)->whereNull('deleted_at')->whereHas('ajStandardStages',function($query){
                                                                        $query->where('audit_type','stage_1')->where('status','approved');
                                                                    });
                                                                })->first();

                                                     if(is_null($auditJustification)){
                                                         $standardDiv= '';
                                                     }else{
                                                         $standardDiv= 'disabledDivStandard';
                                                     }
                                                @endphp
                                                <div class="col-md-2 floting standard_selection"
                                                     data-standard-number="{{ $standard->number }}">
                                                    <input type="checkbox" name="standard_ids[]"
                                                           value="{{ $standard->id }}"
                                                           class="standard_check {{ $standardDiv }}"
                                                           data-modal-id="standard_question_{{ $standard->id }}"
                                                           data-is-ims-enabled="{{ $standards_family->is_ims_enabled }}"
                                                           data-standard-id="{{ $standard->id }}"
                                                           @foreach($company->companyStandards as $companyStandard)
                                                               @if($companyStandard->standard_id === $standard->id)
                                                                   checked="checked"
                                                    @else
                                                            @endif
                                                            @endforeach
                                                    >
                                                    <label title="{{ $standard->name }}">{{ $standard->name }}
                                                        @foreach($company->companyStandards as $companyStandard)
                                                            @if($companyStandard->standard_id === $standard->id)
                                                                <i style="">
                                                                    <a href="" class="edit_standard"
                                                                       id="edit_standard{{$standard->id}}">
                                                                        <small>Edit</small>
                                                                    </a>
                                                                </i>
                                                            @else

                                                            @endif
                                                        @endforeach
                                                        <i style="display: none;"
                                                           id="edit_standard_button_check{{$standard->id}}">
                                                            <a href="" class="edit_standard">
                                                                <small>Edit</small>
                                                            </a>
                                                        </i>
                                                    </label>
                                                </div>
                                            @endforeach
                                            <div class="clearfix"></div>
                                        </div>
                                    @empty
                                        <div class="col-md-12">
                                            <p>No Standard Got Added Yet.</p>
                                        </div>
                                    @endforelse
                                @endforeach
                                {{--                                <div class="col-md-12">--}}
                                {{--                                    <div class="col-md-12">--}}
                                {{--                                        <div class="text-center">--}}
                                {{--                                            <label class="text-uppercase emp_lbl" style="font-weight: 300;"><span--}}
                                {{--                                                        class="text-lg blue_text" style="font-weight: 700;">IMS Standards</span></label>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="mrgn"></div>--}}
                                {{--                                    <div id="ims_standards_list" class="col-md-12 mrgn text-center">--}}
                                {{--                                        @php--}}
                                {{--                                            $selected_ims_standards =\App\CompanyStandards::where('company_id',$company->id)->where('is_ims',1)->get();--}}

                                {{--                                        @endphp--}}
                                {{--                                        @forelse($selected_ims_standards as $ims_standard)--}}
                                {{--                                            <div class="col-md-2 floting">--}}
                                {{--                                                <input type="checkbox" name="ims_standard_ids[]"--}}
                                {{--                                                       value="{{$ims_standard->standard->id}}"--}}
                                {{--                                                       class="ims_standard_check" checked="">&nbsp;--}}
                                {{--                                                <label>{{ $ims_standard->standard->name }}</label>--}}
                                {{--                                            </div>--}}


                                {{--                                        @empty--}}
                                {{--                                            <p>No IMS Standard Got Selected Yet.</p>--}}
                                {{--                                        @endforelse--}}


                                {{--                                    </div>--}}
                                {{--                                    <div id="unchecked_ims_standards_list" class="col-md-12"--}}
                                {{--                                         style="display: @if($selected_ims_standards->count() >0 ) block @else none  @endif">--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="mrgn"></div>--}}
                                {{--                                    <div id="ims_default_values" class="col-md-12"--}}
                                {{--                                         style="display: @if($selected_ims_standards->count() >0 ) block @else none  @endif ;padding-top: 10px;">--}}
                                {{--                                        <div class="col-md-5 floting">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>IMS Survellance Frequency *</label>--}}
                                {{--                                                <select class="form-control" name="default_ims_surveillance_frequency">--}}
                                {{--                                                    <option value="">Select Surveillance Frequency</option>--}}
                                {{--                                                    @foreach($surveillance_frequencies as $frequency)--}}
                                {{--                                                        <option value="{{ $frequency }}" @if($selected_ims_standards->count() > 0){{ ($selected_ims_standards[0]->surveillance_frequency == $frequency) ? 'selected' : '' }} @endif>{{ ucfirst($frequency) }}</option>--}}
                                {{--                                                    @endforeach--}}
                                {{--                                                </select>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}
                                {{--                                        @php--}}
                                {{--                                            use Illuminate\Support\Facades\DB;--}}
                                {{--                                            if(!empty($selected_ims_standards->count() > 0)){--}}
                                {{--                                                $selected_accreditations = DB::table('company_standards_accreditations')->where('company_standard_id',$selected_ims_standards[0]->id)->get();--}}
                                {{--                                            }--}}


                                {{--                                        @endphp--}}
                                {{--                                        <div class="col-md-7 floting">--}}
                                {{--                                            <label>IMS Accreditation</label>--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                @foreach($accreditations->chunk(4) as $accreditation_chunk)--}}
                                {{--                                                    <div class="row">--}}
                                {{--                                                        @foreach($accreditation_chunk as $accreditation)--}}
                                {{--                                                            <div class="col-md-3 floting">--}}
                                {{--                                                                <input type="checkbox"--}}
                                {{--                                                                       name="default_ims_accreditation_ids[]"--}}
                                {{--                                                                       value="{{ $accreditation->id }}" @if(!empty($selected_accreditations))@foreach($selected_accreditations as $data){{ ($accreditation->id == $data->accreditation_id) ? 'checked' : '' }}@endforeach @endif>--}}
                                {{--                                                                <label title="{{ $accreditation->full_name }}">{{ $accreditation->name }}</label>--}}
                                {{--                                                            </div>--}}
                                {{--                                                        @endforeach--}}
                                {{--                                                    </div>--}}
                                {{--                                                @endforeach--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="clearfix"></div>--}}
                                {{--                                </div>--}}
                                <div class="border_div"></div>
                                <div class="col-md-12">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-6 floting">
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn-success btn_back"
                                                        data-back-id="stepTab3">Back
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-success btn_save">Save
                                                    &amp; Next
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn-secondary btn_cncl">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="stepTab5" class="company-step tab-pane fade in">
                        <div class="row">
                            <form action="#" id="step5" method="post" autocomplete="off">
                                <input type="hidden" name="step" value="5">
                                <div class="col-md-12 text-center">
                                    <label class="text-uppercase emp_lbl" style="font-weight: 300;">Upload
                                        Documents</label>
                                </div>
                                <div class="mrgn"></div>
                                <div class="col-md-12 text-center">
                                    <div class="col-md-3 uploadDocs floting">
                                        <label>Initial Inquiry Form</label>
                                        <a href="#" class="open_standards_modal"
                                           data-document-type="initial_inquiry_form"><i class="fa fa-plus-circle"
                                                                                        aria-hidden="true"></i></a>
                                        <div class="uploaded_documents"></div>
                                    </div>
                                    <div class="col-md-3 uploadDocs floting">
                                        <label>Proposal</label>
                                        <a href="#" class="open_standards_modal" data-document-type="proposal"><i
                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <div class="uploaded_documents"></div>
                                    </div>
                                    <div class="col-md-3 uploadDocs floting">
                                        <label>Contract</label>
                                        <a href="#" class="open_standards_modal" data-document-type="contract"><i
                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <div class="uploaded_documents"></div>
                                    </div>
                                    <div class="col-md-3 uploadDocs floting">
                                        <label>Other Documents</label>
                                        <a href="#" class="open_standards_modal" data-document-type="other"><i
                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <div class="uploaded_documents"></div>
                                    </div>
                                </div>
                                <div class="mrgn downloadNav row">
                                    <ul class="nav nav-pills">
                                        <li class="nav-item initial_inquiry_form">
                                            <a class="nav-link active show" href="#iif_tab" data-toggle="tab">Initial
                                                Inquiry Forms</a>
                                        </li>
                                        <li class="nav-item proposal">
                                            <a class="nav-link" href="#ppl_tab" data-toggle="tab">Proposal</a>
                                        </li>
                                        <li class="nav-item contract">
                                            <a class="nav-link" href="#ctt_tab" data-toggle="tab">Contract</a></li>
                                        <li class="nav-item other">
                                            <a class="nav-link" href="#ods_tab" data-toggle="tab">Other Documents</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">

                                        <div class="tab-pane active show initial_inquiry_form" id="iif_tab">
                                            <h2>Initial Inquiry Forms:</h2>
                                            <div class="row">
                                                <div class="col-md-12 ">

                                                    <table class="table">
                                                        <tr>
                                                            <th></th>
                                                            <th>Date</th>
                                                            <th>Standard</th>
                                                            <th>File</th>
                                                            <th>Action</th>

                                                        </tr>
                                                        @if(!empty($standard_documents) && count($standard_documents) >0)
                                                            @foreach ($standard_documents as $inqueryDocuemts)
                                                                @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                    @foreach ($inqueryDocuemts as $index => $data)
                                                                        @if($data->document_type === 'initial_inquiry_form')
                                                                            @php
                                                                                $initial_inquiry_form_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                            @endphp
                                                                            <tr id="#media_docs{{$initial_inquiry_form_media->id}}">

                                                                                <td>
                                                                                    <input type="hidden"
                                                                                           name="company_media_id"
                                                                                           value="{{ $initial_inquiry_form_media->id}}"
                                                                                           id="company_media_id"/>
                                                                                </td>
                                                                                <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                <td>
                                                                                    <a href="{{  asset('storage/'.$initial_inquiry_form_media->id.'/'.$initial_inquiry_form_media->file_name) }}"
                                                                                       target="_blank"
                                                                                       title="{{ $initial_inquiry_form_media->file_name }}">{{ $initial_inquiry_form_media->file_name }}</a>
                                                                                </td>
                                                                                <td>
                                                                                    @can('delete_company_documents')
                                                                                        <a href="javascript:void(0)"
                                                                                           onclick="removeMedia({{ $initial_inquiry_form_media->id}})"
                                                                                           class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                    class="fa fa-remove"></i>
                                                                                        </a>
                                                                                    @endcan


                                                                                </td>

                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane proposal" id="ppl_tab">
                                            <h2>Proposal:</h2>
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <table class="table">
                                                        <tr>
                                                            <th></th>
                                                            <th>Date</th>
                                                            <th>Standard</th>
                                                            <th>File</th>
                                                            <th>Action</th>

                                                        </tr>
                                                        @if(!empty($standard_documents) && count($standard_documents) >0)
                                                            @foreach ($standard_documents as $inqueryDocuemts)
                                                                @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                    @foreach ($inqueryDocuemts as $index => $data)
                                                                        @if($data->document_type === 'proposal')
                                                                            @php
                                                                                $proposal_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                            @endphp
                                                                            <tr id="#media_docs{{$proposal_media->id}}">

                                                                                <td>
                                                                                    <input type="hidden"
                                                                                           name="company_media_id"
                                                                                           value="{{ $proposal_media->id}}"
                                                                                           id="company_media_id"/>
                                                                                </td>
                                                                                <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                <td>
                                                                                    <a href="{{  asset('storage/'.$proposal_media->id.'/'.$proposal_media->file_name) }}"
                                                                                       target="_blank"
                                                                                       title="{{ $proposal_media->file_name }}">{{ $proposal_media->file_name }}</a>
                                                                                </td>
                                                                                <td>
                                                                                    @can('delete_company_documents')
                                                                                        <a href="javascript:void(0)"
                                                                                           onclick="removeMedia({{ $proposal_media->id}})"
                                                                                           class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                    class="fa fa-remove"></i>
                                                                                        </a>
                                                                                    @endcan

                                                                                </td>

                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane contract" id="ctt_tab">
                                            <h2>Contract:</h2>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table">
                                                        <tr>
                                                            <th></th>
                                                            <th>Date</th>
                                                            <th>Standard</th>
                                                            <th>File</th>
                                                            <th>Action</th>

                                                        </tr>
                                                        @if(!empty($standard_documents) && count($standard_documents) >0)
                                                            @foreach ($standard_documents as $inqueryDocuemts)
                                                                @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                    @foreach ($inqueryDocuemts as $index => $data)
                                                                        @if($data->document_type === 'contract')
                                                                            @php
                                                                                $contract_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                            @endphp
                                                                            <tr id="#media_docs{{$contract_media->id}}">

                                                                                <td>
                                                                                    <input type="hidden"
                                                                                           name="company_media_id"
                                                                                           value="{{ $contract_media->id}}"
                                                                                           id="company_media_id"/>
                                                                                </td>
                                                                                <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                <td>
                                                                                    <a href="{{  asset('storage/'.$contract_media->id.'/'.$contract_media->file_name) }}"
                                                                                       target="_blank"
                                                                                       title="{{ $contract_media->file_name }}">{{ $contract_media->file_name }}</a>
                                                                                </td>
                                                                                <td>
                                                                                    @can('delete_company_documents')
                                                                                        <a href="javascript:void(0)"
                                                                                           onclick="removeMedia({{ $contract_media->id}})"
                                                                                           class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                    class="fa fa-remove"></i>
                                                                                        </a>
                                                                                    @endcan

                                                                                </td>

                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane other" id="ods_tab">
                                            <h2>Other Documents:</h2>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table">
                                                        <tr>
                                                            <th></th>
                                                            <th>Date</th>
                                                            <th>Standard</th>
                                                            <th>File</th>
                                                            <th>Action</th>

                                                        </tr>
                                                        @if(!empty($standard_documents) && count($standard_documents) >0)
                                                            @foreach ($standard_documents as $inqueryDocuemts)
                                                                @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                    @foreach ($inqueryDocuemts as $index => $data)
                                                                        @if($data->document_type === 'other')
                                                                            @php
                                                                                $other_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                            @endphp
                                                                            <tr id="#media_docs{{$other_media->id}}">

                                                                                <td>
                                                                                    <input type="hidden"
                                                                                           name="company_media_id"
                                                                                           value="{{ $other_media->id}}"
                                                                                           id="company_media_id"/>
                                                                                </td>
                                                                                <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                <td>
                                                                                    <a href="{{  asset('storage/'.$other_media->id.'/'.$other_media->file_name) }}"
                                                                                       target="_blank"
                                                                                       title="{{ $other_media->file_name }}">{{ $other_media->file_name }}</a>
                                                                                </td>
                                                                                <td>
                                                                                    @can('delete_company_documents')
                                                                                        <a href="javascript:void(0)"
                                                                                           onclick="removeMedia({{$other_media->id}})"
                                                                                           class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                    class="fa fa-remove"></i>
                                                                                        </a>
                                                                                    @endcan

                                                                                </td>

                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="standard_certificates" id="standard_certificate" style="display: none;">
                                    <div class="col-md-12 text-center">
                                        <label class="text-uppercase emp_lbl" style="font-weight: 300;">Documents for
                                            <span class="text-lg standard_name"
                                                  style="font-weight: 700;">ISO 9001</span></label>
                                    </div>
                                    <div class="col-md-12 mrgn">
                                        {{-- <div class="col-md-1 floting"></div> --}}
                                        <div class="col-md-3 floting">
                                            <select name="document_type" class="form-control">
                                                <option value="">Select Document Type</option>
                                                @foreach($certificate_types as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <select name="old_document_files" class="form-control old_document_files">
                                                <option value="">Select From Old Files</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" name="document_file"
                                                           class="custom-file-input document_file" id="select_file">
                                                    <label class="custom-file-label" for="select_file">Choose
                                                        file (10MB)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
											  	<span class="input-group-text">
													<i class="fa fa-calendar"></i>
											  	</span>
                                                </div>
                                                <input type="text" class="float-right active form-control datefield"
                                                       name="document_date">
                                            </div>
                                        </div>
                                        <div class="col-md-1 floting">
                                            <button type="button" class="btn btn-primary add_document" title="Add">Add
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-12 mrgn">
                                        {{-- <div class="col-md-1 floting"></div> --}}
                                        <div class="col-md-12 floting grey_bg">
                                            <table class="table table-hover">
                                                <thead>
                                                <th>Sr#</th>
                                                <th>Document Type</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                            {{-- <div class="col-md-11 floting">
                                                <label>Initial-Enquiery-form.pdf</label>
                                            </div>
                                            <div class="col-md-1 floting text-right">
                                                <a href="#" style="color: red;"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </div> --}}
                                            <div class="clearfix"></div>
                                        </div>
                                        {{-- <div class="col-md-1 floting"></div> --}}
                                        <div class="clearfix"></div>
                                    </div>
                                    {{-- <div class="col-md-12 mrgn text-center">
                                        <a href="#"><h2><i class="fa fa-plus-circle" aria-hidden="true"></i></h2></a>
                                        <label>Want to upload other documents?</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3 floting"></div>
                                    </div> --}}
                                </div>
                                <div class="border_div"></div>
                                <div class="col-md-12">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-6 floting">
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn-success btn_back"
                                                        data-back-id="stepTab4">Back
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="submit" id="final_save"
                                                        class="btn btn-block btn-success btn_save">Save
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn-secondary btn_cncl">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="stepTab6" class="company-step tab-pane fade in">
                        <div class="col-md-12 floting">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Company Details</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <p class="results_ans company_name">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Region</label>
                                                    <p class="results_ans company_region">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <p class="results_ans company_country">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Sister Company</label>
                                                    <p class="results_ans company_sister_company">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Website</label>
                                                    <p class="results_ans company_website">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="border_div"></div>
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <label class="text-uppercase emp_lbl">TOTAL LOCATIONS - <span
                                                            class="text-lg company_total_location"
                                                            style="color: #fc5b00; font-weight: 700;">0 </span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>HO / Main Address *</label>
                                                    <p class="results_ans company_head_office">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Key Process *</label>
                                                    <p class="results_ans company_key_process">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <p class="results_ans company_country">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>City *</label>
                                                    <p class="results_ans company_city">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <label>No. Of Employees *</label>
                                                    <p class="results_ans company_no_of_employees">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12" id="companyMoreLocations">
                                            <div class="col-md-12 company_more_location" style="display: none;">
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Address *</label>
                                                        <p class="results_ans company_more_address">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>Key Process *</label>
                                                        <p class="results_ans company_more_key_process">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>Country *</label>
                                                        <p class="results_ans company_more_country">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>City *</label>
                                                        <p class="results_ans company_more_city">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>No. Of Employees *</label>
                                                        <p class="results_ans company_more_no_of_employees">-</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="border_div"></div>
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <label class="text-uppercase emp_lbl">Total Employees - <span
                                                            class="text-lg company_total_employees"
                                                            style="color: #fc5b00; font-weight: 700;">0 </span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>Permanant Employees</label>
                                                    <p class="results_ans company_permanent_employees">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>Part Time Employees</label>
                                                    <p class="results_ans company_part_time_employees">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>Similar / repeatative tasks</label>
                                                    <p class="results_ans company_similar_tasks">-</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">
                                                    <label>Shifts</label>
                                                    <p class="results_ans company_shifts">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-12 floting">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Scope Details</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Scope to be Certified</label>
                                                    <p class="results_ans company_scope_to_certify">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Any processes are outsourced ?</label>
                                                    <p class="results_ans company_outsourced_processed">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>General Remarks</label>
                                                    <p class="results_ans company_general_remarks">-</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-12 floting">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Contact Person Detail</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 cont_det">
                                            <table class="table table-responsive" id="companyContactNumbers">
                                                <thead>
                                                <tr>
                                                    <th>Primary</th>
                                                    <th>Title *</th>
                                                    <th>Name *</th>
                                                    <th>Position</th>
                                                    <th>Cell Number *</th>
                                                    <th>Landline Number</th>
                                                    <th>Email</th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-12 floting">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Standards</h3>
                                </div>
                                <div class="card-body" id="companyStandardDetails">
                                    <div class="row company_standards" style="display: none;">
                                        <div class="col-md-12">
                                            <div class="text-center stnd_lines">
                                                <label class="text-uppercase emp_lbl company_standard_title">-</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mrgn text-right">
                                            <div class="col-md-6 floting ">
                                                <label>New Client or Transfer From Other CB &nbsp;&nbsp; |</label>
                                            </div>
                                            <div class="col-md-6 floting text-left">
                                                <p class="results_ans company_transfered">-</p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        {{--                                        <div class="col-md-12">--}}
                                        {{--                                            <div class="col-md-12">--}}
                                        {{--                                                <div class="col-md-3 floting">--}}
                                        {{--                                                    <div class="form-group">--}}
                                        {{--                                                        <label>Head Office *</label>--}}
                                        {{--                                                        <p class="results_ans company_head_office">-</p>--}}
                                        {{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="col-md-3 floting">--}}
                                        {{--                                                    <div class="form-group">--}}
                                        {{--                                                        <label>Key Process *</label>--}}
                                        {{--                                                        <p class="results_ans company_key_process">-</p>--}}
                                        {{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="col-md-3 floting">--}}
                                        {{--                                                    <div class="form-group">--}}
                                        {{--                                                        <label>City *</label>--}}
                                        {{--                                                        <p class="results_ans company_city">-</p>--}}
                                        {{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="col-md-3 floting">--}}
                                        {{--                                                    <div class="form-group">--}}
                                        {{--                                                        <label>No. Of Employees *</label>--}}
                                        {{--                                                        <p class="results_ans company_no_of_employees">-</p>--}}
                                        {{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="clearfix"></div>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                        <div class="col-md-12 popup_info">
                                            <div class="col-md-12">
                                                <div class="col-md-12 text-center">
                                                    <h5><strong>Additional Information</strong></h5>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mrgn company_standard_question_answer">
                                            </div>
                                            {{-- <div class="col-md-12 mrgn">
                                                <div class="col-md-12">
                                                    <label>Exclusion</label>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <p class="results_ans">Yes</p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <label>Description</label>
                                                    <div class="form-group">
                                                        <p class="results_ans">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="col-md-12 mrgn">
                                                <div class="col-md-12">
                                                    <label>Proposed Complexity</label>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <p class="results_ans company_proposed_complexity">-</p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            {{--                                            <div class="col-md-12">--}}
                                            {{--                                                <label>General Remarks</label>--}}
                                            {{--                                                <div class="form-group">--}}
                                            {{--                                                    <p class="results_ans company_general_remarks">-</p>--}}
                                            {{--                                                </div>--}}
                                            {{--                                            </div>--}}
                                            <div class="col-md-12 no_padding">
                                                <div class="col-md-6 floting">
                                                    <label>Surveillance Frequency</label>
                                                    <div class="form-group">
                                                        <p class="results_ans company_surveillance_frequency">-</p>
                                                    </div>
                                                </div>
                                                {{--                                                <div class="col-md-6 floting">--}}
                                                {{--                                                    <label>Accreditation</label>--}}
                                                {{--                                                    <div class="form-group">--}}
                                                {{--                                                        <div class="col-md-2 floting">--}}
                                                {{--                                                            <p class="results_ans company_accreditations">-</p>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="border_div"></div>
                                            <div class="col-md-12">
                                                <div class="col-md-12 text-center">
                                                    <h5><strong>CONTRACT PRICE</strong></h5>
                                                </div>
                                            </div>
                                            <div class="col-md-12 floting" style="margin-top: 24px;">
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Currency</label>
                                                        <p class="results_answer company_currency">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Initial Certification</label>
                                                        <p class="results_answer company_initial_amount">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Surveillance</label>
                                                        <p class="results_answer company_surveillance_amount">-</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Re-Audit</label>
                                                        <p class="results_answer company_re_audit_amount">-</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="border_div"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-12 floting">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">File Upload</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12" id="document_uploads">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-12 mrgn">
                            <div class="col-md-3 floting"></div>
                            <div class="col-md-6 floting">
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-block btn-success btn_back"
                                                data-back-id="stepTab5">Back
                                        </button>
                                    </div>
                                </div>
                                {{--                                <div class="col-md-4 floting">--}}
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <button type="button" class="btn btn-block btn-success btn_print">Print</button>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <button type="submit" id="finalSave" class="btn btn-block btn-success btn_save">
                                            Save
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-block btn-secondary btn_cncl">Cancel
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-3 floting"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        special_dropdown = [];
        special_dropdown_multiple = [];
    </script>
@endpush

@push('modals')
    {{-- Standard Questions Modal --}}

    @foreach($standards as $standard)
        @php
            $company_standard = $company->companyStandards->where('standard_id', $standard->id)->first();
        @endphp
        <div class="modal fade standard_questions" id="standard_question_{{ $standard->id }}" tabindex="-1"
             role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: auto">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="#" method="post" class="standard_questionnaire" data-standard-id="{{ $standard->id }}"
                          enctype="multipart/form-data">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">{{ $standard->name }} Additional
                                Information</h4>
                            {{--                            <button type="button" class="close"--}}
                            {{--                                    onclick="closeStandardQuestionModal('{{$standard->id}}')"><span--}}
                            {{--                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>--}}
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="step" value="4">
                            <input type="hidden" name="standard_id" value="{{ $standard->id }}">
                            <input type="hidden" name="company_standard_id" value="{{ $company_standard->id ?? '' }}">
                            <input type="hidden" name="is_ims" value="{{ $company_standard->is_ims ?? '' }}">
                            @if(!empty($company_standard))
                                <input type="hidden" name="company_standard_action" value="update">
                            @endif
                            <div class="col-md-12 text-center" style="display: none">
                                <div class="col-md-6 floting">
                                    <label>Transfered from another CB?</label>
                                </div>
                                <div class="col-md-6 floting">
                                    <input type="checkbox" name="client_type" value="transfered"
                                           class="transfered_customer" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? 'checked' : '') : '' }}>&nbsp;&nbsp;<span>Yes</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="col-md-6 floting">
                                    <label>Transfered from another CB?</label>
                                </div>
                                <div class="col-md-6 floting">
                                    <input type="checkbox" name="is_transfer_standard" value="0"
                                           class="is_transfer_standard" {{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? 'checked' : '') : '' }}>&nbsp;&nbsp;<span>Yes</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 mrgn is_transfer_standard_fields"
                                 style="display: {{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? 'unset' : 'none') : 'none' }};">
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <label>Transfer Year *</label>
                                        <input type="text" name="transfer_year" class="form-control transfer_year"
                                               value="{{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? $company_standard->transfer_year : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->is_transfer_standard == true ? '' : 'disabled') : 'disabled' }}>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                            <div class="col-md-12 mrgn transfered_fields"
                                 style="display: {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? 'unset' : 'none') : 'none' }};">
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <label>CB Name *</label>
                                        <input type="text" name="old_cb_name" class="form-control"
                                               placeholder="Enter CB Name"
                                               value="{{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? $company_standard->old_cb_name : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <label>Certification Issue Date *</label>
                                        <input type="text" name="old_cb_certificate_issue_date"
                                               class="form-control float-right  active old_cb_certificate_issue_date"
                                               {{--                                               onchange="dateStandardBased('{{$standard->id}}')"--}}
                                               id="old_cb_certificate_issue_date{{$standard->id}}"
                                               data-standard-id="{{$standard->id}}"
                                               onkeypress="return false;"
                                               placeholder="Enter Certification Issued"
                                               value="{{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? $company_standard->old_cb_certificate_issue_date->format('m/d/Y') : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <label>Certification Expiry *</label>
                                        <input type="text" name="old_cb_certificate_expiry_date"
                                               class="form-control float-right  active"
                                               id="old_cb_certificate_expiry_date{{$standard->id}}"
                                               data-standard-id="{{$standard->id}}"
                                               placeholder="Enter Certification Expiry"
                                               onkeypress="return false;"
                                               value="{{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? $company_standard->old_cb_certificate_expiry_date->format('m/d/Y') : '') : '' }}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">
                                            <label>Previous Surveillance Frequency *</label>
                                            <select class="form-control"
                                                    id="previousSurveillanceFrequency{{$standard->id}}"
                                                    onchange="previousFrequncy('#previousSurveillanceFrequency{{$standard->id}}',{{$standard->id}})"
                                                    name="old_cb_surveillance_frequency" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                                {{--                                                <option value="">Select Surveillance Frequency</option>--}}
                                                @foreach($surveillance_frequencies as $frequency)
                                                    <option value="{{ $frequency }}"
                                                            @if(!empty($company_standard) )
                                                                @if($frequency == $company_standard->old_cb_surveillance_frequency)
                                                                    selected
                                                            @else
                                                            @endif
                                                            @else
                                                                @if($frequency == 'annual')
                                                                    selected
                                                    @else
                                                            @endif
                                                            @endif
                                                    >
                                                        {{ ucfirst($frequency) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <label>Last Audit Activity *</label>
                                        <select class="form-control" id="old_cb_audit_activity_stage_id"
                                                name="old_cb_audit_activity_stage_id"
                                                data-standard-id="{{$standard->id}}" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                            <option value="">Select Activity</option>
                                            @foreach($audit_activity_stages as $stage)
                                                @continue($stage->name == 'Stage 1')
                                                <option value="{{ $stage->id }}"
                                                        id="last-audit-activity-{{ $stage->id }}{{$standard->id}}"
                                                        {{ !empty($company_standard) ? ($stage->id == $company_standard->old_cb_audit_activity_stage_id ? 'selected' : '') : '' }}>
                                                    {{ $stage->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-9 floting uploadCheck">
                                        {{-- <label>Files Upload</label> --}}
                                        <div class="form-group">
                                            <div class="col-md-4 floting">
                                                <input type="checkbox" name="old_cb_last_audit_report_attached"
                                                       value="attached" checked="" style="display: none;"><label>Last
                                                    Audit Report *</label>
                                                <div class="input-group form-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               id="old_cb_last_audit_report_doc"
                                                               onchange="readURLManyTranseferCase(this, '{{$standard->id}}1')"
                                                               name="old_cb_last_audit_report_doc" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                                        <label class="custom-file-label"
                                                               for="old_cb_last_audit_report_doc">Choose file
                                                            (10MB)</label>
                                                    </div>

                                                    <div class="input-group-append"></div>
                                                </div>
                                                @if( !empty($company_standard) && !is_null($company_standard->old_cb_last_audit_report_doc_media_id))
                                                    <div class="image form-group" id="upld{{$standard->id}}1">

                                                        <h6>Selected File: <span><a target="_blank"
                                                                                    href="{{ asset('uploads/company_audit_report/'.$company_standard->old_cb_last_audit_report_doc_media_id) }}">{{ $company_standard->old_cb_last_audit_report_doc_media_id }}</a></span>
                                                        </h6>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-4 floting">
                                                <input type="checkbox" name="old_cb_certificate_copy_attached"
                                                       value="attached" checked="" style="display: none;"><label>Certificate
                                                    Copy *</label>
                                                <div class="input-group form-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               id="old_cb_certificate_copy_doc"
                                                               onchange="readURLManyTranseferCase(this, '{{$standard->id}}2')"
                                                               name="old_cb_certificate_copy_doc" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                                        <label class="custom-file-label"
                                                               for="old_cb_certificate_copy_doc">Choose file
                                                            (10MB)</label>
                                                    </div>
                                                    <div class="input-group-append"></div>
                                                </div>
                                                @if( !empty($company_standard) && $company_standard->old_cb_certificate_copy_doc_media_id)
                                                    <div class="image form-group" id="upld{{$standard->id}}2">
                                                        <h6>Selected File: <span><a target="_blank"
                                                                                    href="{{ asset('uploads/company_certificate_copy/'.$company_standard->old_cb_certificate_copy_doc_media_id) }}">{{ $company_standard->old_cb_certificate_copy_doc_media_id }}</a></span>
                                                        </h6>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-4 floting">
                                                <input type="checkbox" name="old_cb_others_attached" value="attached"
                                                       checked="" style="display: none;"><label>Other Documents</label>
                                                <div class="input-group form-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               onchange="readURLManyTranseferCase(this, '{{$standard->id}}3')"
                                                               id="old_cb_others_doc"
                                                               name="old_cb_others_doc" {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? '' : 'disabled') : 'disabled' }}>
                                                        <label class="custom-file-label" for="old_cb_others_doc">Choose
                                                            file (10MB)</label>
                                                    </div>
                                                    <div class="input-group-append"></div>
                                                </div>
                                                @if( !empty($company_standard) && !is_null($company_standard->old_cb_others_doc_media_id))
                                                    <div class="image form-group" id="upld{{$standard->id}}3">
                                                        <h6>Selected File: <span><a target="_blank"
                                                                                    href="{{ asset('uploads/company_other_doc/'.$company_standard->old_cb_others_doc_media_id) }}">{{ $company_standard->old_cb_others_doc_media_id }}</a></span>
                                                        </h6>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 previous_activities_dates">
                                    <div class="col-md-6">
                                        <div class="col-md-6 floting activity_type">
                                            <label>Audit Type</label>
                                            @if(!empty($company_standard) && $company_standard->client_type == 'transfered')
                                                @foreach($audit_activity_stages as $stage)
                                                    @continue($stage->name == 'Stage 1')
                                                    <p>{{ $stage->name }}</p>
                                                    @break($stage->id == $company_standard->old_cb_audit_activity_stage_id)
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="col-md-6 floting activity_date">
                                            <label>Previous Audit Date</label>
                                            @if(!empty($company_standard) && $company_standard->client_type == 'transfered')
                                                @php
                                                    $activity_dates = json_decode($company_standard->old_cb_audit_activity_dates, true);
                                                @endphp
                                                @foreach($audit_activity_stages as $stage)
                                                    @continue($stage->name == 'Stage 1')
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                        class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <input type="text"
                                                               class="form-control datefield float-right active"
                                                               id="old_cb_audit_activity_date{{ str_replace(" ", "" , $stage->name) }}{{$standard->id}}"
                                                               name="old_cb_audit_activity_date[{{ $stage->id }}]"
                                                               onkeypress="return false;"
                                                               required=""
                                                               value="{{ $activity_dates[$stage->id] ?? '' }}">
                                                    </div>
                                                    @break($stage->id == $company_standard->old_cb_audit_activity_stage_id)
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="border_div"></div>
                            <div class="popup_info">
                                {{-- <div class="col-md-12">
                                    <div class="col-md-12">
                                        <h5>Additional Information <b>ISO 9001</b></h5>
                                    </div>
                                </div> --}}
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <label class="text-uppercase emp_lbl" style="font-weight: 300;"><span
                                                        class="text-lg blue_text" style="font-weight: 700;">Additional Information</span></label>
                                        </div>
                                    </div>
                                    @foreach($standard->questions()->orderBy('sort')->get() as $question)
                                        @php
                                            $attribs = ' id="question'. $question->id .'" '. ($question->has_dependent_fields ? 'data-dependent-fields="'. $question->dependent_fields_ids .'"' : '') . ($question->has_dependent_fields ? 'data-right-answer="'. $question->right_answer_for_dependent_fields .'"' : '') .' minlength="'. $question->min_length .'" maxlength="'. $question->max_length .'"'. ($question->is_required ? 'required=""' : '') . (($question->is_dependent_field = '0') ? 'disabled=""' : '');
                                            if(!empty($company_standard) && $company_standard->companyStandardsAnswers->where('standard_question_id', $question->id)->count() > 0)
                                            {
                                                $answer = $company_standard->companyStandardsAnswers->where('standard_question_id', $question->id)->first()->answer;
                                            }
                                            else
                                            {
                                                unset($answer);
                                            }
                                        @endphp
                                        <div class="col-md-12">
                                            <div class="col-md-12 floting">
                                                <label>
                                                    {{ $question->question }} {{ $question->is_required ? '*' : ''}}
                                                </label>
                                            </div>
                                            @if($question->field_type == 'textarea')
                                                <div class="col-md-12">
                                                    <textarea rows="1"
                                                              class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }}"
                                                              name="question[{{ $question->id }}]" {!! $attribs !!}
                                                >{{ $answer ?? $question->default_answer }}</textarea>
                                                </div>
                                            @elseif($question->field_type == 'small_text' || $question->field_type == 'text' || $question->field_type == 'date' || $question->field_type == 'datetime')
                                                <div class="col-md-12 {{ $question->field_type == 'small_text' ? 'col-xs-3' : '' }}">
                                                    <input type="{{ $question->field_type == 'number' ? 'number' : 'text' }}"
                                                           name="question[{{ $question->id }}]"
                                                           class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }} {{ $question->field_type == 'datefield' ? '' : '' }} {{ $question->field_type == 'datetime' ? 'datetimefield' : '' }}"
                                                           value="{{ $answer ?? '' }}" {!! $attribs !!}>
                                                </div>
                                            @elseif($question->field_type == 'number')
                                                <div class="col-md-4 {{ $question->field_type == 'small_text' ? 'col-xs-3' : '' }}">
                                                    <input type="{{ $question->field_type == 'number' ? 'number' : 'text' }}"
                                                           name="question[{{ $question->id }}]"
                                                           class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }} {{ $question->field_type == 'datefield' ? '' : '' }} {{ $question->field_type == 'datetime' ? 'datetimefield' : '' }}"
                                                           value="{{ $answer ?? 1 }}" {!! $attribs !!}>
                                                </div>
                                                <dic class="clearfix"></dic>
                                            @elseif($question->field_type == 'radio' || $question->field_type == 'checkbox')
                                                @foreach(collect(json_decode($question->options))->chunk(4) as $chunk)
                                                    @foreach($chunk as $option)
                                                        <div class="col-md-3 floting">
                                                            <label class="radio-inline">
                                                                <input type="{{ $question->field_type == 'radio' ? 'radio' : 'checkbox' }}"
                                                                       name="question[{{ $question->id }}]"
                                                                       value="{{ $option }}"
                                                                       class="{{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }}"
                                                                       {!! $attribs !!}
                                                                       @if(isset($answer) && $answer == $option)
                                                                           checked
                                                                       @else
                                                                           @if(($question->id == 24 || $question->id == 22  || $question->id == 20) && $option == 'no')
                                                                               checked
                                                                        @endif
                                                                        @endif
                                                                > {{ ucfirst($option) }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                @endforeach
                                                <div class="clearfix"></div>
                                            @elseif($question->field_type == 'dropdown' || $question->field_type == 'multi_dropdown')
                                                <div class="col-md-12">
                                                    <select class="form-control {{ $question->has_dependent_fields ? 'has_dependent_fields' : '' }}"
                                                            {{ $question->field_type == 'multi_dropdown' ? 'multiple' : '' }} {!! $attribs !!} name="question[{{ $question->id }}]{{ $question->field_type == 'multi_dropdown' ? '[]' : '' }}">
                                                        @php
                                                            $decoded_options = json_decode($question->options);
                                                            if(isset($answer) && $question->field_type == 'multi_dropdown')
                                                            {
                                                                $decoded_answer = json_decode($answer, true);
                                                            }
                                                        @endphp
                                                        @foreach($decoded_options as $option)
                                                            <option value="{{ $option }}" {{ $question->field_type == 'multi_dropdown' ? (isset($answer) && in_array($option, $decoded_answer) ? 'selected' : '') : (isset($answer) && $option == $answer ? 'selected' : '') }}>
                                                                {{ ucfirst($option) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @elseif($question->field_type == 'special_dropdown')
                                                <div class="col-md-12">
                                                    <select class="form-control special_dropdown"
                                                            data-id="id{{ $question->id }}" {!! $attribs !!}>
                                                        <option>Select {{ $question->question }}</option>
                                                        @php
                                                            $decoded_options = json_decode($question->options);
                                                            if(isset($answer))
                                                            {
                                                                $decoded_answer = json_decode($answer, true);
                                                            }
                                                        @endphp
                                                        @foreach($decoded_options as $option)
                                                            <option value="{{ $option }}" {{ (isset($answer) && in_array($option, $decoded_answer)) ? 'selected' : '' }}>
                                                                {{ ucfirst($option) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @elseif($question->field_type == 'totally_dependent')
                                                <div class="col-md-12">
                                                    <select class="form-control"
                                                            id="id{{ $question->company_standards_question_id }}"
                                                            data-question-title="{{ $question->question }}" {!! $attribs !!}>
                                                        <option>Select {{ $question->question }}</option>
                                                        {{-- @foreach(json_decode($question->options) as $option)
                                                            <option value="{{ $option }}">{{ ucfirst($option) }}</option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>
                                                @push('scripts')
                                                    <script type="text/javascript">
                                                        special_dropdown['id{{ $question->company_standards_question_id }}'] = JSON.parse('{!! $question->options !!}');
                                                    </script>
                                                @endpush
                                            @elseif($question->field_type == 'special_dropdown_multiple')
                                                <div class="col-md-12">
                                                    <select class="form-control special_dropdown_multiple"
                                                            data-id="id{{ $question->id }}" {!! $attribs !!} multiple=""
                                                            name="question[{{ $question->id }}][]">
                                                        {{-- <option>Select {{ $question->question }}</option> --}}
                                                        @php
                                                            $decoded_options = json_decode($question->options);
                                                            if(isset($answer))
                                                            {
                                                                $decoded_answer = json_decode($answer, true);
                                                            }
                                                        @endphp
                                                        @foreach($decoded_options as $option)
                                                            <option value="{{ $option }}" {{ (isset($answer) && in_array($option, $decoded_answer)) ? 'selected' : '' }}>
                                                                {{ ucfirst($option) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @elseif($question->field_type == 'totally_dependent_multiple')
                                                <div class="col-md-12">
                                                    <select class="form-control"
                                                            id="id{{ $question->company_standards_question_id }}"
                                                            data-question-title="{{ $question->question }}"
                                                            {!! $attribs !!} multiple=""
                                                            name="question[{{ $question->id }}][]">
                                                        {{-- <option>Select {{ $question->question }}</option> --}}
                                                        {{-- @foreach(json_decode($question->options) as $option)
                                                            <option value="{{ $option }}">{{ ucfirst($option) }}</option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>
                                                @push('scripts')
                                                    <script type="text/javascript">
                                                        special_dropdown_multiple['id{{ $question->company_standards_question_id }}'] = JSON.parse('{!! $question->options !!}');
                                                    </script>
                                                @endpush
                                            @endif
                                        </div>
                                    @endforeach
                                    @if($standard->standards_family_id != 20)
                                        <div class="col-md-12">
                                            <div class="col-md-6 pull-left">
                                                <div class="col-md-12">
                                                    <label>
                                                        Proposed Complexity *
                                                    </label>
                                                </div>
                                                @foreach($proposed_complexities as $proposed_complexity)
                                                    <div class="col-md-4 floting">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="proposed_complexity"
                                                                   value="{{ $proposed_complexity }}" {{ !empty($company_standard) ? ($proposed_complexity == $company_standard->proposed_complexity ? 'checked' : '') : '' }}> {{ ucfirst($proposed_complexity) }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                                <span id="propsoedComplexity{{$standard->id}}" style="display: none">Proposed Complexity is Required for this Standard</span>
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <div class="col-md-12">
                                                    <label>
                                                        Complexity Remarks
                                                    </label>
                                                </div>
                                                <div class="col-md-12">
                                                <textarea class="form-control"
                                                          name="general_remarks"
                                                          rows="1">{{ $company_standard->general_remarks ?? '' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <h4>Accreditations & IAF Codes *</h4>

                                        <div id="auditor-standard-codes{{$standard->id}}"
                                             data-standard_id="{{$standard->id}}"
                                             data-standard-family-id="{{$standard->standards_family_id}}"
                                             class="auditor-standard-codes{{$standard->id}}">

                                        </div>

                                    </div>


                                    @if($standard->standards_family_id == 23)
                                        <div class="col-md-12">
                                            <h4>Accreditations & Food Codes*</h4>

                                            <div id="auditor-standard-food-codes{{$standard->id}}"
                                                 data-standard_id="{{$standard->id}}"
                                                 data-standard-family-id="{{$standard->standards_family_id}}"
                                                 class="auditor-standard-food-codes{{$standard->id}}">

                                            </div>

                                        </div>
                                    @endif

                                    @if($standard->standards_family_id == 20)
                                        <div class="col-md-12">
                                            <h4>Accreditations & Energy Codes*</h4>

                                            <div id="auditor-standard-energy-codes{{$standard->id}}"
                                                 class="auditor-standard-energy-codes{{$standard->id}}"
                                                 data-standard-family-id="{{$standard->standards_family_id}}"
                                                 data-standard_id="{{$standard->id}}"
                                            >

                                            </div>

                                        </div>
                                    @endif
                                </div>


                                <div class="col-md-12">
                                    <div class="col-md-6 floting">
                                        <div class="form-group">
                                            <label>Surveillance Frequency *</label>
                                            <select class="form-control" name="surveillance_frequency">
                                                {{--                                                <option value="">Select Surveillance Frequency</option>--}}
                                                @foreach($surveillance_frequencies as $frequency)
                                                    <option value="{{ $frequency }}"
                                                            @if(!empty($company_standard) )
                                                                @if($frequency == $company_standard->surveillance_frequency)
                                                                    selected
                                                            @else
                                                            @endif
                                                            @else
                                                                @if($frequency == 'annual')
                                                                    selected
                                                    @else
                                                            @endif
                                                            @endif

                                                    >
                                                        {{ ucfirst($frequency) }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <label class="text-uppercase emp_lbl" style="font-weight: 300;"><span
                                                        class="text-lg blue_text" style="font-weight: 700;">Contract Price Per Standard</span></label>
                                        </div>
                                    </div>
                                    <div class="mrgn"></div>
                                    <div class="col-md-12 floting">
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Currency *</label>
                                                <select class="form-control" name="currency_unit">
                                                    <option>Select Currency</option>
                                                    @foreach($currencies as $code => $name)
                                                        @if(empty($code))
                                                            @continue
                                                        @endif
                                                        <option value="{{ $code }}" {{ !empty($company_standard) ? ($company_standard->initial_certification_currency == $code ? 'selected' : '') : '' }}>
                                                            {{ $code }}
                                                            | {{ $name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Initial Certification Amount *</label>

                                                <input type="text" name="initial_certification_amount"
                                                       class="form-control" placeholder="Initial Certification Amount"
                                                       pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                       {{ !empty($company_standard) ? ($company_standard->client_type == 'transfered' ? 'disabled' : '') : '' }}
                                                       value="{{ $company_standard->initial_certification_amount ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Surveillance Amount *</label>
                                                <input type="text" name="surveillance_amount" class="form-control"
                                                       placeholder="Surveillance Amount"
                                                       pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                       value="{{ $company_standard->surveillance_amount ?? '' }}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Re-Audit Amount</label>
                                                <input type="text" name="re_audit_amount" class="form-control"
                                                       placeholder="Re-Audit Amount" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                       value="{{ $company_standard->re_audit_amount ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{--                            <button type="button" class="btn btn-default"--}}
                            {{--                                    onclick="closeStandardQuestionModal('{{$standard->id}}')">Close--}}
                            {{--                            </button>--}}
                            <button type="submit" class="btn btn-primary save_standard_questionnaire">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <div class="card-body">
        <div class="tab-content">


            <div id="edit-auditor-standard-container">

            </div>

            {{--                                        <div id="edit-food-company-standard-container">--}}

            {{--                                        </div>--}}

            <div id="edit-energy-auditor-standard-container">

            </div>

        </div>

    </div>
    {{-- End Standard Questions Modal --}}
    <div class="modal fade" id="contacts_modal" tabindex="-1" role="dialog" aria-hidden="true"
         style="width: 50%;left: 30%;">
        <div class="modal-dialog">
            <form action="#" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel2">Contact Numbers</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 text-center contact_row floting" style="margin-top: 10px;">
                            <div class="col-md-10 floting">
                                <div class="form-group">
                                    <input type="tel" name="contact_number" class="form-control contact_number"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-2 floting add_remove">
                                <button class="btn btn-sm btn-primary add_contact" style="margin-top: 4px;"><i
                                            class="fa fa-plus"></i></button>
                                <button class="btn btn-sm btn-danger remove_contact" style="margin-top: 4px;"
                                        disabled=""><i class="fa fa-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="save_contact" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade customModal2" id="standards_for_documents" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="#" method="post" id="save_standard_documents" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Standard(s)</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="popup_info">
                            <h4>Select Standard(s)</h4>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="multiselect">
                                    <div class="selectBox">
                                        <select class="form-control" multiple=""></select>
                                        <div class="overSelect"></div>
                                    </div>
                                    <div id="checkboxes" class="selected_standards"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="standard_document_date">Date:</label>
                                <input type="text" class="form-control datefield" id="standard_document_date"
                                       name="date">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label style="color:red;">All Fields are required *</label>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="btn btn-default btn-file noMarginBtmBtn">
                                    Select File(s) (10MB) <input type="file" id="documents" name="documents[]"
                                                                 onchange="readURLMany(this,100)" class="documents">
                                </label>

                                <label>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </label>
                            </div>
                            <div class="col-md-12">
                                <div class="image form-group" id="upld100">

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </form>
        </div>
    </div>
@endpush
<link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">
    <style>
        .disabledDivStandard {
            pointer-events: none;
            opacity: 0.4;
        }

        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top {
            width: 25% !important;
            margin: 0 auto !important;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    <script type="text/javascript">

        $('#standard_document_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        function primaryIndex(index) {
            // Reset all primary radio buttons to unchecked
            $('.primary').each(function() {
                $(this).val(0).removeAttr('checked');
            });

            // Set the selected primary radio button
            $('.removeclass' + index).find('.primary').val(1).attr('checked', 'checked');

            // Update the hidden input to track the primary index
            $('#primary_index').val(index);

        }

        function previousFrequncy(previousFrequncy, standard_id) {

            // e.preventDefault();

            var previousfrequncy = $(previousFrequncy).val();
            if (previousfrequncy === 'annual') {
                $('#last-audit-activity-2' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-3' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-4' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-5' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-6' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-7' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-8' + standard_id + '').css('display', 'block');
            } else if (previousfrequncy === "biannual") {
                $('#last-audit-activity-2' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-3' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-4' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-5' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-6' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-7' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-8' + standard_id + '').css('display', 'block');

            } else {
                $('#last-audit-activity-2' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-3' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-4' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-5' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-6' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-7' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-8' + standard_id + '').css('display', 'none');
            }
        }

        function readURLMany(input, id) {
            var filename = input.files[0].name;
            var html = '';
            if (input.files && input.files[0]) {
                html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
            }
            $('#upld' + id).html(html);
        }

        function readURLManyTranseferCase(input, id) {
            var filename = input.files[0].name;
            var html = '';
            if (input.files && input.files[0]) {
                html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
            }
            $('#upld' + id).html(html);
        }

        String.prototype.ucFirst = function () {
            return this.substring(0, 1).toUpperCase() + this.substring(1, this.length);
        }
        // $('#contacts_modal').modal('show');
        standards = {!! collect($standards)->mapWithKeys(function($item){ return ['id_' . $item->id => $item]; })->toJson() !!};
        company_id = 0;
        ims_standards_selected = [];
        selected_standards = [];
        default_surveillance_frequency_for_ims = "";
        default_amounts_for_ims = {"initial_certification_amount": 0, "surveillance_amount": 0, "re_audit_amount": 0};
        default_accreditation_for_ims = [];
        selected_standards_saved = {};
        document_files = {size: 0};
        clicked_contact_node = "";
        locations_count = {{$company->childCompanies->count()}};
        locations_count = locations_count + 1
        standards_to_hide = [];
        var standardId = null;
        var country_code = '';
        $('#region_id').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#country_id';
            var region_id = $(this).val();
            var request = "region_id=" + region_id;


            if (region_id === '') {

            } else {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.regionCountries') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.region_countries, function (i, obj) {
                                html += '<option value="' + obj.country.id + '">' + obj.country.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            sortMe($(node_to_modify).find('option'));
                            $(node_to_modify).prepend("<option value='' selected>Select Country</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }

        });
        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            country_code = response.data.country.phone_code;
                            // $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });
        $(document).on('change', '.countries_child', function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities_child' + $(this).data('index');
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            country_code = response.data.country.phone_code;
                            // $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        $('#name,#country_id').on('keyup change', function () {
            form = $(this).closest('form');
            node = $(this);
            var name = $('#name').val();
            var country_id = $('#country_id').val();
            var request = "name=" + name + "&country_id=" + country_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.companyNameExist') }}",
                data: request,
                dataType: "json",
                cache: true,
                global: false,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        if (response.data.already_exist) {
                            var standard_message = 'With Standards: ';
                            $.each(response.data.company_standards, function (i, obj) {
                                standard_message += obj.standard.number + ', ';
                            });
                            $('.standard_selection').show();
                            if (!confirm("Company Name Already Exists. " + standard_message + ". Do You Want to Create Another?")) {
                                $(node).val('');
                            } else {
                                $.each(response.data.company_standards, function (i, obj) {
                                    $('.standard_selection[data-standard-number="{{ $standard->number }}"]').hide();
                                });
                            }
                        } else {
                            $('.standard_selection').show();
                        }
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('#iaf_id input[type="checkbox"]').change(function () {
            if ($(this).is(':checked')) {
                form = $(this).closest('form');
                node = $(this);
                node_to_modify = '#ias_id div.ias_list';
                var iaf_id = $(this).val();//JSON.stringify($(this).val());
                console.log(iaf_id);
                var request = "iaf_id=" + iaf_id;

                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.iasByIAF') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            resetIASCodes();
                            var html = "";
                            $.each(response.data.iases, function (i, obj) {
                                html += '<label data-iaf-id="iaf_' + iaf_id + '"><input type="checkbox" value="' + obj.id + '" name="ias_id[]"/>' + obj.code + ' | ' + obj.name + '</label>';
                                // html 	+= '<option value="' + obj.id + '">'+ obj.code +' | '+ obj.name +'</option>';
                            });
                            $(node_to_modify).append(html);
                            // $(node_to_modify).prepend("<option value='' selected>Select IAS</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                resetIASCodes();
            }
        });

        function resetIASCodes() {
            $('#iaf_id input[type="checkbox"]').each(function (k, node) {
                if (!$(node).is(':checked')) {
                    var val = $(node).val();
                    $('label[data-iaf-id="iaf_' + val + '"]').remove();
                }
            });
        }

        $('#step1').on('click', '.remove_other_location', function () {

            $(this).closest('.address_main_div_new').remove();
            locations_count--;
            $('#total_locations_count').text(locations_count);

            var copydiv;
            for (var i = 0; i < $('.address_main_div_new').length; i++) {
                copydiv = $('.address_main_div_new')[i];
                // $(copydiv).find(".address_count").text('Address ' + (i + 1) + '*');
            }
        });

        $('#step1').submit(function (e) {
            e.preventDefault();
            form = $(this);
            var request = $(form).serialize();
            if (company_id != 0) {
                request += "&company_id=" + company_id;
            }
            $.ajax({
                type: "POST",
                url: "{{ route('company.update') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success") {
                        $('a[href="#stepTab1"]').parent().removeClass('pro_working');
                        $('a[href="#stepTab1"]').parent().addClass('pro_done');
                        $('a[href="#stepTab1"]').parent().removeClass('active');
                        $('a[href="#stepTab1"]').parent().addClass('nonactive');
                        $('a[href="#stepTab3"]').parent().removeClass('pro_pending');
                        $('a[href="#stepTab3"]').parent().addClass('pro_working');
                        $('a[href="#stepTab3"]').parent().removeClass('nonactive');
                        $('a[href="#stepTab3"]').parent().addClass('active');
                        // $('a[href="#stepTab2"]').trigger('click');
                        $('.company-step').removeClass('active show');
                        $('#stepTab3').addClass('active show');
                        $(form).append('<input type="hidden" name="company_id" value="' + response.data.company.id + '">');
                        $(form).append('<input type="hidden" name="action" value="update">');
                        company_id = response.data.company.id;
                        var current_progress = parseInt($('#progressValue').text());
                        var now_progress = current_progress + 25;
                        $('#progressValue').text(now_progress + '%');
                        $('#progressBar').css('width', now_progress + '%');
                        $('div.content-header .dashboard_heading').html(response.data.company.name + "'s <span>Profile</span>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('#step2').submit(function (e) {
            e.preventDefault();
            form = $(this);
            var request = $(form).serialize();
            if (company_id != 0) {
                request += "&company_id=" + company_id;
            }
            $.ajax({
                type: "POST",
                url: "{{ route('company.update') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success") {
                        $('a[href="#stepTab2"]').parent().removeClass('pro_working');
                        $('a[href="#stepTab2"]').parent().addClass('pro_done');
                        $('a[href="#stepTab2"]').parent().removeClass('active');
                        $('a[href="#stepTab2"]').parent().addClass('nonactive');
                        $('a[href="#stepTab3"]').parent().removeClass('pro_pending');
                        $('a[href="#stepTab3"]').parent().addClass('pro_working');
                        $('a[href="#stepTab3"]').parent().removeClass('nonactive');
                        $('a[href="#stepTab3"]').parent().addClass('active');
                        // $('a[href="#stepTab3"]').trigger('click');
                        $('.company-step').removeClass('active show');
                        $('#stepTab3').addClass('active show');
                        $(form).append('<input type="hidden" name="company_id" value="' + response.data.company.id + '">');
                        $(form).append('<input type="hidden" name="action" value="update">');
                        var current_progress = parseInt($('#progressValue').text());
                        var now_progress = current_progress + 25;
                        $('#progressValue').text(now_progress + '%');
                        $('#progressBar').css('width', now_progress + '%');
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        total_employees = 0;
        $('#add_address').click(function () {
            var $copyDiv = $('#address_main_div_new').clone();
            $($copyDiv).removeAttr('id');
            $($copyDiv).find('input,select').prop('disabled', false);
            $($copyDiv).show();
            locations_count = parseInt($('#total_locations_count').text());
            locations_count++;
            // $($copyDiv).prepend('<div class="border_div"></div>');
            $('.address_add_div_new').append($copyDiv);
            $($copyDiv).find('.countries_child').attr('data-index', locations_count);
            $($copyDiv).find('.cities_child').attr('data-index', locations_count).addClass('cities_child' + locations_count);
            // $($copyDiv).find('.address_count').text('Address ' + locations_count + '*');
            $('#total_locations_count').text(locations_count);
        });
        $('#enable_sister_company_name').click(function () {
            if ($(this).is(':checked')) {
                $('#sister_company_name').prop("disabled", false);
            } else {
                $('#sister_company_name').prop("disabled", true);
            }
        });
        $('#enable_out_of_country_region_name').click(function () {
            if ($(this).is(':checked')) {
                $('#out_of_country_region_name').prop("disabled", false);
            } else {
                $('#out_of_country_region_name').prop("disabled", true);
            }
        });
        $('#enable_outsourced_process_details').click(function () {
            if ($(this).is(':checked')) {
                $('#outsourced_process_details').prop("disabled", false);
            } else {
                $('#outsourced_process_details').prop("disabled", true);
            }
        });
        $('form').on('keyup keypress blur change', '.total_employees', function () {
            var total = 0;
            $('.total_employees').each(function () {
                var tmp = parseInt($(this).val());
                total += isNaN(tmp) ? 0 : tmp;
            });
            total_employees = total;
            var factor_part_time = '@php echo $effective_employees->factor_part_time @endphp';
            var factor_similar = '@php echo $effective_employees->factor_similar @endphp';
            var repeatative_employees = $('#repeatative_employees').val();
            var part_time_employees = $('#part_time_employees').val();
            var permanent_employees = $('#permanent_employees').val();
            var effective_employees = ((total_employees - part_time_employees - repeatative_employees) + (factor_part_time * part_time_employees) + (factor_similar * repeatative_employees));
            $('#effective_employees_count').text(Math.round(effective_employees));
            // $('#calculate_employees').val(Math.round(effective_employees));
            $('#calculate_employees').val(Math.round(total_employees));
            $('#calculate_employees').trigger('change');
            $('#total_employees_count').text(total_employees);
            // $('#effective_employees_count').text(total_employees);
            $('#permanent_employees').val(total_employees - parseInt($('#part_time_employees').val()));
            $('#permanent_employees').trigger('change');
        });
        $('#permanent_employees').on('keyup keypress blur change', function () {
            var node = $(this);
            var tmp = parseInt($(this).val());
            tmp = isNaN(tmp) ? 0 : tmp;
            if (tmp > total_employees) {
                $("#save_step_1").attr("disabled", true);
                $(node).prev().addClass('text-danger');
                $(node).addClass('is-invalid');
                $(node).parent().find('small.text-danger').remove();
                $(node).after("<small class=\"text-danger\">Permanent Employees Cannot be Greater than Total Employees.</small>");
            } else {
                $("#save_step_1").attr("disabled", false);
                var factor_part_time = '@php echo $effective_employees->factor_part_time @endphp';
                var factor_similar = '@php echo $effective_employees->factor_similar @endphp';
                var repeatative_employees = $('#repeatative_employees').val();
                var part_time_employees = $('#part_time_employees').val();
                var permanent_employees = $('#permanent_employees').val();
                var effective_employees = ((total_employees - part_time_employees - repeatative_employees) + (factor_part_time * part_time_employees) + (factor_similar * repeatative_employees));
                $('#effective_employees_count').text(Math.round(effective_employees));
                // $('#calculate_employees').val(Math.round(effective_employees));
                $('#calculate_employees').val(Math.round(total_employees));
                $('#calculate_employees').trigger('change');
                $(node).prev().removeClass('text-danger');
                $(node).removeClass('is-invalid');
                $(node).parent().find('small').remove();
                $('#part_time_employees').val(total_employees - tmp);
            }
        });
        $('#part_time_employees').on('keyup keypress blur change', function () {
            var node = $(this);
            var tmp = parseInt($(this).val());
            tmp = isNaN(tmp) ? 0 : tmp;
            if (tmp > total_employees && tmp >= 0) {
                $("#save_step_1").attr("disabled", true);
                $(node).prev().addClass('text-danger');
                $(node).addClass('is-invalid');
                $(node).parent().find('small.text-danger').remove();
                $(node).after("<small class=\"text-danger\">Part Time Employees Cannot be Greater than Total Employees.</small>");
            } else {
                $("#save_step_1").attr("disabled", false);
                var factor_part_time = '@php echo $effective_employees->factor_part_time @endphp';
                var factor_similar = '@php echo $effective_employees->factor_similar @endphp';
                var repeatative_employees = $('#repeatative_employees').val();
                var part_time_employees = $('#part_time_employees').val();
                var permanent_employees = $('#permanent_employees').val();
                var effective_employees = ((total_employees - part_time_employees - repeatative_employees) + (factor_part_time * part_time_employees) + (factor_similar * repeatative_employees));
                $('#effective_employees_count').text(Math.round(effective_employees));
                // $('#calculate_employees').val(Math.round(effective_employees));
                $('#calculate_employees').val(Math.round(total_employees));
                $('#calculate_employees').trigger('change');
                $(node).prev().removeClass('text-danger');
                $(node).removeClass('is-invalid');
                $(node).parent().find('small').remove();
                $('#permanent_employees').val(total_employees - tmp);
            }
        });
        $('#repeatative_employees').on('keyup keypress blur change', function () {
            var node = $(this);
            var tmp = parseInt($(this).val());
            tmp = isNaN(tmp) ? 0 : tmp;
            if (tmp > total_employees) {
                $("#save_step_1").attr("disabled", true);
                $(node).prev().addClass('text-danger');
                $(node).addClass('is-invalid');
                $(node).parent().find('small.text-danger').remove();
                $(node).after("<small class=\"text-danger\">Repeatative Employees Cannot be Greater than Total Employees.</small>");
            } else {
                $("#save_step_1").attr("disabled", false);
                var factor_part_time = '@php echo $effective_employees->factor_part_time @endphp';
                var factor_similar = '@php echo $effective_employees->factor_similar @endphp';
                var repeatative_employees = $('#repeatative_employees').val();
                var part_time_employees = $('#part_time_employees').val();
                var permanent_employees = $('#permanent_employees').val();
                var effective_employees = ((total_employees - part_time_employees - repeatative_employees) + (factor_part_time * part_time_employees) + (factor_similar * repeatative_employees));
                // $('#calculate_employees').val(Math.round(effective_employees));
                $('#calculate_employees').val(Math.round(total_employees));
                $(node).prev().removeClass('text-danger');
                $(node).removeClass('is-invalid');
                $(node).parent().find('small').remove();
            }
        });
        $('#more_contact').click(function () {
            var $divCopy = $('#contact_cloneable').clone();
            $($divCopy).show();
            $($divCopy).attr('id', "");
            $($divCopy).find('input').prop('disabled', false);
            $('#cloned_contact').append($divCopy);
        });
        $('#step3').submit(function (e) {
            e.preventDefault();
            form = $(this);
            var request = $(form).serialize();
            if (company_id != 0) {
                request += "&company_id=" + company_id;
            }
            $.ajax({
                type: "POST",
                url: "{{ route('company.update') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success") {
                        $('a[href="#stepTab3"]').parent().removeClass('pro_working');
                        $('a[href="#stepTab3"]').parent().addClass('pro_done');
                        $('a[href="#stepTab3"]').parent().removeClass('active');
                        $('a[href="#stepTab3"]').parent().addClass('nonactive');
                        $('a[href="#stepTab4"]').parent().removeClass('pro_pending');
                        $('a[href="#stepTab4"]').parent().addClass('pro_working');
                        $('a[href="#stepTab4"]').parent().removeClass('nonactive');
                        $('a[href="#stepTab4"]').parent().addClass('active');
                        // $('a[href="#stepTab4"]').trigger('click');
                        $('.company-step').removeClass('active show');
                        $('#stepTab4').addClass('active show');
                        $(form).append('<input type="hidden" name="company_id" value="' + response.data.company.id + '">');
                        $(form).append('<input type="hidden" name="action" value="update">');
                        var current_progress = parseInt($('#progressValue').text());
                        var now_progress = current_progress + 25;
                        $('#progressValue').text(now_progress + '%');
                        $('#progressBar').css('width', now_progress + '%');
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('#step4').submit(function (e) {
            e.preventDefault();
            form = $(this);
            var request = $(form).serialize();
            if (company_id != 0) {
                request += "&company_id=" + company_id;
            }
            var not_saved_standards = "";
            process_final_save = true;
            $.each(selected_standards_saved, function (id, status) {
                if (!status) {
                    process_final_save = false;
                    not_saved_standards += standards['id_' + id].name + " ";
                }
            });
            setTimeout(function () {
                if (process_final_save) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.update') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            ajaxResponseHandler(response, form);
                            if (response.status == "success") {
                                /*$.each(response.data.company_standards, function(i, obj){
                                    var clone = $('#standard_certificate').clone();
                                    $(clone).removeAttr('id');
                                    $(clone).attr('data-company-standard-id', obj.id);
                                    $(clone).attr('data-standard-id', obj.standard.id);
                                    $(clone).find('.standard_name').text(obj.standard.number);
                                    $(clone).show();
                                    // $('#final_save').prepend(clone);
                                    $('form#step5').append(clone);
                                    $('.datefield').datepicker();
                                });*/
                                $('a[href="#stepTab4"]').parent().removeClass('pro_working');
                                $('a[href="#stepTab4"]').parent().addClass('pro_done');
                                $('a[href="#stepTab4"]').parent().removeClass('active');
                                $('a[href="#stepTab4"]').parent().addClass('nonactive');
                                $('a[href="#stepTab5"]').parent().removeClass('pro_pending');
                                $('a[href="#stepTab5"]').parent().addClass('pro_working');
                                $('a[href="#stepTab5"]').parent().removeClass('nonactive');
                                $('a[href="#stepTab5"]').parent().addClass('active');
                                // $('a[href="#stepTab5"]').trigger('click');
                                $('.company-step').removeClass('active show');
                                $('#stepTab5').addClass('active show');
                                $(form).append('<input type="hidden" name="company_id" value="' + response.data.company.id + '">');
                                $(form).append('<input type="hidden" name="action" value="update">');
                                var current_progress = parseInt($('#progressValue').text());
                                var now_progress = current_progress + 50;
                                $('#progressValue').text(now_progress + '%');
                                $('#progressBar').css('width', now_progress + '%');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr["error"]("Standard(s) " + not_saved_standards + " Needs To Be Saved.");
                }
            }, 500);
        });
        $('#step5').submit(function (e) {
            e.preventDefault();
            form = $(this);
            var url = "{{ route('company.show', ['id' => "COMPANY_ID"]) }}".replace("COMPANY_ID", company_id);
            $.ajax({
                type: "GET",
                url: url,
                dataType: "json",
                cache: false,
                success: function (response) {


                    console.log(response);
                    $('a[href="#stepTab5"]').parent().removeClass('pro_working');
                    $('a[href="#stepTab5"]').parent().addClass('pro_done');
                    $('a[href="#stepTab5"]').parent().removeClass('active');
                    $('a[href="#stepTab5"]').parent().addClass('nonactive');
                    $('a[href="#stepTab6"]').parent().removeClass('pro_pending');
                    $('a[href="#stepTab6"]').parent().addClass('pro_working');
                    $('a[href="#stepTab6"]').parent().removeClass('nonactive');
                    $('a[href="#stepTab6"]').parent().addClass('active');

                    $('#stepTab6').find('.company_name').text(response.name);
                    $('#stepTab6').find('.company_region').text(response.region.title);
                    $('#stepTab6').find('.company_country').text(response.country.name);
                    $('#stepTab6').find('.company_sister_company').text(response.sister_company_name);
                    $('#stepTab6').find('.company_website').text(response.website);
                    $('#stepTab6').find('.company_head_office').text(response.address);
                    $('#stepTab6').find('.company_key_process').text(response.key_process);
                    $('#stepTab6').find('.company_city').text(response.city.name);
                    $('#stepTab6').find('.company_no_of_employees').text(response.total_employees);

                    $('#stepTab6').find('.company_total_location').text(response.child_companies.length + 1);
                    if (response.child_companies.length > 0) {
                        var html = $('#companyMoreLocations').find('.company_more_location').html();
                        $('#companyMoreLocations').html("");
                        $('#companyMoreLocations').append('<div class="col-md-12 company_more_location" style="display: none;">' + html + '</div>');
                        $(response.child_companies).each(function (k, v) {
                            console.log(v);
                            $('#companyMoreLocations').append('<div class="col-md-12 company_more_location">' + html + '</div>');
                            setTimeout(function () {
                                $($('#stepTab6 #companyMoreLocations').find('.company_more_address').get(k + 1)).text(v.address);
                                $($('#stepTab6 #companyMoreLocations').find('.company_more_key_process').get(k + 1)).text(v.key_process);
                                $($('#stepTab6 #companyMoreLocations').find('.company_more_country').get(k + 1)).text(v.country.name);
                                $($('#stepTab6 #companyMoreLocations').find('.company_more_city').get(k + 1)).text(v.city.name);
                                $($('#stepTab6 #companyMoreLocations').find('.company_more_no_of_employees').get(k + 1)).text(v.total_employees);
                            });
                        });
                    }

                    $('#stepTab6').find('.company_total_employees').text(response.permanent_employees + response.part_time_employees);
                    $('#stepTab6').find('.company_permanent_employees').text(response.permanent_employees);
                    $('#stepTab6').find('.company_part_time_employees').text(response.part_time_employees);
                    $('#stepTab6').find('.company_similar_tasks').text(response.repeatative_employees);
                    $('#stepTab6').find('.company_shifts').text(response.shift);

                    $('#stepTab6').find('.company_scope_to_certify').text(response.scope_to_certify);
                    // $('#companyIafCodes').html("");
                    // $(response.company_i_a_fs).each(function (k, v) {
                    //     console.log(v);
                    //     $('#companyIafCodes').append('<p class="results_ans">' + v.i_a_f.code + '| ' + v.i_a_f.name + '</p>');
                    // });
                    // $('#companyIasCodes').html("");
                    // $(response.company_i_a_ss).each(function (k, v) {
                    //     console.log(v);
                    //     $('#companyIasCodes').append('<p class="results_ans">' + v.i_a_s.code + '| ' + v.i_a_s.name + '</p>');
                    // });
                    $('#stepTab6').find('.company_outsourced_processed').text(response.outsourced_process_details == '' ? '-' : response.outsourced_process_details);
                    $('#stepTab6').find('.company_general_remarks').text(response.general_remarks == '' ? '-' : response.general_remarks);

                    $('#companyContactNumbers tbody').html("");
                    $(response.contacts).each(function (k, v) {
                        console.log(v);
                        $('#companyContactNumbers tbody').append('<tr><td>' + (v.type == 'primary' ? 'Yes' : 'No') + '</td><td>' + v.title + '</td><td>' + v.name + '</td><td>' + v.position + '</td><td>' + v.contact + '</td><td>' + v.landline + '</td><td>' + v.email + '</td></tr>');
                    });

                    if (response.company_standards.length > 0) {
                        var html = $('#companyStandardDetails').find('.company_standards').html();
                        $('#companyStandardDetails').html("");
                        $('#companyStandardDetails').append('<div class="col-md-12 company_standards" style="display: none;">' + html + '</div>');
                        $(response.company_standards).each(function (k, v) {
                            console.log(v);
                            $('#companyStandardDetails').append('<div class="col-md-12 company_standards">' + html + '</div>');
                            setTimeout(function () {
                                $($('#stepTab6 #companyStandardDetails').find('.company_standard_title').get(k + 1)).text(v.standard.name);
                                $($('#stepTab6 #companyStandardDetails').find('.company_transfered').get(k + 1)).text(v.client_type == 'new' ? 'No' : 'Yes');

                                if (v.client_type != 'new') {

                                }

                                $($('#stepTab6 #companyStandardDetails').find('.company_proposed_complexity').get(k + 1)).text(v.proposed_complexity.ucFirst());
                                $($('#stepTab6 #companyStandardDetails').find('.company_surveillance_frequency').get(k + 1)).text(v.surveillance_frequency.ucFirst());
                                $($('#stepTab6 #companyStandardDetails').find('.company_accreditations').get(k + 1)).text(v.accreditations.map(arr => arr.name).join());

                                $($('#stepTab6 #companyStandardDetails').find('.company_currency').get(k + 1)).text(v.initial_certification_currency);
                                $($('#stepTab6 #companyStandardDetails').find('.company_initial_amount').get(k + 1)).text(v.initial_certification_amount);
                                $($('#stepTab6 #companyStandardDetails').find('.company_surveillance_amount').get(k + 1)).text(v.surveillance_amount);
                                $($('#stepTab6 #companyStandardDetails').find('.company_re_audit_amount').get(k + 1)).text(v.re_audit_amount);
                            });
                        });
                    }

                    $('#document_uploads').html(response.uploaded_docs);

                    $('a[href="#stepTab6"]').trigger('click');
                    $('.company-step').removeClass('active show');
                    $('#stepTab6').addClass('active show');
                },
                error: function (response) {

                }
            });

        });
        $('form#step5').on('change', '.document_file', function () {
            var name = $(this)[0].files[0].name;
            $(this).next().text(name);
        });
        $('form#step5').on('change', 'select[name="old_document_files"]', function () {
            var parent_node = $(this).closest('.standard_certificates');
            if ($(this).val() != "") {
                var type = document_files[$(this).val()].type;
                var date = document_files[$(this).val()].date;
                $(parent_node).find('input[name="document_date"]').val(date);
                if ($(parent_node).find('select[name="document_type"]').val() == "") {
                    $(parent_node).find('select[name="document_type"] option[value="' + type + '"]').prop('selected', true);
                }
            }
        });
        $('form#step5').on('click', '.add_document', function (e) {
            var parent_node = $(this).closest('.standard_certificates');
            var document_type_title = $(parent_node).find('select[name="document_type"] option:selected').text();
            var document_type = $(parent_node).find('select[name="document_type"]').val();
            var old_document_file = $(parent_node).find('select[name="old_document_files"]').val();
            var file_clone = $(parent_node).find('input[name="document_file"]').clone();
            var document_date = $(parent_node).find('input[name="document_date"]').val();
            var company_standard_id = $(parent_node).data('company-standard-id');
            var standard_id = $(parent_node).data('standard-id');
            var is_old_file = false;

            if (document_type == "") {
                toastr['error']("Please Select a Document Type.");
                return;
            }
            if ($(file_clone).val() == "" && old_document_file == "") {
                toastr['error']("Please Select a File.");
                return;
            } else if ($(file_clone).val() != "") {
                var name = $(file_clone)[0].files[0].name;
            }
            if (old_document_file != "" && $(file_clone).val() == "") {
                file_clone = document_files[old_document_file].file_obj;
                name = document_files[old_document_file].name;
                is_old_file = true;
            }

            var temp = {
                name: name,
                file_obj: is_old_file ? file_clone : $(file_clone)[0].files[0],
                type: document_type_title,
                date: document_date,
                company_standard_id: company_standard_id,
                standard_id: standard_id
            };
            document_files[name + document_files.size] = temp;
            document_files.size = document_files.size + 1;

            $(parent_node).find('select[name="document_type"]').val("");
            $(parent_node).find('select[name="old_document_files"]').val("");
            $(parent_node).find('input[name="document_file"]').next().text("Choose File");
            $(parent_node).find('input[name="document_date"]').val("");

            var index = 1;
            var html = '<option value="">Select From Old Files</option>';
            var tbl_html = '';
            var already_processed = [];
            var already_done = [];
            $.each(document_files, function (i, instance) {
                if (i == 'size')
                    return;
                if (already_done.indexOf(instance.name) == -1) {
                    already_done = instance.name;
                    html += '<option value="' + i + '">' + instance.name + '</option>';
                }
                tbl_html += '<tr>';
                tbl_html += '<td>';
                tbl_html += index;
                tbl_html += '</td>';
                tbl_html += '<td>';
                tbl_html += instance.type;
                tbl_html += '</td>';
                tbl_html += '<td>';
                tbl_html += instance.name;
                tbl_html += '</td>';
                tbl_html += '<td>';
                tbl_html += instance.date;
                tbl_html += '</td>';
                tbl_html += '<td>';
                tbl_html += '<a href="#" class="remove_document" style="color: red;" data-document-index="' + i + '"><i class="fa fa-times" aria-hidden="true"></i></a>';
                tbl_html += '</td>';
                tbl_html += '</tr>';
                index++;
            });
            $(parent_node).find('tbody').html(tbl_html);
            $('.old_document_files').html(html);
            toastr['success']("Document Added.");
        });

        $(document).on('click', 'form#step5 .remove_document', function (e) {
            e.preventDefault();
            var index = $(this).data('document-index');
            $(this).closest('tr').remove();
            delete document_files[index];
            document_files.size = document_files.size - 1;
        });
        $('.standard_questionnaire').submit(function (e) {

            e.preventDefault();
            form = $(this);
            standard_id = $(form).data('standard-id');
            is_ims = $(form).find('input[name="is_ims"]').val();
            var request = new FormData(form[0]); //$(form).serialize();
            if (company_id != 0) {
                request.append('company_id', company_id);

            }


            if ($('#auditor-standard-food-codes' + standard_id).data('standard-family-id') == 23) {
                if ($('#auditor-standard-codes' + standard_id).find('tbody').find('td').length > 1 && $('#auditor-standard-food-codes' + standard_id).find('tbody').find('td').length > 1) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.update') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {

                            ajaxResponseHandler(response, form);
                            if (typeof response.data !== 'undefined' && typeof response.data.proposed_complexity !== 'undefined') {
                                $('#propsoedComplexity' + standard_id).css('display', '');
                                $('#propsoedComplexity' + standard_id).css('color', 'red');
                            } else {
                                $('#propsoedComplexity' + standard_id).css('display', 'none');
                            }
                            if (response.status == "success") {


                                $('#edit_standard_button_check' + standard_id).css('display', '');
                                $('#edit_standard' + standard_id).css('display', 'none');
                                //Editable
                                selected_standards.push(standard_id);
                                selected_standards_saved[standard_id] = true;
                                if (is_ims) {
                                    default_amounts_for_ims.initial_certification_amount = $(form).find('input[name="initial_certification_amount"]').val();
                                    default_amounts_for_ims.surveillance_amount = $(form).find('input[name="surveillance_amount"]').val();
                                    default_amounts_for_ims.re_audit_amount = $(form).find('input[name="re_audit_amount"]').val();
                                    default_accreditation_for_ims = [];
                                    // $('.standard_questionnaire input[name="accreditation_ids[]"]').prop('checked', false);
                                    $('input[name="default_ims_accreditation_ids[]"]').prop('checked', false);
                                    $.each($(form).find("input[name='accreditation_ids[]']:checked"), function (i, node) {
                                        default_accreditation_for_ims.push($(node).val());
                                        // $('.standard_questionnaire input[name="accreditation_ids[]"][value="'+ $(node).val() +'"]').prop('checked', true);
                                        $('input[name="default_ims_accreditation_ids[]"][value="' + $(node).val() + '"]').prop('checked', true);
                                    });
                                    $('select[name="default_ims_surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
                                }
                                $(form).find('input[name="company_standard_id"]').val(response.data.company_standard.id);
                                $(form).closest('.modal').modal('hide');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("IAF and Food Codes is required.");
                }

            } else if ($('#auditor-standard-energy-codes' + standard_id).data('standard-family-id') == 20) {
                if ($('#auditor-standard-codes' + standard_id).find('tbody').find('td').length > 1 && $('#auditor-standard-energy-codes' + standard_id).find('tbody').find('td').length > 1) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.update') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {

                            ajaxResponseHandler(response, form);
                            // if (typeof response.data !== 'undefined' && typeof response.data.proposed_complexity !== 'undefined') {
                            //     $('#propsoedComplexity' + standard_id).css('display', '');
                            //     $('#propsoedComplexity' + standard_id).css('color', 'red');
                            // } else {
                            //     $('#propsoedComplexity' + standard_id).css('display', 'none');
                            // }
                            if (response.status == "success") {

                                $('#edit_standard_button_check' + standard_id).css('display', '');
                                $('#edit_standard' + standard_id).css('display', 'none');
                                //Editable
                                selected_standards.push(standard_id);
                                selected_standards_saved[standard_id] = true;
                                if (is_ims) {
                                    default_amounts_for_ims.initial_certification_amount = $(form).find('input[name="initial_certification_amount"]').val();
                                    default_amounts_for_ims.surveillance_amount = $(form).find('input[name="surveillance_amount"]').val();
                                    default_amounts_for_ims.re_audit_amount = $(form).find('input[name="re_audit_amount"]').val();
                                    default_accreditation_for_ims = [];
                                    // $('.standard_questionnaire input[name="accreditation_ids[]"]').prop('checked', false);
                                    $('input[name="default_ims_accreditation_ids[]"]').prop('checked', false);
                                    $.each($(form).find("input[name='accreditation_ids[]']:checked"), function (i, node) {
                                        default_accreditation_for_ims.push($(node).val());
                                        // $('.standard_questionnaire input[name="accreditation_ids[]"][value="'+ $(node).val() +'"]').prop('checked', true);
                                        $('input[name="default_ims_accreditation_ids[]"][value="' + $(node).val() + '"]').prop('checked', true);
                                    });
                                    $('select[name="default_ims_surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
                                }
                                $(form).find('input[name="company_standard_id"]').val(response.data.company_standard.id);
                                $(form).closest('.modal').modal('hide');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("IAF and Energy Codes is required.");
                }

            } else {
                if ($('#auditor-standard-codes' + standard_id).find('tbody').find('td').length > 1) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.update') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {

                            ajaxResponseHandler(response, form);
                            if (typeof response.data !== 'undefined' && typeof response.data.proposed_complexity !== 'undefined') {
                                $('#propsoedComplexity' + standard_id).css('display', '');
                                $('#propsoedComplexity' + standard_id).css('color', 'red');
                            } else {
                                $('#propsoedComplexity' + standard_id).css('display', 'none');
                            }
                            if (response.status == "success") {

                                $('#edit_standard_button_check' + standard_id).css('display', '');
                                $('#edit_standard' + standard_id).css('display', 'none');
                                //Editable
                                selected_standards.push(standard_id);
                                selected_standards_saved[standard_id] = true;
                                if (is_ims) {
                                    default_amounts_for_ims.initial_certification_amount = $(form).find('input[name="initial_certification_amount"]').val();
                                    default_amounts_for_ims.surveillance_amount = $(form).find('input[name="surveillance_amount"]').val();
                                    default_amounts_for_ims.re_audit_amount = $(form).find('input[name="re_audit_amount"]').val();
                                    default_accreditation_for_ims = [];
                                    // $('.standard_questionnaire input[name="accreditation_ids[]"]').prop('checked', false);
                                    $('input[name="default_ims_accreditation_ids[]"]').prop('checked', false);
                                    $.each($(form).find("input[name='accreditation_ids[]']:checked"), function (i, node) {
                                        default_accreditation_for_ims.push($(node).val());
                                        // $('.standard_questionnaire input[name="accreditation_ids[]"][value="'+ $(node).val() +'"]').prop('checked', true);
                                        $('input[name="default_ims_accreditation_ids[]"][value="' + $(node).val() + '"]').prop('checked', true);
                                    });
                                    $('select[name="default_ims_surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
                                }
                                $(form).find('input[name="company_standard_id"]').val(response.data.company_standard.id);
                                $(form).closest('.modal').modal('hide');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("IAF Codes is required.");
                }

            }
        });
        $('select[name="surveillance_frequency"]').change(function () {
            var surveillance_frequency = $(this).val();
            var standard_id = $(this).closest('form').data('standard-id');
            if ($('#step4').find('input[value="' + standard_id + '"]').data('is-ims-enabled')) {
                default_surveillance_frequency_for_ims = surveillance_frequency;
            }
        });


        function standardCheckAction(node, edit = true) {
            var standard_id = $(node).data('standard-id');
            $('#propsoedComplexity' + standard_id).css('display', 'none');
            standardId = standard_id;

            $("#old_cb_certificate_issue_date" + standardId).datepicker().on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                $('#old_cb_certificate_expiry_date' + standardId).datepicker('setStartDate', minDate);
            });

            $("#old_cb_certificate_expiry_date" + standardId).datepicker();
            var company_id = $('#company_id').val();
            if ($(node).is(':checked')) {
                companyStandardCodesView(standard_id, company_id);
                companyStandardFoodCodesView(standard_id, company_id);
                companyStandardEnergyCodesView(standard_id, company_id);
            }


            var is_ims_enabled = $(node).data('is-ims-enabled');
            if ($(node).is(':checked')) {

                $(node).next().find('i').show();
                previousFrequncy('#previousSurveillanceFrequency' + standard_id, standard_id);
                var modal_id = $(node).data('modal-id');
                $('#' + modal_id).modal(
                    {
                        backdrop: 'static',
                        keyboard: false
                    }
                );
                selected_standards_saved[standard_id] = false;
                // if (is_ims_enabled && !edit) {
                //     var already_ims_confirmed = ims_standards_selected.indexOf(standard_id);
                //     ims_standards_selected.push(standard_id);
                // }
                // if (ims_standards_selected.length > 1 && already_ims_confirmed == -1 && !edit) {
                //     if (confirm("Do You Want to Make an IMS?")) {
                //         $('#' + modal_id).find('form input[name="is_ims"]').val(1);
                //         $('#' + modal_id).find('form select[name="surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
                //         $('#' + modal_id).find('form input[name="initial_certification_amount"]').val(default_amounts_for_ims.initial_certification_amount);
                //         $('#' + modal_id).find('form input[name="surveillance_amount"]').val(default_amounts_for_ims.surveillance_amount);
                //         $('#' + modal_id).find('form input[name="re_audit_amount"]').val(default_amounts_for_ims.re_audit_amount);
                //
                //         $('#ims_standards_list').html("");
                //         $(ims_standards_selected).each(function (i, id) {
                //             $('#ims_standards_list').append('<div class="col-md-2 floting"><input type="checkbox" name="ims_standard_ids[]" value="' + id + '" class="ims_standard_check" checked="">&nbsp;<label>' + standards['id_' + id].name + '</label></div>');
                //         });
                //         $(default_accreditation_for_ims).each(function (i, id) {
                //             $('#' + modal_id).find('input[name="accreditation_ids[]"][value="' + id + '"]').prop('checked', true);
                //         });
                //         if (ims_standards_selected.length > 0) {
                //             $('#ims_default_values').show();
                //         } else {
                //             $('#ims_default_values').hide();
                //         }
                //     } else {
                //         var index = ims_standards_selected.indexOf(standard_id);
                //         if (index > -1) {
                //             ims_standards_selected.splice(index, 1);
                //         }
                //     }
                // }
                // console.log(ims_standards_selected);
            } else {
                if (confirm("Do You Really Wants to Remove This?")) {
                    $(node).next().find('i').hide();
                    /* Ajax Call for Removing Company Standard Here */

                    // if (selected_standards_saved[standard_id]) {
                    var request = new FormData($('form#step4')[0]);
                    if (company_id != 0) {
                        request.append('company_id', company_id);
                        request.append('standard_id', standard_id);
                        request.append('standard_action', "remove");
                    }
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.update') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            ajaxResponseHandler(response, form);
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                    // }

                    delete selected_standards[selected_standards.indexOf(standard_id)];
                    delete selected_standards_saved[standard_id];

                    var index = ims_standards_selected.indexOf(standard_id);
                    if (index > -1) {
                        $('#unchecked_ims_standards_list').find('input[value=' + standard_id + ']').remove();
                        $('#unchecked_ims_standards_list').append('<input type="hidden" name="unchecked_ims_standard_ids[]" value="' + standard_id + '">');
                        ims_standards_selected.splice(index, 1);
                        $('#ims_standards_list').html("");
                        if (ims_standards_selected.length > 1) {
                            $(ims_standards_selected).each(function (i, id) {
                                $('#ims_standards_list').append('<div class="col-md-2 floting"><input type="checkbox" name="ims_standard_ids[]" value="' + id + '" class="ims_standard_check" checked="">&nbsp;<label>' + standards['id_' + id].name + '</label></div>');
                            });
                        } else {
                            $('#ims_standards_list').html('<p>No IMS Standard Got Selected Yet.</p>');
                        }
                    }
                } else {
                    $(node).prop('checked', true);
                }
            }
        }

        $('.standard_check').click(function () {
            var standard_id = $(this).data('standard-id');
            var company_check_id = $('#company_id').val();


            var request = new FormData();
            if (company_check_id != 0) {
                request.append('company_id', company_check_id);
                request.append('standard_id', standard_id);
            }

            var newThis = this;
            $.ajax({
                type: "POST",
                url: "{{ route('company.checkStandard') }}",
                data: request,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (response) {

                    if ($(newThis).is(':checked')) {
                        if (response.status == 'success') {
                            standardCheckAction(newThis, false);
                        } else {
                            toastr[response.status](response.message);
                            if ($(newThis).is(':checked')) {
                                $(newThis).prop('checked', false);
                                $(newThis).trigger('change');
                            }
                        }
                    } else {
                        standardCheckAction(newThis, false);
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('.edit_standard').click(function (e) {
            e.preventDefault();

            standardCheckAction($(this).closest('div').find('input'), true);
        });
        $('#ims_standards_list').on('.ims_standard_check', 'click', function () {

            var standard_id = $(this).val();
            if (!$(this).is(':checked')) {
                $('#unchecked_ims_standards_list').find('input[value=' + standard_id + ']').remove();
                $('#unchecked_ims_standards_list').append('<input type="hidden" name="unchecked_ims_standard_ids[]" value="' + standard_id + '">');
            }
        });


        $('.transfered_customer').click(function () {
            if ($(this).is(':checked')) {
                $(this).closest('form').find('.transfered_fields').find('input,select').prop('disabled', false);
                $(this).closest('form').find('.transfered_fields').show();
                $('input[name="initial_certification_amount"]').prop('readonly', true);
            } else {
                $(this).closest('form').find('.transfered_fields').find('input,select').prop('disabled', true);
                $(this).closest('form').find('.transfered_fields').hide();
                $('input[name="initial_certification_amount"]').prop('readonly', false);
            }
        });
        $('.is_transfer_standard').click(function () {
            if ($(this).is(':checked')) {
                $(this).val(1);
                $(this).closest('form').find('.is_transfer_standard_fields').find('input,select').prop('disabled', false);
                $(this).closest('form').find('.is_transfer_standard_fields').show();
            } else {
                $(this).val(0);
                $(this).closest('form').find('.is_transfer_standard_fields').find('input,select').prop('disabled', true);
                $(this).closest('form').find('.is_transfer_standard_fields').hide();
            }
        });
        $('.transfer_year').datepicker({
            format: "yyyy",
            orientation: "bottom",
            keyboardNavigation: false,
            viewMode: "years",
            minViewMode: "years"
        });
        $('form#step3').on('click', '.add_main_contact', function () {
            var row = $(this).closest('.main-contact-form-clone').clone();
            $(row).find('input').val("");
            $('form#step3').find('.main_contact_form').last().after(row);
            $('form#step3').find('.main_contact_form').find('.add_remove').html('<br><button type="button" class="btn btn-sm btn-primary add_main_contact" style="margin-top: 4px;" disabled><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-danger remove_main_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
            $('form#step3').find('.main_contact_form').last().find('.add_remove').html('<br><button type="button" class="btn btn-sm btn-primary add_main_contact" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn  btn-danger remove_main_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
            $.each($(row).closest('form').find('input.primary'), function (k, node) {
                $(node).val(k + 1);
            });
        });
        $('form#step3').on('click', '.remove_main_contact', function () {
            $(this).closest('.main_contact_form').remove();
            if ($('form#step3').find('.add_remove').length > 1) {
                $('form#step3').find('.main_contact_form').find('.add_remove').html('<br><button type="button" class="btn btn-sm btn-primary add_main_contact" style="margin-top: 4px;" disabled><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn  btn-danger remove_main_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
                $('form#step3').find('.main_contact_form').last().find('.add_remove').html('<br><button type="button" class="btn btn-sm btn-primary add_main_contact" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-danger remove_main_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
            } else {
                $('form#step3').find('.main_contact_form').last().find('.add_remove').html('<br><button type="button" class="btn btn-sm btn-primary add_main_contact" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn  btn-danger remove_main_contact" style="margin-top: 4px;" disabled><i class="fa fa-minus"></i></button>');
            }
            $.each($('form#step3').find('input.primary'), function (k, node) {
                $(node).val(k + 1);
            });
        });
        var default_currency = "USD";
        var intlTelInputConfig = {
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get("{{ route('ajax.ipInfo') }}", function () {
                }, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    var default_currency = (resp && resp.currency) ? resp.currency : "USD";
                    $('select[name="currency_unit"] option[value="' + default_currency + '"]').prop('selected', true);
                    callback(countryCode);
                });
            },
            utilsScript: "{{ asset('plugins/intlTelInput/js/utils.js') }}", // just for formatting/placeholders etc
            blur: function () {
                console.log($(this));
            }
        };

        $('#contacts_modal').on('click', '.add_contact', function () {
            var row = $(this).closest('.contact_row').clone();
            $(row).find('input').val("");
            $('#contacts_modal').find('.modal-body').append(row);
            $('#contacts_modal').find('.contact_row').find('.add_remove').html('<button type="button" class="btn btn-sm btn-primary add_contact" style="margin-top: 4px;" disabled><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-sm btn-danger remove_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
            $('#contacts_modal').find('.contact_row:last-child').find('.add_remove').html('<button type="button" class="btn btn-sm btn-primary add_contact" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-sm btn-danger remove_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
            $(row).find('input.contact_number').intlTelInput("destroy");
            $(row).find('input.contact_number').intlTelInput(intlTelInputConfig);
        });
        $('#contacts_modal').on('click', '.remove_contact', function () {
            $(this).closest('.contact_row').remove();
            if ($('#contacts_modal').find('.remove_contact').length > 1) {
                $('#contacts_modal').find('.contact_row').find('.add_remove').html('<button type="button" class="btn btn-sm btn-primary add_contact" style="margin-top: 4px;" disabled><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-sm btn-danger remove_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
                $('#contacts_modal').find('.contact_row:last-child').find('.add_remove').html('<button type="button" class="btn btn-sm btn-primary add_contact" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-sm btn-danger remove_contact" style="margin-top: 4px;"><i class="fa fa-minus"></i></button>');
            } else {
                $('#contacts_modal').find('.contact_row:last-child').find('.add_remove').html('<button type="button" class="btn btn-sm btn-primary add_contact" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-sm btn-danger remove_contact" style="margin-top: 4px;" disabled><i class="fa fa-minus"></i></button>');
            }

        });
        $('#contacts_modal').on('keyup blur', 'input.contact_number', function () {
            var number = $(this).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
            if (number == '' || !$(this).intlTelInput("isValidNumber")) {
                has_error = true;
                $(this).closest('.form-group').addClass('text-danger');
                $(this).addClass('is-invalid');
                $(this).closest('.form-group').find('small.text-danger').remove();
                $(this).closest('.form-group').append('<small class="text-danger" style="float:left">Invalid Contact Number.</small>');
            } else {
                $(this).closest('.form-group').removeClass('text-danger');
                $(this).removeClass('is-invalid');
                $(this).closest('.form-group').find('small.text-danger').remove();
            }
        });
        $('#save_contact').click(function () {
            var contact_list = "";
            var has_error = false;
            $.each($('#contacts_modal input.contact_number'), function (i, node) {
                var number = $(this).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
                if (number != '' && !$(this).intlTelInput("isValidNumber")) {
                    $(".btn_save").attr("disabled", true);
                    has_error = true;
                    $(this).closest('.form-group').addClass('text-danger');
                    $(this).addClass('is-invalid');
                    $(this).closest('.form-group').find('small.text-danger').remove();
                    $(this).closest('.form-group').append('<small class="text-danger" style="float:left">Invalid Contact Number.</small>');
                } else {
                    $(".btn_save").attr("disabled", false);
                    $(this).closest('.form-group').removeClass('text-danger');
                    $(this).removeClass('is-invalid');
                    $(this).closest('.form-group').find('small.text-danger').remove();
                    if (number != '')
                        contact_list += number + ",";
                }
                console.log(number);
                $(this).val("");
            }).promise().done(function () {
                if (!has_error) {
                    contact_list = contact_list.substring(0, contact_list.length - 1);
                    $(clicked_contact_node).val(contact_list);
                    $(this).closest('.modal').modal('hide');
                }
            });
        });
        $('form#step3').on('focus', 'input[name="contact[]"],input[name="landline[]"]', function () {
            clicked_contact_node = $(this);
            var inp = $(this).val();
            var contacts = inp.split(',');
            console.log(contacts);
            var count = $('#contacts_modal .contact_row').length - 1;
            $('#contacts_modal .contact_row').each(function (i, node) {
                if (i < count)
                    $(node).remove();
                else
                    $(node).find('input').val('');
            });
            if (contacts.length > 0) {
                $.each(contacts, function (i, contact) {
                    if (contact != "") {
                        var row = $('#contacts_modal').find('.contact_row').last();
                        $(row).find('.contact_number').val(contact);
                        $(row).find('.add_remove button.add_contact').trigger('click');
                    }
                });
            }
            $('#contacts_modal').modal('show');
        });

        $('.dashboard_tabs').on('click', '.btn_back,.pro_done', function () {
            // $('button.btn_back,.pro_done').click(function(){
            var form = $(this).closest('form');
            var id = $(this).data('back-id').replace(/[A-Za-z#]*/, '') || $(this).find('a').attr('href').replace(/[A-Za-z#]*/, '');
            var curr_step = $(form).find('input[name="step"]').val() || $(".company-step:visible").attr('id').replace(/[A-Za-z#]*/, '');
            var request = "id=" + id + "&company_id=" + company_id;

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.companyProgressUpdate') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        // $('a[href="#stepTab'+ curr_step +'"]').parent().removeClass('pro_working');
                        // $('a[href="#stepTab'+ curr_step +'"]').parent().addClass('pro_done');
                        $('a[href="#stepTab' + curr_step + '"]').parent().removeClass('active');
                        $('a[href="#stepTab' + curr_step + '"]').parent().addClass('nonactive');
                        $('a[href="#stepTab' + id + '"]').parent().removeClass('pro_done');
                        $('a[href="#stepTab' + id + '"]').parent().addClass('pro_working');
                        $('a[href="#stepTab' + id + '"]').parent().removeClass('nonactive');
                        $('a[href="#stepTab' + id + '"]').parent().addClass('active');
                        $('.company-step').removeClass('active show');
                        $('#stepTab' + id).addClass('active show');
                        // $('a[href="#'+ id +'"]').trigger('click');
                        // $(form).append('<input type="hidden" name="company_id" value="' + response.data.company.id + '">');
                        // $(form).append('<input type="hidden" name="action" value="update">');

                        // $('a[href="#'+ id +'"]').click();
                        var current_progress = parseInt($('#progressValue').text());
                        if (curr_step != "6") {
                            var now_progress = current_progress - 25;
                            $('#progressValue').text(now_progress + '%');
                            $('#progressBar').css('width', now_progress + '%');
                        }

                    } else {
                        toastr['error'](response.message);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('ul.nav.nav-tabs').on('click', '.pro_pending', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        $('.special_dropdown').change(function () {
            var question_id = $(this).data('id');
            var value = $(this).val();
            var question_title = $('#' + question_id).data('question-title');
            var html = '<option>Select ' + question_title + '</option>';
            $.each(special_dropdown[question_id], function (k, obj) {
                if (k == value) {
                    $.each(obj, function (i, v) {
                        if (v != '')
                            html += '<option value="' + v + '">' + v + '</option>';
                    })
                }
            });
            $('#' + question_id).html(html);
        });
        $('.special_dropdown_multiple').change(function () {
            var question_id = $(this).data('id');
            var value = $(this).val();
            var question_title = $('#' + question_id).data('question-title');
            var html = '';//<option>Select '+ question_title +'</option>';
            $(this).find('option:selected').each(function (j, obj1) {
                var value = $(obj1).val();
                $.each(special_dropdown_multiple[question_id], function (k, obj) {
                    if (k == value) {
                        $.each(obj, function (i, v) {
                            if (v != '')
                                html += '<option value="' + v + '">' + v + '</option>';
                        });
                    }
                });
            });
            setTimeout(function () {
                $('#' + question_id).html(html);
            });
        });
        $('.datefield').datepicker();


        function dateStandardBased(standardId) {

        }


        $('.contact_number').intlTelInput(intlTelInputConfig);

        $(document).ready(function () {
            $('.has_dependent_fields').on('keyup keydown change', function () {
                var node = $(this);
                var answer = $(node).val();
                var right_answer = $(node).data('right-answer');
                var dependent_fields = $(node).data('dependent-fields').toString();
                var tmp = dependent_fields.split(',');
                if (right_answer == answer) {
                    $(tmp).each(function (i, k) {
                        $('#question' + k).prop('disabled', false);
                    });
                } else {
                    $(tmp).each(function (i, k) {
                        $('#question' + k).prop('disabled', true);
                    });
                }
            });
            $('.standard_questions select[name="old_cb_audit_activity_stage_id"]').change(function () {
                console.log('changed');
                var node = $(this).closest('.standard_questions');
                var selected_node = $(this);
                $(node).find('.previous_activities_dates .activity_type').html('<label>Audit Type</label>');
                $(node).find('.previous_activities_dates .activity_date').html('<label>Previous Audit Date</label>');
                var end_now = false;
                $($(this).find('option')).each(function (i, opt) {
                    if (end_now)
                        return false;
                    if ($(opt).val() == '')
                        return;
                    if ($(opt).val() == $(selected_node).val())
                        end_now = true;
                    var stroption = $(opt).text();
                    stroption = stroption.replace(/\s+/g, '');
                    $(node).find('.previous_activities_dates .activity_type').append('<p>' + $(opt).text() + '</p>');
                    $(node).find('.previous_activities_dates .activity_date').append('<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div><input type="text" class="form-control datefield float-right active" id="old_cb_audit_activity_date' + stroption + '' + standardId + '" name="old_cb_audit_activity_date[' + $(opt).val() + ']" required="" onkeypress="return false;"></div>');
                });
                // $(node).find('.datefield').datepicker();


                $("#old_cb_certificate_issue_date" + standardId).datepicker().on('changeDate', function (selected) {

                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateStage2' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateStage2" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance1' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance1" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance2' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance2" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance3').datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance3" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance4' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance4" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance5').datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance5" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateReaudit' + standardId).datepicker('setStartDate', minDate);
                });

                $('#old_cb_audit_activity_dateReaudit' + standardId).datepicker();
            });


        });


        var standard_document_node = null;
        $('.open_standards_modal').click(function () {
            standard_document_node = $(this);
            var document_type = $(this).data('document-type');
            console.log(document_type);
            var company_id = $('#company_id').val();
            var request = {"company_id": company_id};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.get.standards')}}',
                data: request,
                success: function (response) {
                    var html = '';
                    $.each(response.data, function (i, val) {

                        var std = val.standard;
                        html += '<label><input type="radio" name="standards[]" class="standard_has_documents" value="' + std.id + '">&nbsp;' + std.name + '</label>';
                    });

                    setTimeout(function () {
                        html += '<input type="hidden" name="document_type" value="' + document_type + '">';
                        if (response.data.length > 0) {
                            $('#standards_for_documents').find('.selected_standards').html(html);
                        }
                        $('#standards_for_documents').modal('show');
                    });
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });


            // if (selected_standards.length <= 0) {
            //     $('.standard_check:checked').each(function (k, node) {
            //
            //         var standard_id = $(node).val();
            //         selected_standards.push(standard_id);
            //
            //     });
            //
            // }
            //
            // var html = '';
            // $.each(selected_standards, function (i, val) {
            //     var std = standards['id_' + val];
            //     html += '<label><input type="radio" name="standards[]" class="standard_has_documents" value="' + std.id + '">&nbsp;' + std.name + '</label>';
            // });
            // setTimeout(function () {
            //     html += '<input type="hidden" name="document_type" value="' + document_type + '">';
            //     if (selected_standards.length > 0) {
            //         $('#standards_for_documents').find('.selected_standards').html(html);
            //     }
            //     $('#standards_for_documents').modal('show');
            // });
        });
        $('.selected_standards').on('click', '.select_all_standards', function () {
            if ($(this).is(':checked')) {
                $(this).closest('div').find('input').prop('checked', true);
            } else {
                $(this).closest('div').find('input').prop('checked', false);
            }
        });
        $('#save_standard_documents').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            if (form.find('input.standard_has_documents:checked').length > 0) {
                var is_all_checked = form.find('input.select_all_standards').is(':checked');
                if (is_all_checked)
                    form.find('input.select_all_standards').prop('checked', false);
                var request = new FormData(form[0]);
                if (is_all_checked)
                    form.find('input.select_all_standards').prop('checked', true);
                if (company_id != 0) {
                    request.append('company_id', company_id);
                }
                request.append('step', 5);
                request.append('step_action', 'upload_documents');
                // request.append('documents[]', form.find('.documents')[0]);
                // request.append('date', form.find('input[name="date"]').val());
                // request.append('document_type', form.find('input[name="document_type"]').val());
                $.ajax({
                    url: "{{ route('company.update') }}",
                    method: 'POST',
                    data: request,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log(response);
                        ajaxResponseHandler(response, form);
                        if (response.status == "success") {
                            var uploaded_documents_standards = '';
                            $(form.find('input[name="standards[]"]:checked')).each(function (i, elm) {
                                uploaded_documents_standards += $(elm).parent().text() + ', ';
                            });

                            var document_type = form.find('input[name="document_type"]').val();
                            $('#stepTab5').find('div.' + document_type).append(response.data.uploaded_doc_card);
                            $('#stepTab5').find('li.' + document_type + ' a').trigger('click');
                            // $(standard_document_node).closest('div.uploadDocs').find('div.uploaded_documents').append('<div class="col-md-12 uploaded_documents_card"><div class="card collapsed-card cardMargin"><img src="{{ asset('img/closeIcon.png') }}" alt="Close" class="iconClose remove_uploaded_documents" data-standard-document-ids="'+ JSON.stringify(response.data.documents) +'"><div class="card-header cardHead"><h3 class="card-title">'+ document_type.replace('_', ' ').replace(/\b[a-z]/g, function(letter) { return letter.toUpperCase(); }) +'</h3> <div class="card-tools"><button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button> </div></div><div class="card-body p-0" style="display: none;"><ul class="products-list product-list-in-card pl-2 pr-2"><li class="item"><div class="col-md-6 floting text-left"><label>Standards</label></div><div class="col-md-6 floting text-right"><label>'+ (form.find('input[name="date"]').val()) +'</label></div><div class="col-md-12 text-left">'+ uploaded_documents_standards +'</div><div class="clearfix"></div></li></ul></div></div></div>');
                            form.find('input[name="standards[]"]').prop('checked', false);
                            form.find('input[name="date"]').val('');
                            $('#standards_for_documents').modal('hide');

                            $('#upld100').empty();
                            $('#save_standard_documents').trigger('reset');
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please select atleast one standard.");
            }
        });
        $('#step5').on('click', '.remove_uploaded_documents', function (e) {
            node = $(this);
            var documents_to_remove = $(this).data('standard-document-ids');
            var request = new FormData();
            if (company_id != 0) {
                request.append('company_id', company_id);
            }
            request.append('step', 5);
            request.append('step_action', 'remove_document');
            request.append('document_ids', documents_to_remove);
            $.ajax({
                url: "{{ route('company.update') }}",
                method: 'POST',
                data: request,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log($(this));
                    toastr['success'](response.message);
                    node.closest('div.uploaded_documents_card').remove();
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
            /* Back end remove Ajax call here */
        });
        $('#finalSave').click(function () {
            if (confirm("Are You Sure? You Really Want To Final Save?")) {
                var request = new FormData();
                if (company_id != 0) {
                    request.append('company_id', company_id);
                }
                request.append('step', 6);
                request.append('step_action', 'final_save');
                $.ajax({
                    url: "{{ route('company.update') }}",
                    method: 'POST',
                    data: request,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        toastr[response.status](response.message);
                        if (response.status == 'success') {
                            window.location = "{{ route('company.show', ['id' => "_COMPANY_ID_"]) }}".replace('_COMPANY_ID_', response.data.id);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        });

        function removeMedia(id) {


            var request = "id=" + id;


            $.ajax({
                url: "{{ route('company.media.remove') }}",
                method: 'POST',
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {

                    console.log($(this));
                    toastr['success'](response.message + '.It will refresh on final Save');

                    $('a[href="#stepTab5"]').parent().addClass('pro_working');
                    $('a[href="#stepTab5"]').parent().removeClass('nonactive');
                    $('a[href="#stepTab5"]').parent().addClass('active');

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }


    </script>

    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>


    <script type="text/javascript">
        function companyStandardCodesView(standardId, companyId) {

            var request = {"standard_id": standardId, "company_id": companyId};
            $.ajax({
                type: "GET",
                url: '{{route('company.standards.codes.index')}}',
                data: request,
                success: function (response) {
                    $("#auditor-standard-codes" + standardId).html(response);
                    var $table = $('.FormalEducationTableCodes' + standardId + '').dataTable();
                    $('.FormalEducationTableCodes' + standardId + '').trigger('change');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardFoodCodesView(standardId, companyId) {
            var request = {"standard_id": standardId, "company_id": companyId};
            $.ajax({
                type: "GET",
                url: '{{route('company.standards.food.codes.index')}}',
                data: request,
                success: function (response) {
                    $("#auditor-standard-food-codes" + standardId).html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardEnergyCodesView(standardId, companyId) {
            var request = {"standard_id": standardId, "company_id": companyId};
            $.ajax({
                type: "GET",
                url: '{{route('company.standards.energy.codes.index')}}',
                data: request,
                success: function (response) {
                    $("#auditor-standard-energy-codes" + standardId).html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });


        }

        function closeStandardQuestionModal(standardId) {

            $('#standard_question_' + standardId).modal('hide');
            $('#edit_standard' + standardId).css('display', 'none');

        }
    </script>



    <script type="text/javascript">

        @if($company->contacts->count() >0)
        var room = '{{$company->contacts->count() -1}}';
        @else
        var room = 0;

        @endif


        function addContactHTML(room) {

            var html = '';
            html += '<div class="col-md-12">';

            html += '<div class="col-md-1 floting">';
            html += '<label>Primary *</label>';
            html += '<div class="form-group">';
            html += '<input type="radio" class="form-control primary" name="primary" value="0"  onclick="primaryIndex('+room+')" required="">';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-md-1 floting">';
            html += '<label>Title *</label>';
            html += '<div class="form-group">';
            html += '<select class="form-control" name="title[]">';
            html += '<option value="Mr" selected>Mr</option>';
            html += '<option value="Mrs" >Mrs</option>';
            html += '<option value="Ms" >Ms</option>';
            html += '<option value="Dr" >Dr</option>';
            html += '</select>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-md-1 floting">';
            html += '<div class="form-group">';
            html += '<label>Name *</label>';
            html += '<input type="text" name="name[]" class="form-control" placeholder="Enter Name" value="" required>';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-md-2 floting">';
            html += '<div class="form-group">';
            html += '<label>Position *</label>';
            html += '<input type="text" name="position[]" class="form-control" placeholder="Enter Position" value="" required>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-2 floting">';
            html += '<div class="form-group">';
            html += '<label>Cell Number *</label>';
            html += '<input type="tel" name="contact[]" class="form-control" placeholder="Enter Contact Number" value="" required>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-2 floting">';
            html += '<div class="form-group">';
            html += '<label>Landline </label>';
            html += '<input type="tel" name="landline[]" class="form-control" placeholder="Enter Contact Number" value="">';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-md-2 floting">';
            html += '<div class="form-group">';
            html += '<label>Email </label>';
            html += '<input type="email" name="email[]" class="form-control" placeholder="Enter Email" value="">';
            html += '</div>';
            html += '</div>';


            html += '<div class="col-md-1 floting">';
            html += '<div class="form-group add_remove">';
            html += '<br>';
            html += '<button class="btn btn-danger removeBtnCompany" type="button" onclick="remove_main_contact(' + room + ');"  style="margin-top: 4px;"><i class="fa fa-minus"></i></button>';
            html += '</div>';
            html += '</div>';

            html + '</div>'
            html += '<div class="clearfix"></div>';


            return html;
        }


        function add_main_contact() {
            room = parseInt(room);
            room++;
            var objTo = document.getElementById('education_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room);
            var rdiv = 'removeclass' + room;
            var html = addContactHTML(room);

            divtest.innerHTML = html;


            objTo.appendChild(divtest)
        }

        function remove_main_contact(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>
@endpush
