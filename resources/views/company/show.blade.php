@extends('layouts.master')
@section('title', "Company")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Company <span>Details</span></h1>
                    </div>
                    <!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="row comp_det_view">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="card mrgn">
                            <div class="card-header d-flex p-0">
                                <h3 class="card-title p-3">Company Name Edit
                                </h3>
                                <ul class="nav nav-pills ml-auto p-2">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#companyDetails" data-toggle="tab">Company
                                            Details </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#contactPersons" data-toggle="tab">Contact Person
                                            Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#scopeTechnical" data-toggle="tab">Scope Detail</a>
                                    </li>
                                </ul>
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="companyDetails">
                                        <input type="hidden" name="company_id" id="company_id" value="{{$company->id}}">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-4 floting">
                                                    <div class="form-group">
                                                        <label>Company Name</label>
                                                        <p class="results_ans">{{ $company->name }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 floting">
                                                    <div class="form-group">
                                                        <label>Region</label>
                                                        <p class="results_ans">{{ $company->region->title }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 floting">
                                                    <div class="form-group">
                                                        <label>Website</label>
                                                        <p class="results_ans">{{ $company->website }}</p>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-12">


                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="border_div"></div>
                                            <div class="col-md-12">
                                                <div class="text-left">
                                                    <label class="text-uppercase emp_lbl">Total Locations - <span
                                                                class="text-lg"
                                                                style="color: #fc5b00; font-weight: 700;">{{ $company->childCompanies->count() +1 }} </span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>HO / Main Address</label>
                                                        <p class="results_ans">{{ $company->address }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Key Process</label>
                                                        <p class="results_ans">{{ $company->key_process }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>Country</label>
                                                        <p class="results_ans">{{ $company->country->name }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <p class="results_ans">{{ $company->city->name }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <label>No. of Employees</label>
                                                        <p class="results_ans">{{ $company->total_employees ?? '-' }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @foreach($company->childCompanies as $child_company)
                                                <div class="col-md-12">
                                                    <div class="col-md-3 floting">
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <p class="results_ans">{{ $child_company->address }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 floting">
                                                        <div class="form-group">
                                                            <label>Key Process</label>
                                                            <p class="results_ans">{{ $child_company->key_process }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 floting">
                                                        <div class="form-group">
                                                            <label>Country</label>
                                                            <p class="results_ans">{{ $child_company->country ? $child_company->country->name : 'N/A' }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 floting">
                                                        <div class="form-group">
                                                            <label>City</label>
                                                            <p class="results_ans">{{ $child_company->city->name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 floting">
                                                        <div class="form-group">
                                                            <label>No. of Employees</label>
                                                            <p class="results_ans">{{ $child_company->total_employees ?? '-' }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            @endforeach
                                            <div class="border_div"></div>
                                            <div class="col-md-12">
                                                <div class="text-left">
                                                    <label class="text-uppercase emp_lbl">Total Employees - <span
                                                                class="text-lg"
                                                                style="color: #fc5b00; font-weight: 700;">{{ $company->total_employees + $company->childCompanies->sum('total_employees') }} </span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Permanant Employees</label>
                                                        <p class="results_ans">{{ $company->permanent_employees }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Part Time Employees</label>
                                                        <p class="results_ans">{{ $company->part_time_employees }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Similar / repeatative tasks</label>
                                                        <p class="results_ans">{{ $company->repeatative_employees }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 floting">
                                                    <div class="form-group">
                                                        <label>Shifts</label>
                                                        <p class="results_ans">{{ $company->shift ?? '-' }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>sister company</label>
                                                        <p class="results_ans">{{ $company->sister_company_name ?? '-' }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="contactPersons">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="companyScrollTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr style="color: #FFF;">
                                                            <th>Primary</th>
                                                            <th>Title</th>
                                                            <th>Name</th>
                                                            <th>Position</th>
                                                            <th>Cell Number</th>
                                                            <th>Landline Number</th>
                                                            <th>Email</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($company->contacts as $contact)
                                                            <tr>
                                                                <td>{{ $contact->isPrimary() ? 'Yes' : 'No' }}</td>
                                                                <td>{{ $contact->title }}</td>
                                                                <td>{{ $contact->name }}</td>
                                                                <td>{{ $contact->position ?? '-' }}</td>
                                                                <td>{{ $contact->contact }}</td>
                                                                <td>{{ $contact->landline ?? '-' }}</td>
                                                                <td>{{ $contact->email ?? '-' }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="scopeTechnical">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Scope to be Certified</label>
                                                        <p class="results_ans">{{ $company->scope_to_certify ?? '-' }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Any processes are outsourced ?</label>
                                                        <p class="results_ans">{{ $company->outsourced_process_details ?? '-' }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>General Remarks</label>
                                                        <p class="results_ans">{{ $company->general_remarks }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>
                        @if(auth()->user()->user_type !== 'management')
                            <div class="card margn">
                                <div class="card-header d-flex p-0">
                                    <h3 class="card-title p-3">Make IMS</h3>
                                </div>
                                <div class="card-body">

                                    <form method="post" onsubmit="return validate(event)"
                                          action="{{route('store.ims.standard')}}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">

                                            <div class="col-md-8 ml-8">
                                                <label for="standard_one">Standards for IMS</label>
                                                <select name="standard_one[]" id="standard_one"
                                                        class="form-control select2"
                                                        multiple="multiple" required>
                                                    <option value="">Please Select Standard</option>
                                                    @if(!empty($companyStandards) && count($companyStandards) > 0)
                                                        @foreach($companyStandards as $companyStd)
                                                            @if(is_null($companyStd->ims_standard_ids) && (strtolower(str_replace(' ','_',$companyStd->standard->standardFamily->name)) != 'food' && strtolower(str_replace(' ','_',$companyStd->standard->standardFamily->name)) != 'energy_management') )
                                                                <option value="{{$companyStd->id}}">{{ $companyStd->standard->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary" style="margin-top: 25px;">
                                                    SAVE
                                                </button>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header d-flex p-0">
                                @if(auth()->user()->user_type !== 'management')
                                    <h3 class="card-title p-3">Standards
                                        <a href="javascript:void(0)" id="addNewStandard"
                                           class="edit_comp"><i class="fa fa-plus-circle"
                                                                aria-hidden="true"></i>
                                            Add/Edit</a>
                                    </h3>
                                @endif
                                <ul class="nav nav-pills ml-auto p-2 responsiveTabs">
                                    @foreach($company->companyStandards as $companyStandard)
                                        @if($companyStandard->is_ims)
                                            @continue(isset($ims_heading))
                                            @php
                                                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->where('type','child')->get();
                                                $getBaseImsHeading =$company->companyStandards()->where('is_ims', true)->where('type','base')->first();
                                                $ims_heading = $getBaseImsHeading->standard->name;
                                            @endphp
                                            @foreach($imsCompanyStandards as $imsCompanyStandard)
                                                @php
                                                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                                                @endphp
                                            @endforeach
                                            <li class="nav-item">
                                                <a class="nav-link standard-tab {{ $loop->index == 0 ? 'active' : '' }}"
                                                   href="#companyStandardIMS_{{ $getBaseImsHeading->id }}"
                                                   data-toggle="tab">{{ $ims_heading }}</a>
                                            </li>
                                        @elseif($companyStandard->is_ims == false)
                                            <li class="nav-item">
                                                <a class="nav-link standard-tab {{ $loop->index == 0 ? 'active' : '' }}    @if($companyStandard->status == 'pending') disabledDiv @endif"
                                                   href="#companyStandard_{{ $companyStandard->id }}"
                                                   data-toggle="tab">

                                                    @if(!is_null($companyStandard->ims_standard_ids))

                                                        @php
                                                            $companyStandardIds = explode(',', $companyStandard->ims_standard_ids);
                                                            $activeImsStandards = \App\CompanyStandards::whereIn('id',$companyStandardIds)->where('company_id', $companyStandard->company_id)->get();
                                                            $ims_headingTwo = $companyStandard->standard->name;
                                                        @endphp

                                                        @foreach($activeImsStandards as $activeImsStandard)
                                                            @continue($companyStandard->standard->id == $activeImsStandard->standard->id)
                                                            @php
                                                                $ims_headingTwo .= " & " . $activeImsStandard->standard->name;
                                                            @endphp
                                                        @endforeach
                                                        {{ $ims_headingTwo }}
                                                        @if($companyStandard->status == 'disabled')
                                                            (Not-Active)
                                                        @elseif($companyStandard->status == 'pending')
                                                            (Pending)
                                                        @else
                                                        @endif
                                                    @else
                                                        {{ $companyStandard->standard->name }}
                                                        @if($companyStandard->status == 'disabled')
                                                            (Not-Active)
                                                        @elseif($companyStandard->status == 'pending')
                                                            (Pending)
                                                        @else
                                                        @endif
                                                    @endif
                                                </a>

                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div><!-- /.card-header -->

                            <div class="card-body">
                                <div class="tab-content" id="tab-content-section">
                                    @php
                                        $ims_print_done = false;
                                        $company_standard_ids=[];
                                        foreach($company->companyStandards as $key=>$companyStandard){
                                            $company_standard_ids[$key]= $companyStandard->standard_id;
                                        }


                                    @endphp
                                    {{--                                    ->orderBy('type','asc')--}}
                                    @foreach($company->companyStandards()->get() as $index=>$companyStandard)
                                        @if($companyStandard->is_ims)
                                            @continue($ims_print_done)
                                            @php
                                                $ims_print_done = true;
                                            @endphp

                                            @include('company.partials.ims-aj')
                                        @else
                                            @include('company.partials.single-aj')
                                        @endif
                                        <!-- /.tab-pane -->
                                    @endforeach

                                </div>
                                <div style="display:none" id="standard_section">
                                    <form action="#" method="post" autocomplete="off" id="submit-data">
                                        <input type="hidden" name="company_id" value="{{ $company->id }}">
                                        <input type="hidden" name="step" value="4">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <label class="text-uppercase emp_lbl" style="font-weight: 300;">Add
                                                    <span class="text-lg"
                                                          style="font-weight: 700;">Standards</span></label>
                                                <h6 class="text-center dashboard_heading2">Add Each Standard
                                                    Seperately as each may have any specific information</h6>
                                            </div>
                                        </div>
                                        @php
                                            $already_save_standards = $company->companyStandards->pluck('standard_id')->toArray();
                                            $standards 				= [];

                                        @endphp
                                        @foreach($standards_families as $standards_family)
                                            <div class="card-header">
                                                <h3 class="card-title">{{ $standards_family->name }}</h3>
                                            </div>
                                            @forelse($standards_family->standards->chunk(6) as $chunk)
                                                <div class="col-md-12 mrgn text-center">
                                                    @foreach($chunk as $standard)

                                                        @php
                                                            $is_standard_enrolled= in_array($standard->id, $already_save_standards);
                                                            $standards[] = $standard;

                                                            $auditJustification = \App\AuditJustification::where('company_id',$company->id)->whereHas('ajStandards',function($q) use ($standard){
                                                                        $q->where('standard_id',$standard->id)->whereNull('deleted_at')->whereHas('ajStandardStages',function($query){
                                                                        $query->where('audit_type','stage_1')->where('status','approved');
                                                            });
                                                            })->first();

                                                            if(is_null($auditJustification)){
                                                                 $standardDiv= '';
                                                            }else{
                                                                 $standardDiv= 'disabledDivStandard';
                                                            }

                                                        @endphp
                                                        <div class="col-md-2 floting standard_selection"
                                                             data-standard-number="{{ $standard->number }}">
                                                            <input type="checkbox" name="standard_ids[]"

                                                                   value="{{ $standard->id }}"
                                                                   class="standard_check {{ $standardDiv }} @if(auth()->user()->user_type !== 'scheme_manager' && $is_standard_enrolled) disabledDiv @endif"
                                                                   data-modal-id="standard_question_{{ $standard->id }}"
                                                                   data-is-ims-enabled="{{ $standards_family->is_ims_enabled }}"
                                                                   data-standard-id="{{ $standard->id }}" {{ $is_standard_enrolled ? 'checked' : '' }}>
                                                            <label title="{{ $standard->name }}">{{ $standard->name }}
                                                                @foreach($company->companyStandards as $companyStandard)
                                                                    @if($companyStandard->standard_id === $standard->id)
                                                                        @if (auth()->user()->user_type == 'scheme_manager')
                                                                            <i style="">
                                                                                <a href="" class="edit_standard"
                                                                                   id="edit_standard{{$standard->id}}">
                                                                                    <small>Edit</small>
                                                                                </a>
                                                                            </i>
                                                                        @endif
                                                                    @else

                                                                    @endif
                                                                @endforeach
                                                                @if (auth()->user()->user_type == 'scheme_manager')
                                                                    <i style="display: none;"
                                                                       id="edit_standard_button_check{{$standard->id}}">
                                                                        <a href="" class="edit_standard">
                                                                            <small>Edit</small>
                                                                        </a>
                                                                    </i>
                                                                @endif

                                                            </label>
                                                        </div>
                                                    @endforeach

                                                    <div class="clearfix"></div>
                                                </div>
                                            @empty
                                                <div class="col-md-12">
                                                    <p>No Standard Got Added Yet.</p>
                                                </div>
                                            @endforelse
                                        @endforeach

                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="text-center">
                                                    <label class="text-uppercase emp_lbl"
                                                           style="font-weight: 300;"><span
                                                                class="text-lg blue_text" style="font-weight: 700;">Files Upload</span></label>
                                                </div>
                                            </div>

                                            <div class="mrgn"></div>
                                            <div class="col-md-12 text-center">
                                                <div class="col-md-3 uploadDocs floting">
                                                    <label>Initial Inquiry Form</label>
                                                    <a href="#" class="open_standards_modal"
                                                       data-document-type="initial_inquiry_form"><i
                                                                class="fa fa-plus-circle"
                                                                aria-hidden="true"></i></a>
                                                    <div class="uploaded_documents"></div>
                                                </div>
                                                <div class="col-md-3 uploadDocs floting">
                                                    <label>Proposal</label>
                                                    <a href="#" class="open_standards_modal"
                                                       data-document-type="proposal"><i
                                                                class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <div class="uploaded_documents"></div>
                                                </div>
                                                <div class="col-md-3 uploadDocs floting">
                                                    <label>Contract</label>
                                                    <a href="#" class="open_standards_modal"
                                                       data-document-type="contract"><i
                                                                class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <div class="uploaded_documents"></div>
                                                </div>
                                                <div class="col-md-3 uploadDocs floting">
                                                    <label>Other Documents</label>
                                                    <a href="#" class="open_standards_modal" data-document-type="other"><i
                                                                class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <div class="uploaded_documents"></div>
                                                </div>
                                            </div>
                                            <div class="mrgn downloadNav row">
                                                <ul class="nav nav-pills">

                                                    <li class="nav-item initial_inquiry_form">
                                                        <a class="nav-link active show" href="#iif_tab"
                                                           data-toggle="tab" style="color:#000">Initial
                                                            Inquiry Forms</a>
                                                    </li>
                                                    <li class="nav-item proposal">
                                                        <a class="nav-link" href="#ppl_tab" data-toggle="tab"
                                                           style="color:#000">Proposal</a>
                                                    </li>
                                                    <li class="nav-item contract">
                                                        <a class="nav-link" href="#ctt_tab" data-toggle="tab"
                                                           style="color:#000">Contract</a></li>
                                                    <li class="nav-item other">
                                                        <a class="nav-link" href="#ods_tab" data-toggle="tab"
                                                           style="color:#000">Other Documents</a>
                                                    </li>
                                                </ul>


                                                <div class="tab-content">

                                                    <div class="tab-pane active show initial_inquiry_form" id="iif_tab">
                                                        <h2>Initial Inquiry Forms:</h2>
                                                        <div class="row">
                                                            <div class="col-md-12 ">

                                                                <table class="table">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Date</th>
                                                                        <th>Standard</th>
                                                                        <th>File</th>
                                                                        <th>Action</th>

                                                                    </tr>

                                                                    @if(!empty($standard_documents) && count($standard_documents) >0)
                                                                        @foreach ($standard_documents as $inqueryDocuemts)
                                                                            @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                                @foreach ($inqueryDocuemts as $index => $data)

                                                                                    @if($data->document_type === 'initial_inquiry_form')
                                                                                        @php
                                                                                            $initial_inquiry_form_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                                        @endphp
                                                                                        <tr id="#media_docs{{$initial_inquiry_form_media->id}}">

                                                                                            <td>
                                                                                                <input type="hidden"
                                                                                                       name="company_media_id"
                                                                                                       value="{{ $initial_inquiry_form_media->id}}"
                                                                                                       id="company_media_id"/>
                                                                                            </td>
                                                                                            <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                            <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                            <td>
                                                                                                <a href="{{  asset('storage/'.$initial_inquiry_form_media->id.'/'.$initial_inquiry_form_media->file_name) }}"
                                                                                                   target="_blank"
                                                                                                   title="{{ $initial_inquiry_form_media->file_name }}">{{ $initial_inquiry_form_media->file_name }}</a>
                                                                                            </td>
                                                                                            <td>
                                                                                                @can('delete_company_documents')
                                                                                                    <a href="javascript:void(0)"
                                                                                                       onclick="removeMedia({{ $initial_inquiry_form_media->id}})"
                                                                                                       class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                                class="fa fa-remove"></i>
                                                                                                    </a>
                                                                                                @endcan

                                                                                            </td>

                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif

                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane proposal" id="ppl_tab">
                                                        <h2>Proposal:</h2>
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Date</th>
                                                                        <th>Standard</th>
                                                                        <th>File</th>
                                                                        <th>Action</th>

                                                                    </tr>
                                                                    @if(!empty($standard_documents) && count($standard_documents) >0)
                                                                        @foreach ($standard_documents as $inqueryDocuemts)
                                                                            @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                                @foreach ($inqueryDocuemts as $index => $data)
                                                                                    @if($data->document_type === 'proposal')
                                                                                        @php
                                                                                            $proposal_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                                        @endphp
                                                                                        <tr id="#media_docs{{$proposal_media->id}}">

                                                                                            <td>
                                                                                                <input type="hidden"
                                                                                                       name="company_media_id"
                                                                                                       value="{{ $proposal_media->id}}"
                                                                                                       id="company_media_id"/>
                                                                                            </td>
                                                                                            <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                            <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                            <td>
                                                                                                <a href="{{  asset('storage/'.$proposal_media->id.'/'.$proposal_media->file_name) }}"
                                                                                                   target="_blank"
                                                                                                   title="{{ $proposal_media->file_name }}">{{ $proposal_media->file_name }}</a>
                                                                                            </td>
                                                                                            <td>
                                                                                                @can('delete_company_documents')
                                                                                                    <a href="javascript:void(0)"
                                                                                                       onclick="removeMedia({{ $proposal_media->id}})"
                                                                                                       class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                                class="fa fa-remove"></i>
                                                                                                    </a>
                                                                                                @endcan

                                                                                            </td>

                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane contract" id="ctt_tab">
                                                        <h2>Contract:</h2>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Date</th>
                                                                        <th>Standard</th>
                                                                        <th>File</th>
                                                                        <th>Action</th>

                                                                    </tr>
                                                                    @if(!empty($standard_documents) && count($standard_documents) >0)
                                                                        @foreach ($standard_documents as $inqueryDocuemts)
                                                                            @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                                @foreach ($inqueryDocuemts as $index => $data)
                                                                                    @if($data->document_type === 'contract')
                                                                                        @php
                                                                                            $contract_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                                        @endphp
                                                                                        <tr id="#media_docs{{$contract_media->id}}">

                                                                                            <td>
                                                                                                <input type="hidden"
                                                                                                       name="company_media_id"
                                                                                                       value="{{ $contract_media->id}}"
                                                                                                       id="company_media_id"/>
                                                                                            </td>
                                                                                            <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                            <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                            <td>
                                                                                                <a href="{{  asset('storage/'.$contract_media->id.'/'.$contract_media->file_name) }}"
                                                                                                   target="_blank"
                                                                                                   title="{{ $contract_media->file_name }}">{{ $contract_media->file_name }}</a>
                                                                                            </td>
                                                                                            <td>
                                                                                                @can('delete_company_documents')
                                                                                                    <a href="javascript:void(0)"
                                                                                                       onclick="removeMedia({{ $contract_media->id}})"
                                                                                                       class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                                class="fa fa-remove"></i>
                                                                                                    </a>
                                                                                                @endcan

                                                                                            </td>

                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane other" id="ods_tab">
                                                        <h2>Other Documents:</h2>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Date</th>
                                                                        <th>Standard</th>
                                                                        <th>File</th>
                                                                        <th>Action</th>

                                                                    </tr>
                                                                    @if(!empty($standard_documents) && count($standard_documents) >0)
                                                                        @foreach ($standard_documents as $inqueryDocuemts)
                                                                            @if(!empty($inqueryDocuemts) && count($inqueryDocuemts) >0)
                                                                                @foreach ($inqueryDocuemts as $index => $data)
                                                                                    @if($data->document_type === 'other')
                                                                                        @php
                                                                                            $other_media = \Spatie\MediaLibrary\Models\Media::where('id',$data->media_id)->first();
                                                                                        @endphp
                                                                                        <tr id="#media_docs{{$other_media->id}}">

                                                                                            <td>
                                                                                                <input type="hidden"
                                                                                                       name="company_media_id"
                                                                                                       value="{{ $other_media->id}}"
                                                                                                       id="company_media_id"/>
                                                                                            </td>
                                                                                            <td>{{ $data->date->format('d/m/Y') }}</td>
                                                                                            <td>{{ $data->companyStandard->standard->name}}</td>
                                                                                            <td>
                                                                                                <a href="{{  asset('storage/'.$other_media->id.'/'.$other_media->file_name) }}"
                                                                                                   target="_blank"
                                                                                                   title="{{ $other_media->file_name }}">{{ $other_media->file_name }}</a>
                                                                                            </td>
                                                                                            <td>
                                                                                                @can('delete_company_documents')
                                                                                                    <a href="javascript:void(0)"
                                                                                                       onclick="removeMedia({{$other_media->id}})"
                                                                                                       class="btn btn-sm btn-danger fontIconMedia"><i
                                                                                                                class="fa fa-remove"></i>
                                                                                                    </a>
                                                                                                @endcan

                                                                                            </td>

                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="border_div"></div>
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting"></div>
                                            <div class="col-md-6 floting">
                                                <div class="col-md-4 floting"></div>
                                                <div class="col-md-4 floting">
                                                    <div class="form-group">
                                                        <button type="submit"
                                                                class="btn btn-block btn-success btn_save">Save
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 floting"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-3 floting"></div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </form>
                                </div>


                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>
                        <!-- ./card -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')

    <script type="text/javascript">
        $("#addNewStandard").click(function () {
            $("#standard_section").toggle();
            $("#tab-content-section").toggle();
        });
        special_dropdown = [];
        special_dropdown_multiple = [];
    </script>
@endpush

@push('modals')

    @include('company.partials.modal')

@endpush
<link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
@push('styles')
    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

        input[type="file"] {
            display: none;
        }

        .btnColorDanger {
            background: red;
        }

        .disabledDivStandard {
            pointer-events: none;
            opacity: 0.4;
        }
    </style>
@endpush

@push('scripts')

    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    <script type="text/javascript">
        $('#standard_document_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        String.prototype.ucFirst = function () {
            return this.substring(0, 1).toUpperCase() + this.substring(1, this.length);
        };
        // $('#contacts_modal').modal('show');
        standards = {!! collect($standards)->mapWithKeys(function($item){ return ['id_' . $item->id => $item]; })->toJson() !!};
        company_id = {{ $company->id }};
        ims_standards_selected = [];
        ims_standards_checked = [];
        selected_standards = [];
        default_surveillance_frequency_for_ims = "";
        default_amounts_for_ims = {"initial_certification_amount": 0, "surveillance_amount": 0, "re_audit_amount": 0};
        default_accreditation_for_ims = [];
        selected_standards_saved = {};
        document_files = {size: 0};
        standards_to_hide = [];
        var standardId = null;
        var country_code = '';
        var responseApproved = false;
        @foreach($company->companyStandards as $company_standard)
        @if($company_standard->is_ims)
        ims_standards_selected.push({{ $company_standard->standard_id }});
        ims_standards_checked[{{ $company_standard->standard_id }}] = true;
        default_surveillance_frequency_for_ims = '{{ $company_standard->surveillance_frequency }}';
        default_amounts_for_ims = {
            "initial_certification_amount": {{ $company_standard->initial_certification_amount }},
            "surveillance_amount": {{ $company_standard->surveillance_amount }},
            "re_audit_amount": {{ $company_standard->re_audit_amount }}
        };
        default_accreditation_for_ims = [{{ $company_standard->accreditations->pluck('id')->implode(', ') }}];
        @elseif($company_standard->standard->standardFamily->is_ims_enabled)
        ims_standards_selected.push({{ $company_standard->standard_id }});
        @endif
        selected_standards.push({{ $company_standard->standard_id }});
        selected_standards_saved[{{ $company_standard->standard_id }}] = true;
        @endforeach


        $('.standard_questionnaire').submit(function (e) {

            e.preventDefault();
            form = $(this);
            standard_id = $(form).data('standard-id');
            is_ims = $(form).find('input[name="is_ims"]').val();
            var request = new FormData(form[0]); //$(form).serialize();
            if (company_id != 0) {
                request.append('company_id', company_id);

            }
            if ($('#auditor-standard-food-codes' + standard_id).data('standard-family-id') == 23) {
                if ($('#auditor-standard-codes' + standard_id).find('tbody').find('td').length > 1 && $('#auditor-standard-food-codes' + standard_id).find('tbody').find('td').length > 1) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.store') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {

// ajaxResponseHandler(response, form);
                            if (typeof response.data !== 'undefined' && typeof response.data.proposed_complexity !== 'undefined') {
                                $('#propsoedComplexity' + standard_id).css('display', '');
                                $('#propsoedComplexity' + standard_id).css('color', 'red');
                            } else {
                                $('#propsoedComplexity' + standard_id).css('display', 'none');
                            }
                            if (response.status == "success") {


                                $('#edit_standard_button_check' + standard_id).css('display', '');
                                $('#edit_standard' + standard_id).css('display', 'none');
//Editable
                                selected_standards.push(standard_id);
                                selected_standards_saved[standard_id] = true;
                                if (is_ims) {
                                    default_amounts_for_ims.initial_certification_amount = $(form).find('input[name="initial_certification_amount"]').val();
                                    default_amounts_for_ims.surveillance_amount = $(form).find('input[name="surveillance_amount"]').val();
                                    default_amounts_for_ims.re_audit_amount = $(form).find('input[name="re_audit_amount"]').val();
                                    default_accreditation_for_ims = [];

                                    $('input[name="default_ims_accreditation_ids[]"]').prop('checked', false);
                                    $.each($(form).find("input[name='accreditation_ids[]']:checked"), function (i, node) {
                                        default_accreditation_for_ims.push($(node).val());

                                        $('input[name="default_ims_accreditation_ids[]"][value="' + $(node).val() + '"]').prop('checked', true);
                                    });
                                    $('select[name="default_ims_surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
                                }
                                $(form).find('input[name="company_standard_id"]').val(response.data.company_standard.id);
                                $(form).closest('.modal').modal('hide');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("IAF and Food Codes is required.");
                }

            } else if ($('#auditor-standard-energy-codes' + standard_id).data('standard-family-id') == 20) {
                if ($('#auditor-standard-codes' + standard_id).find('tbody').find('td').length > 1 && $('#auditor-standard-energy-codes' + standard_id).find('tbody').find('td').length > 1) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.store') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {

                            ajaxResponseHandler(response, form);
// if (typeof response.data !== 'undefined' && typeof response.data.proposed_complexity !== 'undefined') {
//     $('#propsoedComplexity' + standard_id).css('display', '');
//     $('#propsoedComplexity' + standard_id).css('color', 'red');
// } else {
//     $('#propsoedComplexity' + standard_id).css('display', 'none');
// }
                            if (response.status == "success") {

                                $('#edit_standard_button_check' + standard_id).css('display', '');
                                $('#edit_standard' + standard_id).css('display', 'none');
//Editable
                                selected_standards.push(standard_id);
                                selected_standards_saved[standard_id] = true;
                                if (is_ims) {
                                    default_amounts_for_ims.initial_certification_amount = $(form).find('input[name="initial_certification_amount"]').val();
                                    default_amounts_for_ims.surveillance_amount = $(form).find('input[name="surveillance_amount"]').val();
                                    default_amounts_for_ims.re_audit_amount = $(form).find('input[name="re_audit_amount"]').val();
                                    default_accreditation_for_ims = [];

                                    $('input[name="default_ims_accreditation_ids[]"]').prop('checked', false);
                                    $.each($(form).find("input[name='accreditation_ids[]']:checked"), function (i, node) {
                                        default_accreditation_for_ims.push($(node).val());

                                        $('input[name="default_ims_accreditation_ids[]"][value="' + $(node).val() + '"]').prop('checked', true);
                                    });
                                    $('select[name="default_ims_surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
                                }
                                $(form).find('input[name="company_standard_id"]').val(response.data.company_standard.id);
                                $(form).closest('.modal').modal('hide');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("IAF and Energy Codes is required.");
                }

            } else {
                if ($('#auditor-standard-codes' + standard_id).find('tbody').find('td').length > 1) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.store') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {


// ajaxResponseHandler(response, form);
                            if (typeof response.data !== 'undefined' && typeof response.data.proposed_complexity !== 'undefined') {
                                $('#propsoedComplexity' + standard_id).css('display', '');
                                $('#propsoedComplexity' + standard_id).css('color', 'red');
                            } else {
                                $('#propsoedComplexity' + standard_id).css('display', 'none');
                            }
                            if (response.status == "success") {

                                $('#edit_standard_button_check' + standard_id).css('display', '');
                                $('#edit_standard' + standard_id).css('display', 'none');
//Editable
                                selected_standards.push(standard_id);
                                selected_standards_saved[standard_id] = true;

                                $(form).find('input[name="company_standard_id"]').val(response.data.company_standard.id);
                                $(form).closest('.modal').modal('hide');
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("IAF Codes is required.");
                }

            }
        });
        $('select[name="surveillance_frequency"]').change(function () {
            var surveillance_frequency = $(this).val();
            var standard_id = $(this).closest('form').data('standard-id');
            if ($('#step4').find('input[value="' + standard_id + '"]').data('is-ims-enabled')) {
                default_surveillance_frequency_for_ims = surveillance_frequency;
            }
        });


        function removeMedia(id) {

            var request = "id=" + id;


            $.ajax({
                url: "{{ route('company.media.remove') }}",
                method: 'POST',
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {

                    console.log($(this));
                    toastr['success'](response.message + '.It will refresh on final Save');


                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function standardCheckAction(node, edit = true) {

            var standard_id = $(node).data('standard-id');
            $('#propsoedComplexity' + standard_id).css('display', 'none');
            standardId = standard_id;

            $("#old_cb_certificate_issue_date" + standardId).datepicker().on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                $('#old_cb_certificate_expiry_date' + standardId).datepicker('setStartDate', minDate);
            });

            $("#old_cb_certificate_expiry_date" + standardId).datepicker();
            var company_id = $('#company_id').val();
            if ($(node).is(':checked')) {
                companyStandardCodesView(standard_id, company_id);
                companyStandardFoodCodesView(standard_id, company_id);
                companyStandardEnergyCodesView(standard_id, company_id);
            }


            var is_ims_enabled = $(node).data('is-ims-enabled');
            if ($(node).is(':checked')) {

                $(node).next().find('i').show();
                previousFrequncy('#previousSurveillanceFrequency' + standard_id, standard_id);
                var modal_id = $(node).data('modal-id');
                $('#' + modal_id).modal({backdrop: 'static', keyboard: false}, 'show');
                selected_standards_saved[standard_id] = false;

                console.log(ims_standards_selected);
            } else {


                if (confirm("Do You Really Wants to Remove This?")) {
                    $(node).next().find('i').hide();
                    /* Ajax Call for Removing Company Standard Here */


                    var request = new FormData($('form#step4')[0]);
                    if (company_id != 0) {
                        request.append('company_id', company_id);
                        request.append('standard_id', standard_id);
                        request.append('standard_action', "remove");
                        request.append('step', "4");
                    }
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.store') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (response) {

                            ajaxResponseHandler(response);
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });


                    delete selected_standards[selected_standards.indexOf(standard_id)];
                    delete selected_standards_saved[standard_id];

                    var index = ims_standards_selected.indexOf(standard_id);
                    if (index > -1) {
                        $('#unchecked_ims_standards_list').find('input[value=' + standard_id + ']').remove();
                        $('#unchecked_ims_standards_list').append('<input type="hidden" name="unchecked_ims_standard_ids[]" value="' + standard_id + '">');
                        ims_standards_selected.splice(index, 1);
                        $('#ims_standards_list').html("");

                        if (ims_standards_selected.length > 1) {
                            $(ims_standards_selected).each(function (i, id) {
                                $('#ims_standards_list').append('<div class="col-md-2 floting"><input type="checkbox" name="ims_standard_ids[]" value="' + id + '" class="ims_standard_check" checked="">&nbsp;<label>' + standards['id_' + id].name + '</label></div>');
                            });
                        } else {
                            $('#ims_standards_list').html('<p>No IMS Standard Got Selected Yet.</p>');
                        }
                    }
                } else {
                    $(node).prop('checked', true);
                }


            }
        }

        function standardCheckYes(id) {
            var standard_id = id;
            var company_check_id = $('#company_id').val();


            var request = new FormData();
            if (company_check_id != 0) {
                request.append('company_id', company_check_id);
                request.append('standard_id', standard_id);
            }
            $.ajax({
                type: "POST",
                url: "{{ route('company.checkStandardApproved') }}",
                data: request,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (response) {


                    if (response.status === 'success') {
                        return true;
                    } else {
                        return false;
                    }


                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $('.standard_check').click(function () {
            var standard_id = $(this).data('standard-id');
            var company_check_id = $('#company_id').val();


// var responseReturn = standardCheckYes(standard_id);
//
// if (responseReturn === false) {
//     $(node).prop('checked', true);
//     toastr['warning']("Standard Not deleted.AJ of Stage 1 is Approved");
//
// } else {


            var request = new FormData();
            if (company_check_id != 0) {
                request.append('company_id', company_check_id);
                request.append('standard_id', standard_id);
            }

            var newThis = this;
            $.ajax({
                type: "POST",
                url: "{{ route('company.checkStandard') }}",
                data: request,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (response) {

                    if ($(newThis).is(':checked')) {
                        if (response.status == 'success') {
                            standardCheckAction(newThis, false);
                        } else {
                            toastr[response.status](response.message);
                            if ($(newThis).is(':checked')) {
                                $(newThis).prop('checked', false);
                                $(newThis).trigger('change');
                            }
                        }
                    } else {
                        standardCheckAction(newThis, false);
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
// }


        });
        $(document).on('click', '.edit_standard', function (e) {
            e.preventDefault();
            standardCheckAction($(this).closest('div').find('input'), true);
        });
        $('#ims_standards_list').on('.ims_standard_check', 'click', function () {
            var standard_id = $(this).val();
            if (!$(this).is(':checked')) {
                $('#unchecked_ims_standards_list').find('input[value=' + standard_id + ']').remove();
                $('#unchecked_ims_standards_list').append('<input type="hidden" name="unchecked_ims_standard_ids[]" value="' + standard_id + '">');
            }
        });
        $('.transfered_customer').click(function () {
            if ($(this).is(':checked')) {
                $(this).closest('form').find('.transfered_fields').find('input,select').prop('disabled', false);
                $(this).closest('form').find('.transfered_fields').show();
                $('input[name="initial_certification_amount"]').prop('disabled', true);
            } else {
                $(this).closest('form').find('.transfered_fields').find('input,select').prop('disabled', true);
                $(this).closest('form').find('.transfered_fields').hide();
                $('input[name="initial_certification_amount"]').prop('disabled', false);
            }
        });
        $('.is_transfer_standard').click(function () {
            if ($(this).is(':checked')) {
                $(this).val(1);
                $(this).closest('form').find('.is_transfer_standard_fields').find('input,select').prop('disabled', false);
                $(this).closest('form').find('.is_transfer_standard_fields').show();
            } else {
                $(this).val(0);
                $(this).closest('form').find('.is_transfer_standard_fields').find('input,select').prop('disabled', true);
                $(this).closest('form').find('.is_transfer_standard_fields').hide();
            }
        });
        $('.transfer_year').datepicker({
            format: "yyyy",
            orientation: "bottom",
            keyboardNavigation: false,
            viewMode: "years",
            minViewMode: "years"
        });
        $('.special_dropdown').change(function () {
            var question_id = $(this).data('id');
            var value = $(this).val();
            var question_title = $('#' + question_id).data('question-title');
            var html = '<option>Select ' + question_title + '</option>';
            $.each(special_dropdown[question_id], function (k, obj) {
                if (k == value) {
                    $.each(obj, function (i, v) {
                        if (v != '')
                            html += '<option value="' + v + '">' + v + '</option>';
                    })
                }
            });
            $('#' + question_id).html(html);
        });
        $('.special_dropdown_multiple').change(function () {
            var question_id = $(this).data('id');
            var value = $(this).val();
            var question_title = $('#' + question_id).data('question-title');
            var html = '';//<option>Select '+ question_title +'</option>';
            $(this).find('option:selected').each(function (j, obj1) {
                var value = $(obj1).val();
                $.each(special_dropdown_multiple[question_id], function (k, obj) {
                    if (k == value) {
                        $.each(obj, function (i, v) {
                            if (v != '')
                                html += '<option value="' + v + '">' + v + '</option>';
                        });
                    }
                });
            });
            setTimeout(function () {
                $('#' + question_id).html(html);
            });
        });
        $('.datefield').datepicker();
        $(document).ready(function () {
            $('.has_dependent_fields').on('keyup keydown change', function () {
                var node = $(this);
                var answer = $(node).val();
                var right_answer = $(node).data('right-answer');
                var dependent_fields = $(node).data('dependent-fields').toString();
                var tmp = dependent_fields.split(',');
                if (right_answer == answer) {
                    $(tmp).each(function (i, k) {
                        $('#question' + k).prop('disabled', false);
                    });
                } else {
                    $(tmp).each(function (i, k) {
                        $('#question' + k).prop('disabled', true);
                    });
                }
            });

            $('.standard_questions select[name="old_cb_audit_activity_stage_id"]').change(function () {
                console.log('changed');
                var node = $(this).closest('.standard_questions');
                var selected_node = $(this);
                $(node).find('.previous_activities_dates .activity_type').html('<label>Audit Type</label>');
                $(node).find('.previous_activities_dates .activity_date').html('<label>Previous Audit Date</label>');
                var end_now = false;
                $($(this).find('option')).each(function (i, opt) {
                    if (end_now)
                        return false;
                    if ($(opt).val() == '')
                        return;
                    if ($(opt).val() == $(selected_node).val())
                        end_now = true;
                    var stroption = $(opt).text();
                    stroption = stroption.replace(/\s+/g, '');
                    $(node).find('.previous_activities_dates .activity_type').append('<p>' + $(opt).text() + '</p>');
                    $(node).find('.previous_activities_dates .activity_date').append('<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div><input type="text" class="form-control datefield float-right active" id="old_cb_audit_activity_date' + stroption + '' + standardId + '" name="old_cb_audit_activity_date[' + $(opt).val() + ']" required="" onkeypress="return false;"></div>');
                });


                $("#old_cb_certificate_issue_date" + standardId).datepicker().on('changeDate', function (selected) {

                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateStage2' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateStage2" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance1' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance1" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance2' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance2" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance3').datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance3" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance4' + standardId).datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance4" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateSurveillance5').datepicker('setStartDate', minDate);
                });

                $("#old_cb_audit_activity_dateSurveillance5" + standardId).datepicker().on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf() + (1000 * 60 * 60 * 24 * 1));
                    $('#old_cb_audit_activity_dateReaudit' + standardId).datepicker('setStartDate', minDate);
                });

                $('#old_cb_audit_activity_dateReaudit' + standardId).datepicker();
            });
            /**
             * Only for This View
             */
            $('.has_dependent_fields').trigger('change');
            $('.has_dependent_fields option:selected').trigger('change');
            $('.has_dependent_fields:checked').trigger('change');
            $('#ims_standards_list').html("");
            if (ims_standards_selected.length > 1) {
                $(ims_standards_selected).each(function (i, id) {
                    $('#ims_standards_list').append('<div class="col-md-2 floting"><input type="checkbox" name="ims_standard_ids[]" value="' + id + '" class="ims_standard_check" ' + (typeof ims_standards_checked[id] == 'undefined' ? '' : 'checked') + '>&nbsp;<label>' + standards['id_' + id].name + '</label></div>');
                });
            }
            $('select[name="default_ims_surveillance_frequency"] option[value="' + default_surveillance_frequency_for_ims + '"]').prop('selected', true);
            $(default_accreditation_for_ims).each(function (i, id) {
                $('input[name="default_ims_accreditation_ids[]"][value="' + id + '"]').prop('checked', true);
            });
            if (ims_standards_selected.length > 0) {
                $('#ims_default_values').show();
            }
            /*End*/
        });
        $('#submit-data').submit(function (e) {
            e.preventDefault();
            form = $(this);
            var request = $(form).serialize();
            if (company_id != 0) {
                request += "&company_id=" + company_id;
            }
            process_final_save = true;
            $.each(selected_standards_saved, function (id, status) {
                if (!status)
                    process_final_save = false;
            });
            setTimeout(function () {
                if (process_final_save) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('company.store') }}",
                        data: request,
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            ajaxResponseHandler(response, form);
                            if (response.status == "success") {
                                window.location.reload()
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr["error"]("Please Save All Standards.");
                }
            }, 500);
        });
        var standard_document_node = null;
        $('.open_standards_modal').click(function () {
            standard_document_node = $(this);
            var document_type = $(this).data('document-type');
            console.log(document_type);
            var html = '';
            $.each(selected_standards, function (i, val) {
                var std = standards['id_' + val];
                html += '<label><input type="radio" name="standards[]" class="standard_has_documents" value="' + std.id + '">&nbsp;' + std.name + '</label>';
            });
            setTimeout(function () {
                html += '<input type="hidden" name="document_type" value="' + document_type + '">';
                if (selected_standards.length > 0) {
                    $('#standards_for_documents').find('.selected_standards').html(html);
                }
                $('#standards_for_documents').modal('show');
            });
        });
        $('.selected_standards').on('click', '.select_all_standards', function () {
            if ($(this).is(':checked')) {
                $(this).closest('div').find('input').prop('checked', true);
            } else {
                $(this).closest('div').find('input').prop('checked', false);
            }
        });
        $('#save_standard_documents').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            if (form.find('input.standard_has_documents:checked').length > 0) {
                var is_all_checked = form.find('input.select_all_standards').is(':checked');
                if (is_all_checked)
                    form.find('input.select_all_standards').prop('checked', false);
                var request = new FormData(form[0]);
                if (is_all_checked)
                    form.find('input.select_all_standards').prop('checked', true);
                if (company_id != 0) {
                    request.append('company_id', company_id);
                }
                request.append('step', 5);
                request.append('step_action', 'upload_documents');

                $.ajax({
                    url: "{{ route('company.store') }}",
                    method: 'POST',
                    data: request,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        ajaxResponseHandler(response, form);
                        if (response.status == "success") {
                            var uploaded_documents_standards = '';
                            $(form.find('input[name="standards[]"]:checked')).each(function (i, elm) {
                                uploaded_documents_standards += $(elm).parent().text() + ', ';
                            });

                            var document_type = form.find('input[name="document_type"]').val();
                            $('#addNewStandard').find('div.' + document_type).append(response.data.uploaded_doc_card);
                            $('#addNewStandard').find('li.' + document_type + ' a').trigger('click');
                            form.find('input[name="standards[]"]').prop('checked', false);
                            form.find('input[name="date"]').val('');
                            $('#standards_for_documents').modal('hide');

                            $('#upld100').empty();
                            $('#save_standard_documents').trigger('reset');


                            toastr['success']("The file has been uploaded.It will refresh on final Save");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please select atleast one standard.");
            }
        });
        $('#addNewStandard').on('click', '.remove_uploaded_documents', function (e) {
            var node = $(this);
            var documents_to_remove = $(node).data('standard-document-ids');
            var request = new FormData();
            if (company_id != 0) {
                request.append('company_id', company_id);
            }
            request.append('step', 5);
            request.append('step_action', 'remove_document');
            request.append('document_ids', documents_to_remove);
            $.ajax({
                url: "{{ route('company.store') }}",
                method: 'POST',
                data: request,
                contentType: false,
                processData: false,
                success: function (response) {
                    toastr['success'](response.message);
                    $(node).closest('div.uploaded_documents_card').remove();
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
            /* Back end remove Ajax call here */
        });

        $('.nav-link').click(function () {
            $('.edit_comp').removeClass('active');
            $('#addNewStandard').removeClass('active');
        });

        function checkFrequency(frequency) {

            if (frequency.value === 'annually') {
                $(".annually").css("display", "block");
                $(".biannually").css("display", "none");
                $(".previous_annually").css("display", "block");
                $(".previous_biannually").css("display", "none");
            } else if (frequency.value === 'biannually') {
                $(".annually").css("display", "none");
                $(".biannually").css("display", "block");
                $(".previous_annually").css("display", "none");
                $(".previous_biannually").css("display", "block");
            } else {
                $(".annually").css("display", "none");
                $(".biannually").css("display", "none");
                $(".previous_annually").css("display", "none");
                $(".previous_biannually").css("display", "none");
            }
        }
    </script>

    <script>


        function previousFrequncy(previousFrequncy, standardId) {

            var standard_id = standardId;
            var previousfrequncy = $(previousFrequncy).val();
            if (previousfrequncy == 'annual') {
                $('#last-audit-activity-2' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-3' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-4' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-5' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-6' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-7' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-8' + standard_id + '').css('display', 'block');
            } else if (previousfrequncy == "biannual") {
                $('#last-audit-activity-2' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-3' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-4' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-5' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-6' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-7' + standard_id + '').css('display', 'block');
                $('#last-audit-activity-8' + standard_id + '').css('display', 'block');

            } else {
                $('#last-audit-activity-2' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-3' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-4' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-5' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-6' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-7' + standard_id + '').css('display', 'none');
                $('#last-audit-activity-8' + standard_id + '').css('display', 'none');
            }
        }
    </script>


    {{--    Audit Activity --}}
    <script type="text/javascript">

        function readURLMany(input, id) {

            var filename = input.files[0].name;
            var html = '';
            if (input.files && input.files[0]) {
                html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
            }
            $("#upld" + id).html(html);
        }

        function readURLManyTranseferCase(input, id) {

            var filename = input.files[0].name;
            var html = '';
            if (input.files && input.files[0]) {
                html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
            }
            $('#upld' + id).html(html);
        }

        function readURLMultiple(input, id) {

            var html = '';
            $.each(input.files, function (i, node) {
                html += '<a href="#" class="linkClr " style="margin-right: 200px;">' + i + '-' + node.name + '</a>'
                html += '<a href="#" class="trash_icon"><i class="fa fa-trash"></i></a>'
                html += '<br>';

            });
            $('.filename').append(html);
        }

        function auditActivity(companyId, auditType, standardName, standardStageId) {

            $('.audit_activity_names').text('{{ $company->name }}' + ' - ' + auditType + ' - ' + standardName + ' Standard(s)');
            if (auditType == 'Stage 1') {
                $('.check_stage').css('display', 'block');
            } else {
                $('.check_stage').css('display', 'block');
            }
            $('#auditActivity').modal('show');

        }

        function question_one(input, value) {
            if (value == 'no') {
                $('.question_two').css('display', 'block');
                $('.upload_evidences').css('display', 'block');
            } else {
                $('.question_two').css('display', 'none');
                $('.upload_evidences').css('display', 'none');
            }
        }


        function auditTaskSubmit(type, e) {


            e.preventDefault();
            var form = $('#main-audit-form')[0];
            var formData = new FormData(form);
            var status = '';
            if (type === 'save') {
                status = "created";
            } else if (type === 'send') {
                status = "applied";
            } else if (type === 'approved') {
                status = "approved";
            } else if (type === 'reject') {
                status = "rejected";
            }

            formData.append('status', status);

            $.ajax({
                type: "POST",
                url: "{{ route('justification.update') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    if (type === 'save') {
                        if (response.flash_status == 'warning') {
                            toastr[response.flash_status](response.flash_message);
                        } else {
                            ajaxResponseHandler(response, form);
                        }

                    } else if (type === 'send' || type === 'reject' || type === 'approved') {
                        if (response.data.status == 'success') {
                            console.log(response);
                            var html = '';
                            $(response.data.aj_id).each(function (i, d) {
                                html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                            });
                            $("#aj-ids").html(html);
                            $('#sendPopup').modal('toggle');
                        } else {
                            ajaxResponseHandler(response, form);
                        }
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });


        }
    </script>
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript">


        function companyStandardCodesView(standardId, companyId) {

            var request = {"standard_id": standardId, "company_id": companyId};
            $.ajax({
                type: "GET",
                url: '{{route('company.standards.codes.index')}}',
                data: request,
                success: function (response) {
                    $("#auditor-standard-codes" + standardId).html(response);
                    var $table = $('.FormalEducationTableCodes' + standardId + '').dataTable();
                    $('.FormalEducationTableCodes' + standardId + '').trigger('change');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardFoodCodesView(standardId, companyId) {
            var request = {"standard_id": standardId, "company_id": companyId};
            $.ajax({
                type: "GET",
                url: '{{route('company.standards.food.codes.index')}}',
                data: request,
                success: function (response) {
                    $("#auditor-standard-food-codes" + standardId).html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardEnergyCodesView(standardId, companyId) {
            var request = {"standard_id": standardId, "company_id": companyId};
            $.ajax({
                type: "GET",
                url: '{{route('company.standards.energy.codes.index')}}',
                data: request,
                success: function (response) {
                    $("#auditor-standard-energy-codes" + standardId).html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });


        }

        function closeStandardQuestionModal(standardId) {

            $('#standard_question_' + standardId).modal('hide');
            $('#edit_standard' + standardId).css('display', 'none');

        }


        function certificate(companyStandardId) {
            $('.suspension_error_required').hide();
            var request = {"company_standard_id": companyStandardId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.certificate.standard.index')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#certificate_html").html(response.data.html);
                        $('#certificate_date').datepicker({
                            format: 'dd-mm-yyyy',
                        });
                        $('#certificatesStandard').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function certificateSubmit(e) {

            e.preventDefault();

            var response = true;
            if ($('#certificate_status option:selected').val() === 'suspended') {
                if ($(".certificate_radio_check_value").is(":checked") === true) {
                    response = true;
                    $('.suspension_error_required').hide();
                } else {
                    response = false;
                }

            }

            if (response === true) {
                var form = $('#certificate-store')[0];
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.certificate.standard.store') }}",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: formData,
                    success: function (response) {
                        if (response.status == "success") {
                            $('#certificatesStandard').modal('hide');
                            toastr['success']("Certificate Status Updated Successfully.");
                            $('#new_status').text(response.data.certificate.certificate_status);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);

                        } else if (response.status == "no_suspension_found") {
                            toastr['error']("Certificate must be suspended first.");
                        } else if (response.status == "withdrawn_error") {
                            toastr['error']("Certificate must be suspended first.");
                        } else if (response.status == "withdrawn_date_error") {
                            toastr['error']("Withdrawn Date should be greater than suspension date.");
                        } else {
                            toastr['error']("Form has Some Errors.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $('.suspension_error_required').show();

            }


        }

        function updateOriginalIssueDate(schemeManagerPostAuditId) {
            var request = {"scheme_manager_post_audit_id": schemeManagerPostAuditId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.scheme_manager_post_audit.index')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#original_issue_date_html").html(response.data.html);
                        $('#original_issue_date').datepicker({
                            format: 'dd-mm-yyyy',
                        });
                        $('#originalIssueDateModal').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function updateOriginalIssueDateSubmit(e) {

            e.preventDefault();
            var form = $('#original-issue-date-store')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.scheme_manager_post_audit.store') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    if (response.status == "success") {
                        $('#originalIssueDateModal').modal('hide');
                        toastr['success']("Original Issue Date Updated Successfully.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);

                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardRemarks(companyStandardId) {
            var request = {"company_standard_id": companyStandardId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.company_standard_remarks')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#company_standard_remarks_html").html(response.data.html);
                        $('#companyStandardRemarksModal').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function companyStandardRemarksSubmit(e, company_standard_id) {

            e.preventDefault();

            var form = $('#company-standard-remarks-store')[0];
            var formData = new FormData(form);

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.company_standard_remarks.store') }}",
                enctype: 'multipart/form-data',
// processData: false,
// contentType: false,
// cache: false,
                data: {
                    planning_remarks: $('#planning_remark' + company_standard_id).val(),
                    scheme_manager_remarks: $('#scheme_manager_remark' + company_standard_id).val(),
                    company_standard_id: company_standard_id
                },
                success: function (response) {
                    if (response.status == "success") {
                        $('#companyStandardRemarksModal').modal('hide');
                        toastr['success']("Remarks has been added Successfully.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);

                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }


        $(document).on('click', '.deleteCertificateSubmit', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.certificate.standard.delete') }}",
                data: {id: id},
                success: function (response) {
                    if (response.status == "success") {
                        $('#certificatesStandard').modal('hide');
                        toastr['success']("Certificate Delete Successfully.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });


        function directoryUpdate(companyStandardId, radioValue) {
            var directory_updated = radioValue;
            var company_standard_id = companyStandardId;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.directory.company.standard.store') }}",
                data: {directory_updated: directory_updated, company_standard_id: company_standard_id},
                success: function (response) {
                    if (response.status == "success") {
                        toastr['success']("Direcorty Updated Successfully.");
                        if (radioValue == 'yes') {
                            $('#directory_updated_yes' + companyStandardId).prop('checked', true);
                            $('#directory_updated_no' + companyStandardId).prop('checked', false);
                        } else {
                            $('#directory_updated_yes' + companyStandardId).prop('checked', false);
                            $('#directory_updated_no' + companyStandardId).prop('checked', true);
                        }
                    } else {
                        toastr['error']("Something Went Wrong.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $("#standard_one").select2({
            maximumSelectionLength: 3,
            placeholder: "Select Standard"//placeholder
        });

        /*showing confirm for stage1 popup box*/
        function showConfirmForStage(href) {

            if (confirm("Are you sure you want to apply Stage I AJ only. Please note that Stage II cannot be applied till the time stage I AJ is approved?")) {
                window.location.href = href;
            } else {

            }
        }

        /*showing confirm for stage1 popup box*/
        function showConfirmForDelete(href) {

            if (confirm("Are you sure you want to delete.")) {
                window.location.href = href;
            } else {

            }
        }


        /*showing confirm for stage1 popup box*/
        function validate(event) {

            if (confirm("Are you sure you want to combine Standard(s) .Please note this activity cann't be reversed back.")) {
                return true;
            } else {
                return false;
            }
        }

        /* AJ Approved Status for report */
        $(document).on('change', '#ajApprovedStatus', function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            var aj_approved_status = 0;
            var id = $(this).data('id');
            if ($(this).is(':checked') == true) {
                aj_approved_status = 1;
            }
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.aj-standard-stage.approved.status') }}",
                data: {id: id, aj_approved_status: aj_approved_status},
                success: function (response) {
                    if (response.status == "success") {
                        toastr['success']("Status Updated Successfully.");
                    } else {
                        toastr['error']("Something Went Wrong.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $(document).on('change', '#certificate_status', function () {

            if ($(this).find('option:selected').val() == 'suspended') {
                $('#forSuspension').show();
                $('#forSuspensionReason').show();

            } else {
                $('#forSuspension').hide();
                $('#forSuspensionReason').hide();
            }
        })

        $(document).on('change', '#is_print_letter', function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
        })

        $(document).on('change', '.gradeChange', function () {
            var companyStandardId = $(this).data('company-standard-id');
            var selectedGrade = $('#grade' + companyStandardId).find('option:selected').val();
            if (selectedGrade === '') {
                toastr['error']("Please select grade.");
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.company-standard.add-grade') }}",
                    data: {companyStandardId: companyStandardId, grade: selectedGrade},
                    success: function (response) {
                        if (response.status === "success") {
                            toastr['success']("Grade Updated Successfully.");
                        } else {
                            toastr['error']("Something Went Wrong.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        })

        $(document).on('click', '#edit_standard_prices', function (e) {
            e.preventDefault();
            var initial_certification_currency = $(this).data('initial-certification-currency');
            var initial_certification_amount = $(this).data('initial-certification-amount');
            var surveillance_amount = $(this).data('surveillance-amount');
            var re_audit_amount = $(this).data('re_audit-amount');
            var client_type = $(this).data('client-type');
            var company_standard_id = $(this).data('company-standard-id');
            $('#price_currency_unit option[value="' + initial_certification_currency + '"]').attr('selected', true);
            let price_initial_certification_amount = $('#price_initial_certification_amount');
            price_initial_certification_amount.val(initial_certification_amount);
            if (client_type === 'transfered') {
                price_initial_certification_amount.prop('readonly', true);
            } else {
                price_initial_certification_amount.prop('readonly', false);
            }
            $('#price_surveillance_amount').val(surveillance_amount);
            $('#price_re_audit_amount').val(re_audit_amount);
            $('#price_company_standard_id').val(company_standard_id);
            $('#price_client_type').val(client_type);
            $('.priceModal').modal({backdrop: 'static', keyboard: false}, 'show');
        })
        $(document).on('click', '.close_price_modal', function () {
            $('.priceModal').modal('hide');
        })

        $(document).on('click', '.save_standard_prices', function () {
            var companyStandardId = $('#price_company_standard_id').val();
            var currency_unit = $('#price_currency_unit').find('option:selected').val();
            var initial_certification_amount = $('#price_initial_certification_amount').val();
            var surveillance_amount = $('#price_surveillance_amount').val();
            var re_audit_amount = $('#price_re_audit_amount').val();
            var client_type = $('#price_client_type').val();
            if (currency_unit === '' || initial_certification_amount === '' || surveillance_amount === '' || re_audit_amount === '') {
                toastr['error']("All fields are required");
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.company-standard.update.contract-price') }}",
                    data: {
                        companyStandardId: companyStandardId,
                        currency_unit: currency_unit,
                        initial_certification_amount: initial_certification_amount,
                        surveillance_amount: surveillance_amount,
                        re_audit_amount: re_audit_amount,
                        client_type: client_type,
                    },
                    success: function (response) {
                        if (response.status === "success") {
                            toastr['success']("CONTRACT PRICE Updated Successfully.");
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000)
                        } else {
                            toastr['error']("Something Went Wrong.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        })


    </script>

@endpush
