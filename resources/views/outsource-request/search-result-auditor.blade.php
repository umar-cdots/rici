
<div class="col-md-12">
    <label>Available Auditors</label>
    <table class="table table-bordered availableAuditorTable">
        <tbody>
        <tr>
            <th>Sugggest</th>
            <th>List/Name of Available Auditors</th>
            <th>Based In Region / Country</th>
        </tr>
        @foreach($auditorStandards as $key=> $auditorStandard)
            @if(!is_null($auditorStandard->auditor) && ($auditorStandard->auditor->role == 'auditor')  && $auditorStandard->auditor->region_id != auth()->user()->region_id)
                <tr>
                    <td><input type="radio" class="auditor-list" name="auditor_id"
                               value="{{ $auditorStandard->auditor->id }}" required></td>
                    <td>{{$auditorStandard->auditor->fullName()}}</td>
                    <td>{{$auditorStandard->auditor->region->title}} / {{$auditorStandard->auditor->country->name}}</td>
                </tr>
            @endif
        @endforeach

        </tbody>
    </table>
</div>



