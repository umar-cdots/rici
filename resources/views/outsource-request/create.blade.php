@extends('layouts.master')
@section('title', "Outsource Requests")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">Outsource <span>Request</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">&nbsp;</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body card_cutom">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Audit Type *</label>
                                <div class="form-group">

                                    <div class="col-md-4 floting no_padding">
                                        <input type="radio" name="audit" value="single" class="flat-red audit_radio" id="refresh-single" onclick="refresh('single')"
                                               checked>
                                        <label class="job_status">Single Audit</label>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <input type="radio" name="audit" value="ims" class="flat-red audit_radio"   id="refresh-ims" onclick="refresh('ims')">
                                        <label class="job_status">IMS</label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="singleAudit CustomTabs auditType" id="single">
                            <form action="" method="post" id="single-outsource-request">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Standards</label>
                                            <select name="standard_id" class="form-control" id="standard_type"
                                                    onchange="updateAccreditationsOnOutsourceRequest('standard_type','accreditation-outsource-request', 'iaf-outsource-request', 'ias-outsource-request')">
                                                <option value="">Select Standard</option>
                                                @if(!empty($standards))
                                                    @foreach($standards as $standard)
                                                        <option data-id="{{ strtolower(str_slug($standard->standardFamily->name)) }}"
                                                                value="{{ $standard->id }}">{{ $standard->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Required Date</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                  </span>
                                                </div>
                                                <input type="text" class="form-control float-right" id="reservation"
                                                       name="required_date">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Accreditations</label>
                                            <div class="list-group" id="accreditation-outsource-request">
                                                @if(!empty($accreditations))
                                                    @foreach($accreditations as $accreditation)

                                                        <div style="display: none;"
                                                             data-standard-id="{{$accreditation->standard_id}}">

                                                            <label class="list-group-item"
                                                                   for="single-audit-accreditation{{$accreditation->id}}">
                                                                <input
                                                                        type="radio"
                                                                        name="accreditations[]"
                                                                        value="{{ $accreditation->id }}"
                                                                        id="single-audit-accreditation{{$accreditation->id}}"
                                                                        class="single-audit-accreditation"
                                                                        data-standard-family="{{ ($accreditation->standard) ?  str_replace(' ','-',strtolower($accreditation->standard->standardFamily->name)) : ''}}"
                                                                        onclick="updateIAFOnOutsourceRequest(this, 'iaf-outsource-request', 'ias-outsource-request')"
                                                                />
                                                                &nbsp;&nbsp;{{ $accreditation->name }}
                                                            </label>
                                                        </div>

                                                    @endforeach
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>IAF Codes</label>

                                            <div class="list-group" id="iaf-outsource-request">
                                                @if(!empty($iaf_codes))
                                                    @foreach($iaf_codes as $iaf_code)
                                                        <div style="display: none;"
                                                             data-accreditation-id="{{$iaf_code->accreditation_id}}"
                                                             data-standard-id="{{$iaf_code->standard_id}}"
                                                        >
                                                            <label class="list-group-item"
                                                                   for="single-audit-iaf{{$iaf_code->id}}">
                                                                <input
                                                                        type="radio"
                                                                        name="iaf_ids[]"
                                                                        value="{{ $iaf_code->id }}"
                                                                        class="single-audit-iaf"
                                                                        id="single-audit-iaf{{$iaf_code->id}}"
                                                                        onclick="updateIASOnOutsourceRequest(this, 'ias-outsource-request')"
                                                                />
                                                                &nbsp;&nbsp;{{ $iaf_code->name }}
                                                            </label>
                                                        </div>

                                                    @endforeach
                                                @endif

                                            </div>
                                            {{--<select class="form-control iaf_select" name="iaf_ids[]" id="iaf_id"--}}
                                            {{--multiple="" required>--}}
                                            {{--@if(!empty($iaf_codes))--}}
                                            {{--@foreach($iaf_codes as $iaf_code)--}}
                                            {{--<option value="{{ $iaf_code->id }}">{{ $iaf_code->nameWithCode() }}</option>--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--</select>--}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>IAS Codes</label>
                                            <div class="list-group" id="ias-outsource-request">
                                                @if(!empty($ias_codes))
                                                    @foreach($ias_codes as $ias_code)
                                                        <div style="display: none;"
                                                             data-accreditation-id="{{$ias_code->accreditation_id}}"
                                                             data-iaf-id="{{$ias_code->iaf_id}}"
                                                        >
                                                            <label class="list-group-item"
                                                                   for="single-audit-ias{{$ias_code->id}}">
                                                                <input
                                                                        type="radio"
                                                                        name="ias_ids[]"
                                                                        value="{{ $ias_code->id }}"
                                                                        class="single-audit-ias"
                                                                        id="single-audit-ias{{$ias_code->id}}"/>
                                                                &nbsp;&nbsp;{{ $ias_code->name }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                @endif

                                            </div>
                                            {{--<select class="form-control iaf_select" name="ias_ids[]" id="ias_id"--}}
                                            {{--multiple="" required>--}}
                                            {{--</select>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="food" style="display: none">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Food Category</label>
                                            <select name="food_category" class="form-control" id="food_category">
                                                <option value="">Select Food Category</option>
                                                {{--                                                @if(!empty($food_categories))--}}
                                                {{--                                                    @foreach($food_categories as $food_category)--}}
                                                {{--                                                        <option value="{{ $food_category->id }}"--}}
                                                {{--                                                                data-code="{{ $food_category->code }}">{{ $food_category->code }}--}}
                                                {{--                                                            | {{ $food_category->name }}</option>--}}
                                                {{--                                                    @endforeach--}}
                                                {{--                                                @endif--}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Food Sub Category</label>
                                            <select name="food_subcategory" class="form-control" id="food-subcategory">
                                                <option value="">Select Food Sub Category</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Food Category Code</label>
                                            <p class="results_answer" id="food-category-code">A</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Food Sub Category Code</label>
                                            <p class="results_answer" id="food-subcategory-code">A1</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row" id="energy-management">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Technical Area</label>
                                            <select name="technical_area" class="form-control" id="energy-codes">
                                                <option value="">Select Technical Area</option>
                                                {{--                                                @if(!empty($energy_codes))--}}
                                                {{--                                                    @foreach($energy_codes as $energy_code)--}}
                                                {{--                                                        <option value="{{ $energy_code->id }}"--}}
                                                {{--                                                                data-code="{{ $energy_code->code }}">{{ $energy_code->code }}--}}
                                                {{--                                                            | {{ $energy_code->name }}</option>--}}
                                                {{--                                                    @endforeach--}}
                                                {{--                                                @endif--}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Industry Code</label>
                                            <p class="results_answer" id="energy-code">III</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <label>&nbsp;</label>
                                        <div class="form-group">

                                            <button type="button" class="btn btn_search" id="search_auditor">SEARCH
                                                AUDITOR
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p><strong>Note:</strong> The Auditor wil be available for 10 days for AJTF
                                            selection upon approval.</p>
                                    </div>
                                </div>
                                <div class="row" id="availableAuditor">

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Remarks*</label>
                                            <textarea name="om_remarks" rows="4" class="form-control"
                                                      placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mrgn no_padding">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting no_padding">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success btn_save">SEND
                                                REQUEST
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="ims_sec CustomTabs auditType" id="ims">
                            <form action="" method="post" id="multi-outsource-request">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Standards</label>
                                            <select name="standard_id[]" class="form-control select2" id="standards"
                                                    multiple required
                                                    onchange="updateAccreditationsOnOutsourceRequest('standards','ims-accreditation-outsource-request', 'ims-iaf-outsource-request', 'ims-ias-outsource-request')">
                                                @if(!empty($single_standards))
                                                    @foreach($single_standards as $standard)
                                                        @if(strtolower($standard->standardFamily->name) == "quality" ||
                                                            strtolower($standard->standardFamily->name) == "environment" || $standard->standardFamily->name == 'Occupational Health and Safety')
                                                            <option value="{{ $standard->id }}">{{ $standard->name }}</option>

                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Required month</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                  </span>
                                                </div>
                                                <input type="text" class="form-control float-right" id="reservation2"
                                                       name="required_date">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Accreditations</label>

                                            <div class="list-group" id="ims-accreditation-outsource-request">
                                                @if(!empty($accreditations))
                                                    @foreach($accreditations as $accreditation)

                                                        <div style="display: none;"
                                                             data-standard-id="{{$accreditation->standard_id}}"
                                                        >

                                                            <label class="list-group-item"
                                                                   for="ims-accreditation{{$accreditation->id}}">
                                                                <input
                                                                        type="radio"
                                                                        name="accreditations[]"
                                                                        value="{{ $accreditation->id }}"
                                                                        id="ims-accreditation{{$accreditation->id}}"
                                                                        class="ims-accreditation"
                                                                        onclick="updateIAFOnOutsourceRequestIMS(this, 'ims-iaf-outsource-request', 'ims-ias-outsource-request')"
                                                                />
                                                                &nbsp;&nbsp;{{ $accreditation->name }}
                                                                ({{ $accreditation->standard['name'] }})
                                                            </label>
                                                        </div>

                                                    @endforeach
                                                @endif

                                            </div>

                                            {{--<select name="accreditations[]" class="form-control" multiple="multiple" id="accreditation2">--}}
                                            {{--@if(!empty($accreditations))--}}
                                            {{--@foreach($accreditations as $accreditation)--}}
                                            {{--<option value="{{ $accreditation->id }}">{{ $accreditation->name }}</option>--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--</select>--}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>IAF Codes</label>

                                            <div class="list-group" id="ims-iaf-outsource-request">
                                                @if(!empty($iaf_codes))
                                                    @foreach($iaf_codes as $iaf_code)
                                                        <div style="display: none;"
                                                             data-standard-id="{{$iaf_code->standard_id}}"
                                                             data-accreditation-id="{{$iaf_code->accreditation_id}}"
                                                        >

                                                            <label class="list-group-item"
                                                                   for="ims-iaf{{$iaf_code->id}}">
                                                                <input
                                                                        type="radio"
                                                                        name="iaf_ids[]"
                                                                        value="{{ $iaf_code->id }}"
                                                                        class="ims-iaf"
                                                                        id="ims-iaf{{$iaf_code->id}}"
                                                                        onclick="updateIASOnOutsourceRequest(this, 'ims-ias-outsource-request')"
                                                                />
                                                                &nbsp;&nbsp;{{ $iaf_code->code }}
                                                                . {{ $iaf_code->name }}
                                                                ({{ $iaf_code->standard['name'] }})
                                                            </label>
                                                        </div>

                                                    @endforeach
                                                @endif

                                            </div>

                                            {{--<select class="form-control iaf_select" name="iaf_ids[]" id="iaf_id2"--}}
                                            {{--multiple="" required>--}}
                                            {{--@if(!empty($iaf_codes))--}}
                                            {{--@foreach($iaf_codes as $iaf_code)--}}
                                            {{--<option value="{{ $iaf_code->id }}">{{ $iaf_code->nameWithCode() }}</option>--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--</select>--}}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>IAS Codes</label>

                                            <div class="list-group" id="ims-ias-outsource-request">
                                                @if(!empty($ias_codes))
                                                    @foreach($ias_codes as $ias_code)
                                                        <div style="display: none;"
                                                             data-accreditation-id="{{$ias_code->accreditation_id}}"
                                                             data-iaf-id="{{$ias_code->iaf_id}}"
                                                        >
                                                            <label class="list-group-item"
                                                                   for="ims-ias{{$ias_code->id}}">
                                                                <input
                                                                        type="radio"
                                                                        name="ias_ids[]"
                                                                        value="{{ $ias_code->id }}"
                                                                        class="ims-ias"
                                                                        id="ims-ias{{$ias_code->id}}"/>
                                                                &nbsp;&nbsp;{{ $ias_code->code }}
                                                                . {{ $ias_code->name }}
                                                                ({{ $ias_code->standard['name'] }})
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                @endif

                                            </div>

                                            {{--<select class="form-control iaf_select" name="ias_ids[]" id="ias_id2"--}}
                                            {{--multiple="" required>--}}
                                            {{--</select>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <label>&nbsp;</label>
                                        <div class="form-group">

                                            <button type="button" class="btn btn_search" id="search_auditor_ims">SEARCH
                                                AUDITOR
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p><strong>Note:</strong> The Auditor wil be available for 10 days for AJTF
                                            selection upon approval.</p>
                                    </div>
                                </div>
                                <div class="row" id="availableAuditorIMS">

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <textarea name="remarks" rows="4" class="form-control"
                                                      placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mrgn no_padding">
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting"></div>
                                    <div class="col-md-3 floting no_padding">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success btn_save">SEND
                                                REQUEST
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>

        var previousID = null;
        $(function () {
            //Initialize Select2 Elements
            $(function () {
                //Initialize Select2 Elements
                $('.select2').select2({
                    width: '100%'
                });
            });


            //Date range picker
            $('#reservation').daterangepicker({
                format: 'YYYY-MM-DD'
            });
            $('#reservation2').daterangepicker({
                format: 'YYYY-MM-DD'
            });


            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        });


        function updateAccreditationsOnOutsourceRequest(standard, accreditation, iaf, ias) {

            $("#" + accreditation + " div").hide();

            if ($("#" + standard).val()) {
                if ($("#" + standard).val().constructor === Array) {

                    $("#" + standard).val().map(ismStandard => {

                        $("#" + accreditation + " div[data-standard-id=" + ismStandard + "] input[type=radio]").prop('checked', false);
                        $("#" + accreditation + " div[data-standard-id=" + ismStandard + "]").show();

                    });

                } else {

                    $("#" + accreditation + " div[data-standard-id=" + $("#" + standard).val() + "] input[type=radio]").prop('checked', false);
                    $("#" + accreditation + " div[data-standard-id=" + $("#" + standard).val() + "]").show();

                }
            }


            $("#" + iaf + " div").hide();
            $("#" + iaf + " input[type=radio]").prop('checked', false);

            $("#" + ias + " div").hide();
            $("#" + ias + " input[type=radio]").prop('checked', false);

        }


        function updateIAFOnOutsourceRequest(e, iaf, ias) {

            if ($(e).is(":checked")) {
                $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "]").show();


                if ($(e).data('standard-family') == 'food') {
                    form = $(this).closest('form');
                    node = $(this);
                    node_to_modify = '#food_category';
                    var accreditation_id = $(e).val();
                    console.log(accreditation_id);
                    var request = "accreditation_id=" + accreditation_id;

                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.foodCategoryByAccreditation') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            console.log(response);
                            ajaxResponseHandler(response);
                            if (response.status == "success") {
                                var html = "";
                                $.each(response.data.food_category, function (i, obj) {
                                    html += '<option value="">-- Select Category --</option>';
                                    html += '<option data-code="' + obj.code + '" value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                                });
                                $(node_to_modify).html(html);
                                $('#food').fadeIn();
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else if ($(e).data('standard-family') == 'energy-management') {
                    //    Energy Family
                    form = $(this).closest('form');
                    node = $(this);
                    node_to_modify = '#energy-codes';
                    var accreditation_id = $(e).val();
                    console.log(accreditation_id);
                    var request = "accreditation_id=" + accreditation_id;

                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.energyByAccreditation') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            console.log(response);
                            ajaxResponseHandler(response);
                            if (response.status == "success") {
                                var html = "";
                                $.each(response.data.energy_codes, function (i, obj) {
                                    html += '<option value="">-- Select  Technical Area --</option>';
                                    html += '<option data-code="' + obj.code + '" value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                                });
                                $(node_to_modify).html(html);
                                $('#energy-management').fadeIn();
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    $('#food').fadeOut();
                    $('#energy-management').fadeOut();
                }


            } else {


                let checkBox = $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "] input[type=radio]");

                for (var i = checkBox.length - 1; i >= 0; i--) {
                    if (checkBox.is(':checked')) {

                        $("#" + ias + " div[data-iaf-id=" + checkBox.val() + "][data-accreditation-id=" + $(e).val() + "]").hide();
                        $("#" + ias + " div[data-iaf-id=" + checkBox.val() + "][data-accreditation-id=" + $(e).val() + "]").prop('checked', false);

                    }

                }


                $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "] input[type=radio]").prop('checked', false);
                $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "]").hide();

            }

        }

        function updateIAFOnOutsourceRequestIMS(e, iaf, ias) {

            if ($(e).is(":checked")) {
                $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "]").show();


                if ($(e).data('standard-family') == 'food') {
                    form = $(this).closest('form');
                    node = $(this);
                    node_to_modify = '#food_category';
                    var accreditation_id = $(e).val();
                    console.log(accreditation_id);
                    var request = "accreditation_id=" + accreditation_id;

                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.foodCategoryByAccreditation') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            console.log(response);
                            ajaxResponseHandler(response);
                            if (response.status == "success") {
                                var html = "";
                                $.each(response.data.food_category, function (i, obj) {
                                    html += '<option value="">-- Select Category --</option>';
                                    html += '<option data-code="' + obj.code + '" value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                                });
                                $(node_to_modify).html(html);
                                $('#food').fadeIn();
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else if ($(e).data('standard-family') == 'energy-management') {
                    //    Energy Family
                    form = $(this).closest('form');
                    node = $(this);
                    node_to_modify = '#energy-codes';
                    var accreditation_id = $(e).val();
                    console.log(accreditation_id);
                    var request = "accreditation_id=" + accreditation_id;

                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.energyByAccreditation') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            console.log(response);
                            ajaxResponseHandler(response);
                            if (response.status == "success") {
                                var html = "";
                                $.each(response.data.energy_codes, function (i, obj) {
                                    html += '<option value="">-- Select  Technical Area --</option>';
                                    html += '<option data-code="' + obj.code + '" value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                                });
                                $(node_to_modify).html(html);
                                $('#energy-management').fadeIn();
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    $('#food').fadeOut();
                    $('#energy-management').fadeOut();
                }


            } else {


                let checkBox = $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "] input[type=radio]");

                for (var i = checkBox.length - 1; i >= 0; i--) {
                    if (checkBox.is(':checked')) {

                        $("#" + ias + " div[data-iaf-id=" + checkBox.val() + "][data-accreditation-id=" + $(e).val() + "]").hide();
                        $("#" + ias + " div[data-iaf-id=" + checkBox.val() + "][data-accreditation-id=" + $(e).val() + "]").prop('checked', false);

                    }

                }


                $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "] input[type=radio]").prop('checked', false);
                $("#" + iaf + " div[data-accreditation-id=" + $(e).val() + "]").hide();

            }

        }

        function updateIASOnOutsourceRequest(e, ias) {

            if (previousID == null) {

            } else {
                $("#" + ias + " div[data-iaf-id=" + previousID + "]").hide();

            }
            if ($(e).is(":checked")) {

                $("#" + ias + " div[data-iaf-id=" + $(e).val() + "]").show();
                previousID = $(e).val();
            } else {

                $("#" + ias + " div[data-iaf-id=" + $(e).val() + "] input[type=radio]").prop('checked', false);
                $("#" + ias + " div[data-iaf-id=" + $(e).val() + "]").hide();
            }

        }

        $(document).ready(function () {
            $('#add_address').click(function () {
                var $copyDiv = $('.address_main_div').clone();
                $('.address_add_div').html($copyDiv);
            });
        });

        $('#singleAudit_div').hide();
        $('#ims').hide();

        $('input.audit_radio').on('ifChanged', function () {
            var val = $(this).val();
            $('.auditType').hide();
            $('#' + val).show();
        });


        $('#food').hide();
        $('#energy-management').hide();
        $('#standard_type').change(function () {
            var standard_type = $(this).find(':selected').attr('data-id');
            console.log(standard_type);
            // if (standard_type === 'food') {
            //     $("#food").fadeIn();
            //     $("#energy-management").fadeOut();
            // } else if (standard_type === 'energy-management') {
            //     $("#energy-management").fadeIn();
            //     $("#food").fadeOut();
            // } else {
            $("#energy-management").fadeOut();
            $("#food").fadeOut();
            // }
        });

        $('#iaf_id').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#ias_id';
            var iaf_id = $(this).val();
            console.log(iaf_id);
            var request = "iaf_id=" + iaf_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.iasByIAF') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    console.log(response);
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        resetIASCodes();
                        var html = "";
                        $.each(response.data.iases, function (i, obj) {
                            html += '<option data-iaf-id="iaf_' + iaf_id + '" value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).append(html);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });
        $('#iaf_id2').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#ias_id2';
            var iaf_id = $(this).val();
            console.log(iaf_id);
            var request = "iaf_id=" + iaf_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.iasByIAF') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    console.log(response);
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        resetIASCodes2();
                        var html = "";
                        $.each(response.data.iases, function (i, obj) {
                            html += '<option data-iaf-id="iaf_' + iaf_id + '" value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).append(html);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function resetIASCodes() {
            $('#iaf_id').each(function (k, node) {
                if (!$(node).is(':selected')) {
                    var val = $(node).val();
                    $('option[data-iaf-id="iaf_' + val + '"]').remove();
                }
            });
        }

        function resetIASCodes2() {
            $('#iaf_id2').each(function (k, node) {
                if (!$(node).is(':selected')) {
                    var val = $(node).val();
                    $('option[data-iaf-id="iaf_' + val + '"]').remove();
                }
            });
        }

        $("#search_auditor").click(function () {

            var standardId = $("#standard_type").val();

            var accreditation = $(".single-audit-accreditation:radio:checked").map(function () {
                return $(this).val();
            }).get();

            var iaf_id = $(".single-audit-iaf:radio:checked").map(function () {
                return $(this).val();
            }).get();

            var ias_id = $(".single-audit-ias:radio:checked").map(function () {
                return $(this).val();
            }).get();
            console.log(accreditation, iaf_id, ias_id)

            $.ajax({
                type: "POST",
                url: "{{ route('outsource.search.auditor') }}",
                data: {
                    'standard_id': standardId,
                    'accreditation': accreditation,
                    'iaf_id': iaf_id,
                    'ias_id': ias_id
                },
                /* processData: false,*/
                /*  contentType: false,*/
                cache: false,
                success: function (response) {
                    $("#availableAuditor").html(response);

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#search_auditor_ims").click(function () {
            var standards_id = $("#standards").val();
            var standard_type = $("#standard_type").val();

            var accreditation = $(".ims-accreditation:radio:checked").map(function () {
                return $(this).val();
            }).get();

            var iaf_id = $(".ims-iaf:radio:checked").map(function () {
                return $(this).val();
            }).get();
            var ias_id = $(".ims-ias:radio:checked").map(function () {
                return $(this).val();
            }).get();


            $.ajax({
                type: "POST",
                url: "{{ route('outsource.search.auditor.ims') }}",
                data: {
                    'standard_type': standard_type,
                    'standard_id': standards_id,
                    'accreditation': accreditation,
                    'iaf_id': iaf_id,
                    'ias_id': ias_id
                },
                /* processData: false,*/
                /*  contentType: false,*/
                cache: false,
                success: function (response) {

                    $("#availableAuditorIMS").html(response);

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#food_category").change(function () {

            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '#food-subcategory';
            var food_category = $(this).val();
            var food_code = $('#food_category').find('option:selected').attr('data-code');
            $("#food-category-code").text(food_code);
            var request = "food_category_id=" + food_category;

            if (food_category !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.foodSubCategoryByFoodCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.foodsubcategories, function (i, obj) {

                                html += '<option value="' + obj.id + '" data-code="' + obj.code + '">' + obj.code + ' | ' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            sortMe($(node_to_modify).find('option'));
                            $(node_to_modify).prepend("<option value='' selected>Select Food Sub-Code</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select Food Sub-Code</option>");
            }
        });

        $("#food-subcategory").change(function () {
            var code = $("#food-subcategory").find('option:selected').attr('data-code');
            $("#food-subcategory-code").text(code);
        });

        $("#energy-codes").change(function () {
            var code = $("#energy-codes").find('option:selected').attr('data-code');
            $("#energy-code").text(code);
        });

        $("#single-outsource-request").submit(function (e) {
            e.preventDefault();
            var form = $("#single-outsource-request")[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('outsource.request.store') }}",
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        window.location.href = '{{route('outsource.index')}}';
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        $("#multi-outsource-request").submit(function (e) {
            e.preventDefault();
            var form = $("#multi-outsource-request")[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('multi.outsource.request.store') }}",
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        window.location.href = '{{route('outsource.index')}}';

                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });


        $('#refresh-ims').on('ifChanged', function () {
            refresh('single');
        });


        $('#refresh-single').on('ifChanged', function () {
            refresh('ims');
        });

        function refresh(type) {
            if(type =='ims'){
                $("#single-outsource-request")[0].reset();
                $('#standard_type').val('').trigger('change');

            }else{
                $("#multi-outsource-request")[0].reset();
                $('.select2').val('').trigger('change');
            }

        }
    </script>
@endpush