@extends('layouts.master')
@section('title', "Outsource Request List")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-6 floting">
                            <h1>Outsource Request</h1>
                        </div>
                        {{--                        <div class="col-sm-6 floting">--}}
                        {{--                            --}}{{--                            @can('add_outsource_request')--}}
                        {{--                            <a href="{{route('outsource.create')}}" class="add_comp"><label--}}
                        {{--                                        class="text-capitalize"><i class="fa fa-plus-circle text-lg"--}}
                        {{--                                                                   aria-hidden="true"></i> Add New--}}
                        {{--                                    Outsource Request</label></a>--}}
                        {{--                            --}}{{--                            @endcan--}}
                        {{--                        </div>--}}
                    </div>

                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('outsource-request') }}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of Outsource Request</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="8%;">Sr. #</th>
                                    <th>Auditor Name</th>
                                    <th>Auditor Region</th>
                                    <th>Requested Date</th>
                                    {{-- <th>Requested By</th>
                                    <th>Requested To</th> --}}
                                    <th>Status</th>
                                    <th>Expiry Status</th>
                                    <th>Expiry Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($outsourceRequests as $key => $outsourceRequest)
                                    <tr>

                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $outsourceRequest->outsource_request_auditor->auditor->fullName() }}</td>
                                        <td>{{ $outsourceRequest->outsource_request_auditor->auditor->region->title }}</td>
                                        <td>{{ $outsourceRequest->created_at->format('d-m-Y') }}</td>
                                        
                                        {{-- @if($outsourceRequest->operation_manager_id != NULL)
                                            <td>{{ ucfirst($outsourceRequest->operation_manager->user->fullName()) }}</td>
                                        @else
                                            <td>{{ ucfirst($outsourceRequest->operation_coordinator->user->fullName()) }}</td>
                                        @endif --}}

                                        {{-- <td>{{ ucfirst($outsourceRequest->operation_approval_manager->user->fullName()) }}
                                            / {{ ucfirst($outsourceRequest->operation_approval_manager->user->region->title) }}</td> --}}
                                        <td>
                                            @if($outsourceRequest->status == 'request')
                                                Requested
                                            @elseif($outsourceRequest->status == 'approved')
                                                Approved
                                            @else
                                                Rejected
                                            @endif
                                        </td>
                                        <td>
                                            @if($outsourceRequest->status == 'request')
                                                <button class="btn btn-sm btn-info">Not Accepted yet</button>
                                            @elseif($outsourceRequest->status == 'approved')
                                                @if($outsourceRequest->expiry_status == 'expired')
                                                    <button class="btn btn-sm btn-danger">{{ ucfirst($outsourceRequest->expiry_status) }}</button>
                                                @else
                                                    <button class="btn btn-sm  btn-warning">{{ ucfirst($outsourceRequest->expiry_status) }}</button>
                                                @endif
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>

                                            @if($outsourceRequest->status == 'request' && is_null($outsourceRequest->expired_date))
                                                Not Accepted yet
                                            @elseif($outsourceRequest->status == 'approved')
                                                @if($outsourceRequest->expiry_status == 'expired')
                                                    {{ date('d-m-Y', strtotime($outsourceRequest->expired_date)) }}
                                                @else
                                                    {{ date('d-m-Y', strtotime($outsourceRequest->expired_date)) }}
                                                @endif
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>
                                            <ul class="data_list">
                                                <li>
                                                    @can('show_outsource_request')
                                                        <a href="{{route('outsource.show', $outsourceRequest->id)}}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endcan
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#example1').dataTable();
        });
    </script>
@endpush