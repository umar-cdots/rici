@extends('layouts.master')
@section('title', "Outsource Request View")

@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 620.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">OUTSOURCE <span>REQUEST</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">View</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="#" method="post" id="outsource_request">
                        <input type="hidden" name="outsource_request_id" value="{{ $outsourceRequest->id }}">
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Suggested Auditor</label>
                                    <p class="results_answer">{{ $outsourceRequest->outsource_request_auditor->auditor->fullName() }}
                                        / {{ $outsourceRequest->outsource_request_auditor->auditor->region->title }}</p>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Requested By</label>
                                        @if($outsourceRequest->operation_manager_id != NULL)
                                            <p class="results_answer">{{ ucfirst($outsourceRequest->operation_manager->user->fullName()) }}
                                                / {{ ucfirst($outsourceRequest->operation_manager->user->region->title) }}</p>
                                        @else
                                            <p class="results_answer">{{ ucfirst($outsourceRequest->operation_coordinator->user->fullName()) }}</p>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Requested To</label>
                                        <p class="results_answer">{{ ucfirst($outsourceRequest->operation_approval_manager->user->fullName()) }}
                                            / {{ ucfirst($outsourceRequest->operation_approval_manager->user->region->title) }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Standards</label>

                                        <p class="results_answer">
                                            @foreach($outsourceRequest->outsource_request_standards as $key=>$outsourceRequestStandard)
                                                @php
                                                    $company = \App\Company::where('id', $outsourceRequestStandard->company_id)->first();
                                                    $companyStandard = \App\CompanyStandards::where('company_id', $outsourceRequestStandard->company_id)->where('standard_id', $outsourceRequestStandard->standard->id)->first();
                                                    $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();

                                                    if(!is_null($companyStandard)){
                                                        $ims_heading = $companyStandard->standard->name;
                                                    }

                                                    if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                                        foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                                            if ($companyStandard->id == $imsCompanyStandard->id) {
                                                                continue;
                                                            } else {
                                                                $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                                                            }
                                                        }
                                                    } else {
                                                       $ims_heading =$outsourceRequestStandard->standard->name;
                                                    }
                                                    $len=count($outsourceRequest->outsource_request_standards);
                                                @endphp
                                                {{ $ims_heading }}
                                                @if($key == $len -1)
                                                @else
                                                    @if($key%2 == 0)
                                                        ,
                                                    @else
                                                        ,
                                                    @endif
                                                @endif

                                            @endforeach
                                        </p>


                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Accreditations</label>
                                        <p class="results_answer">
                                            @foreach($outsourceRequest->outsource_request_standards as $std)
                                                @foreach($std->outsource_request_standard_accreditations as $key=>$accreditations)
                                                    {{$accreditations->accreditation->name }}
                                                    @php

                                                        $len=count($std->outsource_request_standard_accreditations);
                                                    @endphp
                                                    @if($key == $len -1)
                                                    @else
                                                        @if($key%2 == 0)
                                                            ,
                                                        @else
                                                            ,
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>IAF Codes</label>
                                        <p class="results_answer">
                                            @foreach($outsourceRequest->outsource_request_standards as $std)
                                                @if($std->outsource_request_standard_iaf_codes->count() > 0)

                                                    @foreach($std->outsource_request_standard_iaf_codes as $key=>$iaf_codes)
                                                        {{$iaf_codes->iaf_code->code }}
                                                        @php

                                                            $len=count($std->outsource_request_standard_iaf_codes);
                                                        @endphp
                                                        @if($key == $len -1)
                                                        @else
                                                            @if($key%2 == 0)
                                                                ,
                                                            @else
                                                                ,
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    ----
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>IAS Codes</label>
                                        <p class="results_answer">
                                            @foreach($outsourceRequest->outsource_request_standards as $std)
                                                @if($std->outsource_request_standard_ias_codes->count() > 0)
                                                    @foreach($std->outsource_request_standard_ias_codes as $key=>$ias_codes)
                                                        @php

                                                            $len=count($std->outsource_request_standard_ias_codes);
                                                        @endphp
                                                        {{$ias_codes->ias_code->code }}
                                                        @if($key == $len -1)
                                                        @else
                                                            @if($key%2 == 0)
                                                                ,
                                                            @else
                                                                ,
                                                            @endif
                                                        @endif



                                                    @endforeach
                                                @else
                                                    ----
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Food Category</label>
                                        <p class="results_answer">

                                            @foreach($outsourceRequest->outsource_request_standards as $std)
                                                @if($std->outsource_request_standard_food_codes->count() > 0)

                                                    @foreach($std->outsource_request_standard_food_codes as $key=>$food_codes)
                                                        @php

                                                            $len=count($std->outsource_request_standard_food_codes);
                                                        @endphp
                                                        {{$food_codes->food_category->name }}
                                                        @if($key == $len -1)
                                                        @else
                                                            @if($key%2 == 0)
                                                                ,
                                                            @else
                                                                ,
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    ----
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Energy Codes</label>
                                        <p class="results_answer">
                                            @foreach($outsourceRequest->outsource_request_standards as $std)
                                                @if($std->outsource_request_standard_energy_codes->count() > 0)
                                                    @foreach($std->outsource_request_standard_energy_codes as $key=>$energy_codes)
                                                        {{$energy_codes->energy_category->name }}
                                                        @php

                                                            $len=count($std->outsource_request_standard_energy_codes);
                                                        @endphp
                                                        @if($key == $len -1)
                                                        @else
                                                            @if($key%2 == 0)
                                                                ,
                                                            @else
                                                                ,
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    ----
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Requested Date</label>
                                        <p>{{ $outsourceRequest->created_at->format('d/m/Y') }}</p>
                                    </div>
                                </div>
                                @if($outsourceRequest->status == 'approved')
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Approval Date</label>
                                            <p>{{ $outsourceRequest->updated_at->format('d/m/Y') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Expiry Date</label>
                                            <p>{{ $outsourceRequest->updated_at->addDays(6)->format('d/m/Y') }}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if(($outsourceRequest->status == 'request') &&  auth()->user()->user_type =='operation_manager' )
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Approval/Rejection Operation Manager Remarks *</label>
                                            <textarea class="form-control" name="sm_remarks" rows="4" id="sm_remarks"
                                                      placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($outsourceRequest->outsource_request_remarks))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Comments Box</label>
                                            <div class="card-body chat_box" style="display: block;">
                                                <div class="direct-chat-messages">

                                                    @foreach($outsourceRequest->outsource_request_remarks as $key=>$remarks)

                                                        @if((($outsourceRequest->operation_manager_id) || ($outsourceRequest->operation_coordinator_id)) && $remarks->operations_remarks != NULL)
                                                            <div class="direct-chat-msg">
                                                                <div class="direct-chat-info clearfix">
                                                                    <span class="direct-chat-name">Requested By:</span>
                                                                </div>
                                                                <span class="direct-chat-timestamp">{{ucfirst( $remarks->operations_remarks) }}</span><br>
                                                                <span class="direct-chat-timestamp">{{ $remarks->created_at->diffForHumans() }}</span>
                                                            </div>

                                                        @endif
                                                        @if( $outsourceRequest->operation_approval_manager_id && $remarks->manager_remarks)
                                                            <div class="direct-chat-msg text-right">
                                                                <div class="direct-chat-info clearfix">
                                                                    <span class="direct-chat-name">Requested To:</span>
                                                                </div>
                                                                <span class="direct-chat-timestamp">  {{ucfirst( $remarks->manager_remarks) }}</span><br>
                                                                <span class="direct-chat-timestamp">  {{ $remarks->created_at->diffForHumans() }}</span>
                                                            </div>
                                                        @endif
                                                    @endforeach

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if(auth()->user()->user_type =='operation_manager')
                                @if($outsourceRequest->status == 'request')
                                    <div class="col-md-12 mrgn no_padding">
                                        <div class="col-md-3 floting"></div>
                                        <div class="col-md-3 floting"></div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-success btn_save"
                                                        onclick="outSourceRequestSubmit('approved',event)">APPROVE
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting no_padding">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-block btn_search"
                                                        onclick="outSourceRequestSubmit('rejected',event)">REJECT
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                @endif
                            @endif
                        </div>
                        <!-- /.card-body -->
                    </form>

                </div>

            </div>


        </section>
        <!-- /.content -->
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script>
        $(function () {
            //Date range picker
            //Date range picker
            $('#reservation').daterangepicker({
                format: 'MM-DD-YYYY'
            });

        });

        function outSourceRequestSubmit(type, e) {


            e.preventDefault();
            var form = $('#outsource_request')[0];
            var formData = new FormData(form);
            var status = '';
            if (type === 'approved') {
                status = type;
            } else if (type === 'rejected') {
                status = "rejected";
            }


            formData.append('status', status);

            $.ajax({
                type: "POST",
                url: "{{ route('outsource.request.update') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {


                    ajaxResponseHandler(response, form);
                    if (response.data.status == 'success' || response.data.status == 'error') {
                        setTimeout(function () {
                            window.location = '{{ route('outsource.index') }}';
                        }, 3000);
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });


        }

    </script>
@endpush