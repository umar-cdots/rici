@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

    <style>
        .largerCheckbox{
            width: 40px;
            height: 40px;
        }
    </style>
@endpush

<div class="modal fade changeStatus" id="certificatesStandard" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);">
        <form id="certificate-store" action="#" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">Change Status</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <p>Current Change Status:
                                <span>
                                    @if(!is_null($certificate_current_status))
                                        {{ str_replace('_',' ', ucwords($certificate_current_status->certificate_status)) }}
                                    @else
                                        Not Certified
                                    @endif
                                </span>
                            </p>
                        </div>
                        <input type="hidden" name="company_standard_id" id="company_standard_id"
                               value="{{$company_standard_id}}">
                        <div class="col-md-4 floting">
                            <div class="form-group">
                                <label for="certificate_status">Change Status To</label>
                                <select name="certificate_status" id="certificate_status" class="form-control">
                                    <option value="certified">Certified</option>
                                    <option value="suspended">Suspended</option>
                                    <option value="withdrawn">Withdrawn</option>
                                    <option value="closed">Closed</option>
                                    <option value="not_certified" selected>Not Certified</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 floting">
                            <label for="certificate_date">Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control float-right active certificate_date" required
                                       name="certificate_date" id="certificate_date" placeholder="dd-mm-yyyy"
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-2 floting">
                            <label for="certificate_date">Print Letter</label>
                            <div class="input-group">
                                <input type="checkbox" class="float-right largerCheckbox" style="width: 30px !important; height: 30px !important;" checked value="1"
                                       name="is_print_letter" id="is_print_letter"
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-12 floting" id="forSuspension" style="display: none">
                            <label for="certificate_date">Next Due Activity</label>
                            <div class="input-group">
                                @if(!empty($ajStandardStages) && count($ajStandardStages) > 0)
                                    @foreach($ajStandardStages as $ajStandardStage)
                                        <div class="col-md-3">

                                            <label for="certificate_date" class="radio-inline">
                                                <input type="radio"
                                                       class="certificate_radio_check_value"
                                                       value="{{$ajStandardStage->id}}"
                                                       name="aj_standard_id"
                                                       id="aj_standard_id"
                                                       placeholder=""
                                                       autocomplete="off">({{ucfirst(str_replace('_',' ',$ajStandardStage->audit_type))}}
                                                )
                                                Cycle @if(!is_null($ajStandardStage->recycle))
                                                    {{$ajStandardStage->recycle + 1}}
                                                @else
                                                    1
                                                @endif
                                            </label>

                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="suspension_error_required" style="color: red;display: none;">Please select atleast one option.</div>
                        </div>
                        <div class="col-md-4 floting" id="forSuspensionReason" style="display: none">
                            <div class="form-group">
                                <label for="suspension_reason">Suspension Reason</label>
                                <select name="suspension_reason" id="suspension_reason" class="form-control">
                                    <option value="delayed_audit">Delayed audit</option>
                                    <option value="cars_not_closed">CARs not closed</option>
                                    <option value="cat_one_car_raised">CAT I CAR raised</option>
                                    <option value="voluntary_withdrawl">Voluntary withdrawl</option>
                                    <option value="not_following_scheme_rules">Not following Scheme Rules</option>
                                    <option value="changed_CB">Changed CB</option>
                                    <option value="company_is_closed">Company is Closed</option>
                                    <option value="certificate_expired">Certificate Expired</option>
                                    <option value="other" selected>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn-success btn_save"
                                            onclick="certificateSubmit(event)">Save
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-2 floting no_padding">
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn_search" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Note: If you delete any record first add new record then delete.</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="card">
                            <div class="card-header border-transparent">
                                <h3 class="card-title">View History</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse">
                                        ▼
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0" style="display: block;">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Status</th>
                                            <th>Date Approved</th>
                                            <th>Reason</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($certificates) && (count($certificates)> 0))

                                            @foreach($certificates as $key=>$certificate)
                                                <tr>
                                                    <td>{{$key +1 }}</td>
                                                    <td>{{ str_replace('_',' ', ucwords($certificate->certificate_status)) }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($certificate->certificate_date)) }}</td>
                                                    <td>
                                                        @if($certificate->certificate_status == 'suspended')
                                                            {{!is_null($certificate->suspension_reason) ? ucfirst(str_replace('_',' ',$certificate->suspension_reason)) : 'N/A'}}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </td>
                                                    <td><a href="javascript:void(0)" data-id="{{$certificate->id}}"
                                                           id="deleteCertificateSubmit" class="deleteCertificateSubmit"><i
                                                                    class="fa fa-trash"></i></a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3">No Record Found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.card-body -->
                            <!-- /.card-footer -->
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>

    </div>
</div>
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#certificate-store').reset();

        });
        $('.certificate_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        // $('#certificate_status').on('change', function () {
        //     if ($(this).find('option:selected').val() == 'suspended') {
        //         $('#forSuspension').show();
        //     } else {
        //         $('#forSuspension').hide();
        //     }
        // })
        // function certificateStatusChange(){
        //
        //     if ($('#certificate_status').find('option:selected').val() == 'suspended') {
        //         $('#forSuspension').show();
        //     } else {
        //         $('#forSuspension').hide();
        //     }
        // }
        $(document).on('change', '#certificate_status', function () {

            if ($(this).find('option:selected').val() == 'suspended') {
                $('#forSuspension').show();
                $('#forSuspensionReason').show();
            } else {
                $('#forSuspension').hide();
                $('#forSuspensionReason').hide();
            }
        })

    </script>

@endpush
