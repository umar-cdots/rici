<div class="modal fade changeStatus" id="companyStandardRemarksModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="position: fixed;
    top: 50%;
    width: 30% !important;
    left: 50%;
    transform: translate(-50%, -50%);">
        <form id="company-standard-remarks-store" action="#" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">Add/Update Planning Remarks</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <input type="hidden" name="company_standard_id" id="company_standard_id"
                               value="{{$company_standard_id}}">
                        <div class="col-md-12 floting">
                            <div class="form-group">
                                <label for="certificate_status">Remarks</label>
                                <br>
                                <textarea name="remarks" id="remarks{{$company_standard_id}}" class="form-control"
                                          rows="5" cols="5"
                                          required>{{ $companyStandard->main_remarks ?? '' }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-4 floting">
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn-success btn_save"
                                            onclick="companyStandardRemarksSubmit(event,{{$company_standard_id}})">Save
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 floting no_padding">
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn_search" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </form>

    </div>
</div>
@push('scripts')

@endpush