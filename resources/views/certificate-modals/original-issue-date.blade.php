@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush

<div class="modal fade changeStatus" id="originalIssueDateModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);">
        <form id="original-issue-date-store" action="#" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">Change Original Issue Date</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <input type="hidden" name="scheme_manager_post_audit_id" id="scheme_manager_post_audit_id"
                               value="{{$scheme_manager_post_audit_id}}">
                        <div class="col-md-12 floting">
                            <label for="original_issue_date">Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control float-right active original_issue_date" required
                                       name="original_issue_date" id="original_issue_date" placeholder="dd-mm-yyyy" value="{{date("d-m-Y", strtotime($scheme_manager_post_audit->original_issue_date))}}"
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-4 floting"></div>
                            <div class="col-md-2 floting">
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn-success btn_save"
                                            onclick="updateOriginalIssueDateSubmit(event)">Save
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-2 floting no_padding">
                                <div class="form-group">
                                    <button type="button" class="btn btn-block btn_search" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>

    </div>
</div>
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#original-issue-date-store').reset();
        });
        $('.original_issue_date').datepicker({
            format: 'dd-mm-yyyy',
        });
    </script>
@endpush