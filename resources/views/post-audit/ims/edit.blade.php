@extends('layouts.master')
@section('title', "Edit Post Audit {$audit_type}")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
    <style>
        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 1px !important;
        }

        .daterangepicker .ranges .input-mini {
            border: 1px solid #ccc;
            border-radius: 4px;
            color: #555;
            display: block;
            font-size: 17px !important;
            height: 37px !important;
            line-height: 30px;
            vertical-align: middle;
            margin: 0 0 10px 0;
            padding: 0 6px;
            width: 160px !important;
        }

        .daterangepicker .daterangepicker_start_input label, .daterangepicker .daterangepicker_end_input label {
            font-size: 16px !important;

        }

        .actual_audit_date_error,
        .q1_name_error,
        .q2_name_error,
        .q3_name_error,
        .question_ones_filename_0_error,
        .question_two_filename_0_error,
        .question_three_filename_0_error,
        .question_four_filename_0_error,
        .remarks_error,
        .evidence_error {
            color: red;
        }

        .daterangepicker .ranges .input-mini {
            font-size: 20px !important;
            height: 41px !important;
            width: 120px !important;
        }

        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 0px !important;
        }
    </style>
@endpush
@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 620.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">AUDIT <span>ACTIVITY</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Audit Activity</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" onsubmit="return validate(event)" action="{{ route('ims.post.audit.update') }}"
                          method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="status" id="status" value="">
                        <input type="hidden" name="company_id" value="{{$company->id}}">
                        <input type="hidden" name="standard_id" value="{{$standard->id}}">
                        <input type="hidden" name="aj_standard_stage_id" value="{{$standardStage->id}}">
                        <input type="hidden" name="audit_type" value="{{$audit_type}}">
                        <input type="hidden" name="questions_one_id" value="{{ $questions[0]->id }}">
                        <input type="hidden" name="questions_two_id" value="{{ $questions[1]->id }}">
                        <input type="hidden" name="questions_three_id" value="{{ $questions[2]->id }}">
                        <input type="hidden" name="post_audit_id" value="{{ $postAudit->id }}">
                        <input type="hidden" name="ims_heading" value="{{ $ims_heading }}">
                        @if(!is_null($postAuditQuestionsOne))
                            <input type="hidden" name="post_audit_question_one_id"
                                   value="{{ $postAuditQuestionsOne->id }}">
                        @endif

                        @if(!is_null($postAuditQuestionsTwo))
                            <input type="hidden" name="post_audit_question_two_id"
                                   value="{{ $postAuditQuestionsTwo->id }}">
                        @endif

                        @if(!is_null($postAuditQuestionsThree))
                            <input type="hidden" name="post_audit_question_three_id"
                                   value="{{ $postAuditQuestionsThree->id }}">
                        @endif


                        <div class="card-body card_cutom auditActivity auditJustificationTable filesUploadColor">
                            <div class="row">
                                <h6 class="blu_clr">{{ ucfirst($company->name) }} &nbsp; - &nbsp; {{ $audit_type }}
                                    Audit
                                    &nbsp; - &nbsp; {{ $ims_heading }} Standard(s)</h6>
                            </div>
                            <div class="row rowBgGrey mrgn">
                                <div class="col-md-12">

                                    <div class="col-md-2 floting">
                                        <label for="actual_audit_date">Actual Audit Date :</label>
                                    </div>
                                    <div class="col-md-4 floting">

                                        <div class="form-group">
                                            <div class="input-group mb-0">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>


                                                <input type="text" name="actual_audit_date" readonly
                                                       id="actual_audit_date"
                                                       value="{{ date("d-m-Y", strtotime($postAudit->actual_audit_date)) }} - {{ date("d-m-Y", strtotime($postAudit->actual_audit_date_to)) }}"
                                                       class="form-control float-right active actual_audit_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="actual_audit_date_error"></label>
                                </div>
                            </div>
                            @if($audit_type != 'Stage 1')
                                <div class="row rowBgGrey mrgn">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h5 class="blu_clr">Corrective Action Request Confirmation:</h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[0]->title_for_oc_om }}</label>

                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q1_name" id="q1_name_yes"
                                                                   value="YES" {{ (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES') ? 'checked' : '' }}>
                                                            <label class="q1_name_yes">Yes</label>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q1_name" id="q1_name_no"
                                                                   value="NO" {{ (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'NO') ? 'checked' : '' }}>
                                                            <label class="q1_name_no">No</label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <label class="q1_name_error"></label>
                                            <label class="question_ones_filename_0_error"></label>

                                            <div class="clearfix"></div>
                                        </div>


                                        @if(isset($postAuditQuestionsOne) &&  $postAuditQuestionsOne->value == 'YES')
                                            <div class="col-md-12 question_1"
                                                 style="display:{{((!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                                <div class="row rowBgGrey">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2 floting">
                                                            <label> Upload Verification Pack</label>
                                                        </div>
                                                        <div class="col-md-10 floting">


                                                            <div class="input-group control-group_q1_files increment_q1_files">
                                                                <input type="file" name="q1_files[]"
                                                                       class="form-control" id="q1_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-success q1_files_add"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-plus"></i>Add
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="clone_q1_files hide">
                                                                <div class="control-group_q1_files input-group"
                                                                     style="margin-top:10px">
                                                                    <input type="file" name="q1_files[]"
                                                                           class="form-control" id="q1_files">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-danger q1_files_delete"
                                                                                type="button"><i
                                                                                    class="glyphicon glyphicon-remove"></i>
                                                                            Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="col-md-12 ">
                                                        <div class="col-md-6 trash_list" id="file_append_question_1">

                                                            @if(!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)
                                                                @foreach($postAuditQuestionsOne->postAuditQuestionFile as $key=>$file)
                                                                    <p class="upload_question_files1_{{$key+1}}">
                                                                        <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                           download
                                                                           class="linkClr"><span>{{$file->file_name}}</span></a>
                                                                        <a href="javascript:void(0)" class="trash_icon"
                                                                           onclick="deleteQuestionOneFile('{{$key+1}}','{{$file->id}}')"><i
                                                                                    class="fa fa-trash"></i></a>
                                                                        {{--                                                                        <input type="hidden" name="q1_files[]"--}}
                                                                        {{--                                                                               id="question_one_files_{{$key+1}}"--}}
                                                                        {{--                                                                               value="{{$file->file_path}}">--}}
                                                                        {{--                                                                        <input type="hidden"--}}
                                                                        {{--                                                                               name="question_ones_filename[]"--}}
                                                                        {{--                                                                               value="{{$file->file_name}}">--}}
                                                                        <input type="hidden"
                                                                               name="question_one_file_id[]"
                                                                               id="question_one_files_id_{{$key+1}}"
                                                                               value="{{$file->id}}">
                                                                    </p>
                                                                @endforeach
                                                            @endif


                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        @else
                                            <div class="col-md-12 question_1" style="display:none">
                                                <div class="row rowBgGrey">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2 floting">
                                                            <label> Upload Verification Pack</label>
                                                        </div>
                                                        <div class="col-md-10 floting">

                                                            <div class="input-group control-group_q1_files increment_q1_files">
                                                                <input type="file" name="q1_files[]"
                                                                       class="form-control" id="q1_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-success q1_files_add"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-plus"></i>Add
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="clone_q1_files hide">
                                                                <div class="control-group_q1_files input-group"
                                                                     style="margin-top:10px">
                                                                    <input type="file" name="q1_files[]"
                                                                           class="form-control" id="q1_files">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-danger q1_files_delete"
                                                                                type="button"><i
                                                                                    class="glyphicon glyphicon-remove"></i>
                                                                            Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="col-md-12 ">
                                                        <div class="col-md-6 trash_list"
                                                             id="file_append_question_1">
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        @endif
                                        <div class="col-md-12 mrgn">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[1]->title_for_oc_om }}</label>
                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q2_name" id="q2_name_yes"
                                                                   value="YES" {{ (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') ? 'checked' : '' }}>
                                                            <label class="q2_name_yes">Yes</label>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q2_name" id="q2_name_no"
                                                                   value="NO" {{ (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'NO') ? 'checked' : '' }}>
                                                            <label class="q2_name_no">No</label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <label class="q2_name_error"></label>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        @if( isset($postAuditQuestionsTwo) &&  $postAuditQuestionsTwo->value == 'YES')
                                            <div class="col-md-12 question_2"
                                                 style="display:{{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                                <div class="row rowBgGrey">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2 floting">
                                                            <label> Upload CAR(s) </label>
                                                        </div>
                                                        <div class="col-md-10 floting">

                                                            <div class="input-group control-group_q2_files increment_q2_files">
                                                                <input type="file" name="q2_files[]"
                                                                       class="form-control" id="q2_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-success q2_files_add"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-plus"></i>Add
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="clone_q2_files hide">
                                                                <div class="control-group_q2_files input-group"
                                                                     style="margin-top:10px">
                                                                    <input type="file" name="q2_files[]"
                                                                           class="form-control" id="q2_files">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-danger q2_files_delete"
                                                                                type="button"><i
                                                                                    class="glyphicon glyphicon-remove"></i>
                                                                            Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{--                                                            <div class="custom-file">--}}
                                                            {{--                                                                <input type="file" class="" name="q2_files[]"--}}
                                                            {{--                                                                       id="q2_files"--}}
                                                            {{--                                                                       value="" multiple/>--}}
                                                            {{--                                                            </div>--}}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="col-md-12 ">
                                                        <div class="col-md-6 trash_list" id="file_append_question_2">

                                                            @if(!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)
                                                                @foreach($postAuditQuestionsTwo->postAuditQuestionFile as $key=>$file)
                                                                    <p class="upload_question_files2_{{$key+1}}">
                                                                        <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                           class="linkClr"><span>{{$file->file_name}}</span></a>
                                                                        <a href="javascript:void(0)" class="trash_icon"
                                                                           onclick="deleteQuestionTwoFile('{{$key+1}}','{{$file->id}}')"><i
                                                                                    class="fa fa-trash"></i></a>
                                                                        {{--                                                                        <input type="hidden" name="question_two_file[]"--}}
                                                                        {{--                                                                               id="question_two_files_{{$key+1}}"--}}
                                                                        {{--                                                                               value="{{$file->file_path}}">--}}
                                                                        {{--                                                                        <input type="hidden"--}}
                                                                        {{--                                                                               name="question_two_filename[]"--}}
                                                                        {{--                                                                               value="{{$file->file_name}}">--}}
                                                                        <input type="hidden"
                                                                               name="question_two_file_id[]"
                                                                               id="question_two_files_id_{{$key+1}}"
                                                                               value="{{$file->id}}">
                                                                    </p>
                                                                @endforeach
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                                <label class="question_two_filename_0_error"></label>
                                            </div>
                                        @else
                                            <div class="col-md-12 question_2" style="display:none">
                                                <div class="row rowBgGrey">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2 floting">
                                                            <label> Upload CAR(s) </label>
                                                        </div>
                                                        <div class="col-md-10 floting">

                                                            <div class="input-group control-group_q2_files increment_q2_files">
                                                                <input type="file" name="q2_files[]"
                                                                       class="form-control" id="q2_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-success q2_files_add"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-plus"></i>Add
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="clone_q2_files hide">
                                                                <div class="control-group_q2_files input-group"
                                                                     style="margin-top:10px">
                                                                    <input type="file" name="q2_files[]"
                                                                           class="form-control" id="q2_files">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-danger q2_files_delete"
                                                                                type="button"><i
                                                                                    class="glyphicon glyphicon-remove"></i>
                                                                            Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {{--                                                            <div class="custom-file">--}}
                                                            {{--                                                                <input type="file" class="" name="q2_files[]"--}}
                                                            {{--                                                                       id="q2_files"--}}
                                                            {{--                                                                       value="" multiple/>--}}
                                                            {{--                                                            </div>--}}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="col-md-12 ">
                                                        <div class="col-md-6 trash_list" id="file_append_question_2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <label class="question_two_filename_0_error"></label>
                                            </div>
                                        @endif




                                        @if(isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES')
                                            <div class="col-md-12 mrgn floting row question_2"
                                                 style="display:{{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                                <div class="col-md-12">
                                                    {{--                                                    <div class="col-md-2">--}}
                                                    {{--                                                        <input type="checkbox" class="form-control" value="1"--}}
                                                    {{--                                                               name="evidence"--}}
                                                    {{--                                                               id="evidence" {{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'checked' : ''}}>--}}
                                                    {{--                                                    </div>--}}
                                                    {{--                                                    <div class="col-md-10">--}}
                                                    {{--                                                        <label>Confirm if closed &amp;Evidences are uploaded below--}}
                                                    {{--                                                        </label>--}}
                                                    {{--                                                       --}}
                                                    {{--                                                    </div>--}}


                                                    <label style="display: inline-flex;width: 100%;">
                                                        <span>Confirm CAR(s) are closed
                                                        & Evidences Uploaded</span>
                                                        <input type="checkbox" class="form-control" value="1"
                                                               style="width: 5%; height: 20px;margin-top: 3px;"
                                                               {{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'checked' : ''}}
                                                               name="evidence" id="evidence">
                                                    </label>
                                                    <label class="evidence_error"></label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        @else
                                            <div class="col-md-12 mrgn floting row question_2" style="display:none">
                                                <div class="col-md-12">
                                                    <label style="display: inline-flex;width: 100%;">
                                                        <span>Confirm CAR(s) are closed
                                                        & Evidences Uploaded</span>
                                                        <input type="checkbox" class="form-control" value="1"
                                                               style="width: 5%; height: 20px;margin-top: 3px;"
                                                               name="evidence" id="evidence">
                                                    </label>
                                                    <label class="evidence_error"></label>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            <div class="row rowBgGrey mrgn">
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <h5 class="blu_clr">{{ $questions[2]->title_for_oc_om }}</h5>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">

                                            <div class="col-md-4 floting no_padding">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q3_name" id="q3_name_no"
                                                           value="YES" {{ (isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'YES') ? 'checked' : '' }}>
                                                    <label class="q3_name_no">Yes</label>

                                                </div>

                                            </div>
                                            <div class="col-md-4 floting">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q3_name" id="q3_name_no" value="NO"
                                                            {{ (isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'NO') ? 'checked' : '' }}>
                                                    <label class="q3_name_no">No</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <label class="q3_name_error"></label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12 question_3"
                                     style="display:{{((isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'YES' && !empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)) ? 'block' : 'none'}};">
                                    <div class="col-md-2 floting">
                                        <label> Attach Files </label>
                                    </div>
                                    <div class="col-md-10 floting">

                                        <div class="input-group control-group_q3_files increment_q3_files">
                                            <input type="file" name="q3_files[]"
                                                   class="form-control" id="q3_files">
                                            <div class="input-group-btn">
                                                <button class="btn btn-success q3_files_add"
                                                        type="button"><i
                                                            class="glyphicon glyphicon-plus"></i>Add
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clone_q3_files hide">
                                            <div class="control-group_q3_files input-group"
                                                 style="margin-top:10px">
                                                <input type="file" name="q3_files[]"
                                                       class="form-control" id="q3_files">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger q3_files_delete"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-remove"></i>
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        {{--                                        <div class="custom-file">--}}
                                        {{--                                            <input type="file" class="" name="q3_files[]" id="q3_files"--}}
                                        {{--                                                   value="" multiple/>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 mrgn question_3"
                                     style="display:{{((!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)) ? 'block' : 'none'}};">
                                    <div class="col-md-12 ">
                                        <div class="col-md-6 trash_list" id="file_append_question_3">

                                            @if(!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)
                                                @foreach($postAuditQuestionsThree->postAuditQuestionFile as $key=>$file)
                                                    <p class="upload_question_files3_{{$key+1}}">
                                                        <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                           class="linkClr"><span>{{$file->file_name}}</span></a>
                                                        <a href="javascript:void(0)" class="trash_icon"
                                                           onclick="deleteQuestionThreeFile('{{$key+1}}','{{$file->id}}')"><i
                                                                    class="fa fa-trash"></i></a>
                                                        {{--                                                        <input type="hidden" name="question_three_file[]"--}}
                                                        {{--                                                               id="question_three_files_{{$key+1}}"--}}
                                                        {{--                                                               value="{{$file->file_path}}">--}}
                                                        {{--                                                        <input type="hidden" name="question_three_filename[]"--}}
                                                        {{--                                                               value="{{$file->file_name}}">--}}
                                                        <input type="hidden" name="question_three_file_id[]"
                                                               id="question_three_files_id_{{$key+1}}"
                                                               value="{{$file->id}}">
                                                    </p>
                                                @endforeach

                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <label class="question_three_filename_0_error"></label>
                            </div>
                            <div class="row rowBgGrey mrgn @if($postAudit->status == 'approved') disabledDiv @endif">
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <h5 class="blu_clr">Communication </h5>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">

                                            <div class="col-md-4 floting no_padding">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q4_name" id="q4_name_no"
                                                           value="YES" {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'checked' : ''}}>
                                                    <label class="q4_name_no">Yes</label>

                                                </div>

                                            </div>
                                            <div class="col-md-4 floting">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q4_name" id="q4_name_no" value="NO"
                                                            {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'NO') ? 'checked' : ''}}>
                                                    <label class="q4_name_no">No</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <label class="q4_name_error"></label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 question_4"
                                     style="display: {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'block' : 'none'}}">
                                    <div class="col-md-2 floting">
                                        <label> Attach Files </label>
                                    </div>
                                    <div class="col-md-10 floting">
                                        <div class="input-group control-group_q4_files increment_q4_files">
                                            <input type="file" name="q4_files[]"
                                                   class="form-control" id="q4_files">
                                            <div class="input-group-btn">
                                                <button class="btn btn-success q4_files_add"
                                                        type="button"><i
                                                            class="glyphicon glyphicon-plus"></i>Add
                                                </button>
                                            </div>
                                        </div>
                                        <p id="q4_file_added" style="color:lightgreen"></p>
                                        <div class="clone_q4_files hide">
                                            <div class="control-group_q4_files input-group"
                                                 style="margin-top:10px">
                                                <input type="file" name="q4_files[]"
                                                       class="form-control" id="q4_files">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger q4_files_delete"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-remove"></i>
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 mrgn question_4"
                                     style="display:{{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'block' : 'none'}};">
                                    <div class="col-md-12 ">
                                        <div class="col-md-4 trash_list" id="file_append_question_4">
                                            @if(!is_null($postAudit->q4_file))
                                                <p class="upload_question_files4_1">
                                                    <a href="{{ asset('/uploads/post-audit/communication/'.$postAudit->id.'/'.$postAudit->q4_file) }}"

                                                       download class="linkClr"><span>Click to download</span></a>
                                                    <a href="javascript:void(0)" class="trash_icon"
                                                       onclick="deleteQuestionFourFile({{$postAudit->id}})"><i
                                                                class="fa fa-trash"></i></a>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <label class="question_four_filename_0_error"></label>
                            </div>
                            <div class="row mrgn">
                                <div class="col-md-12">
                                    <div class="col-md-8 floting">
                                        <div class="form-group">
                                            <label for="remarks">Task Comments*</label>
                                            <input type="text" id="remarks" name="remarks" class="form-control">
                                        </div>
                                        <label class="remarks_error"></label>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="receiver_id">Receiver</label>
                                            <select name="receiver_id" id="receiver_id" class="form-control">

                                                @if($postAudit->status == 'rejected')
                                                    <option value="{{$postAudit->sentToUser->id}}"
                                                            selected>{{ucfirst($postAudit->sentToUser->fullName())}}</option>

                                                @else
                                                    @if(auth()->user()->user_type == 'scheme_manager')
                                                    @else
                                                        <option value="{{$standard->scheme_manager[0]->scheme_coordinators[0]->user_id}}" {{ ($postAudit->receiver_id ==$standard->scheme_manager[0]->scheme_coordinators[0]->user_id) ? 'selected' :'' }}>{{ucfirst($standard->scheme_manager[0]->scheme_coordinators[0]->full_name)}}</option>
                                                    @endif
                                                    <option value="{{$standard->scheme_manager[0]->user_id}}" {{ ($postAudit->receiver_id ==$standard->scheme_manager[0]->user_id) ? 'selected' :'' }}>{{ucfirst($standard->scheme_manager[0]->full_name)}}</option>
                                                @endif
                                            </select>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            @if($isComments)
                                <div class="row">
                                    <div class="col-md-12 floting">
                                        <div class="form-group">
                                            <label for="comments">Pack review comments</label>
                                            <textarea rows="3" class="form-control" name="comments"
                                                      @if(auth()->user()->user_type !== 'scheme_manager' && auth()->user()->user_type !== 'scheme_coordinator') readonly
                                                      @endif
                                                      id="comments">{{ $postAudit->comments }}</textarea>

                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">

                                <div class="col-md-4"></div>
                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator' ||  auth()->user()->user_type == 'operation_coordinator' ||  auth()->user()->user_type == 'operation_manager')
                                    @if(!empty($notifications) && count($notifications) > 0)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Comments Box</label>
                                                <div class="card-body chat_box" style="display: block;">
                                                    <div class="direct-chat-messages">


                                                        @foreach($notifications as $key=>$notification)

                                                            @if($key ==0)
                                                                @if($notification->sent_by === Auth::user()->id )
                                                                    <div class="direct-chat-msg">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                    </div>

                                                                @else
                                                                    <div class="direct-chat-msg text-right">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                    </div>
                                                                @endif
                                                            @elseif($key > 0 && $notifications[$key-1]->status != $notifications[$key]->status )
                                                                @if($notification->sent_by === Auth::user()->id )
                                                                    <div class="direct-chat-msg">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                    </div>

                                                                @else
                                                                    <div class="direct-chat-msg text-right">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                <div class="col-md-4"></div>


                            </div>
                            <div class="col-md-12 mrgn no_padding">
                                <div class="col-md-4 floting"></div>
                                <div class="col-md-4 floting"></div>


                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'admin')
                                    @if($postAudit->status == 'approved')
                                    @else
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-success btn_save"
                                                        data-status="save">Save Only
                                                </button>
                                            </div>
                                        </div>
                                        @if(auth()->user()->user_type == 'scheme_manager' )
                                            <div class="col-md-2 floting no_padding saveSendButton">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn_search"
                                                            data-status="send">
                                                        Save &amp; Send
                                                    </button>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @else
                                    @php
                                        $companyStandard  = \App\CompanyStandards::where([
                                            'company_id'=>$postAudit->company_id,
                                            'standard_id'=>$postAudit->standard_id,
                                        ])->orderBy('id','desc')->first(['id']);
                                        $certificate = \App\Certificate::where('company_standard_id',$companyStandard->id)->where('report_status','new')->orderBy('id','desc')->first(['certificate_status']);
                                    @endphp
                                    @if(!is_null($certificate) && $certificate->certificate_status === 'withdrawn')
                                    @else
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-success btn_save"
                                                        data-status="save">Save Only
                                                </button>
                                            </div>
                                        </div>
                                        @if($postAudit->status == 'unapproved')
                                            @if($postAuditQuestionsThree->value =='YES')
                                                <div class="col-md-2 floting no_padding saveSendButton">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-block btn_search"
                                                                data-status="send">
                                                            Save &amp; Send
                                                        </button>
                                                    </div>
                                                </div>
                                            @else
                                            @endif
                                        @elseif($postAudit->status == 'rejected')
                                            @if($postAuditQuestionsThree->value =='YES')
                                                <div class="col-md-2 floting no_padding saveSendButton">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-block btn_search"
                                                                data-status="resent">
                                                            Save &amp; Resent
                                                        </button>
                                                    </div>
                                                </div>

                                            @else
                                            @endif
                                        @elseif($postAudit->status == 'save')
                                            <div class="col-md-2 floting no_padding saveSendButton"
                                                 style="display: {{(isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'YES') ? 'block' : 'none'}}">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn_search"
                                                            data-status="send">
                                                        Save &amp; Send
                                                    </button>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @endif

                            </div>
                        </div>
                        <!-- /.card-body -->


                    </form>

                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>

@endsection
@push('models')
@endpush
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        var approvalDate = '{{$standardStage->approval_date}}';
        userRole = @json(auth()->user()->user_type); // Assuming 'role' is a property of the user
        cutOfDays = parseInt('{{$cut_of_days}}'); // $cut_of_days
        //Date range picker
        $('.actual_audit_date').daterangepicker({
            format: "DD-MM-YYYY",
            minDate: (userRole === 'operation_manager' || userRole === 'operation_coordinator') ? moment().subtract(cutOfDays, 'days') : false, // Set minDate to current date - Cut of days for OM and OC roles
            maxDate: (userRole === 'operation_manager' || userRole === 'operation_coordinator') ? moment() : false, // Apply maxDate only for OM and OC roles
        }).on('apply.daterangepicker', function (ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            var dateAr = this.value.split('-');
            var newDate = startDate.format('YYYY-MM-DD');
            var newApprovalDate = moment(approvalDate).format('YYYY-MM-DD');
            var newActualAuditDate = moment(newDate).format('YYYY-MM-DD');

            var duration = moment.duration(endDate.diff(startDate));
            var days = duration.asDays();


            if (days > 20) {
                toastr['error']('Date From and To difference should not be > 20 days');
                $('#actual_date').val('');
            } else {
                if (newActualAuditDate > newApprovalDate) {

                } else if (newActualAuditDate < newApprovalDate) {
                    toastr['error']('Actual Audit Date always greater than AJ Approval Date');
                    $('#actual_date').val('');
                } else {
                    toastr['error']('Actual Audit Date always greater than AJ Approval Date');
                    $('#actual_date').val('');
                }
            }
        });


    </script>

    <script type="text/javascript">

        var questionOneCount = {{(isset($postAuditQuestionsOne)) ? count($postAuditQuestionsOne->postAuditQuestionFile) : 0}};
        var questionTwoCount = {{(isset($postAuditQuestionsTwo)) ?count($postAuditQuestionsTwo->postAuditQuestionFile) : 0}};
        var questionThreeCount = {{(isset($postAuditQuestionsThree)) ? count($postAuditQuestionsThree->postAuditQuestionFile) :0}};
        var verificationCount = {{$approvedVerificationCount}};
        var questionFourCount = {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES' && !is_null($postAudit->q4_file)) ? 1 : 0}};
        var audit_type = "{{$audit_type}}";
        $("input[name='q1_name']").click(function () {
            var radioValue = $("input[name='q1_name']:checked").val();


            if (radioValue) {
                if (radioValue == 'YES') {
                    if (verificationCount > 0) {
                        $('.question_1').show();
                    } else {
                        alert('Note: Category I CAR(s) was raised - “Verification pack” cannot be uploaded, No AJ is approved.');
                    }
                } else {
                    $('.question_ones_filename_0_error').text('');

                    $('.question_1').hide();
                }

            }


            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();

            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }

        });


        function deleteQuestionOneFile(id, fileId) {
            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": fileId, 'type': 'ims'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {
                            $('.upload_question_files1_' + id).remove();
                            questionOneCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }


        }


        $("input[name='q2_name']").click(function () {
            var radioValue = $("input[name='q2_name']:checked").val();
            if (radioValue == 'YES') {

                $('.question_2').show();
            } else {
                $('.question_two_filename_0_error').text('');
                $('.question_2').hide();
            }


            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();

            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }


        });


        function deleteQuestionTwoFile(id, fileId) {
            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": fileId, 'type': 'ims'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {

                            $('.upload_question_files2_' + id).remove();
                            questionTwoCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }


        }


        $("input[name='q3_name']").click(function () {
            var radioValue = $("input[name='q3_name']:checked").val();
            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();
            if (radioValue == 'YES') {
                $('.question_3').show();
            } else {
                $('.question_three_filename_0_error').text('');
                $('.question_3').hide();
            }


            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }
        });


        function deleteQuestionThreeFile(id, fileId) {

            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": fileId, 'type': 'ims'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {

                            $('.upload_question_files3_' + id).remove();
                            questionThreeCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }


        function getBase64(file, index, id) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                $('#' + id + '' + index).val(reader.result);
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }

        $("input[name='q4_name']").click(function () {
            var radioValue = $("input[name='q4_name']:checked").val();
            if (radioValue === 'YES') {
                $('.question_4').show();
            } else {
                $('.question_four_filename_0_error').text('');
                $('.question_4').hide();
            }
        });

        function deleteQuestionFourFile(id) {

            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": id, 'type': 'postAudit'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {

                            $('.upload_question_files4_1').remove();
                            questionFourCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }

        function validate(e) {
            $('#loader').show();
            $(':input[type="submit"]').prop('disabled', true);
            if (e.submitter.dataset.status == 'save') {
                $('#status').val('save');
            } else if (e.submitter.dataset.status == 'send') {
                $('#status').val('unapproved');
            } else if (e.submitter.dataset.status == 'resent') {
                $('#status').val('resent');

            }

            if (e.submitter.dataset.status == 'send' && audit_type !== 'Stage 1') {
                var regionName = '{{$company->region->title }}';
                var days = 0;
                if (regionName === 'Pakistan Region') {
                    days = parseInt('{{$pk_days}}');
                } else if (regionName === 'Saudi Arabia Region') {
                    days = parseInt('{{$ksa_days}}');
                }


                var actual_audit_date = $('#actual_audit_date').val();
                if (actual_audit_date !== '' && actual_audit_date !== null && actual_audit_date !== undefined) {
                    // Split the string using the ' - ' separator
                    var dates = actual_audit_date.split(" - ");
                    var endDate = dates[1];

                    // Parse the end date into a JavaScript Date object
                    var parts = endDate.split("-");
                    endDate = new Date(parts[2], parts[1] - 1, parts[0]); // year, month (0-based), day

                    // Get the current date
                    var currentDate = new Date();

                    // Calculate the difference in time (in milliseconds)
                    var timeDifference = currentDate.getTime() - endDate.getTime();

                    // Convert time difference from milliseconds to days
                    var daysDifference = Math.ceil(timeDifference / (1000 * 3600 * 24)); // 1000ms * 3600s * 24hrs

                    if (daysDifference < days) {
                        toastr['error']('NOTE:: Save & Pack Send not allowed. Please clear your oldest packs first........'); //toaster message issue
                        $('#loader').hide();
                        $(':input[type="submit"]').prop('disabled', false);
                        return false;
                    }

                }
            }

            clearErrorMessages();
            var response = false;
            var response1 = false;
            var response2 = false;
            var response3 = false;
            var response4 = false;
            var response5 = false;

            if (questionOneCount == 0) {
                var radioValue = $("input[name='q1_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_ones_filename_0_error').text('Please Upload Verfication Pack');
                    response = false;
                } else {
                    $('.question_ones_filename_0_error').text('');
                    response = true;
                }

            } else {
                response = true;
            }
            if (questionTwoCount == 0) {
                var radioValue = $("input[name='q2_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_two_filename_0_error').text('Please Upload CAR(s)');
                    response1 = false;

                } else {
                    $('.question_two_filename_0_error').text('');
                    response1 = true;
                }
            } else {

                if ($('#evidence').is(':checked') == true) {
                    $('.evidence_error').text('');
                    response1 = true;
                } else {
                    $('.evidence_error').text('Evidence checked is required.');
                    response1 = false;

                }
            }
            if (questionThreeCount == 0) {
                var radioValue = $("input[name='q3_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_three_filename_0_error').text('Please Upload Pack');
                    response2 = false;
                } else {
                    $('.question_three_filename_0_error').text('');
                    response2 = true;
                }
            } else {
                response2 = true;
            }
            if ($('#actual_audit_date').val() == '') {
                response3 = false;
                $('.actual_audit_date_error').text('Actual Audit Date is required');
            } else {
                response3 = true;
                $('.actual_audit_date_error').text('');
            }

            if (e.submitter.dataset.status != 'save') {
                if ($('#remarks').val() == '') {
                    response4 = false;
                    $('.remarks_error').text('Comments missing');
                } else {
                    response4 = true;
                    $('.remarks_error').text('');
                }

            } else {
                response4 = true;
                $('.remarks_error').text('');
            }

            if (questionFourCount === 0) {
                var radioValue = $("input[name='q4_name']:checked").val();
                if (radioValue === 'YES' && e.submitter.dataset.status === 'send') {
                    $('.question_four_filename_0_error').text('File is required');
                    response5 = false;
                } else {
                    $('.question_four_filename_0_error').text('');
                    response5 = true;
                }
            } else {
                response5 = true;
            }


            if (response == false || response1 == false || response2 == false || response3 == false || response4 == false || response5 === 0) {
                toastr['error']('Form has some errors'); //toaster message issue
                $(':input[type="submit"]').prop('disabled', false);
                $('#loader').hide();
                return false;
            } else {

                return true;
            }
        }

        function clearErrorMessages() {
            $('.actual_audit_date_error').text('');
            $('.q1_name_error').text('');
            $('.q2_name_error').text('');
            $('.q3_name_error').text('');
            $('.question_ones_filename_0_error').text('');
            $('.question_two_filename_0_error').text('');
            $('.question_three_filename_0_error').text('');
            $('.question_four_filename_0_error').text('');

            $('.remarks_error').text('');
            $('.evidence_error').text('');

        }

    </script>
    <script>
        $(document).ready(function () {
            //Question 1 Files

            $(".q1_files_add").click(function () {
                var html = $(".clone_q1_files").html();
                $(".increment_q1_files").after(html);
                questionOneCount++;


            });
            $("body").on("click", ".q1_files_delete", function () {
                $(this).parents(".control-group_q1_files").remove();
                questionOneCount--;
            });


            //Question 2 Files

            $(".q2_files_add").click(function () {
                var html = $(".clone_q2_files").html();
                $(".increment_q2_files").after(html);
                questionTwoCount++;


            });
            $("body").on("click", ".q2_files_delete", function () {
                $(this).parents(".control-group_q2_files").remove();
                questionTwoCount--;
            });


            //Question 3 Files

            $(".q3_files_add").click(function () {
                var html = $(".clone_q3_files").html();
                $(".increment_q3_files").after(html);
                questionThreeCount++;


            });
            $("body").on("click", ".q3_files_delete", function () {
                $(this).parents(".control-group_q3_files").remove();
                questionThreeCount--;
            });

            $(".q4_files_add").click(function () {
                questionFourCount++;
                $('#q4_file_added').text('File added successfully.')
            });
            $("body").on("click", ".q4_files_delete", function () {
                questionFourCount--;
            });
        });

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
    </script>

@endpush
