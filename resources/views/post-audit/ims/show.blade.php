@extends('layouts.master')
@section('title', "View Post Audit {$audit_type}")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 620.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">AUDIT <span>ACTIVITY</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Audit Activity</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <input type="hidden" name="status" id="status" value="">
                    <input type="hidden" name="company_id" value="{{$company->id}}">
                    <input type="hidden" name="standard_id" value="{{$standard->id}}">
                    <input type="hidden" name="aj_standard_stage_id" value="{{$standardStage->id}}">
                    <input type="hidden" name="audit_type" value="{{$audit_type}}">
                    <input type="hidden" name="questions_one_id" value="{{ $questions[0]->id }}">
                    <input type="hidden" name="questions_two_id" value="{{ $questions[1]->id }}">
                    <input type="hidden" name="questions_three_id" value="{{ $questions[2]->id }}">
                    <input type="hidden" name="post_audit_id" value="{{ $postAudit->id }}">
                    <input type="hidden" name="ims_heading" value="{{ $ims_heading }}">
                    @if(!is_null($postAuditQuestionsOne))
                        <input type="hidden" name="post_audit_question_one_id"
                               value="{{ $postAuditQuestionsOne->id }}">
                    @endif

                    @if(!is_null($postAuditQuestionsTwo))
                        <input type="hidden" name="post_audit_question_two_id"
                               value="{{ $postAuditQuestionsTwo->id }}">
                    @endif

                    @if(!is_null($postAuditQuestionsThree))
                        <input type="hidden" name="post_audit_question_three_id"
                               value="{{ $postAuditQuestionsThree->id }}">
                    @endif


                    <div class="card-body card_cutom auditActivity auditJustificationTable filesUploadColor ">
                        <div class="row">
                            <h6 class="blu_clr">{{ ucfirst($company->name) }} &nbsp; - &nbsp; {{ $audit_type }}
                                Audit
                                &nbsp; - &nbsp; {{ $ims_heading }} Standard(s)</h6>
                        </div>
                        <div class="row rowBgGrey mrgn">
                            <div class="col-md-12">

                                <div class="col-md-2 floting">
                                    <label for="actual_audit_date">Actual Audit Date :</label>
                                </div>
                                <div class="col-md-4 floting">

                                    <div class="form-group">
                                        <div class="input-group mb-0">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>

                                            <input type="text" name="actual_audit_date" id="actual_audit_date"
                                                   value="{{ date("d-m-Y", strtotime($postAudit->actual_audit_date)) }} - {{ date("d-m-Y", strtotime($postAudit->actual_audit_date_to)) }}"
                                                   class="form-control float-right active actual_audit_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <label class="actual_audit_date_error"></label>
                            </div>
                        </div>
                        @if($audit_type != 'Stage 1')
                            <div class="row rowBgGrey mrgn">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <h5 class="blu_clr">Corrective Action Request Confirmation:</h5>
                                    </div>
                                    <div class="col-md-12 disabledDiv">
                                        <div class="col-md-3 floting">
                                            <label>{{ $questions[0]->title_for_oc_om }}</label>

                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">

                                                <div class="col-md-4 floting no_padding">
                                                    <div style="position: relative;">
                                                        <input type="radio" name="q1_name" id="q1_name_yes"
                                                               value="YES" {{ (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES') ? 'checked' : '' }}>
                                                        <label class="q1_name_yes">Yes</label>

                                                    </div>

                                                </div>
                                                <div class="col-md-4 floting">
                                                    <div style="position: relative;">
                                                        <input type="radio" name="q1_name" id="q1_name_no"
                                                               value="NO" {{ (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'NO') ? 'checked' : '' }}>
                                                        <label class="q1_name_no">No</label>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <label class="q1_name_error"></label>
                                        <label class="question_ones_filename_0_error"></label>

                                        <div class="clearfix"></div>
                                    </div>


                                    @if(isset($postAuditQuestionsOne) &&  $postAuditQuestionsOne->value == 'YES')
                                        <div class="col-md-12 question_1"
                                             style="display:{{((!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                            <div class="row rowBgGrey">

                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_1">

                                                        @if(!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)
                                                            @foreach($postAuditQuestionsOne->postAuditQuestionFile as $key=>$file)
                                                                <p class="upload_question_files1_'{{$key+1}}'">
                                                                    <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                       download
                                                                       class="linkClr"><span>{{$file->file_name}}</span></a>

                                                                    <input type="hidden" name="question_ones_file[]"
                                                                           id="question_one_files_'{{$key+1}}'"
                                                                           value="{{$file->file_path}}">
                                                                    <input type="hidden"
                                                                           name="question_ones_filename[]"
                                                                           value="{{$file->file_name}}">
                                                                </p>
                                                            @endforeach
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    @endif
                                    <div class="col-md-12 mrgn disabledDiv">
                                        <div class="col-md-3 floting">
                                            <label>{{ $questions[1]->title_for_oc_om }}</label>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">

                                                <div class="col-md-4 floting no_padding">
                                                    <div style="position: relative;">
                                                        <input type="radio" name="q2_name" id="q2_name_yes"
                                                               value="YES" {{ (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') ? 'checked' : '' }}>
                                                        <label class="q2_name_yes">Yes</label>

                                                    </div>

                                                </div>
                                                <div class="col-md-4 floting">
                                                    <div style="position: relative;">
                                                        <input type="radio" name="q2_name" id="q2_name_no"
                                                               value="NO" {{ (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'NO') ? 'checked' : '' }}>
                                                        <label class="q2_name_no">No</label>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <label class="q2_name_error"></label>
                                        </div>
                                        @if(isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES')
                                            <div class="col-md-12 mrgn floting row question_2"
                                                 style="display:{{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <input type="checkbox" class="form-control" value="1"
                                                               name="evidence"
                                                               id="evidence" {{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'checked' : ''}}>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <label>Confirm if closed &amp;Evidences are uploaded below
                                                        </label>
                                                        <label class="evidence_error"></label>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    @if( isset($postAuditQuestionsTwo) &&  $postAuditQuestionsTwo->value == 'YES')
                                        <div class="col-md-12 question_2"
                                             style="display:{{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                            <div class="row rowBgGrey">


                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_2">

                                                        @if(!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)
                                                            @foreach($postAuditQuestionsTwo->postAuditQuestionFile as $key=>$file)
                                                                <p class="upload_question_files2_'{{$key+1}}'">
                                                                    <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                       download
                                                                       class="linkClr"><span>{{$file->file_name}}</span></a>

                                                                    <input type="hidden" name="question_two_file[]"
                                                                           id="question_two_files_'{{$key+1}}'"
                                                                           value="{{$file->file_path}}">
                                                                    <input type="hidden"
                                                                           name="question_two_filename[]"
                                                                           value="{{$file->file_name}}">
                                                                </p>
                                                            @endforeach
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <label class="question_two_filename_0_error"></label>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        @endif
                        <div class="row rowBgGrey mrgn">
                            <div class="col-md-12 disabledDiv">
                                <div class="col-md-3 floting">
                                    <h5 class="blu_clr">{{ $questions[2]->title_for_oc_om }}</h5>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">

                                        <div class="col-md-4 floting no_padding">
                                            <div style="position: relative;">
                                                <input type="radio" name="q3_name" id="q3_name_no"
                                                       value="YES" {{ (isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'YES') ? 'checked' : '' }}>
                                                <label class="q3_name_no">Yes</label>

                                            </div>

                                        </div>
                                        <div class="col-md-4 floting">
                                            <div style="position: relative;">
                                                <input type="radio" name="q3_name" id="q3_name_no" value="NO"
                                                        {{ (isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'NO') ? 'checked' : '' }}>
                                                <label class="q3_name_no">No</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <label class="q3_name_error"></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-12 mrgn question_3"
                                 style="display:{{((!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)) ? 'block' : 'none'}};">
                                <div class="col-md-12 ">
                                    <div class="col-md-6 trash_list" id="file_append_question_3">

                                        @if(!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)
                                            @foreach($postAuditQuestionsThree->postAuditQuestionFile as $key=>$file)
                                                <p class="upload_question_files3_'{{$key+1}}'">
                                                    <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                       download
                                                       class="linkClr"><span>{{$file->file_name}}</span></a>
                                                    <input type="hidden" name="question_three_file[]"
                                                           id="question_three_files_'{{$key+1}}'"
                                                           value="{{$file->file_path}}">
                                                    <input type="hidden" name="question_three_filename[]"
                                                           value="{{$file->file_name}}">
                                                </p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mrgn question_3"
                                 style="display:{{((!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)) ? 'block' : 'none'}};">
                                <div class="col-md-12 ">
                                    <div class="col-md-6 trash_list" id="file_append_question_3">
                                        @if($postAudit->status == 'approved')
                                            <h5 class="blu_clr"> Pack Approval Date :
                                                @if(!is_null($postAudit->postAuditSchemeInfo))
                                                    {{ date('d-m-Y',strtotime($postAudit->postAuditSchemeInfo->approval_date)) }}
                                                @endif
                                            </h5>

                                        @endif
                                    </div>
                                </div>
                            </div>
                            <label class="question_three_filename_0_error"></label>
                        </div>
                        <div class="row rowBgGrey mrgn ">
                            <div class="col-md-12 disabledDiv">
                                <div class="col-md-3 floting">
                                    <h5 class="blu_clr">Communication </h5>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">

                                        <div class="col-md-4 floting no_padding">
                                            <div style="position: relative;">
                                                <input type="radio" name="q4_name" id="q4_name_no"
                                                       value="YES" {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'checked' : ''}}>
                                                <label class="q4_name_no">Yes</label>

                                            </div>

                                        </div>
                                        <div class="col-md-4 floting">
                                            <div style="position: relative;">
                                                <input type="radio" name="q4_name" id="q4_name_no" value="NO"
                                                        {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'NO') ? 'checked' : ''}}>
                                                <label class="q4_name_no">No</label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <label class="q4_name_error"></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            @if(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES' && !is_null($postAudit->q4_file))
                                <div class="col-md-12 mrgn question_4"
                                     style="display:{{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'block' : 'none'}};">
                                    <div class="col-md-12 ">
                                        <div class="col-md-4 trash_list" id="file_append_question_4">
                                            <p class="upload_question_files4_1">
                                                <a href="{{ asset('/uploads/post-audit/communication/'.$postAudit->id.'/'.$postAudit->q4_file) }}"
                                                   download class="btn btn-block btn_search linkClr"
                                                ><span>Click to download</span></a>
                                            </p>
                                        </div>
                                    </div>

                                    <label class="question_four_filename_0_error"></label>
                                </div>
                            @endif
                        </div>
                        <div class="row rowBgGrey mrgn">

                            @if(auth()->user()->is_enabled == true)
                                <div class="col-md-12">
                                    <label>Certificate Copy</label>
                                    @php
                                        $certificate_file = \App\CertificateFile::where('post_audit_id',$postAudit->id)->orderBy('id','desc')->first();
                                    @endphp
                                    @if($standardStage->audit_type !='stage_1' && !is_null($certificate_file))


                                        <ul>
                                            @if($certificate_file->file_type === 'file')
                                                <li class="">
                                                    <a href="{{asset('uploads/certificateFiles/'.$certificate_file->file_path)}}"
                                                       data-file="{{asset('uploads/certificateFiles/'.$certificate_file->file_path)}}"
                                                       download
                                                       class="linkClr"><span>{{$certificate_file->file_name}}</span></a>
                                                </li>
                                            @else
                                                <li class="">
                                                    <a href="{{$certificate_file->file_path}}"
                                                       data-file="{{$certificate_file->file_path}}"
                                                       download
                                                       class="linkClr"><span>{{$certificate_file->file_name}}</span></a>
                                                </li>
                                            @endif
                                        </ul>
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                            @endif
                            @if(auth()->user()->user_type == 'scheme_manager')
                                @if($standardStage->audit_type !='stage_1' && !is_null($certificate_file))
                                    <form role="form"
                                          action="{{ route('ims.post.audit.draft.print.update') }}"
                                          method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="row auditJustificationTable mrgn popupLbl dataNextLine">
                                            <div class="col-md-6 floting">
                                                <label>Update Certificate File</label>
                                                <input type="hidden" name="certificate_file_id"
                                                       value="{{$certificate_file->id}}">
                                                <input type="hidden" name="company_id" value="{{$company->id}}">
                                                <input type="hidden" name="standard_id" value="{{$standard->id}}">
                                                <input type="hidden" name="aj_standard_stage_id"
                                                       value="{{$standardStage->id}}">
                                                <input type="hidden" name="audit_type" value="{{$audit_type}}">
                                                <input type="hidden" name="post_audit_id" value="{{ $postAudit->id }}">
                                                <div class="custom-file">
                                                    <input type="file" class="" name="certificate_files"
                                                           id="certificate_files"
                                                           value=""/>
                                                </div>
                                                <div class="col-md-12 trash_list" id="file_append_certificate_1">
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group" style="width: 30%;float: right; padding-right: 10px;">
                                            <button type="submit" class="btn btn-block btn-success btn_save">SAVE
                                            </button>
                                        </div>
                                    </form>
                                @endif
                            @endif


                        </div>
                        @if($isComments)
                            <div class="row">
                                <div class="col-md-12 floting">
                                    <div class="form-group">
                                        <label for="comments">Pack review comments</label>
                                        <textarea rows="3" class="form-control" name="comments"
                                                  @if(auth()->user()->user_type !== 'scheme_manager' && auth()->user()->user_type !== 'scheme_coordinator') readonly @endif
                                                  id="comments">{{ $postAudit->comments }}</textarea>

                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row">

                            <div class="col-md-4"></div>
                            @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator' ||  auth()->user()->user_type == 'operation_coordinator' ||  auth()->user()->user_type == 'operation_manager')
                                @if(!empty($notifications) && count($notifications) > 0)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Comments Box</label>
                                            <div class="card-body chat_box" style="display: block;">
                                                <div class="direct-chat-messages">


                                                    @foreach($notifications as $key=>$notification)

                                                        @if($key ==0)
                                                            @if($notification->sent_by === Auth::user()->id )
                                                                <div class="direct-chat-msg">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                </div>

                                                            @else
                                                                <div class="direct-chat-msg text-right">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                </div>
                                                            @endif
                                                        @elseif($key > 0 && $notifications[$key-1]->status != $notifications[$key]->status )
                                                            @if($notification->sent_by === Auth::user()->id )
                                                                <div class="direct-chat-msg">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                </div>

                                                            @else
                                                                <div class="direct-chat-msg text-right">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    @endforeach


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div class="col-md-4">


                            </div>


                        </div>

                    </div>
                    <!-- /.card-body -->


                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>

@endsection
@push('models')
@endpush
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">


        var approvalDate = '{{$standardStage->approval_date}}';
        $('.actual_audit_date').daterangepicker({
            format: "DD-MM-YYYY",
        }).on('apply.daterangepicker', function (ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
            var dateAr = this.value.split('-');
            // var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
            var newDate = startDate.format('YYYY-MM-DD');
            var newApprovalDate = moment(approvalDate).format('YYYY-MM-DD');
            var newActualAuditDate = moment(newDate).format('YYYY-MM-DD');
            if (newActualAuditDate > newApprovalDate) {
                var date = new Date();
                var momentDate = moment(date).format('YYYY-MM-DD');
                if (newActualAuditDate > momentDate) {
                } else {
                    toastr['error']('Actual Audit Date always greater than Current Date and Approval Date');
                    $('#actual_date').val('');
                }
            } else if (newActualAuditDate < newApprovalDate) {
                toastr['error']('Actual Audit Date always greater than Current Date and Approval Date');
                $('#actual_date').val('');
            } else {
                toastr['error']('Actual Audit Date always greater than Current Date and Approval Date');
                $('#actual_date').val('');
            }
        });
        // $('.actual_audit_date').datepicker({
        //     format: 'dd-mm-yyyy',
        // }).on("changeDate", function () {
        //     var dateAr = this.value.split('-');
        //     var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
        //     var newApprovalDate = moment(approvalDate).format('YYYY-MM-DD');
        //     var newActualAuditDate = moment(newDate).format('YYYY-MM-DD');
        //     if (newActualAuditDate > newApprovalDate) {
        //         var date = new Date();
        //         var momentDate = moment(date).format('YYYY-MM-DD');
        //         if (newActualAuditDate > momentDate) {
        //         } else {
        //             toastr['error']('Actual Audit Date always greater than Current Date and Approval Date');
        //             $('#actual_date').val('');
        //         }
        //     } else if (newActualAuditDate < newApprovalDate) {
        //         toastr['error']('Actual Audit Date always greater than Current Date and Approval Date');
        //         $('#actual_date').val('');
        //     } else {
        //         toastr['error']('Actual Audit Date always greater than Current Date and Approval Date');
        //         $('#actual_date').val('');
        //     }
        //
        //
        // });

    </script>

    <script type="text/javascript">

        var questionOneCount = {{(isset($postAuditQuestionsOne)) ? count($postAuditQuestionsOne->postAuditQuestionFile) : 0}};
        var questionTwoCount = {{(isset($postAuditQuestionsTwo)) ?count($postAuditQuestionsTwo->postAuditQuestionFile) : 0}};
        var questionThreeCount = {{(isset($postAuditQuestionsThree)) ? count($postAuditQuestionsThree->postAuditQuestionFile) :0}};
        var verificationCount = {{$approvedVerificationCount}};

        $("input[name='q1_name']").click(function () {
            var radioValue = $("input[name='q1_name']:checked").val();


            if (radioValue) {
                if (radioValue == 'YES') {
                    if (verificationCount > 0) {
                        $('.question_1').show();
                    } else {
                        alert('Note: Category I CAR(s) was raised - “Verification pack” cannot be uploaded, No AJ is approved.');
                    }
                } else {
                    $('.question_ones_filename_0_error').text('');

                    $('.question_1').hide();
                }

            }


            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();

            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }

        });


        $('#q1_files').change(function () {


            var totalfiles = this.files.length;
            var html = '';
            for (var index = 0; index < totalfiles; index++) {
                var filename = this.files[index].name;

                if (this.files && this.files[index]) {
                    questionOneCount++;
                    html += '<p class="upload_question_files1_' + questionOneCount + '">';
                    html += '<a href="#" class="linkClr"><span> ' + filename + '</span></a>';
                    html += '<a href="#" class="trash_icon" onclick="deleteQuestionOnFile(' + questionOneCount + ')"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" name="question_ones_file[]" id="question_one_files_' + questionOneCount + '" value="">';
                    html += '<input type="hidden" name="question_ones_filename[]" value=" ' + filename + '">';
                    html += '</p>'
                    getBase64(this.files[index], questionOneCount, 'question_one_files_');
                }
            }
            $('#file_append_question_1').append(html);
        });

        function deleteQuestionOnFile(id) {
            $('.upload_question_files1_' + id).remove();
            questionOneCount--;
        }

        $("input[name='q2_name']").click(function () {
            var radioValue = $("input[name='q2_name']:checked").val();
            if (radioValue == 'YES') {

                $('.question_2').show();
            } else {
                $('.question_two_filename_0_error').text('');
                $('.question_2').hide();
            }


            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();

            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }


        });

        $('#q2_files').change(function () {


            var totalfiles = this.files.length;
            var html = '';
            for (var index = 0; index < totalfiles; index++) {
                var filename = this.files[index].name;

                if (this.files && this.files[index]) {
                    questionTwoCount++;
                    html += '<p class="upload_question_files2_' + questionTwoCount + '">';
                    html += '<a href="#" class="linkClr"><span> ' + filename + '</span></a>';
                    html += '<a href="#" class="trash_icon" onclick="deleteQuestionTwoFile(' + questionTwoCount + ')"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" name="question_two_file[]" id="question_two_files_' + questionTwoCount + '" value="">';
                    html += '<input type="hidden" name="question_two_filename[]" value=" ' + filename + '">';
                    html += '</p>'
                    getBase64(this.files[index], questionTwoCount, 'question_two_files_');
                }
            }
            $('#file_append_question_2').append(html);
        });

        function deleteQuestionTwoFile(id) {
            $('.upload_question_files2_' + id).remove();
            questionTwoCount--;
        }


        $("input[name='q3_name']").click(function () {
            var radioValue = $("input[name='q3_name']:checked").val();
            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();
            if (radioValue == 'YES') {
                $('.question_3').show();
            } else {
                $('.question_three_filename_0_error').text('');
                $('.question_3').hide();
            }


            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }
        });


        $('#q3_files').change(function () {

            var totalfiles = this.files.length;
            var html = '';
            for (var index = 0; index < totalfiles; index++) {
                var filename = this.files[index].name;

                if (this.files && this.files[index]) {
                    questionThreeCount++;
                    html += '<p class="upload_question_files3_' + questionThreeCount + '">';
                    html += '<a href="#" class="linkClr"><span> ' + filename + '</span></a>';
                    html += '<a href="#" class="trash_icon" onclick="deleteQuestionThreeFile(' + questionThreeCount + ')"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" name="question_three_file[]" id="question_three_files_' + questionThreeCount + '" value="">';
                    html += '<input type="hidden" name="question_three_filename[]" value=" ' + filename + '">';
                    html += '</p>'
                    getBase64(this.files[index], questionThreeCount, 'question_three_files_');
                }
            }
            $('#file_append_question_3').append(html);
        });

        function deleteQuestionThreeFile(id) {
            $('.upload_question_files3_' + id).remove();
            questionThreeCount--;
        }


        function getBase64(file, index, id) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                $('#' + id + '' + index).val(reader.result);
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }

        function validate(e) {

            if (e.submitter.dataset.status == 'save') {
                $('#status').val('save');
            } else if (e.submitter.dataset.status == 'send') {
                $('#status').val('unapproved');
            } else if (e.submitter.dataset.status == 'resent') {
                $('#status').val('resent');

            }
            clearErrorMessages();
            var response = false;
            var response1 = false;
            var response2 = false;
            var response3 = false;
            var response4 = false;
            var response5 = false;

            if (questionOneCount == 0) {
                var radioValue = $("input[name='q1_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_ones_filename_0_error').text('Please Upload Pack');
                    response = false;
                } else {
                    $('.question_ones_filename_0_error').text('');
                    response = true;
                }

            } else {
                response = true;
            }
            if (questionTwoCount == 0) {
                var radioValue = $("input[name='q2_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_two_filename_0_error').text('Please Upload Pack');
                    response1 = false;

                } else {
                    $('.question_two_filename_0_error').text('');
                    response1 = true;
                }
            } else {

                if ($('#evidence').is(':checked') == true) {
                    $('.evidence_error').text('');
                    response1 = true;
                } else {
                    $('.evidence_error').text('Evidence checked is required.');
                    response1 = false;

                }
            }
            if (questionThreeCount == 0) {
                var radioValue = $("input[name='q3_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_three_filename_0_error').text('Please Upload Pack');
                    response2 = false;
                } else {
                    $('.question_three_filename_0_error').text('');
                    response2 = true;
                }
            } else {
                response2 = true;
            }
            if ($('#actual_audit_date').val() == '') {
                response3 = false;
                $('.actual_audit_date_error').text('Actual Audit Date is required');
            } else {
                response3 = true;
                $('.actual_audit_date_error').text('');
            }

            if (e.submitter.dataset.status != 'save') {
                if ($('#remarks').val() == '') {
                    response4 = false;
                    $('.remarks_error').text('Remarks is required');
                } else {
                    response4 = true;
                    $('.remarks_error').text('');
                }

            } else {
                response4 = true;
                $('.remarks_error').text('');
            }


            if (response == false || response1 == false || response2 == false || response3 == false || response4 == false) {
                toastr['error']('Form has some errors'); //toaster message issue
                return false;
            } else {
                return true;
            }
        }

        function clearErrorMessages() {
            $('.actual_audit_date_error').text('');
            $('.q1_name_error').text('');
            $('.q2_name_error').text('');
            $('.q3_name_error').text('');
            $('.question_ones_filename_0_error').text('');
            $('.question_two_filename_0_error').text('');
            $('.question_three_filename_0_error').text('');
            $('.remarks_error').text('');
            $('.evidence_error').text('');

        }

    </script>

    <script>
        var certificateCount = 0;


        $('#certificate_files').change(function () {


            var totalfiles = this.files.length;
            var html = '';
            for (var index = 0; index < totalfiles; index++) {
                var filename = this.files[index].name;

                if (this.files && this.files[index]) {
                    certificateCount++;
                    html += '<p class="upload_certificate_files1_' + certificateCount + '">';
                    html += '<a href="#" class="linkClr"><span> ' + filename + '</span></a>';
                    html += '<a href="#" class="trash_icon" onclick="deleteCertificateOnFile(' + certificateCount + ')"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" name="certificate_file[]" id="certificate_files_' + certificateCount + '" value="">';
                    html += '<input type="hidden" name="certificate_filename[]" value=" ' + filename + '">';
                    html += '</p>'
                    getBase64(this.files[index], certificateCount, 'certificate_files_');
                }
            }
            $('#file_append_certificate_1').append(html);
        });

        function deleteCertificateOnFile(id) {
            $('.upload_certificate_files1_' + id).remove();
            certificateCount--;
        }

        function getBase64(file, index, id) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                $('#' + id + '' + index).val(reader.result);
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }
    </script>

@endpush
