@extends('layouts.master')
@section('title', "Draft for printing {$audit_type}")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
    <style>
        .hidden {
            display: none;
        }
    </style>
@endpush
@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 620.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">AUDIT <span>ACTIVITY</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Audit Activity</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" onsubmit="return validate(event)"
                          action="{{ route('ims.post.audit.draft.print.store') }}"
                          method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="status" id="status" value="">
                        <input type="hidden" name="company_id" value="{{$company->id}}">
                        <input type="hidden" name="standard_id" value="{{$standard->id}}">
                        <input type="hidden" name="aj_standard_stage_id" value="{{$standardStage->id}}">
                        <input type="hidden" name="audit_type" id="audit_type" value="{{$audit_type}}">
                        <input type="hidden" name="post_audit_id" value="{{ $postAudit->id }}">
                        <input type="hidden" name="ims_heading" value="{{ $ims_heading }}">

                        <div class="modal-body">
                            <div class="row auditJustificationTable mrgn popupLbl">
                                <div class="col-md-3">
                                    <label><strong>{{ ucfirst($company->name) }}</strong></label>
                                </div>
                                <div class="col-md-6">
                                    <label><strong>Standards ({{ $ims_heading }})</strong></label>
                                </div>
                                <div class="col-md-3 floting">
                                    <a href="{{route('ims.justification.show', [$company->id, $standardStage->getAuditType(), $standard->name,$standardStage->id]) }}"
                                       class="btn btn-block btn_search">View AJ</a>
                                </div>
                                @if(isset($aj_standard_change) && !is_null($aj_standard_change) && ($aj_standard_change->name == 'yes' || $aj_standard_change->scope == 'yes' || $aj_standard_change->location == 'yes' || $aj_standard_change->version == 'yes'))
                                    <span style="color: red">Some Change in AJ</span>
                                @endif
                                <div class="clearfix"></div>
                            </div>
                            <div class="row auditJustificationTable mrgn popupLbl">
                                <div class="col-md-3">
                                    <label>{{ $audit_type }} Audit</label>
                                </div>
                                <div class="col-md-3">
                                    <label>Audit
                                        Date:<span> {{ date("d-m-Y", strtotime($postAudit->actual_audit_date)) }} - {{ date("d-m-Y", strtotime($postAudit->actual_audit_date_to)) }}</span></label>

                                </div>
                                <div class="col-md-3">
                                    @if($postAudit->status == 'unapproved')
                                        <label>Initial submission</label>
                                    @else
                                        <label>Re-submission</label>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label>Recieved From: <span>{{ $postAudit->sentByUser->fullName() }}</span></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="blu_clr">Corrective Action Request Confirmation:</h5>

                                    @if(isset($postAuditQuestionsOne) &&$postAuditQuestionsOne->value == 'YES')
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[0]->title_for_oc_om }}</label>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 question_1"
                                             style="display:{{((!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_1">

                                                        @if(!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)
                                                            @foreach($postAuditQuestionsOne->postAuditQuestionFile as $key=>$file)
                                                                <ul>
                                                                    <li class="upload_question_files1_'{{$key+1}}'">
                                                                        <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                           download
                                                                           class="linkClr"><span>{{$file->file_name}}</span></a>
                                                                        <input type="hidden" name="question_file[]"
                                                                               id="question_files_'{{$key+1}}'"
                                                                               value="{{$file->file_path}}">
                                                                        <input type="hidden" name="question_filename[]"
                                                                               value="{{$file->file_name}}">
                                                                    </li>
                                                                </ul>

                                                            @endforeach
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    @endif
                                    @if(isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES')
                                        <div class="col-md-12 mrgn">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[1]->title_for_oc_om }}</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 question_2"
                                             style="display:{{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_2">

                                                        @if(!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)
                                                            @foreach($postAuditQuestionsTwo->postAuditQuestionFile as $key=>$file)
                                                                <ul>
                                                                    <li class="upload_question_files2_'{{$key+1}}'">
                                                                        <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                           download
                                                                           class="linkClr"><span>{{$file->file_name}}</span></a>
                                                                        <input type="hidden" name="question_two_file[]"
                                                                               id="question_two_files_'{{$key+1}}'"
                                                                               value="{{$file->file_path}}">
                                                                        <input type="hidden"
                                                                               name="question_two_filename[]"
                                                                               value="{{$file->file_name}}">
                                                                    </li>
                                                                </ul>

                                                            @endforeach
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <label class="question_two_filename_0_error"></label>
                                        </div>
                                    @endif
                                </div>

                                @if(isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'YES')
                                    <div class="col-md-12  mrgn popupLbl">
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <h5 class="blu_clr">{{ $questions[2]->title_for_oc_om }}</h5>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 mrgn question_3"
                                             style="display:{{((!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)) ? 'block' : 'none'}};">
                                            <div class="col-md-12 ">
                                                <div class="col-md-6 trash_list" id="file_append_question_3">

                                                    @if(!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)
                                                        @foreach($postAuditQuestionsThree->postAuditQuestionFile as $key=>$file)
                                                            <ul>
                                                                <li class="upload_question_files3_'{{$key+1}}'">
                                                                    <a href="{{ asset('/uploads/post-audit-ims/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                       download
                                                                       class="linkClr"><span>{{$file->file_name}}</span></a>
                                                                    <input type="hidden" name="question_three_file[]"
                                                                           id="question_three_files_'{{$key+1}}'"
                                                                           value="{{$file->file_path}}">
                                                                    <input type="hidden"
                                                                           name="question_three_filename[]"
                                                                           value="{{$file->file_name}}">
                                                                </li>
                                                            </ul>

                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <label class="question_three_filename_0_error"></label>
                                    </div>
                                @endif
                            </div>


                            <div class="card collapsed-card">
                                <div class="card-header border-transparent">
                                    <h3 class="card-title">&nbsp;</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                            ▼
                                        </button>
                                    </div>
                                </div>

                                <!-- /.card-header -->
                                <div class="card-body p-0" style="display: block;">
                                    <div class="table-responsive">
                                        <table class="table m-0">
                                            <thead>
                                            <tr>
                                                <th>Review Questions</th>
                                                <th>Response</th>
                                            </tr>
                                            </thead>
                                            <tbody class="disabledDiv">
                                            <tr>
                                                <td>{{ $questions[0]->title_for_sc_sm }}</td>

                                                @if(!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value))
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       {{($postAuditQuestions[0]->scheme_value  == 'YES') ? 'checked' : ''}}
                                                                       value="YES"> &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="NO" {{($postAuditQuestions[0]->scheme_value  == 'NO') ? 'checked' : ''}}>&nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="N/A" {{($postAuditQuestions[0]->scheme_value  == 'N/A') ? 'checked' : ''}}>
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                @else
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="YES"> &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="NO" {{ (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES') ? 'checked' : '' }}>&nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                @if(isset($postAuditQuestionsOne))
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="N/A"{{ ($postAuditQuestionsOne->value == 'NO') ? 'checked' : '' }}>
                                                                    &nbsp;&nbsp;N/A
                                                                @else
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="N/A" checked>
                                                                    &nbsp;&nbsp;N/A
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td>{{ $questions[1]->title_for_sc_sm }}</td>
                                                @if(!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value))
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       {{($postAuditQuestions[1]->scheme_value  == 'YES') ? 'checked' : ''}}
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="NO" {{($postAuditQuestions[1]->scheme_value  == 'NO') ? 'checked' : ''}}>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="N/A" {{($postAuditQuestions[1]->scheme_value  == 'N/A') ? 'checked' : ''}}>
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                @else
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="NO" {{ (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') ? 'checked' : '' }}>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>

                                                                @if(isset($postAuditQuestionsTwo))
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="N/A" {{ (isset($postAuditQuestionsTwo) &&  $postAuditQuestionsTwo->value == 'NO') ? 'checked' : '' }}>
                                                                    &nbsp;&nbsp;N/A
                                                                @else
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="N/A" checked>
                                                                    &nbsp;&nbsp;N/A
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                            @if(!empty($postAuditQuestions) && count($postAuditQuestions) > 2)
                                                @foreach($postAuditQuestions as $key=>$postAuditQuestion)
                                                    @if($key ==0 || $key == 1)
                                                    @else
                                                        @if($audit_type =='Stage 1')
                                                            <tr class="{{ ($postAuditQuestion->question_id == 14) ? 'hidden' : '' }}">
                                                                <td>{{ $postAuditQuestion->question->title_for_sc_sm }}</td>
                                                                <td>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   {{ ($postAuditQuestion->scheme_value == 'YES') ? 'checked': '' }}
                                                                                   value="YES">
                                                                            &nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   {{ ($postAuditQuestion->scheme_value == 'NO') ? 'checked': '' }}
                                                                                   value="NO"
                                                                            >
                                                                            &nbsp;&nbsp;No
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   {{ ($postAuditQuestion->scheme_value == 'N/A') ? 'checked': '' }}
                                                                                   value="N/A">
                                                                            &nbsp;&nbsp;N/A
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr class="{{ ($postAuditQuestion->question_id == 13) ? 'hidden' : '' }}">
                                                                <td style="width: 78%;">{{ $postAuditQuestion->question->title_for_sc_sm }}</td>
                                                                <td>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   {{ ($postAuditQuestion->scheme_value == 'YES') ? 'checked': '' }}
                                                                                   value="YES">
                                                                            &nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   {{ ($postAuditQuestion->scheme_value == 'NO') ? 'checked': '' }}
                                                                                   value="NO"
                                                                            >
                                                                            &nbsp;&nbsp;No
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   {{ ($postAuditQuestion->scheme_value == 'N/A') ? 'checked': '' }}
                                                                                   value="N/A">
                                                                            &nbsp;&nbsp;N/A
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif

                                                    @endif

                                                @endforeach

                                            @else

                                                <tr>
                                                    <td>{{ $questions[3]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[2]"
                                                                       id="three"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[2]"
                                                                       id="three"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[2]"
                                                                       id="three"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[4]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[3]" id="four"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[3]" id="four"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[3]" id="four"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[5]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[4]" id="five"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[4]" id="five"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[4]" id="five"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[6]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[5]" id="six"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[5]" id="six"
                                                                       value="NO" checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[5]" id="six"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[7]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[6]"
                                                                       id="seven"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[6]"
                                                                       id="seven"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[6]"
                                                                       id="seven"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[8]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[7]"
                                                                       id="eight"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[7]"
                                                                       id="eight"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[7]"
                                                                       id="eight"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[9]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[8]" id="nine"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[8]" id="nine"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[8]" id="nine"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[10]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[9]" id="ten"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[9]" id="ten"
                                                                       value="NO" checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[9]" id="ten"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[11]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[10]"
                                                                       id="eleven"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[10]"
                                                                       id="eleven"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[10]"
                                                                       id="eleven"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="{{ ($audit_type != 'Stage 1') ? 'hidden': '' }}">
                                                    <td>{{ $questions[12]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[11]"
                                                                       id="twelve"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[11]"
                                                                       id="twelve"
                                                                       value="NO" checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[11]"
                                                                       id="twelve"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="{{ ($audit_type == 'Stage 1') ? 'hidden': '' }}">
                                                    <td>{{ $questions[13]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[12]"
                                                                       id="thirteen"
                                                                       value="YES"> &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[12]"
                                                                       id="thirteen"
                                                                       value="NO" checked> &nbsp;&nbsp;NO
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[12]"
                                                                       id="thirteen"
                                                                       value="N/A"> &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>

                                <!-- /.card-body -->
                                <!-- /.card-footer -->
                            </div>
                            <div class="row auditJustificationTable mrgn popupLbl dataNextLine">
                                <div class="col-md-3">
                                    <div class="col-md-5 floting">
                                        <label for="packed_closed">Pack Closed:</label>
                                        <span>{{(isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->pack_closed == 'YES') ? 'YES' : 'NO'}}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label>Current
                                        Status:
                                        <span>{{ (isset($companyStandard->certificates)) ? str_replace('_',' ',ucfirst($companyStandard->certificates->certificate_status)) : 'Not Certified' }}</span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            @if(isset($postAudit->postAuditSchemeInfo))
                                <div class="row auditJustificationTable mrgn popupLbl dataNextLine">
                                    <div class="col-md-3 floting packedClosedNew">
                                        <div class="col-md-12">
                                            <label for="approval_date">Approval / Issue Date </label>
                                        </div>
                                        <div class="col-md-12 floting">
                                            <span>{{(isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->approval_date) ?  date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->approval_date)):'' }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <label for="certificate_validity">Certificate Validity:
                                            <span>{{ ( isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->certificate_validity == '1 Year') ? '1 Year' :'3 Years' }}</span></label>
                                    </div>
                                    <div class="col-md-6 floting">
                                        <div class="col-md-12">
                                            <label>Issue History:</label>
                                        </div>
                                        <div class="col-md-6 floting">
                                            <label>Original Issue Date
                                                <span>{{(isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->original_issue_date) ?  date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->original_issue_date)):'' }}</span></label>
                                        </div>
                                        <div class="col-md-6 floting">
                                            <label>Last Expiry Date
                                                <span>{{(isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->new_expiry_date) ?  date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->new_expiry_date)):'' }}</span></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row auditJustificationTable mrgn popupLbl dataNextLine">
                                    <div class="col-md-3 floting packedClosedNew">
                                        <div class="col-md-12">
                                            <label for="last_certificate_issue_date">Last Certificate Issue
                                                Date </label>
                                        </div>
                                        <div class="col-md-12 floting">
                                            <span>{{(isset($companyStandard) && $companyStandard->last_certificate_issue_date) ?  date("d-m-Y", strtotime($companyStandard->last_certificate_issue_date)):'' }}</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="row auditJustificationTable mrgn popupLbl dataNextLine">
                                    <div class="col-md-3 floting">
                                        <div class="col-md-12">
                                            <label for="print_required">Printout Required:
                                                <span>{{ ( isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->print_required == 'YES') ? 'YES' :'NO' }}</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 floting printAssignTo"
                                            {{--                                         style="display: none;"--}}
                                    >
                                        <label for="print_assigned_to">Printing Assign To:
                                            <span>{{ (isset($postAudit->postAuditSchemeInfo) &&  $postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_manager') ? 'Scheme Manager' :'Scheme Coordinator' }}</span></label>
                                    </div>


                                    <div class="col-md-4 floting">
                                        <div class="col-md-6">
                                            <label for="new_expiry_date">New Expiry Date </label>
                                        </div>
                                        <div class="col-md-12 floting">
                                            <span>{{(isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->new_expiry_date) ?  date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->new_expiry_date)):'' }}</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            @endif
                        </div>

                        <div class="border_div"></div>

                        @if(!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0)
                            @foreach($imsCompanyStandards as $imsStandard)
                                @php
                                    if ($imsStandard->standard->standardFamily->name == 'Food') {
                                       $accreditationId = \App\CompanyStandardFoodCode::where('company_standard_id', $imsStandard->id)->where('company_id', $company->id)->where('standard_id', $imsStandard->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();
                                    } elseif ($imsStandard->standard->standardFamily->name == 'Energy Management') {
                                       $accreditationId =\App\CompanyStandardEnergyCode::where('company_standard_id', $imsStandard->id)->where('company_id',  $company->id)->where('standard_id', $imsStandard->standard_id)->whereNull('deleted_at')->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();
                                    } else {
                                       $accreditationId = \App\CompanyStandardCode::where('company_standard_id', $imsStandard->id)->where('company_id',  $company->id)->where('standard_id', $imsStandard->standard_id)->whereNull('deleted_at')->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();
                                    }
                                    $accreditations = \App\Accreditation::whereIn('id', $accreditationId)->get();
                                @endphp
                                <div class="modal-body">
                                    <label style="margin-left: 10px;">{{ $imsStandard->standard->name }}
                                        Accreditations</label>
                                    <br>
                                    <div class="row auditJustificationTable mrgn popupLbl dataNextLine">

                                        <div class="col-md-6 floting">
                                            <table class="table table-bordered text-center manday_tbl">

                                                <tbody>
                                                <tr>
                                                    <th>Standard</th>
                                                    <th>Accreditation Code</th>
                                                    <th>Accreditation Name</th>
                                                </tr>
                                                @if(!empty($accreditations) && count($accreditations) > 0)
                                                    @foreach($accreditations as $key=>$accreditation)
                                                        <tr>
                                                            <td>{{ $imsStandard->standard->name }}</td>
                                                            <td>{{ $accreditation->name }}</td>
                                                            <td>{{ $accreditation->full_name }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="border_div"></div>
                            @endforeach
                        @endif
                        <div class="modal-body">
                            <div class="row auditJustificationTable mrgn popupLbl dataNextLine">
                                <div class="col-md-6 floting">
                                    <label>Confirm Certificate Printed and upload the certificate file. <a href="#"></a></label>
                                    <div class="custom-file">
                                        <input type="file" class="" name="certificate_files[]" id="certificate_files"
                                               value="" multiple/>
                                    </div>
                                    <div class="col-md-12 trash_list" id="file_append_certificate_1">
                                        <ul>
                                            @if(!empty($postAudit->postAuditCertificateFile) && count($postAudit->postAuditCertificateFile) > 0)
                                                @foreach($postAudit->postAuditCertificateFile as $key=>$file)
                                                    <li class="upload_certificate_files1_'{{$key+1}}'">
                                                        @if($certificate_file->file_type === 'file')
                                                            <a href="{{asset('uploads/certificateFiles/'.$file->file_path)}}"
                                                               download
                                                               class="linkClr"><span>{{$file->file_name}}</span></a>
                                                        @else
                                                            <a href="{{$file->file_path}}" download
                                                               class="linkClr"><span>{{$file->file_name}}</span></a>
                                                        @endif
                                                        <a href="#" class="trash_icon"
                                                           onclick="deleteCertificateOnFile('{{$key+1}}')"><i
                                                                    class="fa fa-trash"></i></a>
                                                        {{--                                                        <input type="hidden" name="certificate_file[]"--}}
                                                        {{--                                                               id="certificate_files_'{{$key+1}}'"--}}
                                                        {{--                                                               value="{{$file->file_path}}">--}}
                                                        {{--                                                        <input type="hidden" name="certificate_filename[]"--}}
                                                        {{--                                                               value="{{$file->file_name}}">--}}
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                    </div>
                                </div>
                                {{--                                <div class="col-md-6 floting">--}}
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <label>Add UCI # </label>--}}
                                {{--                                        <input type="text" class="form-control hasBorder" name="uci_number"--}}
                                {{--                                               id="disble_fld"--}}
                                {{--                                               value="{{ (isset($postAudit->postAuditSchemeInfo) &&  isset($postAudit->postAuditSchemeInfo->uci_number)) ? $postAudit->postAuditSchemeInfo->uci_number : ''  }}">--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}


                            </div>

                            <div class="col-md-12 mrgn no_padding">
                                <div class="col-md-7 floting"></div>
                                <div class="col-md-2 floting text-right">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success btn_save">Accept &amp;
                                            Close
                                        </button>
                                    </div>


                                </div>
                                <div class="col-md-3 floting text-right">
                                    @if ($standardStage->audit_type =='stage_2')
                                        <a class="btn btn-primary btn-block tblBtn"
                                           href="{{ route('certificate-letter', [ $companyStandard->id,$standardStage->id]) }}"
                                           title="Certificate Letter">Download Draft for Printing
                                            Letter
                                        </a>
                                    @else
                                        @if($standardStage->audit_type !='stage_1')
                                            <a class="btn btn-primary btn-block tblBtn"
                                               href="{{ route('certificate-letter-others', [ $companyStandard->id,$standardStage->id]) }}"
                                               title="Certificate Letter">Download Draft for Printing
                                                Letter
                                            </a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->


                    </form>

                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>

@endsection
@push('models')
@endpush
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">

        var certificateCount = 0;


        $('#certificate_files').change(function () {


            var totalfiles = this.files.length;
            var html = '';
            for (var index = 0; index < totalfiles; index++) {
                var filename = this.files[index].name;

                if (this.files && this.files[index]) {
                    certificateCount++;
                    html += '<p class="upload_certificate_files1_' + certificateCount + '">';
                    html += '<a href="#" class="linkClr"><span> ' + filename + '</span></a>';
                    html += '<a href="#" class="trash_icon" onclick="deleteCertificateOnFile(' + certificateCount + ')"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" name="certificate_file[]" id="certificate_files_' + certificateCount + '" value="">';
                    html += '<input type="hidden" name="certificate_filename[]" value=" ' + filename + '">';
                    html += '</p>'
                    getBase64(this.files[index], certificateCount, 'certificate_files_');
                }
            }
            $('#file_append_certificate_1').append(html);
        });

        function deleteCertificateOnFile(id) {
            $('.upload_certificate_files1_' + id).remove();
            certificateCount--;
        }

        function getBase64(file, index, id) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                $('#' + id + '' + index).val(reader.result);
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }

        function validate(e) {
            $('#loader').show();
            if (certificateCount === 0) {
                $('#loader').hide();
                toastr['error']('Certificate file is mandatory'); //toaster message issue
                return false;
            } else {

                return true;
            }

        }
    </script>

@endpush
