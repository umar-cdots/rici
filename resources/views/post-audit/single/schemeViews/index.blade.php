@extends('layouts.master')
@section('title', "Edit Post Audit {$audit_type}")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
    <style>
        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 1px !important;
        }

        .daterangepicker .ranges .input-mini {
            border: 1px solid #ccc;
            border-radius: 4px;
            color: #555;
            display: block;
            font-size: 17px !important;
            height: 37px !important;
            line-height: 30px;
            vertical-align: middle;
            margin: 0 0 10px 0;
            padding: 0 6px;
            width: 160px !important;
        }

        .daterangepicker .daterangepicker_start_input label, .daterangepicker .daterangepicker_end_input label {
            font-size: 16px !important;

        }

        .hidden {
            display: none;
        }

        .actual_audit_date_error,
        .q1_name_error,
        .q2_name_error,
        .q3_name_error,
        .question_ones_filename_0_error,
        .question_two_filename_0_error,
        .question_three_filename_0_error,
        .question_four_filename_0_error,
        .remarks_error,
        .evidence_error,
        .expiry_date_error,
        .approval_date_error {
            color: red !important;
        }
    </style>
@endpush
@section('content')
    @php
        if(auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator'){
            $disable = 'disabledDiv';
        }else{
            $disable = '';
        }
    @endphp
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 620.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->
        @php
            $aj_standard_change = \App\AjStageChange::where('aj_standard_stage_id',$standardStage->id)->first();

        @endphp
                <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">AUDIT
                            <span>ACTIVITY</span> @if(isset($aj_standard_change) && !is_null($aj_standard_change) && ($aj_standard_change->name == 'yes' || $aj_standard_change->scope == 'yes' || $aj_standard_change->location == 'yes' || $aj_standard_change->version == 'yes'))
                                (<span style="color: red;font-weight: bold">Some Change in AJ</span>)
                            @endif</h1>



                    </div><!-- /.col -->
                    @if(auth()->user()->user_type == 'scheme_manager' && $postAudit->status == 'approved')
                        <div class="col-sm-12">
                            <a href="{{ route('post.audit.scheme.print', [$company->id, $standardStage->getAuditType(), $companyStandard->standard->id,$standardStage->id]) }}"
                               class="printAj float-right"><i
                                        class="fa fa-print"></i></a>
                        </div>
                    @endif

                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn formUploadBtnHeightFix">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Audit Activity
                            @if(!is_null($companyStandard->version_change))
                                &nbsp; <a href="javascript:void(0)" class="float-right"
                                          data-target="#viewModal"
                                          data-toggle="modal"><i
                                            class="fa fa-eye"></i> Transfer Case History</a>
                            @endif
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" onsubmit="return validate(event)" action="{{ route('post.audit.scheme.store') }}"
                          method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="status" id="status" value="">
                        <input type="hidden" name="company_id" value="{{$company->id}}">
                        <input type="hidden" name="standard_id" value="{{$standard->id}}">
                        <input type="hidden" name="recycle" value="{{$standardStage->recycle}}">
                        <input type="hidden" name="aj_standard_stage_id" value="{{$standardStage->id}}">
                        <input type="hidden" name="audit_type" id="audit_type" value="{{$audit_type}}">
                        <input type="hidden" name="post_audit_id" value="{{ $postAudit->id }}">

                        @php

                            if($audit_type === 'Stage 2'){
                                $stage1AjStage = \App\AJStandardStage::where('audit_type','stage_1')->where('aj_standard_id',$standardStage->aj_standard_id)->first(['id']);
                                $stage1PostAudit = \App\PostAudit::where('aj_standard_stage_id',$stage1AjStage->id)->first(['status']);
                            }else{
                                $stage1PostAudit = null;
                            }

                        @endphp


                        @if(!is_null($postAuditQuestionsOne))
                            <input type="hidden" name="post_audit_question_one_id"
                                   value="{{ $postAuditQuestionsOne->id }}">
                        @endif

                        @if(!is_null($postAuditQuestionsTwo))
                            <input type="hidden" name="post_audit_question_two_id"
                                   value="{{ $postAuditQuestionsTwo->id }}">
                        @endif

                        @if(!is_null($postAuditQuestionsThree))
                            <input type="hidden" name="post_audit_question_three_id"
                                   value="{{ $postAuditQuestionsThree->id }}">
                        @endif

                        <div class="modal-body">
                            <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                <div class="col-md-3">
                                    <label><strong>{{ ucfirst($company->name) }}</strong></label>
                                </div>
                                <div class="col-md-6">
                                    <label><strong>Standards ({{ $standard->name }})</strong></label>
                                </div>

                                <div class="col-md-3 floting">
                                    @if(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES' && !is_null($postAudit->q4_file))
                                        <input type="checkbox" id="enableQ4" style="width: 20px;height: 20px;">
                                    @endif
                                    <a href="{{route('justification.show', [$company->id, $standardStage->getAuditType(), $standard->name,$standardStage->id]) }}"
                                       class="btn btn-block btn_search">View AJ</a>
                                    @if(isset($aj_standard_change) && !is_null($aj_standard_change) && ($aj_standard_change->name == 'yes' || $aj_standard_change->scope == 'yes' || $aj_standard_change->location == 'yes' || $aj_standard_change->version == 'yes'))
                                        (<span style="color: red">Some Change in AJ</span>)
                                    @endif

                                </div>


                                <div class="clearfix"></div>
                            </div>
                            <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                <div class="col-md-3">
                                    <label>{{ $audit_type }} Audit</label>
                                </div>
                                @if(auth()->user()->user_type !== 'scheme_manager')
                                    <div class="col-md-3">
                                        <label>Audit
                                            Date:<span> {{ date("d-m-Y", strtotime($postAudit->actual_audit_date)) }} - {{ date("d-m-Y", strtotime($postAudit->actual_audit_date_to)) }}</span></label>
                                    </div>
                                @endif
                                <div class="col-md-3">
                                    @if($postAudit->status == 'unapproved')
                                        <label>Initial submission</label>
                                    @else
                                        <label>Re-submission</label>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label>Received From: <span>{{ $postAudit->sentByUser->fullName() }}</span></label>
                                </div>
                            </div>
                            @if(auth()->user()->user_type == 'scheme_manager')
                                <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                    <div class="col-md-2 floting">
                                        <label for="actual_audit_date">Actual Audit Date :</label>
                                    </div>
                                    <div class="col-md-4 floting">

                                        <div class="form-group">
                                            <div class="input-group mb-0">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>


                                                <input type="text" name="actual_audit_date" readonly
                                                       id="actual_audit_date"
                                                       value="{{ date("d-m-Y", strtotime($postAudit->actual_audit_date)) }} - {{ date("d-m-Y", strtotime($postAudit->actual_audit_date_to)) }}"
                                                       class="form-control float-right active actual_audit_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="actual_audit_date_error"></label>
                                </div>
                            @endif
                            <div class="row {{ $disable }}">
                                <div class="col-md-12">
                                    <h5 class="blu_clr">Corrective Action Request Confirmation:</h5>

                                    @if(isset($postAuditQuestionsOne) &&$postAuditQuestionsOne->value == 'YES')
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[0]->title_for_oc_om }}</label>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 question_1"
                                             style="display:{{((!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 floting">
                                                        <label> Upload Verification Pack</label>
                                                    </div>
                                                    <div class="col-md-10 floting">

                                                        <div class="input-group control-group_q1_files increment_q1_files">
                                                            <input type="file" name="q1_files[]"
                                                                   class="form-control noBorder" id="q1_files">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-success q1_files_add"
                                                                        type="button"><i
                                                                            class="glyphicon glyphicon-plus"></i>Add
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="clone_q1_files hide">
                                                            <div class="control-group_q1_files"
                                                                 style="margin-top:10px">
                                                                <input type="file" name="q1_files[]"
                                                                       class="form-control noBorder" id="q1_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-danger q1_files_delete"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-remove"></i>
                                                                        Remove
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_1">

                                                        @if(!empty($postAuditQuestionsOne->postAuditQuestionFile) && count($postAuditQuestionsOne->postAuditQuestionFile) > 0)
                                                            @foreach($postAuditQuestionsOne->postAuditQuestionFile as $key=>$file)
                                                                @php
                                                                    $precision = 2;
                                                                     $filename = public_path().'/uploads/post-audit/'. $file->post_audit_question_id.'/'.$file->file_path;
                                                                     if (file_exists($filename)) {
                                                                         $bytes = \File::size(($filename));
                                                                         $units = array('B', 'KB', 'MB', 'GB', 'TB');
                                                                         $bytes = max($bytes, 0);
                                                                         $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
                                                                         $pow = min($pow, count($units) - 1);
                                                                         $bytes /= (1 << (10 * $pow));
                                                                         $fileSize =round($bytes, $precision) . ' ' . $units[$pow];
                                                                     }else{
                                                                         $fileSize = '';
                                                                     }
                                                                @endphp
                                                                <ul>
                                                                    <li class="upload_question_files1_{{$key+1}}">
                                                                        <a href="{{ asset('/uploads/post-audit/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                           download
                                                                           class="linkClr"><span>{{$file->file_name}}</span>
                                                                            <span>&nbsp;&nbsp; {{ $fileSize }} &nbsp;&nbsp;</span></a>
                                                                        @if(auth()->user()->user_type == 'scheme_coordinator' && ($postAudit->status == 'approved' || $postAudit->status == 'accepted'))
                                                                            {{--                                                                            @if($postAudit->status == 'accepted')--}}
                                                                            {{--                                                                            @else--}}
                                                                            {{--                                                                                <a href="javascript:void(0)"--}}
                                                                            {{--                                                                                   class="trash_icon"--}}
                                                                            {{--                                                                                   onclick="deleteQuestionOneFile('{{$key+1}}','{{$file->id}}')"><i--}}
                                                                            {{--                                                                                            class="fa fa-trash"></i></a>--}}
                                                                            {{--                                                                            @endif--}}
                                                                        @else
                                                                            <a href="javascript:void(0)"
                                                                               class="trash_icon"
                                                                               onclick="deleteQuestionOneFile('{{$key+1}}','{{$file->id}}')"><i
                                                                                        class="fa fa-trash"></i></a>
                                                                        @endif
                                                                        {{--                                                                    <input type="hidden" name="q1_files[]"--}}
                                                                        {{--                                                                           id="question_one_files_{{$key+1}}"--}}
                                                                        {{--                                                                           value="{{$file->file_path}}">--}}
                                                                        {{--                                                                    <input type="hidden"--}}
                                                                        {{--                                                                           name="question_ones_filename[]"--}}
                                                                        {{--                                                                           value="{{$file->file_name}}">--}}
                                                                        <input type="hidden"
                                                                               name="question_one_file_id[]"
                                                                               id="question_one_files_id_{{$key+1}}"
                                                                               value="{{$file->id}}">
                                                                    </li>
                                                                </ul>

                                                            @endforeach
                                                        @endif


                                                    </div>
                                                </div>
                                            </div>
                                            <label class="question_ones_filename_0_error"></label>

                                        </div>
                                    @endif
                                    @if(isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES')
                                        <div class="col-md-12 mrgn">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[1]->title_for_oc_om }}</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 question_2"
                                             style="display:{{((!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)) ? 'block' : 'none'}}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 floting">
                                                        <label> Upload CAR(s) </label>
                                                    </div>
                                                    <div class="col-md-10 floting">
                                                        <div class="input-group control-group_q2_files increment_q2_files">
                                                            <input type="file" name="q2_files[]"
                                                                   class="form-control noBorder" id="q2_files">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-success q2_files_add"
                                                                        type="button"><i
                                                                            class="glyphicon glyphicon-plus"></i>Add
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="clone_q2_files hide">
                                                            <div class="control-group_q2_files input-group"
                                                                 style="margin-top:10px">
                                                                <input type="file" name="q2_files[]"
                                                                       class="form-control noBorder" id="q2_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-danger q2_files_delete"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-remove"></i>
                                                                        Remove
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_2">

                                                        @if(!empty($postAuditQuestionsTwo->postAuditQuestionFile) && count($postAuditQuestionsTwo->postAuditQuestionFile) > 0)
                                                            @foreach($postAuditQuestionsTwo->postAuditQuestionFile as $key=>$file)
                                                                @php
                                                                    $precision = 2;
                                                                     $filename = public_path().'/uploads/post-audit/'. $file->post_audit_question_id.'/'.$file->file_path;
                                                                     if (file_exists($filename)) {
                                                                         $bytes = \File::size(($filename));
                                                                         $units = array('B', 'KB', 'MB', 'GB', 'TB');
                                                                         $bytes = max($bytes, 0);
                                                                         $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
                                                                         $pow = min($pow, count($units) - 1);
                                                                         $bytes /= (1 << (10 * $pow));
                                                                         $fileSize =round($bytes, $precision) . ' ' . $units[$pow];
                                                                     }else{
                                                                         $fileSize = '';
                                                                     }
                                                                @endphp
                                                                <ul>
                                                                    <li class="upload_question_files2_{{$key+1}}">
                                                                        <a href="{{ asset('/uploads/post-audit/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                           class="linkClr"><span>{{$file->file_name}}</span>
                                                                            <span>&nbsp;&nbsp; {{ $fileSize }} &nbsp;&nbsp;</span></a>
                                                                        @if(auth()->user()->user_type == 'scheme_coordinator'  && ($postAudit->status == 'approved' || $postAudit->status == 'accepted'))
                                                                            {{--                                                                            @if($postAudit->status == 'accepted')--}}
                                                                            {{--                                                                            @else--}}
                                                                            {{--                                                                                <a href="javascript:void(0)"--}}
                                                                            {{--                                                                                   class="trash_icon"--}}
                                                                            {{--                                                                                   onclick="deleteQuestionTwoFile('{{$key+1}}','{{$file->id}}')"><i--}}
                                                                            {{--                                                                                            class="fa fa-trash"></i></a>--}}
                                                                            {{--                                                                            @endif--}}
                                                                        @else
                                                                            <a href="javascript:void(0)"
                                                                               class="trash_icon"
                                                                               onclick="deleteQuestionTwoFile('{{$key+1}}','{{$file->id}}')"><i
                                                                                        class="fa fa-trash"></i></a>
                                                                        @endif
                                                                        {{--                                                                    <input type="hidden" name="question_two_file[]"--}}
                                                                        {{--                                                                           id="question_two_files_{{$key+1}}"--}}
                                                                        {{--                                                                           value="{{$file->file_path}}">--}}
                                                                        {{--                                                                    <input type="hidden"--}}
                                                                        {{--                                                                           name="question_two_filename[]"--}}
                                                                        {{--                                                                           value="{{$file->file_name}}">--}}
                                                                        <input type="hidden"
                                                                               name="question_two_file_id[]"
                                                                               id="question_two_files_id_{{$key+1}}"
                                                                               value="{{$file->id}}">
                                                                    </li>
                                                                </ul>

                                                            @endforeach
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <label class="question_two_filename_0_error"></label>
                                        </div>
                                    @endif
                                </div>

                                @if(isset($postAuditQuestionsThree) && $postAuditQuestionsThree->value == 'YES')
                                    <div class="col-md-12  mrgn popupLbl">
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <h5 class="blu_clr">{{ $questions[2]->title_for_oc_om }}</h5>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 question_3  mrgn popupLbl" style="display: block">
                                            <div class="col-md-2 floting">
                                                <label> Attach Files </label>
                                            </div>
                                            <div class="col-md-10 floting">
                                                <div class="input-group control-group_q3_files increment_q3_files">
                                                    <input type="file" name="q3_files[]"
                                                           class="form-control noBorder" id="q3_files">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-success q3_files_add"
                                                                type="button"><i
                                                                    class="glyphicon glyphicon-plus"></i>Add
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="clone_q3_files hide">
                                                    <div class="control-group_q3_files input-group"
                                                         style="margin-top:10px">
                                                        <input type="file" name="q3_files[]"
                                                               class="form-control noBorder" id="q3_files">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-danger q3_files_delete"
                                                                    type="button"><i
                                                                        class="glyphicon glyphicon-remove"></i>
                                                                Remove
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 mrgn question_3"
                                             style="display:{{((!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)) ? 'block' : 'none'}};">
                                            <div class="col-md-12 ">
                                                <div class="col-md-6 trash_list" id="file_append_question_3">

                                                    @if(!empty($postAuditQuestionsThree->postAuditQuestionFile) && count($postAuditQuestionsThree->postAuditQuestionFile) > 0)
                                                        @foreach($postAuditQuestionsThree->postAuditQuestionFile as $key=>$file)
                                                            @php
                                                                $precision = 2;
                                                                     $filename = public_path().'/uploads/post-audit/'. $file->post_audit_question_id.'/'.$file->file_path;
                                                                     if (file_exists($filename)) {
                                                                         $bytes = \File::size(($filename));
                                                                         $units = array('B', 'KB', 'MB', 'GB', 'TB');
                                                                         $bytes = max($bytes, 0);
                                                                         $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
                                                                         $pow = min($pow, count($units) - 1);
                                                                         $bytes /= (1 << (10 * $pow));
                                                                         $fileSize =round($bytes, $precision) . ' ' . $units[$pow];
                                                                     }else{
                                                                         $fileSize = '';
                                                                     }
                                                            @endphp
                                                            <ul>
                                                                <li class="upload_question_files3_{{$key+1}}">
                                                                    <a href="{{ asset('/uploads/post-audit/'. $file->post_audit_question_id.'/'.$file->file_path) }}"
                                                                       class="linkClr"><span>{{$file->file_name}}</span>
                                                                        <span>&nbsp;&nbsp; {{ $fileSize }} &nbsp;&nbsp;</span></a>
                                                                    @if(auth()->user()->user_type == 'scheme_coordinator'  && ($postAudit->status == 'approved' || $postAudit->status == 'accepted'))
                                                                        {{--                                                                        @if($postAudit->status == 'accepted')--}}
                                                                        {{--                                                                        @else--}}
                                                                        {{--                                                                            <a href="javascript:void(0)"--}}
                                                                        {{--                                                                               class="trash_icon"--}}
                                                                        {{--                                                                               onclick="deleteQuestionThreeFile('{{$key+1}}','{{$file->id}}')"><i--}}
                                                                        {{--                                                                                        class="fa fa-trash"></i></a>--}}
                                                                        {{--                                                                        @endif--}}
                                                                    @else
                                                                        <a href="javascript:void(0)"
                                                                           class="trash_icon"
                                                                           onclick="deleteQuestionThreeFile('{{$key+1}}','{{$file->id}}')"><i
                                                                                    class="fa fa-trash"></i></a>
                                                                    @endif
                                                                    {{--                                                                <input type="hidden" name="question_three_file[]"--}}
                                                                    {{--                                                                       id="question_three_files_{{$key+1}}"--}}
                                                                    {{--                                                                       value="{{$file->file_path}}">--}}
                                                                    {{--                                                                <input type="hidden" name="question_three_filename[]"--}}
                                                                    {{--                                                                       value="{{$file->file_name}}">--}}
                                                                    <input type="hidden" name="question_three_file_id[]"
                                                                           id="question_three_files_id_{{$key+1}}"
                                                                           value="{{$file->id}}">
                                                                </li>
                                                            </ul>

                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <label class="question_three_filename_0_error"></label>
                                    </div>
                                @endif

                                <div class="col-md-12 margn popupLbl" id="enableQ4Files" style="display: none">
                                    <div class="col-md-12 question_4"
                                         style="display: {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'block' : 'none'}}">
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <h5 class="blu_clr">Communication</h5>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <label> Attach Files </label>
                                        </div>
                                        <div class="col-md-10 floting">
                                            <div class="input-group control-group_q4_files increment_q4_files">
                                                <input type="file" name="q4_files[]"
                                                       class="form-control" id="q4_files">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-success q4_files_add"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-plus"></i>Add
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="clone_q4_files hide">
                                                <div class="control-group_q4_files input-group"
                                                     style="margin-top:10px">
                                                    <input type="file" name="q4_files[]"
                                                           class="form-control" id="q4_files">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-danger q4_files_delete"
                                                                type="button"><i
                                                                    class="glyphicon glyphicon-remove"></i>
                                                            Remove
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-12 mrgn question_4"
                                         style="display:{{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES') ? 'block' : 'none'}};">
                                        <div class="col-md-12 ">
                                            <div class="col-md-4 trash_list" id="file_append_question_4">
                                                <p class="upload_question_files4_1">
                                                    <a href="{{ asset('/uploads/post-audit/communication/'.$postAudit->id.'/'.$postAudit->q4_file) }}"
                                                       download class="linkClr"><span>Click to download</span></a>
                                                    <a href="javascript:void(0)" class="trash_icon"
                                                       onclick="deleteQuestionFourFile({{$postAudit->id}})"><i
                                                                class="fa fa-trash"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <label class="question_four_filename_0_error"></label>
                                </div>
                            </div>


                            <div class="card collapsed-card {{ $disable }}">
                                <div class="card-header border-transparent">
                                    <h3 class="card-title">&nbsp;</h3>

                                    <div class="card-tools">
                                        @if(auth()->user()->user_type == 'scheme_manager')
                                            <button id="selectAll" type="button">Select All</button>
                                        @endif
                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                            ▼
                                        </button>
                                    </div>
                                </div>

                                <!-- /.card-header -->
                                <div class="card-body p-0" style="display: block;">
                                    <div class="table-responsive">
                                        <table class="table m-0">
                                            <thead>
                                            <tr>
                                                <th>Review Questions</th>
                                                <th>Response</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{ $questions[0]->title_for_sc_sm }}</td>

                                                @if(!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value))
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       {{($postAuditQuestions[0]->scheme_value  == 'YES') ? 'checked' : ''}}
                                                                       value="YES"> &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="NO" {{($postAuditQuestions[0]->scheme_value  == 'NO') ? 'checked' : ''}}>&nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="N/A" {{($postAuditQuestions[0]->scheme_value  == 'N/A') ? 'checked' : ''}}>
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                @else
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[0]" id="one"
                                                                       value="YES"> &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>

                                                        @if((isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES'))
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="NO" checked>&nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="N/A">&nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        @elseif((isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'NO'))
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="NO">&nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="N/A" checked>&nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="NO">&nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[0]"
                                                                           id="one"
                                                                           value="N/A" checked>&nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td>{{ $questions[1]->title_for_sc_sm }}</td>
                                                @if(!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value))
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       {{($postAuditQuestions[1]->scheme_value  == 'YES') ? 'checked' : ''}}
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="NO" {{($postAuditQuestions[1]->scheme_value  == 'NO') ? 'checked' : ''}}>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="N/A" {{($postAuditQuestions[1]->scheme_value  == 'N/A') ? 'checked' : ''}}>
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                @else
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[1]"
                                                                       id="two"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>

                                                        @if((isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES'))
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="NO" checked>&nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="N/A">&nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        @elseif((isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'NO'))
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="NO">&nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="N/A" checked>&nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="NO">&nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[1]"
                                                                           id="two"
                                                                           value="N/A" checked>&nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        @endif


                                                        {{--                                                        <div class="col-md-3 floting">--}}
                                                        {{--                                                            <div>--}}
                                                        {{--                                                                <input type="radio" name="scheme_questions[1]"--}}
                                                        {{--                                                                       id="two"--}}
                                                        {{--                                                                       value="NO" {{ (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') ? 'checked' : '' }}>--}}
                                                        {{--                                                                &nbsp;&nbsp;No--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                        <div class="col-md-3 floting">--}}
                                                        {{--                                                            <div>--}}

                                                        {{--                                                                @if(isset($postAuditQuestionsTwo))--}}
                                                        {{--                                                                    <input type="radio" name="scheme_questions[1]"--}}
                                                        {{--                                                                           id="two"--}}
                                                        {{--                                                                           value="N/A" {{ (isset($postAuditQuestionsTwo) &&  $postAuditQuestionsTwo->value == 'NO') ? 'checked' : '' }}>--}}
                                                        {{--                                                                    &nbsp;&nbsp;N/A--}}
                                                        {{--                                                                @else--}}
                                                        {{--                                                                    <input type="radio" name="scheme_questions[1]"--}}
                                                        {{--                                                                           id="two"--}}
                                                        {{--                                                                           value="N/A" checked>--}}
                                                        {{--                                                                    &nbsp;&nbsp;N/A--}}
                                                        {{--                                                                @endif--}}

                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                    </td>
                                                @endif
                                            </tr>
                                            @if(!empty($postAuditQuestions) && count($postAuditQuestions) > 2)
                                                @foreach($postAuditQuestions as $key=>$postAuditQuestion)
                                                    @if($key ==0 || $key == 1)
                                                    @else
                                                        @if($audit_type =='Stage 1' && $postAuditQuestion->question_id == 14)
                                                        @elseif($audit_type !='Stage 1' && $postAuditQuestion->question_id == 13)
                                                            {{--                                                            <tr class="{{ ($postAuditQuestion->question_id == 14) ? 'hidden' : '' }}">--}}
                                                            {{--                                                                <td>{{ $postAuditQuestion->question->title_for_sc_sm }}</td>--}}
                                                            {{--                                                                <td>--}}
                                                            {{--                                                                    <div class="col-md-3 floting">--}}
                                                            {{--                                                                        <div>--}}
                                                            {{--                                                                            <input type="radio"--}}
                                                            {{--                                                                                   name="scheme_questions[{{$key}}]"--}}
                                                            {{--                                                                                   class="{{ ($postAuditQuestion->question_id == 14) ? 'hidden' : '' }}"--}}
                                                            {{--                                                                                   {{ ($postAuditQuestion->scheme_value == 'YES') ? 'checked': '' }}--}}
                                                            {{--                                                                                   value="YES">--}}
                                                            {{--                                                                            &nbsp;&nbsp;Yes--}}
                                                            {{--                                                                        </div>--}}
                                                            {{--                                                                    </div>--}}
                                                            {{--                                                                    <div class="col-md-3 floting">--}}
                                                            {{--                                                                        <div>--}}
                                                            {{--                                                                            <input type="radio"--}}
                                                            {{--                                                                                   name="scheme_questions[{{$key}}]"--}}
                                                            {{--                                                                                   class="{{ ($postAuditQuestion->question_id == 14) ? 'hidden' : '' }}"--}}
                                                            {{--                                                                                   {{ ($postAuditQuestion->scheme_value == 'NO') ? 'checked': '' }}--}}
                                                            {{--                                                                                   value="NO"--}}
                                                            {{--                                                                            >--}}
                                                            {{--                                                                            &nbsp;&nbsp;No--}}
                                                            {{--                                                                        </div>--}}
                                                            {{--                                                                    </div>--}}
                                                            {{--                                                                    <div class="col-md-3 floting">--}}
                                                            {{--                                                                        <div>--}}
                                                            {{--                                                                            <input type="radio"--}}
                                                            {{--                                                                                   name="scheme_questions[{{$key}}]"--}}
                                                            {{--                                                                                   class="{{ ($postAuditQuestion->question_id == 14) ? 'hidden' : '' }}"--}}
                                                            {{--                                                                                   {{ ($postAuditQuestion->scheme_value == 'N/A') ? 'checked': '' }}--}}
                                                            {{--                                                                                   value="N/A">--}}
                                                            {{--                                                                            &nbsp;&nbsp;N/A--}}
                                                            {{--                                                                        </div>--}}
                                                            {{--                                                                    </div>--}}
                                                            {{--                                                                </td>--}}
                                                            {{--                                                            </tr>--}}
                                                        @else
                                                            <tr class="">
                                                                <td style="width: 78%;">{{ $postAuditQuestion->question->title_for_sc_sm }}</td>
                                                                <td>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   class=""
                                                                                   {{ ($postAuditQuestion->scheme_value == 'YES') ? 'checked': '' }}
                                                                                   value="YES">
                                                                            &nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   class=""
                                                                                   {{ ($postAuditQuestion->scheme_value == 'NO') ? 'checked': '' }}
                                                                                   value="NO"
                                                                            >
                                                                            &nbsp;&nbsp;No
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[{{$key}}]"
                                                                                   class=""
                                                                                   {{ ($postAuditQuestion->scheme_value == 'N/A') ? 'checked': '' }}
                                                                                   value="N/A">
                                                                            &nbsp;&nbsp;N/A
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif

                                                    @endif

                                                @endforeach

                                                @if($audit_type != 'Stage 1')
                                                    @if(count($postAuditQuestions) < 13)
                                                        @if($audit_type != 'Stage 2')
                                                            <tr class="">
                                                                <td>{{ $questions[14]->title_for_sc_sm }}</td>
                                                                <td>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[13]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="YES" checked>
                                                                            &nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[13]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="NO">
                                                                            &nbsp;&nbsp;No
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[13]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="N/A">
                                                                            &nbsp;&nbsp;N/A
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if($standard->name == 'ISO 45001:2018' || $standard->name == 'OHSAS 18001:2007')
                                                            <tr class="">
                                                                <td style="width: 78%;">{{ $questions[16]->title_for_sc_sm }}</td>
                                                                <td>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[14]"
                                                                                   id="twelve"
                                                                                   checked
                                                                                   class=""
                                                                                   value="YES">
                                                                            &nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[14]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="NO">
                                                                            &nbsp;&nbsp;No
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[14]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="N/A">
                                                                            &nbsp;&nbsp;N/A
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if($audit_type == 'Reaudit')
                                                            <tr class="">
                                                                <td>{{ $questions[15]->title_for_sc_sm }}</td>
                                                                <td>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[15]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   checked
                                                                                   value="YES">
                                                                            &nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[15]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="NO">
                                                                            &nbsp;&nbsp;No
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 floting">
                                                                        <div>
                                                                            <input type="radio"
                                                                                   name="scheme_questions[15]"
                                                                                   id="twelve"
                                                                                   class=""
                                                                                   value="N/A">
                                                                            &nbsp;&nbsp;N/A
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endif
                                                @endif
                                            @else

                                                <tr>
                                                    <td>{{ $questions[3]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[2]"
                                                                       id="three"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[2]"
                                                                       id="three"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[2]"
                                                                       id="three"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[4]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[3]" id="four"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[3]" id="four"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[3]" id="four"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[5]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[4]" id="five"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[4]" id="five"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[4]" id="five"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[6]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[5]" id="six"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[5]" id="six"
                                                                       value="NO" checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[5]" id="six"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[7]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[6]"
                                                                       id="seven"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[6]"
                                                                       id="seven"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[6]"
                                                                       id="seven"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[8]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[7]"
                                                                       id="eight"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[7]"
                                                                       id="eight"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[7]"
                                                                       id="eight"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[9]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[8]" id="nine"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[8]" id="nine"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[8]" id="nine"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[10]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[9]" id="ten"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[9]" id="ten"
                                                                       value="NO" checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[9]" id="ten"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $questions[11]->title_for_sc_sm }}</td>
                                                    <td>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[10]"
                                                                       id="eleven"
                                                                       value="YES">
                                                                &nbsp;&nbsp;Yes
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[10]"
                                                                       id="eleven"
                                                                       value="NO"
                                                                       checked>
                                                                &nbsp;&nbsp;No
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 floting">
                                                            <div>
                                                                <input type="radio" name="scheme_questions[10]"
                                                                       id="eleven"
                                                                       value="N/A">
                                                                &nbsp;&nbsp;N/A
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @if($audit_type == 'Stage 1')
                                                    <tr class="">
                                                        <td>{{ $questions[12]->title_for_sc_sm }}</td>
                                                        <td>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[11]"
                                                                           id="twelve"
                                                                           class=""
                                                                           value="YES">
                                                                    &nbsp;&nbsp;Yes
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[11]"
                                                                           id="twelve"
                                                                           class=""
                                                                           value="NO" checked>
                                                                    &nbsp;&nbsp;No
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[11]"
                                                                           id="twelve"
                                                                           class=""
                                                                           value="N/A">
                                                                    &nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if($audit_type != 'Stage 1')
                                                    <tr class="">
                                                        <td>{{ $questions[13]->title_for_sc_sm }}</td>
                                                        <td>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[12]"
                                                                           id="thirteen"
                                                                           class=""
                                                                           value="YES"> &nbsp;&nbsp;Yes
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[12]"
                                                                           id="thirteen"
                                                                           class=""
                                                                           value="NO" checked> &nbsp;&nbsp;NO
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 floting">
                                                                <div>
                                                                    <input type="radio" name="scheme_questions[12]"
                                                                           id="thirteen"
                                                                           class=""
                                                                           value="N/A"> &nbsp;&nbsp;N/A
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @if($audit_type != 'Stage 2')
                                                        <tr class="">
                                                            <td>{{ $questions[14]->title_for_sc_sm }}</td>
                                                            <td>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[13]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="YES" checked>
                                                                        &nbsp;&nbsp;Yes
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[13]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="NO">
                                                                        &nbsp;&nbsp;No
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[13]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="N/A">
                                                                        &nbsp;&nbsp;N/A
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @if($standard->name == 'ISO 45001:2018' || $standard->name == 'OHSAS 18001:2007')
                                                        <tr class="">
                                                            <td style="width: 78%;">{{ $questions[16]->title_for_sc_sm }}</td>
                                                            <td>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[14]"
                                                                               id="twelve"
                                                                               checked
                                                                               class=""
                                                                               value="YES">
                                                                        &nbsp;&nbsp;Yes
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[14]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="NO">
                                                                        &nbsp;&nbsp;No
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[14]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="N/A">
                                                                        &nbsp;&nbsp;N/A
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @if($audit_type == 'Reaudit')
                                                        <tr class="">
                                                            <td>{{ $questions[15]->title_for_sc_sm }}</td>
                                                            <td>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[15]"
                                                                               id="twelve"
                                                                               class=""
                                                                               checked
                                                                               value="YES">
                                                                        &nbsp;&nbsp;Yes
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[15]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="NO">
                                                                        &nbsp;&nbsp;No
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 floting">
                                                                    <div>
                                                                        <input type="radio" name="scheme_questions[15]"
                                                                               id="twelve"
                                                                               class=""
                                                                               value="N/A">
                                                                        &nbsp;&nbsp;N/A
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endif
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>

                                <!-- /.card-body -->
                                <!-- /.card-footer -->
                            </div>
                            @if(auth()->user()->user_type == 'scheme_manager')

                                <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                    <div class="col-md-3">
                                        <div class="col-md-5 floting">
                                            <label for="packed_closed">Pack Closed:</label>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div>
                                                <input type="radio" name="pack_closed" id="packed_closed"
                                                       value="TRUE" {{ (isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->pack_closed == 'YES') ? 'checked' :'' }}>&nbsp;&nbsp;
                                                YES
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div>
                                                @if(isset($postAudit->postAuditSchemeInfo))
                                                    <input type="radio" name="pack_closed" id="packed_closed"
                                                           value="FALSE" {{ (($postAudit->postAuditSchemeInfo->pack_closed == 'NO')) ? 'checked' :'' }}>
                                                    &nbsp;&nbsp;
                                                    NO
                                                @else
                                                    <input type="radio" name="pack_closed" id="packed_closed"
                                                           value="FALSE" checked>&nbsp;&nbsp;
                                                    NO
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Current Status: <span
                                                    id="new_status">{{ (isset($companyStandard->certificates)) ? str_replace('_',' ',ucfirst($companyStandard->certificates->certificate_status)) : 'Not Certified' }}</span>
                                            @if($audit_type == 'Stage 1')
                                            @else
                                                <a href="#"
                                                   onclick="certificate('{{$companyStandard->id}}')"><i
                                                            class="fa fa-pencil"></i></a>
                                            @endif
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                @php
                                    if(isset($getTranferStageInfo)){

                                        $issue_date = Carbon\Carbon::parse($getTranferStageInfo->old_cb_certificate_issue_date);
                                        $expiry_date = Carbon\Carbon::parse($getTranferStageInfo->old_cb_certificate_expiry_date);
                                        $diff = $expiry_date->diffInYears($issue_date);
                                    }

                                @endphp

                                @if(isset($postAudit->postAuditSchemeInfo))
                                    <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                        <div class="col-md-4 floting packedClosedNew"
                                             style="display: {{($postAudit->postAuditSchemeInfo->pack_closed == 'YES') ? 'block' : 'none'}}">
                                            <div class="col-md-12">
                                                <label for="approval_date">Approval / Issue
                                                    Date @if(auth()->user()->user_type == 'scheme_manager')
                                                        <i class="fa fa-copy"
                                                           id="copyApprovalDate"></i>
                                                    @endif</label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">  <i
                                                                class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="approval_date" id="approval_date"
                                                           value="{{(isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->approval_date) ?  date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->approval_date)):'' }}"
                                                           class="form-control float-right active approval_date"
                                                           placeholder="dd//mm/yyyy">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting  packedClosed"
                                             style="display: {{(($postAudit->postAuditSchemeInfo->pack_closed == 'YES') && $audit_type != 'Stage 1') ? 'block' : 'none'}}">
                                            <label for="certificate_validity">Certificate Validity:</label>
                                            <div class="col-md-12 floting">


                                                @php
                                                    if(isset($getTranferStageInfo)){
                                                        $issue_date = Carbon\Carbon::parse($getTranferStageInfo->old_cb_certificate_issue_date);
                                                        $expiry_date = Carbon\Carbon::parse($getTranferStageInfo->old_cb_certificate_expiry_date);
                                                        $diff = $expiry_date->diffInYears($issue_date);
                                                    }


                                                @endphp


                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="certificate_validity"
                                                               @if($audit_type == 'Reaudit')
                                                               @elseif(preg_match('/Scope-Extension/', $audit_type) || preg_match('/Special-Transition/', $audit_type))
                                                                   disabled
                                                               @else
                                                                   {{ (isset($getschemeinfo) && $getschemeinfo->certificate_validity == '3 Years') ? 'disabled' :'' }}
                                                                   {{(isset($getTranferStageInfo) && $diff == 3) ? 'disabled' :'' }}
                                                               @endif


                                                               id="certificate_validity" value="1 Year">&nbsp;&nbsp;
                                                        1 Year
                                                    </div>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="certificate_validity"
                                                               @if($audit_type == 'Reaudit')
                                                               @elseif(preg_match('/Scope-Extension/', $audit_type) || preg_match('/Special-Transition/', $audit_type))
                                                                   disabled
                                                               @else
                                                                   {{(isset($getschemeinfo) && $getschemeinfo->certificate_validity == '1 Year') ? 'disabled' :'' }}
                                                                   {{(isset($getTranferStageInfo) && $diff == 1) ? 'disabled' :'' }}
                                                                   {{ (isset($getschemeinfo) && $getschemeinfo->certificate_validity == '3 Years') ? 'disabled' :'' }}
                                                                   {{(isset($getTranferStageInfo) && $diff == 3) ? 'disabled' :'' }}
                                                               @endif

                                                               id="certificate_validity" value="3 Years">&nbsp;&nbsp;
                                                        3 Years
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting  packedClosed"
                                             style="display: {{(($postAudit->postAuditSchemeInfo->pack_closed == 'YES') && $audit_type != 'Stage 1') ? 'block' : 'none'}}">
                                            <label>Issue History:</label>
                                            <div class="col-md-12 floting">
                                                <div class="col-md-6 floting">
                                                    <label>Original Issue Date <br> <span
                                                                id="txtOriginalIssueDate">
                                                            @if(isset($getschemeinfo))
                                                                {{(isset($getschemeinfo) && $getschemeinfo->original_issue_date) ?  date("d-m-Y", strtotime($getschemeinfo->original_issue_date)):'' }}
                                                            @elseif(isset($getTranferStageInfo))
                                                                {{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_issue_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_issue_date)):'' }}
                                                            @endif


                                                        </span></label>
                                                    <input type="hidden" id="hiddenOriginalIssueDate"
                                                           name="original_issue_date"
                                                           @if(isset($getschemeinfo))
                                                               value="{{(isset($getschemeinfo) && $getschemeinfo->original_issue_date) ?  date("d-m-Y", strtotime($getschemeinfo->original_issue_date)):'' }}"
                                                           @elseif(isset($getTranferStageInfo))
                                                               value="{{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_issue_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_issue_date)):'' }}"
                                                           @else
                                                               value=""
                                                            @endif


                                                    />
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <label>Last Expiry Date <br>
                                                        <span id="txtLastExpiryDate">

                                                            @if(isset($getschemeinfo))
                                                                {{(isset($getschemeinfo) && $getschemeinfo->new_expiry_date) ?  date("d-m-Y", strtotime($getschemeinfo->new_expiry_date)):'' }}
                                                            @elseif(isset($getTranferStageInfo))
                                                                {{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_expiry_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_expiry_date)):'' }}
                                                            @endif
                                                       </span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                        <div class="col-md-4 floting packedClosedNew"
                                             style="display: {{($postAudit->postAuditSchemeInfo->pack_closed == 'YES') ? 'block' : 'none'}}">
                                            <div class="col-md-12">
                                                <label for="last_certificate_issue_date">Last Certificate Issue
                                                    Date </label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">  <i
                                                                class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="last_certificate_issue_date"
                                                           id="last_certificate_issue_date"
                                                           value="{{(isset($companyStandard) && $companyStandard->last_certificate_issue_date) ? date("d-m-Y", strtotime($companyStandard->last_certificate_issue_date)) :'' }}"
                                                           class="form-control float-right active last_certificate_issue_date  @if(auth()->user()->user_type != 'scheme_manager') disabledDiv @endif"
                                                           placeholder="dd//mm/yyyy">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row auditJustificationTable mrgn popupLbl packedClosed {{ $disable }}"
                                         style="display: {{(($postAudit->postAuditSchemeInfo->pack_closed == 'YES') && $audit_type != 'Stage 1') ? 'block' : 'none'}}">
                                        <div class="col-md-3 floting">
                                            <div class="col-md-12">
                                                <label for="print_required">Printout Required:</label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="col-md-4 floting">
                                                    <div>
                                                        <input type="radio" name="print_required" id="print_required"
                                                               {{ (isset($postAudit->postAuditSchemeInfo) &&  $postAudit->postAuditSchemeInfo->print_required == 'YES') ? 'checked' :'' }}
                                                               value="TRUE">&nbsp;&nbsp;
                                                        YES
                                                    </div>
                                                </div>
                                                <div class="col-md-4 floting">
                                                    <div>
                                                        <input type="radio" name="print_required" id="print_required"
                                                               {{ (isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->print_required == 'NO') ? 'checked' :'' }}
                                                               value="FALSE">&nbsp;&nbsp;
                                                        NO
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting printAssignTo"
                                             style="display: {{ (isset($postAudit->postAuditSchemeInfo) &&  $postAudit->postAuditSchemeInfo->print_required == 'YES') ? 'block' :'none' }};">
                                            <label for="print_assigned_to">Printing Assign To:</label>
                                            <div class="col-md-12 floting">
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="print_assigned_to"
                                                               id="print_assigned_to"
                                                               {{ (isset($postAudit->postAuditSchemeInfo) &&  $postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_manager') ? 'checked' :'' }}
                                                               value="scheme_manager">&nbsp;&nbsp;
                                                        Scheme Manager
                                                    </div>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="print_assigned_to"
                                                               id="print_assigned_to"
                                                               {{ (isset($postAudit->postAuditSchemeInfo) &&  $postAudit->postAuditSchemeInfo->print_assigned_to == 'scheme_coordinator') ? 'checked' :'' }}
                                                               value="scheme_coordinator">&nbsp;&nbsp;
                                                        Scheme Coordinator
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="col-md-6">
                                                <label for="new_expiry_date">New Expiry Date </label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                    class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="new_expiry_date" id="new_expiry_date"
                                                           placeholder="dd//mm/yyyy"

                                                           @if(isset($getschemeinfo))
                                                               value="{{(isset($postAudit->postAuditSchemeInfo) &&  $postAudit->postAuditSchemeInfo->new_expiry_date) ?  date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->new_expiry_date)):'' }}"
                                                           @elseif(isset($getTranferStageInfo))
                                                               value="{{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_expiry_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_expiry_date)):'' }}"
                                                           @endif

                                                           class="form-control float-right active new_expiry_date">
                                                </div>
                                                <span class="expiry_date_error"></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @else
                                    <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                        <div class="col-md-4 floting packedClosedNew" style="display: none">
                                            <div class="col-md-12">
                                                <label for="approval_date">Approval / Issue
                                                    Date @if(auth()->user()->user_type == 'scheme_manager')
                                                        <i class="fa fa-copy"
                                                           id="copyApprovalDate"></i>
                                                    @endif</label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">  <i
                                                                class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="approval_date" id="approval_date"
                                                           value=""
                                                           class="form-control float-right active approval_date"
                                                           placeholder="dd//mm/yyyy">
                                                    <span class="approval_date_error"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting packedClosed" style="display: none">
                                            <label for="certificate_validity"
                                                   style="display: @if($audit_type == 'Reaudit' || $audit_type == 'Surveillance 1' || $audit_type == 'Surveillance 2' || $audit_type == 'Surveillance 3' || $audit_type == 'Surveillance 4' || $audit_type == 'Surveillance 5') none @endif">Certificate
                                                Validity:</label>


                                            @php
                                                if(isset($getTranferStageInfo)){
                                                    $issue_date = Carbon\Carbon::parse($getTranferStageInfo->old_cb_certificate_issue_date);
                                                    $expiry_date = Carbon\Carbon::parse($getTranferStageInfo->old_cb_certificate_expiry_date);
                                                    $diff = $expiry_date->diffInYears($issue_date);
                                                }


                                            @endphp


                                            <div class="col-md-12 floting"
                                                 style="display: @if($audit_type == 'Reaudit' || $audit_type == 'Surveillance 1' || $audit_type == 'Surveillance 2' || $audit_type == 'Surveillance 3' || $audit_type == 'Surveillance 4' || $audit_type == 'Surveillance 5') none @endif">
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="certificate_validity"
                                                               id="certificate_validity"
                                                               value="1 Year"
                                                               @if($audit_type == 'Reaudit')
                                                               @elseif(preg_match('/Scope-Extension/', $audit_type) || preg_match('/Special-Transition/', $audit_type))
                                                                   disabled
                                                        @else
                                                            {{(isset($getschemeinfo) && $getschemeinfo->certificate_validity == '3 Years') ? 'disabled' :'' }}
                                                                    {{(isset($getTranferStageInfo) && $diff == 3) ? 'disabled' :'' }}
                                                                @endif

                                                        >&nbsp;&nbsp;
                                                        1 Year
                                                    </div>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="certificate_validity"
                                                               id="certificate_validity"
                                                               value="3 Years"
                                                               @if($audit_type == 'Reaudit')
                                                               @elseif(preg_match('/Scope-Extension/', $audit_type) || preg_match('/Special-Transition/', $audit_type))
                                                                   disabled
                                                        @else
                                                            {{(isset($getschemeinfo) && $getschemeinfo->certificate_validity == '1 Year') ? 'disabled' :'' }}
                                                                    {{(isset($getTranferStageInfo) && $diff == 1) ? 'disabled' :'' }}
                                                                    {{ (isset($getschemeinfo) && $getschemeinfo->certificate_validity == '3 Years') ? 'disabled' :'' }}
                                                                    {{(isset($getTranferStageInfo) && $diff == 3) ? 'disabled' :'' }}
                                                                @endif

                                                        >&nbsp;&nbsp;
                                                        3 Years
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting  packedClosed" style="display: none;">
                                            <label>Issue History:</label>
                                            <div class="col-md-12 floting">
                                                <div class="col-md-6 floting">
                                                    <label>Original Issue Date <br> <span
                                                                id="txtOriginalIssueDate">

                                                            @if(isset($getschemeinfo))
                                                                {{(isset($getschemeinfo) && $getschemeinfo->original_issue_date) ?  date("d-m-Y", strtotime($getschemeinfo->original_issue_date)):'' }}
                                                            @elseif(isset($getTranferStageInfo))
                                                                {{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_issue_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_issue_date)):'' }}
                                                            @endif



                                                        </span></label>
                                                    <input type="hidden" id="hiddenOriginalIssueDate"
                                                           name="original_issue_date"

                                                           @if(isset($getschemeinfo))
                                                               value="{{(isset($getschemeinfo) && $getschemeinfo->original_issue_date) ?  date("d-m-Y", strtotime($getschemeinfo->original_issue_date)):'' }}"
                                                           @elseif(isset($getTranferStageInfo))
                                                               value="{{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_issue_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_issue_date)):'' }}"
                                                           @else
                                                               value=""
                                                            @endif

                                                    />
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <label>Last Expiry Date <br> <span
                                                                id="txtLastExpiryDate">
                                                            @if(isset($getschemeinfo))
                                                                {{(isset($getschemeinfo) && $getschemeinfo->new_expiry_date) ?  date("d-m-Y", strtotime($getschemeinfo->new_expiry_date)):'' }}
                                                            @elseif(isset($getTranferStageInfo))
                                                                {{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_expiry_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_expiry_date)):'' }}
                                                            @endif

                                                     </span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">
                                        <div class="col-md-4 floting packedClosedNew"
                                             style="display: none">
                                            <div class="col-md-12">
                                                <label for="last_certificate_issue_date">Last Certificate Issue
                                                    Date </label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">  <i
                                                                class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="last_certificate_issue_date"
                                                           id="last_certificate_issue_date"
                                                           value="{{(isset($companyStandard) && $companyStandard->last_certificate_issue_date) ?  date("d-m-Y", strtotime($companyStandard->last_certificate_issue_date)):'' }}"
                                                           class="form-control float-right active last_certificate_issue_date @if(auth()->user()->user_type != 'scheme_manager') disabledDiv @endif"
                                                           placeholder="dd//mm/yyyy">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row auditJustificationTable mrgn popupLbl packedClosed {{ $disable }}"
                                         style="display: none">
                                        <div class="col-md-3 floting">
                                            <div class="col-md-12">
                                                <label for="print_required">Printout Required:</label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="col-md-4 floting">
                                                    <div>
                                                        <input type="radio" name="print_required"
                                                               id="print_required"
                                                               value="TRUE">&nbsp;&nbsp;
                                                        YES
                                                    </div>
                                                </div>
                                                <div class="col-md-4 floting">
                                                    <div>
                                                        <input type="radio" name="print_required"
                                                               id="print_required"
                                                               value="FALSE" checked>&nbsp;&nbsp;
                                                        NO
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting printAssignTo" style="display: none;">
                                            <label for="print_assigned_to">Printing Assign To:</label>
                                            <div class="col-md-12 floting">
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="print_assigned_to"
                                                               id="print_assigned_to"
                                                               value="scheme_manager">&nbsp;&nbsp;
                                                        Scheme Manager
                                                    </div>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <div>
                                                        <input type="radio" name="print_assigned_to"
                                                               id="print_assigned_to"
                                                               value="scheme_coordinator" checked>&nbsp;&nbsp;
                                                        Scheme Coordinator
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 floting">
                                            <div class="col-md-6">
                                                <label for="new_expiry_date">New Expiry Date </label>
                                            </div>
                                            <div class="col-md-12 floting">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                    class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="new_expiry_date" id="new_expiry_date"
                                                           placeholder="dd//mm/yyyy"

                                                           @if(isset($getschemeinfo))
                                                               value="{{(isset($getschemeinfo) && $getschemeinfo->new_expiry_date) ?  date("d-m-Y", strtotime($getschemeinfo->new_expiry_date)):'' }}"
                                                           @elseif(isset($getTranferStageInfo))
                                                               value="{{(isset($getTranferStageInfo) && $getTranferStageInfo->old_cb_certificate_expiry_date) ?  date("d-m-Y", strtotime($getTranferStageInfo->old_cb_certificate_expiry_date)):'' }}"
                                                           @endif

                                                           class="form-control float-right active new_expiry_date">
                                                </div>
                                            </div>

                                            <span class="expiry_date_error"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endif
                            @endif

                            @if(auth()->user()->user_type == 'scheme_manager' && strtolower(str_replace(' ','_',$standard->standardFamily->name)) == 'information_security')
                                <div class="row">
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="SOA">SOA Version</label>
                                            <input type="text" class="form-control" name="soa_version" id="soa_version"
                                                   value="{{$companyStandard->SOA}}"
                                                   placeholder="Enter SOA">
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="row auditJustificationTable mrgn popupLbl {{ $disable }}">

                                <div class="clearfix"></div>
                            </div>
                            <div class="row {{ $disable }}">
                                <div class="col-md-12 floting">
                                    <div class="form-group">
                                        <label for="remarks">Notification Remarks *</label>
                                        <textarea rows="1" class="form-control" name="remarks" id="remarks"></textarea>
                                        <span class="remarks_error"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="display: {{auth()->user()->user_type === 'scheme_manager' ? '' : 'none'}}">
                                <div class="col-md-12 floting">
                                    <div class="form-group">
                                        <input type="checkbox" name="key_change" id="key_change"
                                               style="width: 20px;height: 20px;"
                                               value="{{$postAudit->key_change === 1 || $postAudit->key_change === '1' ? 1 : 0}}" {{ $postAudit->key_change === 1 || $postAudit->key_change === '1' ? 'checked' : ''}} />
                                        <label for="remarks">Key Change</label>
                                    </div>
                                </div>
                                <div class="col-md-12 floting enableKeyChangeRemarks"
                                     style="display: {{ $postAudit->key_change === 1 || $postAudit->key_change === '1' ? '' : 'none'}}">
                                    <div class="form-group">
                                        <label for="remarks">Key Change Remarks</label>
                                        <textarea rows="1" class="form-control" name="key_remarks"
                                                  id="key_remarks">{{ $postAudit->key_remarks }}</textarea>
                                        <span class="remarks_error"></span>
                                    </div>
                                </div>
                            </div>
                            @if($isComments)
                                <div class="row">
                                    <div class="col-md-12 floting">
                                        <div class="form-group">
                                            <label for="comments">Pack review comments</label>
                                            <textarea rows="3" class="form-control" name="comments"
                                                      @if(auth()->user()->user_type !== 'scheme_manager' && auth()->user()->user_type !== 'scheme_coordinator') readonly
                                                      @endif
                                                      id="comments">{{ $postAudit->comments }}</textarea>

                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">

                                <div class="col-md-4"></div>
                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator')
                                    @if(!empty($notifications) && count($notifications) > 0)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Comments Box</label>
                                                <div class="card-body chat_box" style="display: block;">
                                                    <div class="direct-chat-messages">


                                                        @foreach($notifications as $key=>$notification)

                                                            @if($key ==0)
                                                                @if($notification->sent_by === Auth::user()->id )
                                                                    <div class="direct-chat-msg">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                    </div>

                                                                @else
                                                                    <div class="direct-chat-msg text-right">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                    </div>
                                                                @endif
                                                            @elseif($key > 0 && $notifications[$key-1]->status != $notifications[$key]->status )
                                                                @if($notification->sent_by === Auth::user()->id )
                                                                    <div class="direct-chat-msg">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                    </div>

                                                                @else
                                                                    <div class="direct-chat-msg text-right">
                                                                        <div class="direct-chat-info clearfix">
                                                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                        </div>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                        <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                <div class="col-md-4"></div>


                            </div>
                            @if(auth()->user()->user_type === 'scheme_manager' && !is_null($stage1PostAudit) && $stage1PostAudit->status !== 'approved')
                                <p style="font-weight: bold;font-size: 17px;color: red; text-align: center;">Please
                                    approved stage 1 post audit task after then you are able to approved post audit for
                                    stage 2</p>
                            @else
                                <div class="col-md-12 mrgn no_padding {{ $disable }}">
                                    <div class="col-md-4 floting "></div>
                                    <div class="col-md-4 floting"></div>

                                    @if(auth()->user()->user_type == 'scheme_coordinator')
                                        @if($postAudit->status == 'rejected')
                                            <div class="col-md-2 floting">

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn-success btn_save"
                                                            data-status="resent">Resent
                                                        &amp;
                                                        Forward to SM
                                                    </button>
                                                </div>
                                            </div>
                                        @elseif($postAudit->status == 'approved')
                                        @elseif($postAudit->status == 'accepted')
                                        @else
                                            <div class="col-md-2 floting no_padding rejectedStatus">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn_search"
                                                            data-status="rejected">
                                                        Rejected
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting saveButtonAccept">

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn-success btn_save"
                                                            data-status="accepted">Accept
                                                        &amp;
                                                        Forward to SM
                                                    </button>
                                                </div>
                                            </div>

                                        @endif
                                    @endif
                                    @if(auth()->user()->user_type == 'scheme_manager')
                                        @if($postAudit->status == 'approved')
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn-success btn_save"
                                                            data-status="save">Save Only
                                                    </button>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-2 floting approvedButton"
                                                 style="display: {{((isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->pack_closed == 'YES')) ? 'block' : 'none'}}">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn-success btn_save"
                                                            data-status="approved">Approved
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting no_padding rejectedStatus">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn_search"
                                                            data-status="rejected">
                                                        Rejected
                                                    </button>
                                                </div>
                                            </div>
                                        @endif
                                    @endif


                                </div>
                            @endif

                        </div>


                        <!-- /.card-body -->


                    </form>

                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>

    <div id="certificate_html"></div>
    @if(!is_null($companyStandard->version_change))
        <div class="modal fade" id="viewModal" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel2"
             aria-hidden="true">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-eye"></i>Transfer Case History</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable no-footer"
                                           role="grid" aria-describedby="example1_info">
                                        <thead>
                                        <tr role="row green_row">
                                            <th>Audit Type</th>
                                            <th>Old Standard Version</th>
                                            <th>Upgraded to next Version On</th>
                                        </tr>
                                        </thead>
                                        <tbody id="myTable">


                                        @if(!is_null($companyStandard->version_change))
                                            @php
                                                $versionChangeHistories =  json_decode($companyStandard->version_change,true);
                                            @endphp
                                            @foreach($versionChangeHistories as $history)
                                                @php
                                                    $auditTypeHistory = \App\AJStandardStage::whereId($history['old_aj_standard_stage_id'])->first(['audit_type']);
                                                    $ajStandardHistory  = \App\AJStandard::whereId($history['old_aj_standard_id'])->with('standard:id,name')->first(['id','standard_id']);


                                                @endphp
                                                <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                    <td>{{!is_null($auditTypeHistory) ? $auditTypeHistory->getAuditType() : '--'}}</td>
                                                    <td>{{!is_null($ajStandardHistory) ? $ajStandardHistory->standard->name : '--'}}</td>
                                                    <td>{{$history['change_date'] ? date('d-m-Y',strtotime($history['change_date'])) : '--'}}</td>
                                                </tr>
                                            @endforeach

                                        @else
                                            <tr class='notfound'>
                                                <td colspan='3'>No record found</td>
                                            </tr>
                                        @endif


                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    @endif
@endsection
@push('models')

@endpush
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        @if(auth()->user()->user_type == 'scheme_manager')
        var approvalDate = '{{$standardStage->approval_date}}';
        //Date range picker
        $('.actual_audit_date').daterangepicker({
            format: "DD-MM-YYYY",
        }).on('apply.daterangepicker', function (ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            var dateAr = this.value.split('-');
            var newDate = startDate.format('YYYY-MM-DD');
            var newApprovalDate = moment(approvalDate).format('YYYY-MM-DD');
            var newActualAuditDate = moment(newDate).format('YYYY-MM-DD');
            var duration = moment.duration(endDate.diff(startDate));
            var days = duration.asDays();


            if (days > 20) {
                toastr['error']('Date From and To difference should not be > 20 days');
                $('#actual_date').val('');
            } else {
                if (newActualAuditDate > newApprovalDate) {
                } else if (newActualAuditDate < newApprovalDate) {
                    toastr['error']('Actual Audit Date always greater than AJ Approval Date');
                    $('#actual_date').val('');
                } else {
                    toastr['error']('Actual Audit Date always greater than AJ Approval Date');
                    $('#actual_date').val('');
                }
            }
        });
        @endif

        var audit_type = "{{$audit_type}}";
        $('.new_expiry_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.last_certificate_issue_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        var actual_audit_date = '{{$postAudit->actual_audit_date_to}}';
        var dateCheck = false;

        $('.approval_date').datepicker({
            format: 'dd-mm-yyyy',
        }).on("changeDate", function () {
            var dateAr = this.value.split('-');
            var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
            var newApprovalDate = moment(actual_audit_date).format('YYYY-MM-DD');
            var newActualAuditDate = moment(newDate).format('YYYY-MM-DD');
            if (newActualAuditDate > newApprovalDate) {
                var date = new Date();
                var momentDate = moment(date).format('YYYY-MM-DD');

                dateCheck = true;
                if (audit_type != "Stage 2") {


                    if ($("#hiddenOriginalIssueDate").val() == '') {
                        $("#hiddenOriginalIssueDate").val(this.value);
                    }
                } else {
                    // $("#txtOriginalIssueDate").text('');
                    // $("#txtLastExpiryDate").text('');
                    // $("#txtOriginalIssueDate").text(this.value);
                    $("#hiddenOriginalIssueDate").val(this.value);
                    // var newExpiryDate = moment(this.value, "DD-MM-YYYY").add('years', 3).format('DD-MM-YYYY');
                    // var newExpiryDate1 = moment(newExpiryDate, "DD-MM-YYYY").subtract(1, 'days').format('DD-MM-YYYY');
                    // $("#txtLastExpiryDate").text(newExpiryDate1);
                    // $("#new_expiry_date").val(newExpiryDate1);
                }


                // if (newActualAuditDate > momentDate) {
                //     // dateCheck = true;
                //     // if (audit_type != "Stage 2") {
                //     //
                //     //
                //     //     if ($("#hiddenOriginalIssueDate").val() == '') {
                //     //         $("#hiddenOriginalIssueDate").val(this.value);
                //     //     }
                //     // } else {
                //     //     // $("#txtOriginalIssueDate").text('');
                //     //     // $("#txtLastExpiryDate").text('');
                //     //     // $("#txtOriginalIssueDate").text(this.value);
                //     //     $("#hiddenOriginalIssueDate").val(this.value);
                //     //     // var newExpiryDate = moment(this.value, "DD-MM-YYYY").add('years', 3).format('DD-MM-YYYY');
                //     //     // var newExpiryDate1 = moment(newExpiryDate, "DD-MM-YYYY").subtract(1, 'days').format('DD-MM-YYYY');
                //     //     // $("#txtLastExpiryDate").text(newExpiryDate1);
                //     //     // $("#new_expiry_date").val(newExpiryDate1);
                //     // }
                // } else {
                //     toastr['error']('Approval/Issue Date always greater than Actual Audit Date');
                //     $('#approval_date').val('');
                //     dateCheck = false;
                // }
            } else if (newActualAuditDate < newApprovalDate) {
                toastr['error']('Approval/Issue Date always greater than Actual Audit Date');
                $('#approval_date').val('');
                dateCheck = false;
            } else {
                toastr['error']('Approval/Issue Date always greater than Actual Audit Date');
                $('#approval_date').val('');
                dateCheck = false;
            }


        });

        $('#copyApprovalDate').on('click', function (e) {
            e.preventDefault();
            $('#last_certificate_issue_date').val($('#approval_date').val());
            return false;

        });

        var questionOneCount = {{(isset($postAuditQuestionsOne)) ? count($postAuditQuestionsOne->postAuditQuestionFile) : 0}};
        var questionTwoCount = {{(isset($postAuditQuestionsTwo)) ?count($postAuditQuestionsTwo->postAuditQuestionFile) : 0}};
        var questionThreeCount = {{(isset($postAuditQuestionsThree)) ? count($postAuditQuestionsThree->postAuditQuestionFile) :0}};
        var questionFourCount = {{(isset($postAudit->is_q4) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES' && !is_null($postAudit->q4_file)) ? 1 : 0}};
        var verificationCount = {{$approvedVerificationCount}};


        function deleteQuestionOneFile(id, fileId) {
            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": fileId, 'type': 'single'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {
                            $('.upload_question_files1_' + id).remove();
                            questionOneCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }


        }

        function deleteQuestionTwoFile(id, fileId) {
            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": fileId, 'type': 'single'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {

                            $('.upload_question_files2_' + id).remove();
                            questionTwoCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }


        }

        function deleteQuestionThreeFile(id, fileId) {

            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": fileId, 'type': 'single'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {

                            $('.upload_question_files3_' + id).remove();
                            questionThreeCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }

        $("input[name='q4_name']").click(function () {
            var radioValue = $("input[name='q4_name']:checked").val();
            if (radioValue === 'YES') {
                $('.question_4').show();
            } else {
                $('.question_four_filename_0_error').text('');
                $('.question_4').hide();
            }
        });

        function deleteQuestionFourFile(id) {

            if (confirm('Are you sure you want to delete this file')) {
                var request = {"file_id": id, 'type': 'postAudit'};
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.delete.post.audit.question') }}",
                    data: request,
                    success: function (response) {
                        if (response.status === "success") {

                            $('.upload_question_files4_1').remove();
                            questionFourCount--;
                            toastr['success']("File Deleted Successfully.");

                        } else {
                            toastr['error']("No File Found.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }


        function getBase64(file, index, id) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                console.log(reader.result);
                $('#' + id + '' + index).val(reader.result);
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }

        var certificateStatus = '{{ (isset($companyStandard->certificates)) ? $companyStandard->certificates->certificate_status : 'notcertified' }}';

        function validate(e) {

            var returnFunction = false;
            $(':input[type="submit"]').prop('disabled', true);
            $('#loader').show();

            if (e.submitter.dataset.status == 'rejected') {
                $('#status').val('rejected');
            } else if (e.submitter.dataset.status == 'save') {
                $('#status').val('save');
            } else if (e.submitter.dataset.status == 'accepted') {
                $('#status').val('accepted');
            } else if (e.submitter.dataset.status == 'approved') {
                $('#status').val('approved');
            } else if (e.submitter.dataset.status == 'resent') {
                $('#status').val('resent');

            }

            var response = true;
            var response1 = false;
            var response2 = false;
            var response3 = false;
            var response4 = false;
            var response5 = false;
            var response6 = false;
            var response7 = false;


            if (e.submitter.dataset.status == 'rejected') {
                response = true;
            } else {
                $('input[type="radio"]:checked').each(function () {

                    if ((this.value == 'NO') && this.className == "") {

                        if (e.submitter.dataset.status == 'accepted' || e.submitter.dataset.status == 'resent') {
                            toastr['error']('The value of questions can not be No or N/A.Please recheck.'); //toaster message issue
                        } else {
                            toastr['error']('Please Fill all checklist.'); //toaster message issue
                        }
                        response = false;
                        return false;

                    } else {
                        response = true;
                    }
                });
            }


            if ($('#remarks').val() == '') {
                response1 = false;
                $('.remarks_error').text('Comments missing');
            } else {
                response1 = true;
                $('.remarks_error').text('');
            }

            if ($("input[name='pack_closed']:checked").val() == 'TRUE') {
                if ($('#approval_date').val() == '') {
                    $('.approval_date_error').text('Approval Date is required');
                } else {
                    $('.approval_date_error').text('');
                }
            } else {
                dateCheck = true;
            }
            if (questionThreeCount === 0) {
                $('.question_three_filename_0_error').text('Audit Pack is required');
                response2 = false;
            } else {
                $('.question_three_filename_0_error').text('');
                response2 = true;
            }

            @if(!is_null($postAudit->q4_file) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES')
            if (questionFourCount === 0) {
                $('.question_four_filename_0_error').text('File is required');
                response6 = false;
            } else {
                $('.question_four_filename_0_error').text('');
                response6 = true;
            }
            @else
                response6 = true;
            @endif

            if ($('#audit_type').val() == 'Stage 1') {
                response3 = true;
                response4 = true;
            } else {

                var q2 = false;

                @if(isset($postAuditQuestionsTwo))
                        @if($postAuditQuestionsTwo->value == 'YES')
                    q2 = true;
                @else
                    q2 = false;
                @endif
                        @else
                    q2 = false;
                @endif
                if (q2 === true && questionTwoCount === 0) {
                    $('.question_two_filename_0_error').text('CAR upload is mandatory');
                    response3 = false;
                } else {
                    $('.question_two_filename_0_error').text('');
                    response3 = true;
                }


                var q1 = false;

                @if(isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES')
                    q1 = true;
                @else
                    q1 = false;
                @endif
                if (q1 === true && questionOneCount === 0) {
                    $('.question_ones_filename_0_error').text('Question no 1 file is required.');
                    response4 = false;
                } else {
                    $('.question_ones_filename_0_error').text('');
                    response4 = true;
                }
            }




            @if(auth()->user()->user_type === 'scheme_manager')


            if (audit_type !== 'Stage 1' && $("input[name='print_required']:checked").val() == 'TRUE') {
                var dateAr = $('#approval_date').val().split('-');
                var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
                var approvalDate = moment(newDate).format('YYYY-MM-DD');
                var dateArExp = $('#new_expiry_date').val().split('-');
                var newDateExpiry = dateArExp[2] + '-' + dateArExp[1] + '-' + dateArExp[0];
                var expiryDate = moment(newDateExpiry).format('YYYY-MM-DD');

                var start = moment(approvalDate, "YYYY-MM-DD");
                var end = moment(newDateExpiry, "YYYY-MM-DD");
                var daysDifference = Math.abs(moment.duration(start.diff(end)).asMonths());
                if (expiryDate > approvalDate && daysDifference >= 2.4) {
                    response5 = true;
                    $('.expiry_date_error').text('');
                } else {
                    response5 = false;
                    $('.expiry_date_error').text('Expiry Date should be greater than Approval Date.');
                }
            } else {
                response5 = true;
                $('.expiry_date_error').text('');
            }
            @else
            $('.expiry_date_error').text('');
            response5 = true;
            @endif

            @if(auth()->user()->user_type === 'scheme_manager')

            if (audit_type !== 'Stage 1' && e.submitter.dataset.status === 'approved' && ($('#last_certificate_issue_date').val() === '' || $('#last_certificate_issue_date').val() === null || $('#last_certificate_issue_date').val() === undefined)) {
                toastr['error']('Last Certificate Issue Date is required'); //toaster message issue
                response7 = false;
            } else {
                response7 = true;
            }
            @else
                response7 = true;
            @endif
            if (response == false || response1 == false || response2 == false || response3 == false || dateCheck == false || response4 == false || response5 == false || response6 === false || response7 === false) {
                $('#loader').hide();
                toastr['error']('Form has some errors'); //toaster message issue
                $(':input[type="submit"]').prop('disabled', false);
                returnFunction = false;
            } else {
                certificateStatus = $('#new_status').text().toLowerCase();
                if (certificateStatus != 'certified' && e.submitter.dataset.status == 'approved' && audit_type != 'Stage 1') {
                    var r = confirm("Are you sure,your standard certification status is Correct ?");
                    if (r === true) {
                        returnFunction = true;
                    } else {
                        $('#loader').hide();
                        $(':input[type="submit"]').prop('disabled', false);
                        returnFunction = false;
                    }
                } else {
                    returnFunction = true;
                }


            }
            localStorage.removeItem('yesCheck');
            localStorage.removeItem('new_standard_id');
            localStorage.clear();
            return returnFunction;
        }

        function clearErrorMessages() {
            $('.actual_audit_date_error').text('');
            $('.q1_name_error').text('');
            $('.q2_name_error').text('');
            $('.q3_name_error').text('');
            $('.question_ones_filename_0_error').text('');
            $('.question_two_filename_0_error').text('');
            $('.question_three_filename_0_error').text('');
            $('.question_four_filename_0_error').text('');
            $('.remarks_error').text('');
            $('.evidence_error').text('');
            $('.approval_date_error').text('');

        }

        $(document).on('change', '#certificate_status', function () {

            if ($(this).find('option:selected').val() == 'suspended') {
                $('#forSuspension').show();
                $('#forSuspensionReason').show();
            } else {
                $('#forSuspension').hide();
                $('#forSuspensionReason').hide();
            }
        })

        $("input[name='pack_closed']").click(function () {
            var radioValue = $("input[name='pack_closed']:checked").val();
            if (radioValue) {
                if (radioValue == 'TRUE') {
                    $('.packedClosedNew').show();

                    if ($('#audit_type').val() == 'Stage 1') {
                        $('.packedClosed').hide();

                    } else {
                        $('.packedClosed').show();
                    }
                    $('.approvedButton').show();


                } else {
                    $('.packedClosedNew').hide();
                    $('.packedClosed').hide();
                    $('.approvedButton').hide();
                }

            }

        });

        $("input[name='certificate_validity']").click(function () {
            var radioValue = $("input[name='certificate_validity']:checked").val();
            if (radioValue) {
                if ($('.approval_date').val() == '' && $('#new_expiry_date').val() == '') {
                    toastr['error']("Please Add Approval Day First.");
                    $("input[name='certificate_validity']:checked").prop('checked', false);
                    $("input[name='certificate_validity']:checked").trigger('change');
                } else {

                    if (radioValue == '1 Year') {

                        if (audit_type != "Stage 2") {
                            if ($('#new_expiry_date').val() == '') {

                                var newExpiryDate = moment($('.approval_date').val(), "DD-MM-YYYY").add('years', 1).format('DD-MM-YYYY');
                                var newExpiryDate1 = moment(newExpiryDate, "DD-MM-YYYY").subtract(1, 'days').format('DD-MM-YYYY');
                                // $("#txtLastExpiryDate").text(newExpiryDate1);
                                $("#new_expiry_date").val(newExpiryDate1);
                            } else {
                                var newExpiryDate = moment($('#new_expiry_date').val(), "DD-MM-YYYY").add('years', 1).format('DD-MM-YYYY');
                                var newExpiryDate1 = moment(newExpiryDate, "DD-MM-YYYY").subtract(1, 'days').format('DD-MM-YYYY');
                                // $("#txtLastExpiryDate").text(newExpiryDate1);
                                $("#new_expiry_date").val(newExpiryDate1);
                            }


                        } else {
                            var newExpiryDate = moment($('.approval_date').val(), "DD-MM-YYYY").add('years', 1).format('DD-MM-YYYY');
                            var newExpiryDate1 = moment(newExpiryDate, "DD-MM-YYYY").subtract(1, 'days').format('DD-MM-YYYY');
                            // $("#txtLastExpiryDate").text(newExpiryDate1);
                            $("#new_expiry_date").val(newExpiryDate1);
                        }


                    } else {
                        var newExpiryDate = moment($('.approval_date').val(), "DD-MM-YYYY").add('years', 3).format('DD-MM-YYYY');
                        var newExpiryDate1 = moment(newExpiryDate, "DD-MM-YYYY").subtract(1, 'days').format('DD-MM-YYYY');
                        // $("#txtLastExpiryDate").text(newExpiryDate1);
                        $("#new_expiry_date").val(newExpiryDate1);
                    }

                }

            }

        });

        $("input[name='print_required']").click(function () {
            var radioValue = $("input[name='print_required']:checked").val();
            if (radioValue) {
                if (radioValue == 'TRUE') {
                    $(".printAssignTo").show();
                } else {
                    $(".printAssignTo").hide();
                }

            }

        });


        function certificate(companyStandardId) {
            $('.suspension_error_required').hide();
            var request = {"company_standard_id": companyStandardId};
            $.ajax({
                type: "GET",
                url: '{{route('ajax.certificate.standard.index')}}',
                data: request,
                success: function (response) {
                    if (response.status == 'success') {
                        $("#certificate_html").html(response.data.html);
                        $('#certificate_date').datepicker({
                            format: 'dd-mm-yyyy',
                        });
                        $('#certificatesStandard').modal('show');


                    } else {
                        toastr['error']("Something Went Wrong.");
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function certificateSubmit(e) {


            e.preventDefault();

            var response = true;
            if ($('#certificate_status option:selected').val() === 'suspended') {
                if ($(".certificate_radio_check_value").is(":checked") === true) {
                    response = true;
                    $('.suspension_error_required').hide();
                } else {
                    response = false;
                }
            }
            if (response === true) {
                var form = $('#certificate-store')[0];
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('ajax.certificate.standard.store') }}",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: formData,
                    success: function (response) {
                        if (response.status == "success") {
                            $('#certificatesStandard').modal('hide');
                            toastr['success']("Certificate Status Updated Successfully.");
                            $('#new_status').text(response.data.certificate.certificate_status);

                        } else if (response.status == "withdrawn_error") {
                            toastr['error']("Certificate must be suspended first.");
                        } else {
                            toastr['error']("Form has Some Errors.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $('.suspension_error_required').show();

            }
        }

        $(document).on('change', '#is_print_letter', function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
        })


        $(document).on('click', '.deleteCertificateSubmit', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.certificate.standard.delete') }}",
                data: {id: id},
                success: function (response) {
                    if (response.status == "success") {
                        $('#certificatesStandard').modal('hide');
                        toastr['success']("Certificate Delete Successfully.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    } else {
                        toastr['error']("Form has Some Errors.");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });


        $('#selectAll').on('click', function () {
            $("table input[type=radio][value=YES]").attr("checked", "checked");
        });


    </script>

    <script>
        $(document).ready(function () {
            //Question 1 Files

            $(".q1_files_add").click(function () {
                var html = $(".clone_q1_files").html();
                $(".increment_q1_files").after(html);
                questionOneCount++;


            });
            $("body").on("click", ".q1_files_delete", function () {
                $(this).parents(".control-group_q1_files").remove();
                questionOneCount--;
            });


            //Question 2 Files

            $(".q2_files_add").click(function () {
                var html = $(".clone_q2_files").html();
                $(".increment_q2_files").after(html);
                questionTwoCount++;


            });
            $("body").on("click", ".q2_files_delete", function () {
                $(this).parents(".control-group_q2_files").remove();
                questionTwoCount--;
            });


            //Question 3 Files

            $(".q3_files_add").click(function () {
                var html = $(".clone_q3_files").html();
                $(".increment_q3_files").after(html);
                questionThreeCount++;


            });
            $("body").on("click", ".q3_files_delete", function () {
                $(this).parents(".control-group_q3_files").remove();
                questionThreeCount--;
            });

            $(".q4_files_add").click(function () {
                questionFourCount++;
            });
            $("body").on("click", ".q4_files_delete", function () {
                questionFourCount--;
            });
        });

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        $('#enableQ4').on('change', function () {
            if ($(this).is(':checked') === true) {
                $('#enableQ4Files').show();
            } else {
                $('#enableQ4Files').hide();
            }
        });
        $('#key_change').on('change', function () {
            if ($(this).is(':checked') === true) {
                $('.enableKeyChangeRemarks').show();
                $(this).val(1);
            } else {
                $('.enableKeyChangeRemarks').hide();
                $(this).val(0);
            }
        });
    </script>

@endpush
