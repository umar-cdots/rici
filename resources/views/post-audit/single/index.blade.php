@extends('layouts.master')
@section('title', "Post Audit {$audit_type}")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

    <style>
        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 1px !important;
        }

        .daterangepicker .ranges .input-mini {
            border: 1px solid #ccc;
            border-radius: 4px;
            color: #555;
            display: block;
            font-size: 17px !important;
            height: 37px !important;
            line-height: 30px;
            vertical-align: middle;
            margin: 0 0 10px 0;
            padding: 0 6px;
            width: 160px !important;
        }

        .daterangepicker .daterangepicker_start_input label, .daterangepicker .daterangepicker_end_input label {
            font-size: 16px !important;

        }

        .actual_audit_date_error,
        .q1_name_error,
        .q2_name_error,
        .q3_name_error,
        .question_ones_filename_0_error,
        .question_two_filename_0_error,
        .question_three_filename_0_error,
        .question_four_filename_0_error,
        .remarks_error,
        .evidence_error {
            color: red !important;
        }

        .daterangepicker .ranges .input-mini {
            font-size: 20px !important;
            height: 41px !important;
            width: 120px !important;
        }

        .daterangepicker .ranges .range_inputs > div:nth-child(2) {
            padding-left: 0px !important;
        }
    </style>
@endpush
@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 620.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry !</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">AUDIT <span>ACTIVITY</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Audit Activity</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" onsubmit="return validate(event)" action="{{ route('post.audit.store') }}"
                          method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="status" id="status" value="">
                        <input type="hidden" name="company_id" value="{{$company->id}}">
                        <input type="hidden" name="standard_id" value="{{$standard->id}}">
                        <input type="hidden" name="aj_standard_stage_id" value="{{$standardStage->id}}">
                        <input type="hidden" name="audit_type" value="{{$audit_type}}">
                        <input type="hidden" name="questions_one_id" value="{{ $questions[0]->id }}">
                        <input type="hidden" name="questions_two_id" value="{{ $questions[1]->id }}">
                        <input type="hidden" name="questions_three_id" value="{{ $questions[2]->id }}">
                        <div class="card-body card_cutom auditActivity auditJustificationTable filesUploadColor">
                            <div class="row">
                                <h6 class="blu_clr">{{ ucfirst($company->name) }} &nbsp; - &nbsp; {{ $audit_type }}
                                    Audit
                                    &nbsp; - &nbsp; {{ $standard->name }} Standard(s)</h6>
                            </div>
                            <div class="row rowBgGrey mrgn">
                                <div class="col-md-12">

                                    <div class="col-md-2 floting">
                                        <label for="actual_audit_date">Actual Audit Date :</label>
                                    </div>
                                    <div class="col-md-4 floting">

                                        <div class="form-group">
                                            <div class="input-group mb-0">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="actual_audit_date" readonly
                                                       id="actual_audit_date"
                                                       placeholder="dd//mm/yyyy"
                                                       class="form-control float-right active actual_audit_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="actual_audit_date_error"></label>
                                </div>
                            </div>
                            @if($audit_type != 'Stage 1')
                                <div class="row rowBgGrey mrgn">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h5 class="blu_clr">Corrective Action Request Confirmation:</h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[0]->title_for_oc_om }}</label>

                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q1_name" id="q1_name_yes"
                                                                   value="YES">
                                                            <label class="q1_name_yes">Yes</label>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q1_name" id="q1_name_no"
                                                                   value="NO" checked>
                                                            <label class="q1_name_no">No</label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <label class="q1_name_error"></label>
                                            <label class="question_ones_filename_0_error"></label>

                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-12 question_1" style="display:none">
                                            <div class="row rowBgGrey">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 floting">
                                                        <label> Upload Verification Pack</label>
                                                    </div>
                                                    <div class="col-md-10 floting">

                                                        {{--                                                        <div class="custom-file">--}}
                                                        {{--                                                            <input type="file" class="" name="q1_files[]" id="q1_files"--}}
                                                        {{--                                                                   value="" multiple/>--}}
                                                        {{--                                                        </div>--}}


                                                        <div class="input-group control-group_q1_files increment_q1_files">
                                                            <input type="file" name="q1_files[]" class="form-control"
                                                                   id="q1_files">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-success q1_files_add"
                                                                        type="button"><i
                                                                            class="glyphicon glyphicon-plus"></i>Add
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="clone_q1_files hide">
                                                            <div class="control-group_q1_files input-group"
                                                                 style="margin-top:10px">
                                                                <input type="file" name="q1_files[]"
                                                                       class="form-control" id="q1_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-danger q1_files_delete"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-remove"></i>
                                                                        Remove
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_1">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-md-12 mrgn">
                                            <div class="col-md-3 floting">
                                                <label>{{ $questions[1]->title_for_oc_om }}</label>
                                            </div>
                                            <div class="col-md-3 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q2_name" id="q2_name_yes"
                                                                   value="YES">
                                                            <label class="q2_name_yes">Yes</label>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div style="position: relative;">
                                                            <input type="radio" name="q2_name" id="q2_name_no"
                                                                   value="NO" checked>
                                                            <label class="q2_name_no">No</label>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <label class="q2_name_error"></label>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12 question_2" style="display:none">
                                            <div class="row rowBgGrey">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 floting">
                                                        <label> Upload CAR(s) </label>
                                                    </div>
                                                    <div class="col-md-10 floting">
                                                        {{--                                                        <div class="custom-file">--}}
                                                        {{--                                                            <input type="file" class="" name="q2_files[]" id="q2_files"--}}
                                                        {{--                                                                   value="" multiple/>--}}
                                                        {{--                                                        </div>--}}

                                                        <div class="input-group control-group_q2_files increment_q2_files">
                                                            <input type="file" name="q2_files[]" class="form-control"
                                                                   id="q2_files">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-success q2_files_add"
                                                                        type="button"><i
                                                                            class="glyphicon glyphicon-plus"></i>Add
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="clone_q2_files hide">
                                                            <div class="control-group_q2_files input-group"
                                                                 style="margin-top:10px">
                                                                <input type="file" name="q2_files[]"
                                                                       class="form-control" id="q2_files">
                                                                <div class="input-group-btn">
                                                                    <button class="btn btn-danger q2_files_delete"
                                                                            type="button"><i
                                                                                class="glyphicon glyphicon-remove"></i>
                                                                        Remove
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="col-md-12 ">
                                                    <div class="col-md-6 trash_list" id="file_append_question_2">
                                                    </div>
                                                </div>
                                            </div>
                                            <label class="question_two_filename_0_error"></label>
                                        </div>
                                        <div class="col-md-12 mrgn floting row question_2" style="display:none">
                                            <div class="col-md-12">
                                                <label style="display: inline-flex;width: 100%;">
                                                        <span>Confirm CAR(s) are closed
                                                        & Evidences Uploaded</span>
                                                    <input type="checkbox" class="form-control" value="1"
                                                           style="width: 5%; height: 20px;margin-top: 3px;"
                                                           name="evidence" id="evidence">
                                                </label>
                                                <label class="evidence_error"></label>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                </div>
                            @endif
                            <div class="row rowBgGrey mrgn">
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <h5 class="blu_clr">{{ $questions[2]->title_for_oc_om }}</h5>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">

                                            <div class="col-md-4 floting no_padding">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q3_name" id="q3_name_no" value="YES">
                                                    <label class="q3_name_no">Yes</label>

                                                </div>

                                            </div>
                                            <div class="col-md-4 floting">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q3_name" id="q3_name_no" value="NO"
                                                           checked>
                                                    <label class="q3_name_no">No</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <label class="q3_name_error"></label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 question_3" style="display: none">
                                    <div class="col-md-2 floting">
                                        <label> Attach Files </label>
                                    </div>
                                    <div class="col-md-10 floting">
                                        <div class="input-group control-group_q3_files increment_q3_files">
                                            <input type="file" name="q3_files[]"
                                                   class="form-control" id="q3_files">
                                            <div class="input-group-btn">
                                                <button class="btn btn-success q3_files_add"
                                                        type="button"><i
                                                            class="glyphicon glyphicon-plus"></i>Add
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clone_q3_files hide">
                                            <div class="control-group_q3_files input-group"
                                                 style="margin-top:10px">
                                                <input type="file" name="q3_files[]"
                                                       class="form-control" id="q3_files">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger q3_files_delete"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-remove"></i>
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        {{--                                        <div class="custom-file">--}}
                                        {{--                                            <input type="file" class="" name="q3_files[]" id="q3_files"--}}
                                        {{--                                                   value="" multiple/>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 mrgn question_3" style="display:none;">
                                    <div class="col-md-12 ">
                                        <div class="col-md-6 trash_list" id="file_append_question_3">
                                        </div>
                                    </div>
                                </div>
                                <label class="question_three_filename_0_error"></label>
                            </div>
                            <div class="row rowBgGrey mrgn">
                                <div class="col-md-12">
                                    <div class="col-md-3 floting">
                                        <h5 class="blu_clr">Communication </h5>
                                    </div>
                                    <div class="col-md-3 floting">
                                        <div class="form-group">

                                            <div class="col-md-4 floting no_padding">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q4_name" id="q4_name_no" value="YES"
                                                           class="YesQ4">
                                                    <label class="q4_name_no">Yes</label>

                                                </div>

                                            </div>
                                            <div class="col-md-4 floting hideQ4NoCheckbox">
                                                <div style="position: relative;">
                                                    <input type="radio" name="q4_name" id="q4_name_no" value="NO"
                                                           class="NoQ4">
                                                    <label class="q4_name_no">No</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <label class="q4_name_error"></label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 question_4" style="display: none">
                                    <div class="col-md-2 floting">
                                        <label> Attach Files </label>
                                    </div>
                                    <div class="col-md-10 floting">
                                        <div class="input-group control-group_q4_files increment_q4_files">
                                            <input type="file" name="q4_files[]"
                                                   class="form-control" id="q4_files">
                                            <div class="input-group-btn">
                                                <button class="btn btn-success q4_files_add"
                                                        type="button"><i
                                                            class="glyphicon glyphicon-plus"></i>Add
                                                </button>
                                            </div>
                                        </div>
                                        <p id="q4_file_added" style="color:lightgreen"></p>
                                        <div class="clone_q4_files hide">
                                            <div class="control-group_q4_files input-group"
                                                 style="margin-top:10px">
                                                <input type="file" name="q4_files[]"
                                                       class="form-control" id="q4_files">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger q4_files_delete"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-remove"></i>
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-12 mrgn question_4" style="display:none;">
                                    <div class="col-md-12 ">
                                        <div class="col-md-6 trash_list" id="file_append_question_4">
                                        </div>
                                    </div>
                                </div>
                                <label class="question_four_filename_0_error"></label>
                            </div>

                            <div class="row mrgn">
                                <div class="col-md-12">
                                    <div class="col-md-8 floting">
                                        <div class="form-group">
                                            <label for="remarks">Task Comments *</label>
                                            <input type="text" id="remarks" name="remarks" class="form-control">
                                        </div>
                                        <label class="remarks_error"></label>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="receiver_id">Receiver</label>
                                            <select name="receiver_id" id="receiver_id" class="form-control">
                                                @if(auth()->user()->user_type == 'scheme_manager')
                                                @else
                                                    <option value="{{$standard->scheme_manager[0]->scheme_coordinators[0]->user_id}}">{{ucfirst($standard->scheme_manager[0]->scheme_coordinators[0]->full_name)}}</option>
                                                @endif
                                                <option value="{{$standard->scheme_manager[0]->user_id}}">{{ucfirst($standard->scheme_manager[0]->full_name)}}</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('receiver_id'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('receiver_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                $companyStandard  = \App\CompanyStandards::where([
                                    'company_id'=> $company->id,
                                    'standard_id'=> $standard->id,
                                ])->orderBy('id','desc')->first(['id']);
                                $certificate = \App\Certificate::where('company_standard_id',$companyStandard->id)->where('report_status','new')->orderBy('id','desc')->first(['certificate_status']);
                            @endphp
                            @if(!is_null($certificate) && $certificate->certificate_status === 'withdrawn')
                            @else
                                <div class="col-md-12 mrgn no_padding">
                                    <div class="col-md-4 floting"></div>
                                    <div class="col-md-4 floting"></div>
                                    <div class="col-md-2 floting">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success btn_save"
                                                    data-status="save">Save Only
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-2 floting no_padding saveSendButton" style="display: none;">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn_search" data-status="send">
                                                Save &amp; Send
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            @endif
                        </div>
                        <!-- /.card-body -->


                    </form>

                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>

@endsection
@push('models')
@endpush
@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">


        var approvalDate = '{{$standardStage->approval_date}}';
        //Date range picker
        userRole = @json(auth()->user()->user_type); // Assuming 'role' is a property of the user
        cutOfDays = parseInt('{{$cut_of_days}}'); // $cut_of_days
        //Date range picker
        $('.actual_audit_date').daterangepicker({
            format: "DD-MM-YYYY",
            minDate: (userRole === 'operation_manager' || userRole === 'operation_coordinator') ? moment().subtract(cutOfDays, 'days') : false, // Set minDate to current date - Cut of days for OM and OC roles
            maxDate: (userRole === 'operation_manager' || userRole === 'operation_coordinator') ? moment() : false, // Apply maxDate only for OM and OC roles
        }).on('apply.daterangepicker', function (ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            var dateAr = this.value.split('-');
            var newDate = startDate.format('YYYY-MM-DD');
            var newApprovalDate = moment(approvalDate).format('YYYY-MM-DD');
            var newActualAuditDate = moment(newDate).format('YYYY-MM-DD');
            var duration = moment.duration(endDate.diff(startDate));
            var days = duration.asDays();


            if (days > 20) {
                toastr['error']('Date From and To difference should not be > 20 days');
                $('#actual_date').val('');
            } else {
                if (newActualAuditDate > newApprovalDate) {
                    var dateFifteenJuly = moment('2023-07-15').format('YYYY-MM-DD');
                    if (newActualAuditDate > dateFifteenJuly) {
                        $('.hideQ4NoCheckbox').hide();
                        $('.NoQ4').removeAttr('checked');
                        $('.YesQ4').prop('checked', true).trigger('click');

                    } else {
                        $('.hideQ4NoCheckbox').show();
                        $('.NoQ4').removeAttr('checked');
                        $('.YesQ4').removeAttr('checked');
                        $('.NoQ4').prop('checked', true).trigger('click');
                    }

                } else if (newActualAuditDate < newApprovalDate) {
                    toastr['error']('Actual Audit Date always greater than AJ Approval Date');
                    $('#actual_date').val('');
                } else {
                    toastr['error']('Actual Audit Date always greater than AJ Approval Date');
                    $('#actual_date').val('');
                }
            }


        });
    </script>

    <script type="text/javascript">

        var questionOneCount = 0;
        var questionTwoCount = 0;
        var questionThreeCount = 0;
        var questionFourCount = 0;
        var verificationCount = {{$approvedVerificationCount}};
        var audit_type = "{{$audit_type}}";

        $("input[name='q1_name']").click(function () {

            var radioValue = $("input[name='q1_name']:checked").val();


            if (radioValue) {
                if (radioValue == 'YES') {

                    if (verificationCount > 0) {
                        $('.question_1').show();
                    } else {
                        alert('Note: Category I CAR(s) was raised - “Verification pack” cannot be uploaded, No AJ is approved.');
                        $('#q1_name_no').prop('checked', true);
                        $('#q1_name_yes').prop('checked', false);

                    }

                } else {
                    $('.question_ones_filename_0_error').text('');

                    $('.question_1').hide();

                }

            }


            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();

            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }


        });

        $("input[name='q2_name']").click(function () {
            var radioValue = $("input[name='q2_name']:checked").val();
            if (radioValue == 'YES') {

                $('.question_2').show();
            } else {
                $('.question_two_filename_0_error').text('');

                $('.question_2').hide();
            }
            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();
            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }


        });

        $("input[name='q3_name']").click(function () {
            var radioValue = $("input[name='q3_name']:checked").val();
            var q1 = $("input[name='q1_name']:checked").val();
            var q2 = $("input[name='q2_name']:checked").val();
            var q3 = $("input[name='q3_name']:checked").val();
            if (radioValue == 'YES') {
                $('.question_3').show();
            } else {
                $('.question_three_filename_0_error').text('');
                $('.question_3').hide();
            }


            if (q3 == 'YES') {
                $('.saveSendButton').show();
            } else {
                $('.saveSendButton').hide();
            }
        });

        $("input[name='q4_name']").click(function () {
            var radioValue = $("input[name='q4_name']:checked").val();
            if (radioValue === 'YES') {
                $('.question_4').show();
            } else {
                $('.question_four_filename_0_error').text('');
                $('.question_4').hide();
            }
        });


        function validate(e) {
            $(':input[type="submit"]').prop('disabled', true);
            $('#loader').show();
            if (e.submitter.dataset.status == 'save') {
                $('#status').val('save');
            } else if (e.submitter.dataset.status == 'send') {
                $('#status').val('unapproved');
            }


            if (e.submitter.dataset.status == 'send' && audit_type !== 'Stage 1') {
                var regionName = '{{$company->region->title }}';
                var days = 0;
                if (regionName === 'Pakistan Region') {
                    days = parseInt('{{$pk_days}}');
                }else if(regionName === 'Saudi Arabia Region'){
                    days = parseInt('{{$ksa_days}}');
                }



                var actual_audit_date = $('#actual_audit_date').val();
                if(actual_audit_date !== '' && actual_audit_date !== null && actual_audit_date !== undefined){
                    // Split the string using the ' - ' separator
                    var dates = actual_audit_date.split(" - ");
                    var endDate = dates[1];

                    // Parse the end date into a JavaScript Date object
                    var parts = endDate.split("-");
                    endDate = new Date(parts[2], parts[1] - 1, parts[0]); // year, month (0-based), day

                    // Get the current date
                    var currentDate = new Date();

                    // Calculate the difference in time (in milliseconds)
                    var timeDifference = currentDate.getTime() - endDate.getTime();

                    // Convert time difference from milliseconds to days
                    var daysDifference = Math.ceil(timeDifference / (1000 * 3600 * 24)); // 1000ms * 3600s * 24hrs

                    if(daysDifference < days){
                        toastr['error']('NOTE:: Save & Pack Send not allowed. Please clear your oldest packs first........'); //toaster message issue
                        $('#loader').hide();
                        $(':input[type="submit"]').prop('disabled', false);
                        return false;
                    }

                }
            }
            clearErrorMessages();
            var response = false;
            var response1 = false;
            var response2 = false;
            var response3 = false;
            var response4 = false;
            var response5 = false;

            if (questionOneCount == 0) {
                var radioValue = $("input[name='q1_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_ones_filename_0_error').text('Please Upload Verfication Pack');
                    response = false;
                } else {
                    $('.question_ones_filename_0_error').text('');
                    response = true;
                }

            } else {
                response = true;
            }
            if (questionTwoCount == 0) {
                var radioValue = $("input[name='q2_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_two_filename_0_error').text('Please Upload CAR(s)');
                    response1 = false;

                } else {
                    $('.question_two_filename_0_error').text('');
                    response1 = true;
                }
            } else {

                if ($('#evidence').is(':checked') == true) {
                    $('.evidence_error').text('');
                    response1 = true;
                } else {
                    $('.evidence_error').text('Evidence checked is required.');
                    response1 = false;

                }
            }
            if (questionThreeCount == 0) {
                var radioValue = $("input[name='q3_name']:checked").val();
                if (radioValue == 'YES') {
                    $('.question_three_filename_0_error').text('Please Upload Pack');
                    response2 = false;
                } else {
                    $('.question_three_filename_0_error').text('');
                    response2 = true;
                }
            } else {
                response2 = true;
            }
            if ($('#actual_audit_date').val() == '') {
                response3 = false;
                $('.actual_audit_date_error').text('Actual Audit Date is required');
            } else {
                response3 = true;
                $('.actual_audit_date_error').text('');
            }
            if (e.submitter.dataset.status != 'save') {
                if ($('#remarks').val() == '') {
                    response4 = false;
                    $('.remarks_error').text('Comments missing');
                } else {
                    response4 = true;
                    $('.remarks_error').text('');
                }
            } else {
                response4 = true;
                $('.remarks_error').text('');
            }

            if (questionFourCount === 0) {

                var radioValue = $("input[name='q4_name']:checked").val();
                if (radioValue === 'YES' && e.submitter.dataset.status === 'send') {
                    $('.question_four_filename_0_error').text('File is required');
                    response5 = false;
                } else {
                    $('.question_four_filename_0_error').text('');
                    response5 = true;
                }
            } else {
                response5 = true;
            }


            if (response == false || response1 == false || response2 == false || response3 == false || response4 == false || response5 === false) {
                toastr['error']('Form has some errors'); //toaster message issue
                $('#loader').hide();
                $(':input[type="submit"]').prop('disabled', false);
                return false;
            } else {
                localStorage.removeItem('yesCheck');
                localStorage.removeItem('new_standard_id');
                localStorage.clear();
                return true;
            }
        }

        function clearErrorMessages() {
            $('.actual_audit_date_error').text('');
            $('.q1_name_error').text('');
            $('.q2_name_error').text('');
            $('.q3_name_error').text('');
            $('.question_ones_filename_0_error').text('');
            $('.question_two_filename_0_error').text('');
            $('.question_three_filename_0_error').text('');
            $('.question_four_filename_0_error').text('');
            $('.remarks_error').text('');
            $('.evidence_error').text('');

        }

    </script>
    <script>
        $(document).ready(function () {

            //Question 1 Files

            $(".q1_files_add").click(function () {
                var html = $(".clone_q1_files").html();
                $(".increment_q1_files").after(html);
                questionOneCount++;


            });
            $("body").on("click", ".q1_files_delete", function () {
                $(this).parents(".control-group_q1_files").remove();
                questionOneCount--;
            });


            //Question 2 Files

            $(".q2_files_add").click(function () {
                var html = $(".clone_q2_files").html();
                $(".increment_q2_files").after(html);
                questionTwoCount++;


            });
            $("body").on("click", ".q2_files_delete", function () {
                $(this).parents(".control-group_q2_files").remove();
                questionTwoCount--;
            });


            //Question 3 Files

            $(".q3_files_add").click(function () {
                var html = $(".clone_q3_files").html();
                $(".increment_q3_files").after(html);
                questionThreeCount++;


            });
            $("body").on("click", ".q3_files_delete", function () {
                $(this).parents(".control-group_q3_files").remove();
                questionThreeCount--;
            });


            $(".q4_files_add").click(function () {
                questionFourCount++;
                $('#q4_file_added').text('File added successfully.')
            });
            $("body").on("click", ".q4_files_delete", function () {
                questionFourCount--;
            });

        });
        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
    </script>

@endpush
