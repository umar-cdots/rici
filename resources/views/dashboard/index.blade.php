@extends('layouts.master')
@section('title', "Dashbaord")


@push('css')
    <style>
        .counter {
            background-color: #eaecf0;
            text-align: center;
        }

        .employees, .customer, .design, .order {
            margin-top: 70px;
            margin-bottom: 70px;
        }

        .counter-count {
            font-size: 18px;
            background-color: #00b3e7;
            border-radius: 50%;
            position: relative;
            color: #ffffff;
            text-align: center;
            line-height: 92px;
            width: 92px;
            height: 92px;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            display: inline-block;
        }

        .employee-p, .customer-p, .order-p, .design-p {
            font-size: 24px;
            color: #000000;
            line-height: 34px;
        }
    </style>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark dashboard_heading"> Dashboard</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('dashboard') }}
                    </div>
                    <!-- /.col -->
                </div>
                <div class="accordion" id="faq">
                    <div class="card">
                        <div class="card-header" id="faqhead1">
                            <h3 class="card-title"><a href="#" class="btn btn-header-link" data-toggle="collapse"
                                                      data-target="#faq1"
                                                      aria-expanded="true" aria-controls="faq1">Auditor(s) and Technical
                                    Expert Statistics</a>
                            </h3>

                        </div>
                        <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                            <div class="card-body">
                                <div class="counter">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-info">
                                                <a href="{{ route('no-of-auditor-standard-wise') }}">
                                                    <div class="inner">
                                                        <h3>{{$auditorsCount -1}} </h3>

                                                        <p>{{$no_of_auditors_count}} (Standard(s)) </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-bag"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-auditor-standard-wise') }}"
                                                   class="small-box-footer">More info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-success">
                                                <a href="{{ route('no-of-technical-expert-standard-wise') }}">
                                                    <div class="inner">
                                                        <h3>{{ $no_of_technical_experts_count }}</h3>

                                                        <p>No of Technical Experts </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-stats-bars"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-technical-expert-standard-wise') }}"
                                                   class="small-box-footer">More
                                                    info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-warning">
                                                <a href="{{ route('auditor-evaluation-due-standard-wise') }}">
                                                    <div class="inner">
                                                        <h3>{{$auditor_evaluation_due_schedule_count}}</h3>

                                                        <p>Auditor Evaluations Due </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-person-add"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('auditor-evaluation-due-standard-wise') }}"
                                                   class="small-box-footer">More
                                                    info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="faqhead2">
                            <h3 class="card-title"><a href="#" class="btn btn-header-link" data-toggle="collapse"
                                                      data-target="#faq2"
                                                      aria-expanded="true" aria-controls="faq2">Certified Standards / Companies
                                    Statistics</a>
                            </h3>

                        </div>
                        <div id="faq2" class="collapse show" aria-labelledby="faqhead2" data-parent="#faq">
                            <div class="card-body">
                                <div class="counter">
                                    <div class="row">

                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-danger">
                                                <a href="#">
                                                    <div class="inner">
                                                        <h3>{{$no_of_certified_clients_standard_count }}</h3>

                                                        <p> ({{ $no_of_certified_clients_count }}) Certified/Active
                                                            Companies Only</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-pie-graph"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-certified-clients') }}"
                                                   class="small-box-footer">More info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-info">
                                                <a href="#">
                                                    <div class="inner">
                                                        <h3>{{ $no_of_suspended_clients_standard_count }}</h3>

                                                        <p>({{ $no_of_suspended_clients_count }}) Suspended
                                                            Companies Only</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-bag"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-suspended-clients') }}"
                                                   class="small-box-footer">More info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-success">
                                                <a href="#">
                                                    <div class="inner">
                                                        <h3>{{$no_of_withdrawn_clients_standard_count }}</h3>

                                                        <p>({{$no_of_withdrawn_clients_count}}) Withdrawn Companies Only</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-stats-bars"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-withdrawn-clients') }}"
                                                   class="small-box-footer">More info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="faqhead3">
                            <h3 class="card-title"><a href="#" class="btn btn-header-link" data-toggle="collapse"
                                                      data-target="#faq3"
                                                      aria-expanded="true" aria-controls="faq3">Certified Standards / Companies Statistics for the Year {{ date('Y') }}</a>
                            </h3>

                        </div>
                        <div id="faq3" class="collapse show" aria-labelledby="faqhead3" data-parent="#faq">
                            <div class="card-body">
                                <div class="counter">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-warning">
                                                <a href="#">
                                                    <div class="inner">
                                                        <h3>{{ $certifiedCompanyPerYearCountsStandards }}</h3>

                                                        <p>({{$no_of_certified_per_year_count}}) Certified Companies</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-person-add"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-certified-per-year') }}"
                                                   class="small-box-footer">More info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-danger">
                                                <a href="#">
                                                    <div class="inner">
                                                        <h3>{{$withdrawnCompanyPerYearCountsStandards}}</h3>

                                                        <p>({{ $no_of_withdrawn_per_year_count }}) Withdrawn Companies </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-pie-graph"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-withdrawn-per-year') }}"
                                                   class="small-box-footer">More info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-12 floting">
                                            <!-- small box -->
                                            <div class="small-box bg-info">
                                                <a href="#">
                                                    <div class="inner">
                                                        <h3>{{ $no_of_audit_conducted }}</h3>

                                                        <p>Audits Conducted</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-pie-graph"></i>
                                                    </div>
                                                </a>
                                                <a href="{{ route('no-of-audit-conducted') }}" class="small-box-footer">More
                                                    info <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </div>
@endsection
