@extends('layouts.master')
@section('title', "Profile Update")

@section('content')

    <div class="content-wrapper dashboard_tabs">
        <section class="content-header">
            <div class="container-fluid ">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <div class="col-sm-5 floting">
                            {{-- <h1>Profile Update</h1> --}}
                        </div>
                    </div>

                    <div class="col-sm-4">
                        {{ Breadcrumbs::render('profile') }}

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="card card_cutom">
                <div class="card-header">
                    <h3 class="card-title">Profile Update</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('profile.update') }}"
                          method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            @if(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'data_entry' || auth()->user()->user_type == 'management')
                                <div class="col-md-6 floting">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" name="username" id="username"
                                               value="{{auth()->user()->username}}">
                                    </div>
                                </div>
                                <div class="col-md-6 floting">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email"
                                               value="{{auth()->user()->email}}">
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-4 floting">
                                <div class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                                    <label for="current-password">Current
                                        Password</label>
                                    <input type="password" class="form-control"
                                           name="current-password"
                                           id="current-password">
                                    @if ($errors->has('current-password'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('current-password') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="form-group {{ $errors->has('new-password') ? ' has-error' : '' }}">
                                    <label for="new-password">New Password</label>
                                    <input type="password" class="form-control"
                                           name="new-password" id="new-password">
                                    @if ($errors->has('new-password'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('new-password') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="form-group">
                                    <label for="new-password-confirm">Re-type
                                        Password</label>
                                    <input type="password" class="form-control"
                                           name="new-password-confirm"
                                           id="new-password-confirm">
                                </div>
                            </div>
                            @if(auth()->user()->user_type === 'scheme_manager')
                                <div class="col-md-3 floting">
                                    <div class="form-group {{ $errors->has('pk_days') ? ' has-error' : '' }}">
                                        <label for="pk_days">Do not send PK Packs < Day:-</label>
                                        <input type="number" class="form-control" value="{{$pk_days ?? 0}}"
                                               name="pk_days" id="pk_days">
                                        @if ($errors->has('pk_days'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('pk_days') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group {{ $errors->has('ksa_days') ? ' has-error' : '' }}">
                                        <label for="ksa_days">Do not send KSA Packs < Day:-</label>
                                        <input type="number" class="form-control"
                                               value="{{$ksa_days ?? 0}}"
                                               name="ksa_days" id="ksa_days">
                                        @if ($errors->has('ksa_days'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('ksa_days') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group {{ $errors->has('cut_of_days') ? ' has-error' : '' }}">
                                        <label for="cut_of_days">Cut of Days</label>
                                        <input type="number" class="form-control"
                                               value="{{$cut_of_days ?? 0}}"
                                               name="cut_of_days" id="cut_of_days">
                                        @if ($errors->has('cut_of_days'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('cut_of_days') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 floting">
                                    <div class="form-group">
                                        <label for="comments">Pack review comments </label>
                                        <input type="checkbox"
                                               style="display: block;width: 10%;height: 34px;"
                                               name="comments"
                                               @if($isComments === 1 || $isComments === '1') checked @endif
                                               value="@if($isComments === 1 || $isComments === '1') 1 @else 0 @endif"
                                               id="comments">
                                    </div>
                                </div>

                            @endif
                        </div>

                        <div class="row">
                            <div class="col-md-12 mrgn no_padding">
                                <div class="col-md-3 floting"></div>

                                <div class="col-md-3 floting no_padding float-right">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success btn_save">Update
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).on('change', '#comments', function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
        })
    </script>
@endpush
