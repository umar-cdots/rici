@extends('layouts.master')
@section('title', "Create Technical Expert")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">

        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Technical Expert <span>Profile</span>
                        </h1>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-10 floting">
                        <div class="progress">
                            <div class="progress-bar bg-primary progress-bar-striped barColor" role="progressbar"
                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"
                                 id="progressBar">
                                <span class="sr-only"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center floting barText">
                        <h5 id="progressValue">0%</h5>
                    </div>
                </div>

                <input type="hidden" class="auditor-id" value="">
                <input type="hidden" class="user-id" value="">

                <div class="row text-center mrgn">
                    <div class="col-md-4 text-left"><h3 class="profile_name">Create New Technical Expert</h3></div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2"></div>

                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')
                        <div class="col-md-2 text-right">

                            <input type="checkbox" id="auditor_status_toggle" checked data-toggle="toggle"
                                   data-on="Active" data-off="Inactive"
                                   name="auditor_status" data-onstyle="success" data-offstyle="danger"
                            >
                        </div>
                    @endif


                    <div class="clearfix"></div>
                </div>

                <div class="card card-primary mrgn comp_det_view ">

                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3">Technical Expert Profile </h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item"><a class="nav-link active show techExpTabs" href="#aud_info"
                                                    id="basic-info-link" data-toggle="tab" data-progress="0">Basic
                                    Info</a></li>
                            <li class="nav-item disabledDiv" id="eduTabDis"><a class="nav-link techExpTabs"
                                                                               href="#eduTab" data-toggle="tab"
                                                                               data-progress="25">Education</a></li>
                            <li class="nav-item disabledDiv" id="otherTabDis"><a class="nav-link techExpTabs"
                                                                                 href="#othr" data-toggle="tab"
                                                                                 data-progress="50">Others</a></li>
                            <li class="nav-item disabledDiv" id="standardTabDis"><a class="nav-link techExpTabs"
                                                                                    href="#standardsTab"
                                                                                    data-toggle="tab"
                                                                                    data-progress="75">Standards</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <div class="card-body card_cutom">
                        <div class="tab-content">
                            <div class="tab-pane active" id="aud_info">

                                <form action="#" method="post" id="step1" autocomplete="off"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="auditor_status" id="auditor_status"
                                           value="{{((auth()->user()->getRoleNames()[0] == 'super admin') || (auth()->user()->getRoleNames()[0] == 'scheme manager')) ? 'active' : 'inactive'}}">
                                    <input type="hidden" name="step" value="1">
                                    <div class="row">
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>First Name*</label>
                                                <input type="text" class="form-control" name="first_name">
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Last Name*</label>
                                                <input type="text" class="form-control" name="last_name">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email">
                                            </div>
                                        </div>


                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Working Region*</label>
                                                <select name="region_id" class="form-control regions">
                                                    <option value="">Select Region</option>
                                                    @foreach($regions as $region)
                                                        <option value="{{$region->id}}">{{$region->title}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Resident Country*</label>
                                                <select name="country_id" class="form-control countries">
                                                    <option value="">Select Country</option>
                                                    @if(!empty($countries))
                                                        @foreach($countries as $country)
                                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Resident City*</label>
                                                <select name="city_id" class="form-control cities">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Phone Number*</label>
                                                <div class="row">

                                                    <div class="col-md-4" style="padding-right: 0;">
                                                        <input type="text" class="form-control" value=""
                                                               id="country_code" name="country_code" readonly>
                                                    </div>
                                                    <div class="col-md-8" style="padding-left: 0;">
                                                        <div class="form-group mb-0">

                                                            <input type="text" class="form-control popFld phone"
                                                                   name="phone"
                                                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--                                                <input type="tel" class="form-control popFld phone" name="phone">--}}
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Landline Number</label>
                                                <input type="tel" class="form-control" name="landline">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Postal Address</label>
                                            <input type="text" class="form-control" name="postal_address">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 floting">
                                            <label>Date of Birth*</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control dob active" id="dob"
                                                       placeholder="Enter Date of Birth"
                                                       name="dob">
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Language:*</label>
                                                <select class="form-control languages" name="language_id[]"
                                                        multiple="multiple">
                                                    <option value="">Select Language</option>
                                                    @foreach($languages as $language)
                                                        <option value="{{$language->id}}">{{$language->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Overall Work Exp. (Years):*</label>
                                                <input type="number" class="form-control" name="working_experience"
                                                       step=0.1 min="0.1">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Main Education:*</label>
                                                <input type="text" class="form-control" name="main_education">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Nationality:*</label>
                                                <select name="nationality_id" class="form-control nationalities">
                                                    <option value="">Select Nationality</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Job status *</label>
                                            <div class="form-group">

                                                <div class="col-md-6 floting">
                                                    <label class="job_status">
                                                        <input type="radio" name="job_status" class="icheck-blue"
                                                               value="full_time" checked>
                                                        Full Time</label>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <label class="job_status">
                                                        <input type="radio" name="job_status" class="icheck-blue"
                                                               value="part_time">
                                                        Part Time</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <label>Profile Remarks*</label>
                                        <textarea name="profile_remarks" rows="3" class="form-control"></textarea>
                                    </div>

                                    <div class="col-md-12 mrgn">
                                        <div class="col-md-3 floting"></div>

                                        <div class="col-md-5 floting">
                                            <div class="form-group">
                                                <button type="submit"
                                                        class="btn btn-block btn-success btn_save step1Savebtn">Save &
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting"></div>

                                    </div>

                                </form>

                            </div>
                            <div class="tab-pane" id="eduTab">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Formal Education *</h4>
                                        <div id="formal-education">

                                        </div>
                                        <div id="edit-education-modal-container">

                                        </div>
                                    </div>


                                </div>

                                <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a id="aaaa"
                                                       class="btn btn-block btn-success btn_save educationform">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="othr">

                                <div class="row">
                                    <div class="col-md-12">

                                        <h4>Confidentality Agreement / Joining Date *</h4>

                                        <div id="technical-confidentiality-agreement">

                                        </div>
                                        <div id="edit-agreement-modal-container">

                                        </div>
                                    </div>
                                </div>


                                <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-success btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a id="ccc"
                                                       class="btn btn-block btn-success btn_save confidentilyform">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane " id="standardsTab">


                                <h3 class="text-center card-header">Standards
                                    <a href="#" class="edit_comp" id="addStd"
                                       onclick="getAddTechnicalExpertStandardsModal()">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        Add/Remove
                                    </a>
                                </h3>

                                <div id="addAuditorStandardModalContainer">

                                </div>
                                <div class="tab-pane active" id="auditor-standards-tabs">

                                </div>

                                <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-success btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#"
                                                       class="btn btn-block btn-success btn_save cont finalSave">
                                                        Save
                                                    </a>
                                                </div>
                                            </div>

                                            @if(auth()->user()->user_type == 'operation_manager')
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <a href="javascript:void(0)"
                                                           class="btn btn-block btn-success btn_save cont sendForApproval">
                                                            Send For Approval
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
    </div>



@endsection

@push('scripts')
    <script type="text/javascript">
        special_dropdown = [];
        special_dropdown_multiple = [];
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">

    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: black;
        }

        .select2-container--default .select2-selection--single {
            height: 37px;
        }

        .select2-container--default .select2-selection--multiple {
            height: 37px;
        }

    </style>


@endpush
@push('modals')
    <div class="modal fade" id="sendPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form id="comment-form" action="#" method="post">
                <input type="hidden" name="standard_id" id="standard_id" value="0">
                <input type="hidden" name="auditorStandardStatus" id="auditorStandardStatus" value="">
                <div id="aj-ids"></div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Comments</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><strong>Add Comment</strong></label>
                                    <textarea rows="5" class="form-control" name="remarks" id="remarks"></textarea>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0)"
                           class="btn btn-block btn-secondary btn_save cont finalSentForApproval">
                            Send
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endpush
@push('scripts')

    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>

    <!-- script configurations -->
    <script>

        $('.datefield').datepicker();
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.languages').select2({
            placeholder: 'Select language(s)',
            width: "100%"
        });
        $('.nationalities').select2({
            placeholder: 'Select Nationality'
        });
        $('.cities').select2({
            placeholder: 'Select City'
        });
        $('.countries').select2({
            placeholder: 'Select Country'
        });

        $(".yearfield").datepicker({
            format: "yyyy",
            startView: 'decade',
            minView: 'decade',
            viewSelect: 'decade',
            autoclose: true,
        });

        jQuery('#addStd').on('click', function () {
            jQuery('#stdMdl').modal('toggle');
        });
        /* $('.stopProgation').click(function (event) {
             event.stopPropagation();
         })*/

        $('#auditor_status_toggle').change(function () {
            if ($(this).is(":checked")) {
                $('#auditor_status').val('active')
            } else {
                $('#auditor_status').val('inactive')
            }
        });

        $('#basic-info-link').click(function () {
            $("div[data-toggle='toggle']").show();
        });


    </script>


    <!-- Make tabs persistent -->
    <script>
        $(document).ready(function () {
            // show active tab on reload
            if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

            // remember the hash in the URL without jumping
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if (history.pushState) {
                    history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
                } else {
                    location.hash = '#' + $(e.target).attr('href').substr(1);
                }
            });
        });
    </script>


    <script type="text/javascript">
        var auditorId = $(".auditor-id").val();
        String.prototype.ucFirst = function () {
            return this.substring(0, 1).toUpperCase() + this.substring(1, this.length);
        };

        $(document).ready(function () {
            var auditorId = $(".auditor-id").val();

            if (auditorId !== '') {
                getTechnicalExpertFormalEducationView(auditorId);
                getTechnicalExpertAgreementView(auditorId);
                getTechnicalExpertStandardsTabs(auditorId);
            }

        });


        {{--$('.countries').change(function () {--}}
        {{--    form = $(this).closest('form');--}}
        {{--    node = $(this);--}}
        {{--    node_to_modify = '.cities';--}}
        {{--    var country_id = $(this).val();--}}
        {{--    var request = "country_id=" + country_id;--}}

        {{--    if (country_id !== '') {--}}
        {{--        $.ajax({--}}
        {{--            type: "GET",--}}
        {{--            url: "{{ route('ajax.residentCountryCities') }}",--}}
        {{--            data: request,--}}
        {{--            dataType: "json",--}}
        {{--            cache: true,--}}
        {{--            success: function (response) {--}}
        {{--                ajaxResponseHandler(response);--}}
        {{--                if (response.status == "success") {--}}
        {{--                    var html = "";--}}
        {{--                    $.each(response.data.country_cities, function (i, obj) {--}}
        {{--                        html += '<option value="' + obj.id + '">' + obj.name + '</option>';--}}
        {{--                    });--}}
        {{--                    $(node_to_modify).html(html);--}}
        {{--                    $(node_to_modify).prepend("<option value='' selected>Select City</option>");--}}
        {{--                }--}}
        {{--            },--}}
        {{--            error: function () {--}}
        {{--                toastr['error']("Something Went Wrong.");--}}
        {{--            }--}}
        {{--        });--}}
        {{--    } else {--}}
        {{--        $(node_to_modify).html("<option value='' selected>Select City</option>");--}}
        {{--    }--}}
        {{--});--}}
        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });
        $('#name,#country_id').on('keyup change', function () {
            form = $(this).closest('form');
            node = $(this);
            var name = $('#name').val();
            var country_id = $('#country_id').val();
            var request = "name=" + name + "&country_id=" + country_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.companyNameExist') }}",
                data: request,
                dataType: "json",
                cache: true,
                global: false,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        if (response.data.already_exist) {
                            var standard_message = 'With Standards: ';
                            $.each(response.data.company_standards, function (i, obj) {
                                standard_message += obj.standard.number + ', ';
                            });
                            $('.standard_selection').show();
                            if (!confirm("Company Name Already Exists. " + standard_message + ". Do You Want to Create Another?")) {
                                $(node).val('');
                            } else {
                                $.each(response.data.company_standards, function (i, obj) {
                                    $('.standard_selection[data-standard-number=""]').hide();
                                });
                            }
                        } else {
                            $('.standard_selection').show();
                        }
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        {{--var intlTelInputConfig = {--}}
        {{--    initialCountry: "auto",--}}
        {{--    geoIpLookup: function (callback) {--}}
        {{--        $.get("{{ route('ajax.ipInfo') }}", function () {--}}
        {{--        }, "jsonp").always(function (resp) {--}}
        {{--            var countryCode = (resp && resp.country) ? resp.country : "";--}}
        {{--            callback(countryCode);--}}
        {{--        });--}}
        {{--    },--}}
        {{--    utilsScript: "{{ asset('plugins/intlTelInput/js/utils.js') }}", // just for formatting/placeholders etc--}}
        {{--    blur: function () {--}}
        {{--        console.log($(this));--}}
        {{--    }--}}
        {{--};--}}
        {{--$(".phone").intlTelInput(intlTelInputConfig);--}}
        {{--$(".landline").intlTelInput(intlTelInputConfig);--}}
        {{--$('.landline,.phone').on('keyup blur', function () {--}}
        {{--    var number = $(this).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);--}}
        {{--    if (number == '' || !$(this).intlTelInput("isValidNumber")) {--}}
        {{--        $(".step1Savebtn").attr("disabled", true);--}}
        {{--        has_error = true;--}}
        {{--        $(this).closest('.form-group').addClass('text-danger');--}}
        {{--        $(this).addClass('is-invalid');--}}
        {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
        {{--        $(this).closest('.form-group').append('<small class="text-danger" style="float:left">Invalid Contact Number.</small>');--}}
        {{--    } else {--}}
        {{--        $(".step1Savebtn").attr("disabled", false);--}}
        {{--        $(this).val(number);--}}
        {{--        $(this).closest('.form-group').removeClass('text-danger');--}}
        {{--        $(this).removeClass('is-invalid');--}}
        {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
        {{--    }--}}
        {{--});--}}

        $('#step1').submit(function (e) {
            e.preventDefault();
            var form = $('#step1')[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('technical-expert.store') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        $('a[href="#eduTab"]').tab('show');
                        $('#eduTabDis').removeClass('disabledDiv');
                        $('form').append('<input type="hidden" name="auditor_id" value="' + response.data.auditor_id + '">');
                        $('form').append('<input type="hidden" name="user_id" value="' + response.data.user_id + '">');
                        $('.auditor-id').val(response.data.auditor_id);
                        $('.user-id').val(response.data.user_id);
                        $('.profile_name').html(response.data.full_name);

                        getTechnicalExpertFormalEducationView(response.data.auditor_id);
                        getTechnicalExpertAgreementView(response.data.auditor_id);
                        getTechnicalExpertStandardsTabs(response.data.auditor_id);

                        $("div[data-toggle='toggle']").hide();

                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });


        function getTechnicalExpertFormalEducationView(auditorId) {
            var url = "{{ route('technical-expert.formal.education.index', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#formal-education").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function getTechnicalExpertAgreementView(auditorId) {
            var url = "{{ route('technical-expert.agreement.index', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#technical-confidentiality-agreement").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function getAddTechnicalExpertStandardsModal() {
            var url = "{{ route('technical-expert.standards.modal.index', ['AUDITOR_ID' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#addAuditorStandardModalContainer").html(response);
                    $("#addAuditorStandardsModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function getTechnicalExpertStandardsTabs(auditorId) {
            var url = "{{ route('technical-expert.standards.view', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#auditor-standards-tabs").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

    </script>

    <script>
        $('.confidentilyform').click(function (e) {
            //
            e.preventDefault();
            var form = $('#technical-expert-agreements-form')[0];
            var formData = new FormData(form);
            if ($("#ccc").hasClass("confidentilyform") === true) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('technical-expert.confidentially-agreement.save') }}",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: formData,
                    success: function (response) {
                        ajaxResponseHandler(response, form);
                        if (response.data.status === "success") {
                            $("#ccc").removeClass("confidentilyform").addClass("cont");
                            getTechnicalExpertAgreementView(auditorId);
                            $('[href="#standardsTab"]').tab('show');
                        }

                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $('[href="#standardsTab"]').tab('show');
            }
        });
        $('.educationform').click(function (e) {
            e.preventDefault();
            var form = $('#auditor-education-form')[0];
            var formData = new FormData(form);
            if ($("#aaaa").hasClass("educationform") === true) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('technical-expert.education.save') }}",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    // dataType: html,
                    data: formData,
                    success: function (response) {
                        ajaxResponseHandler(response, form);
                        if (response.data.status === "success") {
                            $('#otherTabDis').removeClass('disabledDiv');
                            $("#aaaa").removeClass("educationform").addClass("cont");
                            getTechnicalExpertFormalEducationView(response.data.auditor_id);
                            $('[href="#othr"]').tab('show');
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });

            } else {
                $('[href="#othr"]').tab('show');
            }
        });


        $('.cont').on("click", function () {
            var nextId = $(this).parents('.tab-pane').next().attr("id") || 'pro_info';

            $('[href="#' + nextId + '"]').tab('show');
        });

        $('.prev').click(function () {
            var nextId = $(this).parents('.tab-pane').prev().attr("id") || 'pro_info';

            $('[href="#' + nextId + '"]').tab('show');
        });

        $('.finalSave').click(function () {
            var aTab;
            var tabs = document.getElementsByClassName('tabs-data');
            var id;
            var foodCodes;
            var energyCodes;
            var codes;
            var grades;

            // var aud_id =document.getElementsByClassName('auditor-id').val();


            if (tabs.length > 0) {
                for (var i = tabs.length; i--;) {

                    if (tabs[i].classList.contains('active')) {
                        aTab = tabs[i];
                    }
                }
                if (document.getElementsByClassName('auditor-standard-food-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    foodCodes = document.getElementsByClassName('auditor-standard-food-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else if (document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    energyCodes = document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else {
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                }


                if (foodCodes && (codes > 0 && foodCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.finish') }}",
                        data: {type: 'create', auditor_id: $(".auditor-id").val()},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else if (energyCodes && (codes > 0 && energyCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.finish') }}",
                        data: {type: 'create', auditor_id: $(".auditor-id").val()},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else if (codes > 0 && grades > 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.finish') }}",
                        data: {type: 'create', auditor_id: $(".auditor-id").val()},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    //
                    toastr['error']("Atleast One Record are Mandatory.");

                }

            } else {
                //
                toastr['error']("Atleast One Standard is Mandatory  .");

            }

        });


    </script>

    <script>
        $(document).ready(function () {
            // show active tab on reload
            if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

            // remember the hash in the URL without jumping
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if (history.pushState) {
                    history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
                } else {
                    location.hash = '#' + $(e.target).attr('href').substr(1);
                }
            });
        });

        $('.techExpTabs').on('shown.bs.tab', function (e) {
            var progress = $(e.target).attr("data-progress");
            $('#progressValue').text(progress + '%');
            $('#progressBar').css('width', progress + '%');
        });
    </script>





    <script>
        $('.sendForApproval').click(function () {
            $('#remarks').val('');
            $('#sendPopup').modal('show');
        });

        $('.finalSentForApproval').click(function () {

            var aTab;
            var tabs = document.getElementsByClassName('tabs-data');
            var id;
            var foodCodes;
            var energyCodes;
            var codes;
            var grades;
            var request = null;

            // var aud_id =document.getElementsByClassName('auditor-id').val();


            if (tabs.length > 0) {
                for (var i = tabs.length; i--;) {

                    if (tabs[i].classList.contains('active')) {
                        aTab = tabs[i];
                    }
                }
                if (document.getElementsByClassName('auditor-standard-food-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    foodCodes = document.getElementsByClassName('auditor-standard-food-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else if (document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    energyCodes = document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else {
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                }


                if ($('#auditorStandardStatus').val() == 'rejected' && $('#standard_id').val() != '') {
                    request = {
                        type: 'create',
                        auditorStandardStatus: $('#auditorStandardStatus').val(),
                        standardId: $('#standard_id').val(),
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    }
                } else if ($('#auditorStandardStatus').val() == 'resent' && $('#standard_id').val() != '') {
                    request = {
                        type: 'create',
                        auditorStandardStatus: $('#auditorStandardStatus').val(),
                        standardId: $('#standard_id').val(),
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    }
                } else if ($('#auditorStandardStatus').val() == 'approved' && $('#standard_id').val() != '') {
                    request = {
                        type: 'create',
                        auditorStandardStatus: $('#auditorStandardStatus').val(),
                        standardId: $('#standard_id').val(),
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    }
                } else {
                    request = {
                        type: 'create',
                        auditorStandardStatus: 'unapproved',
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    };
                }


                if (foodCodes && (codes > 0 && foodCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.send.for.approval') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else if (energyCodes && (codes > 0 && energyCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.send.for.approval') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else if (codes > 0 && grades > 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.send.for.approval') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    //
                    toastr['error']("Atleast One Record are Mandatory.");

                }

            } else {
                //
                toastr['error']("Atleast One Standard is Mandatory  .");

            }

        });
    </script>


@endpush