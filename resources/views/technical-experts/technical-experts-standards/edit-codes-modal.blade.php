<div id="editStandardCodesModal" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="editStandardCodesForm">
                <input type="hidden" name="auditor_standard_code_id" value="{{$auditorStandardCode->id}}">
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="row mrgn">

                            <div class="col-md-6 standards-div">
                                <div class="form-group">
                                    <label>Standard(s)</label>
                                    <select name="standard" id="standard-auditor-standard-on-edit" class="form-control" onchange="updateAccreditations('standard-auditor-standard-on-edit', 'standard-family-auditor-standard', 'accreditation-auditor-standard-on-edit', 'iaf-auditor-standard-on-edit', 'ias-auditor-standard-on-edit')">
                                        @foreach($standards as $standard)
                                            <option style="display:  block" {{ $standardId == $standard->id ? 'selected' : ''}} value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Accreditation</label>
                                    <select name="accreditation_id[]" id="accreditation-auditor-standard-on-edit" class="form-control" onchange="updateIAF('standard-family-auditor-standard', 'standard-auditor-standard-on-edit', 'accreditation-auditor-standard-on-edit', 'iaf-auditor-standard-on-edit', 'ias-auditor-standard-on-edit')">
                                        <option value="">--Select Accreditation--</option>
                                        @foreach($accreditations as $accreditation)
                                            <option style="display: none"
                                                    value="{{ $accreditation->id }}"
                                                    data-standard-id="{{$accreditation->standard_id}}">
                                                {{ $accreditation->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>IAF Code</label>
                                    <select name="iaf_id" class="form-control" id="iaf-auditor-standard-on-edit" onchange="updateIAS('standard-family-auditor-standard', 'standard-auditor-standard-on-edit', 'accreditation-auditor-standard-on-edit','iaf-auditor-standard-on-edit', 'ias-auditor-standard-on-edit')">
                                        <option value="">--Select IAF Code--</option>
                                        @foreach($iafs as $iaf)
                                            <option style="display: none"
                                                    value="{{$iaf->id}}"
                                                    data-standard-id="{{$iaf->standard_id}}"
                                                    data-accreditation-id="{{$iaf->accreditation_id}}">
                                                {{$iaf->code}}. {{$iaf->name}}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>IAS Code</label>
                                    <select name="ias_id" class="form-control" id="ias-auditor-standard-on-edit">
                                        <option value="">--Select IAS Code--</option>
                                        @foreach($iass as $ias)
                                            <option style="display: none"
                                                    value="{{$ias->id}}"
                                                    data-standard-family-id="{{$ias->standards_family_id}}"
                                                    data-standard-id="{{$ias->standard_id}}"
                                                    data-accreditation-id="{{$ias->accreditation_id}}"
                                                    data-iaf-id="{{$ias->iaf_id}}">
                                                {{$ias->code}}. {{$ias->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                        </div>
                        <div class="row mrgn">
                            <div class="col-md-12">
                                <div class="col-md-4 floting">
                                    <label>Status</label>
                                    <select class="form-control" name="status" onchange="showHideRemarksTextArea(this.value)">
                                        <option value="">Select Status</option>
                                        @foreach($status as $item)
                                            <option value="{{$item}}" {{$auditorStandardCode->status == $item ? 'selected': ''}}>{{ucfirst($item)
                                            }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-8 floting">
                                    <label>Remarks</label>
                                    <textarea rows="1" class="form-control remarksTextArea" name="remarks" {{$auditorStandardCode->status ==
                                 'full' ? 'disabled': ''}}>{{$auditorStandardCode->remarks}}</textarea>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary updateDataBtn">Submit</button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel" data-dismiss="modal">Cancel</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    $( "#editStandardCodesModal" ).on('show.bs.modal', function(){
        $("#standard-auditor-standard-on-edit").trigger('change');

        setTimeout(function(){
            $('#accreditation-auditor-standard-on-edit').val('{{ $auditorStandardCode->accreditation_id }}').trigger('change');

            setTimeout(function(){
                $('#iaf-auditor-standard-on-edit').val('{{ $auditorStandardCode->iaf_id }}').trigger('change');

                setTimeout(function(){
                    $('#ias-auditor-standard-on-edit').val('{{ $auditorStandardCode->ias_id }}');
                }, 200);

            }, 200);

        }, 200);

        $(".standards-div").hide();

    });

    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });

    $('.iaf').change(function () {
        form = $(this).closest('form');
        node = $(this);
        node_to_modify = '.ias';
        var iaf_id = $(this).val();
        var request = "iaf_id=" + iaf_id;

        if (iaf_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.iasByIAF') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.iases, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        /*  sortMe($(node_to_modify).find('option'));*/
                        $(node_to_modify).prepend("<option value='' selected>Select IAS</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select IAS</option>");
        }

    });

    function showHideRemarksTextArea($status) {

        if ($status == 'partial') {
            $(".remarksTextArea").prop('disabled', false);
            $("textarea").prop('required',true);
        }
        if ($status == 'full') {
            $("textarea").prop('required',false);
            $(".remarksTextArea").prop('disabled', true);
        }
    }

    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardCodesForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.code.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    technicalExpertStandardCodesView(response.data.auditor_standard_id);
                    $("#editStandardCodesModal").modal('hide');
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    enable_cb();
    disable_cb();
    $('.Non-Accredited').click(enable_cb);
    $('.acceditations').click(disable_cb);

    function enable_cb() {

        if ($('.Non-Accredited').is(':checked')) {
            $('.acceditations').attr('disabled', true).removeAttr('checked');
        } else {
            $('.acceditations').removeAttr('disabled');
        }
    }

    function disable_cb() {
        if ($('.acceditations').is(':checked')) {
            $('.Non-Accredited').attr('disabled', true).removeAttr('checked');
        } else {
            $('.Non-Accredited').removeAttr('disabled');
        }
    }

</script>