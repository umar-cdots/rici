<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-grade-form{{$auditor_standard_id}}">
        <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}" class="auditor_standard_id">
        <table id="FormalEducationTable" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2">
            <thead>
            <tr>
                <th width="15%">Grade</th>
                <th width="15%">Status</th>
                @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')
                    <th width="20%">Approval Date</th>
                @endif
                <th width="10%">Files</th>
                <th>Remarks</th>
                <th width="10%" class="action-column">Action</th>
            </tr>
            </thead>
            <tbody id="BodyStandardTrainingCourses_1">



            @foreach($auditorStandardsGrades as $auditorStandardsGrade)
                <tr>
                    <td>{{$auditorStandardsGrade->grade->name.' -'.$auditorStandardsGrade->grade->short_name }}</td>
                    <td>{{ucwords($auditorStandardsGrade->status)}}</td>
                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')
                    <td>{{$auditorStandardsGrade->approval_date}}</td>
                    @endif
                    <td>
                        <a href="{{ asset('/uploads/technical_expert_grade_document/'.$auditorStandardsGrade->grade_document)}}"
                           target="_blank"
                           title="{{$auditorStandardsGrade->grade_document }}">
                            {{ $auditorStandardsGrade->grade_document }}</a></td>
                    {{--                       <td><a href="{{$auditorStandardsGrade->getFirstMedia()->getFullUrl()}}" target="_blank" title="{{$auditorStandardsGrade->getFirstMedia()->name}}">--}}
                    {{--                               {{str_limit($auditorStandardsGrade->getFirstMedia()->name,'15','...')}}</a></td>--}}
                    <td>{{$auditorStandardsGrade->remarks}}</td>
                    <td align="center" class="action-column-data">
                        <ul class="data_list">
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="getEditTechnicalExpertStandardGradeModal({{$auditorStandardsGrade->id}})">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"
                                   onclick="deleteAuditorStandardGrade({{$auditorStandardsGrade->id}});">
                                    <i class="fa fa-close"></i>
                                </a>
                            </li>
                        </ul>

                    </td>
                </tr>
            @endforeach


            </tbody>
            @if($auditorStandardsGrades->count() == 0)
                <tfoot>
                <tr>
                    <td>
                        <div class="col-md-12" style="width: 230px;">
                            <select name="grade_id"
                                    class="form-control">
                                <option value="">Select</option>
                                @foreach($grades as $grade)
                                    <option value="{{$grade->id}}" {{ ($grade->short_name === 'TE') ? 'selected' : '' }}>{{$grade->name}}
                                        - {{$grade->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>

                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')

                        <td>
                            <select class="form-control" name="status">
                                {{--                            <option value="Not Applicable" selected>Not Applicable</option>--}}
                                @foreach($gradeStatus as $status)
                                    @if($status != 'suspended' && $status != 'withdrawn' && $status != 'Not Applicable' && $status != 'active')
                                        <option value="{{$status}}" {{ ($status == 'current') ? 'selected' :''  }}>{{ucwords($status)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <div class="col-md-12">
                                <input type="text" id="approval_date" name="approval_date"
                                        class="form-control datepicker"
                                        placeholder="Approval Date">
                            </div>
                        </td>
                    @else
                        <td>
                            <select class="form-control" name="status">
                                <option value="Not Applicable" selected>Not Applicable</option>
                                {{--                        @foreach($gradeStatus as $status)--}}
                                {{--                        <option value="{{$status}}">{{ucwords($status)}}</option>--}}
                                {{--                        @endforeach--}}
                            </select>
                        </td>
                    @endif


                    <td>
                        <div class="col-md-12">
                            <label class="upload_documt">Add Document (Max 2MB)
                                <input type="file" name="document_file" onchange="readURLMany(this,8)">
                                <img src="{{asset('/img/new.png')}}" alt="img"></label>
                            <div id="upld8"></div>
                        </div>
                    </td>

                    <td>
                        <div class="col-md-12 floting">
                        <textarea name="remarks"
                                  class="form-control"></textarea>
                        </div>
                    </td>
                    <td>
                        <div class="col-md-5 floting">
                            <input type="button" value="Add New" id="add_new_grade{{$auditor_standard_id}}"
                                   class="add-standard-training-course-btn{{$auditor_standard_id}} btn btn-primary"
                                   onclick="addStandardGrade({{$auditor_standard_id}})">
                        </div>
                    </td>
                </tr>
                </tfoot>
            @endif
        </table>
    </form>
</div>


<script>

    function readURLMany(input, id) {
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#upld' + id).html(html);
    }
    $(document).ready(function() {

        $('.standard-tabs').click(function () {
            $(".datepicker").datepicker({ endDate: "today", format: 'dd/mm/yyyy' });
        });
        $("#approval_date").datepicker({ endDate: "today", format: 'dd/mm/yyyy' });
    });
    function addStandardGrade(auditorStandardId) {

        var form = $("#auditor-standard-grade-form" + auditorStandardId)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.grade.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                if (response.status === 'exist') {
                    toastr['error']("Grade Already exist");
                } else {

                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        technicalExpertStandardGradesView(response.data.auditor_standard_id);
                        $("#approval_date").datepicker({ endDate: "today", format: 'dd/mm/yyyy' });
                    }
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function deleteAuditorStandardGrade(audStdGradeId) {
        var url = "{{ route('technical-expert.standards.grade.delete', ['GRADE_ID' => "GRADE_ID"]) }}".replace("GRADE_ID", audStdGradeId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {

                successResponseHandler(response);
                if (response.status == "success" || response.status == '200') {

                    technicalExpertStandardGradesView(response.data.auditor_standard_id);
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getEditTechnicalExpertStandardGradeModal(audStdGradeId) {
        var url = "{{ route('technical-expert.standards.grade.edit.modal', ['GRADE_ID' => "GRADE_ID"]) }}".replace("GRADE_ID", audStdGradeId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-auditor-standard-grade-container").html(response);
                $("#editStandardGradeModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }


</script>
