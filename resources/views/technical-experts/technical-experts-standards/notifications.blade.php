<div class="row">

    <div class="col-md-4"></div>
    @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'operation_manager')
        @if(!empty($notifications) && count($notifications) > 0)
            <div class="col-md-4">
                <div class="form-group">
                    <label>Comments Box</label>
                    <div class="card-body chat_box" style="display: block;">
                        <div class="direct-chat-messages">


                            @foreach($notifications as $key=>$notification)

                                @if($notification->sent_by === Auth::user()->id )
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                        </div>
                                        <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                        {{--                                        <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>--}}
                                    </div>

                                @else
                                    <div class="direct-chat-msg text-right">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                        </div>
                                        <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                        {{--                                        <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>--}}
                                    </div>
                                @endif
                            @endforeach


                        </div>

                    </div>
                </div>
            </div>
        @endif
    @endif
    <div class="col-md-4"></div>


</div>
<div class="row">


    @if(isset($auditorStandard) && !empty($notifications) && count($notifications) > 0)
        @if(auth()->user()->user_type == 'operation_manager')
            <div class="col-md-9"></div>
            @if($auditorStandard->auditor_standard_status == 'rejected')
                <div class="col-md-3">
                    <div class="form-group">
                        <a href="javascript:void(0)"
                           onclick="sendPopUpModal('resent','{{$auditorStandard->standard_id}}')"
                           class="btn btn-block btn-secondary btn_save cont sendForApproval">
                            Resent
                        </a>
                    </div>
                </div>
            @endif
        @endif
        @if(auth()->user()->user_type == 'scheme_manager')
            <div class="col-md-6"></div>
            @if($auditorStandard->auditor_standard_status == 'resent' || $auditorStandard->auditor_standard_status == 'unapproved')

                @if(!empty($auditorStandardGrades) && count($auditorStandardGrades) > 0 && $auditorStatus == 'active')
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="javascript:void(0)"
                               onclick="sendPopUpModal('approved','{{$auditorStandard->standard_id}}')"
                               class="btn btn-block btn-secondary btn_save cont approved">
                                Approved
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="javascript:void(0)"
                               onclick="sendPopUpModal('rejected','{{$auditorStandard->standard_id}}')"
                               class="btn btn-block btn-secondary btn_save cont rejected">
                                Reject
                            </a>
                        </div>
                    </div>
                @endif
            @endif
        @endif
    @endif
</div>


<script>


    function sendPopUpModal(status, standardId) {
        $('#remarks').val('');
        $('#standard_id').val(standardId);
        $('#auditorStandardStatus').val(status);
        $('#sendPopup').modal('show');
    }


</script>
