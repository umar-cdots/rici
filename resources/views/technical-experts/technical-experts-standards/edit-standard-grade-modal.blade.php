<div class="modal fade codesModal" id="editStandardGradeModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editStandardGradeForm" enctype="multipart/form-data">
            <input type="hidden" name="standard_grade_id" value="{{$auditorStandardsGrade->id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Technical Expert Standard Grade</h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">


                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Grade</label>
                            <select name="grade_id"
                                    class="form-control">
                                <option value="">Select</option>
                                @foreach($grades as $grade)
                                    <option value="{{$grade->id}}"
                                            {{$auditorStandardsGrade->grade_id == $grade->id ? 'selected': ''}}>
                                        {{$grade->name}} -{{$grade->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Status</label>
                                <select class="form-control" name="status">
                                    @foreach($gradeStatus as $status)
                                        @if($status != 'suspended' && $status != 'withdrawn' && $status != 'Not Applicable' && $status != 'active')
                                            <option value="{{$status}}"
                                                    {{$auditorStandardsGrade->status == $status ? 'selected': ''}}>
                                                {{ucwords($status)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Status</label>
                                <select class="form-control disabledDiv" name="status">
                                    @foreach($gradeStatus as $status)
                                        @if($status != 'suspended' && $status != 'withdrawn')
                                            <option value="{{$status}}"
                                                    {{$auditorStandardsGrade->status == $status ? 'selected': ''}}>
                                                {{ucwords($status)}}</option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    @endif
                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Approval Date</label>
                                <input type="text" name="approval_date"
                                       class="form-control datepicker"
                                       placeholder="Approval Date" value="{{$auditorStandardsGrade->approval_date}}">
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Remarks</label>
                            <textarea name="remarks"
                                      class="form-control">{{$auditorStandardsGrade->remarks}}</textarea>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Evaluation Reports</label>
                            <br>
                            @if(!is_null($auditorStandardsGrade->grade_document))

                                @php

                                    $supported_file = array('pdf','docx', 'xlsx');

                                    $src_file_name = ''.$auditorStandardsGrade->grade_document.'';
                                    $ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
                                    $status =false;
                                    if (in_array($ext, $supported_file)) {
                                        $status = true;
                                    } else {
                                       $status = false;
                                    }
                                @endphp
                                @if($status == true)
                                    <a href="{{asset('/uploads/technical_expert_grade_document/'.$auditorStandardsGrade->grade_document)}}" target="_blank">{{ $auditorStandardsGrade->grade_document }}</a>
                                @else
                                    <img src="{{asset('/uploads/technical_expert_grade_document/'.$auditorStandardsGrade->grade_document)}}"
                                         alt="" width="50" height="50">
                                @endif


                                <i class="fa fa-remove removeImage" style="position: absolute;"> Delete Existing
                                    File</i>
                            @endif

                        </div>
                        <label class="upload_documt"> Add Document  (10MB)
                            <input type="file" name="document_file" id="document_file" onchange="readURLMany(this,13)">
                            <img src="{{asset('img/new.png')}}" alt="img"></label>
                        <div id="upld13"></div>
                        <br>
                        <small>
                            Upload file to override the existing file
                        </small>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
                    <button type="button" class="btn btn-primary updateDataBtn">Update Data</button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>


    $(document).ready(function () {
        $(".datepicker").datepicker({
            endDate: "today", format: 'dd/mm/yyyy'
        });
    });

    function readURLMany(input, id) {
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#upld' + id).html(html);
    }

    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardGradeForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.grade.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    technicalExpertStandardGradesView(response.data.auditor_standard_id);
                    $("#editStandardGradeModal").modal('hide');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });

    $('.removeImage').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardGradeForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.grade.remove') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    technicalExpertStandardGradesView(response.data.auditor_standard_id);
                    $("#editStandardGradeModal").modal('hide');

                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
</script>