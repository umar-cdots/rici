<div class="modal fade codesModal" id="editStandardWitnessEvalModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" id="editStandardWitnessEvalForm" enctype="multipart/form-data">
            <input type="hidden" name="standard_witness_eval_id" value="{{$auditorStandardWitnessEval->id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Technical Expert Standard Witness Evaluation</h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">


                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Approval Date</label>
                            <input type="text" name="due_date" class="form-control" data-provide="datepicker"
                                   placeholder="Due Date" value="{{$auditorStandardWitnessEval->due_date}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Actual Date</label>
                            <input type="text" name="actual_date" class="form-control"
                                   data-provide="datepicker"
                                   placeholder="Actual Date" value="{{$auditorStandardWitnessEval->actual_date}}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Satisfactory</label>
                            <select class="form-control" name="satisfactory">
                                @foreach($satisfactory as $item)
                                    <option value="{{$item}}"
                                            {{$auditorStandardWitnessEval->satisfactory == $item ? 'selected': ''}}>
                                        {{ucwords($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Document Existing File</label>
                            <br>

                            @if(!is_null($auditorStandardWitnessEval->witness_document))
                                @php
                                    $supported_file = array('pdf','docx', 'xlsx');

                                    $src_file_name = ''.$auditorStandardWitnessEval->witness_document.'';
                                    $ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
                                    $status =false;
                                    if (in_array($ext, $supported_file)) {
                                        $status = true;
                                    } else {
                                       $status = false;
                                    }
                                @endphp
                                @if($status == true)
                                    <a href="{{asset('/uploads/technical_expert_witness_document/'.$auditorStandardWitnessEval->witness_document)}}"
                                       target="_blank">{{$auditorStandardWitnessEval->witness_document}}</a>
                                @else
                                    <img src="{{asset('/uploads/technical_expert_witness_document/'.$auditorStandardWitnessEval->witness_document)}}"
                                         alt="" width="50" height="50">
                                @endif
                            @endif
                        </div>
                        <label class="upload_documt"> Add Document  (10MB)
                            <input type="file" name="document_file" id="document_file" onchange="readURLMany(this,14)">
                            <img src="{{asset('img/new.png')}}" alt="img"></label>
                        <div id="upld14"></div>
                        <br>
                        <small>
                            Upload file to override the existing file
                        </small>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
                    <button type="button" class="btn btn-primary updateDataBtn">Update Data</button>

                </div>
            </div>
        </form>
    </div>
</div>

<script>

    function readURLMany(input, id) {
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#upld' + id).html(html);
    }

    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardWitnessEvalForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.witness.eval.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    technicalExpertStandardWitnessEvalView(response.data.auditor_standard_id);
                    $("#editStandardWitnessEvalModal").modal('hide');
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
</script>