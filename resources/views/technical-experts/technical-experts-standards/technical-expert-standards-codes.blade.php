<div class="tables">
    <form action="#" autocomplete="off" method="post" id="auditor-standard-codes{{$auditor_standard_id}}">
        <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}" class="auditor_standard_id">
        <table id="FormalEducationTableCodes" cellspacing="20" cellpadding="0" border="0"
               class="table table-striped table-bordered table-bordered2 FormalEducationTableCodes{{$auditor_standard_id}}">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Accreditation</th>
                <th>IAF Code</th>
                <th>IAS Code</th>
                <th>Status</th>
                <th>Remarks</th>
                <th width="10%" class="action-column">Action</th>
            </tr>
            </thead>
            <tbody>


             @foreach($auditorStandardCodes as $key => $auditorStandardsCode)
                 <tr>
                     <td>{{$key + 1}}</td>
                     <td>{!! Helper::getAccreditations($auditorStandardsCode->accreditation_id) !!}</td>
                     <td>{{$auditorStandardsCode->iaf->code}}</td>
                     <td>{{$auditorStandardsCode->ias->code}}</td>
                     <td>{{ucfirst($auditorStandardsCode->status)}}</td>
                     <td>{{$auditorStandardsCode->remarks}}</td>
                     <td align="center" class="action-column-data">
                         <ul class="data_list">
                             <li>
                                 <a href="javascript:void(0)" onclick="getEditCodesModal({{$auditorStandardsCode->id}})">
                                     <i class="fa fa-pencil"></i>
                                 </a>
                             </li>
                             <li>
                                 <a href="javascript:void(0)" onclick="deleteAuditorStandardCode({{$auditorStandardsCode->id}});">
                                     <i class="fa fa-close"></i>
                                 </a>
                             </li>
                         </ul>

                     </td>
                 </tr>
             @endforeach


            </tbody>

        </table>
        <div class="col-md-12"  style="    margin-top: 10px;">
            <button type="button" class="btn btn-primary edit_btn pull-right"
{{--                    data-toggle="modal" data-target="#addStandardCodesModal{{$auditor_standard_id}}" --}}
                    onclick="getStandardId('{{$auditor_standard_id}}')">Add Code
            </button>
        </div>
    </form>
</div>

<div id="addStandardCodesModal{{$auditor_standard_id}}" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="addStandardCodesForm{{$auditor_standard_id}}">
                <input type="hidden" name="auditor_standard_id" value="{{$auditor_standard_id}}">
                <input type="hidden" id="auditor_standard_id1" value="">
                
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">

                        <div class="mrgn">
                            <div class="row mrgn">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="accreditation-auditor-standard{{$auditor_standard_id}}">Accreditation</label>
                                        <select name="accreditation_id[]"
                                                id="accreditation-auditor-standard{{$auditor_standard_id}}"
                                                class="form-control accreditation-auditor-standard{{$auditor_standard_id}}"
                                                onchange="getIaf('{{$auditor_standard_id}}')"

                                                {{--                                                onchange="updateIAF('standard-family-auditor-standard', 'standard-auditor-standard{{$auditor_standard_id}}', 'accreditation-auditor-standard{{$auditor_standard_id}}', 'iaf-auditor-standard{{$auditor_standard_id}}', 'ias-auditor-standard{{$auditor_standard_id}}')"--}}
                                        >
                                            <option value="">--Select Accreditation--</option>
                                            {{--                                            @foreach($accreditations as $accreditation)--}}
                                            {{--                                                <option style="display: none"--}}
                                            {{--                                                        value="{{ $accreditation->id }}"--}}
                                            {{--                                                        data-standard-id="{{$accreditation->standard_id}}">--}}
                                            {{--                                                    {{ $accreditation->name }}--}}
                                            {{--                                                </option>--}}
                                            {{--                                            @endforeach--}}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="iaf-auditor-standard{{$auditor_standard_id}}">IAF Code</label>
                                        <select name="iaf_id" class="form-control"
                                                id="iaf-auditor-standard{{$auditor_standard_id}}"
                                                onchange="getIas('{{$auditor_standard_id}}')">
                                            <option value="">--Select IAF Code--</option>
                                            {{--                                            @foreach($iafs as $iaf)--}}
                                            {{--                                                <option style="display: none"--}}
                                            {{--                                                        value="{{$iaf->iaf_id}}"--}}
                                            {{--                                                        data-standard-id="{{$iaf->standard_id}}"--}}
                                            {{--                                                        data-standard-family-id="{{$iaf->standards_family_id}}"--}}
                                            {{--                                                        data-accreditation-id="{{$iaf->accreditation_id}}">--}}
                                            {{--                                                    {{$iaf->code}}. {{$iaf->name}}--}}
                                            {{--                                                </option>--}}
                                            {{--                                            @endforeach--}}
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>IAS Code</label>
                                        <select name="ias_id" class="form-control"
                                                id="ias-auditor-standard{{$auditor_standard_id}}">
                                            <option value="">--Select IAS Code--</option>
                                            {{-- @foreach($iass as $ias)
                                                <option style="display: none"
                                                        value="{{$ias->id}}"
                                                        data-standard-family-id="{{$ias->standards_family_id}}"
                                                        data-standard-id="{{$ias->standard_id}}"
                                                        data-accreditation-id="{{$ias->accreditation_id}}"
                                                        data-iaf-id="{{$ias->iaf_id}}">
                                                    {{$ias->code}}. {{$ias->name}}</option>
                                            @endforeach --}}
                                        </select>

                                    </div>
                                </div>

{{--                                <div class="col-md-6 standards-div">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Standard(s)</label>--}}
{{--                                        <select name="standard" id="standard-auditor-standard{{$auditor_standard_id}}" class="form-control" onchange="updateAccreditations('standard-auditor-standard{{$auditor_standard_id}}', 'standard-family-auditor-standard', 'accreditation-auditor-standard{{$auditor_standard_id}}', 'iaf-auditor-standard{{$auditor_standard_id}}', 'ias-auditor-standard{{$auditor_standard_id}}')">--}}
{{--                                            @foreach($standards as $standard)--}}
{{--                                                <option style="display: none" value="{{ $standard->id }}" data-standard-family-id="{{$standard->standards_family_id}}">{{ $standard->name }}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-12">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Accreditation</label>--}}
{{--                                        <select name="accreditation_id[]" id="accreditation-auditor-standard{{$auditor_standard_id}}" class="form-control" onchange="updateIAF('standard-family-auditor-standard', 'standard-auditor-standard{{$auditor_standard_id}}', 'accreditation-auditor-standard{{$auditor_standard_id}}', 'iaf-auditor-standard{{$auditor_standard_id}}', 'ias-auditor-standard{{$auditor_standard_id}}')">--}}
{{--                                            <option value="">--Select Accreditation--</option>--}}
{{--                                            @foreach($accreditations as $accreditation)--}}
{{--                                                <option style="display: none"--}}
{{--                                                        value="{{ $accreditation->id }}"--}}
{{--                                                        data-standard-id="{{$accreditation->standard_id}}">--}}
{{--                                                    {{ $accreditation->name }}--}}
{{--                                                </option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-12">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>IAF Code</label>--}}
{{--                                        <select name="iaf_id" class="form-control" id="iaf-auditor-standard{{$auditor_standard_id}}" onchange="getIas()">--}}
{{--                                            <option value="">--Select IAF Code--</option>--}}
{{--                                            @foreach($iafs as $iaf)--}}
{{--                                                <option style="display: none"--}}
{{--                                                        value="{{$iaf->id}}"--}}
{{--                                                        data-standard-id="{{$iaf->standard_id}}"--}}
{{--                                                        data-standard-family-id="{{$iaf->standards_family_id}}"--}}
{{--                                                        data-accreditation-id="{{$iaf->accreditation_id}}">--}}
{{--                                                    {{$iaf->code}}. {{$iaf->name}}--}}
{{--                                                </option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}

{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-12">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>IAS Code</label>--}}
{{--                                        <select name="ias_id" class="form-control" id="ias-auditor-standard{{$auditor_standard_id}}">--}}
{{--                                            <option value="">--Select IAS Code--</option>--}}
{{--                                            --}}{{-- @foreach($iass as $ias)--}}
{{--                                                <option style="display: none"--}}
{{--                                                        value="{{$ias->id}}"--}}
{{--                                                        data-standard-family-id="{{$ias->standards_family_id}}"--}}
{{--                                                        data-standard-id="{{$ias->standard_id}}"--}}
{{--                                                        data-accreditation-id="{{$ias->accreditation_id}}"--}}
{{--                                                        data-iaf-id="{{$ias->iaf_id}}">--}}
{{--                                                    {{$ias->code}}. {{$ias->name}}</option>--}}
{{--                                            @endforeach --}}
{{--                                        </select>--}}

{{--                                    </div>--}}
{{--                                </div>--}}

                            </div>
                        </div>

                        <div class="row mrgn">
                            <div class="col-md-12">
                                <div class="col-md-4 floting">
                                    <label>Status</label>
                                    <select class="form-control" name="status" onchange="showHideRemarksTextArea(this.value)">
                                        <option value="">Select Status</option>
                                        @foreach($status as $item)
                                            <option value="{{$item}}">{{ucfirst($item)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-8 floting">
                                    <label>Remarks</label>
                                    <textarea rows="1" id="remarksTextAreaCode_{{$auditor_standard_id}}" class="form-control remarksTextArea" name="remarks"></textarea>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary" onclick="addStandardCodeSave({{$auditor_standard_id}})">Submit</button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel" data-dismiss="modal">Cancel</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>


    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });

    $('.iaf').change(function () {
        form = $(this).closest('form');
        node = $(this);
        node_to_modify = '.ias';
        var iaf_id = $(this).val();
        var request = "iaf_id=" + iaf_id;

        if (iaf_id !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.iasByIAF') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {


                        var html = "";
                        $.each(response.data.iases, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        // sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>Select IAS</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select IAS</option>");
        }

    });


    function showHideRemarksTextArea($status) {
        var auditor_standard_id =   $('#auditor_standard_id1').val();
        if ($status == 'partial') {
            $("#remarksTextAreaCode_"+auditor_standard_id).prop('disabled', false);
            // $("textarea").prop('required', true);
        }
        if ($status == 'full') {
            $("#remarksTextAreaCode_"+auditor_standard_id).prop('disabled', true);
      
            // $("textarea").prop('disabled', true);
        }
    }

    function addStandardCodeSave(auditorStandardId) {

        var form = $("#addStandardCodesForm" + auditorStandardId)[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.code.save') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {
                if (response.status === 'exist') {
                    toastr['error']("Code Already exist");
                } else {
                    ajaxResponseHandler(response, form);

                    if (response.status == "success" || response.status == '200') {
                        $("#addStandardCodesModal" + response.data.auditor_standard_id).modal('hide');
                        setTimeout(function () {
                            technicalExpertStandardCodesView(response.data.auditor_standard_id);
                        }, 1000);
                    }
                }
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }


    function deleteAuditorStandardCode(modelId) {
        var val = showConfirmDelete();
        if (val) {
            var url = "{{ route('technical-expert.standards.code.delete', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    if (response.status == "success" || response.status == '200') {
                        technicalExpertStandardCodesView(response.data.auditor_standard_id);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }
    }

    function getEditCodesModal(modelId) {
        var url = "{{ route('technical-expert.standards.code.edit.modal', ['MODEL_ID' => "MODEL_ID"]) }}".replace("MODEL_ID", modelId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#edit-auditor-standard-container").html(response);
                $("#editStandardCodesModal").modal('show');
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }
    enable_cb();
    disable_cb();


    $('.Non-Accredited').click(enable_cb);
    $('.acceditations').click(disable_cb);

    function enable_cb() {

        if ($('.Non-Accredited').is(':checked')) {
            $('.acceditations').attr('disabled', true).removeAttr('checked');
        } else {
            $('.acceditations').removeAttr('disabled');
        }
    }

    function disable_cb() {
        if ($('.acceditations').is(':checked')) {
            $('.Non-Accredited').attr('disabled', true).removeAttr('checked');
        } else {
            $('.Non-Accredited').removeAttr('disabled');
        }
    }

    function getStandardId(auditorStandardCodeId) {
        var request = {
            "standard_id": $('#auditor-standard-codes' + $('.standard-tabs.active').data('auditor_standard_id')).attr('data-standard_id'),
        };
        $.ajax({
            type: "GET",
            url: "{{ route('ajax.getAccreditationByStanadardId') }}",
            data: request,
            dataType: "json",
            cache: true,
            success: function (response) {
                if (response.status == "success") {
                    var $accreditation = $('.accreditation-auditor-standard' + auditorStandardCodeId);
                    $accreditation.empty();
                    $accreditation.append('<option value="">--Select Accreditation--</option>');
                    var data = response.data;
                    for (var i = 0; i < data.length; i++) {
                        $accreditation.append('<option id="' + data[i].id + '" value="' + data[i].id + '" data-standard-id="' + data[i].standard_id + '">' + data[i].name + '</option>');
                    }
                    $('#auditor_standard_id1').val(auditorStandardCodeId);
                    $('#standard-auditor-standard' + auditorStandardCodeId).val($('#auditor-standard-codes' + $('.standard-tabs.active').data('auditor_standard_id')).attr('data-standard_id')).trigger('change');
                    $('#addStandardCodesModal' + auditorStandardCodeId).modal('show');
                }


            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });


        // $('.standards-div').hide();

    }

    function getIaf(auditor_standard_id) {
        var accreditation_id = $('#accreditation-auditor-standard' + auditor_standard_id + ' option:selected').val();
        var standard_id = $('#accreditation-auditor-standard' + auditor_standard_id + ' option:selected').data('standard-id');

        var request = {
            "standard_id": standard_id,
            "accreditation_id": accreditation_id,
        };

        console.log(request);

        $.ajax({
            type: "GET",
            url: "{{ route('ajax.getIaf') }}",
            data: request,
            dataType: "json",
            cache: true,
            success: function (response) {
                if (response.status == "success") {
                    var $iaf = $('#iaf-auditor-standard' + auditor_standard_id);
                    $iaf.empty();
                    $iaf.append('<option value="">--Select IAF Code--</option>');
                    var data = response.data;
                    for (var i = 0; i < data.length; i++) {
                        $iaf.append('<option id="' + data[i].id + '" value="' + data[i].id + '" data-standard-id="' + data[i].standard_id + '" data-accreditation_id="' + data[i].accreditation_id + '" data-iaf_id="' + data[i].iaf_id + '">' + data[i].code + '. ' + data[i].name + '</option>');
                    }
                }


            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function getIas(auditor_standard_id) {
        // var auditor_standard_id = $('#auditor_standard_id1').val();
        var iaf_id = $('#iaf-auditor-standard' + auditor_standard_id + ' option:selected').val();
        var accedation_id = $('#iaf-auditor-standard' + auditor_standard_id + ' option:selected').data('accreditation_id');
        var standard_id = $('#iaf-auditor-standard' + auditor_standard_id + ' option:selected').data('standard-id');


        var request = {
            "standard_id": standard_id,
            "accedation_id": accedation_id,
            "iaf_id": iaf_id,
        };

        console.log(request);

        $.ajax({
            type: "GET",
            url: "{{ route('ajax.getIas') }}",
            data: request,
            dataType: "json",
            cache: true,
            success: function (response) {
                if (response.status == "success") {
                    var $ias = $('select[name="ias_id"]');
                    $ias.empty();
                    $ias.append('<option value="">--Select IAS Code--</option>');
                    var data = response.data;
                    for (var i = 0; i < data.length; i++) {
                        $ias.append('<option id=' + data[i].id + ' value=' + data[i].id + '>' + data[i].code + '. ' + data[i].name + '</option>');
                    }
                }


            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    // function getStandardId(auditorStandardCodeId){
    //     $('#auditor_standard_id1').val(auditorStandardCodeId);
    //     $('#standard-auditor-standard'+auditorStandardCodeId).val($('#auditor-standard-codes'+$('.standard-tabs.active').data('auditor_standard_id')).attr('data-standard_id')).trigger('change');
    //     $('.standards-div').hide();
    // }

    function updateAccreditations(standard, selectedStandardFamilyId, accreditation, iaf = null, ias = null){

        let selectedStandard        = $("#"+standard).val();

        if (selectedStandard) {

            $("#"+iaf).children('option').not(':first').hide();
            $("#"+ias).children('option').not(':first').hide();

            if (iaf){
                $("#"+iaf).prop("selectedIndex", 0);
            }

            if (ias){
                $("#"+ias).prop("selectedIndex", 0);
            }


            $("#"+accreditation).children('option').not(':first').hide();
            $("#"+accreditation).prop("selectedIndex", 0);
            $("#"+accreditation + " option[data-standard-id=" + selectedStandard +"]").show();
        }

    }

    function updateIAF(standardFamily, standard, accreditation, iaf, ias = null){

        let standardId          = $("#"+standard).val();
        let accreditationId     = $("#"+accreditation).val();

        $("#"+ias).children('option').not(':first').hide();
        $("#"+ias).prop("selectedIndex", 0);

        if (standardId && accreditationId) {

            $("#"+iaf).children('option').not(':first').hide();
            $("#"+iaf).prop("selectedIndex", 0);
            $("#"+iaf + " option[data-standard-id=" + standardId +"][data-accreditation-id=" + accreditationId +"]").show();
        }

    }
    function updateIAS(standardFamily, standard, accreditation, iaf, ias){

        let standardId          = $("#"+standard).val();
        let accreditationId     = $("#"+accreditation).val();
        let iafId               = $("#"+iaf).val();

        if (standardId && accreditationId && iafId) {

            $("#"+ias).children('option').not(':first').hide();
            $("#"+ias).prop("selectedIndex", 0);
            $("#"+ias + " option[data-standard-id=" + standardId +"][data-accreditation-id=" + accreditationId +"][data-iaf-id=" + iafId +"]").show();
        }

    }

</script>
