<div class="sub_tabs">
    <div class="card">
        <div class="card-header d-flex p-0">
            <ul class="nav nav-pills p-2 ">
                @foreach($auditorStandards as $key =>$auditorStandard)
                    <li class="nav-item">
                        <a class="nav-link standard-tabs {{$key == 0 ? 'active show': ''}}"
                           href="#{{$auditorStandard->id}}"
                           {{ ($auditorStandard->standard->status == 0) ? 'disabled' : '' }}
                           {{--                           onclick="openTab(event, '{{$auditorStandard->id}}')"--}}
                           data-auditor_standard_id="{{$auditorStandard->id}}"
                           data-toggle="tab">{{$auditorStandard->standard->name}}
                            ( <span>
                                @foreach(\App\AuditorStandardGrade::where('auditor_standard_id',$auditorStandard->id)->where('status','current')->get() as $auditorStandardGrade)
                                    <span> {!! $auditorStandardGrade->grade->short_name !!} </span>
                                @endforeach
                            </span> )
                        </a>
                    </li>
                @endforeach
            </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
            <div class="tab-content">
                @foreach($auditorStandards as $key =>$auditorStandard)
                    <div class="tab-pane {{$key == 0 ? 'active': ''}} tabs-data {{ ($auditorStandard->standard->status == 0) ? 'disabledDiv' : '' }}"
                         id="{{$auditorStandard->id}}">
                        <input type="hidden" value="{{$auditorStandard->id}}" name="auditor-standard-id"
                               class="auditor-standard-id">
                        @if(auth()->user()->user_type == 'scheme_manager' )
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4" style="margin-bottom:10px "><span><strong
                                                style="    font-size: 17px;     padding-left: 72px;">Technical Expert Standard Status:</strong></span>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="row">

                                <div class="col-md-4"></div>
                                <div class="col-md-4" style="margin-bottom: 13px">
                                    <select class="form-control" name="auditor_standard_status"
                                            id="auditor_standard_status">
                                        <option data-auditor-standard-id="{{$auditorStandard->id}}"
                                                value="inactive" {{ ($auditorStandard->status == 'inactive') ? 'selected' :'' }}>
                                            Inactive
                                        </option>
                                        <option data-auditor-standard-id="{{$auditorStandard->id}}"
                                                value="active" {{ ($auditorStandard->status == 'active') ? 'selected' :'' }}>
                                            Active
                                        </option>
                                        <option data-auditor-standard-id="{{$auditorStandard->id}}"
                                                value="suspended" {{ ($auditorStandard->status == 'suspended') ? 'selected' :'' }}>
                                            Suspended
                                        </option>
                                        <option data-auditor-standard-id="{{$auditorStandard->id}}"
                                                value="withdrawn" {{ ($auditorStandard->status == 'withdrawn') ? 'selected' :'' }}>
                                            Withdrawn
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        @endif
                        <div class="row">


                            <div class="col-md-12">
                                <h4>Codes * <span class="font-size-15 color-red"></span>
                                </h4>

                                <div id="auditor-standard-codes{{$auditorStandard->id}}"
                                     data-standard_id="{{$auditorStandard->standard_id}}"
                                     class="auditor-standard-codes{{$auditorStandard->id}}">

                                </div>

                            </div>


                            @if($auditorStandard->standard->standards_family_id == 23)
                                <div class="col-md-12">
                                    <h4>Food Codes * <span
                                                class="font-size-15 color-red"></span>
                                    </h4>

                                    <div id="auditor-standard-food-codes{{$auditorStandard->id}}"
                                         class="auditor-standard-food-codes{{$auditorStandard->id}}">

                                    </div>

                                </div>
                            @endif

                            @if($auditorStandard->standard->standards_family_id == 20)
                                <div class="col-md-12">
                                    <h4>Energy Codes * <span class="font-size-15 color-red"></span>
                                    </h4>

                                    <div id="auditor-standard-energy-codes{{$auditorStandard->id}}"
                                         class="auditor-standard-energy-codes{{$auditorStandard->id}}">

                                    </div>

                                </div>
                            @endif


                            <div class="col-md-12">
                                <h4>Grade *</h4>

                                <div id="auditor-standard-grades{{$auditorStandard->id}}"
                                     class="auditor-standard-grades{{$auditorStandard->id}}">

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div id="technical-expert-standard-notifications{{$auditorStandard->id}}">

                                </div>

                            </div>


                            {{--                            <div class="col-md-12">--}}
                            {{--                                <h4>Auditor Witness Evaluation * <span class="font-size-15 color-red">getEditTechnicalExpertStandardGradeModal</span></h4>--}}

                            {{--                                <div id="auditor-standard-witness-eval{{$auditorStandard->id}}" class="auditor-standard-witness-eval{{$auditorStandard->id}}">--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}


                        </div>
                    </div>

                @endforeach
                {{--                <div id="edit-auditor-standard-training-course-container">--}}

                {{--                </div>--}}

                <div id="edit-auditor-standard-grade-container">

                </div>
                {{--                <div id="edit-auditor-standard-witness-eval-container">--}}

                {{--                </div>--}}

                <div id="edit-auditor-standard-container">

                </div>

            </div>

        </div>
    </div>
</div>
<link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
<style>
    .disabledDiv {
        pointer-events: none;
        opacity: 1.4;
    }
</style>
<script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>

    function openTab(e, id) {


        var aTab;
        var tabs = document.getElementsByClassName('tab-content');
        //
        for (var i = tabs.length; i--;) {
            //
            if (tabs[i].getElementsByClassName('tab-pane active')[0].getAttribute('id') == id) {
                aTab = tabs[i].getElementsByClassName('tab-pane active')[0];
            }
        }

        //

        if (aTab !== undefined) {
            // alert('e');
            // we found the active tab, now we get the inputs
            var inputs = aTab.getElementsByTagName("input");

            // check to make sure each is not empty
            for (var j = inputs.length; j--;) {
                if (inputs[j].value === "") {

                    // alert(e);
                    // something wasn't filled out.. do something..
                }
            }

        }

    }

    function fileUpload(input, id) {
        // alert(
        //     id
        // );
        var filename = input.files[0].name;
        var html = '';
        if (input.files && input.files[0]) {
            html += '<h6>Selected File: <span><a href="#">' + filename + '</a></span></h6>';
        }
        $('#img' + id).html(html);
    }


    {{--function technicalExpertStandardWitnessEvalView(auditorStandardId) {--}}
    {{--    var url = "{{ route('technical-expert.standards.witness.eval.index', ['auditor_standard_id' => "AUDITOR_STANDARD_ID"]) }}".replace("AUDITOR_STANDARD_ID",--}}
    {{--        auditorStandardId);--}}
    {{--    $.ajax({--}}
    {{--        type: "GET",--}}
    {{--        url: url,--}}
    {{--        success: function (response) {--}}
    {{--            $("#auditor-standard-witness-eval" + auditorStandardId).html(response);--}}
    {{--        },--}}
    {{--        error: function () {--}}
    {{--            toastr['error']("Something Went Wrong.");--}}
    {{--        }--}}
    {{--    });--}}
    {{--}--}}

    function technicalExpertStandardCodesView(auditorStandardId) {
        var url = "{{ route('technical-expert.standards.codes.index', ['auditor_standard_id' => "AUDITOR_STANDARD_ID"]) }}".replace("AUDITOR_STANDARD_ID",
            auditorStandardId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#auditor-standard-codes" + auditorStandardId).html(response);
                $('.FormalEducationTableCodes' + auditorStandardId + '').dataTable({
                    order: [[2, 'asc']],
                });
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function technicalExpertStandardFoodCodesView(auditorStandardId) {
        var url = "{{ route('technical-expert.standards.food.codes.index', ['auditor_standard_id' => "AUDITOR_STANDARD_ID"]) }}".replace("AUDITOR_STANDARD_ID",
            auditorStandardId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#auditor-standard-food-codes" + auditorStandardId).html(response);
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function technicalExpertStandardEnergyCodesView(auditorStandardId) {
        var url = "{{ route('technical-expert.standards.energy.codes.index', ['auditor_standard_id' => "AUDITOR_STANDARD_ID"]) }}".replace("AUDITOR_STANDARD_ID",
            auditorStandardId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#auditor-standard-energy-codes" + auditorStandardId).html(response);
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function technicalExpertStandardGradesView(auditorStandardId) {
        var url = "{{ route('technical-expert.standards.grades.index', ['auditor_standard_id' => "AUDITOR_STANDARD_ID"]) }}".replace("AUDITOR_STANDARD_ID",
            auditorStandardId);
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                $("#auditor-standard-grades" + auditorStandardId).html(response);
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    function technicalExpertStandardNotificationView(standard_id, auditorStandardId) {

        var auditorId = $(".auditor-id").val();
        $.ajax({
            type: "GET",
            url: "{{ route('technical-expert.getNotifications') }}",
            data: {standard_id: standard_id, auditor_id: auditorId, auditor_standard_id: auditorStandardId},
            success: function (response) {
                $("#technical-expert-standard-notifications" + auditorStandardId).html(response);
            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    }

    $(document).ready(function () {
        $(".auditor-standard-id").each(function () {


            var auditorStandardId = $(this).val();
            var standardId = $(this).data('standard-id');

            technicalExpertStandardGradesView(auditorStandardId);
            technicalExpertStandardCodesView(auditorStandardId);
            technicalExpertStandardFoodCodesView(auditorStandardId);
            technicalExpertStandardEnergyCodesView(auditorStandardId);
            technicalExpertStandardNotificationView(standardId, auditorStandardId);

        });
    });

    $(document).on('change', '#auditor_standard_status', function (e) {
        e.preventDefault();

        var auditor_status = $(this).val();
        var auditor_standard_id = $(this).find(':selected').data('auditor-standard-id');

        $.ajax({
            type: "POST",
            url: "{{ route('ajax.updateAuditorStandardStatus') }}",
            data: {
                'auditor_status': auditor_status,
                'auditor_standard_id': auditor_standard_id,
                "_token": "{{ csrf_token() }}",
            },
            dataType: "json",
            cache: true,
            success: function (response) {
                if (response.status == 'success') {
                    toastr['success']("Status Updated Successfully.");
                    // var status = response.data.auditor_standard.status.charAt(0).toUpperCase() + response.data.auditor_standard.status.slice(1);
                    // $('#changed-auditor-status'+auditor_standard_id+'').text(status);

                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });

    });


</script>
