<div id="editStandardFoodCodesModal" class="modal fade in codesModal" role="dialog">
    <div class="modal-dialog modal-dialog2">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="#" method="post" id="editStandardFoodCodesForm">
                <input type="hidden" name="auditor_standard_food_code_id" value="{{$auditorStandardFoodCode->id}}">
                <div class="modal-header">
                    <h4>Codes</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body modal-body2">
                    <div class="codesDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Accreditations</h3>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food_accreditations" name="food_accreditation_id">
                                        <option value="">Select Accreditation</option>
                                        @foreach($allAccreditations as $accreditations)
                                            <option value="{{$accreditations->id}}" {{$accreditations->id ==$auditorStandardFoodCode->accreditation_id ? 'selected': ''}}>{{ $accreditations->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row"  style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Food Code</h3>
                                </div>
                                {{-- <div class="col-md-6 floting text-right">
                                     <i class="fa fa-plus-circle text-lg" aria-hidden="true"></i>
                                 </div>--}}
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food-category" name="food_category_code">
                                        <option value="">Select Food Code</option>
                                        @foreach($foodCategories as $foodCategory)
                                            <option value="{{$foodCategory->id}}" {{$foodCategory->id ==
                                            $auditorStandardFoodCode->food_category_code ? 'selected': ''}}>
                                                {{$foodCategory->code}} | {{$foodCategory->name}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row "  style="margin-top: 13px;">
                            <div class="col-md-12">
                                <div class="col-md-6 floting">
                                    <h3>Food Sub-Code</h3>
                                </div>
                                {{-- <div class="col-md-6 floting text-right">
                                     <i class="fa fa-plus-circle text-lg" aria-hidden="true"></i>
                                 </div>--}}
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <select class="form-control food-sub-category" name="food_sub_category_code">
                                        <option value="{{$auditorStandardFoodCode->food_sub_category_code}}">
                                            {{$auditorStandardFoodCode->foodsubcategory->code}} |
                                            {{$auditorStandardFoodCode->foodsubcategory->name}}
                                        </option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer code_footer">
                        <div class="col-md-12">
                            <div class="col-md-2 sbmt_btn floting">
                                <button type="button" class="btn btn-primary updateDataBtn">Submit</button>
                            </div>
                            <div class="col-md-2 cncl_btn floting">
                                <button type="button" class="btn " name="cancel" data-dismiss="modal">Cancel</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(".yearfield").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });
    $('.icheck-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%' // optional
    });


    $('.food-category').change(function () {
        form = $(this).closest('form');
        node = $(this);
        node_to_modify = '.food-sub-category';
        var food_category = $(this).val();
        var request = "food_category_id=" + food_category;

        if (food_category !== '') {
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.foodSubCategoryByFoodCategory') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        var html = "";
                        $.each(response.data.foodsubcategories, function (i, obj) {

                            html += '<option value="' + obj.id + '">' + obj.code + ' | ' + obj.name + '</option>';
                        });
                        $(node_to_modify).html(html);
                        sortMe($(node_to_modify).find('option'));
                        $(node_to_modify).prepend("<option value='' selected>Select Food Sub-Code</option>");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        } else {
            $(node_to_modify).html("<option value='' selected>Select Food Sub-Code</option>");
        }

    });


    $('.updateDataBtn').on("click", function () {
        /* e.preventDefault();*/
        var form = $('#editStandardFoodCodesForm')[0];
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('technical-expert.standards.food.code.update') }}",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success: function (response) {

                ajaxResponseHandler(response, form);
                if (response.status == "success" || response.status == '200') {
                    technicalExpertStandardFoodCodesView(response.data.auditor_standard_id);
                    $("#editStandardFoodCodesModal").modal('hide');
                }

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });
    });
    enable_cb();
    disable_cb();
    $('.Non-Accredited').click(enable_cb);
    $('.acceditations').click(disable_cb);

    function enable_cb() {

        if ($('.Non-Accredited').is(':checked')) {
            $('.acceditations').attr('disabled', true).removeAttr('checked');
        } else {
            $('.acceditations').removeAttr('disabled');
        }
    }

    function disable_cb() {
        if ($('.acceditations').is(':checked')) {
            $('.Non-Accredited').attr('disabled', true).removeAttr('checked');
        } else {
            $('.Non-Accredited').removeAttr('disabled');
        }
    }
</script>