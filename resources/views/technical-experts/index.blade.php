@extends('layouts.master')
@section('title', "Technical Expert  List")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="col-sm-6 floting">
                            <h1>Technical Experts</h1>
                        </div>
                        <div class="col-sm-6 floting">

                            @can('create_technical_experts')
                                <a href="{{route('technical-expert.create')}}" class="add_comp"><label
                                            class="text-capitalize"><i class="fa fa-plus-circle text-lg"
                                                                       aria-hidden="true"></i> Add New
                                        Technical Expert</label></a>
                            @endcan

                        </div>
                    </div>

                    <div class="col-sm-6">
                        {{ Breadcrumbs::render('technical-expert') }}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of Technical Experts</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="8%;">Sr. #</th>
                                    <th>Technical Expert Name</th>
                                    <th>Standard(s)</th>
                                    <th>Country</th>
                                    <th>Profile Status</th>
                                    <th>Completion Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($technicalExperts as $key => $technicalExpert)
                                    <tr>

                                        <td>{{$key + 1}}</td>
                                        <td>{{$technicalExpert->fullName()}}</td>
                                        <td>
                                            @foreach($technicalExpertStandards[$key] as $technicalExpertStandard)
                                                {!! $technicalExpertStandard->standard->name !!}

                                                @foreach(\App\AuditorStandardGrade::where('auditor_standard_id',$technicalExpertStandard->id)->where('status','current')->get() as $auditorStandardGrade)
                                                    ({!! $auditorStandardGrade->grade->short_name !!} ,
                                                    <span> {{ucfirst($technicalExpertStandard->status)}}</span>)
                                                @endforeach
                                                <br>
                                            @endforeach

                                            {{--                                        {!! $auditor->auditorStandards->pluck('number')->implode('<br>') !!}--}}
                                        </td>
                                        <td>{{ $technicalExpert->country->name ?? '--' }}</td>
                                        <td>{{ (is_null($technicalExpert->auditor_status) || $technicalExpert->auditor_status == 'inactive') ? 'Inactive' : ucfirst($technicalExpert->auditor_status) }}</td>
                                        <td>{{ ucfirst($technicalExpert->form_progress) }}%</td>
                                        <td>
                                            <ul class="data_list">

                                                @can('show_technical_experts')
                                                    <li>

                                                        <a href="{{route('technical-expert.show', $technicalExpert->id)}}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>

                                                    </li>
                                                @endcan
                                                @can('edit_technical_experts')
                                                    <li>

                                                        <a href="{{route('technical-expert.edit', $technicalExpert->id)}}"><i
                                                                    class="fa fa-pencil"></i></a>

                                                    </li>
                                                @endcan
                                                @can('delete_technical_experts')
                                                    <li>

                                                        <a href="{{route('technical-expert.delete', $technicalExpert->id)}}"
                                                           onclick="return confirm('Are you sure you want to delete Technical Expert?')">
                                                            <i class="fa fa-close"></i></a>

                                                    </li>
                                                @endcan


                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}"> --}}
    <style type="text/css">
        button.rm-inline-button {
            box-shadow: 0px 0px 0px transparent;
            border: 0px solid transparent;
            text-shadow: 0px 0px 0px transparent;
            background-color: transparent;
        }

        button.rm-inline-button:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#example1').dataTable();
        });
    </script>
@endpush