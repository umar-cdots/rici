@extends('layouts.master')
@section('title', "Edit Technical Expert")

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper custom_cont_wrapper">

        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark text-center dashboard_heading">Technical Expert <span>Profile</span>
                        </h1>
                    </div>

                </div>


                <div class="row text-center mrgn">
                    <div class="col-md-4 text-left"><h3 class="profile_name">{{$auditor->fullName()}}</h3></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-2">

                    </div>
                    @if(auth()->user()->getRoleNames()[0] == 'super admin' || auth()->user()->getRoleNames()[0] == 'scheme manager')



                        <div class="col-md-2 text-right">

                            <input type="checkbox"
                                   value="{{($auditor->auditor_status == 'active') ? 'on' : 'off'}}"
                                   {{$auditor->auditor_status == 'active' ? 'checked': ''}} data-toggle="toggle"
                                   data-on="Active"
                                   data-off="Inactive"
                                   data-onstyle="success"
                                   name="auditor_status"
                                   id="auditor_status"
                                   data-offstyle="danger" onchange="updateTechnicalExpertStatus({{$auditor->id}})">
                            {{--                            --}}
                            {{--                            <input type="checkbox"--}}
                            {{--                                   {{($auditor->auditor_status == 'active') ? 'checked' : ''}} data-toggle="toggle"--}}
                            {{--                                   data-on="Active" data-off="Inactive" data-onstyle="success"--}}
                            {{--                                   data-offstyle="danger"--}}
                            {{--                                   onchange="updateTechnicalExpertStatus({{$auditor->id}})">--}}
                        </div>

                    @endif
                    <div class="clearfix"></div>
                </div>

                <div class="card card-primary mrgn comp_det_view ">

                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3">Technical Expert Profile </h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item"><a class="nav-link active show" href="#aud_info" data-toggle="tab">Basic
                                    Info</a></li>
                            <li class="nav-item"><a class="nav-link" href="#eduTab" data-toggle="tab">Education</a></li>
                            <li class="nav-item"><a class="nav-link" href="#othr" data-toggle="tab">Others</a></li>
                            <li class="nav-item"><a class="nav-link" href="#standardsTab"
                                                    data-toggle="tab">Standards</a></li>

                        </ul>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <div class="card-body card_cutom">
                        <div class="tab-content">
                            <div class="tab-pane active" id="aud_info">

                                <form action="#" method="post" id="step1" autocomplete="off"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="step" value="1">
                                    <input type="hidden" class="auditor-id" name="auditor_id" value="{{$auditor->id}}">
                                    <div class="row">
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>First Name*</label>
                                                <input type="text" class="form-control" name="first_name"
                                                       value="{{$auditor->first_name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Last Name*</label>
                                                <input type="text" class="form-control" name="last_name"
                                                       value="{{$auditor->last_name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email"
                                                       value="{{$auditor->email}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Working Region*</label>
                                                <select name="region_id" class="form-control regions">
                                                    <option value="">Select Region</option>
                                                    @foreach($regions as $region)
                                                        <option value="{{$region->id}}"
                                                                {{$auditor->region_id == $region->id ? 'selected': ''}}>
                                                            {{$region->title}}
                                                        </option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Resident Country*</label>
                                                <select name="country_id" class="form-control countries">
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}"
                                                                {{$auditor->country_id == $country->id ? 'selected': ''}}>
                                                            {{$country->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Resident City*</label>
                                                <select name="city_id" class="form-control cities">
                                                    @foreach($cities as $city)
                                                        <option value="{{$city->id}}"
                                                                {{$auditor->city_id == $city->id ? 'selected': ''}}>
                                                            {{$city->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Phone Number*</label>
                                                <div class="row">

                                                    <div class="col-md-4" style="padding-right: 0;">
                                                        <input type="text" class="form-control"
                                                               value="{{ $auditor->country->phone_code ?? '' }}"
                                                               id="country_code" name="country_code" readonly>
                                                    </div>
                                                    <div class="col-md-8" style="padding-left: 0;">
                                                        <div class="form-group mb-0">

                                                            <input type="text" class="form-control popFld phone"
                                                                   name="phone" value="{{ltrim($auditor->phone, '0')}}"
                                                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--                                            <input type="tel" class="form-control popFld phone" name="phone"--}}
                                                {{--                                                   required value="{{$auditor->phone}}">--}}
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Landline Number</label>
                                                <input type="tel" class="form-control" name="landline"
                                                       value="{{$auditor->landline}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Postal Address*</label>
                                            <input type="text" class="form-control" name="postal_address"
                                                   value="{{ $auditor->postal_address }}">
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 floting">
                                            <label>Date of Birth*</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control dob active"
                                                       placeholder="Enter Date of Birth"
                                                       required name="dob" value="{{$auditor->dob}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Language:*</label>

                                                <select class="form-control languages" name="language_id[]"
                                                        multiple="multiple">
                                                    <option value="">Select Language</option>
                                                    @if($languages->count() > 0)
                                                        @foreach($languages as $language)
                                                            <option value="{{$language->id}}"
                                                                    {{$auditor->languages->contains($language->id) == 'true' ? 'selected': ''
                                                                   }}
                                                            >{{$language->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Overall Work Exp. (Years):*</label>
                                                <input type="number" class="form-control" name="working_experience"
                                                       step=0.1 min="0.1"
                                                       value="{{$auditor->working_experience}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Main Education:*</label>
                                                <input type="text" class="form-control" name="main_education"
                                                       value="{{$auditor->main_education}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2 floting">
                                            <div class="form-group">
                                                <label>Nationality:*</label>
                                                <select name="nationality_id" class="form-control nationalities"
                                                        required>
                                                    <option value="">Select Nationality</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}" {{$auditor->nationality_id == $country->id ? 'selected': ''}}>
                                                            {{$country->name}}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Job status</label>
                                            <div class="form-group">

                                                <div class="col-md-6 floting">
                                                    <label class="job_status">
                                                        <input type="radio" name="job_status" class="icheck-blue"
                                                               required value="full_time"
                                                                {{$auditor->job_status == 'full_time' ? 'checked': ''}}>
                                                        Full Time</label>
                                                </div>
                                                <div class="col-md-6 floting">
                                                    <label class="job_status">
                                                        <input type="radio" name="job_status" class="icheck-blue"
                                                               required value="part_time"
                                                                {{$auditor->job_status == 'part_time' ? 'checked': ''}}>
                                                        Part Time</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <label>Profile Remarks*</label>
                                        <textarea name="profile_remarks" rows="3"
                                                  class="form-control">{{$auditor->profile_remarks}}</textarea>
                                    </div>

                                    <div class="col-md-12 mrgn">
                                        <div class="col-md-3 floting"></div>

                                        <div class="col-md-5 floting">
                                            <div class="form-group">
                                                <button type="submit"
                                                        class="btn btn-block btn-secondary btn_save step1Savebtn">Save &
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting"></div>

                                    </div>

                                </form>

                            </div>
                            <div class="tab-pane" id="eduTab">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Formal Education</h4>
                                        <div id="formal-education">

                                        </div>
                                        <div id="edit-education-modal-container">

                                        </div>
                                    </div>


                                </div>
                            {{--                                <div class="row">--}}
                            {{--                                    <div class="col-md-12">--}}
                            {{--                                        <h4>Other Documents</h4>--}}

                            {{--                                        <div id="auditor-documents">--}}

                            {{--                                        </div>--}}

                            {{--                                        <div id="edit-document-modal-container">--}}

                            {{--                                        </div>--}}
                            {{--                                    </div>--}}


                            {{--                                </div>--}}

                            <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save cont">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="othr">

                                <div class="row">
                                    <div class="col-md-12">

                                        <h4>Confidentality Agreement / Joining Date</h4>

                                        <div id="technical-confidentiality-agreement">

                                        </div>


                                        <div id="edit-agreement-modal-container">

                                        </div>
                                    </div>
                                </div>

                            {{--                                <div class="row">--}}
                            {{--                                    <div class="col-md-12">--}}
                            {{--                                        <h4>Employment History</h4>--}}
                            {{--                                        <div id="emp-history">--}}

                            {{--                                        </div>--}}

                            {{--                                        <div id="edit-emp-history-modal-container">--}}

                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}

                            <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save cont">
                                                        Next
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane " id="standardsTab">


                                <h3 class="text-center card-header">Standards
                                    <a href="#" class="edit_comp" id="addStd"
                                       onclick="getAddTechnicalExpertStandardsModal()">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        Edit/Remove
                                    </a>
                                </h3>

                                <div id="addAuditorStandardModalContainer">

                                </div>
                                <div class="tab-pane active" id="auditor-standards-tabs">

                                </div>

                                <!-- Next Previous Button -->
                                <div class="row">
                                    <div class="col-md-12 mrgn">
                                        <div class="row justify-content-center">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-block btn-secondary btn_save prev">
                                                        Previous
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <a href="#"
                                                       class="btn btn-block btn-secondary btn_save cont finalSave">
                                                        Save
                                                    </a>
                                                    {{--                                                    <a href="{{route('auditors.index')}}" class="btn btn-block btn-secondary btn_save cont">--}}
                                                    {{--                                                        Finish and Save--}}
                                                    {{--                                                    </a>--}}
                                                </div>
                                            </div>
                                            @if(auth()->user()->user_type == 'operation_manager')
                                                @if($auditorStandardCount > 0)
                                                    @if($notification_count > 0)
                                                        {{--                                                    <div class="col-md-3">--}}
                                                        {{--                                                        <div class="form-group">--}}
                                                        {{--                                                            <a href="javascript:void(0)"--}}
                                                        {{--                                                               class="btn btn-block btn-secondary btn_save cont">--}}
                                                        {{--                                                                Already Sent For Approval--}}
                                                        {{--                                                            </a>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}
                                                    @else
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <a href="javascript:void(0)"
                                                                   class="btn btn-block btn-secondary btn_save cont sendForApproval">
                                                                    Send For Approval
                                                                </a>
                                                            </div>
                                                        </div>

                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
    </div>



@endsection
@push('modals')
    <div class="modal fade" id="sendPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form id="comment-form" action="#" method="post">
                <input type="hidden" name="standard_id" id="standard_id" value="0">
                <input type="hidden" name="auditorStandardStatus" id="auditorStandardStatus" value="">
                <div id="aj-ids"></div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Comments</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><strong>Add Comment</strong></label>
                                    <textarea rows="5" class="form-control" name="remarks" id="remarks"></textarea>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0)"
                           class="btn btn-block btn-secondary btn_save cont finalSentForApproval">
                            Send
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endpush
@push('scripts')
    <script type="text/javascript">
        special_dropdown = [];
        special_dropdown_multiple = [];

    </script>

@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/intlTelInput/css/intlTelInput.min.css') }}">

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: black;
        }

        .select2-container--default .select2-selection--single {
            height: 37px;
        }

        .select2-container--default .select2-selection--multiple {
            height: 37px;
        }

        .disable-save {
            pointer-events: none;
            opacity: 1.4;
        }

    </style>


@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/intlTelInput/js/intlTelInput.min.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('plugins/bootstrap-multiselect/js/bootstrap-multiselect.js')}}"></script>--}}

    <script>
        // $(document).ready(function () {

        {{--var intlTelInputConfig = {--}}
        {{--    initialCountry: "auto",--}}
        {{--    autoPlaceholder: false,--}}
        {{--    geoIpLookup: function (callback) {--}}
        {{--        $.get("{{ route('ajax.ipInfo') }}", function () {--}}
        {{--        }, "jsonp").always(function (resp) {--}}
        {{--            var countryCode = (resp && resp.country) ? resp.country : "";--}}
        {{--            callback(countryCode);--}}
        {{--        });--}}
        {{--    },--}}
        {{--    utilsScript: $.fn.intlTelInput.loadUtils("{{ asset('plugins/intlTelInput/js/utils.js') }}"), // just for formatting/placeholders etc--}}
        {{--    blur: function () {--}}

        {{--        console.log($(this));--}}
        {{--    }--}}
        {{--};--}}
        {{--// console.log(intlTelInputConfig);--}}
        {{--$(".phone").intlTelInput(intlTelInputConfig);--}}
        {{--$(".landline").intlTelInput(intlTelInputConfig);--}}
        {{--setTimeout(function () {--}}
        {{--    $('.landline,.phone').trigger('blur');--}}
        {{--}, 2000);--}}

        {{--// });--}}


        {{--$('.landline,.phone').on('keyup blur', function () {--}}
        {{--    // alert('azeem')--}}
        {{--    var number = $(this).intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);--}}
        {{--    if (number == '' || !$(this).intlTelInput("isValidNumber")) {--}}
        {{--        has_error = true;--}}
        {{--        $(this).closest('.form-group').addClass('text-danger');--}}
        {{--        $(this).addClass('is-invalid');--}}
        {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
        {{--        $(this).closest('.form-group').append('<small class="text-danger" style="float:left">Invalid Contact Number.</small>');--}}
        {{--    } else {--}}
        {{--        $(this).val(number);--}}
        {{--        $(this).closest('.form-group').removeClass('text-danger');--}}
        {{--        $(this).removeClass('is-invalid');--}}
        {{--        $(this).closest('.form-group').find('small.text-danger').remove();--}}
        {{--    }--}}
        {{--});--}}

        {{--var intlTelInputConfig = {--}}
        {{--    initialCountry: "auto",--}}
        {{--    geoIpLookup: function (callback) {--}}
        {{--        $.get("{{ route('ajax.ipInfo') }}", function () {--}}
        {{--        }, "jsonp").always(function (resp) {--}}
        {{--            var countryCode = (resp && resp.country) ? resp.country : "";--}}
        {{--            callback(countryCode);--}}
        {{--        });--}}
        {{--    },--}}
        {{--    utilsScript: "{{ asset('plugins/intlTelInput/js/utils.js') }}", // just for formatting/placeholders etc--}}
        {{--    blur: function () {--}}
        {{--        console.log($(this));--}}
        {{--    }--}}
        {{--};--}}

        {{--$(".phone").intlTelInput(intlTelInputConfig);--}}
        {{--$(".landline").intlTelInput(intlTelInputConfig);--}}
        $('.datefield').datepicker();
        $('.dob').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.languages').select2({
            placeholder: 'Select language(s)',
            width: "100%"
        });
        $('.nationalities').select2({
            placeholder: 'Select Nationality'
        });
        $('.cities').select2({
            placeholder: 'Select City'
        });
        $('.countries').select2({
            placeholder: 'Select Country'
        });

        $(".yearfield").datepicker({
            format: "yyyy",
            startView: 'decade',
            minView: 'decade',
            viewSelect: 'decade',
            autoclose: true,
        });

        jQuery('#addStd').on('click', function () {
            jQuery('#stdMdl').modal('toggle');
        });
        /* $('.stopProgation').click(function (event) {
             event.stopPropagation();
         })*/


    </script>


    <!-- Make tabs persistent -->
    <script>
        $(document).ready(function () {
            // show active tab on reload
            if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

            // remember the hash in the URL without jumping
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if (history.pushState) {
                    history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
                } else {
                    location.hash = '#' + $(e.target).attr('href').substr(1);
                }
            });
        });
    </script>


    <script type="text/javascript">
        var auditorId = $(".auditor-id").val();
        String.prototype.ucFirst = function () {
            return this.substring(0, 1).toUpperCase() + this.substring(1, this.length);
        };

        $(document).ready(function () {
            var auditorId = $(".auditor-id").val();

            if (auditorId !== '') {
                getTechnicalExpertFormalEducationView(auditorId);
                getTechnicalExpertAgreementView(auditorId);
                getTechnicalExpertStandardsTabs(auditorId);
            }

        });


        function updateAuditorStatus(auditorId) {
            var url = "{{ route('auditors.update.status', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }


        $('.countries').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var country_id = $(this).val();
            var request = "country_id=" + country_id;

            if (country_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.countryCitiesResidentials') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        ajaxResponseHandler(response);
                        if (response.status == "success") {
                            var html = "";
                            $('#country_code').val(response.data.country.phone_code);
                            $.each(response.data.country_cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        $('#name,#country_id').on('keyup change', function () {
            form = $(this).closest('form');
            node = $(this);
            var name = $('#name').val();
            var country_id = $('#country_id').val();
            var request = "name=" + name + "&country_id=" + country_id;

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.companyNameExist') }}",
                data: request,
                dataType: "json",
                cache: true,
                global: false,
                success: function (response) {
                    ajaxResponseHandler(response);
                    if (response.status == "success") {
                        if (response.data.already_exist) {
                            var standard_message = 'With Standards: ';
                            $.each(response.data.company_standards, function (i, obj) {
                                standard_message += obj.standard.number + ', ';
                            });
                            $('.standard_selection').show();
                            if (!confirm("Company Name Already Exists. " + standard_message + ". Do You Want to Create Another?")) {
                                $(node).val('');
                            } else {
                                $.each(response.data.company_standards, function (i, obj) {
                                    $('.standard_selection[data-standard-number=""]').hide();
                                });
                            }
                        } else {
                            $('.standard_selection').show();
                        }
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });


        $('#step1').submit(function (e) {
            e.preventDefault();
            var form = $('#step1')[0];
            var formData = new FormData(form);
            var auditor_status = $('#auditor_status').val();



            if (auditor_status == 'on') {
                auditor_status = 'active';
            } else {
                auditor_status = 'inactive';
            }

            formData.append('auditor_status', auditor_status);
            $.ajax({
                type: "POST",
                url: "{{ route('technical-expert.store') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        $('a[href="#eduTab"]').tab('show');
                        $('form').append('<input type="hidden" name="auditor_id" value="' + response.data.auditor_id + '">');
                        $('form').append('<input type="hidden" name="user_id" value="' + response.data.user_id + '">');
                        $('.auditor-id').val(response.data.auditor_id);
                        $('.user-id').val(response.data.user_id);
                        $('.profile_name').html(response.data.full_name);

                        getTechnicalExpertFormalEducationView(response.data.auditor_id);
                        getTechnicalExpertAgreementView(response.data.auditor_id);
                        getTechnicalExpertStandardsTabs(response.data.auditor_id);

                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function getAddTechnicalExpertStandardsModal() {
            var url = "{{ route('technical-expert.standards.modal.index', ['AUDITOR_ID' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#addAuditorStandardModalContainer").html(response);
                    $("#addAuditorStandardsModal").modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function getTechnicalExpertFormalEducationView(auditorId) {
            var url = "{{ route('technical-expert.formal.education.index', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#formal-education").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function getTechnicalExpertAgreementView(auditorId) {
            var url = "{{ route('technical-expert.agreement.index', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#technical-confidentiality-agreement").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function getTechnicalExpertStandardsTabs(auditorId) {
            var url = "{{ route('technical-expert.standards.view', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    $("#auditor-standards-tabs").html(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function updateTechnicalExpertStatus(auditorId) {
            var url = "{{ route('technical-expert.update.status', ['auditor_id' => "AUDITOR_ID"]) }}".replace("AUDITOR_ID", auditorId);
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    successResponseHandler(response);
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }


    </script>

    <script>
        $('.cont').click(function () {
            var nextId = $(this).parents('.tab-pane').next().attr("id") || 'pro_info';

            $('[href="#' + nextId + '"]').tab('show');
        });

        $('.prev').click(function () {
            var nextId = $(this).parents('.tab-pane').prev().attr("id") || 'pro_info';

            $('[href="#' + nextId + '"]').tab('show');
        });


    </script>

    <script>
        $(document).ready(function () {
            // show active tab on reload
            if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

            // remember the hash in the URL without jumping
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if (history.pushState) {
                    history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
                } else {
                    location.hash = '#' + $(e.target).attr('href').substr(1);
                }
            });
        });

        $('.finalSave').click(function () {

            var aTab;
            var tabs = document.getElementsByClassName('tabs-data');
            var id;
            var foodCodes;
            var energyCodes;
            var codes;
            var grades;

            // var aud_id =document.getElementsByClassName('auditor-id').val();


            if (tabs.length > 0) {
                for (var i = tabs.length; i--;) {

                    if (tabs[i].classList.contains('active')) {
                        aTab = tabs[i];
                    }
                }
                if (document.getElementsByClassName('auditor-standard-food-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    foodCodes = document.getElementsByClassName('auditor-standard-food-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else if (document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    energyCodes = document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else {
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                }


                if (foodCodes && (codes > 0 && foodCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.finish') }}",
                        data: {type: 'edit', auditor_id: auditorId},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else if (energyCodes && (codes > 0 && energyCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.finish') }}",
                        data: {type: 'edit', auditor_id: auditorId},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else if (codes > 0 && grades > 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.finish') }}",
                        data: {type: 'edit', auditor_id: auditorId},
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else {
                    //
                    toastr['error']("Atleast One Record are Mandatory.");

                }

            } else {
                //
                toastr['error']("Atleast One Standard is Mandatory  .");

            }


        });
    </script>


    <script>


        $('.sendForApproval').click(function () {
            $('#remarks').val('');
            $('#sendPopup').modal('show');
        });


        $('.finalSentForApproval').click(function () {

            var aTab;
            var tabs = document.getElementsByClassName('tabs-data');
            var id;
            var foodCodes;
            var energyCodes;
            var codes;
            var grades;
            var request = null;

            // var aud_id =document.getElementsByClassName('auditor-id').val();


            if (tabs.length > 0) {
                for (var i = tabs.length; i--;) {

                    if (tabs[i].classList.contains('active')) {
                        aTab = tabs[i];
                    }
                }
                if (document.getElementsByClassName('auditor-standard-food-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    foodCodes = document.getElementsByClassName('auditor-standard-food-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else if (document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-codes' + aTab.id).length > 0 && document.getElementsByClassName('auditor-standard-grades' + aTab.id).length > 0) {
                    energyCodes = document.getElementsByClassName('auditor-standard-energy-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                } else {
                    codes = document.getElementsByClassName('auditor-standard-codes' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                    grades = document.getElementsByClassName('auditor-standard-grades' + aTab.id)[0].getElementsByTagName('tbody')[0].children.length;
                }

                if ($('#auditorStandardStatus').val() == 'rejected' && $('#standard_id').val() != '') {
                    request = {
                        type: 'edit',
                        auditorStandardStatus: $('#auditorStandardStatus').val(),
                        standardId: $('#standard_id').val(),
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    }
                } else if ($('#auditorStandardStatus').val() == 'resent' && $('#standard_id').val() != '') {
                    request = {
                        type: 'edit',
                        auditorStandardStatus: $('#auditorStandardStatus').val(),
                        standardId: $('#standard_id').val(),
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    }
                } else if ($('#auditorStandardStatus').val() == 'approved' && $('#standard_id').val() != '') {
                    request = {
                        type: 'edit',
                        auditorStandardStatus: $('#auditorStandardStatus').val(),
                        standardId: $('#standard_id').val(),
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    }
                } else {
                    request = {
                        type: 'edit',
                        auditorStandardStatus: 'unapproved',
                        auditor_id: auditorId,
                        remarks: $('#remarks').val()
                    };
                }


                if (foodCodes && (codes > 0 && foodCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.send.for.approval') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else if (energyCodes && (codes > 0 && energyCodes > 0 && grades > 0)) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.send.for.approval') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else if (codes > 0 && grades > 0) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('technical-expert.send.for.approval') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            ajaxResponseHandler(response);
                            setTimeout(function () {
                                window.location = '{{ route('technical-expert.index') }}';
                            }, 3000);
                            // if (response.status == "success") {
                            //
                            // }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });

                } else {
                    //
                    toastr['error']("Atleast One Record are Mandatory.");

                }

            } else {
                //
                toastr['error']("Atleast One Standard is Mandatory  .");

            }


        });
    </script>




@endpush