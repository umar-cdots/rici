<!DOCTYPE html>
<html>
<head>
    <title>RICI - AJ Print Preview</title>
</head>

<style type="text/css">
    body {
        font-family: Gill Sans, Gill Sans MT, Myriad Pro, DejaVu Sans Condensed, Helvetica, Arial, " sans-serif";
        line-height: 20px;
        font-size: 10px;
    }


    .breakNow {
        page-break-inside: avoid;
        page-break-after: always;
    }

    .text-align-right {
        text-align: right;
    }

    p {
        margin: 0px;
        font-weight: normal;
    }

</style>
<body>
@php
    $image_path = '/img/logo.png';
@endphp
<table border="1px" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="17%">
            <img src="{{ public_path() . $image_path }}">
        </td>
        <td colspan="5" style="text-align: center;">
            <h1 style="font-size: 18px; ">
                <br>Audit Justification Form

                @if(isset($audit_type) && $audit_type == 'stage_2')
                    (Stage 1 or Stage 2)
                @else

                    ({{ ucfirst(str_replace('_' , ' ' , $audit_type)) }})
                @endif
                @if(!is_null($certificateStatus))
                    ({{ str_replace('_',' ', ucwords($certificateStatus->certificate_status)) }})
                @endif


            </h1>
        </td>
    </tr>
</table>
<br>
<table border="1px" cellspacing="0" align="center" width="100%">
    <tr>
        <td><b>Client Name:</b></td>
        <td align="left" colspan="5">{{ $company->name ?? '' }}</td>
        <th align="left">Job Number:</th>
        <td align="left">{{$aj_standard_stage->job_number}}</td>
    </tr>
    <tr>
        @if ($company->out_of_country_region_name === '' || is_null($company->out_of_country_region_name))
            <td><b>H.O.address</b></td>
            <td colspan="5"><sup>{{ $company->address ?? '' }},{{$company->city->name}}</sup></td>
            <td><b>Country:</b></td>
            <td><p>{{$company->country->name}}</p></td>
        @else
            <td><b>H.O.address</b></td>
            <td colspan="5"><sup>{{ $company->address ?? '' }}</sup></td>
            <td><b>Country:</b></td>
            <td><p>{{$company->out_of_country_region_name}}</p></td>
        @endif

    </tr>
    <tr>
        <td rowspan="4"><b>Client Contact:<br>Designation:<br>Tel #:<br>Email Address:</b></td>
        <td rowspan="4">{{$company->primaryContact->getNameWithTitle()}}<br>{{ $company->primaryContact->position }}
            <br>{{ $company->primaryContact->contact }}<br>{{$company->primaryContact->email}}</td>
        <td><b>Standard(s):</b></td>
        <td align="left" colspan="3">
            <p>
                {{ $standard_number->name }}
            </p>
        </td>

        <th align="left">
            Audit Frequency:
        </th>
        <th align="left">
            <p>
                @if(!empty($company->companyStandards))
                    @foreach($company->companyStandards as $c_standard)
                        @if($c_standard->standard_id === $standard_number->id)
                            {{ ucfirst($c_standard->surveillance_frequency)}}
                        @endif
                    @endforeach
                @endif
            </p>
        </th>
    </tr>
    <tr>
        <td>
            <b>Accreditations:</b>
        </td>
        <td align="left" colspan="3" id="codes-accreditations">
            @php
                $accreditation_array = [];
            @endphp
            @foreach($company->companyStandards as $c_standard)
                @if($c_standard->standard_id === $standard_number->id)
                    @if(!empty($c_standard->companyStandardCodes))
                        @foreach($c_standard->companyStandardCodes as $accreditation)
                            @if(is_null($accreditation_array) && count($accreditation_array)  == 0)
                                @php
                                    array_push($accreditation_array, $accreditation->accreditation->name);
                                @endphp
                                {{ $accreditation->accreditation->name ?? '' }}
                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                    ,
                                @endif
                            @else
                                @if(in_array( $accreditation->accreditation->name,$accreditation_array))
                                @else
                                    {{ $accreditation->accreditation->name ?? '' }}
                                    @php
                                        array_push($accreditation_array, $accreditation->accreditation->name);
                                    @endphp

                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                        ,
                                    @endif
                                @endif

                            @endif


                        @endforeach
                    @endif
                @endif
            @endforeach
        </td>

        <td>
            <b>Total Employees :</b>
        </td>
        <td>
            <p>  @if($company->childCompanies->count() > 0)
                    {{ $company->total_employees + $company->childCompanies->sum('total_employees') }}
                @else
                    {{$company->total_employees}}
                @endif</p>
        </td>
    </tr>
    <tr>
        <td><b>Complexity:</b></td>
            <td align="left" colspan="3">
            @if(!empty($company->companyStandards))
                @foreach($company->companyStandards as $standard)
                    @if($standard->standard_id === $standard_number->id)
                        {{ ucfirst($standard->proposed_complexity) }}
                    @endif

                @endforeach
            @endif
        </td>
        <td><b>Total Sites :</b></td>
        <td><p>{{$company->childCompanies->count() + 1}}</p></td>
    </tr>

    <tr>
        <td colspan="2">
            <p><b>IAF Code:</b>
                <span id="iaf-codes">
                @php
                    $iafs_array = [];
                @endphp
                    @foreach($company->companyStandards as $c_standard)
                        @if($c_standard->standard_id === $standard_number->id)
                            @if(!empty($c_standard->companyStandardCodes))
                                @foreach($c_standard->companyStandardCodes as $companyIaf)

                                    @if(is_null($iafs_array) && count($iafs_array)  == 0)
                                        @php
                                            array_push($iafs_array, $companyIaf->iaf->code);
                                        @endphp
                                        {{$companyIaf->iaf->code ?? '' }}
                                        @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                            ,
                                        @endif
                                    @else
                                        @if(in_array( $companyIaf->iaf->code,$iafs_array))
                                        @else
                                            {{$companyIaf->iaf->code ?? '' }}
                                            @php
                                                array_push($iafs_array, $companyIaf->iaf->code);
                                            @endphp
                                            @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                ,
                                            @endif
                                        @endif

                                    @endif

                                @endforeach
                            @endif
                        @endif
                    @endforeach

            </span>
            </p>
        </td>
        <td colspan="2">
            <p>
                <b>IAS Code:</b>
                <span id="ias-codes">
                @foreach($company->companyStandards as $c_standard)
                        @if($c_standard->standard_id === $standard_number->id)
                            @if(!empty($c_standard->companyStandardCodes))
                                @foreach($c_standard->companyStandardCodes as $companyIas)
                                    {{ $companyIas->ias->code ?? '' }}
                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                        ,
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
            </span>
            </p>
        </td>
        <td><b>Effective Employees :</b></td>
        <td><p> {{$company->effective_employees}}</p></td>
    </tr>
    <tr>
        <td>
            <b>Certification Scope:</b>
        </td>
        <td colspan="7">{{ $company->scope_to_certify }}
        </td>
    </tr>
{{--    <tr>--}}
{{--        <td ><b>MS :</b></td>--}}
{{--        <td colspan="7"><p> {{$company->multiple_sides ?? 1 }}</p></td>--}}
{{--    </tr>--}}
    @if(strtolower($standard_number->standardFamily->name) == 'food')
        <tr>
            <th>
                Food Code(s)
            </th>
            <td colspan="2">
                <span id="food-codes">
                    @foreach($company->companyStandards as $c_standard)
                        @if($c_standard->standard_id === $standard_number->id)
                            @if(!empty($c_standard->companyStandardFoodCodes))
                                @foreach($c_standard->companyStandardFoodCodes as $companyIas)
                                    {{ $companyIas->foodcategory->code ?? '' }}
                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                        ,
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                </span>
            </td>
            <th colspan="3">
                Food SubCode(s)
            </th>
            <td colspan="2">
                <span id="food-sub-codes">
                    @foreach($company->companyStandards as $c_standard)
                        @if($c_standard->standard_id === $standard_number->id)
                            @if(!empty($c_standard->companyStandardFoodCodes))
                                @foreach($c_standard->companyStandardFoodCodes as $companyIas)
                                    {{ $companyIas->foodsubcategory->code ?? '' }}
                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                        ,
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                </span>
            </td>
        </tr>
        @php

            $hcca_plans= 0;
               $certified_management = 'No';
            if(!empty($company->companyStandards)){
               foreach($company->companyStandards as $standard){
                   if($standard->standard_id === $standard_number->id){
                     $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->where('company_standards_id',$standard->id)->whereNull('deleted_at')->get();
                       foreach($standardQuestions as $index=>$questions){
                           if($index === 0){
                               $hcca_plans=$questions->answer;
                           }
                           elseif($index === 1){
                             $certified_management=$questions->answer;
                           }

                       }
                   }
               }
            }

        @endphp
        <tr>
            <th>
                HACCP Plan
            </th>
            <td colspan="2">
                <span id="food-codes">
                    {{$hcca_plans}}

                </span>
            </td>
            <th colspan="3">
                Certified To Other Management System?
            </th>
            <td colspan="2">
                <span id="food-sub-codes">
                      <p class="results_answer">{{$certified_management == "yes" ? 'Yes' : 'No'}}</p>
                </span>
            </td>
        </tr>
    @endif
    @if(strtolower($standard_number->standardFamily->name) == 'energy')
        <tr>
            <th>
                Energy Code(s)
            </th>
            <td colspan="7">
                 <span id="energy-codes">
                    @foreach($company->companyStandards as $c_standard)
                         @if($c_standard->standard_id === $standard_number->id)
                             @if(!empty($c_standard->companyStandardEnergyCodes))
                                 @foreach($c_standard->companyStandardEnergyCodes as $companyIas)
                                     {{ $companyIas->energycode->code ?? '' }}
                                     @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                         ,
                                     @endif
                                 @endforeach
                             @endif
                         @endif
                     @endforeach
                </span>
            </td>
        </tr>
    @endif
    @if(strtolower(str_replace(' ', '_', $standard_number->standardFamily->name)) == 'information_security')
        <tr>
            <td class="text-left">
               <b>As Per Statement of Applicability Version:</b>
            </td>
            <td colspan="7">
                 <span id="energy-codes">
                    @foreach($company->companyStandards as $c_standard)
                         @if($c_standard->standard_id === $standard_number->id)
                             {{$c_standard->SOA}}
                         @endif
                     @endforeach
                </span>
            </td>
        </tr>
    @endif
    <tr>
        <th>
            @if(isset($audit_type) && $audit_type == 'stage_2')
                @php
                    $approvalDateStage1 = \App\AJStandardStage::where('aj_standard_id',$aj_standard_stage->aj_standard_id)->where('audit_type','stage_1')->where('recycle',$aj_standard_stage->recycle)->first();
                @endphp
                Stage 1 Approval Date:<br><u>{{ date('d/m/Y',strtotime($approvalDateStage1->approval_date)) }}</u>
                <br>
                Stage 2 Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
            @else
                Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
            @endif

        </th>
        <th colspan="2">
            Valid Till Date:<br><br>

            <u>
                {{ !is_null($aj_standard_stage->valid_till_date) ? date('d-m-Y',strtotime($aj_standard_stage->valid_till_date)) : '-'}}
            </u>
        </th>
        <th colspan="3">
            @if(!is_null($approvalPerson))
                Approved By:<br><br><u>{{ucwords($approvalPerson->sentByUser->fullName())}}</u>
            @else
                @if(!is_null($defaultSchemeManager))
                    Approved By:<br><br><u>{{ucwords($defaultSchemeManager->user->fullName())}}</u>
                @endif
            @endif
        </th>
        <th colspan="2">
            @if(!is_null($approvalPerson))
                @php
                    $signature = '/uploads/scheme_managers/'.$approvalPerson->sentByUser->scheme_manager->signature_image;
                @endphp

                Signatures:<br><br><img
                        src="{{ public_path() . $signature }}" alt=""
                        width="60px">
            @else
                @if(!is_null($defaultSchemeManager))
                    @php
                        $signature = '/uploads/scheme_managers/'.$defaultSchemeManager->signature_image;
                    @endphp

                    Signatures:<br><br><img
                            src="{{ public_path() . $signature }}" alt=""
                            width="60px">
                @endif
            @endif
        </th>
    </tr>
</table>
<hr style="border-color: red" size="4" width="100%">

<table border="0px" cellspacing="0" cellpadding="2" align="center" width="100%" class="breakNow">
    <tr>
        <td>
            <span class="text-left">Form 03-4:  Rev 02/01.11.20</span>
        </td>

        <td class="text-align-right">
           <span>Print Date: {{ date('d-m-Y') }} &nbsp;
@if(!empty($company->childCompanies) && count($company->childCompanies) > 0)
                   @if(!empty($aj_standard_change))
                       Page 1/4
                   @else
                       Page 1/3
                   @endif
               @else
                   @if(!empty($aj_standard_change))
                       Page 1/3
                   @else
                       Page 1/2
                   @endif

               @endif
</span>
        </td>
    </tr>
</table>
<table border="1px" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="17%">
            <img src="{{ public_path() . $image_path }}">
        </td>
        <td colspan="5" style="text-align: center;">
            <h1 style="font-size: 18px; ">
                <br>Audit Justification Form
                @if(isset($audit_type) && $audit_type == 'stage_2')
                    (Stage 1 or Stage 2)
                @else
                    ({{ ucfirst(str_replace('_' , ' ' , $audit_type)) }})
                @endif
                @if(!is_null($certificateStatus))
                    ({{ str_replace('_',' ', ucwords($certificateStatus->certificate_status)) }})
                @endif
            </h1>
        </td>
    </tr>
</table>
<br>
<table border="1px" cellspacing="0" cellpadding="2" align="center" width="100%">

    <tr>
        <th align="center" colspan="8">
            Audit and Team Details :
        </th>
    </tr>
    <tr>
        <th>
            Type of Audit
        </th>
        <td colspan="5">

        </td>
        <th>
            OnSite ManDays

        </th>
        <th>
            OffSite ManDays
        </th>
    </tr>
    @if(isset($audit_type) && $audit_type == 'stage_2')
        <tr>
            <th>
                Stage 1
            </th>
            <td colspan="5">
                @php
                    $teams = [];
                @endphp
                @foreach($auditor_grades as $key1 => $grade)
                    @php
                        $teams[$grade->short_name] = [];
                    @endphp
                    @foreach($team_members_stage1 as $key2 =>  $team)
                        @if($grade->id == $team->grade_id )
                            @php $teams[$grade->short_name][] = $team->member_name; @endphp
                        @endif
                    @endforeach
                @endforeach
                @php


                        @endphp
                @foreach($teams as $key => $team)
                    @if ($team)
                        <b>{{$key}}</b> :
                        @foreach($team as $key1 => $te)
                            @if($te)
                                {{$te}} ,
                            @endif
                        @endforeach
                        <br>
                    @endif
                @endforeach

            </td>


            @if(!empty($mandays_stage_1) && count($mandays_stage_1) > 0)
                @foreach($mandays_stage_1 as $manday)

                    @if ($manday->audit_type ==  'stage_1')
                        <td align="center">{{$manday->onsite}}</td>
                        <td align="center">{{$manday->offsite}}</td>
                    @endif
                @endforeach
            @endif
        </tr>

        <tr>
            <th>
                @if(isset($audit_type) )
                    {{ ucfirst(str_replace('_' , ' ' , $audit_type)) }}
                @endif
            </th>
            <td colspan="5">
                @php
                    $teams = [];
                @endphp
                @foreach($auditor_grades as $key1 => $grade)
                    @php
                        $teams[$grade->short_name] = [];
                    @endphp
                    @foreach($team_members as $key2 =>  $team)
                        @if($grade->id == $team->grade_id )
                            @php $teams[$grade->short_name][] = $team->member_name; @endphp
                        @endif
                    @endforeach
                @endforeach
                @php


                        @endphp
                @foreach($teams as $key => $team)
                    @if ($team)
                        <b>{{$key}}</b> :
                        @foreach($team as $key1 => $te)
                            @if($te)
                                {{$te}} ,
                            @endif
                        @endforeach
                        <br>
                    @endif
                @endforeach
            </td>


            @foreach($aj_stages_manday as $manday)
                @if (isset($manday->audit_type))
                    @if ($manday->audit_type ==  $audit_type)
                        <td align="center">{{$manday->onsite}}</td>
                        <td align="center">{{$manday->offsite}}</td>
                    @endif
                @else
                    @foreach($manday as $mand)
                        @if ($mand->audit_type ==  $audit_type)
                            <td align="center">{{$mand->onsite}}</td>
                            <td align="center">{{$mand->offsite}}</td>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </tr>

        <tr>
            <th>
                Remarks Stage 1
            </th>
            <td colspan="5">
                @if(!empty($mandays_stage_1) && count($mandays_stage_1) >0)
                    {{ $mandays_stage_1[0]->manday_remarks != null ?  $mandays_stage_1[0]->manday_remarks : '' }}
                @endif
            </td>

            <th> Expected Start Date Stage 1</th>
            <td align="center"> {{!is_null($mandays_stage_1[0]->actual_expected_date) ? date('d-m-Y',strtotime($mandays_stage_1[0]->actual_expected_date)) : date('d-m-Y',strtotime($mandays_stage_1[0]->excepted_date))}}</td>

        </tr>
        <tr>
            <th>
                Remarks Stage 2
            </th>
            <td colspan="5">
                {{--                @if(!empty($mandays_stage_1) && count($mandays_stage_1) >0)--}}
                {{ $aj_standard_stage->manday_remarks != null ?  $aj_standard_stage->manday_remarks : '' }}
                {{--                @endif--}}
            </td>

            <th> Expected Start Date Stage 2</th>
            <td align="center">  {{ !is_null($aj_standard_stage->actual_expected_date) ? date('d-m-Y',strtotime($aj_standard_stage->actual_expected_date)) : date('d-m-Y',strtotime($aj_standard_stage->excepted_date))}}</td>

        </tr>

    @else
        <tr>
            <th>
                @if(isset($audit_type) )
                    {{ ucfirst(str_replace('_' , ' ' , $audit_type)) }}
                @endif
            </th>
            <td colspan="5">
                @php
                    $teams = [];
                @endphp
                @foreach($auditor_grades as $key1 => $grade)
                    @php
                        $teams[$grade->short_name] = [];
                    @endphp
                    @foreach($team_members as $key2 =>  $team)
                        @if($grade->id == $team->grade_id )
                            @php $teams[$grade->short_name][] = $team->member_name; @endphp
                        @endif
                    @endforeach
                @endforeach
                @php

                @endphp
                @foreach($teams as $key => $team)
                    @if ($team)
                        <b>{{$key}}</b> :
                        @foreach($team as $key1 => $te)
                            @if($te)
                                {{$te}} ,
                            @endif
                        @endforeach
                        <br>
                    @endif
                @endforeach

            </td>


            @foreach($aj_stages_manday as $manday)
                @if (isset($manday->audit_type))
                    @if ($manday->audit_type ==  $audit_type && $manday->recycle === $aj_standard_stage->recycle)
                        <td align="center">{{$manday->onsite}}</td>
                        <td align="center">{{$manday->offsite}}</td>
                    @endif
                @else
                    @foreach($manday as $mand)
                        @if ($mand->audit_type ==  $audit_type  && $mand->recycle === $aj_standard_stage->recycle)
                            <td align="center">{{$mand->onsite}}</td>
                            <td align="center">{{$mand->offsite}}</td>
                        @endif
                    @endforeach
                @endif
            @endforeach

        </tr>
        <tr>
            <th>
                Remarks
            </th>
            <td colspan="5">
                {{--                @if(!empty($mandays_stage_1) && count($mandays_stage_1) >0)--}}
                {{ $aj_standard_stage->manday_remarks != null ?  $aj_standard_stage->manday_remarks : '' }}
                {{--                @endif--}}
            </td>

            <th> Expected Start Date</th>
            <td align="center">  {{ !is_null($aj_standard_stage->actual_expected_date) ? date('d-m-Y',strtotime($aj_standard_stage->actual_expected_date)) : date('d-m-Y',strtotime($aj_standard_stage->excepted_date))}}</td>

        </tr>
    @endif


    <tr>
        <th>
            @if(isset($audit_type) && $audit_type == 'stage_2')
                @php
                    $approvalDateStage1 = \App\AJStandardStage::where('aj_standard_id',$aj_standard_stage->aj_standard_id)->where('audit_type','stage_1')->where('recycle',$aj_standard_stage->recycle)->first();
                @endphp
                Stage 1 Approval Date:<br><u>{{ date('d/m/Y',strtotime($approvalDateStage1->approval_date)) }}</u>
                <br>
                Stage 2 Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
            @else
                Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
            @endif

        </th>
        <th colspan="5">
            Valid Till Date:<br><br>

            <u>
                {{ !is_null($aj_standard_stage->valid_till_date) ? date('d-m-Y',strtotime($aj_standard_stage->valid_till_date)) : '-'}}
            </u>
        </th>
        <th>
            @if(!is_null($approvalPerson))
                Approved By:<br><br><u>{{ucwords($approvalPerson->sentByUser->fullName())}}</u>

            @else
                @if(!is_null($defaultSchemeManager))
                    Approved By:<br><br><u>{{ucwords($defaultSchemeManager->user->fullName())}}</u>
                @endif
            @endif

        </th>
        <th>
            @if(!is_null($approvalPerson))
                @php
                    $signature = '/uploads/scheme_managers/'.$approvalPerson->sentByUser->scheme_manager->signature_image;
                @endphp

                Signatures:<br><br><img
                        src="{{ public_path() . $signature }}" alt=""
                        width="60px">
            @else
                @if(!is_null($defaultSchemeManager))
                    @php
                        $signature = '/uploads/scheme_managers/'.$defaultSchemeManager->signature_image;
                    @endphp
                    Signatures:<br><br><img
                            src="{{ public_path() . $signature }}" alt=""
                            width="60px">
                @endif
            @endif
        </th>
    </tr>
</table>
<hr style="border-color: red" size="4" width="100%">


<table border="0px" cellspacing="0" cellpadding="2" align="center" width="100%"
       class="@if(!empty($company->childCompanies) && count($company->childCompanies) > 0) breakNow @else @if(!empty($aj_standard_change)) breakNow @endif @endif ">
    <tr>
        <td>
            <span class="text-left">Form 03-4:  Rev 02/01.11.20</span>
        </td>

        <td class="text-align-right">
<span>Print Date: {{ date('d-m-Y') }} &nbsp;
@if(!empty($company->childCompanies) && count($company->childCompanies) > 0)

        @if(!empty($aj_standard_change))
            Page 2/4
        @else
            Page 2/3
        @endif
    @else
        @if(!empty($aj_standard_change))
            Page 2/3
        @else
            Page 2/2
        @endif

    @endif
</span>
        </td>
    </tr>
</table>

@if(!empty($company->childCompanies) && count($company->childCompanies) > 0)
    <table border="1px" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td width="17%">
                <img src="{{ public_path() . $image_path }}">
            </td>
            <td colspan="5" style="text-align: center;">
                <h1 style="font-size: 18px; ">
                    <br>Audit Justification Form
                    @if(isset($audit_type) && $audit_type == 'stage_2')
                        (Stage 1 or Stage 2)
                    @else
                        ({{ ucfirst(str_replace('_' , ' ' , $audit_type)) }})
                    @endif
                    @if(!is_null($certificateStatus))
                        ({{ str_replace('_',' ', ucwords($certificateStatus->certificate_status)) }})
                    @endif
                </h1>
            </td>
        </tr>
    </table>
    <br>
    <table border="1px" cellspacing="0" cellpadding="2" width="100%" align="center">
        <tr>
            <td align="center">
                <b>Clients:</b>
            </td>
            <th>
                {{ $company->name ?? '' }}
            </th>
            <th colspan="6">
                Job Number:<br>{{$aj_standard_stage->job_number}}
            </th>
        </tr>
        <tr>
            <th>
                H.O. Address/Site No. 1:
            </th>
            <td colspan="7">
                <sup>{{ $company->address ?? '' }},{{$company->city->name}}</sup>
            </td>
        </tr>
        @if(!empty($company->childCompanies) && count($company->childCompanies) > 0)
            <tr>
                <th colspan="8">
                    Other Addresses
                </th>
            </tr>
            @foreach($company->childCompanies as $key=>$child_company)
                <tr>
                    <th>
                        Site No. {{$key +2}}:
                    </th>
                    <td colspan="7">
                        {{ $child_company->address }},{{$child_company->city->name }}
                    </td>
                </tr>
            @endforeach
        @endif
        <tr>
            <th>
                @if(isset($audit_type) && $audit_type == 'stage_2')
                    @php
                        $approvalDateStage1 = \App\AJStandardStage::where('aj_standard_id',$aj_standard_stage->aj_standard_id)->where('audit_type','stage_1')->where('recycle',$aj_standard_stage->recycle)->first();
                    @endphp
                    Stage 1 Approval Date:<br><u>{{ date('d/m/Y',strtotime($approvalDateStage1->approval_date)) }}</u>
                    <br>
                    Stage 2 Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
                @else
                    Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
                @endif

            </th>
            <th colspan="5">
                Valid Till Date:<br><br>

                <u>
                    {{ !is_null($aj_standard_stage->valid_till_date) ? date('d-m-Y',strtotime($aj_standard_stage->valid_till_date)) : '-'}}
                </u>
            </th>
            <th>

                @if(!is_null($approvalPerson))
                    Approved By:<br><br><u>{{ucwords($approvalPerson->sentByUser->fullName())}}</u>
                @else
                    @if(!is_null($defaultSchemeManager))
                        Approved By:<br><br><u>{{ucwords($defaultSchemeManager->user->fullName())}}</u>
                    @endif
                @endif
            </th>
            <th>
                @if(!is_null($approvalPerson))
                    @php
                        $signature = '/uploads/scheme_managers/'.$approvalPerson->sentByUser->scheme_manager->signature_image;
                    @endphp

                    Signatures:<br><br><img
                            src="{{ public_path() . $signature }}" alt=""
                            width="60px">
                @else
                    @if(!is_null($defaultSchemeManager))
                        @php
                            $signature = '/uploads/scheme_managers/'.$defaultSchemeManager->signature_image;
                        @endphp

                        Signatures:<br><br><img
                                src="{{ public_path() . $signature }}" alt=""
                                width="60px">
                    @endif
                @endif
            </th>
        </tr>
    </table>
    <table border="0px" cellspacing="0" cellpadding="2" align="center" width="100%">
        <tr>
            <td>
                <span class="text-left">Form 03-4:  Rev 02/01.11.20</span>
            </td>

            <td class="text-align-right">
<span>Print Date: {{ date('d-m-Y') }} &nbsp;
@if(!empty($company->childCompanies) && count($company->childCompanies) > 0)

        @if(!empty($aj_standard_change))
            Page 3/4
        @else
            Page 3/3
        @endif
    @endif
</span>
            </td>
        </tr>
    </table>
@endif


@if(!empty($aj_standard_change) && isset($audit_type) && ($audit_type === 'surveillance_1' || $audit_type === 'surveillance_2' || $audit_type === 'surveillance_3' || $audit_type === 'surveillance_4' || $audit_type === 'surveillance_5' || $audit_type === 'reaudit' ))
    <table border="1px" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td width="17%">
                <img src="{{ public_path() . $image_path }}">
            </td>
            <td colspan="5" style="text-align: center;">
                <h1 style="font-size: 18px; ">
                    <br>Audit Justification Form
                    @if(isset($audit_type) && $audit_type == 'stage_2')
                        (Stage 1 or Stage 2)
                    @else
                        ({{ ucfirst(str_replace('_' , ' ' , $audit_type)) }})
                    @endif
                    @if(!is_null($certificateStatus))
                        ({{ str_replace('_',' ', ucwords($certificateStatus->certificate_status)) }})
                    @endif
                </h1>
            </td>
        </tr>
    </table>
    <br>
    <table border="1px" cellspacing="0" cellpadding="2" align="center" width="100%">
        <tr>
            <th align="center" colspan="8">
                Change Status
            </th>
        </tr>
        <tr>
            <th>
                Questions
            </th>
            <th colspan="7">
                Answer
            </th>
        </tr>
        <tr>
            <th align="left">Is there any change in the name required</th>
            <td colspan="7">
                @if(!empty($aj_standard_change))
                    @if($aj_standard_change->name === 'yes')
                        yes
                    @else
                        no
                    @endif
                @else
                    no
                @endif
            </td>
        </tr>
        <tr>
            <th align="left">Is there any change in the scope required</th>
            <td colspan="7">
                @if(!empty($aj_standard_change))
                    @if($aj_standard_change->scope === 'yes')
                        yes
                    @else
                        no
                    @endif
                @else
                    no
                @endif
            </td>
        </tr>
        <tr>
            <th align="left">Is there any change in the address / number of locations required</th>
            <td colspan="7">
                @if(!empty($aj_standard_change))
                    @if($aj_standard_change->location === 'yes')
                        yes
                    @else
                        no
                    @endif
                @else
                    no
                @endif
            </td>


        </tr>
        @if(!empty($aj_standard_change))
            @if($aj_standard_change->name === 'yes' || $aj_standard_change->scope === 'yes' || $aj_standard_change->location === 'yes')
                <tr>
                    <th align="left">
                        Remarks
                    </th>
                    <td colspan="7">
                        {{ !empty($aj_standard_change->remarks) ? $aj_standard_change->remarks : 'N/A' }}
                    </td>
                </tr>
            @endif
        @endif
        <tr>
            <th>
                @if(isset($audit_type) && $audit_type == 'stage_2')
                    @php
                        $approvalDateStage1 = \App\AJStandardStage::where('aj_standard_id',$aj_standard_stage->aj_standard_id)->where('audit_type','stage_1')->where('recycle',$aj_standard_stage->recycle)->first();
                    @endphp
                    Stage 1 Approval Date:<br><u>{{ date('d/m/Y',strtotime($approvalDateStage1->approval_date)) }}</u>
                    <br>
                    Stage 2 Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
                @else
                    Approval Date:<br><u>{{ date('d/m/Y',strtotime($aj_standard_stage->approval_date)) }}</u>
                @endif

            </th>
            <th colspan="5">
                Valid Till Date:<br><br>

                <u>
                    {{ !is_null($aj_standard_stage->valid_till_date) ? date('d-m-Y',strtotime($aj_standard_stage->valid_till_date)) : '-'}}
                </u>
            </th>
            <th>
                @if(!is_null($approvalPerson))
                    Approved By:<br><br><u>{{ucwords($approvalPerson->sentByUser->fullName())}}</u>

                @else
                    @if(!is_null($defaultSchemeManager))
                        Approved By:<br><br><u>{{ucwords($defaultSchemeManager->user->fullName())}}</u>
                    @endif
                @endif

            </th>
            <th>
                @if(!is_null($approvalPerson))
                    @php
                        $signature = '/uploads/scheme_managers/'.$approvalPerson->sentByUser->scheme_manager->signature_image;
                    @endphp

                    Signatures:<br><br><img
                            src="{{ public_path() . $signature }}" alt=""
                            width="60px">
                @else
                    @if(!is_null($defaultSchemeManager))
                        @php
                            $signature = '/uploads/scheme_managers/'.$defaultSchemeManager->signature_image;
                        @endphp
                        Signatures:<br><br><img
                                src="{{ public_path() . $signature }}" alt=""
                                width="60px">
                    @endif
                @endif
            </th>
        </tr>
    </table>
    <hr style="border-color: red" size="4" width="100%">


    <table border="0px" cellspacing="0" cellpadding="2" align="center" width="100%">
        <tr>
            <td>
                <span class="text-left">Form 03-4:  Rev 02/01.11.20</span>
            </td>

            <td class="text-align-right">
<span>Print Date: {{ date('d-m-Y') }} &nbsp;
    @if(!empty($company->childCompanies) && count($company->childCompanies) > 0)
        Page 4/4
    @else
        Page 3/3
    @endif

</span>
            </td>
        </tr>
    </table>
@endif
</body>
</html>
