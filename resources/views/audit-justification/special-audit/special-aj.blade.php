@extends('layouts.master')
@section('title', "Special Audit Justification")

@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 359px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">SPECIAL <span>AUDIT</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">Add Special Audit</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" id="submitAJ" method="post" action="{{ route('justification.add.special') }}">
                        @csrf
                        <input type="hidden" name="company_id" value="{{ $company_id }}">
                        <input type="hidden" name="standard_number" value="{{$standard_number}}">
                        <div class="card-body card_cutom auditActivity">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="special_audit_type">Special Audit Type</label>
                                            <select name="special_audit_type" id="special_audit_type" class="form-control  @if($errors->has('special_audit_type')) is-invalid @endif">
                                                <option value="">Select an option</option>
                                                <option value="verification">Verification</option>
                                                <option value="scope_extension">Scope Extension</option>
                                                <option value="special_transition">Special Transition</option>
                                            </select>
                                            @if ($errors->has('special_audit_type'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('special_audit_type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="add_stage">Add after which stage</label>
                                            <select name="add_stage" id="add_stage" class="form-control  @if($errors->has('add_stage')) is-invalid @endif">
                                                <option value="">Select an option</option>
                                                <option value="stage_2">Stage II </option>
                                                <option value="surveillance_1">Surveillance 1</option>
                                                <option value="surveillance_2">Surveillance 2</option>
                                                <option value="reaudit">Re-audit</option>
{{--                                                <option value="surv_1_ater_reaudit">Surv 1 after re-audit</option>--}}
                                            </select>
                                            @if ($errors->has('add_stage'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('add_stage') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label for="special_audit_remarks">Remarks</label>
                                            <textarea rows="1" class="form-control  @if($errors->has('special_audit_remarks')) is-invalid @endif" name="special_audit_remarks" id="special_audit_remarks"></textarea>
                                            @if ($errors->has('special_audit_remarks'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('special_audit_remarks') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>



                            <div class="col-md-12 mrgn no_padding">
                                <div class="col-md-4 floting"></div>
                                <div class="col-md-4 floting"></div>
                                <div class="col-md-2 floting">
                                </div>
                                <div class="col-md-2 floting no_padding">
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-success btn_save">Save </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->



                    </form>

                </div>


            </div>


        </section>
        <!-- /.content -->
    </div>
@endsection

@push('styles')
@endpush

@push('scripts')

    <script>
        {{--function mainAjSubmit(type, e) {--}}
        {{--    --}}

        {{--    e.preventDefault();--}}
        {{--    var form = $('#submitAJ')[0];--}}
        {{--    var formData = new FormData(form);--}}
        {{--    var status = '';--}}
        {{--    if (type === 'save') {--}}
        {{--        status = "created";--}}
        {{--    }--}}

        {{--    formData.append('status', status);--}}

        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{ route('justification.add.special') }}",--}}
        {{--        enctype: 'multipart/form-data',--}}
        {{--        processData: false,--}}
        {{--        contentType: false,--}}
        {{--        cache: false,--}}
        {{--        data: formData,--}}
        {{--        success: function (response) {--}}
        {{--            --}}
        {{--            ajaxResponseHandler(response,form);--}}
        {{--           --}}

        {{--        },--}}
        {{--        error: function () {--}}
        {{--            toastr['error']("Something Went Wrong.");--}}
        {{--        }--}}
        {{--    });--}}


        {{--}--}}
    </script>
@endpush