@extends('layouts.master')
@section('title', "Special Audit Justification")

@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 608.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">Add <span>SPECIAL AUDIT JUSTIFICATION</span></h1>

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">
                            <div class="col-md-6 floting">COMPANY INFORMATION</div>

                            @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')

                                <div class="col-md-6 floting view_manday">
                                    <a href="{{ route('company.edit', $company->id) }}" target="_blank"><i
                                                class="fa fa-edit"></i>EDIT</a>
                                </div>
                            @endif
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="#" method="post">
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Client/Company Name</label>
                                        <p class="results_answer">{{ $company->name ?? '' }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Head Office</label>
                                        <p class="results_answer">{{ $company->address ?? '' }},{{ $company->city->name }},{{$company->country->name}}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Other Locations &nbsp;
                                            @if(!empty($company->childCompanies) && $company->childCompanies->count() > 2)
                                                <a href="#" data-toggle="modal" data-target="#viewAllAJ"><strong><i
                                                                class="far fa-eye"></i> View All</strong></a>
                                            @endif
                                        </label>
                                        <p class="results_answer">
                                            {{ $company->childCompanies->isNotEmpty() ? $company->childCompanies[0]->address : ''}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>AJ Creation Date</label>
                                        <input type="hidden" name="aj_date" value="{{date('d-m-Y')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Title &amp; Name</label>
                                        <p class="results_answer">{{ $company->primaryContact->getNameWithTitle() }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <p class="results_answer">{{ $company->primaryContact->position }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cell Phone</label>
                                        <p class="results_answer">{{ $company->primaryContact->contact }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Landline No. / Fax No.</label>
                                        <p class="results_answer">{{ $company->primaryContact->landline }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <p class="results_answer">{{ $company->primaryContact->email }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>No. of Sites</label>
                                        <p class="results_answer">{{ $company->childCompanies ?  $company->childCompanies->count() + 1 : 1 }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Total Employees</label>
                                        @if($company->childCompanies->count() > 0)
                                            <p class="results_answer">{{ $company->total_employees + $company->childCompanies->sum('total_employees') }}</p>
                                        @else
                                            <p class="results_answer">{{ $company->total_employees }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Effective Employees</label>
                                        <p class="results_answer">{{ $company->effective_employees }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
                <form role="form" id="main-aj-form" method="post">
                    <input type="hidden" name="aj_date" value="{{date('d-m-Y')}}">
                    <input type="hidden" value="{{ $company->id }}" name="company_id" id="company_id"/>
                    <input type="hidden" value="{{ $standard_number->id }}" name="standard_number"/>

                    @if(isset($aj_standard_stage))
                        <input type="hidden" value="{{ $aj_standard_stage->id }}" name="standard_stage_id">
                    @endif
                    <input type="hidden" value="{{ ($audit_type != '') ? $audit_type : '' }}" name="audit_type"/>
                    <input type="hidden" value="{{ ($team_audit_type != '') ? $team_audit_type : '' }}"
                           name="team_audit_type"/>
                    <input type="hidden" value="{{ $ims }}" name="is_ims">
                    @if(isset($std_number))
                        <input type="hidden" value="{{ $std_number }}" name="standard_number[]"/>
                    @endif

                    @if(isset($company_ims_standards))
                        @foreach($company_ims_standards as $standard)
                            <input type="hidden" value="{{ $standard->standard->name }}" name="standard_number[]"/>
                        @endforeach
                    @endif
                    <input type="hidden" value="{{ $combine_tracker}}" name="combine_tracker">
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">


                            <h3 class="card-title">
                                <div class="col-md-6 floting">STANDARDS INFORMATION</div>

                                @if(auth()->user()->user_type === 'scheme_manager')

                                    <div class="col-md-6 floting view_manday">
                                        <a href="{{ route('company.edit', $company->id) }}" target="_blank"><i
                                                    class="fa fa-edit"></i>EDIT</a>
                                    </div>
                                @endif
                            </h3>

                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-8 floting">
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>IAF Code</label>
                                            <p class="results_answer" id="iaf-codes">


                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->standard_id === $standard_number->id)
                                                        @if(!empty($c_standard->companyStandardCodes))
                                                            @foreach($c_standard->companyStandardCodes as $companyIaf)
                                                                {{ $companyIaf->iaf->code ?? '' }}
                                                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>IAS Code</label>
                                            <p class="results_answer" id="ias-codes">
                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->standard_id === $standard_number->id)
                                                        @if(!empty($c_standard->companyStandardCodes))
                                                            @foreach($c_standard->companyStandardCodes as $companyIas)
                                                                {{ $companyIas->ias->code ?? '' }}
                                                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Frequency</label>
                                            <p class="results_answer">
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            {{ ucfirst($c_standard->surveillance_frequency) }}
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Standards</label>
                                            <p class="results_answer">
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $standard)
                                                        @if($standard->standard_id === $standard_number->id)
                                                            {{ $standard->standard->name }}
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Codes Accreditation</label>
                                            <p class="results_answer" id="codes-accreditations">

                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->standard_id === $standard_number->id)
                                                        @if(!empty($c_standard->companyStandardCodes))
                                                            @foreach($c_standard->companyStandardCodes as $accreditation)
                                                                {{ $accreditation->accreditation->name ?? '' }}
                                                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Complexity</label>
                                            <p class="results_answer">
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $standard)
                                                        @if($standard->standard_id === $standard_number->id)
                                                            {{ ucfirst($standard->proposed_complexity) }}
                                                        @endif

                                                    @endforeach
                                                @endif</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label>Certification Scope</label>
                                        <textarea rows="3" class="form-control" name="certificate_scope"
                                                  id="certificate_scope" {{ (auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin') ? '':'readonly' }}
                                                  id="certificate_scope">{{ $aj_standard_stage->certificate_scope != null ?  $aj_standard_stage->certificate_scope : $company->scope_to_certify }}</textarea>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">

                                    @if(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Food')

                                        @php

                                            $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->get();

                                            foreach($standardQuestions as $index=>$questions){

                                                if($index === 0){
                                                    $hcca_plans=$questions->answer;

                                                }
                                                elseif($index === 1){
                                                  $certified_management=$questions->answer;
                                                }

                                            }

                                        @endphp

                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Category Code</label>
                                                <p class="results_answer" id="food-category-codes">


                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $companyIaf)
                                                                    {{ $companyIaf->foodcategory->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach

                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Category SubCode</label>
                                                <p class="results_answer" id="food-subcategory-codes">
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $companyIas)
                                                                    {{ $companyIas->foodsubcategory->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Code Accreditation</label>
                                                <p class="results_answer" id="food-accreditations">

                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $accreditation)
                                                                    {{ $accreditation->accreditation->name ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label> HACCP Plan </label>
                                                <p class="results_answer">{{ $hcca_plans }}</p>
                                            </div>
                                        </div>
                                    @elseif(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Energy Management')
                                        @php

                                            $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->get();

                                            foreach($standardQuestions as $index=>$questions){

                                                 if($index === 1){
                                                  $energy_consumption_in_tj=$questions->answer;

                                                }
                                                elseif($index === 3){
                                                  $no_of_energy_resources=$questions->answer;
                                                }
                                                elseif($index === 4){
                                                    $no_of_significant_energy=$questions->answer;
                                                }
                                            }

                                        @endphp
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Codes</label>
                                                <p class="results_answer" id="energy-codes">
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardEnergyCodes))
                                                                @foreach($c_standard->companyStandardEnergyCodes as $companyIas)
                                                                    {{ $companyIas->energycode->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Codes Accreditation</label>
                                                <p class="results_answer" id="energy-accreditations">

                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardEnergyCodes))
                                                                @foreach($c_standard->companyStandardEnergyCodes as $accreditation)
                                                                    {{ $accreditation->accreditation->name ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Consumption in TJ</label>
                                                <p class="results_answer">{{$energy_consumption_in_tj}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Number of energy sources</label>
                                                <p class="results_answer">{{$no_of_energy_resources}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Number of significant energy and uses (SEUs)</label>
                                                <p class="results_answer">{{$no_of_significant_energy}}</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                @if(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Food')
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label> Certified To Other Management System? </label>
                                            <p class="results_answer">{{$certified_management}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">
                                <div class="col-md-6 floting">MAN DAYS DETAILS</div>

                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')
                                    <div class="col-md-6 floting view_manday">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"
                                                                                data-target="#viewModal"
                                                                                data-toggle="modal"><i
                                                    class="fa fa-eye"></i> VIEW HISTORY</a></div>
                                @endif
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Man-Day Table</label>
                                        <table class="table table-bordered text-center manday_tbl">
                                            <tbody>
                                            <tr>
                                                <th>Audit Type</th>
                                                <th>Onsite</th>
                                                <th>Offsite</th>
                                            </tr>
                                            @foreach($aj_stages_manday as $manday)
                                                <tr>
                                                    <td>{{ $manday->getAuditType() }}</td>
                                                    <td>
                                                        <input id="input-onsite-{{ $loop->iteration }}"
                                                               name="onsite"

                                                               style="display:block; width: 60px; margin:auto;"
                                                               type="text"
                                                               class="form-control"
                                                               value="1"/>
                                                    </td>
                                                    <td>

                                                        <input id="input-offsite-{{ $loop->iteration }}"
                                                               name="offsite"

                                                               style="display:block; width: 60px; margin:auto;"
                                                               type="text"
                                                               class="form-control"
                                                               value="0">
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6 floting">
                                    <div class="form-group">
                                        <label>Remarks / Justification for reduction in man-days</label>
                                        <textarea rows="3" class="form-control"
                                                  name="manday_remarks">{{ $aj_standard_stage->manday_remarks != null ?  $aj_standard_stage->manday_remarks : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">
                                <div class="col-md-6 floting">AUDIT TEAM SELECTION</div>
                            </h3>
                        </div>
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="callout callout-warning custom_callout">

                                    @php $check = 0; @endphp
                                    @if(  $audit_type === 'Surveillance 1 + Scope Extension' || $audit_type === 'Surveillance 2 + Scope Extension' || $audit_type === 'Surveillance 3 + Scope Extension' || $audit_type === 'Surveillance 4 + Scope Extension' || $audit_type === 'Surveillance 5 + Scope Extension' || $audit_type === 'Reaudit + Scope Extension'
                                    ||  $audit_type === 'Surveillance 1 + Special Transition' || $audit_type === 'Surveillance 2 + Special Transition' || $audit_type === 'Surveillance 3 + Special Transition' || $audit_type === 'Surveillance 4 + Special Transition' || $audit_type === 'Surveillance 5 + Special Transition' || $audit_type === 'Reaudit + Special Transition'
                                    )
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="col-md-6 floting">
                                                    <label>Q4: Any change in the version of Standard.</label>
                                                </div>
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $standard)
                                                        @if($standard->standard_id === $standard_number->id)
                                                            @php   $standard_number_id =   $standard->standard_id @endphp
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @php
                                                    $standard_numbers =App\Standards::where('standards_family_id',$standard_number->standards_family_id)->where('id','!=',$standard_number_id)->where('status',1)->get();
                                                       if(!empty($standard_numbers) && count($standard_numbers) >0){
                                                    $standard_numbers =[];
                                                    foreach($standard_numbers as $record){
                                                        if((int)$record->number > (int)$standard_number->number){
                                                                    array_push( $standard_numbers,$record);
                                                                    $check =1;
                                                        }else{
                                                             $check =0;
                                                        }

                                                    }


                                                }else{
                                                    $check =0;
                                                }

                                                @endphp
                                                <div class="col-md-6 floting">
                                                    <div class="form-group">

                                                        <div class="col-md-4 floting no_padding">
                                                            <input type="radio" name="ch_std" id="yesCheck" value="yes"
                                                                   class="question"
                                                                   @if(!empty($aj_standard_change))@if($aj_standard_change->version === 'yes')checked
                                                                   @else @endif @endif
                                                                   onclick="javascript:yesnoCheck();">

                                                            <label class="job_status">Yes</label>
                                                        </div>
                                                        <div class="col-md-4 floting">
                                                            <input type="radio" name="ch_std" value="no" id="noCheck"
                                                                   class="question"
                                                                   @if(!empty($aj_standard_change)) @if($aj_standard_change->version === 'no')checked
                                                                   @else @endif @endif
                                                                   onclick="javascript:yesnoCheck();">
                                                            <label class="job_status">No</label>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <br>
                                            <div class="col-md-12" id="ifYes" style="display:none">
                                                <label>Version Change</label>
                                                <div class="form-group">

                                                    <div class="col-md-6 floting">
                                                        <p class="results_answer">From
                                                            @if(!empty($company->companyStandards))
                                                                @foreach($company->companyStandards as $standard)
                                                                    @if($standard->standard_id === $standard_number->id)
                                                                        {{ $standard->standard->name }} to
                                                                    @endif
                                                                @endforeach
                                                                <select id="std_number" name="std_number">
                                                                    <option value="">Select an option</option>
                                                                    @foreach($standard_numbers as $standard_num)
                                                                        <option value="{{$standard_num->id}}"
                                                                                @if(!empty($aj_standard_change)) @if($aj_standard_change->standards === $standard_num->id) selected @else @endif @endif>
                                                                            {{$standard_num->name}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            @endif

                                                        </p>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    @endif

                                    <div class="row">
                                        @php
                                            $audit_type = explode( '_', $audit_type);

                                            if(sizeOf($audit_type) === 3){
                                                $audit_type= ucfirst($audit_type[0]).' '.ucfirst($audit_type[1]).' + '.ucfirst($audit_type[2]);
                                            } elseif(sizeOf($audit_type) === 2){
                                                $audit_type= ucfirst($audit_type[0]).' + '.ucfirst($audit_type[1]);
                                            }
                                            elseif(sizeOf($audit_type) === 4){
                                                $audit_type= ucfirst($audit_type[0]).' '.$audit_type[1].' + '.ucfirst($audit_type[2]).' '.ucfirst($audit_type[3]);
                                            }
                                        @endphp
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Audit Type</label>
                                                <p class="results_answer">{{ $audit_type }}</p>
                                            </div>
                                        </div>
{{--                                        <div class="col-md-4">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>Certification Status</label>--}}

{{--                                                <p class="results_answer"> Not Certified</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <a class="btn btn-primary float-right" style="color: white"
                                                   onclick="makeOutSourceRequest('{{ $company->id }}', '{{$standard_number->id}}')">Make
                                                    a Outsource Request ?</a>
                                            </div>
                                        </div>
                                    </div>
                                    @if($combine_tracker == 0)
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Is Supervising Lead Auditor Involved?</label>
                                                <div class="form-group">
                                                    <div class="col-md-4 floting no_padding">
                                                        <div>

                                                            <input type="radio" name="supervisor_involved[stage1]"
                                                                   class="supervisor_involved" value="yes"
                                                                   id="supervisor_involved"
                                                                   onchange="hideItems(this, 'stage1')"/>
                                                        </div>
                                                        <label class="job_status">Yes</label>
                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div>
                                                            <input type="radio" name="supervisor_involved[stage1]"
                                                                   class="supervisor_involved" value="no"
                                                                   id="supervisor_involved"
                                                                   onchange="hideItems(this, 'stage1')" checked
                                                            />
                                                        </div>
                                                        <label class="job_status">No</label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Expected Date</label>

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                              <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                              </span>
                                                        </div>
                                                        <input type="text" name="expected_date[]" required
                                                               value="{{ $aj_standard_stage->excepted_date !=null ? $aj_standard_stage->excepted_date : ''}}"
                                                               class="form-control float-right active expected_date">
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Auditor Type</label>
                                                    <div class="form-group">
                                                        <div class="multiselect">
                                                            <div class="selectBox" onclick="showCheckboxes()">
                                                                <select class="form-control" multiple="">
                                                                </select>
                                                                <div class="overSelect"></div>
                                                            </div>
                                                            <div id="checkboxes">
                                                                @if(!empty($auditor_grades))
                                                                    @foreach($auditor_grades as $grade)
                                                                        <label for="grade{{$loop->iteration}}"
                                                                               style="display: {{ ($grade->short_name == 'SLA') ? 'none' : 'block' }}"
                                                                               class="grade_stage1{{$loop->iteration}}"
                                                                               id="grade{{$loop->iteration}}">
                                                                            <input type="radio"
                                                                                   name="auditor_type_stage"
                                                                                   value="{{ $grade->short_name }}"
                                                                                   data-name="{{$grade->name}}"
                                                                                   data-id="{{$grade->id}}"
                                                                                   data-is_multiple="{{$grade->is_multiple}}"
                                                                                   class="multi-{{ str_replace("_", "", $audit_type) }}"
                                                                                   onchange="loadMember(this,'members-{{ str_replace("_", "", $audit_type) }}', {{$grade->id}}, {{ $grade->is_multiple }},'{{ str_replace("_", "", $audit_type) }}')">{{ $grade->name }}
                                                                        </label>
{{--                                                                        <label for="grade{{$loop->iteration}}"--}}
{{--                                                                               id="grade{{$loop->iteration}}"--}}
{{--                                                                               class="hide-stage1"--}}
{{--                                                                               style="display: {{ ($grade->short_name == 'SLA' && $aj_standard_stage->supervisor_involved === 'no') ? 'none' : 'block' }}">--}}
{{--                                                                            <input type="radio"--}}
{{--                                                                                   name="auditor_type_stage"--}}
{{--                                                                                   value="{{ $grade->short_name }}"--}}
{{--                                                                                   data-id="{{$grade->id}}"--}}
{{--                                                                                   data-name="{{$grade->name}}"--}}
{{--                                                                                   data-is_multiple="{{$grade->is_multiple}}"--}}
{{--                                                                                   class="multi-{{ str_replace("_", "", $audit_type) }}"--}}
{{--                                                                                   onchange="loadMember(this,'members-{{ str_replace("_", "", $audit_type) }}',{{$grade->id}}, {{ $grade->is_multiple }})">{{ $grade->name }}--}}
{{--                                                                        </label>--}}
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Team Members Name</label>
                                                    <div class="form-group">
                                                        <div class="multiselect">

                                                            @if(!empty($auditors) &&  count($auditors) > 0)


                                                                @foreach($auditors as $auditor)

                                                                    @if(!empty($auditor->auditorStandards)  )

                                                                        @foreach($auditor->auditorStandards as $auditorStandards)
                                                                            @if(!empty($auditorStandards->auditorStandardGrades)  )

                                                                                @foreach($auditorStandards->auditorStandardGrades as $auditorStandardGrades)

                                                                                    @if($auditorStandardGrades->grade->is_multiple == 1)
                                                                                        <div class="teamMemberInline">
                                                                                            <input type="hidden"
                                                                                                   name="is_multiple_out_{{$auditorStandardGrades->grade_id}}"
                                                                                                   value="{{$auditorStandardGrades->grade->is_multiple}}">
                                                                                            <label for="grade-out_{{$auditor->id}}"
                                                                                                   class="hide-stage1"
                                                                                                   style="display: block;margin-left: 7px;">
                                                                                                <input type="checkbox"
                                                                                                       id="grade-out_{{$auditor->id}}"
                                                                                                       name="auditor_team_stage_out"
                                                                                                       data-name="{{$auditor->first_name}} {{$auditor->last_name}}"
                                                                                                       data-type="{{$auditorStandardGrades->grade->name}}"
                                                                                                       data-auditor_standard_id="{{$auditorStandards->id}}"
                                                                                                       data-grade_id="{{$auditorStandardGrades->grade_id}}"
                                                                                                       data-id="{{$auditor->id}}"
                                                                                                       value="{{$auditor->id}}">
                                                                                                {{$auditor->first_name}} {{$auditor->last_name}}
                                                                                                ( OS {{$auditorStandardGrades->grade->name}})</label>
                                                                                            <a href="#"
                                                                                               class="hrefcolor"
                                                                                               onclick="partialModel('{{$auditorStandards->id}}')">
                                                                                                <i class="fa fa-eye"></i></a>

                                                                                        </div>
                                                                                    @else
                                                                                        <div class="teamMemberInline">
                                                                                            <label for="grade-out_{{$auditor->id}}"
                                                                                                   class="hide-stage1"
                                                                                                   style="margin-left: 7px;">
                                                                                                <input type="radio"
                                                                                                       id="grade-out_{{$auditor->id}}"
                                                                                                       name="auditor_team_stage_out"
                                                                                                       data-name="{{$auditor->first_name}} {{$auditor->last_name}}"
                                                                                                       data-type="{{$auditorStandardGrades->grade->name}}"
                                                                                                       data-auditor_standard_id="{{$auditorStandards->id}}"
                                                                                                       data-grade_id="{{$auditorStandardGrades->grade_id}}"
                                                                                                       data-id="{{$auditor->id}}"
                                                                                                       value="{{$auditor->id}}"
                                                                                                       class="multi-stage1"
                                                                                                       onchange="">
                                                                                                {{$auditor->first_name}} {{$auditor->last_name}}
                                                                                                (OS {{$auditorStandardGrades->grade->name}})
                                                                                            </label>
                                                                                            <a href="#"
                                                                                               class="hrefcolor"
                                                                                               onclick="partialModel('{{$auditorStandards->id}}')">
                                                                                                <i class="fa fa-eye"></i></a>

                                                                                        </div>
                                                                                    @endif
                                                                                    @break
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endif

                                                            <div id="teamauditors">


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Proposed Team</label>
                                                <table class="table table-bordered proposed_team_tbl">
                                                    <tbody id="team-view-{{ str_replace('_', '', $audit_type) }}">


                                                    @if(!empty($team_members) && count($team_members) > 0)
                                                        @foreach($team_members as $key=>$team)

                                                            <tr id="tr-<?= $team["auditor_id"] ?>">
                                                                <td> <?= $team["member_name"] . " ( " . $team["member_type"] . " )"?>

                                                                    <input type="hidden" name="perposedTeam[]"
                                                                           data-garde_id="<?= $team["grade_id"] ?>"
                                                                           value="<?= $team["auditor_id"] ?>">
                                                                    <input type="hidden" name="perposed_team_grade_id[]"
                                                                           value="<?= $team["grade_id"] ?>">
                                                                    <input type="hidden" name="perposed_team_name[]"
                                                                           value="<?= $team["member_name"] ?>">
                                                                    <input type="hidden" name="perposed_team_type[]"
                                                                           value="<?= $team["member_type"] ?>">
                                                                    <input type="hidden" name="perposedTeamData"
                                                                           data-team=" <?= $team["member_name"] . " ( " . $team["member_type"] . " )"?>"
                                                                           data-grade_id="<?= $team["grade_id"] ?>"
                                                                           value="<?= $team["auditor_id"] ?>">
                                                                    <span class="remove_perpose_team">
                                                                    <a href="#" style="color:red">X</a>
                                                                </span>
                                                                </td>
                                                            </tr>

                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Job Number</label>
                                        {{-- <p class="results_answer"> {{ $job_number }}</p> --}}

                                        <input type="text" class="form-control" name="job_number" {{(auth()->user()->user_type === 'operation_manager' || auth()->user()->user_type === 'operation_coordinator' ? 'readonly' : 'readonly')}}
                                               value="{{ $job_number }}">
                                    </div>
                                </div>
                                @if(auth()->user()->user_type === 'scheme_manager')
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Evaluator Name</label>
                                            <input type="text" name="evaluator_name" class="form-control"
                                                   value="{{ $aj_standard_stage->evaluator_name !=null ? $aj_standard_stage->evaluator_name : auth()->user()->fullName()}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Approval Date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                  </span>
                                                </div>
                                                <input type="text" name="approval_date"
                                                       value="{{ $aj_standard_stage->approval_date !=null ? $aj_standard_stage->approval_date : ''}}"
                                                       class="form-control float-right active approval_date">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-12 floting">
                                    <div class="form-group">
                                        <label>Special AJ Remarks</label>
                                        <textarea rows="3" class="form-control" readonly
                                                  name="special_aj_remarks">{{ $aj_standard_stage->special_aj_remarks != null ?  $aj_standard_stage->special_aj_remarks : '' }}</textarea>
                                    </div>
                                </div>
                            </div>

                            @if(  $audit_type === 'Surveillance 1 + Scope Extension' || $audit_type === 'Surveillance 2 + Scope Extension' || $audit_type === 'Surveillance 3 + Scope Extension' || $audit_type === 'Surveillance 4 + Scope Extension' || $audit_type === 'Surveillance 5 + Scope Extension' || $audit_type === 'Reaudit + Scope Extension'
                            ||  $audit_type === 'Surveillance 1 + Special Transition' || $audit_type === 'Surveillance 2 + Special Transition' || $audit_type === 'Surveillance 3 + Special Transition' || $audit_type === 'Surveillance 4 + Special Transition' || $audit_type === 'Surveillance 5 + Special Transition' || $audit_type === 'Reaudit + Special Transition'
                            )

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Change Confirmation Checklist:</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6 floting">
                                                <label>Q1: Any change in Name</label>
                                            </div>
                                            <div class="col-md-6 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <input type="radio" name="ch_name" value="yes" class="question"
                                                               @if(!empty($aj_standard_change))@if($aj_standard_change->name === 'yes')checked @else @endif @endif>

                                                        <label class="job_status">Yes</label>
                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <input type="radio" name="ch_name" value="no" class="question"
                                                               @if(!empty($aj_standard_change))@if($aj_standard_change->name === 'no')checked @else @endif @endif>
                                                        <label class="job_status">No</label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6 floting">
                                                <label>Q2: Any change in Scope</label>
                                            </div>
                                            <div class="col-md-6 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <input type="radio" name="ch_scp" value="yes" class="question"
                                                               @if(!empty($aj_standard_change))@if($aj_standard_change->scope === 'yes')checked @else @endif @endif>


                                                        <label class="job_status">Yes</label>
                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <input type="radio" name="ch_scp" value="no" class="question"
                                                               @if(!empty($aj_standard_change))@if($aj_standard_change->scope === 'no')checked @else @endif @endif>
                                                        <label class="job_status">No</label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6 floting">
                                                <label>Q3: Any change in Address / Number of locations</label>
                                            </div>
                                            <div class="col-md-6 floting">
                                                <div class="form-group">

                                                    <div class="col-md-4 floting no_padding">
                                                        <input type="radio" name="ch_loc" value="yes" class="question"
                                                               @if(!empty($aj_standard_change)) @if($aj_standard_change->location === 'yes')checked @else @endif @endif>

                                                        <label class="job_status">Yes</label>
                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <input type="radio" name="ch_loc" value="no" class="question"
                                                               @if(!empty($aj_standard_change)) @if($aj_standard_change->location === 'no')checked @else @endif @endif>
                                                        <label class="job_status">No</label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                                <div class="row" id="changeremarks" style="display:none">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Change Details:</label>
                                            <textarea name="remarks" rows="4" class="form-control"
                                                      placeholder="Remarks">{{ !empty($aj_standard_change->remarks) ? $aj_standard_change->remarks : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">

                                <div class="col-md-4"></div>
                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager')
                                    @if(!empty($notifications) && count($notifications) > 0)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Comments Box</label>
                                                <div class="card-body chat_box" style="display: block;">
                                                    <div class="direct-chat-messages">


                                                        @foreach($notifications as $key=>$notification)

                                                            @if($notification->sent_by === Auth::user()->id )
                                                                <div class="direct-chat-msg">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                </div>

                                                            @else
                                                                <div class="direct-chat-msg text-right">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                </div>
                                                            @endif
                                                        @endforeach


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @endif
                                <div class="col-md-4"></div>


                            </div>
                            <div class="row">

                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')
                                    <div class="col-md-12 mrgn no_padding ">
                                        <div class="col-md-4 floting"></div>
                                        <div class="col-md-4 floting"></div>

                                        @if($aj_standard_stage->status == 'applied'  || $aj_standard_stage->status == 'resent')
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <button type="submit" onclick="mainAjSubmit('approved',event)"
                                                            class="btn btn-block btn-success btn_save">Approved
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-2 floting no_padding">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-block btn_search"
                                                            onclick="mainAjSubmit('rejected',event)">Reject
                                                    </button>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-2 floting">
                                                <div class="form-group">
                                                    <button type="submit" onclick="mainAjSubmit('approved',event)"
                                                            class="btn btn-block btn-success btn_save">Save
                                                    </button>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                @else
                                    <div class="col-md-12 mrgn no_padding">
                                        <div class="col-md-4 floting"></div>
                                        <div class="col-md-4 floting"></div>


                                        @if(auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager')
                                            @if($aj_standard_stage->status == 'Not Applied' || $aj_standard_stage->status == 'created')
                                                <div class="col-md-2 floting">
                                                    <div class="form-group">
                                                        <button type="submit" onclick="mainAjSubmit('save',event)"
                                                                class="btn btn-block btn-success btn_save">Save
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 floting no_padding">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-block btn_search"
                                                                onclick="mainAjSubmit('send',event)">Send
                                                        </button>
                                                    </div>
                                                </div>
                                            @elseif($aj_standard_stage->status == 'applied')
                                            @elseif($aj_standard_stage->status == 'approved' || $aj_standard_stage->status == 'rejected')
                                                <div class="col-md-2 floting no_padding">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-block btn_search"
                                                                onclick="mainAjSubmit('resent',event)">Resent
                                                        </button>
                                                    </div>
                                                </div>

                                            @endif
                                        @endif


                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="viewAllAJ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form action="#" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Other Locations</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    @php $i=2; @endphp
                                    @foreach($company->childCompanies as $child)
                                        <label><strong>Address {{$i}}</strong></label>
                                        <p>{{ $child->address }}</p>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="partialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form action="#" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Auditor/Technical Expert Codes</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div id="partial_remarks">

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="sendPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form id="comment-form" action="#" method="post">
                <input type="hidden" name="url" value="">
                <input type="hidden" name="status" id="status" value="">
                <div id="aj-ids"></div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Comments</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><strong>Add Comment</strong></label>
                                    <textarea rows="5" class="form-control" name="remark"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade viewModal2 show" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-eye"></i>VIEW HISTORY</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h3>Man Days History</h3>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div id="example1_filter" class="dataTables_filter">
                                    <form action="#" method="post">
                                        <label>
                                            <input type="text" class="form-control form-control-sm" id="myFilter"
                                                   placeholder="Search" aria-controls="example1">
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable no-footer"
                                       role="grid" aria-describedby="example1_info">
                                    <thead>
                                    <tr role="row green_row">
                                        <th width="15%;">AJ Creation Date</th>
                                        <th width="15%;">Audit Type</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                        <th width="15%;">Acutal Audit Date</th>
                                        <th>Remarks / Justification For Reduction</th>

                                    </tr>
                                    </thead>
                                    <tbody id="myTable">


                                    @if(auth()->user()->user_type === 'scheme_manager')
                                        @foreach($aj_stages_manday as $manday)

                                            <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                <td>{{$manday->created_at->format('d-m-Y')}}</td>
                                                <td>{{ $manday->getAuditType() }}</td>
                                                <td>{{$manday->onsite}}</td>
                                                <td>{{$manday->offsite}}</td>
                                                <td>{{$manday->updated_at->format('d-m-Y')}}</td>
                                                <td>{{$manday->manday_remarks}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class='notfound' style="display:none">
                                            <td colspan='6'>No record found</td>
                                        </tr>

                                    @else
                                        @foreach($aj_stages_manday as $manday)

                                            <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                <td>{{$manday->created_at->format('d-m-Y')}}</td>
                                                <td>{{ $manday->getAuditType() }}</td>
                                                <td>{{$manday->onsite}}</td>
                                                <td>{{$manday->offsite}}</td>
                                                <td>{{$manday->updated_at->format('d-m-Y')}}</td>
                                                <td>{{$manday->manday_remarks}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class='notfound' style="display:none">
                                            <td colspan='6'>No record found</td>
                                        </tr>
                                    @endif


                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    {{--  Outsource Request --}}

    <div class="modal fade" id="outSourceRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form action="" method="post" id="single-outsource-request">
                <input type="hidden" value="{{ $company->id }}" name="company_id" id="company_id"/>
                <input type="hidden" value="{{ $standard_number->id }}" name="standard_id"/>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Outsource Request</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body" id="outsource-request">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Required Date</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                                  <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                  </span>
                                        </div>
                                        <input type="text" class="form-control float-right" id="reservation"
                                               name="required_date">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-12">
                                <p><strong>Note:</strong> The Auditor wil be available for 10 days for AJTF
                                    selection upon approval.</p>
                            </div>
                        </div>
                        <div class="row" id="availableAuditor">

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarks*</label>
                                    <textarea name="om_remarks" rows="4" class="form-control"
                                              placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12 mrgn no_padding">
                            <div class="col-md-3 floting"></div>
                            <div class="col-md-3 floting"></div>
                            <div class="col-md-3 floting"></div>
                            <div class="col-md-3 floting no_padding">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success btn_save">SEND
                                        REQUEST
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .team-member-delete-button {
            border: none;
            background: none;
            float: right;
        }

        .my-active span {
            background-color: #5cb85c !important;
            color: white !important;
            border-color: #5cb85c !important;
        }

        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

    </style>
    <style>
        #partialModal{
            overflow-y: hidden !important;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $('.expected_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.approval_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $(document).ready(function () {
            function unique(list) {
                var result = [];
                $.each(list, function (i, e) {
                    if ($.inArray(e, result) == -1) result.push(e);
                });
                return result;
            }

            function removeDuplicates(id) {
                var records = $(id).text();
                records = records.replace(/\s/g, '');
                let records_array = records.split(',');
                var final_records = unique(records_array);
                $(id).text(final_records.toString());
                return true;
            }

            var iaf_codes = removeDuplicates('#iaf-codes');
            var ias_codes = removeDuplicates('#ias-codes');
            var accreditations_codes = removeDuplicates('#codes-accreditations');
            var food_accreditations = removeDuplicates('#food-accreditations');
            var energy_accreditations = removeDuplicates('#energy-accreditations');
            var food_category_codes = removeDuplicates('#food-category-codes');
            var food_subcategory_codes = removeDuplicates('#food-subcategory-codes');
            var energy_codes = removeDuplicates('#energy-codes');
        });
        $(function () {
            //Initialize Select2 Elements
            $(function () {
                //Initialize Select2 Elements
                $('.select2').select2({
                    width: '100%'
                });
            });


            //Date range picker
            $('#reservation').daterangepicker({
                format: 'DD/MM/YYYY'
            });
            $('#reservation2').daterangepicker({
                format: 'DD/MM/YYYY'
            });


            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        });

        function makeOutSourceRequest(companyId, standardId) {
            var request = "company_id=" + companyId + "&standard_id=" + standardId;

            $.ajax({
                type: "POST",
                url: "{{ route('outsource.search.auditor') }}",
                data: request,
                dataType: "json",
                cache: true,
                global: false,
                success: function (response) {
                    if (response.status == "success") {

                        $('#availableAuditor').html(response.data.auditors);
                        $('#outSourceRequest').modal('show');
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        $("#single-outsource-request").submit(function (e) {
            e.preventDefault();
            var form = $("#single-outsource-request")[0];
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: "{{ route('outsource.request.store') }}",
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {

                    ajaxResponseHandler(response, form);
                    if (response.status == "success" || response.status == '200') {
                        window.location.href = '{{route('outsource.index')}}';
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function partialModel(auditor_standard_id) {
            $.ajax({
                type: "POST",
                url: "{{ route('justification.partial.remarks') }}",
                data: {auditor_standard_id: auditor_standard_id},
                success: function (response) {
                    var html = '';
                    // response.data.partial_remarks.forEach(function (data, index) {
                    //     html += '<p><strong>' + data.iaf.code + ' - </strong><strong>' + data.iaf.name + '</strong></p>';
                    //     html += '<p>' + data.remarks + '</p>';
                    // });
                    $('#partial_remarks').html(response.data.html);


                    // $('#partial_remarks').html(html);
                    $('#FormalEducationTableCodes').dataTable();
                    $('#partialModal').modal('show');
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function yesnoCheck() {
            if (document.getElementById('yesCheck').checked) {
                document.getElementById('ifYes').style.display = 'block';
                document.getElementById('changeremarks').style.display = 'block';
            } else {
                document.getElementById('ifYes').style.display = 'none';
                document.getElementById('changeremarks').style.display = 'none';
            }

        }


        function hideItems(input_id, stage) {


            if ($(input_id).val() == 'yes') {
                $('#grade6').fadeIn();
            } else {
                $('#grade6').fadeOut();
            }
        }


        function loadMember(input_radio, member_stage, grade_id, is_multiple, stage) {

            $(input_radio).attr('checked', 'true');

            var name = $(input_radio).data('name');

            $("." + member_stage).each(function () {
                if ($(this).data('id') == grade_id) {
                    if (is_multiple != 1) {
                        $(this).prop('type', 'radio');
                    }
                    $(this).parent().fadeIn();
                } else {
                    $(this).parent().fadeOut();
                }
            });
            $('.multi-' + stage).each(function () {


                if ($(this).data('name') == name) {
                    $(this).attr('checked', 'true');
                } else {

                    $(this).removeAttr('checked');
                }
            });
        }

        auditor_types = [];
        auditor_names = [];
        auditors_id = [];

        function generateTeam(member_input, stage) {
            var team_html = '';
            //add checked attr to input
            $(member_input).attr('checked', 'true');

            $(".multi-" + stage).each(function () {

                if ($(this).attr('checked') == 'checked') {
                    auditor_types.push($(this).val());
                    // $(this).removeAttr('checked');

                }
            });

            $(".members-" + stage).each(function () {
                if ($(this).attr('checked') == 'checked') {
                    auditor_names.push($(this).val());
                    auditors_id.push($(this).data('auditor-id'));
                    $(this).removeAttr('checked');
                    $(this).attr('disabled', 'true');
                }
            });

            auditor_names = auditor_names.filter((x, i, a) => a.indexOf(x) == i);
            auditor_names.forEach(function (m, n) {
                team_html += '<tr id="' + stage + '-node-' + n + '"><td>' + m + ' - ' + auditor_types[n] + '<button type="button" onclick="deleteNode(' + n + ', \'' + stage + '\', \'' + auditor_types[n] + '\', \'' + m + '\', \'' + auditors_id[n] + '\')" class="team-member-delete-button"><i class="fa fa-close"></i></button></td><input type="hidden" name="auditor_member[' + stage + '][]" value="' + m + '-' + auditor_types[n] + '"><input type="hidden" name="auditor_id[]" value="' + auditors_id[n] + '"></tr>'
            });

            $("#team-view-" + stage).html(team_html);

        }

        function deleteNode(id, stage, auditor_type, auditor_name, auditor_id) {

            $("#" + stage + '-node-' + id).remove();
            auditor_names.pop(auditor_name);
            auditor_types.pop(auditor_type);
            auditors_id.pop(auditor_id);
            $(".members-" + stage).each(function () {
                if ($(this).val() == auditor_name) {
                    $(this).prop('checked', false);
                    $(this).removeAttr('disabled');
                }
            });
        }

        function showInput(site, id) {
            $("#" + site + id).hide();
            $("#input-" + site + "-" + id).show();
            $("#input-" + site + "-" + id).focus();
        }

        function doneEdit(site, id, manday_id) {
            $("#input-" + site + "-" + id).on('keypress keyup', function (e) {
                if (e.keyCode == 13 || e.keyCode == 27) {
                    $("#input-" + site + "-" + id).hide();
                    let inputValue = $("#input-" + site + "-" + id).val();
                    $("#" + site + id).show();
                    $("#" + site + id).text(inputValue);

                    if (document.getElementById("input-" + site + "-" + id).defaultValue !== inputValue) {
                        updateStageManday(site, inputValue, manday_id);
                    }
                    // updateStageManday(site, inputValue, manday_id);
                }
            });
        }

        function hideInput(id, stage, site, manday_id) {
            $("#input-" + site + "-" + id).hide();
            let inputValue = $("#input-" + site + "-" + id).val();
            $("#" + site + id).show();
            $("#" + site + id).text(inputValue);


            if (document.getElementById("input-" + site + "-" + id).defaultValue !== inputValue) {
                updateStageManday(site, inputValue, manday_id);
            }

        }

        function updateStageManday(site, value, id) {
            $.ajax({
                url: "{{ route('justification.update.stage.manday') }}",
                data: {site: site, value: value, id: id},
                type: 'POST',
                cache: true,
                success: function (response) {
                    if (response.status == 'success') {
                        toastr['success'](response.message); //toaster message issue
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            })
        }

        function mainAjSubmit(type, e) {
            e.preventDefault();
            var userType = '{{auth()->user()->user_type}}';

            var form = $('#main-aj-form')[0];
            var formData = new FormData(form);
            var company_id = {{$company->id}};
            var status = '';
            if (type === 'save') {
                status = "created";
            } else if (type === 'send') {
                status = "applied";
            } else if (type === 'approved') {
                status = "approved";
            } else if (type === 'rejected') {
                status = "rejected";
            } else if (type === 'resent') {
                status = "resent";
            } else if (type === 'approved_scheme') {
                status = "approved_scheme";
            }

            $('#status').val(status);

            formData.append('status', status);
            formData.append('std_number', $('#std_number').val());


            var ch_name = $("input[name='ch_name']");
            var ch_scp = $("input[name='ch_scp']");
            var ch_loc = $("input[name='ch_loc']");

            if (userType === 'operation_manager' || userType === 'operation_coordinator') {
                if ((ch_name.is(':checked') === true && $('input[name="ch_name"]:checked').val() === 'no') && (ch_scp.is(':checked') === true && $('input[name="ch_scp"]:checked').val() === 'no') && (ch_loc.is(':checked') === true && $('input[name="ch_loc"]:checked').val() === 'no')) {
                    if (type === 'save' || type === 'send') {
                        if (confirm("No change has been selected - Please confirm this is correct?")) {
                            $.ajax({
                                type: "POST",
                                url: "{{ route('justification.special.update') }}",
                                enctype: 'multipart/form-data',
                                processData: false,
                                contentType: false,
                                cache: false,
                                data: formData,
                                success: function (response) {

                                    if (type === 'save') {
                                        if (response.flash_status == 'warning') {
                                            toastr[response.flash_status](response.flash_message);
                                        } else {
                                            ajaxResponseHandler(response, form);
                                        }

                                    } else if (type === 'send' || type === 'rejected' || type === 'approved' || type == 'resent') {
                                        //
                                        // if (response.status == 200) {
                                        //     console.log(response);
                                        //     var html = '';
                                        //     $(response.data.aj_id).each(function (i, d) {
                                        //         html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                                        //     });
                                        //     $("#aj-ids").html(html);
                                        //     $('#sendPopup').modal('toggle');
                                        // } else {
                                        //     ajaxResponseHandler(response, form);
                                        // }

                                        if (response.flash_status === 'warning') {
                                            toastr[response.flash_status](response.flash_message);
                                        } else {
                                            if (response.data.status === 'success') {


                                                var html = '';
                                                $(response.data.aj_id).each(function (i, d) {
                                                    html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                                                });
                                                $("#aj-ids").html(html);
                                                $('#sendPopup').modal('toggle');
                                            } else if (response.data.status === 'warning') {
                                                toastr[response.data.status](response.data.message);
                                            } else {
                                                ajaxResponseHandler(response, form);
                                            }
                                        }

                                    } else if (type === 'approved_scheme') {
                                        if (response.flash_status === 'warning') {
                                            toastr[response.flash_status](response.flash_message);
                                        } else {
                                            if (response.data.status === 'success') {
                                                toastr[response.data.status]('AJ Updated Successfully.');
                                                window.location = "{{ route('company.show', ['id' => "_COMPANY_ID_"]) }}".replace('_COMPANY_ID_', company_id);
                                            } else if (response.data.status === 'warning') {
                                                toastr[response.data.status](response.data.message);
                                            } else {
                                                ajaxResponseHandler(response, form);
                                            }
                                        }
                                    }

                                },
                                error: function () {
                                    toastr['error']("Something Went Wrong.");
                                }
                            });
                        } else {
                            return false;
                        }
                    }
                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('justification.special.update') }}",
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        cache: false,
                        data: formData,
                        success: function (response) {

                            if (type === 'save') {
                                if (response.flash_status == 'warning') {
                                    toastr[response.flash_status](response.flash_message);
                                } else {
                                    ajaxResponseHandler(response, form);
                                }

                            } else if (type === 'send' || type === 'rejected' || type === 'approved' || type == 'resent') {
                                // debugger;
                                // if (response.status == 200) {
                                //     console.log(response);
                                //     var html = '';
                                //     $(response.data.aj_id).each(function (i, d) {
                                //         html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                                //     });
                                //     $("#aj-ids").html(html);
                                //     $('#sendPopup').modal('toggle');
                                // } else {
                                //     ajaxResponseHandler(response, form);
                                // }

                                if (response.flash_status === 'warning') {
                                    toastr[response.flash_status](response.flash_message);
                                } else {
                                    if (response.data.status === 'success') {


                                        var html = '';
                                        $(response.data.aj_id).each(function (i, d) {
                                            html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                                        });
                                        $("#aj-ids").html(html);
                                        $('#sendPopup').modal('toggle');
                                    } else if (response.data.status === 'warning') {
                                        toastr[response.data.status](response.data.message);
                                    } else {
                                        ajaxResponseHandler(response, form);
                                    }
                                }

                            } else if (type === 'approved_scheme') {
                                if (response.flash_status === 'warning') {
                                    toastr[response.flash_status](response.flash_message);
                                } else {
                                    if (response.data.status === 'success') {
                                        toastr[response.data.status]('AJ Updated Successfully.');
                                        window.location = "{{ route('company.show', ['id' => "_COMPANY_ID_"]) }}".replace('_COMPANY_ID_', company_id);
                                    } else if (response.data.status === 'warning') {
                                        toastr[response.data.status](response.data.message);
                                    } else {
                                        ajaxResponseHandler(response, form);
                                    }
                                }
                            }

                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                }
            } else {
                $.ajax({
                    type: "POST",
                    url: "{{ route('justification.special.update') }}",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: formData,
                    success: function (response) {
                        debugger;
                        if (type === 'save') {
                            if (response.flash_status == 'warning') {
                                toastr[response.flash_status](response.flash_message);
                            } else {
                                ajaxResponseHandler(response, form);
                            }

                        } else if (type === 'send' || type === 'rejected' || type === 'approved' || type == 'resent') {
                            // debugger;
                            // if (response.status == 200) {
                            //     console.log(response);
                            //     var html = '';
                            //     $(response.data.aj_id).each(function (i, d) {
                            //         html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                            //     });
                            //     $("#aj-ids").html(html);
                            //     $('#sendPopup').modal('toggle');
                            // } else {
                            //     ajaxResponseHandler(response, form);
                            // }

                            if (response.flash_status === 'warning') {
                                toastr[response.flash_status](response.flash_message);
                            } else {
                                if (response.data.status === 'success') {


                                    var html = '';
                                    $(response.data.aj_id).each(function (i, d) {
                                        html += '<input name="aj_id[]" value="' + d.id + '" type="hidden">';
                                    });
                                    $("#aj-ids").html(html);
                                    $('#sendPopup').modal('toggle');
                                } else if (response.data.status === 'warning') {
                                    toastr[response.data.status](response.data.message);
                                } else {
                                    ajaxResponseHandler(response, form);
                                }
                            }

                        } else if (type === 'approved_scheme') {
                            if (response.flash_status === 'warning') {
                                toastr[response.flash_status](response.flash_message);
                            } else {
                                if (response.data.status === 'success') {
                                    toastr[response.data.status]('AJ Updated Successfully.');
                                    window.location = "{{ route('company.show', ['id' => "_COMPANY_ID_"]) }}".replace('_COMPANY_ID_', company_id);
                                } else if (response.data.status === 'warning') {
                                    toastr[response.data.status](response.data.message);
                                } else {
                                    ajaxResponseHandler(response, form);
                                }
                            }
                        }

                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }


        }

        $("#comment-form").submit(function (e) {
            $(':input[type="submit"]').prop('disabled', true);
            e.preventDefault();
            var form = $('#comment-form')[0];

            var company_id ={{$company->id}};
            var formData = new FormData(form);
            formData.append('company_id', company_id);
            $.ajax({
                type: "POST",
                url: "{{ route('justification.special.remarks.update') }}",
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (response) {
                    ajaxResponseHandler(response, form);

                    if (response.data.status == "success") {
                        // toastr['success'](response.message);
                        $('#sendPopup').modal('hide');

                        setTimeout(function () {
                            window.location = "{{ route('company.show', ['id' => "_COMPANY_ID_"]) }}".replace('_COMPANY_ID_', {{$company->id}});
                        }, 3000);
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        });

        function buttonPopup() {
            $('#sendPopup').modal('toggle');
        }

        // Case-insensitive searching (Note - remove the below script for Case sensitive search )
        $.expr[":"].contains = $.expr.createPseudo(function (arg) {
            return function (elem) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        $("#myFilter").on("keyup", function () {
            // Search Text
            var search = $(this).val();

            // Hide all table tbody rows
            $('#myTable tr').hide();

            // Count total search result
            var len = $('#myTable tr:not(.notfound) td:contains("' + search + '")').length;

            if (len > 0) {
                // Searching text in columns and show match row
                $('#myTable tr:not(.notfound) td:contains("' + search + '")').filter(function () {

                    $(this).closest('tr').show();
                });
            } else {
                $('.notfound').show();
            }

            // var value = $(this).val().toLowerCase();
            //
            // $("#myTable tr").filter(function () {
            //     $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            // });

        });
        @if(  $audit_type === 'Surveillance 1 + Scope Extension' || $audit_type === 'Surveillance 2 + Scope Extension' || $audit_type === 'Surveillance 3 + Scope Extension' || $audit_type === 'Surveillance 4 + Scope Extension' || $audit_type === 'Surveillance 5 + Scope Extension' || $audit_type === 'Reaudit + Scope Extension'
        ||  $audit_type === 'Surveillance 1 + Special Transition' || $audit_type === 'Surveillance 2 + Special Transition' || $audit_type === 'Surveillance 3 + Special Transition' || $audit_type === 'Surveillance 4 + Special Transition' || $audit_type === 'Surveillance 5 + Special Transition' || $audit_type === 'Reaudit + Special Transition'
        )
        (function defaultChecks() {
            // yesnoCheck();

        })();


        @endif
        $(".question").change(function () {

            check = $(this).val();

            if (check == 'yes') {
                $('#changeremarks').css("display", "block")
            }

        });


        $("input[name='auditor_type_stage2']").change(function () {

            var grade_id = $(this).data('id');
            var is_multiple = $(this).data('is_multiple');
            var newStandandSelected = $("#std_number option:selected").val();
            if (newStandandSelected != '' && newStandandSelected != undefined) {
                var standard_id = parseInt(newStandandSelected);
            } else {
                var standard_id ={{$standard_number->id}};
            }
            var company_id ={{$company->id}};

            auditorStandaradTeam(grade_id, company_id, standard_id, is_multiple, 'stage2');

        });
        $("input[name='auditor_type_stage1']").change(function () {

            var grade_id = $(this).data('id');
            var is_multiple = $(this).data('is_multiple');


            var newStandandSelected = $("#std_number option:selected").val();
            if (newStandandSelected != '' && newStandandSelected != undefined) {
                var standard_id = parseInt(newStandandSelected);
            } else {
                var standard_id ={{$standard_number->id}};
            }
            var company_id ={{$company->id}};

            auditorStandaradTeam(grade_id, company_id, standard_id, is_multiple, 'stage1');

        });
        $("input[name='auditor_type_stage']").change(function () {

            var grade_id = $(this).data('id');
            var is_multiple = $(this).data('is_multiple');


            var newStandandSelected = $("#std_number option:selected").val();
            if (newStandandSelected != '' && newStandandSelected != undefined) {
                var standard_id = parseInt(newStandandSelected);
            } else {
                var standard_id ={{$standard_number->id}};
            }


            var company_id ={{$company->id}};

            auditorStandaradTeam(grade_id, company_id, standard_id, is_multiple, null);

        });

        $(function () {
            $(document).on('click', "input[name='auditor_team_stage']", function (e) {


                // Stop the link moving to the top of the page

                var perposedid = $(this).data('id');
                var perposedTeam = $("input[name='perposedTeam[]']").map(function () {
                    return $(this).val();
                }).get();


                var is_multiple = $("input[name='is_multiple']").val();
                var check = 0;
                var name = $(this).data('name');
                var type = $(this).data('type');

                $(perposedTeam).each(function (i, d) {
                    if (d === perposedid.toString()) {
                        check = 1;
                    }
                });


                idVal = $(this).attr('id');
                team_member = $("label[for='" + idVal + "']").text();
                perposedStandardId = $(this).data('auditor_standard_id');
                gradeId = $(this).data('grade_id');

                $("input[name=auditor_team_stage][value='" + idVal + "']").attr('checked', true);

                if (is_multiple == 1) {

                    if ($(this).prop("checked") == true) {

                        auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, null);

                    } else if ($(this).prop("checked") == false) {


                        $("#tr-" + perposedid).remove();

                    }


                } else {

                    if (gradeId == 1) {

                        auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, null);

                    } else {
                        if (check == 0) {
                            auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, null);
                        } else if ($(this).prop("checked") == false) {

                            $("#tr-" + perposedid).remove();
                        }
                    }


                }
            });
        });

        $(function () {
            $(document).on('click', "input[name='auditor_team_stage_out']", function (e) {


                // Stop the link moving to the top of the page

                var perposedid = $(this).data('id');

                var perposedTeam = $("input[name='perposedTeam_out[]']").map(function () {
                    return $(this).val();
                }).get();

                gradeId = $(this).data('grade_id');
                var is_multiple = $("input[name='is_multiple_out_" + gradeId + "']").val();
                var check = 0;
                var name = $(this).data('name');
                var type = $(this).data('type');

                $(perposedTeam).each(function (i, d) {
                    if (d === perposedid.toString()) {
                        check = 1;
                    }
                });


                idVal = $(this).attr('id');
                team_member = $("label[for='" + idVal + "']").text();
                perposedStandardId = $(this).data('auditor_standard_id');

                $("input[name=auditor_team_stage_out][value='" + idVal + "']").attr('checked', true);

                if (is_multiple == 1) {

                    if ($(this).prop("checked") == true) {

                        auditorStandaradProposedTeamOut(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, null);

                    } else if ($(this).prop("checked") == false) {

                        $("#tr-out_" + perposedid).remove();

                    }

                } else {
                    if (check == 0) {
                        auditorStandaradProposedTeamOut(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, null);
                    } else if ($(this).prop("checked") == false) {
                        $("#tr-out_" + perposedid).remove();
                    }
                }
            });
        });


        $(function () {
            $(document).on('click', "input[name='auditor_team_stage1']", function (e) {
// Stop the link moving to the top of the page

                var perposedid = $(this).data('id');
                var perposedTeam = $("input[name='perposedTeam[]']").map(function () {
                    return $(this).val();
                }).get();
                var is_multiple = $("input[name='is_multiple']").val();
                var check = 0;
                var name = $(this).data('name');
                var type = $(this).data('type');
                $(perposedTeam).each(function (i, d) {
                    if (d === perposedid.toString()) {
                        check = 1;
                    }
                });
                idVal = $(this).attr('id');
                $("input[name=auditor_team_stage1][value='" + idVal + "']").attr('checked', true);

                team_member = $("label[for='" + idVal + "']").text();

                // Fetch the data-link attribute

                perposedStandardId = $(this).data('auditor_standard_id');
                gradeId = $(this).data('grade_id');

                if (is_multiple == 1) {

                    if ($(this).prop("checked") == true) {
                        auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, 'stage1');

                    } else if ($(this).prop("checked") == false) {
                        $("#tr-" + 'stage1_' + perposedid).remove();
                    }

                } else {


                    if (gradeId == 1) {

                        auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, 'stage1');

                    } else {
                        if (check == 0) {
                            auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, 'stage1');
                        } else if ($(this).prop("checked") == false) {

                            $("#tr-" + perposedid).remove();
                        }
                    }


                }
            });
        });

        $(function () {
            $(document).on('click', "input[name='auditor_team_stage2']", function (e) {
                // Stop the link moving to the top of the page

                var perposedid = $(this).data('id');
                var perposedTeam = $("input[name='perposedTeam[]']").map(function () {
                    return $(this).val();
                }).get();
                var is_multiple = $("input[name='is_multiple']").val();
                var check = 0;
                var name = $(this).data('name');
                var type = $(this).data('type');
                $(perposedTeam).each(function (i, d) {
                    if (d === perposedid.toString()) {
                        check = 1;
                    }
                });
                idVal = $(this).attr('id');
                $("input[name=auditor_team_stage2][value='" + idVal + "']").attr('checked', true);
                team_member = $("label[for='" + idVal + "']").text();

                // Fetch the data-link attribute
                perposedStandardId = $(this).data('auditor_standard_id');
                gradeId = $(this).data('grade_id');

                if (is_multiple == 1) {
                    if ($(this).prop("checked") == true) {
                        auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, 'stage2');

                    } else if ($(this).prop("checked") == false) {
                        $("#tr-" + perposedid).remove();
                    }
                } else {
                    if (gradeId == 1) {

                        auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, 'stage2');

                    } else {
                        if (check == 0) {
                            auditorStandaradProposedTeam(team_member, perposedid, perposedStandardId, gradeId, is_multiple, name, type, 'stage2');
                        } else if ($(this).prop("checked") == false) {

                            $("#tr-" + perposedid).remove();
                        }
                    }

                }

                // $("label[for='"+idVal+"']").remove();
            });
        });

        $(function () {
            $(document).on('click', ".remove_perpose_team_out", function (e) {

                // Stop the link moving to the top of the page
                e.preventDefault();
                var id = $(this).siblings("input[name='perposedTeamData_out']").val();

                var stage = $(this).siblings("input[name='perposedTeamData_out']").data('stage');
                var grade_id = $(this).siblings("input[name='perposedTeamData_out']").data('grade_id');
                var perposedStandardId = $(this).siblings("input[name='perposedTeamData_out']").data('auditor_standard_id');
                var team_member = $.trim($(this).siblings("input[name='perposedTeamData_out']").data('team'));


                var is_multiple = $("input[name='is_multiple_out_" + gradeId + "']").val();

                var audit_type = '{{ $audit_type }}';

                if (is_multiple == 0) {
                    $("input[name=auditor_team_stage_out]").val(['0']);
                } else {

                    $("#grade-out_" + id).prop('checked', false);
                }

                $("#tr-out_" + id).remove();
            });
        });
        $(function () {
            $(document).on('click', ".remove_perpose_team", function (e) {
                // Stop the link moving to the top of the page
                e.preventDefault();
                var id = $(this).siblings("input[name='perposedTeamData']").val();
                var stage = $(this).siblings("input[name='perposedTeamData']").data('stage');
                var perposedStandardId = $(this).siblings("input[name='perposedTeamData']").data('auditor_standard_id');
                var team_member = $(this).siblings("input[name='perposedTeamData']").data('team');


                var is_multiple = $("input[name='is_multiple']").val();

                var audit_type = '{{ $audit_type }}';
                if (stage && audit_type == 'stage_1|stage_2') {
                    if (is_multiple == 0) {
                        $("input[name=auditor_team_" + stage + "]").val(['0']);
                    } else {
                        $("#grade-" + id).prop('checked', false);
                    }
                } else {
                    if (is_multiple == 0) {
                        $("input[name=auditor_team_stage]").val(['0']);
                    } else {

                        $("#grade-" + id).prop('checked', false);
                    }
                }
                $("#tr-" + id).remove();
            });
        });

        function auditorStandaradProposedTeam(team_member, id, perposedStandardId, gradeId, is_multiple, name, type, stage = null) {

            var html = '';

            var grade_id_selected = $("input[name='perposed_team_grade_id[]']")
                .map(function () {
                    return $(this).val();
                }).get();
            var audit_type = '{{ $audit_type }}';


            if (team_member) {
                if (stage === null) {

                    html += '<tr id="tr-' + id + '" class="tr-' + gradeId + '"><td>' + team_member;
                    html += '<input type="hidden"  name="perposedTeam[]" value="' + id + '" >';
                    html += '<input type="hidden"  name="perposedTeam1[]" value="' + id + '" >';
                    html += '<input type="hidden"  name="perposed_team_grade_id[]" value="' + gradeId + '" >';
                    html += '<input type="hidden"  name="perposed_team_name[]"  value="' + name + '" >';
                    html += '<input type="hidden"  name="perposed_team_type[]"  value="' + type + '" >';
                    html += '<input type="hidden"  name="perposedTeamData" data-stage="' + stage + '" data-team="' + team_member + '" data-grade_id="' + gradeId + '"  data-auditor_standard_id="' + perposedStandardId + '" value="' + id + '" >';
                    html += '<span class="remove_perpose_team"  ><a href="#" style="color:red">X</a></span></td></tr>';

                } else {
                    html += '<tr id="tr-' + stage + '_' + id + '" class="tr-' + stage + '_' + gradeId + '"><td>' + team_member;
                    html += '<input type="hidden"  name="perposedTeam_' + stage + '[]" value="' + id + '" >';
                    html += '<input type="hidden"  name="perposedTeam1[' + stage + '][]" value="' + id + '" >';
                    html += '<input type="hidden"  name="perposed_team_grade_id[' + stage + '][]" value="' + gradeId + '" >';
                    html += '<input type="hidden"  name="perposed_team_name[' + stage + '][]"  value="' + name + '" >';
                    html += '<input type="hidden"  name="perposed_team_type[' + stage + '][]"  value="' + type + '" >';
                    html += '<input type="hidden"  name="perposedTeamData" data-stage="' + stage + '" data-team="' + team_member + '" data-grade_id="' + gradeId + '"  data-auditor_standard_id="' + perposedStandardId + '" value="' + stage + '_' + id + '" >';
                    html += '<span class="remove_perpose_team" ><a href="#" style="color:red">X</a></span></td></tr>';
                }
            }


            var check = 0;
            $(grade_id_selected).each(function (i, d) {
                if (d === gradeId.toString()) {
                    check = 1;
                }
            });

            if (stage == null) {
                var perposedTeam = $("input[name='perposedTeam[]']").map(function () {
                    return $(this).val();
                }).get();

                var check1 = 0;
                $(perposedTeam).each(function (i, d) {
                    if (d === id.toString()) {
                        check1 = 1;
                    }
                });

                if (is_multiple == 0) {
                    if (check == 1) {
                        //replace allready selected perpose team
                        if (check1 == 1) {
                            //dont replace same checked radio
                            if (gradeId == 1 || gradeId == 7) {
                                $(".tr-" + gradeId).remove();
                                $(".proposed_team_tbl > tbody").append(html);
                            }
                        } else {
                            // replace to diffrent person checked radio
                            $(".tr-" + gradeId).remove();
                            $(".proposed_team_tbl > tbody").append(html);
                        }
                    } else {
                        //add allready selected perpose team
                        $(".proposed_team_tbl > tbody").append(html);
                    }
                } else {
                    $(".proposed_team_tbl > tbody").append(html);
                }


            } else {

                var perposedTeam = $("input[name='perposedTeam_" + stage + "[]']").map(function () {
                    return $(this).val();
                }).get();

                var check1 = 0;
                $(perposedTeam).each(function (i, d) {
                    if (d === id.toString()) {
                        check1 = 1;
                    }
                });

                if (is_multiple == 0) {
                    if (check == 1) {
                        //replace allready selected perpose team
                        if (gradeId == 1 || gradeId == 7) {
                            $(".tr-" + gradeId).remove();
                            $(".proposed_team_tbl > tbody").append(html);
                        }
                        if (check1 == 1) {
                            //dont replace same checked radio
                        } else {
                            // replace to diffrent person checked radio
                            $(".tr-" + stage + "_" + gradeId).remove();
                            $(".proposed_team_tbl_" + stage + " > tbody").append(html);
                        }
                    } else {
                        //add allready selected perpose team
                        $(".proposed_team_tbl_" + stage + " > tbody").append(html);
                    }
                } else {
                    $(".proposed_team_tbl_" + stage + " > tbody").append(html);
                }
            }
        }

        function auditorStandaradProposedTeamOut(team_member, id, perposedStandardId, gradeId, is_multiple, name, type, stage = null) {

            var html = '';

            var grade_id_selected = $("input[name='perposed_team_grade_out_id[]']")
                .map(function () {
                    return $(this).val();
                }).get();
            var audit_type = '{{ $audit_type }}';


            if (team_member) {

                html += '<tr id="tr-out_' + id + '" class="tr-out_' + gradeId + '"><td>' + team_member;
                html += '<input type="hidden"  name="perposedTeam_out[]" value="' + id + '" >';
                html += '<input type="hidden"  name="perposedTeam[]" value="' + id + '" >';
                html += '<input type="hidden"  name="perposed_team_grade_id[]" value="' + gradeId + '" >';
                html += '<input type="hidden"  name="perposed_team_name[]"  value="' + name + '" >';
                html += '<input type="hidden"  name="perposed_team_type[]"  value="' + type + '" >';
                html += '<input type="hidden"  name="perposedTeamData_out" data-stage="' + stage + '" data-team="' + team_member + '" data-grade_id="' + gradeId + '"  data-auditor_standard_id="' + perposedStandardId + '" value="' + id + '" >';
                html += '<span class="remove_perpose_team_out"  ><a href="#" style="color:red">X</a></span></td></tr>';
            }

            var check = 0;
            $(grade_id_selected).each(function (i, d) {
                if (d === gradeId.toString()) {
                    check = 1;
                }
            });

            if (stage == null) {
                var perposedTeam = $("input[name='perposedTeam_out[]']").map(function () {
                    return $(this).val();
                }).get();

                var check1 = 0;
                $(perposedTeam).each(function (i, d) {
                    if (d === id.toString()) {
                        check1 = 1;
                    }
                });

                if (is_multiple == 0) {
                    if (check == 1) {
                        //replace allready selected perpose team
                        if (check1 == 1) {
                            //dont replace same checked radio
                        } else {
                            // replace to diffrent person checked radio
                            $(".tr-" + gradeId).remove();
                            $(".proposed_team_tbl > tbody").append(html);
                        }
                    } else {
                        //add allready selected perpose team
                        $(".proposed_team_tbl > tbody").append(html);
                    }
                } else {
                    $(".proposed_team_tbl > tbody").append(html);
                }
            }
        }


        function auditorStandaradTeam(grade_id, company_id, standard_id, is_multiple, stage = null) {


            var audit_type = '{{ $audit_type }}';
            if (stage) {
                var perposedTeam = $("input[name='perposedTeam_" + stage + "[]']")
                    .map(function () {
                        return $(this).val();
                    }).get();

            } else {
                var perposedTeam = $("input[name='perposedTeam[]']")
                    .map(function () {
                        return $(this).val();
                    }).get();
            }

            var perposed_team_grade_id = $("input[name='perposed_team_grade_id[]']")
                .map(function () {
                    return $(this).val();
                }).get();

            if (stage && audit_type == 'stage_1|stage_2') {
                var supervisorInvolve = $("input:radio.supervisor_involved_" + stage + ":checked").val();
            } else {
                var supervisorInvolve = $("input:radio.supervisor_involved:checked").val();
            }


            var request = {
                "grade_id": grade_id,
                "company_id": company_id,
                "standard_id": standard_id,
                "perposedTeam": perposedTeam,
                "perposed_team_grade_id": perposed_team_grade_id

            };

            $.ajax({
                type: "GET",
                url: "{{ route('ajax.auditorStandaradTeam') }}",
                data: request,
                dataType: "json",
                cache: true,
                success: function (response) {
                    if (response.status == "success") {
                        var html = '';
                        html += '<input type="hidden"  name="is_multiple" value="' + is_multiple + '" >';
                        response.data.auditors.forEach(function (data, index) {
                            var audit_type = '{{ $audit_type }}';
                            if (stage && audit_type == 'stage_1|stage_2') {
                                html += '<div class="teamMemberInline"><label for="grade-' + stage + '_' + data.id + '" class="hide-stage1" style="margin-left: 7px;" >';
                            } else {
                                html += '<div class="teamMemberInline"><label for="grade-' + data.id + '" class="hide-stage1" style="margin-left: 7px;" >';
                            }
                            var type = data.auditorStandards[0].auditorStandardGrades[0].grade.short_name;
                            var type1 = '';
                            if (supervisorInvolve == "yes" && grade_id == 6) {
                                type1 = "SLA";
                            }
                            if (stage && audit_type == 'stage_1|stage_2') {
                                if (is_multiple == 0) {
                                    html += '<input type="radio" id="grade-' + stage + '_' + data.id + '" name="auditor_team_' + stage + '"  data-name="' + data.first_name + ' ' + data.last_name + '" data-type="' + type + '"  data-auditor_standard_id="' + data.auditorStandards[0].id + '" data-grade_id="' + grade_id + '"  data-id="' + data.id + '"   value="' + data.id + '" class="multi-stage1" onchange="">';
                                } else {
                                    html += '<input type="checkbox" id="grade-' + stage + '_' + data.id + '" name="auditor_team_' + stage + '" data-name="' + data.first_name + ' ' + data.last_name + '" data-type="' + type + '" data-auditor_standard_id="' + data.auditorStandards[0].id + '" data-grade_id="' + grade_id + '"  data-id="' + data.id + '"   value="' + data.id + '">';
                                }
                            } else {

                                if (is_multiple == 0) {
                                    html += '<input type="radio" id="grade-' + data.id + '" name="auditor_team_stage"  data-name="' + data.first_name + ' ' + data.last_name + '" data-type="' + type + '"  data-auditor_standard_id="' + data.auditorStandards[0].id + '" data-grade_id="' + grade_id + '"  data-id="' + data.id + '"   value="' + data.id + '" class="multi-stage1" onchange="">';
                                } else {
                                    html += '<input type="checkbox" id="grade-' + data.id + '" name="auditor_team_stage" data-name="' + data.first_name + ' ' + data.last_name + '" data-type="' + type + '" data-auditor_standard_id="' + data.auditorStandards[0].id + '" data-grade_id="' + grade_id + '"  data-id="' + data.id + '"   value="' + data.id + '">';
                                }
                            }
                            if (supervisorInvolve == "yes" && grade_id == 6) {
                                html += ' ' + data.first_name + ' ' + data.last_name + ' (' + type1 + ') </label><a href="#" class="hrefcolor" onclick="partialModel(' + data.auditorStandards[0].id + ')"> <i class="fa fa-eye"></i></a></div>';
                            } else {
                                html += ' ' + data.first_name + ' ' + data.last_name + ' (' + type + ') </label><a href="#" class="hrefcolor" onclick="partialModel(' + data.auditorStandards[0].id + ')"> <i class="fa fa-eye"></i></a></div>';

                            }

                        });


                        if (stage && audit_type == 'stage_1|stage_2') {

                            $('#teamauditors_' + stage).html(html);

                        } else {

                            $('#teamauditors').html(html);

                        }

                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });
        }

        function saveChnageStandardVersionInCompany() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('justification.transfer.addchangeversioncomapnystandard') }}",
                data: {
                    standard_id: $("#std_number option:selected").val(),
                    company_id: $("#company_id").val(),
                    old_standard_id: "{{$standard_number->id}}"
                },
                success: function (response) {
                    if (response.status == true) {
                        toastr['success'](response.message);
                        $('#team-view-stage1').empty();
                        $('#teamauditors').empty();
                    } else {
                        toastr['warning'](response.message);
                    }
                },
                error: function (error) {
                    toastr['error']('something went wrong');
                }
            });
        }


    </script>
    <style>
        .addScroll {
            height: 500px !important;
            overflow-y: auto !important;
        }

        a.hrefcolor {

            color: #000;
            font-size: 15px;
        }
    </style>
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#FormalEducationTableCodes').dataTable();
        });
    </script>
@endpush
