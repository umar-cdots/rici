@extends('layouts.master')
@section('title', "Special Audit Justification")

@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 608.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">View <span>AUDIT JUSTIFICATION</span></h1>

                        @if($aj_standard_stage->status == 'approved')
                            <div class="col-md-6 floting view_manday float-right">
                                <a href="{{asset(''.$href.'')}}" target="_blank"><i
                                            class="fa fa-print"></i></a>
                            </div>
                        @endif
                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">
                            <div class="col-md-6 floting">COMPANY INFORMATION</div>

                            @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')

                                <div class="col-md-6 floting view_manday">
                                    <a href="{{ route('company.edit', $company->id) }}" target="_blank"><i
                                                class="fa fa-edit"></i>EDIT</a>
                                </div>
                            @endif
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="#" method="post">
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Client/Company Name</label>
                                        <p class="results_answer">{{ $company->name ?? '' }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Head Office</label>
                                        <p class="results_answer">{{ $company->address ?? '' }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Other Locations &nbsp;
                                            @if(!empty($company->childCompanies) && $company->childCompanies->count() > 2)
                                                <a href="#" data-toggle="modal" data-target="#viewAllAJ"><strong><i
                                                                class="far fa-eye"></i> View All</strong></a>
                                            @endif
                                        </label>
                                        <p class="results_answer">
                                            {{ $company->childCompanies->isNotEmpty() ? $company->childCompanies[0]->address : ''}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>AJ Creation Date</label>
                                        <p class="results_answer">{{ date('d-m-Y' , strtotime($aj_standard_stage->aj_date)) }}</p>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Title &amp; Name</label>
                                        <p class="results_answer">{{ $company->primaryContact->getNameWithTitle() }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <p class="results_answer">{{ $company->primaryContact->position }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cell Phone</label>
                                        <p class="results_answer">{{ $company->primaryContact->contact }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Landline No. / Fax No.</label>
                                        <p class="results_answer">{{ $company->primaryContact->landline }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <p class="results_answer">{{ $company->primaryContact->email }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>No. of Sites</label>
                                        <p class="results_answer">{{ $company->childCompanies ?  $company->childCompanies->count() + 1 : 1 }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Total Employees</label>
                                        @if($company->childCompanies->count() > 0)
                                            <p class="results_answer">{{ $company->total_employees + $company->childCompanies->sum('total_employees') }}</p>
                                        @else
                                            <p class="results_answer">{{ $company->total_employees }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Effective Employees</label>
                                        <p class="results_answer">{{ $company->effective_employees }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
                <form role="form" id="main-aj-form" method="post">
                    <input type="hidden" value="{{ $company->id }}" name="company_id"/>
                    @if(isset($aj_standard_stage))
                        <input type="hidden" value="{{ $aj_standard_stage->id }}" name="standard_stage_id">
                    @endif
                    <input type="hidden" value="{{ ($audit_type != '') ? $audit_type : '' }}" name="audit_type"/>
                    <input type="hidden" value="{{ ($team_audit_type != '') ? $team_audit_type : '' }}"
                           name="team_audit_type"/>
                    <input type="hidden" value="{{ $ims }}" name="is_ims">
                    @if(isset($std_number))
                        <input type="hidden" value="{{ $std_number }}" name="standard_number[]"/>
                    @endif

                    @if(isset($company_ims_standards))
                        @foreach($company_ims_standards as $standard)
                            <input type="hidden" value="{{ $standard->standard->name }}" name="standard_number[]"/>
                        @endforeach
                    @endif
                    <input type="hidden" value="{{ $combine_tracker}}" name="combine_tracker">
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">


                            <h3 class="card-title">
                                <div class="col-md-6 floting">STANDARDS INFORMATION</div>

                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')

                                    <div class="col-md-6 floting view_manday">
                                        <a href="{{ route('company.edit', $company->id) }}" target="_blank"><i
                                                    class="fa fa-edit"></i>EDIT</a>
                                    </div>
                                @endif
                            </h3>

                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom disabledDiv">
                            <div class="row">
                                @php
                                    $getAllImsStandradsData = \App\CompanyStandards::where('standard_id',$standard_number->id)->where('company_id',$company->id)->first();
                                    $ims_standard_ids = explode(',',$getAllImsStandradsData->ims_standard_ids);
                                @endphp
                                @if(!is_null($ims_standard_ids))
                                    @foreach ($ims_standard_ids as $key=>$item)
                                        <div class="col-md-12">
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>IAF Code</label>
                                                    <p class="results_answer" id="iaf-codes{{$key}}">
                                                        @foreach($company->companyStandards as $c_standard)
                                                            @if($c_standard->id === (int)$item)

                                                                @if(!empty($c_standard->companyStandardCodes))
                                                                    @foreach($c_standard->companyStandardCodes as $companyIaf)
                                                                        {{ $companyIaf->iaf->code ?? '' }}
                                                                        @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                            ,
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach

                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>IAS Code</label>
                                                    <p class="results_answer" id="ias-codes{{$key}}">
                                                        @foreach($company->companyStandards as $c_standard)
                                                            @if($c_standard->id === (int)$item)
                                                                @if(!empty($c_standard->companyStandardCodes))
                                                                    @foreach($c_standard->companyStandardCodes as $companyIas)
                                                                        {{ $companyIas->ias->code ?? '' }}
                                                                        @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                            ,
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            @if(!empty($company->companyStandards))
                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->id === (int)$item)
                                                        @php   $frequency =  $c_standard->surveillance_frequency @endphp
                                                        <div class="col-md-4 floting">
                                                            <div class="form-group">
                                                                <label>Frequency</label>
                                                                <p class="results_answer">
                                                                    {{$frequency}}
                                                                    <input type="hidden" name="frequency"
                                                                           value="{{$frequency}}">
                                                                </p>
                                                            </div>
                                                        </div>

                                                    @endif

                                                @endforeach
                                            @endif

                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Standards</label>
                                                    <p class="results_answer">
                                                        @if(!empty($company->companyStandards))
                                                            @foreach($company->companyStandards as $standard)
                                                                @if($standard->id === (int)$item)
                                                                    {{ $standard->standard->name }}
                                                                @endif

                                                            @endforeach
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Codes Accreditation</label>
                                                    <p class="results_answer" id="codes-accreditations{{$key}}">

                                                        @foreach($company->companyStandards as $c_standard)
                                                            @if($c_standard->id === (int)$item)
                                                                @if(!empty($c_standard->companyStandardCodes))
                                                                    @foreach($c_standard->companyStandardCodes as $accreditation)
                                                                        {{ $accreditation->accreditation->name ?? '' }}
                                                                        @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                            ,
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 floting">
                                                <div class="form-group">
                                                    <label>Complexity</label>
                                                    <p class="results_answer">
                                                        @if(!empty($company->companyStandards))
                                                            @foreach($company->companyStandards as $standard)
                                                                @if($standard->id === (int)$item)
                                                                    {{ $standard->proposed_complexity }}
                                                                @endif

                                                            @endforeach
                                                        @endif</p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <hr style="color: black">

                                    @endforeach
                                @endif
                                <div class="col-md-12">
                                    <div class="form-group disabledDiv">
                                        <label>Certification Scope</label>
                                        <textarea rows="3" class="form-control" name="certificate_scope"
                                                  id="certificate_scope">{{ $aj_standard_stage->certificate_scope != null ?  $aj_standard_stage->certificate_scope : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">

                                    @if(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Food')
                                        @php

                                            $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->get();

                                            foreach($standardQuestions as $index=>$questions){

                                                if($index === 0){
                                                    $hcca_plans=$questions->answer;

                                                }
                                                elseif($index === 1){
                                                  $certified_management=$questions->answer;
                                                }

                                            }

                                        @endphp
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Category Code</label>
                                                <p class="results_answer" id="food-category-codes">


                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $companyIaf)
                                                                    {{ $companyIaf->foodcategory->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach

                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Category SubCode</label>
                                                <p class="results_answer" id="food-subcategory-codes">
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $companyIas)
                                                                    {{ $companyIas->foodsubcategory->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Code Accreditation</label>
                                                <p class="results_answer" id="food-accreditations">

                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $accreditation)
                                                                    {{ $accreditation->accreditation->name ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label> HACCP Plan </label>
                                                <p class="results_answer">{{ $hcca_plans }}</p>
                                            </div>
                                        </div>
                                    @elseif(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Energy Management')
                                        @php

                                            $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->get();

                                            foreach($standardQuestions as $index=>$questions){

                                                 if($index === 1){
                                                  $energy_consumption_in_tj=$questions->answer;

                                                }
                                                elseif($index === 3){
                                                  $no_of_energy_resources=$questions->answer;
                                                }
                                                elseif($index === 4){
                                                    $no_of_significant_energy=$questions->answer;
                                                }
                                            }

                                        @endphp
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Codes</label>
                                                <p class="results_answer" id="energy-codes">
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardEnergyCodes))
                                                                @foreach($c_standard->companyStandardEnergyCodes as $companyIas)
                                                                    {{ $companyIas->energycode->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Codes Accreditation</label>
                                                <p class="results_answer" id="energy-accreditations">

                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardEnergyCodes))
                                                                @foreach($c_standard->companyStandardEnergyCodes as $accreditation)
                                                                    {{ $accreditation->accreditation->name ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Consumption in TJ</label>
                                                <p class="results_answer">{{$energy_consumption_in_tj}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Number of energy sources</label>
                                                <p class="results_answer">{{$no_of_energy_resources}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Number of significant energy and uses (SEUs)</label>
                                                <p class="results_answer">{{$no_of_significant_energy}}</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                @if(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Food')
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label> Certified To Other Management System? </label>
                                            <p class="results_answer">{{$certified_management == 1 ? 'YES' : 'NO'}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">
                                <div class="col-md-6 floting">MAN DAYS DETAILS</div>

                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')
                                    <div class="col-md-6 floting view_manday">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"
                                                                                data-target="#viewModal"
                                                                                data-toggle="modal"><i
                                                    class="fa fa-eye"></i> VIEW HISTORY</a></div>
                                @endif
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom disabledDiv">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Man-Day Table</label>
                                        <table class="table table-bordered text-center manday_tbl">
                                            <tbody>
                                            <tr>
                                                <th>Audit Type</th>
                                                <th>Onsite</th>
                                                <th>Offsite</th>
                                            </tr>

                                            @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')
                                                @foreach($aj_stages_manday as $manday)
                                                    <tr>
                                                        <td>{{ $manday->getAuditType() }}</td>
                                                        <td>
                                                            <span ondblclick="showInput('onsite',{{ $loop->iteration }})"
                                                                  id="onsite{{ $loop->iteration }}">{{ $manday->onsite }}</span>
                                                            <input id="input-onsite-{{ $loop->iteration }}"
                                                                   name="onsite"
                                                                   onkeypress="doneEdit('onsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                   onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'onsite', {{ $manday->id }})"
                                                                   style="display:none; width: 60px; margin:auto;"
                                                                   type="text"
                                                                   class="form-control"
                                                                   value="{{ $manday->onsite }}">
                                                        </td>

                                                        <td>
                                                            <span ondblclick="showInput('offsite',{{ $loop->iteration }})"
                                                                  id="offsite{{ $loop->iteration }}">{{ $manday->offsite }}</span>
                                                            <input id="input-offsite-{{ $loop->iteration }}"
                                                                   name="offsite"
                                                                   onkeypress="doneEdit('offsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                   onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'offsite', {{ $manday->id }})"
                                                                   style="display:none; width: 60px; margin:auto;"
                                                                   type="text"
                                                                   class="form-control"
                                                                   value="{{ $manday->offsite }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else

                                                @foreach($aj_stages_manday as $manday)

                                                    <tr>
                                                        <td>{{ $manday->getAuditType() }}</td>
                                                        <td>
                                                            <span id="onsite{{ $loop->iteration }}">{{ $manday->onsite }}</span>
                                                            <input id="input-onsite-{{ $loop->iteration }}"
                                                                   name="onsite"
                                                                   onkeypress="doneEdit('onsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                   onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'onsite', {{ $manday->id }})"
                                                                   style="display:none; width: 60px; margin:auto;"
                                                                   type="text"
                                                                   class="form-control"
                                                                   value="{{ $manday->onsite }}">
                                                        </td>

                                                        <td>
                                                            <span id="offsite{{ $loop->iteration }}">{{ $manday->offsite }}</span>
                                                            <input id="input-offsite-{{ $loop->iteration }}"
                                                                   name="offsite"
                                                                   onkeypress="doneEdit('offsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                   onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'offsite', {{ $manday->id }})"
                                                                   style="display:none; width: 60px; margin:auto;"
                                                                   type="text"
                                                                   class="form-control"
                                                                   value="{{ $manday->offsite }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6 floting">
                                    <div class="form-group">
                                        <label>Remarks / Justification for reduction in man-days</label>
                                        <textarea rows="3" class="form-control" readonly
                                                  name="manday_remarks">{{ $aj_standard_stage->manday_remarks != null ?  $aj_standard_stage->manday_remarks : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">
                                <div class="col-md-6 floting">AUDIT TEAM SELECTION</div>
                            </h3>
                        </div>
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="callout callout-warning custom_callout disabledDiv">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Audit Type</label>
                                                @php
                                                    $audit_type = explode('_', $audit_type);

                                                    if(sizeOf($audit_type) === 2){
                                                        $new_audit_type= ucfirst($audit_type[0]).' + '.ucfirst($audit_type[1]);
                                                    }
                                                    if(sizeOf($audit_type) === 3){
                                                        $new_audit_type= ucfirst($audit_type[0]).' + '.ucfirst($audit_type[1]).' '.ucfirst($audit_type[2]);
                                                    }
                                                    elseif(sizeOf($audit_type) === 4){
                                                        $new_audit_type= ucfirst($audit_type[0]).' '.$audit_type[1].' + '.ucfirst($audit_type[2]).' '.ucfirst($audit_type[3]);
                                                    }
                                                @endphp
                                                <p class="results_answer">{{ $new_audit_type }}</p>
                                            </div>
                                        </div>
                                        {{--                                        <div class="col-md-4">--}}
                                        {{--                                            <div class="form-group">--}}
                                        {{--                                                <label>Certification Status</label>--}}

                                        {{--                                                <p class="results_answer"> Not Certified</p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    @if($combine_tracker == 0)

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Is Supervising Lead Auditor Involved?</label>
                                                <div class="form-group">
                                                    <div class="col-md-4 floting no_padding">
                                                        <div>
                                                            <input type="radio" name="supervisor_involved[stage1]"
                                                                   value="yes"
                                                                   id="supervisor_involved"
                                                                   onchange="hideItems(this, 'stage1')"
                                                                    {{($aj_standard_stage->supervisor_involved === "yes") ? 'checked' : ''}}/>
                                                        </div>
                                                        <label class="job_status">Yes</label>
                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div>
                                                            <input type="radio" name="supervisor_involved[stage1]"
                                                                   value="no"
                                                                   id="supervisor_involved"
                                                                   onchange="hideItems(this, 'stage1')"
                                                                    {{($aj_standard_stage->supervisor_involved === "no" || $aj_standard_stage->supervisor_involved === null)  ? 'checked' : ''}}/>
                                                        </div>
                                                        <label class="job_status">No</label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Expected Date</label>

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                              <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                              </span>
                                                        </div>
                                                        <input type="date" name="expected_date[]" readonly
                                                               value="{{ $aj_standard_stage->excepted_date !=null ? $aj_standard_stage->excepted_date : ''}}"
                                                               class="form-control float-right active">
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Proposed Team</label>
                                                <table class="table table-bordered proposed_team_tbl">
                                                    <tbody id="team-view-{{ str_replace('_', '', $team_audit_type) }}">
                                                    @foreach($team_members as $key=>$team)
                                                        <tr id="{{str_replace('_', '', $team_audit_type)}}node-{{$key}}">
                                                            <td>
                                                                {{$team->member_name}} ( {{$team->member_type}} )
                                                            </td>
                                                            <input type="hidden"
                                                                   id="{{str_replace('_', '', $team_audit_type)}}_{{$team->id}}"
                                                                   name="auditor_member[{{str_replace('_', '', $team_audit_type)}}][]"
                                                                   value="{{$team->member_name}}">
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    @if( $audit_type === 'Surveillance 1 + Verification' || $audit_type === 'Surveillance 2 + Verification' || $audit_type === 'Surveillance 3 + Verification' || $audit_type === 'Surveillance 4 + Verification' || $audit_type === 'Surveillance 5 + Verification' || $audit_type === 'Reaudit + Verification'
                                    ||  $audit_type === 'Surveillance 1 + Scope Extension' || $audit_type === 'Surveillance 2 + Scope Extension' || $audit_type === 'Surveillance 3 + Scope Extension' || $audit_type === 'Surveillance 4 + Scope Extension' || $audit_type === 'Surveillance 5 + Scope Extension' || $audit_type === 'Reaudit + Scope Extension'
                                    ||  $audit_type === 'Surveillance 1 + Special Transition' || $audit_type === 'Surveillance 2 + Special Transition' || $audit_type === 'Surveillance 3 + Special Transition' || $audit_type === 'Surveillance 4 + Special Transition' || $audit_type === 'Surveillance 5 + Special Transition' || $audit_type === 'Reaudit + Special Transition'
                                    )
                                        @if($aj_standard_change->count() >0 || !empty($aj_standard_change))
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Change Confirmation Checklist:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q1: Any change in Name</label>
                                                <p class="results_answer">{{ $aj_standard_change->name }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q2: Any change in Scope</label>
                                                <p class="results_answer">{{ $aj_standard_change->scope }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q3: Any change in Address / Number of locations</label>
                                                <p class="results_answer">{{ $aj_standard_change->location }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q4: Any change in the version of Standard.</label>
                                                <p class="results_answer">{{ $aj_standard_change->version }}</p>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            @if(!empty($aj_standard_change))
                                @if($aj_standard_change->change_in_version === 'yes')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Change Details:</label>
                                                <textarea name="remarks" rows="4" class="form-control"
                                                          placeholder="Remarks"
                                                          readonly>{{ $aj_standard_change->remarks ? $aj_standard_stage->remarks :''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Job Number</label>
                                        <input type="text" class="form-control" name="job_number" disabled
                                               value="{{ $aj_standard_stage->job_number }}">
                                        {{-- <p class="results_answer">{{ $aj_standard_stage->job_number ? $aj_standard_stage->job_number : '---' }}</p> --}}
                                    </div>
                                </div>
                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Evaluator Name</label>
                                            <input type="text" name="evaluator_name" class="form-control" readonly
                                                   value="{{ $aj_standard_stage->evaluator_name !=null ? $aj_standard_stage->evaluator_name : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Approval Date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                  </span>
                                                </div>
                                                <input type="date" name="approval_date" readonly
                                                       value="{{ $aj_standard_stage->approval_date !=null ? $aj_standard_stage->approval_date : ''}}"
                                                       class="form-control float-right active">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-12 floting">
                                    <div class="form-group">
                                        <label>Special AJ Remarks</label>
                                        <textarea rows="3" class="form-control" readonly
                                                  name="special_aj_remarks">{{ $aj_standard_stage->special_aj_remarks != null ?  $aj_standard_stage->special_aj_remarks : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4"></div>
                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager')
                                    @if(!empty($notifications) && count($notifications) > 0)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Comments Box</label>
                                                <div class="card-body chat_box" style="display: block;">
                                                    <div class="direct-chat-messages">


                                                        @foreach($notifications as $key=>$notification)

                                                            @if($notification->sent_by === Auth::user()->id )
                                                                <div class="direct-chat-msg">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                </div>

                                                            @else
                                                                <div class="direct-chat-msg text-right">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                </div>
                                                            @endif
                                                        @endforeach


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @endif
                                <div class="col-md-4"></div>


                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="viewAllAJ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form action="#" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Other Locations</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    @php $i=2; @endphp
                                    @foreach($company->childCompanies as $child)
                                        <label><strong>Address {{$i}}</strong></label>
                                        <p>{{ $child->address }}</p>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade viewModal2 show" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-eye"></i>VIEW HISTORY</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h3>Man Days History</h3>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div id="example1_filter" class="dataTables_filter">
                                    <form action="#" method="post">
                                        <label>
                                            <input type="text" class="form-control form-control-sm" id="myFilter"
                                                   placeholder="Search" aria-controls="example1">
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable no-footer"
                                       role="grid" aria-describedby="example1_info">
                                    <thead>
                                    <tr role="row green_row">
                                        <th width="10%;">AJ Creation Date</th>
                                        <th width="15%;">Audit Type</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                        <th width="12%;">Acutal Audit Date</th>
                                        <th>Remarks / Justification For Reduction</th>
                                    </tr>
                                    </thead>
                                    <tbody id="myTable">


                                    @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type == 'admin')
                                        @foreach($aj_stages_manday as $manday)

                                            <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                <td>{{$manday->created_at->format('d-m-Y')}}</td>
                                                <td>{{ $manday->getAuditType() }}</td>
                                                <td>{{$manday->onsite}}</td>
                                                <td>{{$manday->offsite}}</td>
                                                <td>{{$manday->updated_at->format('d-m-Y')}}</td>
                                                <td>{{$manday->manday_remarks}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class='notfound' style="display:none">
                                            <td colspan='6'>No record found</td>
                                        </tr>

                                    @else
                                        @foreach($aj_stages_manday as $manday)

                                            <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                <td>{{$manday->created_at->format('d-m-Y')}}</td>
                                                <td>{{ $manday->getAuditType() }}</td>
                                                <td>{{$manday->onsite}}</td>
                                                <td>{{$manday->offsite}}</td>
                                                <td>{{$manday->updated_at->format('d-m-Y')}}</td>
                                                <td>{{$manday->manday_remarks}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class='notfound' style="display:none">
                                            <td colspan='6'>No record found</td>
                                        </tr>
                                    @endif


                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@push('styles')
    <style>
        .team-member-delete-button {
            border: none;
            background: none;
            float: right;
        }

        .my-active span {
            background-color: #5cb85c !important;
            color: white !important;
            border-color: #5cb85c !important;
        }

        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            function unique(list) {
                var result = [];
                $.each(list, function (i, e) {
                    if ($.inArray(e, result) == -1) result.push(e);
                });
                return result;
            }

            function removeDuplicates(id) {
                var records = $(id).text();
                records = records.replace(/\s/g, '');
                let records_array = records.split(',');
                var final_records = unique(records_array);
                $(id).text(final_records.toString());
                return true;
            }

            var iaf_codes = removeDuplicates('#iaf-codes0');
            var iaf_codes1 = removeDuplicates('#iaf-codes1');
            var iaf_codes2 = removeDuplicates('#iaf-codes2');
            var ias_codes = removeDuplicates('#ias-codes0');
            var ias_codes1 = removeDuplicates('#ias-codes1');
            var ias_codes2 = removeDuplicates('#ias-codes2');
            var accreditations_codes = removeDuplicates('#codes-accreditations0');
            var accreditations_codes1 = removeDuplicates('#codes-accreditations1');
            var accreditations_codes2 = removeDuplicates('#codes-accreditations2');
            var food_accreditations = removeDuplicates('#food-accreditations');
            var energy_accreditations = removeDuplicates('#energy-accreditations');
            var food_category_codes = removeDuplicates('#food-category-codes');
            var food_subcategory_codes = removeDuplicates('#food-subcategory-codes');
            var energy_codes = removeDuplicates('#energy-codes');
        });

        function yesnoCheck() {
            if (document.getElementById('yesCheck').checked) {
                document.getElementById('ifYes').style.display = 'block';
                document.getElementById('changeremarks').style.display = 'block';
            } else {
                document.getElementById('ifYes').style.display = 'none';
                document.getElementById('changeremarks').style.display = 'none';
            }

        }


        // Case-insensitive searching (Note - remove the below script for Case sensitive search )
        $.expr[":"].contains = $.expr.createPseudo(function (arg) {
            return function (elem) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        $("#myFilter").on("keyup", function () {
            // Search Text
            var search = $(this).val();

            // Hide all table tbody rows
            $('#myTable tr').hide();

            // Count total search result
            var len = $('#myTable tr:not(.notfound) td:contains("' + search + '")').length;

            if (len > 0) {
                // Searching text in columns and show match row
                $('#myTable tr:not(.notfound) td:contains("' + search + '")').filter(function () {

                    $(this).closest('tr').show();
                });
            } else {
                $('.notfound').show();
            }

            @if( $audit_type === 'Surveillance 1 + Verification' || $audit_type === 'Surveillance 2 + Verification' || $audit_type === 'Surveillance 3 + Verification' || $audit_type === 'Surveillance 4 + Verification' || $audit_type === 'Surveillance 5 + Verification' || $audit_type === 'Reaudit + Verification'
            ||  $audit_type === 'Surveillance 1 + Scope Extension' || $audit_type === 'Surveillance 2 + Scope Extension' || $audit_type === 'Surveillance 3 + Scope Extension' || $audit_type === 'Surveillance 4 + Scope Extension' || $audit_type === 'Surveillance 5 + Scope Extension' || $audit_type === 'Reaudit + Scope Extension'
            ||  $audit_type === 'Surveillance 1 + Special Transition' || $audit_type === 'Surveillance 2 + Special Transition' || $audit_type === 'Surveillance 3 + Special Transition' || $audit_type === 'Surveillance 4 + Special Transition' || $audit_type === 'Surveillance 5 + Special Transition' || $audit_type === 'Reaudit + Special Transition'
            )
            (function defaultChecks() {
                yesnoCheck();

            })();


            @endif

        });

    </script>

@endpush
