<div>
    <div class="row">
        <div class="col-md-12">
            <label><b>Region/Country:&nbsp;</b></label>
            <span>
               {{ $auditorStandards->auditor->region->title }}/ {{ $auditorStandards->auditor->country->name  }}
            </span>
        </div>
    </div>
</div>

<table id="FormalEducationTableCodes" cellspacing="20" cellpadding="0" border="0"
       class="table table-striped table-bordered table-bordered2">
    <thead>
    <tr>
        <th>S.No</th>
        <th>Accreditation</th>
        <th>IAF Code</th>
        <th>IAS Code</th>
        <th>Status</th>
        <th>Remarks</th>
        <th>See Full Profile</th>
    </tr>
    </thead>
    <tbody>

    @if(!empty($auditorStandards->auditorStandardCodes) && count($auditorStandards->auditorStandardCodes) > 0)
        @foreach($auditorStandards->auditorStandardCodes as $key => $auditorStandardCode)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{!! $auditorStandardCode->accreditation->name !!}</td>
                <td>{{$auditorStandardCode->iaf->code}}</td>
                <td>{{$auditorStandardCode->ias->code}}</td>
                <td>{{($auditorStandardCode->status)  ? ucfirst($auditorStandardCode->status)  : 'N/A'}}</td>
                <td>{{($auditorStandardCode->remarks) ? ucfirst($auditorStandardCode->remarks) : 'N/A'}}</td>
                <td>

                    @if($auditorStandardCode->auditorStandard->auditor->role === 'auditor')
                        <a href="{{route('auditors.show', $auditorStandardCode->auditorStandard->auditor->id)}}#standardsTab"
                           target="_blank">
                            <i class="fa fa-eye"></i>
                        </a>
                    @elseif($auditorStandardCode->auditorStandard->auditor->role === 'technical_expert')
                        <a href="{{route('technical-expert.show', $auditorStandardCode->auditorStandard->auditor->id)}}#standardsTab"
                           target="_blank">
                            <i class="fa fa-eye"></i>
                        </a>
                    @else
                    @endif


                </td>
            </tr>
        @endforeach
    @else

        <tr>
            <td colspan="6"> Record Not Found</td>
        </tr>

    @endif


    </tbody>

</table>

@push('scripts')
    <script type="text/javascript" src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#FormalEducationTableCodes').dataTable();
        });
    </script>
@endpush