@extends('layouts.master')
@section('title', "Audit Justification")
@push('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@endpush
@section('content')
    <div class="content-wrapper custom_cont_wrapper" style="min-height: 608.4px;">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid dashboard_tabs">
                <div class="row mb-2 mrgn">
                    <div class="col-sm-12">
                        <h1 class="m-0 text-dark dashboard_heading">View <span>AUDIT JUSTIFICATION
                             @if(!is_null($certificateStatus))
                                    <b> (<span style="color: #90c744;    font-weight: 600;">{{ str_replace('_',' ', ucwords($certificateStatus->certificate_status)) }}</span>)</b>
                                @endif
                            </span></h1>

                        @if($aj_standard_stage->status == 'approved')
                            <div class="col-md-6 floting view_manday float-right">
                                <a href="{{asset(''.$href.'')}}" target="_blank"><i
                                            class="fa fa-print"></i></a>
                            </div>
                        @endif

                    </div><!-- /.col -->
                    <!-- /.col -->
                </div>
                <div class="card card-primary mrgn">
                    <div class="card-header cardNewHeader">
                        <h3 class="card-title">
                            <div class="col-md-6 floting">COMPANY INFORMATION</div>

                            @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type === 'admin')

                                <div class="col-md-6 floting view_manday">
                                    <a href="{{ route('company.edit', $company->id) }}" target="_blank"><i
                                                class="fa fa-edit"></i>EDIT</a>
                                </div>
                            @endif
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="#" method="post">
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Client/Company Name</label>
                                        <p class="results_answer">{{ $company->name ?? '' }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Head Office</label>
                                        <p class="results_answer">{{ $company->address ?? '' }}
                                            ,{{ $company->city->name }},{{$company->country->name}}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Other Locations &nbsp;
                                            @if(!empty($company->childCompanies) && $company->childCompanies->count() > 2)
                                                <a href="#" data-toggle="modal" data-target="#viewAllAJ"><strong><i
                                                                class="far fa-eye"></i> View All</strong></a>
                                            @endif
                                        </label>
                                        <p class="results_answer">
                                            @if($company->childCompanies->isNotEmpty())
                                                {{$company->childCompanies[0]->address}}
                                                ,{{ $company->childCompanies[0]->city->name }}
                                                ,{{$company->country->name}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>AJ Creation Date</label>
                                        <p class="results_answer">{{ date('d-m-Y' , strtotime($aj_standard_stage->aj_date)) }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Title &amp; Name</label>
                                        <p class="results_answer">{{ $company->primaryContact->getNameWithTitle() }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Position</label>
                                        <p class="results_answer">{{ $company->primaryContact->position }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cell Phone</label>
                                        <p class="results_answer">{{ $company->primaryContact->contact }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Landline No. / Fax No.</label>
                                        <p class="results_answer">{{ $company->primaryContact->landline }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <p class="results_answer">{{ $company->primaryContact->email }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>No. of Sites</label>
                                        <p class="results_answer">{{ $company->childCompanies ?  $company->childCompanies->count() + 1 : 1 }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Total Employees</label>
                                        @if($company->childCompanies->count() > 0)
                                            <p class="results_answer">{{ $company->total_employees + $company->childCompanies->sum('total_employees') }}</p>
                                        @else
                                            <p class="results_answer">{{ $company->total_employees }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Effective Employees</label>
                                        <p class="results_answer">{{ $company->effective_employees }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>MS</label>
                                        <p class="results_answer">{{ $company->multiple_sides ?? 1 }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
                <form role="form" id="main-aj-form" method="post">
                    <input type="hidden" value="{{ $company->id }}" name="company_id"/>
                    @if(isset($aj_standard_stage))
                        <input type="hidden" value="{{ $aj_standard_stage->id }}" name="standard_stage_id">
                    @endif
                    <input type="hidden" value="{{ ($audit_type != '') ? $audit_type : '' }}" name="audit_type"/>
                    <input type="hidden" value="{{ $ims }}" name="is_ims">
                    @if(isset($std_number))
                        <input type="hidden" value="{{ $std_number }}" name="standard_number[]"/>
                    @endif

                    @if(isset($company_ims_standards))
                        @foreach($company_ims_standards as $standard)
                            <input type="hidden" value="{{ $standard->standard->name }}" name="standard_number[]"/>
                        @endforeach
                    @endif
                    <input type="hidden" value="{{ $combine_tracker}}" name="combine_tracker">
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">


                            <h3 class="card-title">
                                <div class="col-md-6 floting">STANDARDS INFORMATION</div>

                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type === 'admin')

                                    <div class="col-md-6 floting view_manday">
                                        <a href="{{ route('company.edit', $company->id) }}" target="_blank"><i
                                                    class="fa fa-edit"></i>EDIT</a>
                                    </div>
                                @endif
                            </h3>

                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="col-md-8 floting">
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>IAF Code</label>
                                            <p class="results_answer" id="iaf-codes">


                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->standard_id === $standard_number->id)
                                                        @if(!empty($c_standard->companyStandardCodes))
                                                            @foreach($c_standard->companyStandardCodes as $companyIaf)
                                                                {{ $companyIaf->iaf->code ?? '' }}
                                                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>IAS Code</label>
                                            <p class="results_answer" id="ias-codes">
                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->standard_id === $standard_number->id)
                                                        @if(!empty($c_standard->companyStandardCodes))
                                                            @foreach($c_standard->companyStandardCodes as $companyIas)
                                                                {{ $companyIas->ias->code ?? '' }}
                                                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Frequency</label>
                                            <p class="results_answer">
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            {{ ucfirst($c_standard->surveillance_frequency) }}
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Standards</label>
                                            <p class="results_answer">
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $standard)
                                                        @if($standard->standard_id === $standard_number->id)
                                                            {{ $standard->standard->name }}
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Codes Accreditation</label>
                                            <p class="results_answer" id="codes-accreditations">

                                                @foreach($company->companyStandards as $c_standard)
                                                    @if($c_standard->standard_id === $standard_number->id)
                                                        @if(!empty($c_standard->companyStandardCodes))
                                                            @foreach($c_standard->companyStandardCodes as $accreditation)
                                                                {{ $accreditation->accreditation->name ?? '' }}
                                                                @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardCodes) - 1)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label>Complexity</label>
                                            <p class="results_answer">
                                                @if(!empty($company->companyStandards))
                                                    @foreach($company->companyStandards as $standard)
                                                        @if($standard->standard_id === $standard_number->id)
                                                            {{ ucfirst($standard->proposed_complexity) }}
                                                        @endif

                                                    @endforeach
                                                @endif</p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 floting">
                                    <div class="form-group">
                                        <label>Certification Scope</label>
                                        <textarea rows="3" class="form-control disabledDiv" name="certificate_scope"
                                                  id="certificate_scope"
                                                  id="certificate_scope">{{ !is_null($aj_standard_stage->certificate_scope)  ?  $aj_standard_stage->certificate_scope : $company->scope_to_certify }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">

                                    @if(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Food')

                                        @php
                                            $hcca_plans = 0;
                                            $certified_management = 'No';
                                            if(!empty($company->companyStandards)){
                                               foreach($company->companyStandards as $standard){
                                                   if($standard->standard_id === $standard_number->id){
                                                     $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->where('company_standards_id',$standard->id)->whereNull('deleted_at')->get();
                                                       foreach($standardQuestions as $index=>$questions){
                                                           if($index === 0){
                                                               $hcca_plans=$questions->answer;
                                                           }
                                                           elseif($index === 1){
                                                             $certified_management=$questions->answer;
                                                           }

                                                       }
                                                   }
                                               }
                                            }

                                        @endphp

                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Category Code</label>
                                                <p class="results_answer" id="food-category-codes">


                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $companyIaf)
                                                                    {{ $companyIaf->foodcategory->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach

                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Category SubCode</label>
                                                <p class="results_answer" id="food-subcategory-codes">
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $companyIas)
                                                                    {{ $companyIas->foodsubcategory->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Food Code Accreditation</label>
                                                <p class="results_answer" id="food-accreditations">

                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardFoodCodes))
                                                                @foreach($c_standard->companyStandardFoodCodes as $accreditation)
                                                                    {{ $accreditation->accreditation->name ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardFoodCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label> HACCP Plan </label>
                                                <p class="results_answer">{{ $hcca_plans }}</p>
                                            </div>
                                        </div>
                                    @elseif(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Energy Management')
                                        @php

                                            $standardQuestions=App\CompanyStandardsAnswers::where('standard_id',$standard_number->id)->where('company_standards_id',$standard->id)->whereNull('deleted_at')->get();


                                            foreach($standardQuestions as $index=>$questions){

                                                 if($index === 0){
                                                  $energy_consumption_in_tj=$questions->answer;

                                                }
                                                elseif($index === 1){
                                                  $no_of_energy_resources=$questions->answer;
                                                }
                                                elseif($index === 2){
                                                    $no_of_significant_energy=$questions->answer;
                                                }
                                            }

                                        @endphp
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Codes</label>
                                                <p class="results_answer" id="energy-codes">
                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardEnergyCodes))
                                                                @foreach($c_standard->companyStandardEnergyCodes as $companyIas)
                                                                    {{ $companyIas->energycode->code ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Codes Accreditation</label>
                                                <p class="results_answer" id="energy-accreditations">

                                                    @foreach($company->companyStandards as $c_standard)
                                                        @if($c_standard->standard_id === $standard_number->id)
                                                            @if(!empty($c_standard->companyStandardEnergyCodes))
                                                                @foreach($c_standard->companyStandardEnergyCodes as $accreditation)
                                                                    {{ $accreditation->accreditation->name ?? '' }}
                                                                    @if($loop->index >= 0 && $loop->index < count($c_standard->companyStandardEnergyCodes) - 1)
                                                                        ,
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Energy Consumption in TJ</label>
                                                <p class="results_answer">{{$energy_consumption_in_tj}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Number of energy sources</label>
                                                <p class="results_answer">{{$no_of_energy_resources}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 floting">
                                            <div class="form-group">
                                                <label>Number of significant energy and uses (SEUs)</label>
                                                <p class="results_answer">{{$no_of_significant_energy}}</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                @if(App\StandardsFamily::where('id',$standard_number->standards_family_id)->first()->name === 'Food')
                                    <div class="col-md-4 floting">
                                        <div class="form-group">
                                            <label> Certified To Other Management System? </label>
                                            <p class="results_answer">{{$certified_management == "yes" ? 'Yes' : 'No'}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">
                                <div class="col-md-6 floting">MAN DAYS DETAILS</div>

                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type === 'admin')
                                    <div class="col-md-6 floting view_manday">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"
                                                                                data-target="#viewModal"
                                                                                data-toggle="modal"><i
                                                    class="fa fa-eye"></i> VIEW HISTORY</a></div>
                                @endif
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body card_cutom disabledDiv">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Man-Day Table</label>
                                        <table class="table table-bordered text-center manday_tbl">
                                            <tbody>
                                            <tr>
                                                <th>Audit Type</th>
                                                <th>Onsite</th>
                                                <th>Offsite</th>
                                            </tr>

                                            @if(auth()->user()->user_type === 'scheme_manager'  || auth()->user()->user_type === 'admin')
                                                @foreach($aj_stages_manday as $stage_manday)
                                                    @foreach($stage_manday as $manday)
                                                        <tr>
                                                            <td>{{ $manday->getAuditType() }}</td>
                                                            <td>
                                                            <span ondblclick="showInput('onsite',{{ $loop->iteration }})"
                                                                  id="onsite{{ $loop->iteration }}">{{ $manday->onsite }}</span>
                                                                <input id="input-onsite-{{ $loop->iteration }}"
                                                                       name="onsite"
                                                                       onkeypress="doneEdit('onsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                       onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'onsite', {{ $manday->id }})"
                                                                       style="display:none; width: 60px; margin:auto;"
                                                                       type="text"
                                                                       class="form-control"
                                                                       value="{{ $manday->onsite }}">
                                                            </td>

                                                            <td>
                                                            <span ondblclick="showInput('offsite',{{ $loop->iteration }})"
                                                                  id="offsite{{ $loop->iteration }}">{{ $manday->offsite }}</span>
                                                                <input id="input-offsite-{{ $loop->iteration }}"
                                                                       name="offsite"
                                                                       onkeypress="doneEdit('offsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                       onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'offsite', {{ $manday->id }})"
                                                                       style="display:none; width: 60px; margin:auto;"
                                                                       type="text"
                                                                       class="form-control"
                                                                       value="{{ $manday->offsite }}">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach

                                            @else

                                                @foreach($aj_stages_manday as $manday)
                                                    @if($manday->audit_type == $audit_type)
                                                        <tr>
                                                            <td>{{ $manday->getAuditType() }}</td>
                                                            <td>
                                                                <span id="onsite{{ $loop->iteration }}">{{ $manday->onsite }}</span>
                                                                <input id="input-onsite-{{ $loop->iteration }}"
                                                                       name="onsite"
                                                                       onkeypress="doneEdit('onsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                       onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'onsite', {{ $manday->id }})"
                                                                       style="display:none; width: 60px; margin:auto;"
                                                                       type="text"
                                                                       class="form-control"
                                                                       value="{{ $manday->onsite }}">
                                                            </td>

                                                            <td>
                                                                <span id="offsite{{ $loop->iteration }}">{{ $manday->offsite }}</span>
                                                                <input id="input-offsite-{{ $loop->iteration }}"
                                                                       name="offsite"
                                                                       onkeypress="doneEdit('offsite',{{ $loop->iteration }}, {{ $manday->id }})"
                                                                       onblur="hideInput({{ $loop->iteration }}, {{ $manday->id }}, 'offsite', {{ $manday->id }})"
                                                                       style="display:none; width: 60px; margin:auto;"
                                                                       type="text"
                                                                       class="form-control"
                                                                       value="{{ $manday->offsite }}">
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6 floting">
                                    <div class="form-group">
                                        <label>Remarks / Justification for reduction in man-days</label>
                                        <textarea rows="3" class="form-control" readonly
                                                  name="manday_remarks">{{ $aj_standard_stage->manday_remarks != null ?  $aj_standard_stage->manday_remarks : '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                    </div>
                    <div class="card card-primary mrgn">
                        <div class="card-header cardNewHeader">
                            <h3 class="card-title">
                                <div class="col-md-6 floting">AUDIT TEAM SELECTION</div>
                            </h3>
                        </div>
                        <div class="card-body card_cutom">
                            <div class="row">
                                <div class="callout callout-warning custom_callout disabledDiv">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Audit Type</label>
                                                <p class="results_answer">{{ str_replace('_',' ',$audit_type) }}</p>
                                            </div>
                                        </div>
                                        {{--                                        <div class="col-md-4">--}}
                                        {{--                                            <div class="form-group">--}}
                                        {{--                                                <label>Certification Status</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    @if($combine_tracker == 0)
                                        {{--                                        <div class="row">--}}
                                        {{--                                            <div class="col-md-4">--}}
                                        {{--                                                <div class="form-group">--}}
                                        {{--                                                    <label>Audit Type</label>--}}
                                        {{--                                                    <p class="results_answer"><strong>{{$audit_type}}</strong></p>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Is Supervising Lead Auditor Involved?</label>
                                                <div class="form-group">
                                                    <div class="col-md-4 floting no_padding">
                                                        <div>
                                                            <input type="radio" name="supervisor_involved" value="yes"
                                                                   id="supervisor_involved"
                                                                   onchange="hideItems(this, 'grade_check')"
                                                                    {{$aj_standard_stage->supervisor_involved === 'yes' ? 'checked' : ''}}/>
                                                        </div>
                                                        <label class="job_status">Yes</label>
                                                    </div>
                                                    <div class="col-md-4 floting">
                                                        <div>
                                                            <input type="radio" name="supervisor_involved" value="no"
                                                                   id="supervisor_involved"
                                                                   onchange="hideItems(this, 'grade_check')"
                                                                    {{$aj_standard_stage->supervisor_involved === 'no' ? 'checked' : ''}}/>
                                                        </div>
                                                        <label class="job_status">No</label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Due Date</label>

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                              <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                              </span>
                                                        </div>
                                                        @if($audit_type === 'surveillance_1' || $audit_type === 'surveillance_2' || $audit_type === 'surveillance_3' || $audit_type === 'surveillance_4' || $audit_type === 'surveillance_5' || $audit_type === 'reaudit' )

                                                            <input type="text" name="expected_date[]"
                                                                   onclick="return false;"
                                                                   id="expected_date" readonly
                                                                   value="{{ !is_null($aj_standard_stage->excepted_date) ? date('d-m-Y',strtotime($aj_standard_stage->excepted_date)) : date('d-m-Y')}}"
                                                                   class="form-control float-right active expected_date disabledDiv">
                                                        @else
                                                            <input type="text" name="expected_date[]" required
                                                                   id="expected_date"
                                                                   value="{{ !is_null($aj_standard_stage->excepted_date) ? date('d-m-Y',strtotime($aj_standard_stage->excepted_date)) : date('d-m-Y')}}"
                                                                   class="form-control float-right active expected_date">
                                                        @endif
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>

                                            @php

                                                if($audit_type === 'surveillance_1' && is_null($aj_standard_stage->recycle)){
                                                    $standard_stage = \App\AJStandardStage::where('aj_standard_id',$aj_standard_stage->aj_standard_id)->where('audit_type','stage_2')->first(['id']);

                                                   if(!is_null($standard_stage->postAudit) && !is_null($standard_stage->postAudit->postAuditSchemeInfo) && !is_null($standard_stage->postAudit->postAuditSchemeInfo->approval_date)){
                                                        $approvalDate = date('d-m-Y',strtotime($standard_stage->postAudit->postAuditSchemeInfo->approval_date));

                                                    }else{
                                                        $approvalDate = null;
                                                    }
                                                    if(!is_null($approvalDate)){
                                                        $approvalDate = date('Y-m-d', strtotime("+1 year", strtotime($approvalDate)));
                                                    }
                                                }
                                            @endphp
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Expected Start Date</label>

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                              <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                              </span>
                                                        </div>
                                                        <input type="text" name="actual_expected_date"
                                                               id="actual_expected_date"
                                                               value="{{ !is_null($aj_standard_stage->actual_expected_date) ? date('d-m-Y',strtotime($aj_standard_stage->actual_expected_date)) : date('d-m-Y')}}"
                                                               class="form-control float-right active actual_expected_date">


                                                        @if($audit_type === 'surveillance_1' && is_null($aj_standard_stage->recycle) && !is_null($aj_standard_stage->actual_expected_date) && !is_null($approvalDate) && strtotime($aj_standard_stage->actual_expected_date) > strtotime($approvalDate))
                                                            <span style="color: red">Date Over Due</span>
                                                        @endif
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Proposed Team</label>
                                                <table class="table table-bordered proposed_team_tbl">
                                                    <tbody id="team-view-{{ str_replace('_', '', $audit_type) }}">

                                                    @foreach($team_members as $key=>$team)

                                                        <tr id="tr-<?= $team["auditor_id"] ?>">
                                                            <td> <?= $team["member_name"] . " ( " . $team["member_type"] . " )"?>

                                                                <input type="hidden" name="perposedTeam[]"
                                                                       data-garde_id="<?= $team["grade_id"] ?>"
                                                                       value="<?= $team["auditor_id"] ?>">
                                                                <input type="hidden" name="perposed_team_grade_id[]"
                                                                       value="<?= $team["grade_id"] ?>">
                                                                <input type="hidden" name="perposed_team_name[]"
                                                                       value="<?= $team["member_name"] ?>">
                                                                <input type="hidden" name="perposed_team_type[]"
                                                                       value="<?= $team["member_type"] ?>">
                                                                <input type="hidden" name="perposedTeamData"
                                                                       data-team=" <?= $team["member_name"] . " ( " . $team["member_type"] . " )"?>"
                                                                       data-grade_id="<?= $team["grade_id"] ?>"
                                                                       value="<?= $team["auditor_id"] ?>">
                                                                {{--                                                                <span class="remove_perpose_team">--}}
                                                                {{--                                                                    <a href="#" style="color:red">X</a>--}}
                                                                {{--                                                                </span>--}}
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    @if($audit_type === 'surveillance_1' || $audit_type === 'surveillance_2' || $audit_type === 'surveillance_3' || $audit_type === 'surveillance_4' || $audit_type === 'surveillance_5' || $audit_type === 'reaudit' )
                                        {{--                                        {{ dd($aj_standard_change) }}--}}
                                        @if($aj_standard_change->count() >0 || !empty($aj_standard_change))
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="font-weight: 600 !important;color: red;">Change Confirmation Checklist:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q1: Any change in Name</label>
                                                <p class="results_answer">{{ $aj_standard_change->name }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q2: Any change in Scope</label>
                                                <p class="results_answer">{{ $aj_standard_change->scope }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q3: Any change in Address / Number of locations</label>
                                                <p class="results_answer">{{ $aj_standard_change->location }}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Q4: Any change in the version of Standard.</label>
                                                <p class="results_answer">{{ $aj_standard_change->version }}</p>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endif
                                    @endif
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-4"></div>
                                @if(auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'operation_manager')
                                    @if(!empty($notifications) && count($notifications) > 0)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Comments Box</label>
                                                <div class="card-body chat_box" style="display: block;">
                                                    <div class="direct-chat-messages">


                                                        @foreach($notifications as $key=>$notification)

                                                            @if($notification->sent_by === Auth::user()->id )
                                                                <div class="direct-chat-msg">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">{{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">{{ $notification->created_at }}</span>
                                                                </div>

                                                            @else
                                                                <div class="direct-chat-msg text-right">
                                                                    <div class="direct-chat-info clearfix">
                                                                        <span class="direct-chat-name">{{$notification->sentByUser->username}} ({{ucfirst($notification->status)}}):</span>
                                                                    </div>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->body }}</span><br>
                                                                    <span class="direct-chat-timestamp">  {{ $notification->created_at }}</span>
                                                                </div>
                                                            @endif
                                                        @endforeach


                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                <div class="col-md-4"></div>


                            </div>
                            @if(!empty($aj_standard_change))
                                {{--                                @if($aj_standard_change->version === 'yes' )--}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Change Details:</label>
                                            <textarea name="remarks" rows="4" class="form-control"
                                                      placeholder="Remarks"
                                                      readonly>{{ $aj_standard_change->remarks ? $aj_standard_change->remarks :''}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                {{--                                @endif--}}
                            @endif


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">

                                        {{--                                        --}}{{-- <p class="results_answer"></p> --}}
                                        {{--                                        @if(auth()->user()->user_type === 'scheme_manager')--}}
                                        {{--                                            <label>Job Number</label>--}}
                                        {{--                                            <input type="text" name="job_number" readonly class="form-control"--}}
                                        {{--                                                   value="{{ $aj_standard_stage->job_number ? $aj_standard_stage->job_number : '---' }}">--}}
                                        {{--                                        @else--}}
                                        {{--                                            @if($aj_standard_stage->status == 'approved')--}}
                                        {{--                                                <label>Job Number</label>--}}
                                        {{--                                                <input type="hidden" name="job_number" value="{{$job_number ?? ''}}">--}}
                                        {{--                                                <input type="text" name="job_number_new" readonly class="form-control"--}}
                                        {{--                                                       value="{{ $aj_standard_stage->job_number ? $aj_standard_stage->job_number : '---' }}">--}}
                                        {{--                                            @endif--}}
                                        {{--                                        @endif--}}
                                        <label>Job Number</label>
                                        @if(auth()->user()->user_type === 'scheme_manager')
                                            <input type="text" name="job_number" class="form-control"
                                                   value="{{ $aj_standard_stage->job_number }}" readonly>
                                        @else

                                            <input type="text" name="job_number" class="form-control"
                                                   value="{{ $aj_standard_stage->job_number }}" readonly>
                                        @endif


                                    </div>
                                </div>
                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type === 'admin')
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Evaluator Name</label>
                                            <input type="text" name="evaluator_name" class="form-control" disabled
                                                   value="{{ $aj_standard_stage->evaluator_name !=null ? $aj_standard_stage->evaluator_name : auth()->user()->fullName()}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Approval Date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                  </span>
                                                </div>
                                                <input type="text" name="approval_date" id="approval_date"
                                                       value="{{ !is_null($aj_standard_stage->approval_date) ? date('d-m-Y',strtotime($aj_standard_stage->approval_date)) : date('d-m-Y')}}"
                                                       class="form-control float-right active approval_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8"></div>
                                @endif
                                @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type === 'admin')
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Valid Till Date</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                              <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                              </span>
                                                </div>

                                                <input type="text" name="valid_till_date"
                                                       id="valid_till_date"
                                                       value="{{ !is_null($aj_standard_stage->valid_till_date) ? date('d-m-Y',strtotime($aj_standard_stage->valid_till_date)) : ''}}"
                                                       class="form-control float-right active valid_till_date">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Valid Till Date</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                              <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                              </span>
                                                </div>

                                                <input type="text" name="valid_till_date"
                                                       id="valid_till_date" readonly
                                                       value="{{ !is_null($aj_standard_stage->valid_till_date) ? date('d-m-Y',strtotime($aj_standard_stage->valid_till_date)) : ''}}"
                                                       class="form-control float-right active valid_till_date">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-8"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Last Approval Date</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fa fa-calendar"></i>
                          </span>
                                            </div>

                                            <input type="text" name="last_approval_date"
                                                   id="last_approval_date" readonly
                                                   value="{{ !is_null($aj_standard_stage->last_approval_date) ? date('d-m-Y',strtotime($aj_standard_stage->last_approval_date)) : ''}}"
                                                   class="form-control float-right active last_approval_date">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="viewAllAJ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">
            <form action="#" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Other Locations</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12 cont_row mrgn">
                            <div class="col-md-12">
                                <div class="form-group">
                                    @php $i=2; @endphp
                                    @foreach($company->childCompanies as $child)
                                        <label><strong>Address {{$i}}</strong></label>
                                        <p>{{ $child->address }},{{ $child->city->name }}
                                            ,{{$company->country->name}}</p>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade viewModal2 show" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-eye"></i>VIEW HISTORY</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h3>Man Days History</h3>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div id="example1_filter" class="dataTables_filter">
                                    <form action="#" method="post">
                                        <label>
                                            <input type="text" class="form-control form-control-sm" id="myFilter"
                                                   placeholder="Search" aria-controls="example1">
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable no-footer"
                                       role="grid" aria-describedby="example1_info">
                                    <thead>
                                    <tr role="row green_row">
                                        <th width="10%;">AJ Creation Date</th>
                                        <th width="15%;">Audit Type</th>
                                        <th>Onsite</th>
                                        <th>Offsite</th>
                                        <th width="12%;">Acutal Audit Date</th>
                                        <th>Remarks / Justification For Reduction</th>
                                    </tr>
                                    </thead>
                                    <tbody id="myTable">


                                    @if(auth()->user()->user_type === 'scheme_manager' || auth()->user()->user_type === 'admin')
                                        @foreach($aj_stages_manday as $stages_manday)
                                            @foreach($stages_manday as $manday)

                                                <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                    <td>{{$manday->created_at->format('d-m-Y')}}</td>
                                                    <td>{{ $manday->getAuditType() }}</td>
                                                    <td>{{$manday->onsite}}</td>
                                                    <td>{{$manday->offsite}}</td>
                                                    <td>{{$manday->updated_at->format('d-m-Y')}}</td>
                                                    <td>{{$manday->manday_remarks}}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        <tr class='notfound' style="display:none">
                                            <td colspan='6'>No record found</td>
                                        </tr>
                                    @else
                                        @foreach($aj_stages_manday as $manday)

                                            <tr role="row" class="{{$loop->iteration%2 === 0 ? 'even':'odd'}}">
                                                <td>{{$manday->created_at->format('d-m-Y')}}</td>
                                                <td>{{ $manday->getAuditType() }}</td>
                                                <td>{{$manday->onsite}}</td>
                                                <td>{{$manday->offsite}}</td>
                                                <td>{{$manday->updated_at->format('d-m-Y')}}</td>
                                                <td>{{$manday->manday_remarks}}</td>
                                            </tr>
                                        @endforeach
                                        <tr class='notfound' style="display:none">
                                            <td colspan='6'>No record found</td>
                                        </tr>
                                    @endif


                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@push('styles')
    <style>
        .team-member-delete-button {
            border: none;
            background: none;
            float: right;
        }

        .my-active span {
            background-color: #5cb85c !important;
            color: white !important;
            border-color: #5cb85c !important;
        }

        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

    </style>
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $(document).ready(function () {


            function unique(list) {
                var result = [];
                $.each(list, function (i, e) {
                    if ($.inArray(e, result) == -1) result.push(e);
                });
                return result;
            }

            function removeDuplicates(id) {
                var records = $(id).text();
                records = records.replace(/\s/g, '');
                let records_array = records.split(',');
                var final_records = unique(records_array);
                $(id).text(final_records.toString());
                return true;
            }

            var iaf_codes = removeDuplicates('#iaf-codes');
            var ias_codes = removeDuplicates('#ias-codes');
            var accreditations_codes = removeDuplicates('#codes-accreditations');
            var food_accreditations = removeDuplicates('#food-accreditations');
            var energy_accreditations = removeDuplicates('#energy-accreditations');
            var food_category_codes = removeDuplicates('#food-category-codes');
            var food_subcategory_codes = removeDuplicates('#food-subcategory-codes');
            var energy_codes = removeDuplicates('#energy-codes');


        });
        $('.expected_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.approvaldate').datepicker({
            format: 'dd-mm-yyyy',
        });

        // Case-insensitive searching (Note - remove the below script for Case sensitive search )
        $.expr[":"].contains = $.expr.createPseudo(function (arg) {
            return function (elem) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });

        $("#myFilter").on("keyup", function () {
            // Search Text
            var search = $(this).val();

            // Hide all table tbody rows
            $('#myTable tr').hide();

            // Count total search result
            var len = $('#myTable tr:not(.notfound) td:contains("' + search + '")').length;

            if (len > 0) {
                // Searching text in columns and show match row
                $('#myTable tr:not(.notfound) td:contains("' + search + '")').filter(function () {

                    $(this).closest('tr').show();
                });
            } else {
                $('.notfound').show();
            }
        });
    </script>
@endpush
