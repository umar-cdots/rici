<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyStandardsAccreditationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->dropColumn('accreditation_ids');
        });
        Schema::create('company_standards_accreditations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_standard_id');
            $table->integer('accreditation_id');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_standards_accreditations');
        Schema::table('company_standards', function (Blueprint $table) {
            $table->string('accreditation_ids');
        });
    }
}
