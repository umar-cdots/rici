<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OldCbColumnsCompanyStandards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->integer('old_cb_last_audit_report_doc_media_id')->nullable()->after('old_cb_audit_activity_stage_id');
            $table->integer('old_cb_certificate_copy_doc_media_id')->nullable()->after('old_cb_last_audit_report_doc_media_id');
            $table->integer('old_cb_others_doc_media_id')->nullable()->after('old_cb_certificate_copy_doc_media_id');
            $table->text('old_cb_audit_activity_dates')->nullable()->after('old_cb_others_doc_media_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            //
        });
    }
}
