<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aj_stage_team', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aj_standard_stage_id')->unsigned();
            $table->boolean('supervisor_involved')->default(0);
            $table->string('member_type')->nullable();
            $table->string('member_name')->nullable();
            $table->date('expected_date')->nullable();
            $table->foreign('aj_standard_stage_id')->on('aj_standard_stages')->references('id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_teams');
    }
}
