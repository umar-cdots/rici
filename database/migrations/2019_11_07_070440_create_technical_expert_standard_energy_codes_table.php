<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalExpertStandardEnergyCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_expert_standard_energy_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_standard_id');
            $table->integer('energy_code');
            $table->string('accreditation_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_expert_standard_energy_codes');
    }
}
