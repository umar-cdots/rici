<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIafCodeStatusToIafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('i_a_fs', function (Blueprint $table) {
            $table->boolean('iaf_code_status')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('i_a_fs', function (Blueprint $table) {
            $table->dropColumn('iaf_code_status');
        });
    }
}
