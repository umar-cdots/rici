<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_manager', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('full_name', 100)->nullable();
            $table->date('joining_date')->nullable();
            $table->integer('standard_id')->unsigned();
            $table->string('landline')->nullable();
            $table->string('signature_image')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('additional_files')->nullable();
            $table->foreign('user_id')->on('users')->references('id');
            $table->foreign('standard_id')->on('standards')->references('id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme_manager');
    }
}
