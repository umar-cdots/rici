<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuditorIdToAuditorStandardWitnessEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditor_standard_witness_evaluations', function (Blueprint $table) {
            $table->unsignedInteger('auditor_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditor_standard_witness_evaluations', function (Blueprint $table) {
            $table->dropColumn('auditor_id');

        });
    }
}
