<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyStandardAccreditationColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->dropColumn('accreditation_id');
            $table->string('accreditation_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->dropColumn('accreditation_ids');
            $table->integer('accreditation_id');
        });
    }
}
