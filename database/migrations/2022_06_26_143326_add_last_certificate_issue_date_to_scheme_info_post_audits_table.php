<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastCertificateIssueDateToSchemeInfoPostAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scheme_info_post_audits', function (Blueprint $table) {

            $table->date('last_certificate_issue_date')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheme_info_post_audits', function (Blueprint $table) {
            $table->date('last_certificate_issue_date')->nullable();
        });
    }
}
