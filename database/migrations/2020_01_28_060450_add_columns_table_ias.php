<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableIas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('i_a_s', function (Blueprint $table) {
            $table->unsignedInteger('standards_family_id')->nullable();
            $table->unsignedInteger('standard_id')->nullable();
            $table->unsignedInteger('accreditation_id')->nullable();
            $table->unsignedInteger('ias_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('i_a_s', function (Blueprint $table) {
            $table->dropColumn('standards_family_id');
            $table->dropColumn('standard_id');
            $table->dropColumn('accreditation_id');
            $table->dropColumn('ias_id');
        });
    }
}
