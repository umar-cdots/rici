<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewUserColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['name']);
            $table->string('first_name')->after('id');
            $table->string('middle_name')->after('first_name');
            $table->string('last_name')->after('middle_name');
            $table->string('username')->after('last_name');
            $table->date('dob')->after('username');
            $table->integer('working_experience')->after('dob');
            $table->string('main_education')->after('working_experience');
            $table->integer('nationality_id')->after('main_education');
            $table->integer('region_id')->after('nationality_id')->nullable();
            $table->integer('country_id')->after('region_id')->nullable();
            $table->integer('state_id')->after('country_id')->nullable();
            $table->integer('city_id')->after('state_id')->nullable();
            $table->text('address')->after('city_id')->nullable();
            $table->string('phone_number')->after('address')->nullable();
            $table->enum('job_status', ['full_time', 'part_time', 'none'])->after('address')->default('none');
            $table->text('remarks')->after('job_status')->nullable();
            $table->enum('user_type', ['admin', 'scheme_manager', 'scheme_coordinator', 'operation_manager', 'operation_coordinator', 'auditor'])->after('remember_token');
            $table->enum('status', ['active', 'applied', 'close'])->after('user_type');
            $table->softDeletes()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->dropColumn(['first_name', 'middle_name', 'last_name', 'username', 'dob', 'working_experience', 'main_education', 'nationality_id', 'user_defined_region_id', 'country_id', 'region_id', 'city_id', 'address', 'phone_number', 'job_status', 'remarks', 'user_type', 'status']);
        });
    }
}
