<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnergyFamilySheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('energy_family_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('energy_code_id')->unsigned();
            $table->string('complexity')->nullable();
            $table->string('stages_manday')->nullable();
            $table->string('surveillance')->nullable();
            $table->string('recertificate')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->foreign('energy_code_id')->on('energy_codes')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('energy_family_sheets');
    }
}
