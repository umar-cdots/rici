<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionalOperationManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regional_operation_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id');
            $table->integer('user_id');
            $table->integer('office_country_id');
            $table->integer('office_state_id');
            $table->integer('office_city_id');
            $table->text('office_address');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regional_operation_managers');
    }
}
