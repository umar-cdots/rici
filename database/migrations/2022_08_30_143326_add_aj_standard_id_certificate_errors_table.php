<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAjStandardIdCertificateErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('i_a_s_certificate_errors', function (Blueprint $table) {
            $table->unsignedBigInteger('company_standard_id')->nullable();
            $table->unsignedBigInteger('aj_standard_id')->nullable();
            $table->unsignedBigInteger('aj_standard_stage_id')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('i_a_s_certificate_errors', function (Blueprint $table) {
            $table->dropColumn('company_standard_id');
            $table->dropColumn('aj_standard_id');
            $table->dropColumn('aj_standard_stage_id');
        });
    }
}
