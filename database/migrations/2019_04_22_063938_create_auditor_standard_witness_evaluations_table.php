<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorStandardWitnessEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_standard_witness_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_standard_id');
            $table->date('due_date')->nullable();
            $table->date('actual_date')->nullable();
            $table->enum('satisfactory', ['yes', 'no','not applicable'])->default('not applicable');
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_standard_witness_evaluations');
    }
}
