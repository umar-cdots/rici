<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeyChangeAndKeyRemarksToPostAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_audits', function (Blueprint $table) {
            $table->boolean('key_change')->default(false);
            $table->longText('key_remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_audits', function (Blueprint $table) {
            $table->dropColumn('key_change');
            $table->dropColumn('key_remarks');
        });
    }
}
