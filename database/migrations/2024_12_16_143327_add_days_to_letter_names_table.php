<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDaysToLetterNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letter_names', function (Blueprint $table) {
            $table->text('pk_days')->nullable();
            $table->text('ksa_days')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letter_names', function (Blueprint $table) {
            $table->dropColumn('pk_days');
            $table->dropColumn('ksa_days');
        });
    }
}
