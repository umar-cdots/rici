<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAjDateToAjStandardStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->timestamp('aj_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->dropColumn('aj_date');
        });
    }
}
