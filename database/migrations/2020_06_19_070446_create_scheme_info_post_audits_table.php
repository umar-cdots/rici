<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeInfoPostAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_info_post_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_audit_id')->unsigned();
            $table->integer('current_certificate_status_id')->unsigned();
            $table->enum('pack_closed', ['YES', 'NO'])->nullable();
            $table->enum('print_required', ['YES', 'NO'])->nullable();
            $table->enum('certificate_validity', ['1 Year', '3 Years'])->nullable();
            $table->enum('print_assigned_to', ['scheme_manager', 'scheme_coordinator'])->nullable();
            $table->date('approval_date')->nullable();
            $table->date('new_expiry_date')->nullable();
            $table->date('original_issue_date')->nullable();
            $table->text('uci_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme_info_post_audits');
    }
}
