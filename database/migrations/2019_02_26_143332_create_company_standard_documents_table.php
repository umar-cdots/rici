<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyStandardDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_standard_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_standard_id');
            $table->integer('media_id');
            $table->date('date')->nullable();
            $table->text('note')->nullable();
            $table->enum('document_type', ['initial_inquiry_form', 'proposal', 'contract', 'other'])->default('other');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_standard_documents');
    }
}
