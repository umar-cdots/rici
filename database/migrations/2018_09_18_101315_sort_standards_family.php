<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SortStandardsFamily extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('standards_families', function (Blueprint $table) {
            $table->integer('sort')->default(0)->after('is_ims_enabled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('standards_families', function (Blueprint $table) {
            $table->dropColumn(['sort']);
        });
    }
}
