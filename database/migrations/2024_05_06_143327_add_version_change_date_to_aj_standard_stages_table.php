<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVersionChangeDateToAjStandardStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->text('version_change_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->dropColumn('version_change_date');
        });
    }
}
