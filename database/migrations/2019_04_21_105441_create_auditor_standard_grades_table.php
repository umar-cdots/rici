<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorStandardGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_standard_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_standard_id');
            $table->integer('grade_id');
            $table->enum('status', ['Not Applicable','current', 'active', 'suspended', 'withdrawn'])->default('Not Applicable');
            $table->string('approval_date')->nullable();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_standard_grades');
    }
}
