<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueCodeCompanyStandardEnergyCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standard_energy_codes', function (Blueprint $table) {
            $table->text('unique_code')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_standard_energy_codes', function (Blueprint $table) {
            $table->dropColumn('unique_code');

        });
    }
}
