<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorEmploymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_employment_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_id');
            $table->string('company_name');
            $table->string('position_held');
            $table->integer('employed_from');
            $table->integer('employed_to');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_employment_histories');
    }
}
