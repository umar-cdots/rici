<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableIaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('i_a_fs', function (Blueprint $table) {
            $table->unsignedInteger('standards_family_id')->nullable();
            $table->unsignedInteger('standard_id')->nullable();
            $table->unsignedInteger('accreditation_id')->nullable();
            $table->unsignedInteger('iaf_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('i_a_fs', function (Blueprint $table) {
            $table->dropColumn('standards_family_id');
            $table->dropColumn('standard_id');
            $table->dropColumn('accreditation_id');
            $table->dropColumn('iaf_id');
        });
    }
}
