<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostAuditQueestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_audit_queestions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_audit_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->enum('value', ['YES', 'NO', 'N/A'])->default('NO');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_audit_queestions');
    }
}
