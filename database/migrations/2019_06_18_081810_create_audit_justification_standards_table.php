<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditJustificationStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aj_standards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('audit_justification_id')->unsigned();
            $table->integer('standard_id')->unsigned();
            $table->foreign('audit_justification_id')->on('audit_justifications')->references('id');
            $table->foreign('standard_id')->on('standards')->references('id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_justification_standards');
    }
}
