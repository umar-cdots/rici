<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditJustificationMandayDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aj_standard_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aj_standard_id')->unsigned();
            $table->enum('audit_type', ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'reaudit','stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'reaudit_verification','stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'reaudit_scope_extension','stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'reaudit_special_transition']);
            $table->float('onsite')->nullable();
            $table->float('offsite')->nullable();
            $table->text('certificate_scope')->nullable();
            $table->text('manday_remarks')->nullable();
            $table->date('excepted_date')->nullable();
            $table->boolean('supervisor_involved')->default(0);
            $table->string('job_number')->nullable();
            $table->string('evaluator_name')->nullable();
            $table->date('approval_date')->nullable();
            $table->foreign('aj_standard_id')->on('aj_standards')->references('id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_justification_manday_details');
    }
}
