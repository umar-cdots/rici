<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgreementCerticateToAuditorConfidentialityAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditor_confidentiality_agreements', function (Blueprint $table) {
            $table->string('agreement_document')->nullable()->after('date_signed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditor_confidentiality_agreements', function (Blueprint $table) {
            $table->dropColumn('agreement_document');
        });
    }
}
