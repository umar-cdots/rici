<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecialAjRemarksToAjStandardStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->string('special_aj_remarks', 500)->nullable()->after('audit_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->dropColumn('transfer_data');

        });
    }
}
