<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalExpertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_expert', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('landline')->nullable();
            $table->text('postal_address')->nullable();
            $table->integer('region_id');
            $table->integer('country_id');
            $table->integer('city_id');
            $table->string('dob')->nullable();
            /*$table->integer('language_id');*/
            $table->float('working_experience')->comment('in years');
            $table->string('main_education');
            $table->integer('nationality_id')->comment('country id');
            $table->enum('job_status', ['full_time', 'part_time']);
            $table->text('profile_remarks')->nullable();
            $table->tinyInteger('form_progress_step')->default(1);
            $table->integer('form_progress')->default(0);
            $table->string('form_current_progress')->nullable();
            $table->string('profile_pic')->nullable();
            $table->integer('user_id')->nullable();
            $table->enum('technical_expert_status', ['active', 'inactive'])->default('inactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_expert');
    }
}
