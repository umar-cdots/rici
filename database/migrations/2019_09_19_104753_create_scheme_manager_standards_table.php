<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeManagerStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_manager_standards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_manager_id')->unsigned();
            $table->integer('standard_id')->unsigned();
            $table->integer('standard_family_id')->unsigned();
//            $table->foreign('scheme_manager_id')->on('scheme_manager')->references('id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme_manager_standards');
    }
}
