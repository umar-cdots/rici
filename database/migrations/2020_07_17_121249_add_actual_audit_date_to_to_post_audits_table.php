<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualAuditDateToToPostAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_audits', function (Blueprint $table) {
            $table->date('actual_audit_date_to'); //dd/mm/yyyy
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_audits', function (Blueprint $table) {
            $table->dropColumn('actual_audit_date_to');
        });
    }
}
