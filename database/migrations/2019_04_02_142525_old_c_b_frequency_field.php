<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OldCBFrequencyField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->enum('old_cb_surveillance_frequency', ['biannually', 'annually'])->after('old_cb_certificate_expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->dropColumn('old_cb_surveillance_frequency');
        });
    }
}
