<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsExpiryStatusAndExpiryDateTableOutSourceRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('out_source_requests', function (Blueprint $table) {
            $table->unsignedInteger('requested_by')->nullable();
            $table->dateTime('approved_date')->nullable();
            $table->dateTime('expired_date')->nullable();
            $table->enum('expiry_status', ['not expired', 'expired'])->default('not expired');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('out_source_requests', function (Blueprint $table) {
            $table->dropColumn('requested_by');
            $table->dropColumn('approved_date');
            $table->dropColumn('expired_date');
            $table->dropColumn('expiry_status');
        });
    }
}
