<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('company_standard_id');
            $table->unsignedBigInteger('aj_standard_id');
            $table->unsignedBigInteger('aj_standard_stage_id');
            $table->text('invoice_number');
            $table->text('audit_type');
            $table->text('standard');
            $table->text('company_name');
            $table->text('contact_name');
            $table->text('designation');
            $table->text('phone_number');
            $table->text('email');
            $table->text('invoice_unit');
            $table->text('invoice_amount');
            $table->text('tax_authority')->nullable();
            $table->text('tax_percentage')->nullable();
            $table->text('inflation_percentage')->nullable();
            $table->date('invoice_date');
            $table->date('due_date');
            $table->boolean('letter_one_print')->default(false);
            $table->date('letter_one_date')->nullable();
            $table->boolean('letter_two_print')->default(false);
            $table->date('letter_two_date')->nullable();
            $table->boolean('letter_three_print')->default(false);
            $table->date('letter_three_date')->nullable();
            $table->boolean('invoice_status')->default(0);
            $table->date('second_last_email_date')->nullable();
            $table->date('last_email_date')->nullable();
            $table->text('attached_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
