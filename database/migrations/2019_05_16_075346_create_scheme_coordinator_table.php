<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeCoordinatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_coordinator', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('full_name')->nullable();
            $table->date('joining_date');
            $table->integer('standard_id')->unsigned();
            $table->string('landline')->nullable();
            $table->integer('scheme_manager_id')->unsigned();
            $table->string('signature_image')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('additional_files')->nullable();
            $table->foreign('user_id')->on('users')->references('id');
            $table->foreign('standard_id')->on('standards')->references('id');
            $table->foreign('scheme_manager_id')->on('scheme_manager')->references('id');
            $table->softdeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme_coordinator');
    }
}
