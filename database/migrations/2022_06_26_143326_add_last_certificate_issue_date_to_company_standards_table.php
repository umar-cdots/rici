<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastCertificateIssueDateToCompanyStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standards', function (Blueprint $table) {

            $table->date('last_certificate_issue_date')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->date('last_certificate_issue_date')->nullable();
        });
    }
}
