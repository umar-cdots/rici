<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToAjStandardStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->string('status')->default('Not Applied')->after('approval_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
