<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuditorStandardStatusColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditor_standards', function (Blueprint $table) {
            $table->enum('auditor_standard_status', ['unapproved', 'approved', 'rejected', 'resent'])->default('unapproved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditor_standards', function (Blueprint $table) {

            $table->dropColumn('auditor_standard_status');
            //
        });
    }
}
