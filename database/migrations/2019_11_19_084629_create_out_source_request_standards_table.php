<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutSourceRequestStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_source_request_standards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('outsource_request_id')->unsigned();
            $table->integer('standard_id')->unsigned();
            $table->enum('type', ['general', 'food', 'energy']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('out_source_request_standards');
    }
}
