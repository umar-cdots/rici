<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_standards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('standard_id');
            $table->enum('client_type', ['new', 'transfered'])->default('new');
            $table->string('old_cb_name')->nullable();
            $table->date('old_cb_certificate_issue_date')->nullable();
            $table->date('old_cb_certificate_expiry_date')->nullable();
            $table->integer('old_cb_audit_activity_stage_id')->nullable();
            $table->enum('proposed_complexity', ['low', 'medium', 'high'])->default('medium');
            $table->boolean('is_ims')->default(false);
            $table->integer('audit_activity_stage_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_standards');
    }
}
