<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWitnessDocumentToAuditorStandardWitnessEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditor_standard_witness_evaluations', function (Blueprint $table) {
            $table->string('witness_document')->nullable()->after('satisfactory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditor_standard_witness_evaluations', function (Blueprint $table) {
            $table->dropColumn('training_document');
        });
    }
}
