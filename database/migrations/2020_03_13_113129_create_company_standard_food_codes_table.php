<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyStandardFoodCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_standard_food_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('company_standard_id')->nullable();
            $table->integer('standard_id');
            $table->integer('food_category_id');
            $table->integer('food_sub_category_id');
            $table->string('accreditation_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_standard_food_codes');
    }
}
