<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->enum('title', ['Mr', 'Mrs', 'Ms','Dr'])->default('Mr');
            $table->string('name');
            $table->string('position')->nullable();
            $table->string('contact');
            $table->string('email')->nullable();
            $table->enum('type', ['primary', 'secondary'])->default('secondary');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_contacts');
    }
}
