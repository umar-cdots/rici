<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompaniesStep4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('intial_certification_currency', 3);
            $table->double('intial_certification_amount');
            $table->string('surveillance_currency', 3);
            $table->double('surveillance_amount');
            $table->string('re_audit_currency', 3);
            $table->double('re_audit_amount');
            $table->enum('surveillance_frequency', ['biannually', 'annually']);
            $table->integer('accreditation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumns(['intial_certification_currency', 'intial_certification_amount', 'surveillance_currency', 'surveillance_amount', 're_audit_currency', 're_audit_amount', 'surveillance_frequency', 'accreditation_id']);
        });
    }
}
