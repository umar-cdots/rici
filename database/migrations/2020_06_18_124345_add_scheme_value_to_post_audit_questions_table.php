<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchemeValueToPostAuditQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_audit_queestions', function (Blueprint $table) {
            $table->enum('scheme_value', ['YES', 'NO', 'N/A'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_audit_queestions', function (Blueprint $table) {
            $table->dropColumn('scheme_value');
        });
    }
}
