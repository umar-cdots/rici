<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyStandardsQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_standards_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_id');
            $table->text('question');
            $table->enum('field_type', ['radio', 'checkbox', 'number', 'small_text', 'text', 'date', 'datetime', 'textarea', 'dropdown', 'multi_dropdown', 'heading', 'special_dropdown', 'totally_dependent', 'special_dropdown_multiple', 'totally_dependent_multiple'])->default('text');
            $table->text('default_answer')->nullable();
            $table->text('options')->nullable();
            $table->integer('min_length')->nullable();
            $table->integer('max_length')->nullable();
            $table->boolean('is_required')->default(false);
            $table->boolean('has_dependent_fields')->default(false);
            $table->boolean('is_dependent_field')->default(false);
            $table->string('right_answer_for_dependent_fields')->nullable();
            $table->string('dependent_fields_ids')->nullable();
            $table->integer('sort');
            $table->enum('question_format', ['string', 'json'])->default('string');
            $table->integer('company_standards_question_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_standards_questions');
    }
}
