<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalExpertConfidentialityAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_expert_confidentiality_agreements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_id');
            $table->string('date_signed');
            $table->string('agreement_document')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_expert_confidentiality_agreements');
    }
}
