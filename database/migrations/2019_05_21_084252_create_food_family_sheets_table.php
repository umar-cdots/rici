<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodFamilySheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_family_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_id')->unsigned();
            $table->integer('food_category_id')->unsigned();
            $table->string('TD')->nullable();
            $table->string('TH')->nullable();
            $table->string('TMS')->nullable();
            $table->string('TFTE_from')->nullable();
            $table->string('TFTE_to')->nullable();
            $table->string('TFTE_value')->nullable();
            $table->foreign('food_category_id')->on('food_categories')->references('id');
            $table->foreign('standard_id')->on('standards')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_family_sheets');
    }
}
