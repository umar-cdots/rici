<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjStageChangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aj_stage_change', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aj_standard_stage_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('scope')->nullable();
            $table->string('location')->nullable();
            $table->string('version')->nullable();
            $table->integer('standards')->nullable();
            $table->string('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aj_stage_change');
    }
}
