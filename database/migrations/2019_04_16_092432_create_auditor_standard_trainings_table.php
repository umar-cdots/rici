<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorStandardTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_standard_trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_standard_id');
            $table->integer('training_year');
            $table->string('certificate_name');
            $table->string('certificate_number');
            $table->string('awarded_body');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_standard_trainings');
    }
}
