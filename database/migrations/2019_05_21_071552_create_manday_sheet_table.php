<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandaySheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manday_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_id')->unsigned();
            $table->string('standard_number')->nullable();
            $table->float('effective_employes_on')->nullable();
            $table->float('effective_employes_off')->nullable();
            $table->string('stage1_on')->nullable();
            $table->string('stage1_off')->nullable();
            $table->string('stage2_on')->nullable();
            $table->string('stage2_off')->nullable();
            $table->string('surveillance_on')->nullable();
            $table->string('surveillance_off')->nullable();
            $table->string('reaudit_on')->nullable();
            $table->string('reaudit_off')->nullable();
            $table->string('complexity')->nullable();
            $table->string('frequency')->nullable();
            $table->foreign('standard_id')->on('standards')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manday_sheet');
    }
}
