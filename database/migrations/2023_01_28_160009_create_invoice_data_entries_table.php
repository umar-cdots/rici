<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDataEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_data_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->text('tax_authorities')->nullable();
            $table->text('tax_percentage')->nullable();
            $table->text('inflation_percentage')->nullable();
            $table->text('invoice_remarks_one')->nullable();
            $table->text('invoice_remarks_two')->nullable();
            $table->text('other_user_name')->nullable();
            $table->text('other_user_designation')->nullable();
            $table->text('other_user_email')->nullable();
            $table->text('other_user_phone_number')->nullable();
            $table->text('other_details_name')->nullable();
            $table->text('other_details_address')->nullable();
            $table->text('other_details_email')->nullable();
            $table->text('other_details_phone_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_data_entries');
    }
}
