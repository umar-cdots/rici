<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFilePostAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_audits', function (Blueprint $table) {

            $table->text('is_q4')->nullable();
            $table->text('q4_file')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_audits', function (Blueprint $table) {
            $table->dropColumn('is_q4');
            $table->dropColumn('q4_file');
        });
    }
}
