<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('region_id');
            $table->integer('country_id');
            $table->integer('city_id');
            $table->text('address');
            $table->text('key_process');
            $table->integer('total_employees');
            $table->integer('permanent_employees');
            $table->integer('part_time_employees');
            $table->integer('repeatative_employees');
            $table->enum('shift', ['1', '2', '3']);
            $table->string('sister_company_name')->nullable();
            $table->text('general_remarks')->nullable();
            $table->string('website')->nullable();
            $table->boolean('is_parent_company')->default(true);
            $table->integer('company_id')->nullable();
            $table->float('company_form_progress')->nullable();
            $table->enum('company_form_status', ['processing', 'completed']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
