<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('type_id')->nullable();
            $table->integer('request_id')->nullable();
            $table->string('status')->nullable();
            $table->string('sent_type')->nullable();
            $table->string('sent_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropColumn('type_id');
            $table->dropColumn('request_id');
            $table->dropColumn('status');
            $table->dropColumn('sent_type');
            $table->dropColumn('sent_message');
        });
    }
}
