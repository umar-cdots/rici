<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewVersionChangeToAjStandardStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->unsignedBigInteger('old_aj_standard_stage_id')->nullable();
            $table->unsignedBigInteger('old_aj_standard_id')->nullable();
            $table->boolean('new_version_change')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aj_standard_stages', function (Blueprint $table) {
            $table->dropColumn('old_aj_standard_stage_id');
            $table->dropColumn('old_aj_standard_id');
            $table->dropColumn('new_version_change');
        });
    }
}
