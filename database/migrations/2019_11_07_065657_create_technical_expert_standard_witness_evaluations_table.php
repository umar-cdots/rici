<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalExpertStandardWitnessEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_expert_standard_witness_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_standard_id');
            $table->string('due_date');
            $table->string('actual_date');
            $table->enum('satisfactory', ['yes', 'no'])->default('yes');
            $table->string('witness_document')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_expert_standard_witness_evaluations');
    }
}
