<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('standard_id')->unsigned();
            $table->integer('aj_standard_stage_id')->unsigned();
            $table->date('actual_audit_date'); //dd/mm/yyyy
            $table->integer('receiver_id')->unsigned(); //sc or sm
            $table->integer('sender_id')->unsigned();  //oc or om
            $table->enum('status', ['unapproved', 'rejected', 'approved', 'resent', 'save'])->default('unapproved');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_audits');
    }
}
