<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutSourceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('out_source_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('operation_manager_id')->unsigned()->nullable();
            $table->integer('operation_coordinator_id')->unsigned()->nullable();
            $table->integer('operation_approval_manager_id')->unsigned()->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->enum('status', ['approved', 'reject', 'request']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('out_source_requests');
    }
}
