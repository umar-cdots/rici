<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalExpertEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_expert_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_id');
            $table->integer('year_passing');
            $table->string('institution');
            $table->string('degree_diploma');
            $table->string('major_subjects');
            $table->string('education_document')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_expert_educations');
    }
}
