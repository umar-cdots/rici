<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorStandardFoodCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_standard_food_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditor_standard_id');
            $table->integer('food_category_code');
            $table->integer('food_sub_category_code');
            $table->string('accreditation_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_standard_food_codes');
    }
}
