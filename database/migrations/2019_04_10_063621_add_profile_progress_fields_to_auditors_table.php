<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileProgressFieldsToAuditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditors', function (Blueprint $table) {
            $table->tinyInteger('form_progress_step')->after('profile_remarks')->default(1);
            $table->integer('form_progress')->after('profile_remarks')->default(0);
            $table->string('form_current_progress')->after('profile_remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditors', function (Blueprint $table) {
            $table->dropColumn(['form_progress_step', 'form_progress', 'form_current_progress']);
        });
    }
}
