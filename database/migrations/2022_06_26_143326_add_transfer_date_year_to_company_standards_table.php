<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferDateYearToCompanyStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_standards', function (Blueprint $table) {

            $table->text('transfer_year')->nullable();
            $table->boolean('is_transfer_standard')->default(false);
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_standards', function (Blueprint $table) {
            $table->dropColumn('is_transfer_standard');
            $table->dropColumn('transfer_year');
        });
    }
}
