<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardsNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Standards', function (Blueprint $table) {
            $table->boolean('auto_calculate_proposed_complexity')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Standards', function (Blueprint $table) {
            $table->dropColumn(['auto_calculate_proposed_complexity']);
        });
    }
}
