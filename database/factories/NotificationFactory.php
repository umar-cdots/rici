<?php

use Faker\Generator as Faker;

$factory->define(App\Notification::class, function (Faker $faker) {
    return [
        'type' => '',
        'sent_by' => $faker->numberBetween(1,5),
        'sent_to' => $faker->numberBetween(6,9),
        'icon' => $faker->text(10),
        'body' => $faker->text(60),
        'is_read' => $faker->boolean()
    ];
});
