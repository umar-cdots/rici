<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'middle_name' => $faker->name,
        'last_name' => $faker->name,
        'username' => $faker->name,
        'dob' => '2000/04/04',
        'working_experience' => 1,
        'main_education' => 'bachelors',
        'nationality_id' => 1,
        'region_id' => 1,
        'country_id' => 1,
        'state_id' => 1,
        'city_id' => 1,
        'address' => 'the testing address',
        'job_status' => 'full_time',
        'remarks' => 'the testing remark',
        'status' => 'active',
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
