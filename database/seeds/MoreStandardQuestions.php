<?php

use Illuminate\Database\Seeder;
use App\StandardsQuestions;
use App\Standards;

class MoreStandardQuestions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
    	 * Standard = 'ISO 50001:2011', 'ISO 50001:2018'
    	 */
        foreach(['ISO 50001:2011', 'ISO 50001:2018'] as $standard_number)
        {
	    	$standard 				= Standards::where('number', $standard_number)->first();

	        $question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 	= "Industry Type";
	        $question->field_type 	= "radio";
	        $question->options 		= json_encode(['Industry – light to medium', 'Industry – Heavy', 'Buildings Complexes', 'Transport', 'Mining', 'Agriculture', 'Energy Supply']);
	        $question->sort 		= 5;
	        $question->save();
	    }

	    /**
    	 * Standard = 'ISO 22000:2005', 'ISO 22000:2018', 'HACCP', 'FSSC 22000 Ver 4.1'
    	 */
        foreach(['ISO 22000:2005', 'ISO 22000:2018', 'HACCP', 'FSSC 22000 Ver 4.1'] as $standard_number)
        {
        	$standard 				= Standards::where('number', $standard_number)->first();

	        $question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 	= "Food Chain Category.";
	        $question->field_type 	= "special_dropdown";
	        $question->options 		= json_encode(['Farming of Animals', 'Farming of Plants', 'Food Manufacturing', 'Animal Feed Production', 'Catering', 'Provision of Transport and Storage Services', 'Auxiliary services', 'Biochemical']);
	        $question->has_dependent_fields	= true;
	        $question->sort 		= 5;
	        $question->save();

	        $sub_question 				= new StandardsQuestions;
	        $sub_question->standard_id	= $standard->id;
	        $sub_question->question 	= "Food Chain Sub Category.";
	        $sub_question->field_type 	= "dropdown";
	        $sub_question->options 		= json_encode(['Farming of Animals' => array('Farming of Animals for Meat/Milk/Eggs Honey', 'Farming of Fish and Seafood'), 'Farming of Plants' => array('Farming of Plants (other than grains and pulses)', 'Farming of grains and Pulses'), 'Food Manufacturing' => array('Processing of perishable animal products', 'Processing of perisable plant products', 'Processing of perishable animal and plant products(mixed products)', 'Processing of ambient stable products'), 'Animal Feed Production' => array('Production of Feed', 'Production of Pet Food'), 'Catering' => array(''), 'Distribution' => array('Retail/Wholesale', 'Food Broking/Trading'), 'Provision of Transport and Storage Services' => array('Provision of Transport and Storage Services for Perishable Food and Feed', 'Provision of Transport and Storage Services for Ambient Stable Food and Feed'), 'Auxiliary services' => array('Services', 'Production of Food Packaging and Packaging Material', 'Equipment manufacturing'), 'Biochemical' => array('Production of (Bio) Chemicals')]);
	        $sub_question->company_standards_question_id 	= $question->id;
	        $sub_question->save();

	        $question->dependent_fields_ids = $sub_question->id;
	        $question->save();
        }
    }
} 