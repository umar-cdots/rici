<?php

use Illuminate\Database\Seeder;

class InvoiceDataEntrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('invoice_data_entries')->insert([
            'tax_authorities' => 'P.R.A,S.R.B,K.P.K.A',
            'tax_percentage' => '16,20,30',
            'inflation_percentage' => '0,10,15',
            'invoice_remarks_one' => 'As per the Punjab Government Sales Tax regulation for Active taxpayers companies, sales tax of RICI cannot be withheld.',
            'invoice_remarks_two' => 'As per new Income Tax Rule 153(1) (b) withholding for Advance/Income tax for certification/Testing/Inspection/Training Services is changed to 3% from 8 %.',
            'other_user_name' => 'Faizan Ahmed',
            'other_user_designation' => 'Operations Coordinator',
            'other_user_email' => 'Coordination2@ricipakistan.com',
            'other_user_phone_number' => '0346-4497125',
            'other_details_name' => 'RICI Pakistan (Pvt.) Ltd.',
            'other_details_address' => '181-A, M BLOCK GULBERG III Lahore – Pakistan ',
            'other_details_email' => 'operations@ricipakistan.com',
            'other_details_phone_number' => '+92-344-4442484',
        ]);
    }
}
