<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 	= storage_path('data/languages.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$languages 	= json_decode(file_get_contents($path));
        	if(!empty($languages))
        	{
				$data 	= array();
        		foreach($languages as $iso => $language)
        		{
        			$data[]  = array(
        				'iso' 	=> $iso,
        				'name' 			=> $language->name,
        				'native_name' 	=> $language->nativeName,
        				'created_at'	=> $now,
        				'updated_at'	=> $now
        			);
        		}
        		DB::table('languages')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
