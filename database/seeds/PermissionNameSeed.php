<?php

use Illuminate\Database\Seeder;

class PermissionNameSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_names')->insert([
            'name' => 'show_scheme_manager',
            'heading' => 'View Scheme Manager'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_scheme_manager',
            'heading' => 'Create Scheme Manager'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_scheme_manager',
            'heading' => 'Edit Scheme Manager'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_scheme_manager',
            'heading' => 'Delete Scheme Manager'
        ]);


        DB::table('permission_names')->insert([
            'name' => 'show_scheme_coordinator',
            'heading' => 'View Scheme Coordinator'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_scheme_coordinator',
            'heading' => 'Create Scheme Coordinator'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_scheme_coordinator',
            'heading' => 'Edit Scheme Coordinator'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_scheme_coordinator',
            'heading' => 'Delete Scheme Coordinator'
        ]);

        
        DB::table('permission_names')->insert([
            'name' => 'show_operation_manager',
            'heading' => 'View Operation Manager'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_operation_manager',
            'heading' => 'Create Operation Manager'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_operation_manager',
            'heading' => 'Edit Operation Manager'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_operation_manager',
            'heading' => 'Delete Operation Manager'
        ]);
        
        DB::table('permission_names')->insert([
            'name' => 'show_operation_coordinator',
            'heading' => 'View Operation Coordinator'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_operation_coordinator',
            'heading' => 'Create Operation Coordinator'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_operation_coordinator',
            'heading' => 'Edit Operation Coordinator'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_operation_coordinator',
            'heading' => 'Delete Operation Coordinator'
        ]);
        
        DB::table('permission_names')->insert([
            'name' => 'show_auditors',
            'heading' => 'View Auditors'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_auditors',
            'heading' => 'Create Auditors'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_auditors',
            'heading' => 'Edit Auditors'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_auditors',
            'heading' => 'Delete Auditors'
        ]);

        DB::table('permission_names')->insert([
            'name' => 'show_technical_experts',
            'heading' => 'View Technical Experts'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_technical_experts',
            'heading' => 'Create Technical Experts'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_technical_experts',
            'heading' => 'Edit Technical Experts'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_technical_experts',
            'heading' => 'Delete Technical Experts'
        ]);


        DB::table('permission_names')->insert([
            'name' => 'show_company',
            'heading' => 'View Company'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_company',
            'heading' => 'Create Company'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_company',
            'heading' => 'Edit Company'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_company',
            'heading' => 'Delete Company'
        ]);



        DB::table('permission_names')->insert([
            'name' => 'show_company_standard',
            'heading' => 'View Company Standard'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_company_standard',
            'heading' => 'Create Company Standard'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_company_standard',
            'heading' => 'Edit Company Standard'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_company_standard',
            'heading' => 'Delete Company Standard'
        ]);


        DB::table('permission_names')->insert([
            'name' => 'show_aj',
            'heading' => 'View Audit Justification'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_aj',
            'heading' => 'Create Audit Justification'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_aj',
            'heading' => 'Edit Audit Justification'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_aj',
            'heading' => 'Delete Audit Justification'
        ]);


        DB::table('permission_names')->insert([
            'name' => 'show_outsource_request',
            'heading' => 'View Outsource Request'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_outsource_request',
            'heading' => 'Create Outsource Request'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_outsource_request',
            'heading' => 'Edit Outsource Request'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_outsource_request',
            'heading' => 'Delete Outsource Request'
        ]);

        DB::table('permission_names')->insert([
            'name' => 'show_data_entry',
            'heading' => 'View Data Entry'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_data_entry',
            'heading' => 'Create Data Entry'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_data_entry',
            'heading' => 'Edit Data Entry'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_data_entry',
            'heading' => 'Delete Data Entry'
        ]);

        DB::table('permission_names')->insert([
            'name' => 'show_documents',
            'heading' => 'View Documents'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_documents',
            'heading' => 'Create Documents'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_documents',
            'heading' => 'Edit Documents'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_documents',
            'heading' => 'Delete Documents'
        ]);

        DB::table('permission_names')->insert([
            'name' => 'show_notification',
            'heading' => 'View Notification'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'create_notification',
            'heading' => 'Create Notification'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'edit_notification',
            'heading' => 'Edit Notification'
        ]);
        DB::table('permission_names')->insert([
            'name' => 'delete_notification',
            'heading' => 'Delete Notification'
        ]);
    }
}
