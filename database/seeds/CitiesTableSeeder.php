<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $countries 	= App\Countries::all();
        $now 		= date('Y-m-d H:i:s');

        foreach($countries as $country)
        {
        	if($country->states->count() > 0)
        	{
        		$states 	= $country->states;
        		foreach($states as $state)
        		{
		        	$path 	= storage_path('data/countries_regions_cities/cities/'. $country->name .'/'. $state->name .'.json');
			        if(is_file($path))
			        {
			        	$cities 	= json_decode(file_get_contents($path));
			        	if(!empty($cities))
			        	{
			        		$data 	= array();
			        		foreach($cities->cities as $city)
			        		{
			        			$data[]  = array(
			        				'country_id' 	=> $country->id,
			        				'state_id' 		=> $state->id,
			        				'name' 			=> $city->name,
			        				'created_at'	=> $now,
			        				'updated_at'	=> $now,
			        			);
			        		}
			        		DB::table('resident_cities')->insert($data);
			        	}
			        }
			    }
	    	}
        }
    }
}
