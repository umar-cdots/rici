<?php

use Illuminate\Database\Seeder;

class IAFTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 		= storage_path('data/iaf.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$iafs 	= json_decode(file_get_contents($path));
        	if(!empty($iafs))
        	{
				$data 	= array();
        		foreach($iafs as $iaf)
        		{
        			$data[]  = array(
        				'code' 			=> $iaf->Code,
        				'name' 			=> $iaf->Description,
        				'created_at'	=> $now,
        				'updated_at'	=> $now
        			);
        		}
        		DB::table('i_a_fs')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
