<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
//            CountriesTableSeeder::class,
//            StatesTableSeeder::class,
//            CitiesTableSeeder::class,
//            NationalitiesTableSeeder::class,
//            LanguagesTableSeeder::class,
//            IAFTableSeeder::class,
//            IASTableSeeder::class,
//            StandardsFamilyTableSeeder::class,
//            StandardsTableSeeder::class,
//            AuditActivityStagesSeeder::class,
//            StandardsQuestionsSeeder::class,
//            RoleTableSeed::class,
//            PermissionSeeder::class,
//            SuperadminSeed::class,
//            PermissionNameSeed::class,
            InvoiceDataEntrySeeder::class
        ]);
    }
}
