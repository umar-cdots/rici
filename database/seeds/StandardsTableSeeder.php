<?php

use Illuminate\Database\Seeder;

class StandardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 		= storage_path('data/standards.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$family 	= json_decode(file_get_contents($path));
        	if(!empty($family))
        	{
				$data 	= array();
        		foreach($family as $family_name => $standards)
        		{
        			$familyObj 	= App\StandardsFamily::where('name', $family_name)->first();
        			foreach($standards as $standard)
        			{
	        			$data[]  = array(
	        				'standards_family_id'	=> $familyObj->id,
	        				'name' 					=> $standard->Name,
	        				'number' 				=> $standard->Number,
	        				'created_at'			=> $now,
	        				'updated_at'			=> $now
	        			);
	        		}
        		}
        		DB::table('standards')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
