<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now 		= date('Y-m-d H:i:s');
        $region_id 	= DB::table('regions')->insertGetId([
        	'title'				=> "Pakistan",
        	'office_country_id'	=> 179,
        	'office_state_id'	=> 3062,
        	'office_city_id'	=> 44794,
        	'office_address'	=> "DHA Phase 1",
			'created_at'		=> $now,
			'updated_at'		=> $now
        ]);

        DB::table('region_countries')->insert([
        	'region_id'			=> $region_id,
        	'country_id'		=> 179,
			'created_at'		=> $now,
			'updated_at'		=> $now
        ]);

        DB::table('region_countries')->insert([
        	'region_id'			=> $region_id,
        	'country_id'		=> 105,
			'created_at'		=> $now,
			'updated_at'		=> $now
        ]);

        DB::table('region_countries')->insert([
        	'region_id'			=> $region_id,
        	'country_id'		=> 2,
			'created_at'		=> $now,
			'updated_at'		=> $now
        ]);
    }
}
