<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
                'name' => 'super admin',
                'guard_name' => 'web'
        ]);
        DB::table('roles')->insert([
            'name' => 'scheme manager',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'scheme coordinator',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'operation manager',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'operation coordinator',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'auditor',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'data entry operator',
            'guard_name' => 'web'
        ]);
    }
}
 