<?php

use Illuminate\Database\Seeder;

class StandardsFamilyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 		= storage_path('data/standards_families.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$families 	= json_decode(file_get_contents($path));
        	if(!empty($families))
        	{
				$data 	= array();
        		foreach($families as $family)
        		{
        			$data[]  = array(
        				'name' 				=> $family->Name,
        				'is_ims_enabled'	=> $family->IMS,
        				'created_at'		=> $now,
        				'updated_at'		=> $now
        			);
        		}
        		DB::table('standards_families')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
