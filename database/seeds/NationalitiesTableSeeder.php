<?php

use Illuminate\Database\Seeder;

class NationalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 	= storage_path('data/nationalities.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$nationalities 	= json_decode(file_get_contents($path));
        	if(count($nationalities) > 0)
        	{
				$data 	= array();
        		foreach($nationalities as $title)
        		{
        			$data[]  = array(
        				'title' 		=> $title,
        				'created_at'	=> $now,
        				'updated_at'	=> $now
        			);
        		}
        		DB::table('nationalities')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
