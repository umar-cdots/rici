<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['scheme_manager', 'scheme_coordinator', 'operation_coordinator', 'operation_manager', 'auditor', 'company', 'standards',
                  'standard_family', 'accreditation', 'iaf_code', 'food_category','food_subcategory', 'energy_code', 'country', 'city', 'region', 'data_entry', 'manday_calculator'];
        foreach ($roles as $role){
            DB::table('permissions')->insert([
                    'name' => 'add_'.$role,
                    'guard_name' => 'web',
                    'created_at' => now()
            ]);
            DB::table('permissions')->insert([
                'name' => 'edit_'.$role,
                'guard_name' => 'web',
                'created_at' => now()
            ]);
            DB::table('permissions')->insert([
                'name' => 'delete_'.$role,
                'guard_name' => 'web',
                'created_at' => now()
            ]);
            DB::table('permissions')->insert([
                'name' => 'show_'.$role,
                'guard_name' => 'web',
                'created_at' => now()
            ]);
        }

    }
}
