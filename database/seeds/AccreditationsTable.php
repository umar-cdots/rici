<?php

use Illuminate\Database\Seeder;
use App\Accreditation;

class AccreditationsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accreditation 				= new Accreditation;
        $accreditation->name 		= "IAS";
        $accreditation->full_name 	= "International Accounting Standards";
        $accreditation->save();

        $accreditation 				= new Accreditation;
        $accreditation->name 		= "PNAC";
        $accreditation->full_name 	= "Pakistan National Accreditation Council";
        $accreditation->save();

        $accreditation 				= new Accreditation;
        $accreditation->name 		= "SASO";
        $accreditation->full_name 	= "Saudi Arabia Standards Organisation";
        $accreditation->save();

        $accreditation 				= new Accreditation;
        $accreditation->name 		= "Non-Accredited";
        $accreditation->full_name 	= "Non Accredited";
        $accreditation->save();
    }
}
