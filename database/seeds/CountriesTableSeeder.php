<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 	= storage_path('data/countries_regions_cities/countries.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$countries 	= json_decode(file_get_contents($path));
        	if(count($countries) > 0)
        	{
        		$data 	= array();
        		foreach($countries as $country)
        		{
        			$data[]  = array(
        				'name' 			=> $country->country,
        				'iso' 			=> $country->ISO,
        				'iso3' 			=> $country->ISO3,
        				'phone_code' 	=> $country->Phone,
        				'currency_code' => $country->CurrencyCode,
        				'currency_name' => $country->CurrencyName,
        				'continent' 	=> $country->Continent,
        				'created_at'	=> $now,
        				'updated_at'	=> $now
        			);
        		}
        		DB::table('resident_countries')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
