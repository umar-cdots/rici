<?php

use Illuminate\Database\Seeder;
use App\StandardsQuestions;
use App\Standards;

class StandardsQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/**
    	 * Standard = OHSAS 18001:2007
    	 */
    	$standard_number 		= "OHSAS 18001:2007";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities of Production/Services and Substances That Bring Occupational Health and Safety Hazards.";
        $question->field_type 	= "textarea";
        $question->sort 		= 1;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Hazardous Substances Used in Process.";
        $question->field_type 	= "textarea";
        $question->sort 		= 2;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Facilities/Equipment and Materials That Result in Form of Occupational Health and Safety Hazards.";
        $question->field_type 	= "textarea";
        $question->sort 		= 3;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Proposed Complexity";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['high', 'medium', 'low']);
        $question->sort 		= 4;
        $question->save();

    	/**
    	 * Standard = ISO 45001:2018
    	 */

        $standard_number 		= "ISO 45001:2018";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities of Production/Services and Substances That Bring Occupational Health and Safety Hazards.";
        $question->field_type 	= "textarea";
        $question->sort 		= 1;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Hazardous Substances Used in Process.";
        $question->field_type 	= "textarea";
        $question->sort 		= 2;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Facilities/Equipment and Materials That Result in Form of Occupational Health and Safety Hazards.";
        $question->field_type 	= "textarea";
        $question->sort 		= 3;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Proposed Complexity";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['high', 'medium', 'low']);
        $question->sort 		= 4;
        $question->save();

    	/**
    	 * Standard = ISO 14001:2015
    	 */

        $standard_number 		= "ISO 14001:2015";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities of Production/Services and Substances That Bring Occupational Health and Safety Hazards.";
        $question->field_type 	= "checkbox";
        $question->options 		= json_encode(['ocean', 'river', 'forest', 'mountain areas', 'farmland', 'industry', 'area', 'residential district', 'other']);
        $question->has_dependent_fields 				= true;
        $question->right_answer_for_dependent_fields 	= "other";
        $question->sort 		= 1;
        $question->save();

        $sub_question 				= new StandardsQuestions;
        $sub_question->standard_id 	= $question->standard_id;
        $sub_question->question 	= $question->question;
        $sub_question->field_type 	= "small_text";
        $sub_question->sort 		= 1;
        $sub_question->company_standards_question_id 	= $question->id;
        $sub_question->save();

        $question->dependent_fields_ids = $sub_question->id;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities of Production/Services and Substances That Result in Significant Enviornmental Aspects.";
        $question->field_type 	= "textarea";
        $question->sort 		= 2;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Facilities/Equipment and Materials That Result in Significant Enviornmental Aspects.";
        $question->field_type 	= "textarea";
        $question->sort 		= 3;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Proposed Complexity";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['high', 'medium', 'low']);
        $question->sort 		= 4;
        $question->save();
        
    	/**
    	 * Standard = ISO 14001:2004
    	 */

        $standard_number 		= "ISO 14001:2004";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities of Production/Services and Substances That Bring Occupational Health and Safety Hazards.";
        $question->field_type 	= "checkbox";
        $question->options 		= json_encode(['ocean', 'river', 'forest', 'mountain areas', 'farmland', 'industry', 'area', 'residential district', 'other']);
        $question->has_dependent_fields 				= true;
        $question->right_answer_for_dependent_fields 	= "other";
        $question->sort 		= 1;
        $question->save();

        $sub_question 				= new StandardsQuestions;
        $sub_question->standard_id 	= $question->standard_id;
        $sub_question->question 	= $question->question;
        $sub_question->field_type 	= "small_text";
        $sub_question->sort 		= 1;
        $sub_question->company_standards_question_id 	= $question->id;
        $sub_question->save();

        $question->dependent_fields_ids = $sub_question->id;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities of Production/Services and Substances That Result in Significant Enviornmental Aspects.";
        $question->field_type 	= "textarea";
        $question->sort 		= 2;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Facilities/Equipment and Materials That Result in Significant Enviornmental Aspects.";
        $question->field_type 	= "textarea";
        $question->sort 		= 3;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Proposed Complexity";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['high', 'medium', 'low']);
        $question->sort 		= 4;
        $question->save();
        
    	/**
    	 * Standard = ISO 50001:2018
    	 */

        $standard_number 		= "ISO 50001:2018";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Process/Activities/Facilities/Equipment That Result in Significant Energy Comsumption.";
        $question->field_type 	= "textarea";
        $question->sort 		= 1;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Technical Area Identification (Industry, Building, Building Complex, Transport, Mining, Agriculture, Energy Supply).";
        $question->field_type 	= "textarea";
        $question->sort 		= 2;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 	= "Energy Data";
        $question->field_type 	= "heading";
        $question->sort 		= 3;
        $question->save();

        $sub_question  				= new StandardsQuestions;
        $sub_question->standard_id 	= $standard->id;
        $sub_question->question 	= "Annual Energy Consumption in TJ of all Facilities in Scope.";
        $sub_question->field_type 	= "text";
        $sub_question->sort 		= 1;
        $sub_question->company_standards_question_id 	= $question->id;        
        $sub_question->save();

        $sub_question  				= new StandardsQuestions;
        $sub_question->standard_id 	= $standard->id;
        $sub_question->question 	= "Number of Engery Sources.";
        $sub_question->field_type 	= "number";
        $sub_question->sort 		= 2;
        $sub_question->company_standards_question_id 	= $question->id;        
        $sub_question->save();

        $sub_question  				= new StandardsQuestions;
        $sub_question->standard_id 	= $standard->id;
        $sub_question->question 	= "Number of Significant Energy Uses.";
        $sub_question->field_type 	= "number";
        $sub_question->sort 		= 3;
        $sub_question->company_standards_question_id 	= $question->id;        
        $sub_question->save();

    	/**
    	 * Standard = ISO 9001:2015
    	 */

        $standard_number 		= "ISO 9001:2015";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 		= "Exclusion";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['yes', 'no']);
        $question->has_dependent_fields 				= true;
        $question->right_answer_for_dependent_fields 	= "yes";
        $question->sort 		= 1;
        $question->save();

        $sub_question 				= new StandardsQuestions;
        $sub_question->standard_id 	= $question->standard_id;
        $sub_question->question 		= "Description";
        $sub_question->field_type 	= "textarea";
        $sub_question->sort 		= 1;
        $sub_question->company_standards_question_id 	= $question->id;
        $sub_question->save();

        $question->dependent_fields_ids = $sub_question->id;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 		= "Proposed Complexity";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['high', 'medium', 'low']);
        $question->sort 		= 4;
        $question->save();

    	/**
    	 * Standard = ISO 9001:2008
    	 */

        $standard_number 		= "ISO 9001:2008";
    	$standard 				= Standards::where('number', $standard_number)->first();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 		= "Exclusion";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['yes', 'no']);
        $question->has_dependent_fields 				= true;
        $question->right_answer_for_dependent_fields 	= "yes";
        $question->sort 		= 1;
        $question->save();

        $sub_question 				= new StandardsQuestions;
        $sub_question->standard_id 	= $question->standard_id;
        $sub_question->question 		= "Description";
        $sub_question->field_type 	= "textarea";
        $sub_question->sort 		= 1;
        $sub_question->company_standards_question_id 	= $question->id;
        $sub_question->save();

        $question->dependent_fields_ids = $sub_question->id;
        $question->save();

        $question 				= new StandardsQuestions;
        $question->standard_id	= $standard->id;
        $question->question 		= "Proposed Complexity";
        $question->field_type 	= "radio";
        $question->options 		= json_encode(['high', 'medium', 'low']);
        $question->sort 		= 4;
        $question->save();


    	/**
    	 * Standard = 'ISO 22000:2005', 'ISO 22000:2018', 'HACCP', 'FSSC 22000 Ver 4.1'
    	 */
    	foreach(['ISO 22000:2005', 'ISO 22000:2018', 'HACCP', 'FSSC 22000 Ver 4.1'] as $standard_number)
       	{ 
           	$standard 				= Standards::where('number', $standard_number)->first();

           	$question  				= new StandardsQuestions;
	        $question->standard_id 	= $standard->id;
	        $question->question 		= "Number of HACCP Plans";
	        $question->field_type 	= "number";
	        $question->min_length 	= 1;
	        $question->max_length 	= 99;
	        $question->sort 		= 1;       
	        $question->save();

			$question 				= new StandardsQuestions;
			$question->standard_id	= $standard->id;
			$question->question 		= "Food Category";
			$question->field_type 	= "radio";
			$question->options 		= json_encode(['high', 'medium', 'low']);
			$question->sort 		= 2;
			$question->save();
       
			$question 				= new StandardsQuestions;
			$question->standard_id	= $standard->id;
			$question->question 		= "Certified on Any Other Managment System?";
			$question->field_type 	= "select";
			$question->options 		= json_encode(['veg', 'non veg']);
			$question->sort 		= 3;
			$question->save();

			$question 				= new StandardsQuestions;
			$question->standard_id	= $standard->id;
			$question->question 		= "Proposed Complexity";
			$question->field_type 	= "radio";
			$question->options 		= json_encode(['high', 'medium', 'low']);
			$question->sort 		= 4;
			$question->save();
        }

        /**
    	 * Standard = 'ISO 27001:2013', 'ISO 50001:2011', 'ISO 29990:2018'
    	 */
        foreach(['ISO 27001:2013', 'ISO 50001:2011', 'ISO 29990:2018'] as $standard_number)
        {
	    	$standard 				= Standards::where('number', $standard_number)->first();

	        $question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 		= "Exclusion";
	        $question->field_type 	= "radio";
	        $question->options 		= json_encode(['yes', 'no']);
	        $question->has_dependent_fields 				= true;
	        $question->right_answer_for_dependent_fields 	= "yes";
	        $question->sort 		= 1;
	        $question->save();

	        $sub_question 				= new StandardsQuestions;
	        $sub_question->standard_id 	= $question->standard_id;
	        $sub_question->question 		= "Description";
	        $sub_question->field_type 	= "textarea";
	        $sub_question->sort 		= 1;
	        $sub_question->company_standards_question_id 	= $question->id;
	        $sub_question->save();

	        $question->dependent_fields_ids = $sub_question->id;
	        $question->save();

	        $question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 		= "Proposed Complexity";
	        $question->field_type 	= "radio";
	        $question->options 		= json_encode(['high', 'medium', 'low']);
	        $question->sort 		= 4;
	        $question->save();
	    }

        /**
    	 * Standard = 'ISO 29990:2010', 'ISO 29993:2017'
    	 */
        foreach(['ISO 29990:2010', 'ISO 29993:2017'] as $standard_number)
        {
	    	$standard 				= Standards::where('number', $standard_number)->first();

	    	$question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 		= "Types of Vocational Courses Being Conducted (Management, Engineering, Automotive).";
	        $question->field_type 	= "textarea";
	        $question->sort 		= 1;
	        $question->save();

	    	$question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 		= "Types of Training Provided (Classroom, Distance Learning etc).";
	        $question->field_type 	= "textarea";
	        $question->sort 		= 2;
	        $question->save();

	    	$question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 		= "Target Learner Market In-house Only or Commericial.";
	        $question->field_type 	= "textarea";
	        $question->sort 		= 3;
	        $question->save();
	       	
	        $question 				= new StandardsQuestions;
	        $question->standard_id	= $standard->id;
	        $question->question 		= "Proposed Complexity";
	        $question->field_type 	= "radio";
	        $question->options 		= json_encode(['high', 'medium', 'low']);
	        $question->sort 		= 4;
	        $question->save();
	    }
    }
}
