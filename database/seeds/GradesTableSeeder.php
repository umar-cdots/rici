<?php

use Illuminate\Database\Seeder;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $grade = new \App\Grades();
        $grade->name = 'Lead Auditor';
        $grade->short_name = 'LA';
        $grade->save();

        $grade = new \App\Grades();
        $grade->name = 'Trainee Lead Auditor';
        $grade->short_name = 'TLA';
        $grade->save();

        $grade = new \App\Grades();
        $grade->name = 'Auditor';
        $grade->short_name = 'A';
        $grade->save();

        $grade = new \App\Grades();
        $grade->name = 'Trainee Auditor';
        $grade->short_name = 'TA';
        $grade->save();

        $grade = new \App\Grades();
        $grade->name = 'Supervisor Auditor';
        $grade->short_name = 'SA';
        $grade->save();

        $grade = new \App\Grades();
        $grade->name = 'Observer Auditor';
        $grade->short_name = 'OA';
        $grade->save();


    }
}
