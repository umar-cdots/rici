<?php

use Illuminate\Database\Seeder;

class IASTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->disableQueryLog();
        $path 		= storage_path('data/ias.json');
        $now 		= date('Y-m-d H:i:s');

        if(is_file($path))
        {
        	$iases 	= json_decode(file_get_contents($path));
        	if(!empty($iases))
        	{
				$data 	= array();
        		foreach($iases as $ias)
        		{
        			$iaf 	 = App\IAF::whereRaw("code * 1 = {$ias->IAF_Code}")->first();
        			$data[]  = array(
        				'iaf_id' 		=> $iaf->id,
        				'code' 			=> $ias->Code,
        				'name' 			=> $ias->Description,
        				'created_at'	=> $now,
        				'updated_at'	=> $now
        			);
        		}
        		DB::table('i_a_s')->insert($data);
        	}
        }
        else
        {
        	throw new Exception("File not Found.");
        }
    }
}
