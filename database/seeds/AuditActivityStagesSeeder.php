<?php

use Illuminate\Database\Seeder;
use App\AuditActivityStages;

class AuditActivityStagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $audit_activity_stage 		= new AuditActivityStages;
        $audit_activity_stage->name = "Stage 1"; 
        $audit_activity_stage->save();

        $audit_activity_stage 		= new AuditActivityStages;
        $audit_activity_stage->name = "Stage 2"; 
        $audit_activity_stage->save();

        $audit_activity_stage 		= new AuditActivityStages;
        $audit_activity_stage->name = "Surveillance 1"; 
        $audit_activity_stage->save();

        $audit_activity_stage 		= new AuditActivityStages;
        $audit_activity_stage->name = "Surveillance 2"; 
        $audit_activity_stage->save();

        $audit_activity_stage 		= new AuditActivityStages;
        $audit_activity_stage->name = "Re-audit"; 
        $audit_activity_stage->save();
    }
}
