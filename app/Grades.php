<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grades extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function getIsMultiple(){
        return ($this->is_multiple) ? 'yes' : 'no';
    }
}
