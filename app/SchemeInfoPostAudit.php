<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeInfoPostAudit extends Model
{
    public function postAudit(){
        return $this->belongsTo(PostAudit::class,'post_audit_id','id');
    }
}
