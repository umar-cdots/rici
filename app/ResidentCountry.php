<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ResidentCountry extends Model
{
    protected $table = "resident_countries";
    protected $guarded = [];
    use SoftDeletes;
    use LogsActivity;

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;


    public function cities()
    {
        return $this->hasMany(ResidentCity::class, 'country_id');
    }
}
