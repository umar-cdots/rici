<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDataEntry extends Model
{
    protected $table = "invoice_data_entries";
    protected $guarded = [];
}
