<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\Auditor;
use App\AuditorConfidentialityAgreement;
use App\AuditorDocument;
use App\AuditorEducation;
use App\AuditorEmploymentHistory;
use App\AuditorStandard;
use App\Cities;
use App\Company;
use App\CompanyContacts;
use App\CompanyStandards;
use App\CompanyStandardsAnswers;
use App\Countries;
use App\IAF;
use App\IAS;
use App\Languages;
use App\Notification;
use App\Regions;
use App\ResidentCity;
use App\ResidentCountry;
use App\Standards;
use App\StandardsFamily;
use App\User;
use App\UserLanguages;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\AccessRights;
use Mockery\Matcher\Not;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuditorController extends Controller
{
    use AccessRights;

    public function index()
    {
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $auditors = Auditor::where('role', 'auditor')->get();

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $auditors = Auditor::where('role', 'auditor')->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {


            $auditors = Auditor::where('role', 'auditor')->where('region_id', auth()->user()->region_id)->get();

        } else {
            $auditors = Auditor::where('role', 'auditor')->where('user_id', auth()->user()->id)->get();
        }
        $auditorStandards = [];
        foreach ($auditors as $key => $auditor) {
            $auditorStandards[$key] = AuditorStandard::where('auditor_id', $auditor->id)->where('deleted_at', NULL)->get();
        }
        return view('auditors.index', compact('auditors', 'auditorStandards'));
    }

    public function create()
    {
        if (auth()->user()->user_type == 'admin') {
            $regions = Regions::all();
            $standards_families = StandardsFamily::orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $standards_families = StandardsFamily::orderBy('sort')->get();
        }


        $languages = Languages::all();
        $countries = Countries::all();


        $data = array(
            'regions' => $regions,
            'languages' => $languages,
            'countries' => $countries,
            'standards_families' => $standards_families
        );


        return view('auditors.create')->with($data);
    }

    public function deleteAuditor($auditorId)
    {
        $deleteAuditor = Auditor::find($auditorId)->delete();

        if ($deleteAuditor) {

            return redirect()->back()->with(['status' => 'success', 'message' => 'Auditor Deleted Successfully']);

        } else {
            return redirect()->back()->with('error', 'Failed to delete Auditor');
        }

    }

    public function show($auditorId)
    {
        $auditor = Auditor::find($auditorId);


        $user_language = UserLanguages::select()
            ->where('user_id', $auditor->user_id)
            ->groupBy('language_id')
            ->get();
        $auditor_language = [];
        foreach ($user_language as $key => $language) {
            $auditor_language[$key] = Languages::where('id', $language->language_id)->get();
        }


        return view('auditors.show', compact('auditor', 'auditor_language'));

    }

    public function edit($auditorId)
    {
        $auditor = Auditor::find($auditorId);
        if (auth()->user()->user_type == 'admin') {
            $regions = Regions::all();
            $standards_families = StandardsFamily::orderBy('sort')->get();
            $standards = Standards::orderBy('name')->get();
            $auditorStandardCount = AuditorStandard::where('auditor_id', '=', $auditorId)->where('auditor_standard_status', 'unapproved')->count();
            $notification_count = Notification::where('request_id', '=', $auditorId)->where('status', 'unapproved')->where('type', 'auditor')->count();


        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();

            $standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('name')->get();
            $auditorStandardCount = AuditorStandard::where('auditor_id', '=', $auditorId)->where('auditor_standard_status', 'unapproved')->count();
            $notification_count = Notification::where('request_id', '=', $auditorId)->where('status', 'unapproved')->where('type', 'auditor')->count();

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $standards_families = StandardsFamily::orderBy('sort')->get();
            $standards = Standards::orderBy('name')->get();
            $auditorStandardCount = AuditorStandard::where('auditor_id', '=', $auditorId)->where('auditor_standard_status', 'unapproved')->count();
            $notification_count = Notification::where('request_id', '=', $auditorId)->where('status', 'unapproved')->where('type', 'auditor')->count();

        }


        $languages = Languages::all();
        $countries = Countries::all();

        $country_zones = Zone::where('country_id', $auditor->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $auditor->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
//        $cities             = ResidentCity::where('country_id', $auditor->country_id)->get();

        $accreditations = Accreditation::orderBy('name')->get();
        $iafs = IAF::orderBy('code')->get();
        $iass = IAS::orderBy('code')->get();

        $data = array(
            'regions' => $regions,
            'languages' => $languages,
            'countries' => $countries,
            'standards_families' => $standards_families,
            'cities' => $cities,
            'auditor' => $auditor,
            'standards' => $standards,
            'accreditations' => $accreditations,
            'iafs' => $iafs,
            'iass' => $iass,
            'auditorStandardCount' => $auditorStandardCount,
            'notification_count' => $notification_count,

        );


        return view('auditors.edit')->with($data);

    }

    public function updateAuditorStatus($auditorId)
    {
        $response = array();
        try {

            $auditor = Auditor::find($auditorId);

            if ($auditor) {
                $toggleStatus = 'active';

                $auditStatus = $auditor->auditor_status;
                if ($auditStatus == 'active') {
                    $toggleStatus = 'inactive';
                }

                $updateAuditorStatus = $auditor->update([
                    'auditor_status' => $toggleStatus
                ]);

                $response = (new ApiMessageController())->saveresponse('Auditor Status Updated Successfully');

            } else {
                $response = (new ApiMessageController())->failedresponse('Auditor Does Not Exist');
            }


        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function store(Request $request)
    {
        $response = array();

        if ($request->step == 1) {
            if ($request->has('auditor_id')) {
                $validator = Validator::make($request->all(), [
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
                    'email' => 'required|email',
                    'phone' => 'required|numeric',
//                    'landline' => 'numeric',
                    'dob' => 'required',
                    'postal_address' => 'required|min:5',
                    'region_id' => 'required|exists:regions,id',
                    'country_id' => 'required|exists:countries,id',
                    'city_id' => 'required|exists:cities,id',
//                    "languages_id"    => "required|array|exists:languages,id'",
                    "language_id.0" => "required",
                    'working_experience' => 'required|min:0.1',
                    'main_education' => 'required|string',
                    'nationality_id' => 'required|exists:countries,id',
                    'job_status' => ['required', Rule::in(Auditor::getEnumValues()->get('job_status'))],
                    'username' => 'required|min:2',
//                    'password' => 'required|min:6|confirmed',
                    'profile_remarks' => 'string',
                    'pp' => 'file|image',
                ], [
                    'region_id.required' => "Region Required.",
                    'region_id.exists' => "Invalid Region Selected.",
                    'country_id.required' => "Country Required.",
                    'country_id.exists' => "Invalid Country Selected.",
                    'city_id.required' => "City Required.",
                    'city_id.exists' => "Invalid City Selected.",
                    'nationality_id.required' => "Nationality Required.",
                    'nationality_id.exists' => "Invalid Nationality Selected.",
                    'pp.required' => "Profile Image is required!",
                    'pp.file' => "Profile Image Must be a file!",
                    'pp.image' => "Profile Image must be an image file",
                    'language_id.0.required' => "Language Required"
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
//                    'email' => 'required|email|unique:users',
                    'email' => 'required|email',
                    'phone' => 'required|numeric',
//                    'landline' => 'numeric',
                    'dob' => 'required',
                    'postal_address' => 'required|min:5',
                    'region_id' => 'required|exists:regions,id',
                    'country_id' => 'required|exists:countries,id',
                    'city_id' => 'required|exists:cities,id',
//                    "languages_id"    => "required|array|exists:languages,id'",
                    "language_id.0" => "required",
                    'working_experience' => 'required|numeric|min:0',
                    'main_education' => 'required',
                    'nationality_id' => 'required|exists:countries,id',
                    'job_status' => [Rule::in(Auditor::getEnumValues()->get('job_status'))],
                    'username' => 'required|min:2|unique:users',
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required',
                    'profile_remarks' => 'string',
                    'pp' => 'required|file|image',
                ], [
                    'region_id.required' => "Region Required.",
                    'region_id.exists' => "Invalid Region Selected.",
                    'country_id.required' => "Country Required.",
                    'country_id.exists' => "Invalid Country Selected.",
                    'city_id.required' => "City Required.",
                    'city_id.exists' => "Invalid City Selected.",
                    'nationality_id.required' => "Nationality Required.",
                    'nationality_id.exists' => "Invalid Nationality Selected.",
                    'pp.required' => "Profile Image is required!",
                    'pp.file' => "Profile Image Must be a file!",
                    'pp.image' => "Profile Image must be an image file",
                    'language_id.0.required' => "Language Required"
                ]);
            }

            if (!$validator->fails()) {
                if ($request->has('auditor_id')) {
                    $auditor = Auditor::find($request['auditor_id']);
                } else {
                    $auditor = new Auditor();
                    $auditor->auditor_status = $request->auditor_status;
                }
                $auditor->first_name = $request->first_name;
                $auditor->last_name = $request->last_name;
                $auditor->email = $request->email;
                $auditor->phone = $request->phone;
                $auditor->landline = $request->landline;
                $auditor->postal_address = $request->postal_address;
                $auditor->region_id = $request->region_id;
                $auditor->country_id = $request->country_id;
                $auditor->city_id = $request->city_id;
                $auditor->dob = date("Y/m/d", strtotime($request->dob));
                $auditor->working_experience = $request->working_experience;
                $auditor->main_education = $request->main_education;
                $auditor->nationality_id = $request->nationality_id;
                $auditor->job_status = $request->job_status;
                $auditor->main_education = $request->main_education;
                $auditor->profile_remarks = $request->profile_remarks;
                $auditor->form_progress_step = $request->step;
                if ($request->has('auditor_id')) {

//                    $auditor->form_progress = 100;
//                    $auditor->form_current_progress = 'completed';
                } else {
                    $auditor->form_progress = 25;

                }

                $auditor->form_current_progress = 'processing';
                $auditor->role = 'auditor';


                if ($request->has('pp')) {
                    $image = $request->file('pp');
                    $name = $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/auditors');
                    $auditor_files_with_time = time() . '_' . $name;
                    $image->move($destinationPath, $auditor_files_with_time);
                    $auditor->profile_pic = $auditor_files_with_time;
                }

//
                $auditor->save();

                if ($request->has('auditor_id')) {
                    $user = $auditor->user;
                } else {
                    $user = new User();
                }
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->username = $request->username;
                $user->dob = date("Y/m/d", strtotime($request->dob));
                $user->working_experience = $request->working_experience;
                $user->main_education = $request->main_education;
                $user->nationality_id = $request->nationality_id;
                $user->region_id = $request->region_id;
                $user->country_id = $request->country_id;
                $user->city_id = $request->city_id;
                $user->address = $request->postal_address;
                $user->job_status = $request->job_status;
                $user->remarks = $request->profile_remarks;
                $user->phone_number = $request->phone;
                $user->email = $request->email;
                $user->password = ($request->password == '' || $request->password == null) ? $user->password : bcrypt($request->password);
                $user->user_type = 'auditor';
                $user->status = 'applied';
                $user->save();

                $auditor->user_id = $user->id;
                $auditor->save();

                if ($request->has('auditor_id')) {

                } else {
                    //            //assigning roles and permissions to user
                    $role = Role::where('id', 2)->first();
                    $user->assignRole($role->name);
                    foreach ($role->permissions as $permission) {
                        $user->givePermissionTo($permission->name);
                    }
                }

                foreach ($request->language_id as $lang) {
//                    $userLang = new UserLanguages();
                    $userLang = new UserLanguages();
                    $userLang->user_id = $user->id;
                    $userLang->language_id = $lang;
                    $userLang->save();

                }


                if ($auditor)

                    $data = [
                        'auditor_id' => $auditor->id,
                        'user_id' => $user->id,
                        'full_name' => $auditor->fullName(),
                        'current_step' => 1,
                        'next_step' => 2
                    ];
                if ($request->has('auditor_id')) {
                    $response = (new ApiMessageController())->successResponse($data, 'Auditor Profile Updated Successfully!');
                } else {
                    $response = (new ApiMessageController())->successResponse($data, 'Auditor Profile Created Successfully!');
                }

            } else {

                $data = $validator->errors()->toArray();

                $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Something Went Wrong.";
        }

        return $response;
    }


    public function getAuditorFormalEducationView($auditor_id = '')
    {
        $auditorEducations = array();

        if (!empty($auditor_id)) {
            $auditorEducations = (new AuditorEducation())->where('auditor_id', '=', $auditor_id)->get();
        }

        return view('auditors.partials.formal-education', compact('auditorEducations', 'auditor_id'));
    }

    public function saveAuditorEducation(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'year_passing' => 'required|numeric',
            'institution' => 'required',
            'degree_diploma' => 'required',
            'major_subjects' => 'required',
            'certificate' => 'file|required',
        ], [
            'auditor_id.required' => "Create Auditor First",
            'auditor_id.exists' => "Auditor Does Not Exist",
            'certificate.file' => "Profile Image Must be a file!",
        ]);

        if (!$validator->fails()) {


            $auditorEducation = new AuditorEducation();
            $auditorEducation->auditor_id = $request->auditor_id;
            $auditorEducation->year_passing = $request->year_passing;
            $auditorEducation->institution = $request->institution;
            $auditorEducation->degree_diploma = $request->degree_diploma;
            $auditorEducation->major_subjects = $request->major_subjects;

            if ($request->hasFile('certificate')) {

                $image = $request->file('certificate');
                $name = $image->getClientOriginalName();

                $destinationPath = public_path('/uploads/auditor_education_certificate');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorEducation->education_document = $auditor_files_with_time;

            }

            $auditorEducation->save();

            $auditor = Auditor::find($request['auditor_id']);
            $auditor->form_progress_step = 2;
            $auditor->form_progress = 50;
            $auditor = $auditor->save();


            if ($auditorEducation && $auditor)

                $data = [
                    'status' => 'success',
                    'auditor_id' => $request->auditor_id,
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Education Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getEditAuditorEducationModal($educationId)
    {
        try {

            $auditorEducation = (new AuditorEducation())->where('id', '=', $educationId)->first();

            return view('auditors.partials.edit-form-education-modal', compact('auditorEducation'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateAuditorEducation(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'year_passing' => 'required|numeric',
            'institution' => 'required',
            'degree_diploma' => 'required',
            'major_subjects' => 'required',
            'auditor_education_id' => 'required|exists:auditor_educations,id',
        ]);

        if (!$validator->fails()) {

            $auditorEducation = AuditorEducation::find($request->auditor_education_id);
            $auditorEducation->year_passing = $request->year_passing;
            $auditorEducation->institution = $request->institution;
            $auditorEducation->degree_diploma = $request->degree_diploma;
            $auditorEducation->major_subjects = $request->major_subjects;
            if ($request->hasFile('certificate')) {
                $image = $request->file('certificate');

                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auditor_education_certificate');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorEducation->education_document = $auditor_files_with_time;
            }

//            dd($auditorEducation);

//            if ($request->hasFile('certificate'))
//                $auditorEducation->education_document = str_replace('public/', 'storage/', $request->file('certificate')->store('public/auditor_education_certificate'));

            $auditorEducation->save();

//
//            if ($request->hasFile('certificate')) {
//                $auditorEducation->getFirstMedia()->delete();
//                $auditorEducationUpdate = (new AuditorEducation())->where('id', '=', $request->auditor_education_id)->first();
//                $path_to_file = storage_path('app/' . $request->file('certificate')->storeAs('public', $request->file('certificate')->getClientOriginalName()));
//                $auditorEducation->addMedia($path_to_file)->toMediaCollection();
//            }

            if ($auditorEducation)

                $data = [
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Education Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteAuditorEducation($auditorEducationId)
    {

        try {
            $education = AuditorEducation::findOrFail($auditorEducationId);
            $deleteItem = $education->delete();

            if ($deleteItem) {
                $response = (new ApiMessageController())->saveresponse("Auditor Education Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }


    public function getAuditorDocumentsView($auditor_id = '')
    {
        $auditorDocuments = array();

        if (!empty($auditor_id)) {
            $auditorDocuments = (new AuditorDocument())->where('auditor_id', '=', $auditor_id)->get();
        }

        return view('auditors.partials.documents', compact('auditorDocuments', 'auditor_id'));
    }

    public function saveAuditorDocument(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'document_name' => 'required',
            'document_file' => 'file|required',
        ], [
            'auditor_id.required' => "Create Auditor First",
            'auditor_id.exists' => "Auditor Does Not Exist",
            'document_file.file' => "Document Must be a file!",
        ]);

        if (!$validator->fails()) {

            $auditorDocument = new AuditorDocument();
            $auditorDocument->auditor_id = $request->auditor_id;
            $auditorDocument->document_name = $request->document_name;
            if ($request->hasFile('document_file')) {
                $image = $request->file('document_file');

                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auditor_document');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorDocument->auditor_document = $auditor_files_with_time;
            }

            $auditorDocument->save();


            if ($auditorDocument)

                $data = [
                    'auditor_id' => $request->auditor_id,
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Document Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteAuditorDocument($auditorEducationId)
    {

        try {

            $education = AuditorDocument::findOrFail($auditorEducationId);
            $deleteItem = $education->delete();

            if ($deleteItem) {
                $response = (new ApiMessageController())->saveresponse("Auditor Document Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;


    }

    public function getEditAuditorDocumentModal($documentId)
    {
        try {

            $auditorDocument = (new AuditorDocument())->where('id', '=', $documentId)->first();

            return view('auditors.partials.edit-document-modal', compact('auditorDocument'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateAuditorDocument(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_document_id' => 'required|exists:auditor_documents,id',
            'document_name' => 'required',
        ], [
            'auditor_document_id.required' => "Add Auditor Document First",
            'auditor_document_id.exists' => "Auditor Document Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $auditorDocument = AuditorDocument::find($request->auditor_document_id);
            $auditorDocument->document_name = $request->document_name;
            if ($request->hasFile('document_file')) {
                $image = $request->file('document_file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auditor_document');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorDocument->auditor_document = $auditor_files_with_time;
            }
            $auditorDocument->save();

            if ($auditorDocument)

                $data = [
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Document Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function getAuditorAgreementView($auditor_id = '')
    {
        $auditorAgreements = array();

        if (!empty($auditor_id)) {
            $auditorAgreements = (new AuditorConfidentialityAgreement())->where('auditor_id', '=', $auditor_id)->get();
        }

        return view('auditors.partials.confidentiality-agreement', compact('auditorAgreements', 'auditor_id'));
    }

    public function saveAuditorAgreement(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'date_signed' => 'required',
            'document_file' => 'file|required',
        ], [
            'auditor_id.required' => "Create Auditor First",
            'auditor_id.exists' => "Auditor Does Not Exist",
            'document_file.file' => "Document Must be a file!",
        ]);

        if (!$validator->fails()) {

            $auditorAgreement = new AuditorConfidentialityAgreement();
            $auditorAgreement->auditor_id = $request->auditor_id;
            $auditorAgreement->date_signed = $request->date_signed;

            if ($request->hasFile('document_file')) {

                $image = $request->file('document_file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auditor_confidentially_agreement');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorAgreement->agreement_document = $auditor_files_with_time;

            }

            $auditorAgreement->save();

            $auditor = Auditor::find($request['auditor_id']);
            $auditor->form_progress_step = 3;
            $auditor->form_progress = 75;

            $auditor = $auditor->save();


            if ($auditorAgreement && $auditor)
                $data = [
                    'status' => 'success',
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Document Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteAuditorAgreement($auditorAgreementId)
    {

        try {

            $agreement = AuditorConfidentialityAgreement::findOrFail($auditorAgreementId);
            $deleteItem = $agreement->delete();

            if ($deleteItem) {
                $response = (new ApiMessageController())->saveresponse("Auditor Agreement Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;


    }

    public function getEditAuditorAgreementModal($agreementId)
    {
        try {

            $auditorAgreement = (new AuditorConfidentialityAgreement())->where('id', '=', $agreementId)->first();


            return view('auditors.partials.edit-agreement-modal', compact('auditorAgreement'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateAuditorAgreement(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_agreement_id' => 'required|exists:auditor_confidentiality_agreements,id',
            'date_signed' => 'required',
        ], [
            'auditor_agreement_id.required' => "Add Auditor Agreement First",
            'auditor_agreement_id.exists' => "Auditor Agreement Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $auditorAgreement = AuditorConfidentialityAgreement::find($request->auditor_agreement_id);
            $auditorAgreement->date_signed = $request->date_signed;
            if ($request->hasFile('document_file')) {
                $image = $request->file('document_file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/auditor_confidentially_agreement');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorAgreement->agreement_document = $auditor_files_with_time;
            }
            $auditorAgreement->save();



            if ($auditorAgreement)
                $data = [
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Agreement Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function getAuditorEmpHistoryView($auditor_id = '')
    {
        $auditorEmpHistories = array();

        if (!empty($auditor_id)) {
            $auditorEmpHistories = (new AuditorEmploymentHistory())->where('auditor_id', '=', $auditor_id)->get();
        }

        return view('auditors.partials.employment-history', compact('auditorEmpHistories', 'auditor_id'));
    }

    public function saveAuditorEmpHistory(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'company_name' => 'required',
            'position_held' => 'required',
            'employed_from' => 'required|numeric',
            'employed_to' => 'required|numeric',

        ], [
            'auditor_id.required' => "Create Auditor First",
            'auditor_id.exists' => "Auditor Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $model = new AuditorEmploymentHistory();
            $model->auditor_id = $request->auditor_id;
            $model->company_name = $request->company_name;
            $model->position_held = $request->position_held;
            $model->employed_from = $request->employed_from;
            $model->employed_to = $request->employed_to;
            $model->save();


            if ($model)
                $data = [
                    'current_step' => 3,
                    'next_step' => 3
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Employment History Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteAuditorEmpHistory($auditorEmpHistoryId)
    {

        try {

            $agreement = AuditorEmploymentHistory::findOrFail($auditorEmpHistoryId);
            $deleteItem = $agreement->delete();

            if ($deleteItem) {
                $response = (new ApiMessageController())->saveresponse("Auditor Employment History Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;


    }

    public function getEditAuditorEmploymentHistoryModal($empId)
    {
        try {

            $auditorEmpHistory = (new AuditorEmploymentHistory())->where('id', '=', $empId)->first();


            return view('auditors.partials.edit-emp-history-modal', compact('auditorEmpHistory'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateAuditorEmpHistory(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_emp_history_id' => 'required|exists:auditor_employment_histories,id',
            'company_name' => 'required',
            'position_held' => 'required',
            'employed_from' => 'required|numeric',
            'employed_to' => 'required|numeric',

        ], [
            'auditor_emp_history_id.required' => "Create Auditor Employment History First",
            'auditor_emp_history_id.exists' => "Auditor Employment History Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $model = AuditorEmploymentHistory::find($request->auditor_emp_history_id);
            $model->company_name = $request->company_name;
            $model->position_held = $request->position_held;
            $model->employed_from = $request->employed_from;
            $model->employed_to = $request->employed_to;
            $model->save();


            if ($model)
                $data = [
                    'current_step' => 3,
                    'next_step' => 3
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Auditor Employment History Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function finish(Request $request)
    {
        $auditor = Auditor::find($request['auditor_id']);
        $auditor->form_progress_step = 4;
        $auditor->form_progress = 100;

        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
            $auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereNull('deleted_at')->get();
            foreach ($auditorStandards as $auditorStandard) {
                $auditorStandard->auditor_standard_status = 'approved';
                $auditorStandard->save();
                $notification = Notification::where('type_id', (int)$auditorStandard->standard_id)->where('type', 'auditor')->where('request_id', (int)$auditor->id)->where('is_read', false)->first();
                if (!is_null($notification)) {
                    $notification->is_read = true;
                    $notification->save();
                }
            }
        }


        $auditor = $auditor->save();

        if ($request->type = "edit") {
            $response = (new ApiMessageController())->saveresponse('Auditor Profile Updated Successfully');
        } else {

            $response = (new ApiMessageController())->saveresponse('Auditor Profile Created Successfully');
        }
        return $response;
    }


    public function sendForApproval(Request $request)
    {
        $auditor = Auditor::find($request['auditor_id']);
        $auditor->form_progress_step = 4;
        $auditor->form_progress = 100;

        $auditor = $auditor->save();

        if ($request->auditorStandardStatus == 'unapproved') {
            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('auditor_standard_status', $request->auditorStandardStatus)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();
            if (!empty($auditorStandards) && count($auditorStandards) > 0) {


                $auditor = Auditor::find($request['auditor_id']);
                foreach ($auditorStandards as $auditorStandard) {


                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'auditor',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $auditorStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/auditors/edit-auditor/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'New for '. $auditorStandard->standard->name .' - ' . $auditor->fullName(),
                        ]);
                    }


                }
                if ($request->type = "edit" && $request->auditorStandardStatus == 'unapproved') {
                    $response = (new ApiMessageController())->saveresponse('Auditor Standard Save and Notifications sent Successfully');
                }
            }


        } elseif ($request->auditorStandardStatus == 'rejected') {

            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('standard_id', (int)$request->standardId)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();

            if (!empty($auditorStandards) && count($auditorStandards) > 0) {

                $auditor = Auditor::find($request['auditor_id']);
                $operation_manager = Notification::where('type_id', (int)$request->standardId)->where('type', 'auditor')->where('request_id', (int)$request->auditor_id)->where('is_read', false)->first();


                foreach ($auditorStandards as $auditorStandard) {
                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'auditor',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $operation_manager->sent_by,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/auditors/edit-auditor/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'Rejected for '. $auditorStandard->standard->name .' - ' . $auditor->fullName()

                        ]);
                    }
                    $auditorStandard->auditor_standard_status = 'rejected';
                    $auditorStandard->save();

                }
                $operation_manager->is_read = true;
                $operation_manager->save();
                if ($request->type = "edit" && $request->auditorStandardStatus == 'rejected') {

                    $response = (new ApiMessageController())->saveresponse('Auditor Standard Save and Notifications sent Successfully');
                }
            }
        } elseif ($request->auditorStandardStatus == 'resent') {

            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('standard_id', (int)$request->standardId)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();

            if (!empty($auditorStandards) && count($auditorStandards) > 0) {

                $auditor = Auditor::find($request['auditor_id']);
                $operation_manager = Notification::where('type_id', (int)$request->standardId)->where('type', 'auditor')->where('request_id', (int)$request->auditor_id)->where('is_read', false)->first();


                foreach ($auditorStandards as $auditorStandard) {
                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'auditor',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $operation_manager->sent_by,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/auditors/edit-auditor/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'Re-Submitted for '. $auditorStandard->standard->name .' - ' . $auditor->fullName()

                        ]);
                    }
                    $auditorStandard->auditor_standard_status = 'resent';
                    $auditorStandard->save();

                }

                if(!is_null($operation_manager)){
                    $operation_manager->is_read = true;
                    $operation_manager->save();
                }

                if ($request->type = "edit" && $request->auditorStandardStatus == 'resent') {

                    $response = (new ApiMessageController())->saveresponse('Auditor Standard Save and Notifications sent Successfully');
                }
            }
        } elseif ($request->auditorStandardStatus == 'approved') {

            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('standard_id', (int)$request->standardId)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();

            if (!empty($auditorStandards) && count($auditorStandards) > 0) {

                $auditor = Auditor::find($request['auditor_id']);
                $operation_manager = Notification::where('type_id', (int)$request->standardId)->where('type', 'auditor')->where('request_id', (int)$request->auditor_id)->where('is_read', false)->first();


                foreach ($auditorStandards as $auditorStandard) {
                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'auditor',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $operation_manager->sent_by,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/auditors/edit-auditor/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'Approved for '. $auditorStandard->standard->name .' - ' . $auditor->fullName()

                        ]);
                    }
                    $auditorStandard->auditor_standard_status = 'approved';
                    $auditorStandard->save();

                }
                $operation_manager->is_read = true;
                $operation_manager->save();
                if ($request->type = "edit" && $request->auditorStandardStatus == 'approved') {

                    $response = (new ApiMessageController())->saveresponse('Auditor Standard Save and Notifications sent Successfully');
                }
            }
        }
        return $response;
    }

}
