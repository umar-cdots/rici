<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\Auditor;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\Cities;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\Countries;
use App\DataEntryIAF;
use App\DateEntryIAS;
use App\EffectiveEmployeesFormula;
use App\EnergyCode;
use App\EnergyEnms;
use App\EnergyFamilySheet;
use App\EnergyWeightage;
use App\FoodCategory;
use App\FoodFamilySheet;
use App\FoodSubCategory;
use App\IAF;
use App\IAS;
use App\InvoiceDataEntry;
use App\LetterName;
use App\ManDaySheet;
use App\Regions;
use App\RegionsCountries;
use App\SchemeManagerStandards;
use App\StandardsFamily;
use App\Standards;
use App\User;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\AccessRights;

class DataEntryController extends Controller
{
    use AccessRights;

    public function index()
    {
        // $this->VerifyRights();
        $standardFamilies = StandardsFamily::orderBy('name')->get();
        $accreditations = Accreditation::orderBy('name')->get();
        $iafs = IAF::orderBy('code')->get();
        $iafs_nullable = DataEntryIAF::orderBy('code')->get();
        $iass = IAS::orderBy('code')->get();
        $ias_nullable = DateEntryIAS::orderBy('code')->get();
        $foodCategories = FoodCategory::orderBy('code')->get();
        $countries = Countries::orderBy('name')->get();
        $zones = Zone::orderBy('name')->get();
        $standards = Standards::with('standardFamily')->get();
        $generic_sheets = ManDaySheet::all();
        $food_sheets = FoodFamilySheet::all();
        $energy_sheets = EnergyFamilySheet::all();
        $energy_weightage = EnergyWeightage::all();
        $energy_enms = EnergyEnms::all();
        $effective_employee_formula = EffectiveEmployeesFormula::first();
        $letters = LetterName::first();
        $invoice = InvoiceDataEntry::first();

        $data = array(
            'standard_families' => $standardFamilies,
            'accreditations' => $accreditations,
            'iafs' => $iafs,
            'iafs_nullable' => $iafs_nullable,
            'ias_nullable' => $ias_nullable,
            'iass' => $iass,
            'foodCategories' => $foodCategories,
            'countries' => $countries,
            'standards' => $standards,
            'generic_sheets' => $generic_sheets,
            'food_sheets' => $food_sheets,
            'energy_sheets' => $energy_sheets,
            'energy_weightage' => $energy_weightage,
            'energy_enms' => $energy_enms,
            'zones' => $zones,
            'effective_employee_formula' => $effective_employee_formula,
            'letters' => $letters,
            'invoice' => $invoice,
        );
        // if($this->redirectUrl){return redirect($this->redirectUrl);}
        return view('dataentry.index')->with($data)->with(['check_rights' => $this->check_employee_rights]);
    }

    public function saveEffectiveEmployees(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'factor_part_time' => 'required',
            'factor_similar' => 'required'
        ]);

        if (!$validator->fails()) {
            $effective_employee_formula = EffectiveEmployeesFormula::all();
            if ($effective_employee_formula->isEmpty()) {
                EffectiveEmployeesFormula::create([
                    'factor_part_time' => $request->factor_part_time,
                    'factor_similar' => $request->factor_similar,
                ]);
            } else {
                EffectiveEmployeesFormula::first()->update([
                    'factor_part_time' => $request->factor_part_time,
                    'factor_similar' => $request->factor_similar
                ]);
            }

            $response = (new ApiMessageController())->saveresponse('Formula Params Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function saveLetterNames(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'rici_name' => 'required',
            'rici_address' => 'required',
            'doc_name_one' => 'required',
            'doc_name_two' => 'required',
        ]);

        if (!$validator->fails()) {
            $letter_names = LetterName::all();
            if ($letter_names->isEmpty()) {
                LetterName::create([
                    'rici_name' => $request->rici_name,
                    'rici_address' => $request->rici_address,
                    'doc_name_one' => $request->doc_name_one,
                    'doc_name_two' => $request->doc_name_two,
                ]);
            } else {
                LetterName::first()->update([
                    'rici_name' => $request->rici_name,
                    'rici_address' => $request->rici_address,
                    'doc_name_one' => $request->doc_name_one,
                    'doc_name_two' => $request->doc_name_two,
                ]);
            }

            $response = (new ApiMessageController())->saveresponse('Letter Names Saved Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*Standard Family*/
    public function saveStandardFamily(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:standards_families',
        ], [
            'name.required' => "Please Enter Standard Family Name",
        ]);

        if (!$validator->fails()) {

            $standardFamily = new StandardsFamily();
            $standardFamily->name = $request->name;
            $standardFamily->is_ims_enabled = $request->is_ims_enabled;
            $standardFamily->save();


            if ($standardFamily) {
                $response = (new ApiMessageController())->saveresponse('Standard Family Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Standard Family. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getStandFamilyList()
    {
        $standardFamilies = StandardsFamily::all();

        return view('dataentry.standard-families-list', compact('standardFamilies'));
    }

    public function deleteStandardFamily($standardFamilyId)
    {

        try {

            $standardFamily = StandardsFamily::findOrFail($standardFamilyId);

            if ($standardFamily->standards->count() === 0) {
                $deleteItem = $standardFamily->delete();
                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Standard Family Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse('Sorry, the system cannot delete this family');
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;


    }

    public function getEditStandardFamilyModal($standardFamilyId)
    {
        try {

            $standardFamily = StandardsFamily::find($standardFamilyId);

            return view('dataentry.edit-standard-family-modal', compact('standardFamily'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateStandardFamily(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'standard_family_id' => 'required|exists:standards_families,id',

        ], [
            'name.required' => "Please Enter Standard Family Name",
        ]);

        if (!$validator->fails()) {

            $standardFamily = StandardsFamily::find($request->standard_family_id);
            $standardFamily->name = $request->name;
            $standardFamily->is_ims_enabled = $request->is_ims_enabled;
            $standardFamily->save();


            if ($standardFamily) {
                $response = (new ApiMessageController())->saveresponse('Standard Family Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Standard Family. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*standards*/
    public function saveStandard(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'standard_number' => 'required',
            'standards_family_id' => 'required|exists:standards_families,id',

        ], [
            'standards_family_id.required' => "Please Select Standard Family",
        ]);

        if (!$validator->fails()) {

            $standardPrefix = explode(':', $request->standard_name);
            $checkStandard = Standards::where('name', 'LIKE', "%" . $standardPrefix[0] . "%")->where('standards_family_id', $request->standards_family_id)->orderBy('id', 'desc')->first();

            $standard = new Standards();
            $standard->standards_family_id = $request->standards_family_id;
            $standard->name = $request->standard_name;
            $standard->number = $request->standard_number;
            $standard->status = $request->status;
            $saveStandard = $standard->save();


            $schemeManager = SchemeManagerStandards::where('standard_family_id', $request->standards_family_id)->first();
            if (!is_null($schemeManager) && isset($schemeManager)) {
                SchemeManagerStandards::create([
                    'scheme_manager_id' => $schemeManager->scheme_manager_id,
                    'standard_family_id' => $request->standards_family_id,
                    'standard_id' => $standard->id
                ]);
            }
            if (isset($checkStandard) && !is_null($checkStandard)) {

                $accreditations = Accreditation::where('standards_family_id', $checkStandard->standards_family_id)->where('standard_id', $checkStandard->id)->get();
                if (!empty($accreditations) && count($accreditations) > 0) {
                    foreach ($accreditations as $accreditation) {
                        $model = new Accreditation();
                        $model->standards_family_id = $standard->standards_family_id;
                        $model->standard_id = $standard->id;
                        $model->full_name = $accreditation->full_name;
                        $model->name = $accreditation->name;
                        $saveModel = $model->save();

                        $iafs = IAF::where('standards_family_id', $checkStandard->standards_family_id)->where('standard_id', $checkStandard->id)->where('accreditation_id', $accreditation->id)->get();
                        if (!empty($iafs) && count($iafs) > 0) {
                            foreach ($iafs as $iaf) {
                                $model1 = new IAF();
                                $model1->code = $iaf->code;
                                $model1->name = $iaf->name;
                                $model1->standard_id = $standard->id;
                                $model1->standards_family_id = $standard->standards_family_id;
                                $model1->accreditation_id = $model->id;
                                $model1->iaf_id = $iaf->iaf_id;
                                $saveModel1 = $model1->save();

                                $iass = IAS::where('standards_family_id', $checkStandard->standards_family_id)->where('standard_id', $checkStandard->id)->where('accreditation_id', $accreditation->id)->where('iaf_id', $iaf->id)->get();
                                if (!empty($iass) && count($iass) > 0) {
                                    foreach ($iass as $ias) {
                                        $model2 = new IAS();
                                        $model2->code = $ias->code;
                                        $model2->name = $ias->name;
                                        $model2->standard_id = $standard->id;
                                        $model2->standards_family_id = $standard->standards_family_id;
                                        $model2->accreditation_id = $model->id;
                                        $model2->iaf_id = $model1->id;
                                        $model2->ias_id = $ias->ias_id;
                                        $saveModel2 = $model2->save();
                                    }
                                }
                            }
                        }


                        $foodCategories = FoodCategory::where('standards_family_id', $checkStandard->standards_family_id)->where('standard_id', $checkStandard->id)->where('accreditation_id', $accreditation->id)->get();
                        if (!empty($foodCategories) && count($foodCategories) > 0) {
                            foreach ($foodCategories as $foodCategory) {
                                $model3 = new FoodCategory();
                                $model3->code = $foodCategory->code;
                                $model3->name = $foodCategory->name;
                                $model3->standards_family_id = $standard->standards_family_id;
                                $model3->standard_id = $standard->id;
                                $model3->accreditation_id = $model->id;
                                $model3->save();

                                $foodSubCategories = FoodSubCategory::where('standards_family_id', $checkStandard->standards_family_id)->where('standard_id', $checkStandard->id)->where('accreditation_id', $accreditation->id)->where('food_category_id', $foodCategory->id)->get();
                                if (!empty($foodSubCategories) && count($foodSubCategories) > 0) {
                                    foreach ($foodSubCategories as $foodSubCategory) {

                                        $model4 = new FoodSubCategory();
                                        $model4->code = $foodSubCategory->code;
                                        $model4->name = $foodSubCategory->name;
                                        $model4->food_category_id = $model3->id;
                                        $model4->standards_family_id = $standard->standards_family_id;
                                        $model4->standard_id = $standard->id;
                                        $model4->accreditation_id = $model->id;
                                        $saveModel4 = $model4->save();
                                    }
                                }


                            }
                        }

                        $energyCategories = EnergyCode::where('standards_family_id', $checkStandard->standards_family_id)->where('standard_id', $checkStandard->id)->where('accreditation_id', $accreditation->id)->get();

                        if (!empty($energyCategories) && count($energyCategories) > 0) {
                            foreach ($energyCategories as $energyCategory) {
                                $model5 = new EnergyCode();
                                $model5->code = $energyCategory->code;
                                $model5->name = $energyCategory->name;
                                $model2->standard_id = $standard->id;
                                $model2->standards_family_id = $standard->standards_family_id;
                                $model2->accreditation_id = $model->id;
                                $saveModel5 = $model5->save();
                            }
                        }

                    }
                }


            }


            if ($standard) {
                $response = (new ApiMessageController())->saveresponse('Standard Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Standard. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getStandsList()
    {
        $standards = Standards::all();

        return view('dataentry.standards-list', compact('standards'));
    }

    public function deleteStandard($modelId)
    {


        try {

            $standard = Standards::findOrFail($modelId);
            if ($standard->scheme_manager->count() == 0 && $standard->auditors->count() == 0 && $standard->company->count() == 0) {
                $deleteItem = $standard->delete();
                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Standard Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }

            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system cannot able to delete the record.");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditStandardModal($modelId)
    {
        try {

            $standard_families = StandardsFamily::all();
            $standard = Standards::find($modelId);

            return view('dataentry.edit-standard-modal', compact('standard', 'standard_families'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateStandard(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'standard_number' => 'required',
            'standards_family_id' => 'required|exists:standards_families,id',
            'standard_id' => 'required|exists:standards,id',

        ], [
            'standard_id.required' => "Please Create Standard First",
        ]);

        if (!$validator->fails()) {

            $standard = Standards::find($request->standard_id);
            $standard->standards_family_id = $request->standards_family_id;
            $standard->name = $request->standard_name;
            $standard->number = $request->standard_number;
            $standard->status = $request->status;
            $updateStandard = $standard->save();

            if ($updateStandard) {
                $response = (new ApiMessageController())->saveresponse('Standard Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Standard. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*accreditation*/
    public function saveAccreditation(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'standards_family_id' => 'required',
            'standard_id' => 'required|integer|exists:standards,id',
            'full_name' => ['required', 'string',
                Rule::unique('accreditations')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard_id)->whereNull('deleted_at');
                })
            ],
            'name' => ['required', 'string',
                Rule::unique('accreditations')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard_id)->whereNull('deleted_at');
                })
            ],


        ]);

        if (!$validator->fails()) {

            $model = new Accreditation();
            $model->standards_family_id = $request->standards_family_id;
            $model->standard_id = $request->standard_id;
            $model->full_name = $request->full_name;
            $model->name = $request->name;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Accreditation Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Accreditation. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getAccreditationList()
    {
        $accreditations = Accreditation::all();

        return view('dataentry.accreditation-list', compact('accreditations'));
    }

    public function deleteAccreditation($modelId)
    {


        try {


            $model = Accreditation::findOrFail($modelId);

            if ($model->iafs->count() == 0) {

                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Accreditation Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system cannot able to delete this record.");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditAccreditationModal($modelId)
    {
        try {

            $accreditation = Accreditation::find($modelId);
            $standards = Standards::all();
            $standard_families = StandardsFamily::orderBy('name')->get();
            $standards_family_id = null;

            if (!is_null($accreditation->standard_id)) {
                $standards_family_id = Standards::find($accreditation->standard_id)->standards_family_id;
            }


            return view('dataentry.edit-accreditation-modal', compact('accreditation', 'standards', 'standard_families', 'standards_family_id'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateAccreditation(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'standard_id' => 'required|integer|exists:standards,id',
            'full_name' => 'required|string',
            'name' => 'required|string',
            'accreditation_id' => 'required|exists:accreditations,id',


        ], [
            'accreditation_id.required' => "Please Create Accreditation First",
        ]);

        if (!$validator->fails()) {

            $model = Accreditation::find($request->accreditation_id);
            $model->standard_id = $request->standard_id;
            $model->full_name = $request->full_name;
            $model->name = $request->name;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Accreditation Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Accreditation. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*iaf codes*/

    public function saveIAF(Request $request)
    {


        $validator = Validator::make($request->all(), [
//            'code'              =>  ['required', 'string',
//                                        Rule::unique('i_a_fs')->where(function($query) use ($request) {
//                                            $query->where('standard_id',$request->standard)->where('accreditation_id',$request->accreditation);
//                                        })
//                                    ],
//            'name' => ['required',
//                Rule::unique('i_a_fs')->where(function ($query) use ($request) {
//                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->where('iaf_id',$request->name);
//                })
//            ],
            'standard' => 'required',
            'standards_family' => 'required',
            'accreditation' => 'required',

        ]);


        if (!$validator->fails()) {

            foreach ($request->name as $id) {

                $iaf = IAF::where('iaf_id', $id)->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->first();
                $data_entry_iaf = DataEntryIAF::where('id', $id)->first();
                if (!empty($iaf) && !is_null($iaf)) {
                    $iaf->code = $data_entry_iaf->code;
                    $iaf->name = $data_entry_iaf->name;
                    $saveModel = $iaf->save();
                } else {
                    $model = new IAF();
                    $model->code = $data_entry_iaf->code;
                    $model->name = $data_entry_iaf->name;
                    $model->standard_id = $request->standard;
                    $model->standards_family_id = $request->standards_family;
                    $model->accreditation_id = $request->accreditation;
                    $model->iaf_id = $data_entry_iaf->id;
                    $saveModel = $model->save();
                }
            }


            if ($saveModel || $iaf) {
                $response = (new ApiMessageController())->saveresponse('IAF Code Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save IAF Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getIAFList()
    {
        $iafs = IAF::all();

        return view('dataentry.iaf-codes-list', compact('iafs'));
    }

    public function deleteIAF($modelId)
    {
        try {

            $model = IAF::findOrFail($modelId);

//            dd($model->ias);
            if ($model->ias->count() == 0 && $model->companies->count() == 0) {
                $deleteItem = $model->delete();
                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("IAF Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system cannot able to delete this record.");
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditIAFModal($modelId)
    {
        try {

            $data['iaf'] = IAF::find($modelId);
            $data['standards'] = Standards::with('standardFamily')->get();
            $data['standard_families'] = StandardsFamily::orderBy('name')->get();
            $data['accreditations'] = Accreditation::orderBy('name')->get();
            $data['iafs_nullable'] = DataEntryIAF::all();


            return view('dataentry.edit-iaf-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateIAF(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'iaf_id' => 'required|exists:i_a_fs,id',
            'standard' => 'required',
            'standards_family' => 'required',
            'accreditation' => 'required',


        ], [
            'iaf_id.required' => "Please Create IAF Code First",
        ]);

        if (!$validator->fails()) {

            $model = IAF::find($request->iaf_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $model->standard_id = $request->standard;
            $model->standards_family_id = $request->standards_family;
            $model->accreditation_id = $request->accreditation;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('IAF Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save IAF. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*ias codes*/

    public function saveIAS(Request $request)
    {


        $validator = Validator::make($request->all(), [
//            'code' => ['required', 'string',
//                Rule::unique('i_a_s')->where(function ($query) use ($request) {
//                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->where('iaf_id', $request->iaf_id);
//                })
//            ],
//            'name' => ['required', 'string',
//                Rule::unique('i_a_s')->where(function ($query) use ($request) {
//                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->where('iaf_id', $request->iaf_id);
//                })
//            ],
            'standard' => 'required',
            'standards_family' => 'required',
            'accreditation' => 'required',
            'iaf_id' => 'required|exists:i_a_fs,id',


        ]);

        if (!$validator->fails()) {


            foreach ($request->name as $id) {

                $ias = IAS::where('ias_id', $id)->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->where('iaf_id', $request->iaf_id)->first();
                $data_entry_ias = DateEntryIAS::where('id', $id)->first();
                if (!empty($ias) && !is_null($ias)) {
                    $ias->code = $data_entry_ias->code;
                    $ias->name = $data_entry_ias->name;
                    $saveModel = $ias->save();
                } else {
                    $model = new IAS();
                    $model->code = $data_entry_ias->code;
                    $model->name = $data_entry_ias->name;
                    $model->standard_id = $request->standard;
                    $model->standards_family_id = $request->standards_family;
                    $model->accreditation_id = $request->accreditation;
                    $model->iaf_id = $request->iaf_id;
                    $model->ias_id = $data_entry_ias->id;
                    $saveModel = $model->save();
                }
            }

            if ($saveModel || $ias) {
                $response = (new ApiMessageController())->saveresponse('IAS Code Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save IAS Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getIASList()
    {
//        $iafs = IAF::all();
        $iafs = IAS::all();

        return view('dataentry.ias-codes-list', compact('iafs'));
    }

    public function getIASCodesList($iaf)
    {
        try {

            $iasCodes = IAS::where('iaf_id', '=', $iaf)->get();

            return view('dataentry.ias-codes-table-list', compact('iasCodes'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function deleteIAS($modelId)
    {
        try {

            $model = IAS::findOrFail($modelId);

            $companiesCodes = CompanyStandardCode::where('ias_id', $model->id)->get();
            $auditorCodes = AuditorStandardCode::where('ias_id', $model->id)->get();

            if (count($companiesCodes) == 0 && count($auditorCodes) == 0) {
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("IAS Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse('Sorry the system can not able to delete this record.');
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditIASModal($modelId)
    {
        try {

            $data['iafs'] = IAF::all();
            $data['ias'] = IAS::find($modelId);
            $data['standards'] = Standards::with('standardFamily')->get();
            $data['standard_families'] = StandardsFamily::orderBy('name')->get();
            $data['accreditations'] = Accreditation::orderBy('name')->get();


            return view('dataentry.edit-ias-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateIAS(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'name' => 'required',
            'iaf_id' => 'required|exists:i_a_fs,id',
            'ias_id' => 'required|exists:i_a_s,id',
            'standard' => 'required',
            'standards_family' => 'required',
            'accreditation' => 'required',

        ]);

        if (!$validator->fails()) {

            $model = IAS::find($request->ias_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $model->iaf_id = $request->iaf_id;
            $model->standard_id = $request->standard;
            $model->standards_family_id = $request->standards_family;
            $model->accreditation_id = $request->accreditation;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('IAS Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save IAS. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*food category codes*/

    public function saveFoodCategory(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'code' => ['required', 'string',
                Rule::unique('food_categories')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation);
                })
            ],
            'name' => ['required', 'string',
                Rule::unique('food_categories')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation);
                })
            ],
            'standards_family' => 'required',
            'standard' => 'required',
            'accreditation' => 'required',
//            'iaf'               => 'required',
//            'ias'               => 'required',

        ]);

        if (!$validator->fails()) {

            $model = new FoodCategory();
            $model->code = $request->code;
            $model->name = $request->name;
            $model->standards_family_id = $request->standards_family;
            $model->standard_id = $request->standard;
            $model->accreditation_id = $request->accreditation;
//            $model->iaf_id              = $request->iaf;
//            $model->ias_id              = $request->ias;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Food Category Code Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Food Category Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getFoodCategoryList()
    {
        $foodCategories = FoodCategory:: orderBy('code', 'asc')->get();

        return view('dataentry.food-category-codes-list', compact('foodCategories'));
    }

    public function deleteFoodCategory($modelId)
    {
        try {

            $model = FoodCategory::findOrFail($modelId);
//            dd($model);
            if ($model->foodsubcategories->count() == 0) {
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Food Category Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system can not able to delete this records");

            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditFoodCategoryModal($modelId)
    {
        try {

            $data['foodCategory'] = FoodCategory::find($modelId);
            $data['standards'] = Standards::with('standardFamily')->get();
            $data['standard_families'] = StandardsFamily::orderBy('name')->get();
            $data['accreditations'] = Accreditation::orderBy('name')->get();
            $data['iafs'] = IAF::orderBy('code')->get();
            $data['iass'] = IAS::orderBy('code')->get();


            return view('dataentry.edit-food-category-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateFoodCategory(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'food_category_id' => 'required|exists:food_categories,id',
            'standards_family' => 'required',
            'standard' => 'required',
            'accreditation' => 'required',
//            'iaf'               => 'required',
//            'ias'               => 'required',


        ], [
            'food_category_id.required' => "Please Create Food Category Code First",
        ]);

        if (!$validator->fails()) {

            $model = FoodCategory::find($request->food_category_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $model->standards_family_id = $request->standards_family;
            $model->standard_id = $request->standard;
            $model->accreditation_id = $request->accreditation;
//            $model->iaf_id              = $request->iaf;
//            $model->ias_id              = $request->ias;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Food Category Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to update Food Category Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*food sub category codes*/

    public function saveFoodSubCategory(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'code' => ['required', 'string',
                Rule::unique('food_sub_categories')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->where('food_category_id', $request->food_category_id);
                })
            ],
            'name' => ['required', 'string',
                Rule::unique('food_sub_categories')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation)->where('food_category_id', $request->food_category_id);
                })
            ],
            'food_category_id' => 'required|exists:food_categories,id',
            'standards_family' => 'required',
            'standard' => 'required',
            'accreditation' => 'required',
//            'iaf'               => 'required',
//            'ias'               => 'required',
        ]);

        if (!$validator->fails()) {

            $model = new FoodSubCategory();
            $model->code = $request->code;
            $model->name = $request->name;
            $model->food_category_id = $request->food_category_id;
            $model->standards_family_id = $request->standards_family;
            $model->standard_id = $request->standard;
            $model->accreditation_id = $request->accreditation;
//            $model->iaf_id              = $request->iaf;
//            $model->ias_id              = $request->ias;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Food Sub Category Code Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Food Sub Category Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getFoodSubCategoryList()
    {
        $foodSubCategories = FoodSubCategory::with('foodCategories')->orderBy('code', 'asc')->get();

        return view('dataentry.food-sub-categories-codes-list', compact('foodSubCategories'));
    }

    public function deleteFoodSubCategory($modelId)
    {
        try {

            $model = FoodSubCategory::findOrFail($modelId);

            $companiesFoodCodes = CompanyStandardFoodCode::where('food_sub_category_id', $model->id)->get();
            $auditorFoodCodes = AuditorStandardFoodCode::where('food_sub_category_code', $model->id)->get();
            if (count($companiesFoodCodes) == 0 && count($auditorFoodCodes) == 0) {
                $deleteItem = $model->delete();
                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Food Sub Category Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse('Sorry the system can not able to delete this record.');
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditFoodSubCategoryModal($modelId)
    {
        try {

            $data['foodCategories'] = FoodCategory::all();
            $data['foodSubCategory'] = FoodSubCategory::find($modelId);
            $data['standards'] = Standards::with('standardFamily')->get();
            $data['standard_families'] = StandardsFamily::orderBy('name')->get();
            $data['accreditations'] = Accreditation::orderBy('name')->get();
            $data['iafs'] = IAF::orderBy('code')->get();
            $data['iass'] = IAS::orderBy('code')->get();


            return view('dataentry.edit-food-sub-category-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateFoodSubCategory(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'name' => 'required',
            'food_sub_category_id' => 'required|exists:food_sub_categories,id',
            'food_category_id' => 'required|exists:food_categories,id',
            'standards_family' => 'required',
            'standard' => 'required',
            'accreditation' => 'required',
//            'iaf'                   => 'required',
//            'ias'                   => 'required',

        ]);

        if (!$validator->fails()) {

            $model = FoodSubCategory::find($request->food_sub_category_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $model->food_category_id = $request->food_category_id;
            $model->standards_family_id = $request->standards_family;
            $model->standard_id = $request->standard;
            $model->accreditation_id = $request->accreditation;
//            $model->iaf_id              = $request->iaf;
//            $model->ias_id              = $request->ias;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Food Sub Category Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to update Food Sub Category. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*energy codes*/

    public function saveEnergyCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'code' => ['required', 'string',
                Rule::unique('energy_codes')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation);
                })
            ],
            'name' => ['required', 'string',
                Rule::unique('energy_codes')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->standard)->where('accreditation_id', $request->accreditation);
                })
            ],
            'standards_family' => 'required',
            'standard' => 'required',
            'accreditation' => 'required',

        ]);

        if (!$validator->fails()) {

            $model = new EnergyCode();
            $model->code = $request->code;
            $model->name = $request->name;
            $model->standards_family_id = $request->standards_family;
            $model->standard_id = $request->standard;
            $model->accreditation_id = $request->accreditation;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Energy Code Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Energy Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getEnergyCodesList()
    {
        $energyCodes = EnergyCode::all();

        return view('dataentry.energy-codes-list', compact('energyCodes'));
    }

    public function deleteEnergyCode($modelId)
    {
        try {

            $model = EnergyCode::findOrFail($modelId);
            $companiesEnergyCodes = CompanyStandardEnergyCode::where('energy_id', $model->id)->get();
            $auditorEnergyCodes = AuditorStandardEnergyCode::where('energy_code', $model->id)->get();


            if (count($companiesEnergyCodes) == 0 && count($auditorEnergyCodes) == 0) {
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Energy Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse('Sorry the system can not able to delete this record.');
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditEnergyCodeModal($modelId)
    {
        try {

            $data['energyCode'] = EnergyCode::find($modelId);
            $data['standards'] = Standards::with('standardFamily')->get();
            $data['standard_families'] = StandardsFamily::orderBy('name')->get();
            $data['accreditations'] = Accreditation::orderBy('name')->get();
            $data['iafs'] = IAF::orderBy('code')->get();
            $data['iass'] = IAS::orderBy('code')->get();


            return view('dataentry.edit-energy-code-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateEnergyCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'energy_code_id' => 'required|exists:energy_codes,id',
            'standards_family' => 'required',
            'standard' => 'required',
            'accreditation' => 'required',
//            'iaf'                   => 'required',
//            'ias'                   => 'required',


        ], [
            'energy_code_id.required' => "Please Create Energy Code First",
        ]);

        if (!$validator->fails()) {

            $model = EnergyCode::find($request->energy_code_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $model->standards_family_id = $request->standards_family;
            $model->standard_id = $request->standard;
            $model->accreditation_id = $request->accreditation;
//            $model->iaf_id              = $request->iaf;
//            $model->ias_id              = $request->ias;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Energy Code Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to update Energy Code. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*country*/

    public function saveCountry(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'phone_code' => 'required'
        ]);

        if (!$validator->fails()) {

            $model = new Countries();
            $model->name = $request->name;
            $model->iso = $request->code;
            $model->phone_code = $request->phone_code;
            $model->currency_name = $request->currency_name;
            $model->currency_code = $request->currency_code;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Country Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save country. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getCountriesList()
    {
        $countries = Countries::orderBy('name')->get();

        return view('dataentry.countries-list', compact('countries'));
    }

    public function deleteCountry($modelId)
    {
        try {

            $model = Countries::findOrFail($modelId);

//            dd([$model->cities->count(),$model->zones->count()]);
            if ($model->cities->count() == 0 && $model->zones->count() == 0) {
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Country Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system can not able to delete this records");

            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditCountryModal($modelId)
    {
        try {

            $country = Countries::find($modelId);

            return view('dataentry.edit-country-modal', compact('country'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateCountry(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'country_id' => 'required|exists:countries,id',
            'phone_code' => 'required',
            'currency_name' => 'required',
            'currency_code' => 'required'


        ], [
            'country_id.required' => "Please Create Country First",
            'phone_code.required' => "Phone Code is Required",
        ]);

        if (!$validator->fails()) {

            $model = Countries::find($request->country_id);
            $model->name = $request->name;
            $model->iso = $request->code;
            $model->phone_code = $request->phone_code;
            $model->currency_name = $request->currency_name;
            $model->currency_code = $request->currency_code;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Country Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to update Country. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*cities*/
    public function saveCity(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'post_code' => 'required',

//            'country_id' => 'required|exists:countries,id',

        ]);

        if (!$validator->fails()) {

            $model = new Cities();

            $model->name = $request->name;
            $model->post_code = $request->post_code;
            $model->zoneable_id = ($request->type == 'zone') ? $request->zone_id : $request->country_id;
            $model->zoneable_type = ($request->type == 'zone') ? Zone::class : Countries::class;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('City Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save city. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getCitiesList(Request $request)
    {
        $cities = Cities::with('zoneable')->get();
//        dd($cities);

//        if ($request->get('search')) {
//
//            // Get keyword
//            $keyword = $request->get('search');
//
//            $cities = Cities::where('name', 'LIKE', '%' .$keyword. '%')->orderBy('name', 'asc')->paginate(50);
//
//        }else{
//
//            // Display cities by default
//            $cities = Cities::with('country')->orderBy('name', 'asc')->paginate(50);
//
//        }
//
//        // Remeber old input
//        Input::flash();

        return view('dataentry.cities-list-view', compact('cities'));
    }

    public function deleteCity($modelId)
    {
        try {

            $model = Cities::findOrFail($modelId);

            $users = User::where('city_id', $model->id)->get();
            $companies = Company::where('city_id', $model->id)->get();
            $auditors = Auditor::where('city_id', $model->id)->get();

            if (count($users) == 0 && count($companies) == 0 && count($auditors) == 0) {
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("City Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system can not able to delete this records");

            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditCityModal($modelId)
    {
        try {

//            $countries = Countries::all();
            $city = Cities::find($modelId);
//            dd($city);
            if ($city->zoneable_type == "App\Zone") {
                $zones = Zone::all();
                return view('dataentry.edit-city-modal', compact('city', 'zones'));
            } else {
                $countries = Countries::all();
                return view('dataentry.edit-city-modal', compact('city', 'countries'));
            }


        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateCity(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'post_code' => 'required',
//            'city_id' => 'required|exists:cities,id',
//            'country_id' => 'required|exists:countries,id',

        ]);

        if (!$validator->fails()) {

            $model = Cities::find($request->city_id);
            $model->name = $request->name;
            $model->post_code = $request->post_code;
            $model->zoneable_id = ($request->type == 'zone') ? $request->zone_id : $request->country_id;
            $model->zoneable_type = ($request->type == 'zone') ? Zone::class : Countries::class;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('City Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to update City. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*regions*/

    public function saveRegion(Request $request)
    {


        $validator = Validator::make($request->all(), [

            'title' => ['required',
                Rule::unique('regions')->where(function ($query) use ($request) {
                    $query->where('title', $request->title)->whereNull('deleted_at');
                })
            ],
//            'title' => 'required|unique:regions',


        ]);

        if (!$validator->fails()) {


            $data = RegionsCountries::whereIn('country_id', $request->country_id)->get();

            if ($data->count() == 0) {
                $model = new Regions();
                $model->title = $request->title;
                $saveModel = $model->save();

                foreach ($request->country_id as $countryId) {
                    $regionCountryModel = new RegionsCountries();
                    $regionCountryModel->region_id = $model->id;
                    $regionCountryModel->country_id = $countryId;
                    $saveRegionCountry = $regionCountryModel->save();
                }
                if ($saveRegionCountry) {
                    $response = (new ApiMessageController())->saveresponse('Region Added Successfully!');
                } else {
                    $response = (new ApiMessageController())->saveresponse('Unable to save Region. Try Again!');
                }

            } else {
                $response = (new ApiMessageController())->failedresponse('Some Country already exist in other regions.');
            }


        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getRegionsList()
    {
        $regions = Regions::with(['regionscountries'])->get();

        return view('dataentry.regions-list', compact('regions'));
    }

    public function deleteRegion($modelId)
    {
        try {

            $model = Regions::findOrFail($modelId);

            $users = User::where('region_id', $model->id)->get();
            $companies = Company::where('region_id', $model->id)->get();
            $auditors = Auditor::where('region_id', $model->id)->get();
            $regionCountries = RegionsCountries::where('region_id', $modelId)->pluck('country_id')->toArray();
            $countries = Countries::whereIn('id', $regionCountries)->get();


            if (count($countries) == 0 && count($users) == 0 && count($companies) == 0 && count($auditors) == 0) {
                RegionsCountries::where('region_id', $modelId)->delete();
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Region Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system can not able to delete this records");

            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditRegionModal($modelId)
    {
        try {


            $countries = Countries::all();
            $region = Regions::where('id', $modelId)->with('regionscountries')->first();


            return view('dataentry.edit-region-modal', compact('countries', 'region'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateRegion(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if (!$validator->fails()) {

            $data = RegionsCountries::where('region_id', '!=', $request->region_id)->whereIn('country_id', $request->country_id)->get();

            if ($data->count() == 0) {
                $model = Regions::find($request->region_id);
                $model->title = $request->name;
                $saveModel = $model->save();


                $model->regionscountries()->sync($request->country_id);


                if ($saveModel) {
                    $response = (new ApiMessageController())->saveresponse('Region Updated Successfully!');
                } else {
                    $response = (new ApiMessageController())->saveresponse('Unable to update Region. Try Again!');
                }

            } else {
                $response = (new ApiMessageController())->failedresponse('Some Country already exist in other regions.');
            }


        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    /*zones*/
    public function saveZone(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:zones',
            'country_id' => 'required|exists:countries,id',

        ]);

        if (!$validator->fails()) {

            $model = new Zone();
            $model->name = $request->name;
            $model->country_id = $request->country_id;
            $saveModel = $model->save();

            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Zones Added Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save Zone. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getZonesList(Request $request)
    {
        $zones = Zone::with('country')->get();

//        if ($request->get('search')) {
//
//            // Get keyword
//            $keyword = $request->get('search');
//
//            $cities = Cities::where('name', 'LIKE', '%' .$keyword. '%')->orderBy('name', 'asc')->paginate(50);
//
//        }else{
//
//            // Display cities by default
//            $cities = Cities::with('country')->orderBy('name', 'asc')->paginate(50);
//
//        }
//
//        // Remeber old input
//        Input::flash();

        return view('dataentry.zones-list-view', compact('zones'));
    }

    public function deleteZone($modelId)
    {
        try {

            $model = Zone::findOrFail($modelId);
            if ($model->cities->count() == 0) {
                $deleteItem = $model->delete();

                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("Zone Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry the system can not able to delete this records");

            }
        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditZoneModal($modelId)
    {
        try {

            $countries = Countries::all();
            $zone = Zone::find($modelId);

            return view('dataentry.edit-zone-modal', compact('zone', 'countries'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateZone(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'zone_id' => 'required|exists:zones,id',
            'country_id' => 'required|exists:countries,id',

        ]);
//        dd($request);
        if (!$validator->fails()) {
//        dd($request->zone_id);
            $model = Zone::find($request->zone_id);
//            dd($model);
            $model->name = $request->name;
            $model->country_id = $request->country_id;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('Zone Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to update Zone. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function invoiceSettings(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'tax_authorities' => 'required',
            'tax_percentage' => 'required',
            'invoice_remarks_one' => 'required',
            'invoice_remarks_two' => 'required',
            'other_user_name' => 'required',
            'other_user_designation' => 'required',
            'other_user_email' => 'required',
            'other_user_phone_number' => 'required',
            'other_details_name' => 'required',
            'other_details_address' => 'required',
            'other_details_email' => 'required',
            'other_details_phone_number' => 'required',
        ]);

        if (!$validator->fails()) {
            $invoiceSettings = InvoiceDataEntry::all();
            if ($invoiceSettings->isEmpty()) {
                InvoiceDataEntry::create([
                    'tax_authorities' => $request->tax_authorities,
                    'tax_percentage' => $request->tax_percentage,
                    'inflation_percentage' => $request->inflation_percentage,
                    'invoice_remarks_one' => $request->invoice_remarks_one,
                    'invoice_remarks_two' => $request->invoice_remarks_two,
                    'other_user_name' => $request->other_user_name,
                    'other_user_designation' => $request->other_user_designation,
                    'other_user_email' => $request->other_user_email,
                    'other_user_phone_number' => $request->other_user_phone_number,
                    'other_details_name' => $request->other_details_name,
                    'other_details_address' => $request->other_details_address,
                    'other_details_email' => $request->other_details_email,
                    'other_details_phone_number' => $request->other_details_phone_number,
                ]);
            } else {
                InvoiceDataEntry::first()->update([
                    'tax_authorities' => $request->tax_authorities,
                    'tax_percentage' => $request->tax_percentage,
                    'inflation_percentage' => $request->inflation_percentage,
                    'invoice_remarks_one' => $request->invoice_remarks_one,
                    'invoice_remarks_two' => $request->invoice_remarks_two,
                    'other_user_name' => $request->other_user_name,
                    'other_user_designation' => $request->other_user_designation,
                    'other_user_email' => $request->other_user_email,
                    'other_user_phone_number' => $request->other_user_phone_number,
                    'other_details_name' => $request->other_details_name,
                    'other_details_address' => $request->other_details_address,
                    'other_details_email' => $request->other_details_email,
                    'other_details_phone_number' => $request->other_details_phone_number,
                ]);
            }

            $response = (new ApiMessageController())->saveresponse('Invoice Settings Saved Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

}
