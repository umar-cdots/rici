<?php

namespace App\Http\Controllers;

use App\Cities;
use App\Countries;
use App\OperationCoordinator;
use App\OperationManager;
use App\RegionalOperationCoordinatorCities;
use App\Regions;
use App\RegionsCountries;
use App\SchemeCoordinator;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\User;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Traits\GeneralHelperTrait;
use App\Traits\AccessRights;

class OperationCoordinatorController extends Controller
{
    use AccessRights;
    use GeneralHelperTrait;

    public function index()
    {
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'scheme_manager') {
            $operation_coordinators = User::where('user_type', 'operation_coordinator')->get();
        } elseif (auth()->user()->user_type == 'operation_manager') {
            $operation_coordinators = User::where('user_type', 'operation_coordinator')->where('region_id', auth()->user()->region_id)->get();
        } elseif (auth()->user()->user_type == 'operation_coordinator') {
            $operation_coordinators = User::where('user_type', 'operation_coordinator')->where('id', auth()->user()->id)->get();
        }

        return view('operation-coordinator.index', compact('operation_coordinators'));
    }

    protected function alreadyTakenCities(): array
    {
        $already_taken = [];

        $regional_operational_coordinator_cities = RegionalOperationCoordinatorCities::all();
        foreach ($regional_operational_coordinator_cities as $regional_operational_coordinator_city) {
            $already_taken[] = $regional_operational_coordinator_city->city_id;
        }

        return $already_taken;
    }

    public function create()
    {
        $standards = Standards::all();
        $countries = Countries::all();
        $scheme_managers = User::where('user_type', 'scheme_manager')->get();
        $operation_managers = User::where('user_type', 'operation_manager')->get();
        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::where('role_id', '3')->get();

        return view('operation-coordinator.create', compact('standards', 'countries', 'scheme_managers', 'roles', 'permissions', 'operation_managers'));
    }

    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
//            'scheme_manager_id' => 'required|integer',
            'operation_manager_id' => 'required|integer',
//            'standard_id' => 'required|integer',
            'username' => ['required', 'string',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('username', $request->username)->whereNull('deleted_at');
                })
            ],
            'email' => ['required', 'string', 'email',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('email', $request->email)->whereNull('deleted_at');
                })
            ],
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
            'remarks' => 'required|string',
            'signature_file' => 'required|mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,xlsx,docx',
            'roles.*' => 'integer',
            'permissions.*' => 'integer',
            'om_cities.0' => 'required|integer'
        ], [
            'om_cities.0.required' => 'City is required'
        ]);
        if (!$validator->fails()) {
            //uploading the images
            if ($request->has('signature_file')) {
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_coordinator/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $signature_file_with_time = $name . '_' . time();
                $destinationPath = public_path('/uploads/operation_coordinator');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;

            } else {
                $signature_file = null;
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_coordinator/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/operation_coordinator');
                $profile_image_time = $name . '_' . time();
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;
            } else {
                $profile_image = null;
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_coordinator/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }


                $destinationPath = public_path('/uploads/operation_coordinator');
                $additional_files_with_time = $name . '_' . time();
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;

            } else {
                $additional_files = null;
            }

            $user = User::create([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'country_id' => $request->country_id,
                'region_id' => $request->om_regions,
                'city_id' => $request->city_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'operation_coordinator'
            ]);
            //add record in scheme manager table
            $operation_coordinator = new OperationCoordinator();
            $operation_coordinator->user_id = $user->id;
            //$operation_coordinator->scheme_manager_id = $request->scheme_manager_id;
            $operation_coordinator->operation_manager_id = $request->operation_manager_id;
            $operation_coordinator->full_name = $request->fullname;
            $operation_coordinator->joining_date = date("Y/m/d", strtotime($request->joining_date));
            // $operation_coordinator->standard_id = $request->standard_id;
            $operation_coordinator->landline = $request->landline;
            $operation_coordinator->signature_image = $signature_file;
            $operation_coordinator->profile_image = $profile_image;
            $operation_coordinator->additional_files = $additional_files;
            $operation_coordinator->save();

            //            //assigning roles and permissions to user
            $role = Role::where('id', 5)->first();
            $user->assignRole($role->name);
            foreach ($role->permissions as $permission) {
                $user->givePermissionTo($permission->name);
            }
            if (!empty($request->om_cities)) {

                foreach ($request->om_cities as $city_id) {
                    RegionalOperationCoordinatorCities::create([
                        'regional_operation_coordinator_id' => $operation_coordinator->id,
                        'city_id' => $city_id
                    ]);
                }

            }


            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Operation Coordinator has been added successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $scheme_managers = User::where('user_type', 'scheme_manager')->get();
        $operation_managers = User::where('user_type', 'operation_manager')->get();
        $countries = Countries::all();
        $standards = Standards::all();
        $roles = Role::all();
        $permissions = Permission::all();
//        $cities = Cities::where('zoneable_id', $user->country_id)->get();
        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
        $regions_cities = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', $user->operation_coordinator->id)->get();
        $user_operation_manager = OperationManager::where('id', OperationCoordinator::where('user_id', $user->id)->first()->operation_manager_id)->first();

        return view('operation-coordinator.show', compact('regions_cities', 'user_operation_manager', 'user', 'countries', 'standards', 'roles', 'permissions', 'cities', 'scheme_managers', 'operation_managers'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $scheme_managers = User::where('user_type', 'scheme_manager')->get();
        $operation_managers = User::where('user_type', 'operation_manager')->get();
        $user_operation_manager = OperationManager::where('id', OperationCoordinator::where('user_id', $user->id)->first()->operation_manager_id)->first();

//        dd($user_operation_manager);
        $countries = Countries::all();
        $standards = Standards::all();
        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::whereIn('role_id', $user->roles->pluck('id')->toArray())->get();
//        $cities = Cities::where('zoneable_id', $user->country_id)->get();

        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
        $regions_cities = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', $user->operation_coordinator->id)->get();
        $regions_cities_not_this_user = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', '!=', $user->operation_coordinator->id)->pluck('city_id');
        return view('operation-coordinator.edit', compact('regions_cities_not_this_user', 'regions_cities', 'user', 'user_operation_manager', 'countries', 'standards', 'roles', 'permissions', 'cities', 'scheme_managers', 'operation_managers'));
    }

    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
//            'scheme_manager_id' => 'required|integer',
            'operation_manager_id' => 'required|integer',
            //  'standard_id' => 'required|integer',
            'username' => 'required|string',
            'email' => 'required|string|email|max:255',
            //'password' => 'required|string|min:6|confirmed',
            'remarks' => 'required|string',
            'signature_file' => 'mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,xlsx,docx',
            'roles.*' => 'integer',
            'permissions.*' => 'integer',
            'om_cities.0' => 'required|integer'
        ], [
            'om_cities.0.required' => 'City is required'
        ]);
        if (!$validator->fails()) {

            $user = User::findOrFail($request->user_id);
            //uploading the images
            if ($request->has('signature_file')) {
                $imageName = $request->file('signature_file')->getClientOriginalName();
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_coordinator/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $signature_file_with_time = $name . '_' . time();
                $destinationPath = public_path('/uploads/operation_coordinator');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;
            } else {
                $signature_file = $user->operation_coordinator['signature_image'];
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_coordinator/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $destinationPath = public_path('/uploads/operation_coordinator');
                $profile_image_time = $name . '_' . time();
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;
            } else {
                $profile_image = $user->operation_coordinator['profile_image'];
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_coordinator/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/operation_coordinator');
                $additional_files_with_time = $name . '_' . time();
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = $user->operation_coordinator['additional_files'];
            }
            if (isset($request->password) && !is_null($request->password)) {
                $password = Hash::make($request->password);
            } else {
                $password = $user->password;
            }

            $user_operation_manager = OperationManager::where('id', $request->operation_manager_id)->first();
            $user->update([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'password' => $password,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'region_id' => ($request->om_regions) ? $request->om_regions : $user_operation_manager->user->region_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'operation_coordinator',
                'is_enabled' => $request->is_enabled == 'true' ? true : false
            ]);

            //add record in scheme manager table
            $user->operation_coordinator->update([
                'full_name' => $request->fullname,
                'joining_date' => date("Y/m/d", strtotime($request->joining_date)),
                'operation_manager_id' => $request->operation_manager_id,
                'landline' => $request->landline,
                'signature_image' => $signature_file,
                'profile_image' => $profile_image,
                'additional_files' => $additional_files
            ]);

            //assigning roles and permissions to user
            if ($request->has('permissions')) {
                $user->syncPermissions($request->permissions);
            }

            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }
            if (!empty($request->om_cities)) {
                RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', $user->operation_coordinator->id)->delete();
                foreach ($request->om_cities as $city_id) {
                    RegionalOperationCoordinatorCities::updateOrCreate([
                        'regional_operation_coordinator_id' => $user->operation_coordinator->id,
                        'city_id' => $city_id
                    ]);
                }

            }
            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Operation Coordinator has been updated successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->operation_coordinator()->delete();
        $user->delete();

        return back()->with(['flash_status' => 'success', 'flash_message' => 'Operation Coordinator has been deleted.']);

    }

    public function getOperationManagerDetails(Request $request)
    {
        $already_taken_cities = $this->alreadyTakenCities();
        $op = OperationManager::findOrFail($request->id);
        $user = User::where('id', $op->user_id)->first();
        $region = Regions::findOrFail($user->region_id);
        $region_countries = RegionsCountries::where('region_id', $region->id)->get();
        $zones = [];
        $country = [];

        foreach ($region_countries as $key => $region_country) {
            $country[] = Countries::where('id', $region_country->country_id)->with('cities')->first();
            if (Zone::where('country_id', $region_country->country_id)->exists()) {
                $zones[] = Zone::where('country_id', $region_country->country_id)->with('cities')->first();
            }


        }

        $data = [
            'status' => 'success',
            'region' => $region,
            'country' => $country,
            'zones' => $zones,
            'already_taken_cities' => $already_taken_cities
        ];

        return response()->json($data);
    }
}
