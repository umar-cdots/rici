<?php

namespace App\Http\Controllers;

use App\AJRemarks;
use App\AjStageChange;
use App\AJStageTeam;
use App\AJStandard;
use App\AJStandardStage;
use App\AuditJustification;
use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandardDocument;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\CompanyStandards;
use App\CompanyStandardsAnswers;
use App\FoodCategory;
use App\Grades;
use App\Notification;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\StandardsQuestions;
use App\Traits\GeneralHelperTrait;
use App\Traits\MandayTrait;
use App\User;
use App\Accreditation;
use App\IAS;
use App\IAF;
use App\FoodSubCategory;
use App\EnergyCode;
use App\OutSourceRequest;
use App\OutSourceRequestAuditor;
use App\OutSourceRequestStandard;
use App\AuditorStandardGrade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class AuditJustificationTransferController extends Controller
{

    use MandayTrait, GeneralHelperTrait;

    /*Transfer Audit Justification*/
    public function transferAJ($company_id = null, $audit_type = null, $frequency = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {

        $ims = 0;
        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);
        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }

        if ($ims != null) {
            //for ims
        } else {

            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();
            $job_number = $this->jobNumber(auth()->user()->country->iso);

            $auditor_grades = Grades::all();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

            //calculate manday of food family,energy family and generic family

            if (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Food') {

                $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->get();

                foreach ($standardQuestions as $index => $questions) {

                    if ($index === 0) {
                        $haccp = $questions->answer;
                    } elseif ($index === 1) {
                        $certified_management = $questions->answer;
                    } elseif ($index === 2) {
                        $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
//                        $food_category_id = FoodCategory::where('code', json_decode($questions->answer))->first()->id;
                        $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
                    }
                }

                $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
                $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;

                $manday = $this->generateFoodSafetyTable($standard_number->id, $food_category_id, $haccp, $company->effective_employees, $certified_management, $company->childCompanies->count() + 1, $c_std->old_cb_surveillance_frequency);

            } elseif (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Energy Management') {
                $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->get();
                foreach ($standardQuestions as $index => $questions) {

                    if ($index === 1) {
                        $energy_consumption_in_tj = $questions->answer;
                    } elseif ($index === 3) {
                        $no_of_energy_resources = $questions->answer;
                    } elseif ($index === 4) {
                        $no_of_significant_energy = $questions->answer;
                    }
                }
                $manday = $this->generateEnergyTable($standard_number->id, $energy_consumption_in_tj, $no_of_energy_resources, $no_of_significant_energy, $company->effective_employees, $c_std->old_cb_surveillance_frequency);

            } else {
                $manday = $this->generateSimpleTable($standard_number->id, $c_std->proposed_complexity, $company->effective_employees, $c_std->old_cb_surveillance_frequency);

            }


            if (empty($manday)) {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Man days not available for this standard');
            } else {


                //making manday table according to manager type
                $calc_manday = [];

                $calc_manday = $manday;

                $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];
                $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();

                if (!empty($scheme_manager_standards)) {

                    if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) { //true

                        if (!empty($company->companyStandards)) {

                            foreach ($company->companyStandards as $standard) {
                                $company_standards[] = $standard->standard_id;
                            }
//                        $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->get();
                            $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->get();


                            if ($auditor_standards->isNotEmpty()) {

                                foreach ($auditor_standards as $auditor_standard) {

                                    foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

//                                        if ($auditor_standard->auditor->is_available == 1) {
//                                            $members[] = $auditor_standard;
//                                            $standard_grades[] = $standard_grade;

                                        $members[] = $auditor_standard;
                                        $standard_grades[] = $standard_grade;

//                                        }
                                    }
                                    foreach ($company->companyIAFs as $key => $companyIaf) {
                                        $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                                    }

                                }

                                //checking the stage1 and stage2 combine aj
                                if ($audit_type == 'stage_1|stage_2') {

                                    $combine_tracker = 1;
                                } else {
                                    $combine_tracker = 0;
                                }

                                //check the company already have any aj
                                $aj_obj = AuditJustification::where('company_id', $company->id)->first();


                                if (!empty($aj_obj) && !is_null($aj_obj)) {

//                                    dd($aj_obj);

                                    //check which standards are in company aj
                                    $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                        ->where('standard_id', $standard_number->id)
                                        ->first();
//                                    dd($aj_standards_obj);

                                    if (!empty($aj_standards_obj) && !is_null($aj_standards_obj)) {
                                        if ($combine_tracker == 1) {
                                            //get the standard stage stage_1
                                            $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', 'stage_1')->first();

                                        } else {

                                            $aj_standards_obj = $aj_standards_obj->ajStandardStages->where('audit_type', $audit_type)->first();

                                        }

//                                        dd(empty($aj_standard_stages_obj));


                                        if (!empty($aj_standard_stages_obj)) {

                                            //now we want the single audit stage which want to edit
                                            $aj_standard_stage = $aj_standard_stages_obj;
                                            //also need all other audit stages for manday editing
                                            foreach ($aj_standards_obj->ajStandardStages as $standardStage) {
                                                $aj_stages_manday[] = $standardStage; // 5 stages array stage 1,2 | Survillance 1,2 | reaudit
                                            }


                                            //return to the edit page
                                            return view('audit-justification.transfer-audit.create', compact('job_number', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                        } else {
//

                                            $audit_justification = AuditJustification::updateOrCreate(
                                                [
                                                    'company_id' => $company->id
                                                ],
                                                [
                                                    'company_id' => $company->id
                                                ]
                                            );
                                            $aj_stardard = $audit_justification->ajStandards()->create(
                                                [
                                                    'standard_id' => $standard_number->id
                                                ]
                                            );


                                            $array = collect(json_decode($c_std->old_cb_audit_activity_dates));
                                            $array1 = array_values((array)end($array));
                                            $lastKey = count($array1) - 1;
                                            $lastKeyValue = $array1[$lastKey];
                                            $transferDate = date('Y-m-d', strtotime($lastKeyValue));

                                            if ($frequency === 'biannual') {
                                                $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($transferDate)));
                                                $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                                $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                                $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                                $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                                $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));

                                                if ($audit_type === 'stage_2') {
                                                    $aj_audit_types = ['stage_2 + Transfer', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];
                                                } elseif ($audit_type === 'surveillance_1') {
                                                    $aj_audit_types = ['surveillance_1 + Transfer', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4];
                                                } elseif ($audit_type === 'surveillance_2') {
                                                    $aj_audit_types = ['surveillance_2 + Transfer', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3];
                                                } elseif ($audit_type === 'surveillance_3') {
                                                    $aj_audit_types = ['surveillance_3 + Transfer', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2];
                                                } elseif ($audit_type === 'surveillance_4') {
                                                    $aj_audit_types = ['surveillance_4 + Transfer', 'surveillance_5', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1];
                                                } elseif ($audit_type === 'surveillance_5') {
                                                    $aj_audit_types = ['surveillance_5 + Transfer', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate];
                                                }
                                            } else {

                                                $effectiveDate = date('Y-m-d', strtotime("+1 year", strtotime($transferDate)));
                                                $effectiveDate1 = date('Y-m-d', strtotime("+1 year", strtotime($effectiveDate)));

                                                if ($audit_type === 'stage_2') {

                                                    $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($transferDate)));
                                                    $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($transferDate)));
                                                    $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($transferDate)));

                                                    $aj_audit_types = ['stage_2 + Transfer', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2];

                                                } elseif ($audit_type === 'surveillance_1') {
                                                    $aj_audit_types = ['surveillance_1 + Transfer', 'surveillance_2', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate, $effectiveDate1];
                                                } elseif ($audit_type === 'surveillance_2') {
                                                    $aj_audit_types = ['surveillance_2 + Transfer', 'reaudit'];
                                                    $due_date = [$transferDate, $effectiveDate];
                                                }

                                            }

                                            $i = 0;
                                            foreach ($aj_audit_types as $key => $aj_audit_type) {
                                                if (preg_match('/Transfer/', $aj_audit_type)) {
                                                    $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                        'audit_type' => $aj_audit_type,
                                                        'onsite' => 0,
                                                        'offsite' => 0,
                                                        'status' => 'Approved',
                                                        'excepted_date' => $due_date[$key],
                                                    ]);
                                                } else {
                                                    $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                        'audit_type' => $aj_audit_type,
                                                        'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                        'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                        'excepted_date' => $due_date[$key],
                                                    ]);
                                                }


                                                if ($i == 0 && ($role == 'operation_manager' || $role == 'operation_coordinator' || $role == 'scheme_coordinator')) {

                                                    $aj_standard_stage = $aj_standard_stages_obj;
                                                    $aj_stages_manday[] = $aj_standard_stages_obj;

                                                } elseif ($role != 'scheme_manger') {
                                                    if ($i == 0) {
                                                        $aj_standard_stage = $aj_standard_stages_obj;
                                                        $aj_stages_manday[] = $aj_standard_stages_obj;
                                                    } else {
                                                        $aj_stages_manday[] = $aj_standard_stages_obj;
                                                    }
                                                }
                                                $i++;
                                            }
                                            return view('audit-justification.transfer-audit.create', compact('job_number', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));
                                        }
                                    } else {
                                        $audit_justification = AuditJustification::updateOrCreate(
                                            [
                                                'company_id' => $company->id
                                            ],
                                            [
                                                'company_id' => $company->id
                                            ]
                                        );
                                        $aj_stardard = $audit_justification->ajStandards()->create(
                                            [
                                                'standard_id' => $standard_number->id
                                            ]
                                        );

                                        $array = collect(json_decode($c_std->old_cb_audit_activity_dates));
                                        $array1 = array_values((array)end($array));
                                        $lastKey = count($array1) - 1;
                                        $lastKeyValue = $array1[$lastKey];
                                        $transferDate = date('Y-m-d', strtotime($lastKeyValue));

                                        if ($frequency === 'biannual') {
                                            $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($transferDate)));
                                            $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                            $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                            $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                            $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                            $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));

                                            if ($audit_type === 'stage_2') {
                                                $aj_audit_types = ['stage_2 + Transfer', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];
                                            } elseif ($audit_type === 'surveillance_1') {
                                                $aj_audit_types = ['surveillance_1 + Transfer', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4];
                                            } elseif ($audit_type === 'surveillance_2') {
                                                $aj_audit_types = ['surveillance_2 + Transfer', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3];
                                            } elseif ($audit_type === 'surveillance_3') {
                                                $aj_audit_types = ['surveillance_3 + Transfer', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2];
                                            } elseif ($audit_type === 'surveillance_4') {
                                                $aj_audit_types = ['surveillance_4 + Transfer', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1];
                                            } elseif ($audit_type === 'surveillance_5') {
                                                $aj_audit_types = ['surveillance_5 + Transfer', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate];
                                            }
                                        } else {

                                            $effectiveDate = date('Y-m-d', strtotime("+1 year", strtotime($transferDate)));
                                            $effectiveDate1 = date('Y-m-d', strtotime("+1 year", strtotime($effectiveDate)));

                                            if ($audit_type === 'stage_2') {

                                                $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($transferDate)));
                                                $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($transferDate)));
                                                $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($transferDate)));

                                                $aj_audit_types = ['stage_2 + Transfer', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2];

                                            } elseif ($audit_type === 'surveillance_1') {
                                                $aj_audit_types = ['surveillance_1 + Transfer', 'surveillance_2', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1];
                                            } elseif ($audit_type === 'surveillance_2') {
                                                $aj_audit_types = ['surveillance_2 + Transfer', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate];
                                            }

                                        }

                                        $i = 0;
                                        foreach ($aj_audit_types as $key => $aj_audit_type) {
                                            if (preg_match('/Transfer/', $aj_audit_type)) {
                                                $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                    'audit_type' => $aj_audit_type,
                                                    'onsite' => 0,
                                                    'offsite' => 0,
                                                    'status' => 'Approved',
                                                    'excepted_date' => $due_date[$key],
                                                ]);
                                            } else {
                                                $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                    'audit_type' => $aj_audit_type,
                                                    'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                    'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                    'excepted_date' => $due_date[$key],
                                                ]);
                                            }

                                            if ($i == 0 && (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                $aj_standard_stage = $aj_standard_stages_obj;
                                                $aj_stages_manday[] = $aj_standard_stages_obj;

                                            } elseif (auth()->user()->user_type != 'scheme_manger' || auth()->user()->user_type == 'admin') {
                                                if ($i == 0) {
                                                    $aj_standard_stage = $aj_standard_stages_obj;
                                                    $aj_stages_manday[] = $aj_standard_stages_obj;
                                                } else {
                                                    $aj_stages_manday[] = $aj_standard_stages_obj;
                                                }
                                            }
                                            $i++;
                                        }

                                        return redirect()->route('company.show', $company->id);


                                    }

                                } else {


                                    $audit_justification = AuditJustification::updateOrCreate(
                                        [
                                            'company_id' => $company->id
                                        ],
                                        [
                                            'company_id' => $company->id
                                        ]
                                    );


                                    if (!$audit_justification->ajStandards->isNotEmpty()) {
                                        $aj_stardard = $audit_justification->ajStandards()->create(
                                            [
                                                'standard_id' => $standard_number->id
                                            ]
                                        );

                                        $array = collect(json_decode($c_std->old_cb_audit_activity_dates));
                                        $array1 = array_values((array)end($array));
                                        $lastKey = count($array1) - 1;
                                        $lastKeyValue = $array1[$lastKey];
                                        $transferDate = date('Y-m-d', strtotime($lastKeyValue));

                                        if ($frequency === 'biannual') {
                                            $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($transferDate)));
                                            $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                            $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                            $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                            $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                            $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));

                                            if ($audit_type === 'stage_2') {
                                                $aj_audit_types = ['stage_2 + Transfer', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];
                                            } elseif ($audit_type === 'surveillance_1') {
                                                $aj_audit_types = ['surveillance_1 + Transfer', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4];
                                            } elseif ($audit_type === 'surveillance_2') {
                                                $aj_audit_types = ['surveillance_2 + Transfer', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3];
                                            } elseif ($audit_type === 'surveillance_3') {
                                                $aj_audit_types = ['surveillance_3 + Transfer', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2];
                                            } elseif ($audit_type === 'surveillance_4') {
                                                $aj_audit_types = ['surveillance_4 + Transfer', 'surveillance_5', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1];
                                            } elseif ($audit_type === 'surveillance_5') {
                                                $aj_audit_types = ['surveillance_5 + Transfer', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate];
                                            }
                                        } else {

                                            $effectiveDate = date('Y-m-d', strtotime("+1 year", strtotime($transferDate)));
                                            $effectiveDate1 = date('Y-m-d', strtotime("+1 year", strtotime($effectiveDate)));

                                            if ($audit_type === 'stage_2') {

                                                $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($transferDate)));
                                                $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($transferDate)));
                                                $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($transferDate)));

                                                $aj_audit_types = ['stage_2 + Transfer', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1, $effectiveDate2];

                                            } elseif ($audit_type === 'surveillance_1') {
                                                $aj_audit_types = ['surveillance_1 + Transfer', 'surveillance_2', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate, $effectiveDate1];
                                            } elseif ($audit_type === 'surveillance_2') {
                                                $aj_audit_types = ['surveillance_2 + Transfer', 'reaudit'];
                                                $due_date = [$transferDate, $effectiveDate];
                                            }

                                        }

                                        $i = 0;
                                        foreach ($aj_audit_types as $key => $aj_audit_type) {
                                            if (preg_match('/Transfer/', $aj_audit_type)) {
                                                $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                    'audit_type' => $aj_audit_type,
                                                    'onsite' => 0,
                                                    'offsite' => 0,
                                                    'status' => 'Approved',
                                                    'excepted_date' => $due_date[$key],
                                                ]);
                                            } else {
                                                $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                    'audit_type' => $aj_audit_type,
                                                    'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                    'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                    'excepted_date' => $due_date[$key],
                                                ]);
                                            }

                                            if ($i == 0 && ($role == 'operation_manager' || $role == 'operation_coordinator' || $role == 'scheme_coordinator')) {

                                                $aj_standard_stage = $aj_standard_stages_obj;
                                                $aj_stages_manday[] = $aj_standard_stages_obj;

                                            } elseif ($role != 'scheme_manger') {
                                                if ($i == 0) {
                                                    $aj_standard_stage = $aj_standard_stages_obj;
                                                    $aj_stages_manday[] = $aj_standard_stages_obj;
                                                } else {
                                                    $aj_stages_manday[] = $aj_standard_stages_obj;
                                                }
                                            }
                                            $i++;
                                        }

                                        return redirect()->route('company.show', $company->id);
                                        ///  return view('audit-justification.transfer-audit.create', compact('standard_codes', 'job_number', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));
                                    }
                                }

                            } else {
                                return back()->with('flash_status', 'warning')
                                    ->with('flash_message', 'Auditor standard not available');
                            }
                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Company does not have this standard');
                        }
                    }
                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Scheme manager not available for this standard');
                }
            }
        }

    }


    public function addTransferAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $audit_type = str_replace(' ', '_', strtolower($audit_type));

        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        }
        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {

            $ims = 0;
            $job_number = $this->jobNumber(auth()->user()->country->iso);

            $standard_number = Standards::where('name', $standard_number)->first();

            $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();


            $auditor_grades = Grades::all();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];
            if (!empty($scheme_manager_standards)) {
                if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                    if (!empty($company->companyStandards)) {

                        foreach ($company->companyStandards as $standard) {
                            $company_standards[] = $standard->standard_id;
                        }

//                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                        $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();
                        if (!empty($auditor_standards)) {
                            foreach ($auditor_standards as $auditor_standard) {
                                foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

//                                    if ($auditor_standard->auditor->is_available == 1) {
                                    $members[] = $auditor_standard;
                                    $standard_grades[] = $standard_grade;

//                                    }
                                }
                                foreach ($company->companyIAFs as $key => $companyIaf) {
                                    $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                                }
                            }
                            //checking the stage1 and stage2 combine aj
                            if ($audit_type == 'stage_1|stage_2') {

                                $combine_tracker = 1;
                            } else {

                                $combine_tracker = 0;
                            }

                            //check the company already have any aj
                            $aj_obj = AuditJustification::where('company_id', $company->id)->first();

                            $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                            $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                            if (!empty($aj_obj)) {
                                if (!empty($c_std)) {
                                    $outSourceRequestStandardId = OutSourceRequestStandard::where('standard_id', $standard_number->id)->where('company_id', $company->id)->pluck('id')->toArray();
                                    $outsourceRequestsId = OutSourceRequest::whereIn('id', $outSourceRequestStandardId)->where('status', 'approved')->where('expiry_status', 'not expired')->pluck('id')->toArray();
                                    $outsourceRequestAuditorId = OutSourceRequestAuditor::whereIn('outsource_request_id', $outsourceRequestsId)->where('status', 'allocated')->pluck('auditor_id')->toArray();
                                    $auditorId = Auditor::whereIn('id', $outsourceRequestAuditorId)->where('auditor_status', 'active')->pluck('id')->toArray();
                                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditorId)->where('standard_id', $standard_number->id)->where('status', 'active')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                                    if ($auditorStandardId) {
                                        $auditors = Auditor::whereIn('id', $auditorStandardId)->where('auditor_status', 'active')->get();
                                        $auditors->each(function ($auditor, $key) use ($auditors, $standard_number) {
                                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $standard_number->id)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                            $auditorStandards = $auditors[$key]->auditorStandards;

                                            $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards) {

                                                $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                            });
                                        });
                                    }
                                }

                                //check which standards are in company aj
                                $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                    ->where('standard_id', $standard_number->id)
                                    ->first();

                                if (!empty($aj_standards_obj)) {
                                    if ($combine_tracker == 1) {
                                        //get the standard stage stage_1
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                    } else {
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    }

                                    if (!empty($aj_standard_stages_obj)) {
                                        //now we want the single audit stage which want to edit
                                        $aj_standard_stage = $aj_standard_stages_obj;
                                        //also need all other audit stages for manday editing


                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
//                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                            $mandays = $aj_standards_obj->ajStandardStages;
//
                                        } else {
                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->where('audit_type', $audit_type)->first();
                                        }

                                        $aj_stages_manday[] = $mandays;

                                        $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();
                                        $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();
//


                                        //return to the edit page
                                        return view('audit-justification.transfer-audit.add', compact('notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                    }
                                }
                            }

                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Auditor standard not available');
                        }
                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Company does not have this standard');
                    }
                }
            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Scheme manager not available for this standard');
            }
        }


    }

    private function in_array_any($needles, $haystack)
    {
        return (bool)array_intersect($needles, $haystack);
        // echo in_array_any( array(3,9), array(5,8,3,1,2) ); // true, since 3 is present
        // echo in_array_any( array(4,9), array(5,8,3,1,2) ); // false, neither 4 nor 9 is present
    }

    private function checkTeam($request)
    {
        if (!empty($request->perposed_team_grade_id) && count($request->perposed_team_grade_id) > 0) {
            $numArray = array_map('intval', $request->perposed_team_grade_id);
            if (in_array(1, $numArray) && in_array(7, $numArray)) {
                //both of them are in $arg

                $standardFamilyCheck = Standards::where('id', $request->standard_number)->first();
                if ($standardFamilyCheck->standards_family_id == 20) {
                    //Energy
                    $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->where('role', 'technical_expert')->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardEnergyCodes = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardEnergyCodes, $companyStandardEnergyCodes)) {
                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } elseif ($standardFamilyCheck->standards_family_id == 23) {
                    //Food
                    $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->where('role', 'technical_expert')->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardFoodCodes = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardFoodCodes, $companyStandardFoodCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } else {
                    //Generic
                    $companyStandardCodes = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->where('role', 'technical_expert')->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardCodes = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardCodes, $companyStandardCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];

                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }

                }
            } else {
                $data = [
                    'status' => 'warning',
                    'message' => 'For Audit LA and TE is required.'
                ];

                $response = $data;
                return $response;
            }


        } else {
            $data = [
                'status' => 'warning',
                'message' => 'Please Select Team Members for Audit.'
            ];

            $response = $data;
            return $response;
        }
    }

    public function editTransferAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $audit_type = str_replace(' ', '_', strtolower($audit_type));


        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        }
        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {

            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();
            $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();

            $auditor_grades = Grades::all();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];
            if (!empty($scheme_manager_standards)) {
                if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                    if (!empty($company->companyStandards)) {

                        foreach ($company->companyStandards as $standard) {
                            $company_standards[] = $standard->standard_id;
                        }

//                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                        $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();
                        if (!empty($auditor_standards)) {
                            foreach ($auditor_standards as $auditor_standard) {
                                foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

                                    // if ($auditor_standard->auditor->is_available == 1) {
                                    $members[] = $auditor_standard;
                                    $standard_grades[] = $standard_grade;

                                    // }
                                }
                                foreach ($company->companyIAFs as $key => $companyIaf) {
                                    $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                                }
                            }
                            //checking the stage1 and stage2 combine aj
                            if ($audit_type == 'stage_1|stage_2') {

                                $combine_tracker = 1;
                            } else {

                                $combine_tracker = 0;
                            }

                            //check the company already have any aj
                            $aj_obj = AuditJustification::where('company_id', $company->id)->first();

                            $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();


                            $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                            if (!empty($aj_obj)) {
                                //check which standards are in company aj
                                $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                    ->where('standard_id', $standard_number->id)
                                    ->first();

                                if (!empty($aj_standards_obj)) {

                                    if (!empty($c_std)) {
                                        $outSourceRequestStandardId = OutSourceRequestStandard::where('standard_id', $standard_number->id)->where('company_id', $company->id)->pluck('id')->toArray();
                                        $outsourceRequestsId = OutSourceRequest::whereIn('id', $outSourceRequestStandardId)->where('status', 'approved')->where('expiry_status', 'not expired')->pluck('id')->toArray();
                                        $outsourceRequestAuditorId = OutSourceRequestAuditor::whereIn('outsource_request_id', $outsourceRequestsId)->where('status', 'allocated')->pluck('auditor_id')->toArray();
                                        $auditorId = Auditor::whereIn('id', $outsourceRequestAuditorId)->where('auditor_status', 'active')->pluck('id')->toArray();
                                        $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditorId)->where('standard_id', $standard_number->id)->where('status', 'active')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                                        if ($auditorStandardId) {
                                            $auditors = Auditor::whereIn('id', $auditorStandardId)->where('auditor_status', 'active')->get();
                                            $auditors->each(function ($auditor, $key) use ($auditors, $standard_number) {
                                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $standard_number->id)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                                $auditorStandards = $auditors[$key]->auditorStandards;

                                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards) {

                                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                                });
                                            });
                                        }
                                    }


                                    if ($combine_tracker == 1) {
                                        //get the standard stage stage_1
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                    } else {
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    }

                                    if (!empty($aj_standard_stages_obj)) {
                                        //now we want the single audit stage which want to edit
                                        $aj_standard_stage = $aj_standard_stages_obj;
                                        //also need all other audit stages for manday editing

                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                            $mandays = $aj_standards_obj->ajStandardStages;

                                        } else {
                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->where('audit_type', $audit_type)->first();
                                        }

                                        $aj_stages_manday[] = $mandays;
                                        $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();

                                        $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();
//
//
                                        //return to the edit page
                                        return view('audit-justification.transfer-audit.edit', compact('notifications', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                    }
                                }
                            }

                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Auditor standard not available');
                        }
                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Company does not have this standard');
                    }
                }
            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Scheme manager not available for this standard');
            }
        }
    }

    public function showTransferAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $audit_type = str_replace(' ', '_', strtolower($audit_type));

        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        }
        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {
            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();


            $auditor_grades = Grades::all();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];

            if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                if (!empty($company->companyStandards)) {

                    foreach ($company->companyStandards as $standard) {
                        $company_standards[] = $standard->standard_id;
                    }
//                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                    $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();
                    if (!empty($auditor_standards)) {
                        foreach ($auditor_standards as $auditor_standard) {
                            foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {
                                $members[] = $auditor_standard;
                                $standard_grades[] = $standard_grade;
                            }
                            foreach ($company->companyIAFs as $key => $companyIaf) {
                                $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                            }
                        }
                        //checking the stage1 and stage2 combine aj
                        if ($audit_type == 'stage_1|stage_2') {

                            $combine_tracker = 1;
                        } else {

                            $combine_tracker = 0;
                        }

                        //check the company already have any aj
                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                        $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                        $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                        if (!empty($aj_obj)) {
                            //check which standards are in company aj
                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                ->where('standard_id', $standard_number->id)
                                ->first();
                            $team_members_stage1 = [];
                            if ($audit_type == 'stage_2') {
                                $aj_standard_stages_obj_stage1 = AJStandardStage::where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standards_obj->id)->first();
                                $team_members_stage1 = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stages_obj_stage1->id)->get();
                            }
                            if (!empty($aj_standards_obj)) {
                                if ($combine_tracker == 1) {
                                    //get the standard stage stage_1
                                    $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                } else {
                                    $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                }

                                if (!empty($aj_standard_stages_obj)) {
                                    //now we want the single audit stage which want to edit
                                    $aj_standard_stage = $aj_standard_stages_obj;
                                    //also need all other audit stages for manday editing


                                    if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                        $mandays = $aj_standards_obj->ajStandardStages;
                                    } else {
                                        $mandays = AJStandardStage::where('id', $aj_standard_id)->where('audit_type', $audit_type)->first();
                                    }

                                    $aj_stages_manday[] = $mandays;
                                    $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();
                                    $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();


                                    $checkApproved = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    if ($checkApproved->status == 'approved') {
                                        $approvalPerson = Notification::where('request_id', $aj_standard_id)->where('status', 'approved')->where('type', 'audit_justification')->first();

                                        $pdf = \Barryvdh\DomPDF\Facade::loadView('audit-justification.AjPrintPreview.index', compact('approvalPerson', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');
                                        Storage::put('public/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf', $pdf->output());
                                        $href = 'storage/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf';
                                    } else {
                                        $href = '#';
                                    }


//
                                    //return to the edit page
                                    return view('audit-justification.transfer-audit.show', compact('href', 'notifications', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                }
                            }
                        }

                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Auditor standard not available');
                    }
                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Company does not have this standard');
                }
            }
        }

    }

    public function updateTransferAJ(Request $request)
    {
        $aj_date = $request->aj_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->aj_date);

        $checkTeamMember = $this->checkTeam($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);
            return $response;
        }

        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'offsite' => 'required|string',
            'job_number' => 'required|string',
//            'manday_remarks' => 'string|sometimes',
            'supervisor_involved.stage1' => 'required|string',
            'expected_date' => Rule::requiredIf(function () use ($request) {
                if ($request['supervisor_involved']['stage1'] == "yes") {
                    return true;
                }
            }),
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'perposed_team_type.*.required' => 'Please select the auditor type',
                'perposedTeam.*.required' => 'Please select the auditor member',
            ]);

        if (!$validator->fails()) {

            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }


            if (!empty($request->perposedTeam)) {
                if (!is_null($request->recycle)) {
                    $standard_stage = AJStandardStage::where('id', (int)$request->standard_stage_id)->where('recycle', (int)$request->recycle)->first();
                } else {
                    $standard_stage = AJStandardStage::where('id', (int)$request->standard_stage_id)->whereNull('recycle')->first();

                }


                if ($standard_stage->job_number != $request->job_number) {

                    $jobNumber = AJStandardStage::where('job_number', $request->job_number)->first();

                    if (!empty($jobNumber)) {
//                        $data = [
//                            'flash_status' => 'warning',
//                            'flash_message' => 'job number already exist'
//                        ];
//                        $response = $data;
//                        return $response;


                        $data = [
                            'status' => 'warning',
                            'message' => 'job number already exist'
                        ];
                        $response = (new ApiMessageController())->successResponse($data, 'job number already exist');
                        return $response;

                    } else {

                        $job_number = DB::table('job_numbers')->where('id', 1)->update([

                            'country_code' => substr($request->job_number, 0, 3),
                            'year' => substr($request->job_number, 3, 2),
                            'code' => substr($request->job_number, 5, 4),
                            'created_at' => date('Y-m-d H:i:s'),

                        ]);
                    }
                }

                $standard_stage->update([

                    'certificate_scope' => $request->certificate_scope,
                    'aj_date' => $aj_date->format('Y-m-d'),
                    'manday_remarks' => $request->manday_remarks,
                    'excepted_date' => $request->expected_date[0],
                    'supervisor_involved' => $request->supervisor_involved['stage1'],
                    'evaluator_name' => $request->evaluator_name,
                    'approval_date' => $request->approval_date,
                    'status' => $request->status,
                    'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                    'job_number' => $request->job_number
                ]);


                if ($request['audit_type'] === 'surveillance_1' || $request['audit_type'] === 'surveillance_2' || $request['audit_type'] === 'surveillance_3' || $request['audit_type'] === 'surveillance_4' || $request['audit_type'] === 'surveillance_5' || $request['audit_type'] === 'reaudit') {
                    $standard_stage->ajStageChange()->updateOrCreate([
                        'name' => $request->ch_name,
                        'scope' => $request->ch_scp,
                        'location' => $request->ch_loc,
                        'version' => $request->ch_std,
                        'standards' => $request->std_number,
                        'remarks' => $request->remarks,
                    ]);
                }


                foreach ($standard_stage->ajStageTeams as $ajStageTeam) {
                    $ajStageTeam->auditor->update([
                        'is_available' => 1
                    ]);
                }

                $standard_stage->ajStageTeams()->delete();


                if (!empty($request->perposedTeam)) {
                    $supervisor = 0;
                    if ($request['supervisor_involved']['stage1'] == "yes") {
                        $supervisor = 1;
                    }
                    foreach ($request->perposedTeam as $key => $a_member) {
                        $standard_stage->ajStageTeams()->create([
                            'auditor_id' => (int)$a_member,
                            'supervisor_involved' => $supervisor,
                            'grade_id' => (int)$request->perposed_team_grade_id[$key],
                            'member_name' => $request->perposed_team_name[$key],
                            'member_type' => $request->perposed_team_type[$key],
                        ]);
                        $auditor = Auditor::find((int)$a_member)->update([
                            'is_available' => 0
                        ]);
                    }

                    if (!is_null($request->recycle)) {
                        $standard_stage = AJStandardStage::where('id', (int)$request->standard_stage_id)->where('recycle', (int)$request->recycle)->first();
                    } else {
                        $standard_stage = AJStandardStage::where('id', (int)$request->standard_stage_id)->whereNull('recycle')->first();

                    }

                    $aj_standard = AJStandard::findOrFail((int)$standard_stage->aj_standard_id);


//                    if ($request->audit_type == 'reaudit' && $request->status == 'approved') {
//
//
//                        $recycle_date = date('Y-m-d', strtotime($request->expected_date[0]));
//
//                        if ($request->frequency === 'biannual') {
//
//                            $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($recycle_date)));
//                            $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
//                            $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
//                            $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
//                            $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
//                            $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
//
//                            $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
//                            $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];
//
//                        } else {
//
//
//                            $effectiveDate = date('Y-m-d', strtotime("+1 year", strtotime($recycle_date)));
//                            $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($recycle_date)));
//                            $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($recycle_date)));
//                            $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
//                            $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];
//
//                        }
//
//                        if ($standard_stage->recycle == null) {
//                            $next_recycle = 1;
//                            $all_standard_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->get();
//                        } else {
//                            $next_recycle = $standard_stage->recycle + 1;
//                            $all_standard_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->get();
//                        }
//
//                        foreach ($aj_audit_types as $key => $aj_audit_type) {
//
//                            foreach ($all_standard_stages as $key1 => $stage) {
//
//                                if ($aj_audit_type == $stage->audit_type) {
//
//                                    $aj_standard_stages_obj = $aj_standard->ajStandardStages()->create([
//                                        'audit_type' => $aj_audit_type,
//                                        'onsite' => $stage->onsite,
//                                        'offsite' => $stage->offsite,
//                                        'excepted_date' => $due_date[$key],
//                                        'recycle' => $next_recycle,
//                                    ]);
//
//                                }
//
//                            }
//                        }
//                    }

                    $data = [
                        'aj_id' => $standard_stage,
                        'status' => 'success',
                    ];

                    $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated');


                }
                return $response;

            } else {
                $data = [
                    'status' => 'warning',
                    'message' => 'Team not available for this standard'
                ];
                $response = (new ApiMessageController())->successResponse($data, 'Team not available for this standard');
                return $response;
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');

            return $response;
        }

    }

    public function updateTransferAJRemarks(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'remark' => 'required|string',
            'aj_id.*' => 'required'
        ]);


        if (!$validator->fails()) {
            $get_company = Company::where('id', $request->company_id)->first();
            foreach ($request['aj_id'] as $aj_id) {

                $aj = AJStandardStage::with('ajStandard.standard.scheme_manager.user')->where('id', $aj_id)->first();
                $aj->AJRemarks()->create([
                    'model' => AJStandardStage::class,
                    'user_id' => Auth()->user()->id,
                    'remarks' => $request['remark']
                ]);
            }


            if (auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'scheme_coordinator') {
                if ($request->status == 'applied') {
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/transfer-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'unapproved',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(New)'

                    ]);
                } elseif ($request->status == 'resent') {
                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/transfer-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'resent',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Re-Submitted)'

                    ]);
                    if (!is_null($notification)) {
                        $notification->is_read = true;
                        $notification->save();
                    }
                }


            } else if (auth()->user()->user_type == 'scheme_manager') {
                if ($request->status == 'rejected') {

                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $notification->sent_by,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/transfer-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'rejected',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Rejected)'

                    ]);
                    $notification->is_read = true;
                    $notification->save();
                } elseif ($request->status == 'approved') {

                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $notification->sent_by,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/transfer-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'approved',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Approved)'

                    ]);
                    $notification->is_read = true;
                    $notification->save();
                }


            }
            $data = [
                'status' => 'success'
            ];

            $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated and notification has been sent.');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function deleteTransferAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {

        $standard_stage = AJStandardStage::find((int)$aj_standard_id);
        $standard_stage->delete();
        $standard_stage->ajStageChange()->delete();
        $standard_stage->AJRemarks()->delete();
        $standard_stage->ajStageTeams()->delete();
        return back()->with(['flash_status' => 'success', 'flash_message' => 'Transfer AJ has been deleted successfully']);
    }


    public function addChangeVersionComapnyStandard(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        $company_standard = CompanyStandards::where('standard_id', $request->standard_id)->where('company_id', $request->company_id)->first();
        if (isset($company_standard) && !is_null($company_standard)) {
            $response['status'] = false;
            $response['message'] = "Your Standard Already used in company";
        } else {
            $company_old_standard = CompanyStandards::where('standard_id', $request->old_standard_id)->where('company_id', $request->company_id)->first();

            $company_standard = new CompanyStandards();
            $company_standard->company_id = $request->company_id;
            $company_standard->standard_id = $request->standard_id;
            $company_standard->client_type = $company_old_standard->client_type;
            if ($company_old_standard->client_type == 'transfered') {
                $company_standard->old_cb_name = $company_old_standard->old_cb_name;
                $company_standard->old_cb_certificate_issue_date = $company_old_standard->old_cb_certificate_issue_date;
                $company_standard->old_cb_certificate_expiry_date = $company_old_standard->old_cb_certificate_expiry_date;
                $company_standard->old_cb_surveillance_frequency = $company_old_standard->old_cb_surveillance_frequency;
                $company_standard->old_cb_audit_activity_stage_id = $company_old_standard->old_cb_audit_activity_stage_id;
                $company_standard->old_cb_audit_activity_dates = $company_old_standard->old_cb_audit_activity_dates;
                $company_standard->old_cb_last_audit_report_doc_media_id = $company_old_standard->old_cb_last_audit_report_doc_media_id;
                $company_standard->old_cb_certificate_copy_doc_media_id = $company_old_standard->old_cb_certificate_copy_doc_media_id;
                $company_standard->old_cb_others_doc_media_id = $company_old_standard->old_cb_others_doc_media_id;
            }
            $company_standard->proposed_complexity = $company_old_standard->proposed_complexity;
            $company_standard->general_remarks = $company_old_standard->general_remarks;
            $company_standard->is_ims = $company_old_standard->is_ims === true ? true : false;
            $company_standard->audit_activity_stage_id = $company_old_standard->audit_activity_stage_id;
            $company_standard->initial_certification_currency = $company_old_standard->initial_certification_currency;//$request->initial_certification_currency;
            $company_standard->initial_certification_amount = $company_old_standard->initial_certification_amount;
            $company_standard->surveillance_currency = $company_old_standard->surveillance_currency;//$request->surveillance_currency;
            $company_standard->surveillance_amount = $company_old_standard->surveillance_amount;
            $company_standard->re_audit_currency = $company_old_standard->re_audit_currency;//$request->re_audit_currency;
            $company_standard->re_audit_amount = $company_old_standard->re_audit_amount ?? '';
            $company_standard->surveillance_frequency = $company_old_standard->surveillance_frequency;
            $company_standard->last_certificate_issue_date = $company_old_standard->last_certificate_issue_date;
            $company_standard->SOA = $company_old_standard->SOA;
            $company_standard->status = 'pending';
            $company_standard->save();
            //Generic Company Standard

            $this->checkStandardCodes($company_old_standard->id, $company_standard->id, $request->company_id, $company_standard->standard_id, $company_standard->standard->standards_family_id);

            $this->checkCompanyStandardDocuments($company_old_standard->id, $company_standard->id, $request->company_id, $company_standard->standard_id, $company_standard->standard->standards_family_id);


            $checkStandard = StandardsQuestions::where('standard_id', $company_standard->standard_id)->get();

            if (count($checkStandard) === 0) {
                $standardQuestions = StandardsQuestions::where('standard_id', $request->old_standard_id)->get();

                if (!empty($standardQuestions) && count($standardQuestions) > 0) {
                    foreach ($standardQuestions as $questions) {


                        $standard_questions = new StandardsQuestions();
                        $standard_questions->standard_id = $company_standard->standard_id;
                        $standard_questions->question = $questions->question;
                        $standard_questions->field_type = $questions->field_type;
                        $standard_questions->default_answer = $questions->default_answer;
                        $standard_questions->options = $questions->options;
                        $standard_questions->min_length = $questions->min_length;
                        $standard_questions->max_length = $questions->max_length;
                        $standard_questions->is_required = $questions->is_required;
                        $standard_questions->has_dependent_fields = $questions->has_dependent_fields;
                        $standard_questions->right_answer_for_dependent_fields = $questions->right_answer_for_dependent_fields;
                        $standard_questions->dependent_fields_ids = $questions->dependent_fields_ids;
                        $standard_questions->is_dependent_field = $questions->is_dependent_field;
                        $standard_questions->sort = $questions->sort;
                        $standard_questions->question_format = $questions->question_format;
                        $standard_questions->company_standards_question_id = $questions->company_standards_question_id;
                        $standard_questions->save();


                        $comapnyStandardQuestions = CompanyStandardsAnswers::where('standard_id', $request->old_standard_id)->where('company_standards_id', $company_old_standard->id)->get();
                        if (!empty($comapnyStandardQuestions) && count($comapnyStandardQuestions) > 0) {
                            foreach ($comapnyStandardQuestions as $comapnyStandardQuestion) {

                                if ($comapnyStandardQuestion->id == $questions->id) {
                                    $company_standard_answer = new CompanyStandardsAnswers;
                                    $company_standard_answer->standard_id = $company_standard->standard_id;
                                    $company_standard_answer->standard_question_id = $standard_questions->id;
                                    $company_standard_answer->company_standards_id = $company_standard->id;
                                    $company_standard_answer->answer = $comapnyStandardQuestion->answer;
                                    $company_standard_answer->save();
                                }


                            }
                        }


                    }

                    //need to check if incase standard question not linked with new standard based on old standard
                    $dependentIdDiscussion = StandardsQuestions::where('question', 'Description')->where('standard_id', $company_standard->standard_id)->first();
                    $excluisonData = StandardsQuestions::where('question', 'Exclusion')->where('standard_id', $company_standard->standard_id)->first();

                    if (!is_null($excluisonData)) {
                        $excluisonData->dependent_fields_ids = $dependentIdDiscussion->id;
                        $excluisonData->save();
                    }

                }
            }

            $response['status'] = true;
            $response['message'] = "New standard Added Successfully.";

        }
        return $response;
    }


    private function checkStandardCodes($company_old_standard, $company_standard_id, $company_id, $standard_id, $standards_family_id)
    {
        //Generic Company Standard
        $checkCodes = CompanyStandardCode::where([
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id
        ])->whereNull('deleted_at')->get();
        if (count($checkCodes) === 0) {

            $companyStandardOldCodes = CompanyStandardCode::where([
                'company_id' => $company_id,
                'company_standard_id' => $company_old_standard
            ])->whereNull('deleted_at')->get();

            if (!empty($companyStandardOldCodes) && count($companyStandardOldCodes) > 0) {
                foreach ($companyStandardOldCodes as $companyStandardOldCode) {

                    $getAllAccreditation = Accreditation::whereStandardId($standard_id)->get(['id', 'name']);
                    $getOldAccreditation = Accreditation::whereId($companyStandardOldCode->accreditation_id)->first(['id', 'name']);

                    $accreditation_id = $companyStandardOldCode->accreditation_id;
                    if (!empty($getAllAccreditation) && count($getAllAccreditation) > 0) {
                        foreach ($getAllAccreditation as $accreditation) {
                            if ($accreditation->name === $getOldAccreditation->name) {
                                $accreditation_id = $accreditation->id;
                                break;
                            }

                        }
                    }
                    $codes = new CompanyStandardCode();
                    $codes->company_id = $companyStandardOldCode->company_id;
                    $codes->company_standard_id = $company_standard_id;
                    $codes->standard_id = $standard_id;
                    $codes->iaf_id = $companyStandardOldCode->iaf_id;
                    $codes->ias_id = $companyStandardOldCode->ias_id;
                    $codes->accreditation_id = $accreditation_id;
                    $codes->unique_code = $companyStandardOldCode->unique_code;
                    $codes->save();
                }
            }
        }


        //food company Standard
        $checkFoodCodes = CompanyStandardFoodCode::where([
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id
        ])->get();
        if (count($checkFoodCodes) === 0) {
            if ($standards_family_id == 23) {
                $companyStandardOldFoodCodes = CompanyStandardFoodCode::where([
                    'company_id' => $company_id,
                    'company_standard_id' => $company_old_standard
                ])->get();


                if (!empty($companyStandardOldFoodCodes) && count($companyStandardOldFoodCodes) > 0) {
                    foreach ($companyStandardOldFoodCodes as $companyStandardOldFoodCode) {
                        $getAllAccreditation = Accreditation::whereStandardId($standard_id)->get(['id', 'name']);
                        $getOldAccreditation = Accreditation::whereId($companyStandardOldFoodCode->accreditation_id)->first(['id', 'name']);

                        $accreditation_id = $companyStandardOldFoodCode->accreditation_id;
                        if (!empty($getAllAccreditation) && count($getAllAccreditation) > 0) {
                            foreach ($getAllAccreditation as $accreditation) {
                                if ($accreditation->name === $getOldAccreditation->name) {
                                    $accreditation_id = $accreditation->id;
                                    break;
                                }

                            }
                        }
                        $codes = new CompanyStandardFoodCode();
                        $codes->company_id = $companyStandardOldFoodCode->company_id;
                        $codes->company_standard_id = $company_standard_id;
                        $codes->standard_id = $standard_id;
                        $codes->food_category_id = $companyStandardOldFoodCode->food_category_id;
                        $codes->food_sub_category_id = $companyStandardOldFoodCode->food_sub_category_id;
                        $codes->accreditation_id = $accreditation_id;
                        $codes->unique_code = $companyStandardOldFoodCode->unique_code;
                        $codes->save();
                    }
                }
            }
        }

        //energy company Standard
        $checkEnergyCodes = CompanyStandardEnergyCode::where([
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id
        ])->get();
        if (count($checkEnergyCodes) === 0) {
            if ($standards_family_id == 20) {
                $companyStandardOldEnergyCodes = CompanyStandardEnergyCode::where([
                    'company_id' => $company_id,
                    'company_standard_id' => $company_old_standard
                ])->get();
                if (!empty($companyStandardOldEnergyCodes) && count($companyStandardOldEnergyCodes) > 0) {
                    foreach ($companyStandardOldEnergyCodes as $companyStandardOldEnergyCode) {
                        $getAllAccreditation = Accreditation::whereStandardId($standard_id)->get(['id', 'name']);
                        $getOldAccreditation = Accreditation::whereId($companyStandardOldEnergyCode->accreditation_id)->first(['id', 'name']);

                        $accreditation_id = $companyStandardOldEnergyCode->accreditation_id;
                        if (!empty($getAllAccreditation) && count($getAllAccreditation) > 0) {
                            foreach ($getAllAccreditation as $accreditation) {
                                if ($accreditation->name === $getOldAccreditation->name) {
                                    $accreditation_id = $accreditation->id;
                                    break;
                                }

                            }
                        }
                        $codes = new CompanyStandardEnergyCode();
                        $codes->company_id = $companyStandardOldEnergyCode->company_id;
                        $codes->company_standard_id = $company_standard_id;
                        $codes->standard_id = $standard_id;
                        $codes->energy_id = $companyStandardOldEnergyCode->energy_id;
                        $codes->accreditation_id = $accreditation_id;
                        $codes->unique_code = $companyStandardOldEnergyCode->unique_code;
                        $codes->save();
                    }
                }
            }
        }


//        $newStandardAccreditation = Accreditation::where('standard_id', $company_standard->standard_id)->get();
//        if ($newStandardAccreditation->count() > 0) {
//            foreach ($newStandardAccreditation as $accreditation) {
//                $newStandardIafCode = IAF::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $accreditation->id)->get();
//                if ($newStandardIafCode->count() > 0) {
//                    foreach ($newStandardIafCode as $iaf) {
//                        $newStandardIasCode = IAS::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $accreditation->id)->where('iaf_id', $iaf->id)->get();
//                        if ($newStandardIasCode->count() > 0) {
//                            foreach ($newStandardIasCode as $ias) {
//                                $codes = new CompanyStandardCode();
//                                $codes->company_id = $company_standard->company_id;
//                                $codes->company_standard_id = $company_standard->id;
//                                $codes->standard_id = $company_standard->standard_id;
//                                $codes->iaf_id = $iaf->id;
//                                $codes->ias_id = $ias->id;
//                                $codes->accreditation_id = $accreditation->id;
//                                $codes->unique_code = $accreditation->name . "/" . $iaf->code . "/" . $ias->code;
//                                $codes->save();
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        if ($company_standard->standard->standards_family_id == 23) {
//            //food company Standard
//
//            $newStandardFoodAccreditation = Accreditation::where('standard_id', $company_standard->standard_id)->get();
//            if ($newStandardFoodAccreditation->count() > 0) {
//                foreach ($newStandardFoodAccreditation as $food_accreditation) {
//                    $newStandardFoodCategory = FoodCategory::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $food_accreditation->id)->get();
//                    if ($newStandardFoodCategory->count() > 0) {
//                        foreach ($newStandardFoodCategory as $food_category) {
//                            $newStandardFoodSubCategory = FoodSubCategory::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $food_accreditation->id)->where('food_category_id', $food_category->id)->get();
//                            if ($newStandardFoodSubCategory->count() > 0) {
//                                foreach ($newStandardFoodSubCategory as $food_sub_category) {
//                                    $codes = new CompanyStandardFoodCode();
//                                    $codes->company_id = $company_standard->company_id;
//                                    $codes->company_standard_id = $company_standard->id;
//                                    $codes->standard_id = $company_standard->standard_id;
//                                    $codes->food_category_id = $food_category->id;
//                                    $codes->food_sub_category_id = $food_sub_category->id;
//                                    $codes->accreditation_id = $food_accreditation->id;
//                                    $codes->unique_code = $food_accreditation->name . "/" . $food_category->code . "/" . $food_sub_category->code;
//                                    $codes->save();
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        if ($company_standard->standard->standards_family_id == 20) {
//            //energy Standard
//
//            $newStandardEnergyAccreditation = Accreditation::where('standard_id', $company_standard->standard_id)->get();
//            if ($newStandardEnergyAccreditation->count() > 0) {
//                foreach ($newStandardEnergyAccreditation as $energy_accreditation) {
//                    $newStandardEnergyCategory = EnergyCode::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $energy_accreditation->id)->get();
//                    if ($newStandardEnergyCategory->count() > 0) {
//                        foreach ($newStandardEnergyCategory as $energy_category) {
//
//                            $codes = new CompanyStandardEnergyCode();
//                            $codes->company_id = $company_standard->company_id;
//                            $codes->company_standard_id = $company_standard->id;
//                            $codes->standard_id = $company_standard->standard_id;
//                            $codes->energy_id = $energy_category->id;
//                            $codes->accreditation_id = $energy_accreditation->id;
//                            $codes->unique_code = $energy_accreditation->name . "/" . $energy_category->code;
//                            $codes->save();
//                        }
//                    }
//                }
//            }
//        }

    }

    private function checkCompanyStandardDocuments($old_company_standard_id, $company_standard_id, $company_id, $standard_id, $standards_family_id)
    {

        //initial_inquiry_form
        $check_initial_inquiry_form = CompanyStandardDocument::where([
            'company_standard_id' => $company_standard_id,
            'document_type' => 'initial_inquiry_form'
        ])->whereNull('deleted_at')->get();

        if (count($check_initial_inquiry_form) === 0) {
            $initial_inquiry_forms = CompanyStandardDocument::where([
                'company_standard_id' => $old_company_standard_id,
                'document_type' => 'initial_inquiry_form'
            ])->whereNull('deleted_at')->get();
            if (!empty($initial_inquiry_forms) && count($initial_inquiry_forms) > 0) {
                foreach ($initial_inquiry_forms as $initial_inquiry_form) {

                    CompanyStandardDocument::create([
                        'company_standard_id' => $company_standard_id,
                        'media_id' => $initial_inquiry_form->media_id,
                        'date' => $initial_inquiry_form->date,
                        'note' => $initial_inquiry_form->note,
                        'document_type' => $initial_inquiry_form->document_type,
                    ]);
                }
            }
        }

        //proposal
        $check_proposal = CompanyStandardDocument::where([
            'company_standard_id' => $company_standard_id,
            'document_type' => 'proposal'
        ])->whereNull('deleted_at')->get();

        if (count($check_proposal) === 0) {
            $proposals = CompanyStandardDocument::where([
                'company_standard_id' => $old_company_standard_id,
                'document_type' => 'proposal'
            ])->whereNull('deleted_at')->get();
            if (!empty($proposals) && count($proposals) > 0) {
                foreach ($proposals as $proposal) {

                    CompanyStandardDocument::create([
                        'company_standard_id' => $company_standard_id,
                        'media_id' => $proposal->media_id,
                        'date' => $proposal->date,
                        'note' => $proposal->note,
                        'document_type' => $proposal->document_type,
                    ]);
                }
            }
        }

        //contract
        $check_contract = CompanyStandardDocument::where([
            'company_standard_id' => $company_standard_id,
            'document_type' => 'contract'
        ])->whereNull('deleted_at')->get();

        if (count($check_contract) === 0) {
            $contracts = CompanyStandardDocument::where([
                'company_standard_id' => $old_company_standard_id,
                'document_type' => 'contract'
            ])->whereNull('deleted_at')->get();
            if (!empty($contracts) && count($contracts) > 0) {
                foreach ($contracts as $contract) {

                    CompanyStandardDocument::create([
                        'company_standard_id' => $company_standard_id,
                        'media_id' => $contract->media_id,
                        'date' => $contract->date,
                        'note' => $contract->note,
                        'document_type' => $contract->document_type,
                    ]);
                }
            }
        }

        //other
        $check_other = CompanyStandardDocument::where([
            'company_standard_id' => $company_standard_id,
            'document_type' => 'other'
        ])->whereNull('deleted_at')->get();

        if (count($check_other) === 0) {
            $others = CompanyStandardDocument::where([
                'company_standard_id' => $old_company_standard_id,
                'document_type' => 'other'
            ])->whereNull('deleted_at')->get();
            if (!empty($others) && count($others) > 0) {
                foreach ($others as $other) {

                    CompanyStandardDocument::create([
                        'company_standard_id' => $company_standard_id,
                        'media_id' => $other->media_id,
                        'date' => $other->date,
                        'note' => $other->note,
                        'document_type' => $other->document_type,
                    ]);
                }
            }
        }


    }
}
