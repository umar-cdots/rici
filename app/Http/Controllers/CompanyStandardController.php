<?php

namespace App\Http\Controllers;

use App\Accreditation;

use App\AuditorStandardEnergyCode;

use App\CompanyStandardCode;

use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\EnergyCode;
use App\FoodCategory;
use App\FoodSubCategory;
use App\Grades;
use App\IAF;
use App\IAS;
use App\Standards;
use App\StandardsFamily;
use App\CompanyStandards;
use App\ImsStandard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PhpParser\PrettyPrinter\Standard;

class CompanyStandardController extends Controller
{


    //    Generic Standard Code

    public function companyStandardCodesView(Request $request)
    {
        $data['companyStandardCodes'] = array();
        $data['standards'] = Standards::orderBy('name')->get();
        $data['accreditations'] = Accreditation::all();
        $data['iafs'] = IAF::orderBy('code')->get();
        $data['iass'] = IAS::orderBy('code')->get();
        $data['standard_id'] = $request->standard_id;
        $data['company_id'] = $request->company_id;


        if (!empty($request->standard_id)) {
            $data['companyStandardCodes'] = CompanyStandardCode::where('standard_id', $request->standard_id)->where('company_id', $request->company_id)->with(['ias', 'iaf'])->get();
        }


        return view('company.company_standards_codes.company-standards-codes', $data);
    }

    public function saveCompanyStandardCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required|exists:standards,id',
            'company_id' => 'required|exists:companies,id',
            'accreditation_id' => 'required|exists:accreditations,id',
            'iaf_id' => 'required|exists:i_a_fs,id',
            'ias_id.0' => 'required|exists:i_a_s,id',

        ]);


//        dd($request->ias_id);

        if (!$validator->fails()) {

            $data = CompanyStandardCode::where('standard_id', $request->company_standard_id)->where('company_id', $request->company_id)->where('iaf_id', $request->iaf_id)->whereIn('ias_id', $request->ias_id)->get();
            if ($data->count() > 0) {
                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {


                foreach ($request->ias_id as $ias) {
                    $iaf_name = IAF::where('id', $request->iaf_id)->first()->code;
                    $ias_name = IAS::where('id', $ias)->first()->code;
                    $accreditation_name = Accreditation::where('id', $request->accreditation_id)->first()->name;

                    $unique_code = $accreditation_name . "/" . $iaf_name . "/" . $ias_name;

                    $companyStandardCode = new CompanyStandardCode();
                    $companyStandardCode->standard_id = $request->company_standard_id;
                    $companyStandardCode->company_id = $request->company_id;
                    $companyStandardCode->iaf_id = $request->iaf_id;
                    $companyStandardCode->ias_id = $ias;
                    $companyStandardCode->accreditation_id = $request->accreditation_id;
                    $companyStandardCode->unique_code = $unique_code;

                    $companyStandardCode->save();
                }


//                if ($companyStandardCode)

                $data = [
                    'standard_id' => $request->company_standard_id,
                    'company_id' => $request->company_id,
                ];

                $response = (new ApiMessageController())->successResponse($data, 'Company Standard Code Added Successfully!');
            }
        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getEditCodesModal($modelId)
    {
        try {


            $companyStandardCodes = CompanyStandardCode::find($modelId);

            $allAccreditations = Accreditation::where('standard_id', $companyStandardCodes->standard_id)->get();
            $allAccreditation_ids = Accreditation::where('standard_id', $companyStandardCodes->standard_id)->pluck('id')->toArray();
            $iafs = IAF::whereIn('accreditation_id', $allAccreditation_ids)->orderBy('code')->get();
            $iafs_ids = IAF::whereIn('accreditation_id', $allAccreditation_ids)->pluck('id')->toArray();
            $iass = IAS::whereIn('iaf_id', $iafs_ids)->orderBy('code')->get();

            return view('company.company_standards_codes.edit-company-standards-codes-modal', compact('iafs', 'iass', 'companyStandardCodes', 'allAccreditations'));


//
//            $data['standards'] = Standards::orderBy('name')->get();
//            $data['accreditations'] = Accreditation::all();
//            $data['iafs'] = IAF::orderBy('code')->get();
//            $data['iass'] = IAS::orderBy('code')->get();
//            $data['auditorStandardCode'] = AuditorStandardCode::find($modelId);
//            $data['standardId'] = AuditorStandard::whereId($data['auditorStandardCode']->auditor_standard_id)->first()->standard_id;
//            return view('auditors.auditor-standards.edit-codes-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateCompanyStandardCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'company_standard_code_id' => 'required|exists:company_standard_codes,id',
            'company_standard_id' => 'required|exists:standards,id',
            'company_id' => 'required|exists:companies,id',
            'accreditation_id' => 'required|exists:accreditations,id',
            'iaf_id' => 'required|exists:i_a_fs,id',
            'ias_id' => 'required|exists:i_a_s,id',
        ], [
            /* 'auditor_standard_id.required' => "Add Auditor Standard First",
             'auditor_standard_id.exists' => "Auditor Standard Does Not Exist",
             'document_file.file' => "Document Must be a file!",*/
        ]);

        if (!$validator->fails()) {

            $iaf_name = IAF::where('id', $request->iaf_id)->first()->code;
            $ias_name = IAS::where('id', $request->ias_id)->first()->code;
            $accreditation_name = Accreditation::where('id', $request->accreditation_id)->first()->name;

            $unique_code = $accreditation_name . "/" . $iaf_name . "/" . $ias_name;
            $companyStandardCode = CompanyStandardCode::find($request->company_standard_code_id);
            $companyStandardCode->standard_id = $request->company_standard_id;
            $companyStandardCode->company_id = $request->company_id;
            $companyStandardCode->iaf_id = $request->iaf_id;
            $companyStandardCode->ias_id = $request->ias_id;
            $companyStandardCode->accreditation_id = $request->accreditation_id;
            $companyStandardCode->unique_code = $unique_code;

            $companyStandardCode->save();


            if ($companyStandardCode)

                $data = [
                    'standard_id' => $companyStandardCode->standard_id,
                    'company_id' => $companyStandardCode->company_id,

                ];

            $response = (new ApiMessageController())->successResponse($data, 'Company Standard Code Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteCompanyStandardCode($modelId)
    {

        try {
            $companyStandardCode = CompanyStandardCode::findOrFail($modelId);
            $deleteItem = $companyStandardCode->delete();

            if ($deleteItem) {
                $data = [
                    'standard_id' => $companyStandardCode->standard_id,
                    'company_id' => $companyStandardCode->company_id,
                ];

                $response = (new ApiMessageController())->successResponse($data, "Company Standard Code Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }


    //    Food Standard Code

    public function companyStandardFoodCodesView(Request $request)
    {


        $data['companyStandardFoodCodes'] = array();
        $data['accreditations'] = Accreditation::all();
        $data['standard_id'] = $request->standard_id;
        $data['company_id'] = $request->company_id;

        if (!empty($request->standard_id)) {

            $data['companyStandardFoodCodes'] = CompanyStandardFoodCode::where('standard_id', $request->standard_id)->where('company_id', $request->company_id)->with(['foodcategory', 'foodsubcategory'])->get();
        }


        return view('company.company_standards_food_codes.company-standards-food-codes', $data);
    }

    public function saveCompanyStandardFoodCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required|exists:standards,id',
            'company_id' => 'required|exists:companies,id',
            'food_category_code' => 'required|exists:food_categories,id',
            'food_sub_category_code' => 'required|exists:food_sub_categories,id',
            'food_accreditation_id' => 'required|exists:accreditations,id',
        ]);

        if (!$validator->fails()) {

            $data = CompanyStandardFoodCode::where('standard_id', $request->company_standard_id)->where('company_id', $request->company_id)->where('food_category_id', $request->food_category_code)->where('food_sub_category_id', $request->food_sub_category_code)->get();
            if ($data->count() > 0) {
                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {
                $food_name = FoodCategory::where('id', $request->food_category_code)->first()->code;
                $food_sub_name = FoodSubCategory::where('id', $request->food_sub_category_code)->first()->code;
                $accreditation_name = Accreditation::where('id', $request->food_accreditation_id)->first()->name;

                $unique_code = $accreditation_name . "/" . $food_name . "/" . $food_sub_name;
                $companyStandardFoodCode = new CompanyStandardFoodCode();
                $companyStandardFoodCode->standard_id = $request->company_standard_id;
                $companyStandardFoodCode->company_id = $request->company_id;
                $companyStandardFoodCode->food_category_id = $request->food_category_code;
                $companyStandardFoodCode->food_sub_category_id = $request->food_sub_category_code;
                $companyStandardFoodCode->accreditation_id = $request->food_accreditation_id;
                $companyStandardFoodCode->unique_code = $unique_code;
                $companyStandardFoodCode->save();


                if ($companyStandardFoodCode)

                    $data = [
                        'standard_id' => $request->company_standard_id,
                        'company_id' => $request->company_id,
                    ];

                $response = (new ApiMessageController())->successResponse($data, 'Company Standard Food Code Added Successfully!');
            }
        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteCompanyStandardFoodCode($modelId)
    {

        try {
            $model = CompanyStandardFoodCode::findOrFail($modelId);
            $deleteItem = $model->delete();

            if ($deleteItem) {
                $data = [
                    'standard_id' => $model->standard_id,
                    'company_id' => $model->company_id,
                ];

                $response = (new ApiMessageController())->successResponse($data, "Company Standard Food Code Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditFoodCodesModal($modelId)
    {
        try {

            $companyStandardFoodCodes = CompanyStandardFoodCode::with(['foodcategory', 'foodsubcategory'])->find($modelId);

            $codesAccreditations_id = CompanyStandardCode::where('company_id', $companyStandardFoodCodes->company_id)->where('standard_id', $companyStandardFoodCodes->standard_id)->pluck('accreditation_id')->toArray();

            $allAccreditations = Accreditation::whereIn('id', $codesAccreditations_id)->get();
            $allAccreditation_ids = Accreditation::where('standard_id', $companyStandardFoodCodes->standard_id)->pluck('id')->toArray();
            $foodCategories = FoodCategory::whereIn('accreditation_id', $codesAccreditations_id)->orderBy('code')->get();
            $foodCategory_ids = FoodCategory::whereIn('accreditation_id', $codesAccreditations_id)->pluck('id')->toArray();
            $foodSubCategories = FoodSubCategory::whereIn('food_category_id', $foodCategory_ids)->orderBy('code')->get();

            return view('company.company_standards_food_codes.edit-company-food-codes-modal', compact('foodCategories', 'foodSubCategories', 'companyStandardFoodCodes', 'allAccreditations'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateCompanyStandardFoodCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'company_standard_food_code_id' => 'required|exists:company_standard_food_codes,id',
            'company_standard_id' => 'required|exists:standards,id',
            'company_id' => 'required|exists:companies,id',
            'food_category_code' => 'required|exists:food_categories,id',
            'food_sub_category_code' => 'required|exists:food_sub_categories,id',
            'food_accreditation_id' => 'required|exists:accreditations,id',
        ]);

        if (!$validator->fails()) {


            $companyStandardFoodCode = CompanyStandardFoodCode::find($request->company_standard_food_code_id);

            $food_name = FoodCategory::where('id', $request->food_category_code)->first()->code;
            $food_sub_name = FoodSubCategory::where('id', $request->food_sub_category_code)->first()->code;
            $accreditation_name = Accreditation::where('id', $request->food_accreditation_id)->first()->name;

            $unique_code = $accreditation_name . "/" . $food_name . "/" . $food_sub_name;


            $companyStandardFoodCode->standard_id = $request->company_standard_id;
            $companyStandardFoodCode->company_id = $request->company_id;
            $companyStandardFoodCode->food_category_id = $request->food_category_code;
            $companyStandardFoodCode->food_sub_category_id = $request->food_sub_category_code;
            $companyStandardFoodCode->accreditation_id = $request->food_accreditation_id;
            $companyStandardFoodCode->unique_code = $unique_code;

            $companyStandardFoodCode->save();


            if ($companyStandardFoodCode)

                $data = [
                    'standard_id' => $companyStandardFoodCode->standard_id,
                    'company_id' => $companyStandardFoodCode->company_id,

                ];

            $response = (new ApiMessageController())->successResponse($data, 'Company Standard Food Code Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    //    Energy Standard Code

    public function companyStandardEnergyCodesView(Request $request)
    {
        $data['companyStandardEnergyCodes'] = array();
        $data['accreditations'] = Accreditation::all();
        $data['standard_id'] = $request->standard_id;
        $data['company_id'] = $request->company_id;

        if (!empty($request->standard_id)) {

            $data['companyStandardEnergyCodes'] = CompanyStandardEnergyCode::where('standard_id', $request->standard_id)->where('company_id', $request->company_id)->with(['energycode', 'accreditation'])->get();
        }


        return view('company.company_standards_energy_codes.company-standards-energy-codes', $data);

    }

    public function saveCompanyStandardEnergyCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required|exists:standards,id',
            'company_id' => 'required|exists:companies,id',
            'energy_code' => 'required|exists:energy_codes,id',
            'energy_accreditation_id' => 'required',
        ]);


        if (!$validator->fails()) {
            $data = CompanyStandardEnergyCode::where('standard_id', $request->company_standard_id)->where('company_id', $request->company_id)->where('energy_id', $request->energy_code)->get();
            if ($data->count() > 0) {
                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {

                $energy_name = EnergyCode::where('id', $request->energy_code)->first()->code;
                $accreditation_name = Accreditation::where('id', $request->energy_accreditation_id)->first()->name;

                $unique_code = $accreditation_name . "/" . $energy_name;

                $companyStandardEnergyCode = new CompanyStandardEnergyCode();
                $companyStandardEnergyCode->standard_id = $request->company_standard_id;
                $companyStandardEnergyCode->company_id = $request->company_id;
                $companyStandardEnergyCode->energy_id = $request->energy_code;
                $companyStandardEnergyCode->accreditation_id = $request->energy_accreditation_id;
                $companyStandardEnergyCode->unique_code = $unique_code;
                $companyStandardEnergyCode->save();


                if ($companyStandardEnergyCode)
                    $data = [
                        'standard_id' => $request->company_standard_id,
                        'company_id' => $request->company_id,
                    ];


                $response = (new ApiMessageController())->successResponse($data, 'Company Standard Energy Code Added Successfully!');
            }
        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteCompanyStandardEnergyCode($modelId)
    {

        try {
            $model = CompanyStandardEnergyCode::findOrFail($modelId);
            $deleteItem = $model->delete();

            if ($deleteItem) {
                $data = [
                    'standard_id' => $model->standard_id,
                    'company_id' => $model->company_id,
                ];

                $response = (new ApiMessageController())->successResponse($data, "Company Standard Energy Code Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditEnergyCodesModal($modelId)
    {
        try {


            $companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['energycode'])->find($modelId);
            $codesAccreditations_id = CompanyStandardCode::where('company_id', $companyStandardEnergyCodes->company_id)->where('standard_id', $companyStandardEnergyCodes->standard_id)->pluck('accreditation_id')->toArray();

            $allAccreditations = Accreditation::whereIn('standard_id', $codesAccreditations_id)->get();
            $allAccreditation_ids = Accreditation::where('standard_id', $companyStandardEnergyCodes->standard_id)->pluck('id')->toArray();
            $energyCodes = EnergyCode::whereIn('accreditation_id', $codesAccreditations_id)->orderBy('code')->get();

            return view('company.company_standards_energy_codes.edit-company-energy-codes-modal', compact('energyCodes', 'companyStandardEnergyCodes', 'allAccreditations'));


        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateCompanyStandardEnergyCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'company_standard_energy_code_id' => 'required|exists:company_standard_energy_codes,id',
            'company_standard_id' => 'required|exists:standards,id',
            'company_id' => 'required|exists:companies,id',
            'energy_code' => 'required|exists:energy_codes,id',
            'energy_accreditation_id' => 'required',
        ]);

        if (!$validator->fails()) {


            $companyStandardEnergyCode = CompanyStandardEnergyCode::find($request->company_standard_energy_code_id);

            $energy_name = EnergyCode::where('id', $request->energy_code)->first()->code;
            $accreditation_name = Accreditation::where('id', $request->energy_accreditation_id)->first()->name;

            $unique_code = $accreditation_name . "/" . $energy_name;
            $companyStandardEnergyCode->standard_id = $request->company_standard_id;
            $companyStandardEnergyCode->company_id = $request->company_id;
            $companyStandardEnergyCode->energy_id = $request->energy_code;
            $companyStandardEnergyCode->accreditation_id = $request->energy_accreditation_id;
            $companyStandardEnergyCode->unique_code = $unique_code;
            $companyStandardEnergyCode->save();


            if ($companyStandardEnergyCode)

                $data = [
                    'standard_id' => $companyStandardEnergyCode->standard_id,
                    'company_id' => $companyStandardEnergyCode->company_id,

                ];


            $response = (new ApiMessageController())->successResponse($data, 'Company Standard Energy Code Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function storeImsStandards(Request $request){
        if(count($request->standard_one)==1){
            $getcompanyStandard  = CompanyStandards::where('id',$request->standard_one[0])->first();
            return redirect()->route('company.show', $getcompanyStandard->company_id)->with(['flash_status' => "error", 'flash_message' => "please select atleast two standard."]);
        }
        else{
            $comapny_id = 0;
            $standard_id = 0;
            $name = "";
            $frequency = true;
            if(count($request->standard_one)==2){
                $getcompanyStandard  = CompanyStandards::where('id',$request->standard_one[0])->first();
                $getcompanyStandard1 = CompanyStandards::where('id',$request->standard_one[1])->first();
                if($getcompanyStandard->frequency != $getcompanyStandard1->frequency){
                    return redirect()->route('company.show', $getcompanyStandard->company_id)->with(['flash_status' => "error", 'flash_message' => "please select same frequency standard."]);
                }
            }
            else{
                $getcompanyStandard  = CompanyStandards::where('id',$request->standard_one[0])->first();
                $getcompanyStandard1 = CompanyStandards::where('id',$request->standard_one[1])->first();
                $getcompanyStandard2 = CompanyStandards::where('id',$request->standard_one[2])->first();
                if($getcompanyStandard->frequency != $getcompanyStandard1->frequency && $getcompanyStandard->frequency != $getcompanyStandard2->frequency){
                    return redirect()->route('company.show', $getcompanyStandard->company_id)->with(['flash_status' => "error", 'flash_message' => "please select same frequency standard."]);
                }

            }
            foreach($request->standard_one as $key => $standard){
                $getcompanyStandard = CompanyStandards::where('id',$standard)->first();
                if(!is_null($getcompanyStandard) && isset($getcompanyStandard)){
                    if($key ==0){
                        $comapny_id = $getcompanyStandard->company_id;
                        $standard_id = $getcompanyStandard->standard_id;
                        $getcompanyStandard->type = 'base'; 
                    }
                    else{
                        $getcompanyStandard->type = 'child'; 
                    }
                    if($name == ""){
                        $name = $getcompanyStandard->standard->name;
                    }
                    else{
                        $name = $name ."&".$getcompanyStandard->standard->name;
                    }

                    $getcompanyStandard->is_ims           = true;  
                    $getcompanyStandard->ims_standard_ids = implode(",", $request->standard_one);
                    $getcompanyStandard->status           = 'disabled';
                    $getcompanyStandard->save();
                    
                }
            }

            $ims_standard = new ImsStandard();
            $ims_standard->company_id = $comapny_id;
            $ims_standard->standard_id = $standard_id;
            $ims_standard->name = $name;
            $ims_standard->save();
        }
        return redirect()->route('company.show', $comapny_id)->with(['flash_status' => "success", 'flash_message' => "Ims Create Successfully."]);
    }


}
