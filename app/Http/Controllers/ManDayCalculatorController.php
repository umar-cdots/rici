<?php

namespace App\Http\Controllers;

use App\CompanyStandards;
use App\EnergyEnms;
use App\EnergyFamilySheet;
use App\EnergyWeightage;
use App\FoodCategory;
use App\FoodFamilySheet;
use App\ManDaySheet;
use App\Standards;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Traits\AccessRights;

class ManDayCalculatorController extends Controller
{
    use AccessRights;

    public function index()
    {
        // $this->VerifyRights();
        $surveillance_frequencies = CompanyStandards::getEnumValues()->get('surveillance_frequency');
        $standards = Standards::where('status', 1)->get();
        $food_categories = FoodCategory::all();
        $single_standards = $standards->filter(function ($value, $key) {
            return $value->standardFamily->name != 'Food' && $value->standardFamily->name != 'Occupational Health & Safety';
        });
        // if($this->redirectUrl){return redirect($this->redirectUrl);}
        return view('manday-calculator.index', compact('surveillance_frequencies', 'standards', 'single_standards', 'food_categories'), ['check_rights' => $this->check_employee_rights]);
    }

    public function calculateSingle(Request $request)
    {
        //validate the data
        $validator = Validator::make($request->all(), [
            'single_standard' => 'required|string',
            'complexity' => 'nullable|string',
            'effective_employees' => 'required|integer',
            'company_name' => 'required|string',
            'annual_energy' => 'nullable|integer',
            'energy_source' => 'nullable|integer',
            'energy_use' => 'nullable|integer',
            'haccp' => 'nullable|numeric',
            'food_category' => 'nullable|string',
            'management' => 'nullable|string',
            'no_of_sites' => 'nullable|integer'
        ]);

        if (!$validator->fails()) {
            $frequency = $request->frequency;
            $standard = Standards::findOrFail($request->single_standard);

            if (strtolower($standard->standardFamily->name) == 'food') {
                $tfte_value = FoodFamilySheet::where(function ($query) use ($request, $standard) {
                    $query->where('standard_id', $standard->id);
                    $query->whereRaw('? between TFTE_from and TFTE_to', (float)$request->effective_employees);

                })->first();

                $row = FoodFamilySheet::where(function ($query) use ($request, $standard) {
                    $query->where('standard_id', $standard->id);
                    $query->where('food_category_id', $request->food_category);
                })->first();

                if (!empty($row) && !empty($tfte_value)) {
                    if ($request->management == 'yes') {
                        $tms = 0;

                    } else {
//                        $tms = 0.25;
                        $tms = 0;
                    }


                    if ($request->haccp > 1) {
                        $th = $row->TH * ($request->haccp - 1);

                        $ts = $row->TD + $th + $tms + $tfte_value->TFTE_value;  //ts
                    } else {
                        $th = 0;
                        $ts = $row->TD + $th + $tms + $tfte_value->TFTE_value;  //ts
                    }


                    //fifth value no of sites

                    //total ts

                    if ($request->no_of_sites > 1) {
                        $additional_ts = $ts / 2 * ($request->no_of_sites - 1);

                        $new_ts = $ts + $additional_ts;

                    } else {
                        $additional_ts = 0;
                        $new_ts = $ts + $additional_ts;

//                        $new_ts = $ts/2;  // 0 not divide 2
                    }

                    $standard_name = $standard->name;
                    $company_name = $request->company_name;
                    $effective_employess = $request->effective_employees;
                    $haccp = $request->haccp;
                    $food_category = $request->food_category;
                    $no_of_sites = $request->no_of_sites;
                    $management = $request->management;
//                    $complexity = $request->complexity;

                    $filename = public_path('/uploads/documents/food-safety.pdf');

                    if (File::exists($filename)) {
                        File::delete($filename);  // or unlink($filename);
                    }
                    $filename = public_path('/uploads/documents/food-safety2.pdf');

                    if (File::exists($filename)) {
                        File::delete($filename);  // or unlink($filename);
                    }

                    $pdf = PDF::loadView('manday-calculator.download-pdf.download_food_safety', compact('new_ts', 'frequency', 'standard_name', 'haccp', 'food_category', 'no_of_sites', 'company_name', 'management', 'effective_employess', 'row'));
                    Storage::put('public/uploads/documents/food-safety.pdf', $pdf->output());
                    $pdf2 = PDF::loadView('manday-calculator.download-pdf.download_food_safety2', compact('new_ts', 'frequency', 'standard_name', 'haccp', 'food_category', 'no_of_sites', 'company_name', 'management', 'effective_employess', 'row'));
                    Storage::put('public/uploads/documents/food-safety2.pdf', $pdf2->output());
                    return view('manday-calculator.single-standard-partials.food-safety', compact('new_ts', 'frequency', 'row'));
                } else {

                    $data = [];
                    return (new ApiMessageController())->failedresponse($data, 'No Specific data found.');
                }

            } elseif (strtolower($standard->standardFamily->name) == 'energy management') {
                //dd('fs');
                $standard_id = $request->single_standard;
                //calcuting the value of C
                $c1 = $this->calculateC1($request->annual_energy);
                $c2 = $this->calculateC2($request->energy_source);
                $c3 = $this->calculateC3($request->energy_use);
                $c = $c1 + $c2 + $c3;
                $enms = EnergyEnms::all();

                if ($c > $enms[0]->from && $c <= $enms[0]->to) {
                    $EnMS_complexity = 'Low';
                } elseif ($c > $enms[1]->from && $c <= $enms[1]->to) {
                    $EnMS_complexity = 'Medium';
                } elseif ($c > $enms[2]->from && $c <= $enms[2]->to) {
                    $EnMS_complexity = 'High';
                } else {
                    $EnMS_complexity = 'High';
                }

                $complexity = $EnMS_complexity;
                // session()->put('energy_complexity');
                $row = $this->calcEnergyStageManday($standard->id, $request->effective_employees, $EnMS_complexity, $request->frequency);
                $type = 'energy';

                $c1 = $request->annual_energy;
                $c2 = $request->energy_source;
                $c3 = $request->energy_use;

                $standard_name = $standard->name;
                $company_name = $request->company_name;
                $effective_employess = $request->effective_employees;

                $filename = public_path('/uploads/documents/energy.pdf');

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $pdf = PDF::loadView('manday-calculator.download-pdf.download_energy', compact('row', 'frequency', 'complexity', 'EnMS_complexity', 'type', 'c1', 'c2', 'c3', 'c', 'standard_name', 'company_name', 'effective_employess'));
                Storage::put('public/uploads/documents/energy.pdf', $pdf->output());
                return view('manday-calculator.single-standard-partials.energy', compact('row', 'frequency', 'complexity', 'EnMS_complexity', 'type', 'c'));
            } else {
                $complexity = $request->complexity;
                $row = ManDaySheet::with('standard')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->single_standard);
                    $query->whereRaw('? between effective_employes_on and effective_employes_off', $request->effective_employees);
                    $query->where('complexity', $request->complexity);
                    $query->where('frequency', $request->frequency);
                })->first();

                if (!empty($row)) {


                    $standard_name = $standard->name;
                    $company_name = $request->company_name;
                    $effective_employess = $request->effective_employees;

                    $filename = public_path('/uploads/documents/others.pdf');

                    if (File::exists($filename)) {
                        File::delete($filename);  // or unlink($filename);
                    }
                    $filename = public_path('/uploads/documents/others2.pdf');

                    if (File::exists($filename)) {
                        File::delete($filename);  // or unlink($filename);
                    }
                    $pdf = PDF::loadView('manday-calculator.download-pdf.download_others', compact('row', 'frequency', 'complexity', 'standard_name', 'company_name', 'effective_employess'));
                    Storage::put('public/uploads/documents/others.pdf', $pdf->output());
                    $pdf2 = PDF::loadView('manday-calculator.download-pdf.download_others2', compact('row', 'frequency', 'complexity', 'standard_name', 'company_name', 'effective_employess'));
                    Storage::put('public/uploads/documents/others2.pdf', $pdf2->output());
                    return view('manday-calculator.single-standard-partials.others', compact('row', 'frequency', 'complexity'));
                } else {
                    $data = [];
                    return (new ApiMessageController())->failedresponse($data, 'No Specific data found.');
                }
            }
        } else {
            $data = $validator->errors()->toArray();
            return (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
    }

    protected static function calcEnergyStageManday($standard, $effective_employees, $c, $frequency)
    {
        return ManDaySheet::with('standard')->where(function ($query) use ($standard, $effective_employees, $c, $frequency) {
            $query->where('standard_id', $standard);
            $query->whereRaw("? between `effective_employes_on` and `effective_employes_off`", $effective_employees);
            $query->where('complexity', $c);
            $query->where('frequency', $frequency);
        })->first();
    }

    protected static function calculateC1($annual_energy)
    {

        $sheet_data = EnergyFamilySheet::where(function ($query) use ($annual_energy) {
            $query->whereRaw("? between `from` and `to`", $annual_energy);
            $query->where('energy_code_id', 'C1');
        })->first();


        if (!is_null($sheet_data)) {
            $complexity = $sheet_data->complexity;

            $how_much_percent = EnergyWeightage::where('name', 'EC')->first();
            return (($complexity * $how_much_percent->weightage) / 100);
        } else {
            return 0;
        }


//        $complexity = $sheet_data->complexity;
//
//        $how_much_percent = EnergyWeightage::where('name', 'EC')->first();
//
//        return (($complexity * $how_much_percent->weightage) / 100);
    }

    protected static function calculateC2($energy_source)
    {
        $sheet_data = EnergyFamilySheet::where(function ($query) use ($energy_source) {
            $query->whereRaw("? between `from` and `to`", $energy_source);
            $query->where('energy_code_id', 'C2');
        })->first();
        if (!is_null($sheet_data)) {
            $complexity = $sheet_data->complexity;

            $how_much_percent = EnergyWeightage::where('name', 'ES')->first();

            return (($complexity * $how_much_percent->weightage) / 100);
        } else {
            return 0;
        }

//        $complexity = $sheet_data->complexity;
//
//        $how_much_percent = EnergyWeightage::where('name', 'ES')->first();
//
//        return (($complexity * $how_much_percent->weightage) / 100);
    }

    protected static function calculateC3($energy_use)
    {
        $sheet_data = EnergyFamilySheet::where(function ($query) use ($energy_use) {
            $query->whereRaw("? between `from` and `to`", $energy_use);
            $query->where('energy_code_id', 'C3');
        })->first();

//        $complexity = $sheet_data->complexity;
//
//        $how_much_percent = EnergyWeightage::where('name', 'SEU')->first();
//
//        return (($complexity * $how_much_percent->weightage) / 100);

        if (!is_null($sheet_data)) {
            $complexity = $sheet_data->complexity;

            $how_much_percent = EnergyWeightage::where('name', 'SEU')->first();

            return (($complexity * $how_much_percent->weightage) / 100);
        } else {
            return 0;
        }
    }

    public function calculateMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required|string',
            'standards.*' => 'required|integer',
            'effective_employees' => 'required|integer',
            'frequency' => 'required|string'
        ]);
        $frequency = $request->frequency;
        if (!$validator->fails()) {
            $standards_set = count($request->standards);
            $complexity_tracker = 1;
            foreach ($request->standards as $standard) {
                $standard = Standards::findOrFail($standard);
                if (strtolower($standard->standardFamily->name) != 'food' || strtolower($standard->standardFamily->name) != 'energy management') {
                    $rows[] = ManDaySheet::where(function ($query) use ($request, $standard, $complexity_tracker) {
                        $query->where('standard_id', $standard->id);
                        $query->whereRaw('? between effective_employes_on and effective_employes_off', $request->effective_employees);
                        $query->where('complexity', $request['complexity_' . $complexity_tracker]);
                        $query->where('frequency', $request->frequency);
                    })->first();
                    $complexities[] = $request['complexity_' . $complexity_tracker];
                    $complexity_tracker++;
                }
            }

            $effective_employees = $request->effective_employees;
            $company_name = $request->company_name;
            //dd($rows);
            if (!empty($rows)) {

                $filename = public_path('/uploads/documents/multi_standard.pdf');

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $filename = public_path('/uploads/documents/multi_standard2.pdf');

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $pdf = PDF::loadView('manday-calculator.download-pdf.multi-standard', compact('rows', 'complexities', 'frequency', 'effective_employees', 'company_name'));
                Storage::put('public/uploads/documents/multi_standard.pdf', $pdf->output());

                $pdf2 = PDF::loadView('manday-calculator.download-pdf.multi-standard2', compact('rows', 'complexities', 'frequency', 'effective_employees', 'company_name'));
                Storage::put('public/uploads/documents/multi_standard2.pdf', $pdf2->output());

                return view("manday-calculator.multiple-partial", compact('rows', 'complexities', 'frequency'));
            } else {
                $data = [];
                return (new ApiMessageController())->failedresponse($data, 'No Specific data found.');
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    protected static function getEnergyPercentage(string $param, $standard_id)
    {
        $energy_enm = EnergyWeightage::where('name', $param)->where('standard_id', $standard_id)->first();

        return $energy_enm->weightage;
    }


    public function downloadSingleStandardPDF(Request $request)
    {


        //validate the data
        $validator = Validator::make($request->all(), [
            'single_standard' => 'required|string',
            'complexity' => 'nullable|string',
            'effective_employees' => 'required|integer',
            'company_name' => 'required|string',
            'annual_energy' => 'nullable|integer',
            'energy_source' => 'nullable|integer',
            'energy_use' => 'nullable|integer',
            'haccp' => 'nullable|numeric',
            'food_category' => 'nullable|string',
            'management' => 'nullable|string',
            'no_of_sites' => 'nullable|integer'
        ]);

        if (!$validator->fails()) {
            $frequency = $request->frequency;
            $standard = Standards::findOrFail($request->single_standard);

            if (strtolower($standard->standardFamily->name) == 'food') {
                $tfte_value = FoodFamilySheet::where(function ($query) use ($request, $standard) {
                    $query->where('standard_id', $standard->id);
                    $query->whereRaw('? between TFTE_from and TFTE_to', (float)$request->effective_employees);

                })->first();

                $row = FoodFamilySheet::where(function ($query) use ($request, $standard) {
                    $query->where('standard_id', $standard->id);
                    $query->where('food_category_id', $request->food_category);
                })->first();

                if (!empty($row) && !empty($tfte_value)) {
                    if ($request->management == 'yes') {
                        $tms = 0;

                    } else {
//                        $tms = 0.25;
                        $tms = 0;
                    }


                    if ($request->haccp > 1) {
                        $th = $row->TH * ($request->haccp - 1);

                        $ts = $row->TD + $th + $tms + $tfte_value->TFTE_value;  //ts
                    } else {
                        $th = 0;
                        $ts = $row->TD + $th + $tms + $tfte_value->TFTE_value;  //ts
                    }


                    //fifth value no of sites

                    //total ts

                    if ($request->no_of_sites > 1) {
                        $additional_ts = $ts / 2 * ($request->no_of_sites - 1);

                        $new_ts = $ts + $additional_ts;

                    } else {
                        $additional_ts = 0;
                        $new_ts = $ts + $additional_ts;
                    }

                    $pdf = PDF::loadView('manday-calculator.download-pdf.download_food_safety', compact('new_ts', 'frequency', 'row'));

                    $path = storage_path('app/public/documents/food_safety.pdf');

                    $pdf->save($path);
                    return $path;

                    // $pdf = PDF::loadView('manday-calculator.download-pdf.download_food_safety', compact('new_ts', 'frequency', 'row'));

                    // $pdf->download('food_safety.pdf');

                    // return view('manday-calculator.single-standard-partials.food-safety', compact('new_ts', 'frequency', 'row'));
                } else {

                    $data = [];
                    return (new ApiMessageController())->failedresponse($data, 'No Specific data found.');
                }

            } elseif (strtolower($standard->standardFamily->name) == 'energy management') {

                $standard_id = $request->single_standard;
                //calcuting the value of C
                $c1 = $this->calculateC1($request->annual_energy);
                $c2 = $this->calculateC2($request->energy_source);
                $c3 = $this->calculateC3($request->energy_use);


                $c = $c1 + $c2 + $c3;

                $enms = EnergyEnms::all();

                if ($c > $enms[0]->from && $c <= $enms[0]->to) {
                    $EnMS_complexity = 'Low';
                } elseif ($c > $enms[1]->from && $c <= $enms[1]->to) {
                    $EnMS_complexity = 'Medium';
                } elseif ($c > $enms[2]->from && $c <= $enms[2]->to) {
                    $EnMS_complexity = 'High';
                } else {
                    $EnMS_complexity = 'High';
                }


                $complexity = $EnMS_complexity;

                $row = $this->calcEnergyStageManday($standard->id, $request->effective_employees, $EnMS_complexity, $request->frequency);
                $type = 'energy';

                $pdf = PDF::loadView('manday-calculator.download-pdf.download_energy', compact('row', 'frequency', 'complexity', 'EnMS_complexity', 'type', 'c'));

                $path = storage_path('app/public/documents/energy.pdf');

                $pdf->save($path);
                return $path;

                // return $pdf->download('energy.pdf');

                // return view('manday-calculator.single-standard-partials.energy', compact('row', 'frequency', 'complexity', 'EnMS_complexity', 'type', 'c'));
            } else {
                $complexity = $request->complexity;
                $row = ManDaySheet::with('standard')->where(function ($query) use ($request) {
                    $query->where('standard_id', $request->single_standard);
                    $query->whereRaw('? between effective_employes_on and effective_employes_off', $request->effective_employees);

                    $query->where('complexity', $request->complexity);
                    $query->where('frequency', $request->frequency);
                })->first();
                if (!empty($row)) {

                    $pdf = PDF::loadView('manday-calculator.download-pdf.download_others', compact('row', 'frequency', 'complexity'));

                    $path = storage_path('app/public/documents/others.pdf');

                    $pdf->save($path);
                    return $path;


                    // return view('manday-calculator.single-standard-partials.others', compact('row', 'frequency', 'complexity'));
                } else {
                    $data = [];
                    return (new ApiMessageController())->failedresponse($data, 'No Specific data found.');
                }
            }
        } else {
            $data = $validator->errors()->toArray();
            return (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
    }

}

