<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\Auditor;
use App\AuditorEducation;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\AuditorStandardGrade;
use App\AuditorStandardTraining;
use App\AuditorStandardWitnessEvaluation;
use App\EnergyCode;
use App\FoodCategory;
use App\FoodSubCategory;
use App\Grades;
use App\IAF;
use App\IAS;
use App\Notification;
use App\Standards;
use App\StandardsFamily;
use App\TechnicalExpertStandard;
use App\TechnicalExpertStandardCode;
use App\TechnicalExpertStandardEnergyCode;
use App\TechnicalExpertStandardFoodCode;
use App\TechnicalExpertStandardGrade;
use App\TechnicalExpertStandardWitnessEvaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PhpParser\PrettyPrinter\Standard;

class TechnicalExpertStandardController extends Controller
{

    public function getAddTechnicalExpertStandardsModal($auditorId)
    {
        try {
            if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                $standards_families = StandardsFamily::orderBy('sort')->get();
                $auditorStandards = AuditorStandard::where('auditor_id', '=', $auditorId)->get();


            } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
                if (auth()->user()->user_type == 'scheme_coordinator') {
                    $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
                } else {
                    $scheme_manager_id = auth()->user()->scheme_manager->id;
                }
                $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->orderBy('sort')->get();

                $auditorStandards = AuditorStandard::where('auditor_id', '=', $auditorId)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get();
            }

            return view('technical-experts.technical-experts-standards.add-technical-expert-standards-modal', compact('standards_families', 'auditorStandards', 'auditorId'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;
    }

    public function saveTechnicalExpertStandards(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'standard_ids' => 'required|array|min:1',
            'standard_ids.*' => 'required|numeric|exists:standards,id',
        ], [
            'auditor_id.required' => "Create Technical Expert First",
            'auditor_id.exists' => "Technical Expert Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $standards = $request->standard_ids;

            $existingStandards = AuditorStandard::where('auditor_id', $request->auditor_id)->pluck('standard_id')->toArray();


            $newStandards = array_diff($standards, $existingStandards);

            $uncheckedStandards = array_diff($existingStandards, $standards);

            $deleteUncheckedStandards = (new AuditorStandard())->where('auditor_id', $request->auditor_id)
                ->whereIn('standard_id', $uncheckedStandards)
                ->delete();


            $getNewStandards = (new Standards())->whereIn('id', $newStandards)->get();
            $getExistingStandards = (new Standards())->whereIn('id', $existingStandards)->get();


            foreach ($getNewStandards as $getNewStandard) {
                foreach ($getExistingStandards as $getExistingStandard) {
                    if ($getNewStandard->standards_family_id == $getExistingStandard->standards_family_id) {


                        $getExistingAuditorStandard = AuditorStandard::where('standard_id', $getExistingStandard->id)
                            ->where('auditor_id', $request->auditor_id)->first();

                        $this->replicateExistingStandardData($getExistingAuditorStandard->id);
                    }
                }
            }


            foreach ($newStandards as $standard_id) {

                $auditorStandard = new AuditorStandard();
                $auditorStandard->auditor_id = $request->auditor_id;
                $auditorStandard->standard_id = $standard_id;
                $auditorStandard->save();
            }

            $data = [
                'current_step' => 2,
                'next_step' => 2
            ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standards Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function replicateExistingStandardData($existingAuditorStandardId)
    {
        $getToBeCreatedStandardId = AuditorStandard::latest()->first()->id + 1;

        $getExistingStandardGrade = AuditorStandardGrade::where('auditor_standard_id', $existingAuditorStandardId)->get();

        foreach ($getExistingStandardGrade as $standardGrade) {
            $grade = $standardGrade->replicate();
            $grade->auditor_standard_id = $getToBeCreatedStandardId;
            $grade->save();

            $standardGrade->getFirstMedia()->copy($grade);


        }

        return true;

    }

    public function getTechnicalExpertStandardsTabs($auditor_id = '')
    {

        $auditorStandards = array();

        if (!empty($auditor_id)) {
            if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                $auditorStandards = AuditorStandard::where('auditor_id', '=', $auditor_id)->get();

                if (auth()->user()->user_type == 'operation_manager') {
                    $approvedAuditorStandards = AuditorStandard::where('auditor_id', '=', $auditor_id)->where('auditor_standard_status', 'approved')->get();
//
//                    if(!empty($approvedAuditorStandards)){
//                        foreach ($approvedAuditorStandards as $approvedAuditorStandard) {
//                            $operation_manager = Notification::where('type_id', (int)$approvedAuditorStandard->standard_id)->where('type', 'auditor')->where('request_id', (int)$approvedAuditorStandard->auditor_id)->where('status', 'approved')->first();
//                            $operation_manager->is_read = true;
//                            $operation_manager->save();
//
//                        }
//                    }

                }
            } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
                if (auth()->user()->user_type == 'scheme_coordinator') {
                    $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
                } else {
                    $scheme_manager_id = auth()->user()->scheme_manager->id;
                }

                $auditorStandards = AuditorStandard::where('auditor_id', '=', $auditor_id)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get();
            }
        }

        return view('technical-experts.technical-experts-standards.technical-expert-standards-tabs', compact('auditorStandards', 'auditor_id'));
    }


    public function technicalExpertStandardGradesView($auditor_standard_id = '')
    {
        $auditorStandardsGrades = array();
        $grades = Grades::where('short_name', 'TE')->get();
        $gradeStatus = AuditorStandardGrade::getEnumValues()->get('status');

        if (!empty($auditor_standard_id)) {
            $auditorStandardsGrades = AuditorStandardGrade::where('auditor_standard_id', '=', $auditor_standard_id)->get();
        }
        return view('technical-experts.technical-experts-standards.technical-expert-standards-grades', compact('auditorStandardsGrades', 'auditor_standard_id', 'grades', 'gradeStatus'));
    }

    public function saveTechnicalExpertStandardGrade(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required|exists:auditor_standards,id',
            'grade_id' => 'required|exists:grades,id',
        ], [
            'auditor_standard_id.required' => "Add Technical Expert Standard First",
            'auditor_standard_id.exists' => "Technical Expert Standard Does Not Exist",
        ]);

        if (!$validator->fails()) {
            $data = AuditorStandardGrade::where('auditor_standard_id', $request->auditor_standard_id)->where('grade_id', $request->grade_id)->get();

            if ($data->count() > 0) {

                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {
                $auditorStandardsGrade = new AuditorStandardGrade();
                $auditorStandardsGrade->auditor_standard_id = $request->auditor_standard_id;
                $auditorStandardsGrade->grade_id = $request->grade_id;
                $auditorStandardsGrade->status = $request->status;
                $auditorStandardsGrade->approval_date = ($request->approval_date) ? $request->approval_date : 'NULL';
                $auditorStandardsGrade->remarks = $request->remarks;
                if ($request->hasFile('document_file')) {
                    $image = $request->file('document_file');

                    $name = $image->getClientOriginalName();
                    $destinationPath = public_path('/uploads/technical_expert_grade_document');
                    $auditor_files_with_time = time() . '_' . $name;
                    $image->move($destinationPath, $auditor_files_with_time);
                    $auditorStandardsGrade->grade_document = $auditor_files_with_time;
                }
                $auditorStandardsGrade->save();


                if ($auditorStandardsGrade)

                    $data = [
                        'auditor_standard_id' => $request->auditor_standard_id,
                        'current_step' => 2,
                        'next_step' => 2
                    ];

                $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Grade Added Successfully!');
            }


        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteTechnicalExpertStandardGrade($audStdGradeId)
    {

        try {
            $auditorStandardGrade = AuditorStandardGrade::findOrFail($audStdGradeId);
            $deleteItem = $auditorStandardGrade->delete();

            if ($deleteItem) {
                $data = [
                    'auditor_standard_id' => $auditorStandardGrade->auditor_standard_id
                ];

                $response = (new ApiMessageController())->successResponse($data, "Technical Expert Standard Grade Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditTechnicalExpertStdGradeModal($audStdGradeId)
    {
        try {

            $auditorStandardsGrade = AuditorStandardGrade::find($audStdGradeId);
            $grades = Grades::where('short_name', 'TE')->get();
            $gradeStatus = AuditorStandardGrade::getEnumValues()->get('status');

            return view('technical-experts.technical-experts-standards.edit-standard-grade-modal', compact('auditorStandardsGrade', 'grades', 'gradeStatus'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateTechnicalExpertStandardGrade(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'standard_grade_id' => 'required|exists:auditor_standard_grades,id',
            'grade_id' => 'required|exists:grades,id',
        ], [
            'standard_grade_id.required' => "Add Technical Expert Standard Grade First",
            'standard_grade_id.exists' => "Technical Expert Standard Grade Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $auditorStandardsGrade = AuditorStandardGrade::find($request->standard_grade_id);
            $auditorStandardsGrade->grade_id = $request->grade_id;
            $auditorStandardsGrade->status = $request->status;
            $auditorStandardsGrade->approval_date = ($request->approval_date) ? $request->approval_date : 'NULL';
            $auditorStandardsGrade->remarks = $request->remarks;
            if ($request->hasFile('document_file')) {
                $image = $request->file('document_file');

                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/technical_expert_grade_document');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorStandardsGrade->grade_document = $auditor_files_with_time;
            }
            $auditorStandardsGrade->save();
            if ($auditorStandardsGrade)

                $data = [
                    'auditor_standard_id' => $auditorStandardsGrade->auditor_standard_id,
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Grade Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function technicalExpertStandardCodesView($auditor_standard_id = '')
    {

        $data['auditorStandardCodes'] = array();
        $data['status'] = AuditorStandardCode::getEnumValues()->get('status');
        $data['standards'] = Standards::orderBy('name')->get();
        $data['accreditations'] = Accreditation::all();
        $data['iafs'] = IAF::orderBy('code')->get();
        $data['iass'] = IAS::orderBy('code')->get();
        $data['auditor_standard_id'] = $auditor_standard_id;


        if (!empty($auditor_standard_id)) {
            $data['auditorStandardCodes'] = AuditorStandardCode::where('auditor_standard_id', '=', $auditor_standard_id)->with(['ias', 'iaf'])->get();
        }

        return view('technical-experts.technical-experts-standards.technical-expert-standards-codes', $data);
    }

    public function saveTechnicalExpertStandardCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required|exists:auditor_standards,id',
            'iaf_id' => 'required|exists:i_a_fs,id',
            'ias_id' => 'required|exists:i_a_s,id',
            'accreditation_id.0' => 'required|numeric',
//            'accreditation_id.*' => '',
            'status' => ['required', Rule::in(AuditorStandardCode::getEnumValues()->get('status'))],
            'remarks' => 'required_if:status,==,partial',

        ], [
            'accreditation_id.0.required' => 'Accreditation field is required'
        ]);

        if (!$validator->fails()) {

            $data = AuditorStandardCode::where('auditor_standard_id', $request->auditor_standard_id)->where('iaf_id', $request->iaf_id)->where('ias_id', $request->ias_id)->get();
            if ($data->count() > 0) {
                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {
                $iaf_name = IAF::where('id', $request->iaf_id)->first()->code;
                $ias_name = IAS::where('id', $request->ias_id)->first()->code;
                $accreditation_name = Accreditation::where('id', $request->accreditation_id)->first()->name;

                $unique_code = $accreditation_name . "/" . $iaf_name . "/" . $ias_name;


                $auditorStandardCode = new AuditorStandardCode();
                $auditorStandardCode->auditor_standard_id = $request->auditor_standard_id;
                $auditorStandardCode->iaf_id = $request->iaf_id;
                $auditorStandardCode->ias_id = $request->ias_id;
                $auditorStandardCode->accreditation_id = implode(",", $request->accreditation_id);
                $auditorStandardCode->status = $request->status;
                $auditorStandardCode->remarks = $request->remarks;
                $auditorStandardCode->unique_code = $unique_code;
                $auditorStandardCode->save();


                if ($auditorStandardCode)

                    $data = [
                        'auditor_standard_id' => $request->auditor_standard_id,
                        'current_step' => 2,
                        'next_step' => 2
                    ];

                $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Code Added Successfully!');
            }
        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteTechnicalExpertStandardCode($modelId)
    {

        try {
            $auditorStandardCode = AuditorStandardCode::findOrFail($modelId);
            $deleteItem = $auditorStandardCode->delete();

            if ($deleteItem) {
                $data = [
                    'auditor_standard_id' => $auditorStandardCode->auditor_standard_id
                ];

                $response = (new ApiMessageController())->successResponse($data, "Technical Expert Standard Code Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditCodesModal($modelId)
    {
        try {

            $data['standards'] = Standards::orderBy('name')->get();
            $data['accreditations'] = Accreditation::all();
            $data['iafs'] = IAF::orderBy('code')->get();
            $data['iass'] = IAS::orderBy('code')->get();
            $data['status'] = AuditorStandardCode::getEnumValues()->get('status');
            $data['auditorStandardCode'] = AuditorStandardCode::find($modelId);
            $data['standardId'] = AuditorStandard::whereId($data['auditorStandardCode']->auditor_standard_id)->first()->standard_id;

            return view('technical-experts.technical-experts-standards.edit-codes-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateTechnicalExpertStandardCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'auditor_standard_code_id' => 'required|exists:auditor_standard_codes,id',
            'iaf_id' => 'required|exists:i_a_fs,id',
            'ias_id' => 'required|exists:i_a_s,id',
            'accreditation_id.0' => 'required|numeric',
            'status' => ['required', Rule::in(AuditorStandardCode::getEnumValues()->get('status'))],
            'remarks' => 'required_if:status,==,partial',
        ], [
            'accreditation_id.0.required' => 'Accreditation field is required'
        ]);

        if (!$validator->fails()) {

            $iaf_name = IAF::where('id', $request->iaf_id)->first()->code;
            $ias_name = IAS::where('id', $request->ias_id)->first()->code;
            $accreditation_name = Accreditation::where('id', $request->accreditation_id)->first()->name;

            $unique_code = $accreditation_name . "/" . $iaf_name . "/" . $ias_name;
            $auditorStandardCode = AuditorStandardCode::find($request->auditor_standard_code_id);
            $auditorStandardCode->iaf_id = $request->iaf_id;
            $auditorStandardCode->ias_id = $request->ias_id;
            $auditorStandardCode->accreditation_id = implode(",", $request->accreditation_id);
            $auditorStandardCode->status = $request->status;
            $auditorStandardCode->remarks = $request->remarks;
            $auditorStandardCode->unique_code = $unique_code;
            $auditorStandardCode->save();


            if ($auditorStandardCode)

                $data = [
                    'auditor_standard_id' => $auditorStandardCode->auditor_standard_id,

                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Code Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function technicalExpertStandardFoodCodesView($auditor_standard_id = '')
    {
        $auditorStandardFoodCodes = array();
        $foodCategories = FoodCategory::orderBy('code')->get();
        $accreditations = Accreditation::all();

        if (!empty($auditor_standard_id)) {
            $auditorStandardFoodCodes = AuditorStandardFoodCode::where('auditor_standard_id', '=', (int)$auditor_standard_id)->with(['foodcategory', 'foodsubcategory'])->get();
        }

        return view('technical-experts.technical-experts-standards.technical-expert-standards-food-codes', compact('auditorStandardFoodCodes', 'auditor_standard_id', 'foodCategories', 'accreditations'));
    }

    public function saveTechnicalExpertStandardFoodCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required|exists:auditor_standards,id',
            'food_category_code' => 'required|exists:food_categories,id',
            'food_sub_category_code' => 'required|exists:food_sub_categories,id',
            'food_accreditation_id' => 'required',
        ]);

        if (!$validator->fails()) {

            $data = AuditorStandardFoodCode::where('auditor_standard_id', $request->auditor_standard_id)->where('food_category_code', $request->food_category_code)->where('food_sub_category_code', $request->food_sub_category_code)->get();
            if ($data->count() > 0) {
//                dd('azeem');
                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {
                $food_name = FoodCategory::where('id', $request->food_category_code)->first()->code;
                $food_sub_name = FoodSubCategory::where('id', $request->food_sub_category_code)->first()->code;
                $accreditation_name = Accreditation::where('id', $request->food_accreditation_id)->first()->name;

                $unique_code = $accreditation_name . "/" . $food_name . "/" . $food_sub_name;


                $auditorStandardFoodCode = new AuditorStandardFoodCode();
                $auditorStandardFoodCode->auditor_standard_id = $request->auditor_standard_id;
                $auditorStandardFoodCode->food_category_code = $request->food_category_code;
                $auditorStandardFoodCode->food_sub_category_code = $request->food_sub_category_code;
                $auditorStandardFoodCode->accreditation_id = $request->food_accreditation_id;
                $auditorStandardFoodCode->unique_code = $unique_code;
                $auditorStandardFoodCode->save();


                if ($auditorStandardFoodCode)

                    $data = [
                        'auditor_standard_id' => $request->auditor_standard_id,
                    ];

                $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Food Code Added Successfully!');
            }
        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteTechnicalExpertStandardFoodCode($modelId)
    {

        try {
            $model = AuditorStandardFoodCode::findOrFail($modelId);
            $deleteItem = $model->delete();

            if ($deleteItem) {
                $data = [
                    'auditor_standard_id' => $model->auditor_standard_id
                ];

                $response = (new ApiMessageController())->successResponse($data, "Technical Expert Standard Food Code Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditFoodCodesModal($modelId)
    {
        try {
            $auditorStandardFoodCode = AuditorStandardFoodCode::with(['foodcategory', 'foodsubcategory'])->find($modelId);

            $allAccreditations = Accreditation::where('standard_id', $auditorStandardFoodCode->auditorStandard->standard_id)->get();
            $allAccreditation_ids = Accreditation::where('standard_id', $auditorStandardFoodCode->auditorStandard->standard_id)->pluck('id')->toArray();

            $foodCategories = FoodCategory::whereIn('accreditation_id', $allAccreditation_ids)->orderBy('code')->get();
            $foodCategory_ids = FoodCategory::whereIn('accreditation_id', $allAccreditation_ids)->pluck('id')->toArray();
            $foodSubCategories = FoodSubCategory::whereIn('food_category_id', $foodCategory_ids)->orderBy('code')->get();

            return view('technical-experts.technical-experts-standards.edit-food-codes-modal', compact('foodCategories', 'foodSubCategories', 'auditorStandardFoodCode', 'allAccreditations'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateTechnicalExpertStandardFoodCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'auditor_standard_food_code_id' => 'required|exists:auditor_standard_food_codes,id',
            'food_category_code' => 'required|exists:food_categories,id',
            'food_sub_category_code' => 'required|exists:food_sub_categories,id',
            'food_accreditation_id' => 'required'
        ]);

        if (!$validator->fails()) {

            $food_name = FoodCategory::where('id', $request->food_category_code)->first()->code;
            $food_sub_name = FoodSubCategory::where('id', $request->food_sub_category_code)->first()->code;
            $accreditation_name = Accreditation::where('id', $request->food_accreditation_id)->first()->name;

            $unique_code = $accreditation_name . "/" . $food_name . "/" . $food_sub_name;

            $auditorStandardFoodCode = AuditorStandardFoodCode::find($request->auditor_standard_food_code_id);
            $auditorStandardFoodCode->food_category_code = $request->food_category_code;
            $auditorStandardFoodCode->food_sub_category_code = $request->food_sub_category_code;
            $auditorStandardFoodCode->accreditation_id = $request->food_accreditation_id;
            $auditorStandardFoodCode->unique_code = $unique_code;
            $auditorStandardFoodCode->save();


            if ($auditorStandardFoodCode)

                $data = [
                    'auditor_standard_id' => $auditorStandardFoodCode->auditor_standard_id,
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Food Code Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function technicalExpertStandardEnergyCodesView($auditor_standard_id = '')
    {
        $auditorStandardEnergyCodes = array();
        $energyCodes = EnergyCode::all();
        $accreditations = Accreditation::all();

        if (!empty($auditor_standard_id)) {
            $auditorStandardEnergyCodes = AuditorStandardEnergyCode::where('auditor_standard_id', '=', $auditor_standard_id)->with('energycode')->get();
        }

        return view('technical-experts.technical-experts-standards.technical-expert-standards-energy-codes', compact('auditorStandardEnergyCodes', 'auditor_standard_id', 'energyCodes', 'accreditations'));
    }

    public function saveTechnicalExpertStandardEnergyCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required|exists:auditor_standards,id',
            'energy_code' => 'required|exists:energy_codes,id',
            'energy_accreditation_id' => 'required',
        ]);

        if (!$validator->fails()) {


            $data = AuditorStandardEnergyCode::where('auditor_standard_id', $request->auditor_standard_id)->where('energy_code', $request->energy_code)->get();
            if ($data->count() > 0) {
//                dd('azeem');
                $data = [
                    'status' => 'exist'
                ];

                $response = $data;
            } else {
                $energy_name = EnergyCode::where('id', $request->energy_code)->first()->code;
                $accreditation_name = Accreditation::where('id', $request->energy_accreditation_id)->first()->name;

                $unique_code = $accreditation_name . "/" . $energy_name;

                $auditorStandardEnergyCode = new AuditorStandardEnergyCode();
                $auditorStandardEnergyCode->auditor_standard_id = $request->auditor_standard_id;
                $auditorStandardEnergyCode->energy_code = $request->energy_code;
                $auditorStandardEnergyCode->accreditation_id = $request->energy_accreditation_id;
                $auditorStandardEnergyCode->unique_code = $unique_code;
                $auditorStandardEnergyCode->save();


                if ($auditorStandardEnergyCode)

                    $data = [
                        'auditor_standard_id' => $request->auditor_standard_id,
                    ];

                $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Energy Code Added Successfully!');
            }
        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteTechnicalExpertStandardEnergyCode($modelId)
    {

        try {
            $model = AuditorStandardEnergyCode::findOrFail($modelId);
            $deleteItem = $model->delete();

            if ($deleteItem) {
                $data = [
                    'auditor_standard_id' => $model->auditor_standard_id
                ];

                $response = (new ApiMessageController())->successResponse($data, "Technical Expert Standard Food Code Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditEnergyCodesModal($modelId)
    {
        try {

            $auditorStandardEnergyCode = AuditorStandardEnergyCode::with('energycode')->find($modelId);

            $allAccreditations = Accreditation::where('standard_id', $auditorStandardEnergyCode->auditorStandard->standard_id)->get();
            $allAccreditation_ids = Accreditation::where('standard_id', $auditorStandardEnergyCode->auditorStandard->standard_id)->pluck('id')->toArray();
            $energyCodes = EnergyCode::whereIn('accreditation_id', $allAccreditation_ids)->orderBy('code')->get();


            return view('technical-experts.technical-experts-standards.edit-energy-codes-modal', compact('energyCodes', 'auditorStandardEnergyCode', 'allAccreditations'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateTechnicalExpertStandardEnergyCode(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'auditor_standard_energy_code_id' => 'required|exists:auditor_standard_energy_codes,id',
            'energy_code' => 'required|exists:energy_codes,id',
            'energy_accreditation_id' => 'required',
        ]);

        if (!$validator->fails()) {
            $energy_name = EnergyCode::where('id', $request->energy_code)->first()->code;
            $accreditation_name = Accreditation::where('id', $request->energy_accreditation_id)->first()->name;

            $unique_code = $accreditation_name . "/" . $energy_name;


            $auditorStandardEnergyCode = AuditorStandardEnergyCode::find($request->auditor_standard_energy_code_id);
            $auditorStandardEnergyCode->energy_code = $request->energy_code;
            $auditorStandardEnergyCode->accreditation_id = $request->energy_accreditation_id;
            $auditorStandardEnergyCode->unique_code = $unique_code;
            $auditorStandardEnergyCode->save();


            if ($auditorStandardEnergyCode)

                $data = [
                    'auditor_standard_id' => $auditorStandardEnergyCode->auditor_standard_id,
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Energy Code Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    //    Get Notification

    public function technicalExpertStandardNotification(Request $request)
    {

        $auditorStandard = AuditorStandard::where('id', $request->auditor_standard_id)->first();
        $auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $request->auditor_standard_id)->whereNotNull('approval_date')->whereNull('deleted_at')->get();
        $auditorStatus = Auditor::where('id', $request->auditor_id)->first()->auditor_status;
        $notifications = Notification::where('type_id', (int)$auditorStandard->standard_id)->where('request_id', (int)$auditorStandard->auditor_id)->where('type', 'technical_expert')->get();


        return view('technical-experts.technical-experts-standards.notifications', compact('notifications', 'auditorStandard', 'auditorStandardGrades', 'auditorStatus'));

    }


    //Remove Image
    public function removeTechnicalExpertStandardGrade(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'standard_grade_id' => 'required|exists:auditor_standard_grades,id',
            'grade_id' => 'required|exists:grades,id',
        ], [
            'standard_grade_id.required' => "Add Technical Expert Standard Grade First",
            'standard_grade_id.exists' => "Technical Expert Standard Grade Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $auditorStandardsGrade = AuditorStandardGrade::find($request->standard_grade_id);
            $auditorStandardsGrade->grade_document = null;
            $auditorStandardsGrade->save();
            if ($auditorStandardsGrade)

                $data = [
                    'auditor_standard_id' => $auditorStandardsGrade->auditor_standard_id,
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Standard Grade Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


}
