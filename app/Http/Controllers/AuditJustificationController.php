<?php

namespace App\Http\Controllers;

use App\AJRemarks;
use App\AjStageChange;
use App\AJStageTeam;
use App\AJStandard;
use App\AJStandardStage;
use App\AuditJustification;
use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\Certificate;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\CompanyStandards;
use App\CompanyStandardsAnswers;
use App\FoodCategory;
use App\FoodSubCategory;
use App\Grades;
use App\Notification;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\TechnicalExpertStandard;
use App\Traits\GeneralHelperTrait;
use App\Traits\MandayTrait;
use App\User;
use App\OutSourceRequest;
use App\OutSourceRequestAuditor;
use App\OutSourceRequestStandard;
use App\AuditorStandardGrade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Mockery\Matcher\Not;

class AuditJustificationController extends Controller
{
    use MandayTrait, GeneralHelperTrait;

    public function index($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::findOrFail($company_id);

        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);
        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }
        if (is_null($role)) {
            $role = auth()->user()->user_type;
        }
        if ($ims != null) {
            //for ims
        } else {

            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();

            $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

            //calculate manday of food family,energy family and generic family

            if (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Food') {

                $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->where('company_standards_id', $c_std->id)->get();

//                dd($standardQuestions);
                foreach ($standardQuestions as $index => $questions) {

                    if ($index === 0) {
                        $haccp = $questions->answer;
                    } elseif ($index === 1) {
                        $certified_management = $questions->answer;
                    }
//                    elseif ($index === 2) {
//                        $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
////                        $food_category_id = FoodCategory::where('code', json_decode($questions->answer))->first()->id;
//                        $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
//                    }
                }

                $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
                $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
//                dd([$standard_number->id, $food_category_id, $haccp, $company->effective_employees, $certified_management, $company->childCompanies->count(), $c_std->surveillance_frequency]);
                $manday = $this->generateFoodSafetyTable($standard_number->id, $food_category_id, $haccp, $company->effective_employees, $certified_management, $company->childCompanies->count() + 1, $c_std->surveillance_frequency);

            } elseif (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Energy Management') {
                $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->where('company_standards_id', $c_std->id)->get();
//                dd($standardQuestions);
                foreach ($standardQuestions as $index => $questions) {

                    if ($index === 0) {
                        $energy_consumption_in_tj = $questions->answer;
                    } elseif ($index === 1) {
                        $no_of_energy_resources = $questions->answer;
                    } elseif ($index === 2) {
                        $no_of_significant_energy = $questions->answer;
                    }
                }


                $manday = $this->generateEnergyTable($standard_number->id, $energy_consumption_in_tj, $no_of_energy_resources, $no_of_significant_energy, $company->effective_employees, $c_std->surveillance_frequency);

            } else {

                $manday = $this->generateSimpleTable($standard_number->id, $c_std->proposed_complexity, $company->effective_employees, $c_std->surveillance_frequency);
            }
            if (empty($manday)) {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Man days not available for this standard');
            } else {

                $calc_manday = [];

                $calc_manday = $manday;


                $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];

                $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();
                if (!empty($scheme_manager_standards)) {
                    if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) { //true

                        if (!empty($company->companyStandards)) {

                            foreach ($company->companyStandards as $standard) {
                                $company_standards[] = $standard->standard_id;
                            }
                            $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();
                            if ($auditor_standards->isNotEmpty()) {
                                foreach ($auditor_standards as $auditor_standard) {

                                    foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {
                                        $members[] = $auditor_standard;
                                        $standard_grades[] = $standard_grade;
                                    }
                                    foreach ($company->companyIAFCodes as $key => $companyIaf) {
                                        $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                                    }
                                }

                                //checking the stage1 and stage2 combine aj

                                if ($audit_type == 'stage_1|stage_2') {

                                    $combine_tracker = 1;
                                } else {
                                    $combine_tracker = 0;
                                }

                                //check the company already have any aj
                                $aj_obj = AuditJustification::where('company_id', $company->id)->first();

                                if (!empty($aj_obj)) {


                                    //check which standards are in company aj
                                    $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                        ->where('standard_id', $standard_number->id)
                                        ->first();

                                    if (!empty($aj_standards_obj)) {

//                                        if (!empty($aj_standard_stages_obj)) {
                                        if ($combine_tracker == 1) {

                                            if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                $standardStagesObject = AJStandardStage::whereIn('audit_type', ['stage_1', 'stage_2'])->where('aj_standard_id', (int)$aj_standards_obj->id)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;


                                            } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;
//
                                            }

                                        } else {

                                            if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $audit_type)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;


                                            } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;
//
                                            }
                                        }
                                        $job_number = $standardStagesObject[0]->job_number;


                                        //return to the edit page
                                        return view('audit-justification.create', compact('job_number', 'standard_codes', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                    } else {
                                        $audit_justification = AuditJustification::updateOrCreate(
                                            [
                                                'company_id' => $company->id
                                            ],
                                            [
                                                'company_id' => $company->id
                                            ]
                                        );

                                        $aj_stardard = $audit_justification->ajStandards()->updateOrCreate(
                                            [
                                                'standard_id' => $standard_number->id
                                            ], [
                                                'standard_id' => $standard_number->id
                                            ]
                                        );

                                        if ($c_std->surveillance_frequency === 'biannual') {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                                        } else {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                        }
                                        $i = 0;
//                                        $job_number = $this->jobNumber($company->country->iso);
                                        $job_number = null;

                                        foreach ($aj_audit_types as $aj_audit_type) {

                                            $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                'audit_type' => $aj_audit_type,
                                                'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                'job_number' => $job_number,

                                            ]);
                                        }


                                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                        if ($audit_type == 'stage_1|stage_2') {
                                            $combine_tracker = 1;
                                        } else {
                                            $combine_tracker = 0;
                                        }


                                        if (!empty($aj_obj)) {

                                            //check which standards are in company aj
                                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                                ->where('standard_id', $standard_number->id)
                                                ->first();


                                            if (!empty($aj_standards_obj)) {
                                                if ($combine_tracker == 1) {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::whereIn('audit_type', ['stage_1', 'stage_2'])->where('aj_standard_id', (int)$aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;


                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                        //get the standard stage stage_1

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;
//
                                                    }

                                                } else {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $audit_type)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;


                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                        //get the standard stage stage_1

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;
//
                                                    }


                                                }
                                            }
                                        }

                                        return view('audit-justification.create', compact('job_number', 'standard_codes', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));
                                    }

                                } else {

                                    $audit_justification = AuditJustification::updateOrCreate(
                                        [
                                            'company_id' => $company->id
                                        ],
                                        [
                                            'company_id' => $company->id
                                        ]
                                    );
                                    if (!$audit_justification->ajStandards->isNotEmpty()) {


                                        $aj_stardard = $audit_justification->ajStandards()->updateOrCreate(
                                            [
                                                'standard_id' => $standard_number->id
                                            ], [
                                                'standard_id' => $standard_number->id
                                            ]
                                        );


                                        if ($c_std->surveillance_frequency === 'biannual') {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                                        } else {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                        }

                                        $i = 0;
//                                        $job_number = $this->jobNumber($company->country->iso);

                                        $job_number = null;
                                        foreach ($aj_audit_types as $aj_audit_type) {
                                            $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                'audit_type' => $aj_audit_type,
                                                'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                'job_number' => $job_number,

                                            ]);
                                            $i++;
                                        }

                                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                        if ($audit_type == 'stage_1|stage_2') {
                                            $combine_tracker = 1;
                                        } else {
                                            $combine_tracker = 0;
                                        }


                                        if (!empty($aj_obj)) {

                                            //check which standards are in company aj
                                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                                ->where('standard_id', $standard_number->id)
                                                ->first();


                                            if (!empty($aj_standards_obj)) {

                                                if ($combine_tracker == 1) {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::whereIn('audit_type', ['stage_1', 'stage_2'])->where('aj_standard_id', (int)$aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;


                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;
//
                                                    }

                                                } else {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $audit_type)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;

                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                        //get the standard stage stage_1

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;

                                                    }


                                                }
                                            }
                                        }


                                        return view('audit-justification.create', compact('job_number', 'standard_codes', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));
                                    }
                                }

                            } else {
                                return back()->with('flash_status', 'warning')
                                    ->with('flash_message', 'Auditor standard not available');
                            }
                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Company does not have this standard');
                        }
                    }
                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Scheme manager not available for this standard');
                }
            }
        }

    }

    public function updateStageManday(Request $request)
    {
        $mandayStage = AJStandardStage::findOrFail((int)$request->id);

        $mandayStage->update([
            $request->site => $request->value
        ]);
        $data = [
            'status' => 'success',
            'message' => 'Manday Value has been edit successfully'
        ];
        return response()->json($data);
    }

    public function store(Request $request)
    {

        $checkTeamMember = $this->checkTeam($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);
            return $response;
        }

        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'offsite' => 'required|string',
            'supervisor_involved.stage1' => 'required|string',
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam.*.required' => 'Please select the auditor member',
            ]);

        if (!$validator->fails()) {
            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }
            if (!empty($request->perposedTeam)) {
                foreach ($request->standard_stage_id as $key => $standard_stage_id) {
                    $standard_stage = AJStandardStage::findOrFail((int)$standard_stage_id);

                    $expectedDate = $request->expected_date[0] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[0]);
                    $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);


                    if ($request->status == 'created') {
                        $status = $request->status;
                    } elseif ($request->status == 'approved_scheme') {
                        $status = 'approved';
                    } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                        $status = 'applied';
                    } else {
                        $status = 'created';
                    }

                    if ($request->status == 'approved_scheme' && $approvalDate != null && $standard_stage->approval_date != $approvalDate) {
                        $lastApprovalDate = $standard_stage->approval_date;
                    } else {
                        $lastApprovalDate = $standard_stage->last_approval_date;
                    }

                    $standard_stage->update([
                        'certificate_scope' => $request->certificate_scope,
                        'manday_remarks' => $request->manday_remarks,
                        'excepted_date' => $expectedDate,
                        'supervisor_involved' => $request->supervisor_involved[str_replace('_', '', $stages[$key])],
                        'evaluator_name' => $request->evaluator_name,
                        'approval_date' => $approvalDate,
                        'last_approval_date' => $lastApprovalDate,
                        'status' => $status,
                        'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                        'job_number' => $request->job_number
                    ]);

//                    if ($standard_stage->job_number != $request->job_number) {
//                        $job_number = DB::table('job_numbers')->where('year', substr($request->job_number, 3, 2))->update([
//                            'country_code' => substr($request->job_number, 0, 3),
//                            'year' => substr($request->job_number, 3, 2),
//                            'code' => substr($request->job_number, 5, 4),
//                            'created_at' => date('Y-m-d H:i:s'),
//                        ]);
//
//                        if ($standard_stage->audit_type == 'stage_1') {
//                            $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                                'job_number' => $request->job_number
//                            ]);
//                        }
//
//
//                    }

                    $standard_stage->ajStageTeams()->delete();


                    if (!empty($request->perposedTeam)) {


                        $supervisor = 0;
                        if ($request['supervisor_involved']['stage1'] == "yes") {
                            $supervisor = 1;
                        }

                        foreach ($request->perposedTeam as $key => $a_member) {


                            $standard_stage->ajStageTeams()->create([
                                'auditor_id' => $a_member,
                                'supervisor_involved' => $supervisor,
                                'grade_id' => $request->perposed_team_grade_id[$key],
                                'member_name' => $request->perposed_team_name[$key],
                                'member_type' => $request->perposed_team_type[$key],
                            ]);

                            $auditor = Auditor::find((int)$a_member)->update([
                                'is_available' => 0
                            ]);

                        }

//                        $aj_standard = AJStandard::findOrFail((int)$request->aj_standard_id);
//                        $standard_stage = AJStandardStage::findOrFail((int)$request->standard_stage_id);

//                        if ($request->audit_type == 'reaudit' && $request->status == 'approved') {
//
//                            $recycle_date = date('Y-m-d', strtotime($request->expected_date[0]));
//
//                            if ($request->frequency === 'biannual') {
//
//                                $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($recycle_date)));
//                                $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
//                                $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
//                                $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
//                                $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
//                                $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
//
//                                $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
//                                $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];
//
//                            } else {
//
//                                $effectiveDate = date('Y-m-d', strtotime("+1 year", strtotime($recycle_date)));
//                                $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($recycle_date)));
//                                $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($recycle_date)));
//                                $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
//                                $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];
//
//                            }
//
//                            if ($standard_stage->recycle == null) {
//                                $next_recycle = 1;
//                                $all_standard_stages = AJStandardStage::where('aj_standard_id', $request->aj_standard_id)
//                                    ->get();
//                            } else {
//                                $next_recycle = $standard_stage->recycle + 1;
//                                $all_standard_stages = AJStandardStage::where('aj_standard_id', $request->aj_standard_id)->get();
//                            }
//
//                            foreach ($aj_audit_types as $key => $aj_audit_type) {
//
//                                foreach ($all_standard_stages as $key1 => $stage) {
//
//                                    if ($aj_audit_type == $stage->audit_type) {
//                                        $aj_standard_stages_obj = $aj_standard->ajStandardStages()->create([
//                                            'audit_type' => $aj_audit_type,
//                                            'onsite' => $stage->onsite,
//                                            'offsite' => $stage->offsite,
//                                            'excepted_date' => $due_date[$key],
//                                            'recycle' => $next_recycle,
//                                        ]);
//                                    }
//                                }
//                            }
//                        }
                    }


//                $standard_stage->ajStageTeams()->delete();
//                if (!empty($request->auditor_member)) {
//                    foreach ($request->auditor_member[str_replace('_', '', $stages[$key])] as $a_member) {
//                        $standard_stage->ajStageTeams()->create([
//                            'member_name' => $a_member
//                        ]);
//                    }
//                }

                }


                $data = [
                    'aj_id' => $standard_stage,
                    'status' => 'success',
                ];

                $company = Company::find($request->company_id);
                $company->is_edit_access_to_operations = false;
                $company->save();
                $response = (new ApiMessageController())->successResponse($data, 'AJ has been created');
                return $response;
            } else {
                $data = [
                    'flash_status' => 'warning',
                    'flash_message' => 'Team not available for this standard'
                ];
                $response = $data;
                return $response;
            }
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }


    public function store_multiple(Request $request)
    {


        $checkTeamMember = $this->checkTeamForMultiple($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);
            return $response;
        }


        $aj_date = $request->aj_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->aj_date);


        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'frequency' => 'required|string',
            'offsite' => 'required|string',
//            'job_number' => 'required|max:9|unique:aj_standard_stages',
//            'manday_remarks' => 'string|sometimes',
            'supervisor_involved.stage1' => 'required|string',
//            'expected_date' => Rule::requiredIf(function () use ($request) {
//                if ($request['supervisor_involved']['stage1'] == "yes") {
//                    return true;
//                }
//            }),
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam1.*.required' => 'Please select the auditor member',
            ]);

        if (!$validator->fails()) {
            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }


            if ((isset($request->perposedTeam1['stage1']) && count($request->perposedTeam1['stage1']) >= 2) && isset($request->perposedTeam1['stage2']) && count($request->perposedTeam1['stage2']) >= 2) {


                $standard_stage_return = [];
                foreach ($stages as $stage) {


                    foreach ($request->standard_stage_id as $key => $standard_stage_id) {
                        if ($stage == $key) {

                            $standard_stage = AJStandardStage::findOrFail((int)$standard_stage_id);


//                            if ($standard_stage->job_number != $request->job_number) {
//                                $job_number = DB::table('job_numbers')->where('year', substr($request->job_number, 3, 2))->update([
//                                    'country_code' => substr($request->job_number, 0, 3),
//                                    'year' => substr($request->job_number, 3, 2),
//                                    'code' => substr($request->job_number, 5, 4),
//                                    'created_at' => date('Y-m-d H:i:s'),
//                                ]);
//
//                                if ($standard_stage->audit_type == 'stage_1') {
//                                    $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                                        'job_number' => $request->job_number
//                                    ]);
//                                }
//
//
//                            }


//
//                            if ($standard_stage->job_number != $request->job_number) {
//                                $job_number = DB::table('job_numbers')->where('id', 1)->update([
//                                    'country_code' => substr($request->job_number, 0, 3),
//                                    'year' => substr($request->job_number, 3, 2),
//                                    'code' => substr($request->job_number, 5, 4),
//                                    'created_at' => date('Y-m-d H:i:s'),
//                                ]);
//                            }
                            $expectedDate = $request->expected_date[str_replace('_', '', $stage)] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[str_replace('_', '', $stage)]);
                            $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);

                            if ($request->status == 'created') {
                                $status = $request->status;
                            } elseif ($request->status == 'approved_scheme') {
                                $status = 'approved';
                            } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                                $status = 'applied';
                            } else {
                                $status = 'created';
                            }
                            if ($request->status == 'approved_scheme' && $approvalDate != null && $standard_stage->approval_date != $approvalDate) {
                                $lastApprovalDate = $standard_stage->approval_date;
                            } else {
                                $lastApprovalDate = $standard_stage->last_approval_date;
                            }
                            $standard_stage->update([
                                'certificate_scope' => $request->certificate_scope,
                                'aj_date' => $aj_date->format('Y-m-d'),
                                'manday_remarks' => $request->manday_remarks,
                                'excepted_date' => $expectedDate,
                                'supervisor_involved' => $request->supervisor_involved[str_replace('_', '', $stage)],
                                'evaluator_name' => $request->evaluator_name,
                                'approval_date' => $approvalDate,
                                'last_approval_date' => $lastApprovalDate,
                                'status' => $status,
                                'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                                'job_number' => $request->job_number
                            ]);
                            $standard_stage_return[str_replace('_', '', $stage)] = $standard_stage;


                            foreach ($standard_stage->ajStageTeams as $ajStageTeam) {
                                $ajStageTeam->auditor->update([
                                    'is_available' => 1
                                ]);
                            }

                            $standard_stage->ajStageTeams()->delete();
                            if (!empty($request->perposedTeam1)) {
                                $supervisor = 0;
                                if ($request['supervisor_involved'][str_replace('_', '', $stage)] == "yes") {
                                    $supervisor = 1;
                                }

                                foreach ($request->perposedTeam1[str_replace('_', '', $stage)] as $key => $a_member) {

                                    $standard_stage->ajStageTeams()->create([
                                        'auditor_id' => (int)$a_member,
                                        'supervisor_involved' => $supervisor,
                                        'grade_id' => (int)$request->perposed_team_grade_id[str_replace('_', '', $stage)][$key],
                                        'member_name' => $request->perposed_team_name[str_replace('_', '', $stage)][$key],
                                        'member_type' => $request->perposed_team_type[str_replace('_', '', $stage)][$key],
                                    ]);
                                    $auditor = Auditor::find((int)$a_member)->update([
                                        'is_available' => 0
                                    ]);
                                }
                            }

                            if ($stage == 'stage_2') {

                                if ($request->expected_date[str_replace('_', '', $stage)]) {

                                    $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->get();
                                    $frequency = $request->frequency;
                                    if ($frequency === 'biannual') {
                                        $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                        $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                        $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                        $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                        $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                        $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
                                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];

                                    } else {

                                        $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                        $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                        $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
                                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];

                                    }
                                    foreach ($aj_audit_types as $key => $aj_audit_type) {
                                        foreach ($next_stages as $nextStage) {
                                            if ($nextStage->audit_type == $aj_audit_type) {
                                                $standard_stage = AJStandardStage::findOrFail((int)$nextStage->id);
                                                $standard_stage->update([
                                                    'excepted_date' => $due_date[$key],
                                                ]);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                $data = [
                    'aj_id' => $standard_stage_return,
                    'status' => 'success',
                ];
                $response = (new ApiMessageController())->successResponse($data, 'AJ has been created');
                return $response;
            } else {

                $data = [
                    'status' => 'warning',
                    'message' => 'Team not available for this standard'
                ];
                $response = (new ApiMessageController())->successResponse($data, 'Team not available for this standard');
                return $response;
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }

    private function checkTeamForMultiple($request)
    {
        if (strpos($request['audit_type'], '|') !== false) {
            $stages = explode('|', $request->audit_type);
        } else {
            $stages = [$request['audit_type']];
        }


        if (isset($request->perposedTeam1) && !empty($request->perposedTeam1) && count($request->perposedTeam1) == 2) {
            foreach ($stages as $stage) {

                if (!empty($request->perposed_team_grade_id[str_replace('_', '', $stage)]) && count($request->perposed_team_grade_id[str_replace('_', '', $stage)]) > 0) {
                    $numArray = array_map('intval', $request->perposed_team_grade_id[str_replace('_', '', $stage)]);
                    if (in_array(1, $numArray) && in_array(7, $numArray)) {
                        //both of them are in $arg

                        $standardFamilyCheck = Standards::where('id', $request->standard_number)->first();
                        if ($standardFamilyCheck->standards_family_id == 20) {
                            //Energy
                            $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                            $auditorsArray = array_map('intval', $request->perposedTeam1[str_replace('_', '', $stage)]);
                            $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                                $q->where('grade_id', 7);
                            })->whereNull('deleted_at')->pluck('id')->toArray();
                            $auditorStandardEnergyCodes = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            if ($this->in_array_any($auditorStandardEnergyCodes, $companyStandardEnergyCodes)) {
                                $data = [
                                    'status' => 'success',
                                    'message' => 'Team Member Found for ' . str_replace('_', '', $stage)
                                ];
                                $response = $data;
                                return $response;
                            } else {
                                $data = [
                                    'status' => 'warning',
                                    'message' => 'TE does not have all Company Codes for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;

                            }
                        } elseif ($standardFamilyCheck->standards_family_id == 23) {
                            //Food
                            $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                            $auditorsArray = array_map('intval', $request->perposedTeam1[str_replace('_', '', $stage)]);
                            $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                                $q->where('grade_id', 7);
                            })->whereNull('deleted_at')->pluck('id')->toArray();
                            $auditorStandardFoodCodes = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            if ($this->in_array_any($auditorStandardFoodCodes, $companyStandardFoodCodes)) {

                                $data = [
                                    'status' => 'success',
                                    'message' => 'Team Member Found for ' . str_replace('_', '', $stage)
                                ];
                                $response = $data;
                                return $response;
                            } else {
                                $data = [
                                    'status' => 'warning',
                                    'message' => 'TE does not have all Company Codes for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;

                            }
                        } else {
                            //Generic
//                            dd($request->perposedTeam1[$stage]);
                            $companyStandardCodes = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            $auditorsArray = array_map('intval', $request->perposedTeam1[str_replace('_', '', $stage)]);
                            $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                                $q->where('grade_id', 7);
                            })->whereNull('deleted_at')->pluck('id')->toArray();
                            $auditorStandardCodes = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            if ($this->in_array_any($auditorStandardCodes, $companyStandardCodes)) {

                                $data = [
                                    'status' => 'success',
                                    'message' => 'Team Member Found for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;
                            } else {
                                $data = [
                                    'status' => 'warning',
                                    'message' => 'TE does not have all Company Codes for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;

                            }

                        }
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'For Audit LA and TE is required for ' . str_replace('_', '', $stage)
                        ];

                        $response = $data;
                        return $response;
                    }


                } else {
                    $data = [
                        'status' => 'warning',
                        'message' => 'Please Select Team Members for Audit for ' . str_replace('_', '', $stage)
                    ];

                    $response = $data;
                    return $response;
                }


            }
        }
    }

    public function edit($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {


        $audit_type = str_replace(' ', '_', strtolower($audit_type));

        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        }
        if (is_null($role)) {
            $role = auth()->user()->user_type;
        }

        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {

            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();


            $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
            $company_standards = [];
            $auditors = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
            $certificateStatus = Certificate::where('company_standard_id', $c_std->id)->orderBy('id', 'desc')->first();

            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];
            $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();
            if (!empty($scheme_manager_standards)) {
                if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                    if (!empty($company->companyStandards)) {

                        foreach ($company->companyStandards as $standard) {
                            $company_standards[] = $standard->standard_id;
                        }

//                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                        $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();


                        if (!empty($auditor_standards)) {
                            foreach ($auditor_standards as $auditor_standard) {
                                foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

//                                    if ($auditor_standard->auditor->is_available == 1) {
                                    $members[] = $auditor_standard;
                                    $standard_grades[] = $standard_grade;

                                }
//                                }
                                foreach ($company->companyIAFCodes as $key => $companyIaf) {

                                    $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->where('ias_id', '=', $companyIaf->ias_id)->where('accreditation_id', '=', $companyIaf->accreditation_id)->get();
                                }

                            }
                            //checking the stage1 and stage2 combine aj
                            if ($audit_type == 'stage_1|stage_2') {

                                $combine_tracker = 1;
                            } else {

                                $combine_tracker = 0;
                            }

                            //check the company already have any aj
                            $aj_obj = AuditJustification::where('company_id', $company->id)->first();

                            $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                            $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                            if (!empty($aj_obj)) {
                                //check which standards are in company aj
                                $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                    ->where('standard_id', $standard_number->id)
                                    ->first();

                                $team_members_stage1 = [];
                                if ($audit_type == 'stage_2') {
                                    $aj_standard_stages_obj_stage1 = AJStandardStage::where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standards_obj->id)->first();
                                    $team_members_stage1 = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stages_obj_stage1->id)->get();
                                }

//

                                if (!empty($aj_standards_obj)) {
                                    if ($combine_tracker == 1) {
                                        //get the standard stage stage_1
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                    } else {
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    }

                                    if (!empty($aj_standard_stages_obj)) {
                                        $job_number = $aj_standard_stages_obj->job_number;

                                        //now we want the single audit stage which want to edit
                                        $aj_standard_stage = $aj_standard_stages_obj;
                                        //also need all other audit stages for manday editing


                                        if (!empty($c_std)) {
                                            $outSourceRequestStandardId = OutSourceRequestStandard::where('standard_id', $standard_number->id)->where('company_id', $company->id)->pluck('id')->toArray();

                                            $outsourceRequestsId = OutSourceRequest::whereIn('id', $outSourceRequestStandardId)->where('status', 'approved')->where('expiry_status', 'not expired')->pluck('id')->toArray();

                                            $outsourceRequestAuditorId = OutSourceRequestAuditor::whereIn('outsource_request_id', $outsourceRequestsId)->where('status', 'allocated')->pluck('auditor_id')->toArray();

                                            $auditorId = Auditor::whereIn('id', $outsourceRequestAuditorId)->where('auditor_status', 'active')->pluck('id')->toArray();
                                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditorId)->where('standard_id', $standard_number->id)->where('status', 'active')->groupBy('auditor_id')->pluck('auditor_id')->toArray();


                                            if ($auditorStandardId) {

                                                $auditors = Auditor::whereIn('id', $auditorStandardId)->where('auditor_status', 'active')->get();
                                                $auditors->each(function ($auditor, $key) use ($auditors, $standard_number) {
                                                    $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $standard_number->id)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                                    $auditorStandards = $auditors[$key]->auditorStandards;

                                                    $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards) {

                                                        $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                                    });
                                                });

                                            }
                                        }


//                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
//
//                                            $mandays = $aj_standards_obj->ajStandardStages;
//
//                                        } else {
//                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->where('audit_type', $audit_type)->first();
//                                        }


                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                            if ($audit_type == 'stage_2') {
                                                $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                                $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                            } else {
                                                $mandays_stage_1 = [];
                                                $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                            }

                                        } else {
                                            if ($audit_type == 'stage_2') {
                                                $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                                $mandays = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', $audit_type)->first();
                                            } else {
                                                $mandays_stage_1 = [];
                                                $mandays = AJStandardStage::where('id', (int)$aj_standard_id)->where('audit_type', $audit_type)->first();
                                            }

                                        }

                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                            $aj_stages_manday[] = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                        } else {
                                            $aj_stages_manday[] = $mandays;
                                        }


                                        $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();
                                        $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();
                                        //return to the edit page


                                        $checkApproved = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                        if ($checkApproved->status == 'approved') {
                                            $approvalPerson = Notification::where('request_id', (int)$aj_standard_id)->where('status', 'approved')->where('type', 'audit_justification')->first();
                                            $defaultSchemeManager = SchemeManager::whereHas('standards', function ($q) use ($standard_number) {
                                                $q->where('standard_id', $standard_number->id);
                                            })->with('user')->first();

                                            $pdf = \Barryvdh\DomPDF\Facade::loadView('audit-justification.AjPrintPreview.index', compact('aj_standard_change', 'certificateStatus', 'defaultSchemeManager', 'mandays_stage_1', 'approvalPerson', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');
                                            Storage::put('public/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf', $pdf->output());
                                            $href = 'storage/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf';

                                        } else {
                                            $href = '#';
                                        }


//                                        update approved aj stages

                                        if ($aj_standard_stage->status == 'approved') {

                                            $checkLastRecord = Notification::where('request_id', $aj_standard_stage->id)->where('type', 'audit_justification')->get()->last();


                                            if (!is_null($checkLastRecord) && $checkLastRecord->status == 'approved') {
                                                $notificationUpdate = Notification::where('request_id', $aj_standard_stage->id)->where('type', 'audit_justification')->update([
                                                    'is_read' => true
                                                ]);
                                            }

                                        }


                                        if (auth()->user()->user_type == 'scheme_manager') {

                                            if ($aj_standard_stage->audit_type == 'stage_2') {
                                                $checkApprovedAj = AJStandardStage::whereIn('status', ['approved','rejected'])->where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
//                                                return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));

                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Stage 1 First',

                                                    ]);
                                                }

                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_1') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'stage_2')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Stage 2 First',

                                                    ]);
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_2') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_1')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();

                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    if(is_null($checkApprovedAj)){
                                                        $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_1')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                    }
                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        return redirect()->back()->with([
                                                            'flash_status' => 'warning',
                                                            'flash_message' => 'Please Approved Surveillance 1 First',

                                                        ]);
                                                    }

                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_3' && $c_std->surveillance_frequency == 'biannual') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_2')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();


                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    if(is_null($checkApprovedAj)){
                                                        $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_2')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                    }
                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        return redirect()->back()->with([
                                                            'flash_status' => 'warning',
                                                            'flash_message' => 'Please Approved Surveillance 2 First',

                                                        ]);
                                                    }
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_4' && $c_std->surveillance_frequency == 'biannual') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_3')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();

                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    if(is_null($checkApprovedAj)){
                                                        $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_3')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                    }
                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        return redirect()->back()->with([
                                                            'flash_status' => 'warning',
                                                            'flash_message' => 'Please Approved Surveillance 3 First',

                                                        ]);
                                                    }
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_5' && $c_std->surveillance_frequency == 'biannual') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_4')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if(is_null($checkApprovedAj)){
                                                    $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_4')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                }
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    if(is_null($checkApprovedAj)){
                                                        $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_4')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                    }
                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        return redirect()->back()->with([
                                                            'flash_status' => 'warning',
                                                            'flash_message' => 'Please Approved Surveillance 4 First',

                                                        ]);
                                                    }
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'reaudit') {
                                                if ($c_std->surveillance_frequency == 'biannual') {
                                                    $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_5')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();

                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        if(is_null($checkApprovedAj)){
                                                            $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_5')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                        }
                                                        if (!is_null($checkApprovedAj)) {
                                                            return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                        } else {
                                                            return redirect()->back()->with([
                                                                'flash_status' => 'warning',
                                                                'flash_message' => 'Please Approved Surveillance 5 First',

                                                            ]);
                                                        }
                                                    }
                                                } elseif ($c_std->surveillance_frequency == 'annual') {
                                                    $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_2')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();

                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        if(is_null($checkApprovedAj)){
                                                            $checkApprovedAj =AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_2')->where('aj_standard_id', $aj_standard_stage->old_aj_standard_id)->first();
                                                        }
                                                        if (!is_null($checkApprovedAj)) {
                                                            return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                        } else {
                                                            return redirect()->back()->with([
                                                                'flash_status' => 'warning',
                                                                'flash_message' => 'Please Approved Surveillance 2 First',

                                                            ]);
                                                        }
                                                    }
                                                }

                                            } else {

                                                return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                            }
                                        } else {

                                            return view('audit-justification.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                        }


                                    }
                                }
                            }

                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Auditor standard not available');
                        }
                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Company does not have this standard');
                    }
                }
            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Scheme manager not available for this standard');
            }
        }


    }


    private function in_array_any($needles, $haystack)
    {
        return (bool)array_intersect($needles, $haystack);
        // echo in_array_any( array(3,9), array(5,8,3,1,2) ); // true, since 3 is present
        // echo in_array_any( array(4,9), array(5,8,3,1,2) ); // false, neither 4 nor 9 is present
    }

    private function checkTeam($request)
    {
        if (!empty($request->perposed_team_grade_id) && count($request->perposed_team_grade_id) > 0) {
            $numArray = array_map('intval', $request->perposed_team_grade_id);
            if (in_array(1, $numArray) && in_array(7, $numArray)) {
                //both of them are in $arg

                $standardFamilyCheck = Standards::where('id', $request->standard_number)->first();
                if ($standardFamilyCheck->standards_family_id == 20) {
                    //Energy
                    $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardEnergyCodes = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardEnergyCodes, $companyStandardEnergyCodes)) {
                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } elseif ($standardFamilyCheck->standards_family_id == 23) {
                    //Food
                    $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardFoodCodes = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardFoodCodes, $companyStandardFoodCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } else {
                    //Generic
                    $companyStandardCodes = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardCodes = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardCodes, $companyStandardCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];

                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }

                }
            } else {
                $data = [
                    'status' => 'warning',
                    'message' => 'For Audit LA and TE is required.'
                ];

                $response = $data;
                return $response;
            }


        } else {
            $data = [
                'status' => 'warning',
                'message' => 'Please Select Team Members for Audit.'
            ];

            $response = $data;
            return $response;
        }
    }


    public function update(Request $request)
    {


        $aj_date = $request->aj_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->aj_date);


        $checkTeamMember = $this->checkTeam($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);

            return $response;
        }
        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'offsite' => 'required|string',
//            'job_number' => 'required|string',
//            'manday_remark' => 'string|sometimes',
            'supervisor_involved.stage1' => 'required|string',
            'std_number' => Rule::requiredIf(function () use ($request) {
                return $request['ch_std'] == "yes";
            }),
//            'expected_date.0' => Rule::requiredIf(function () use ($request) {
//                return true;
////                return $request['supervisor_involved[stage1]'] == "yes";
//            }),
//           'evaluator_name' => 'required',
//           'approval_date' => 'required',
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam.*.required' => 'Please select the auditor member',
            ]);

        if (!$validator->fails()) {


            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }

            if (!empty($request->perposedTeam)) {
                if (!is_null($request->recycle)) {
                    $standard_stage = AJStandardStage::where('id', (int)$request->standard_stage_id)->where('recycle', (int)$request->recycle)->first();
                } else {
                    $standard_stage = AJStandardStage::where('id', (int)$request->standard_stage_id)->whereNull('recycle')->first();

                }


//                $standard_stage = AJStandardStage::findOrFail((int)$request->standard_stage_id);

//                if ($standard_stage->job_number != $request->job_number && auth()->user()->user_type == 'scheme_manager') {
//
//                    $jobNumber = AJStandardStage::where('job_number', $request->job_number)->where('aj_standard_id', '!=', $standard_stage->aj_standard_id)->first();
//
//                    if (!empty($jobNumber)) {
//                        $data = [
//                            'flash_status' => 'warning',
//                            'flash_message' => 'job number already exist'
//                        ];
//                        $response = $data;
//                        return $response;
//                    } else {
//
////                        if ($standard_stage->audit_type == 'stage_1') {
//                        $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                            'job_number' => $request->job_number
//                        ]);
////                        }
//
//                    }
//
//
//                } else {
////                    if ($standard_stage->audit_type == 'stage_1') {
//                    $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                        'job_number' => $request->job_number
//                    ]);
////                    }
//                }
                $expectedDate = $request->expected_date[0] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[0]);
                $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);
                $actual_expected_date = $request->actual_expected_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->actual_expected_date);
                $valid_till_date = $request->valid_till_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->valid_till_date);
                $company = Company::where('id', $request->company_id)->first();


                if ($request->status == 'created') {
                    $status = $request->status;
                } elseif ($request->status == 'approved_scheme') {
                    $status = 'approved';
                } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                    $status = 'applied';
                } else {
                    $status = 'created';
                }

                if (auth()->user()->user_type == 'scheme_manager' && $request->status == 'rejected') {
                    $certificationScope = $company->scope_to_certify;
                } else {
                    $certificationScope = $request->certificate_scope;
                    $company->scope_to_certify = $request->certificate_scope;
                    $company->save();
                }
                $standard_stage->update([
                    'certificate_scope' => $certificationScope,
                    'aj_date' => $aj_date->format('Y-m-d'),
                    'manday_remarks' => $request->manday_remarks,
                    'excepted_date' => $expectedDate,
                    'valid_till_date' => $valid_till_date,
                    'actual_expected_date' => $actual_expected_date,
                    'supervisor_involved' => $request->supervisor_involved['stage1'],
                    'evaluator_name' => $request->evaluator_name,
                    'approval_date' => $approvalDate,
                    'last_approval_date' => $standard_stage->approval_date,
                    'status' => $status,
                    'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                    'job_number' => $request->job_number,

                ]);
                if ($request['audit_type'] === 'surveillance_1' || $request['audit_type'] === 'surveillance_2' || $request['audit_type'] === 'surveillance_3' || $request['audit_type'] === 'surveillance_4' || $request['audit_type'] === 'surveillance_5' || $request['audit_type'] === 'reaudit') {
                    $standard_stage->ajStageChange()->delete();
                    $standard_stage->ajStageChange()->updateOrCreate([
                        'name' => $request->ch_name,
                        'scope' => $request->ch_scp,
                        'location' => $request->ch_loc,
                        'version' => $request->ch_std,
                        'standards' => $request->std_number,
                        'remarks' => $request->remarks,
                    ]);
                }
                foreach ($standard_stage->ajStageTeams as $ajStageTeam) {
                    $ajStageTeam->auditor->update([
                        'is_available' => 1
                    ]);
                }

                $standard_stage->ajStageTeams()->delete();


                if (!empty($request->perposedTeam)) {

                    $supervisor = 0;
                    if ($request['supervisor_involved']['stage1'] == "yes") {
                        $supervisor = 1;
                    }
                    foreach ($request->perposedTeam as $key => $a_member) {
                        $standard_stage->ajStageTeams()->create([
                            'auditor_id' => (int)$a_member,
                            'supervisor_involved' => $supervisor,
                            'grade_id' => (int)$request->perposed_team_grade_id[$key],
                            'member_name' => $request->perposed_team_name[$key],
                            'member_type' => $request->perposed_team_type[$key],
                        ]);

                        $auditor = Auditor::find((int)$a_member)->update([
                            'is_available' => 0
                        ]);
                    }

                    // foreach ($request->auditor_member[str_replace('_', '', $request['audit_type'])] as $key => $a_member) {
                    //     $standard_stage->ajStageTeams()->create([
                    //         'auditor_id' => (int)$request->auditor_id[$key],
                    //         'member_name' => $a_member
                    //     ]);
                    //     Auditor::find((int)$request->auditor_id[$key])->update([
                    //         'is_available' => 0
                    //     ]);
                    // }

                }


                if (!is_null($request->recycle)) {
                    $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->where('recycle', (int)$request->recycle)->get();
                } else {
                    $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->whereNull('recycle')->get();

                }


                $frequency = $request->frequency;

                if ($request->expected_date[0] && $request->audit_type == "stage_2" && $request->status == 'approved') {

                    if ($frequency === 'biannual') {
                        $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($request->expected_date[0])));
                        $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                        $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                        $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                        $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                        $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];

                    } else {

                        $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($request->expected_date[0])));
                        $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($request->expected_date[0])));
                        $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($request->expected_date[0])));
                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];

                    }


                    foreach ($aj_audit_types as $key => $aj_audit_type) {
                        foreach ($next_stages as $stage) {
                            if ($stage->audit_type == $aj_audit_type) {
                                $standard_stage1 = AJStandardStage::findOrFail((int)$stage->id);
                                $standard_stage1->update([
                                    'excepted_date' => $due_date[$key],
                                ]);
                            }
                        }
                    }

                }


                $data = [
                    'aj_id' => $standard_stage,
                    'status' => 'success',
                ];

                $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated');
                return $response;
            } else {
                $data = [
                    'flash_status' => 'warning',
                    'flash_message' => 'Team not available for this standard'
                ];
                $response = $data;
                return $response;
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }


    public function show($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {

        $audit_type = str_replace(' ', '_', strtolower($audit_type));

        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }
        if (is_null($role)) {
            $role = auth()->user()->user_type;
        }
        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {
            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();


            $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
            $certificateStatus = Certificate::where('company_standard_id', $c_std->id)->orderBy('id', 'desc')->first();
            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];

            if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                if (!empty($company->companyStandards)) {

                    foreach ($company->companyStandards as $standard) {
                        $company_standards[] = $standard->standard_id;
                    }
                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                    if (!empty($auditor_standards)) {
                        foreach ($auditor_standards as $auditor_standard) {
                            foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {
                                $members[] = $auditor_standard;
                                $standard_grades[] = $standard_grade;
                            }
                            foreach ($auditor_standard->auditorStandardCodes()->where('status', '!=', 'full')->get() as $standard_code) {

                                $standard_codes[] = $standard_code;

                            }
                        }
                        //checking the stage1 and stage2 combine aj

                        if ($audit_type == 'stage_1|stage_2') {

                            $combine_tracker = 1;
                        } else {

                            $combine_tracker = 0;
                        }

                        //check the company already have any aj
                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                        $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                        $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                        if (!empty($aj_obj)) {
                            //check which standards are in company aj
                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                ->where('standard_id', $standard_number->id)
                                ->first();
                            $team_members_stage1 = [];
                            if ($audit_type == 'stage_2') {
                                $aj_standard_stages_obj_stage1 = AJStandardStage::where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standards_obj->id)->first();
                                $team_members_stage1 = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stages_obj_stage1->id)->get();
                            }

                            if (!empty($aj_standards_obj)) {
                                if ($combine_tracker == 1) {
                                    //get the standard stage stage_1
                                    $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                } else {
                                    $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                }

                                if (!empty($aj_standard_stages_obj)) {
                                    //now we want the single audit stage which want to edit
                                    $aj_standard_stage = $aj_standard_stages_obj;
                                    //also need all other audit stages for manday editing


                                    if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                        if ($audit_type == 'stage_2') {
                                            $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                        } else {
                                            $mandays_stage_1 = [];
                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                        }

                                    } else {
                                        if ($audit_type == 'stage_2') {
                                            $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                            $mandays = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', $audit_type)->first();
                                        } else {
                                            $mandays_stage_1 = [];
                                            $mandays = AJStandardStage::where('id', (int)$aj_standard_id)->where('audit_type', $audit_type)->first();
                                        }

                                    }

                                    $aj_stages_manday[] = $mandays;
                                    $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();

                                    $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();

                                    $checkApproved = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    if ($checkApproved->status == 'approved') {
                                        $approvalPerson = Notification::where('request_id', (int)$aj_standard_id)->where('status', 'approved')->where('type', 'audit_justification')->first();
                                        $defaultSchemeManager = SchemeManager::whereHas('standards', function ($q) use ($standard_number) {
                                            $q->where('standard_id', $standard_number->id);
                                        })->with('user')->first();

                                        $pdf = \Barryvdh\DomPDF\Facade::loadView('audit-justification.AjPrintPreview.index', compact('aj_standard_change', 'certificateStatus', 'defaultSchemeManager', 'mandays_stage_1', 'approvalPerson', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');
                                        Storage::put('public/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf', $pdf->output());
                                        $href = 'storage/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf';

                                    } else {
                                        $href = '#';
                                    }

                                    //return to the edit page
//                                    return view('audit-justification.AjPrintPreview.index', compact('mandays_stage_1', 'approvalPerson', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');

//                                    //return to the edit page
                                    return view('audit-justification.show', compact('certificateStatus', 'href', 'notifications', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                }
                            }
                        }

                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Auditor standard not available');
                    }
                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Company does not have this standard');
                }
            }
        }

    }

    public function partialRemarks(Request $request)
    {

        $auditorStandards = AuditorStandard::where('id', $request->auditor_standard_id)->whereNull('deleted_at')->first();

        $data = view('audit-justification.modals.codes', compact('auditorStandards'))->render();

        $response['status'] = 'success';
        $response['data'] = [
            'html' => $data,
        ];
//        $data = [
//            'status' => 'success',
//            'partial_remarks' => $partial_remarks
//        ];

//        $response = (new ApiMessageController())->successResponse($data, '');
        return $response;
    }

    public function deleteAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {


        $standard_stage = AJStandardStage::find((int)$aj_standard_id);

        $standard_stage->update([
            'certificate_scope' => NULL,
            'aj_date' => NULL,
            'manday_remarks' => NULL,
            'supervisor_involved' => 'no',
            'evaluator_name' => NULL,
            'approval_date' => NULL,
            'status' => 'Not Applied',
            'user_id' => NULL,

        ]);


        $notifications = Notification::where('request_id', $standard_stage->id)->get();
        if (!empty($notifications) && count($notifications) > 0) {
            foreach ($notifications as $notification) {
                $notification->delete();
            }
        }
//        $standard_stage->delete();
        $standard_stage->ajStageChange()->delete();
        $standard_stage->AJRemarks()->delete();
        $standard_stage->ajStageTeams()->delete();
        return redirect()->back()->with(['flash_status' => 'success', 'flash_message' => 'AJ has been deleted successfully']);
    }


    public function addRemarks(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'remark' => 'required|string',
            'aj_id.*' => 'required'
        ]);


        if (!$validator->fails()) {
            $get_company = Company::where('id', $request->company_id)->first();
            foreach ($request['aj_id'] as $aj_id) {

                $aj = AJStandardStage::with('ajStandard.standard.scheme_manager.user')->where('id', $aj_id)->first();
                $aj->status = $request->status;
                $this->updateJobNumber($request, $aj);
                $aj->save();
                $aj->AJRemarks()->create([
                    'model' => AJStandardStage::class,
                    'user_id' => Auth()->user()->id,
                    'remarks' => $request['remark']
                ]);


            }
            if (auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'scheme_coordinator') {
                if ($request->status == 'applied') {
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'unapproved',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(New)'
                    ]);
                } elseif ($request->status == 'resent') {
                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'resent',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Re-Submitted)'

                    ]);
                    if (!is_null($notification)) {
                        $notification->is_read = true;
                        $notification->save();
                    }
                }


            } else if (auth()->user()->user_type == 'scheme_manager') {
                if ($request->status == 'rejected') {

                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $notification->sent_by,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'rejected',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Rejected)'

                    ]);
                    $notification->is_read = true;
                    $notification->save();
                } elseif ($request->status == 'approved') {

                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $notification->sent_by,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'approved',
                        'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Approved)'

                    ]);
                    $notification->is_read = true;
                    $notification->save();
                }


            }

            $data = [
                'status' => 'success'
            ];

            $response = (new ApiMessageController())->successResponse($data, 'AJ has been created and notification has been sent.');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function updateRemarks(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'remark' => 'required|string',
            'aj_id.*' => 'required'
        ]);


        if (!$validator->fails()) {
            $get_company = Company::where('id', $request->company_id)->first();
            foreach ($request['aj_id'] as $aj_id) {

                $aj = AJStandardStage::with('ajStandard.standard.scheme_manager.user')->where('id', $aj_id)->first();
                $aj->status = $request->status;
                $this->updateJobNumber($request, $aj);
                $aj->save();
                $aj->AJRemarks()->create([
                    'model' => AJStandardStage::class,
                    'user_id' => Auth()->user()->id,
                    'remarks' => $request['remark']
                ]);

                $ajStandard = AJStandard::where('id', $aj->aj_standard_id)->whereNull('deleted_at')->first();
                $standard = Standards::where('id', $ajStandard->standard_id)->first();

//                dd($aj->ajStandard->standard->name);
                if (auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'scheme_coordinator') {
                    if ($request->status == 'applied') {
                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $standard->id,
                            'request_id' => $aj->id,
                            'status' => 'unapproved',
                            'sent_message' => '' . $get_company->name . ' - ' . $standard->name . '(New)'

                        ]);
                    } elseif ($request->status == 'resent') {
                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'resent',
                            'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Re-Submitted)'

                        ]);
                        if (!is_null($notification)) {
                            $notification->is_read = true;
                            $notification->save();
                        }
                    }


                } else if (auth()->user()->user_type == 'scheme_manager') {
                    if ($request->status == 'rejected') {

                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $notification->sent_by,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'rejected',
                            'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Rejected)'

                        ]);
                        $notification->is_read = true;
                        $notification->save();
                    } elseif ($request->status == 'approved') {

                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();


                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $notification->sent_by,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => '/edit/audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'approved',
                            'sent_message' => '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Approved)'

                        ]);
                        $notification->is_read = true;
                        $notification->save();
                    }


                }

            }


            $data = [
                'status' => 'success'
            ];

            $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated and notification has been sent.');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    private function updateJobNumber($request, $stage)
    {
        if ($request->status == 'approved' && $stage->audit_type == 'stage_1') {

            $auditJustification = AuditJustification::where('id', $stage->ajStandard->audit_justification_id)->first();
            $company = Company::where('id', $auditJustification->company_id)->with('country')->first(['id', 'country_id']);
            AJStandardStage::where('aj_standard_id', $stage->aj_standard_id)->update([
                'job_number' => $this->jobNumber($company->country->iso)
            ]);
        }
    }


}
