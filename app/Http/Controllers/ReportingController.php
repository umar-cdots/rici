<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\AuditActivityStages;
use App\Auditor;
use App\AuditorConfidentialityAgreement;
use App\AuditorDocument;
use App\AuditorEducation;
use App\AuditorEmploymentHistory;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\AuditorStandardGrade;
use App\AuditorStandardTraining;
use App\AuditorStandardWitnessEvaluation;
use App\Countries;
use App\Grades;
use App\IAS;
use App\Regions;
use App\Standards;
use Illuminate\Http\Request;
use App\Traits\AccessRights;
class ReportingController extends Controller
{
    use AccessRights;
    public function index()
    {
        // $this->VerifyRights();
        $regions = Regions::all();
        $countries = Countries::all();
        // if($this->redirectUrl){return redirect($this->redirectUrl);}
        return view('reports.index', compact('regions', 'countries'), ['check_rights' => $this->check_employee_rights] );
    }

    public function auditorsList(Request $request)
    {

        $auditors = Auditor::where('first_name', 'LIKE', '%' . $request->get('name') . '%')->orWhere('region_id', 'LIKE', '%' . $request->get('region_id') . '%')->orWhere('country_id', 'LIKE', '%' . $request->get('country_id') . '%')->orWhere('auditor_status', 'LIKE', '%' . $request->get('status') . '%')->with('auditorStandards', 'country', 'region')->paginate(2);

        $auditor_standard_codes = [];
        $auditor_standard_grades = [];
        $auditors_standard = [];

        foreach ($auditors as $key => $auditor) {
            $auditors_standard[$key] = AuditorStandard::where('auditor_id', $auditor->id)->first();
        }

        foreach ($auditors_standard as $index => $auditor_standard) {
            $auditor_standard_codes[$index] = AuditorStandardCode::where('auditor_standard_id', $auditor_standard->id)->with('ias')->first();
            $auditor_standard_grades[$index] = AuditorStandardGrade::where('auditor_standard_id', $auditor_standard->id)->with('grade')->first();
        }


        $accredations = [];

        foreach ($auditor_standard_codes as $key => $auditor_standard_code) {
            if (!empty($auditor_standard_code)) {
                $accredations[$key] = Accreditation::where('id', $auditor_standard_code->accreditation_id)->first();
            }
        }
        $paginations = "{$auditors->links('vendor.pagination.default')}";

        $data_list = view('reports.list.auditor-list', compact('auditors', 'accredations', 'auditor_standard_codes', 'auditor_standard_grades'))->render();
        $data = [
            'data' => $data_list,
            'paginations' => $paginations
        ];
        $response = (new ApiMessageController())->successResponse($data, 'Report generated successfully');

        return $response;
    }
}
