<?php

namespace App\Http\Controllers;

use App\EnergyFamilySheet;
use App\FoodCategory;
use App\FoodFamilySheet;
use App\Imports\EnergyImport;
use App\Imports\FoodImport;
use App\StandardFamilySheet;
use App\Standards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class FoodFamilyController extends Controller
{
    public function importExcel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if (!$validator->fails()) {

            $food_sheet_family = new StandardFamilySheet();
            FoodFamilySheet::truncate();
            if ($request->hasFile('file')) {
                $image = $request->file('file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/sheet_documents');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                StandardFamilySheet::where('name', 'food_family')->delete();
                $food_sheet_family->name = 'food_family';
                $food_sheet_family->file = $name;
                $food_sheet_family->save();
            }
            $file = $request->file('file');
            Excel::import(new FoodImport(), public_path('/uploads/sheet_documents/sample_generic_food.xlsx'));
            $sheets = FoodFamilySheet::all();
            $records = view('manday-calculator.sheets-partials.food-sheet', compact('sheets'))->render();
            $data = [
                'status' => 200,
                'records' => $records,
                'food_sheet_family' => $food_sheet_family->file,

            ];
            $response = $data;

        } else {
            FoodFamilySheet::truncate();
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function filter(Request $request)
    {
        $records = FoodFamilySheet::where('standard_id', $request->id)->with(['standard', 'food_category'])->get();

        return response()->json([
            'status' => 'success',
            'data' => $records
        ]);
    }
}
