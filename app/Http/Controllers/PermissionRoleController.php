<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionRoleController extends Controller
{
    public function index(){
        $roles = Role::all();
        //dd($roles[0]->getAllPermissions());
        return view('role.permissions-to-role', compact('roles'));
    }

    public function create(){
        $permissions = Permission::all();
        $roles = Role::all();
        return view('role.add-permissions-to-role', compact('permissions', 'roles'));
    }

    public function store(Request $request){
        $this->validator($request->all())->validate();
        $role = Role::findById($request->role);
        $role->syncPermissions($request->permissions);

        return redirect()->route('permissions.role.index')->with('success', 'Permissions has been granted to role successfully.');
    }

    public function edit($id){
        $role = Role::findById($id);
        $permissions = Permission::all();

        return view('role.edit-permissions-to-role', compact('role', 'permissions'));
    }

    public function update(Request $request, $id){
        $this->validator($request->all())->validate();
        $role = Role::findById($id);
        $role->syncPermissions($request->permissions);

        return back()->with('success', 'Role permissions has been updated.');
    }

    public function validator(array $data){
        return Validator::make($data, [
           'role' => 'required|integer',
           'permissions.*' => 'required|integer'
        ]);
    }
}
