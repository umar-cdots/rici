<?php

namespace App\Http\Controllers\Reports;

use App\Auditor;
use App\Regions;
use App\AuditorStandard;
use App\Standards;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportAjaxController extends Controller
{
    public function CountryFoodAuditors(Request $request)
    {
        $food_auditors = [];
        $auditors = Auditor::where('role', 'auditor')->with('auditorStandards')->get();


        if (!empty($auditors)) {
            foreach ($auditors as $auditor) {
                foreach ($auditor->auditorStandards as $standard) {
                    if (strtolower(str_slug($standard->standardFamily->name)) == 'food') {
                        $food_auditors[] = $auditor;
                    }

                }
            }

            $food_auditors = array_unique($food_auditors);
        }


        return response()->json([
            'data' => $food_auditors,
            'status' => 'success'
        ]);

    }

    public function CountryEnergyAuditors(Request $request)
    {
        $food_auditors = [];
        $auditors = Auditor::where('role', 'auditor')->with('auditorStandards')->get();


        if (!empty($auditors)) {
            foreach ($auditors as $auditor) {
                foreach ($auditor->auditorStandards as $standard) {
                    if ($standard->standardFamily->name == 'Energy Management') {
                        $energy_auditors[] = $auditor;
                    }

                }
            }

            $energy_auditors = array_unique($energy_auditors);
        }


        return response()->json([
            'data' => $energy_auditors,
            'status' => 'success'
        ]);

    }

    public function getAuditorStandards(Request $request)
    {


        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $auditor = Auditor::findOrFail($request->id);   
            if(!is_null($auditor)){
                $auditor->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
            }
        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $auditor = Auditor::findOrFail($request->id);
            if(!is_null($auditor)){
                $auditor->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->where('auditor_id', $auditor->id)->get();
                foreach ($auditor->auditorStandards as $key => $auditorStandard) {
                    $auditor->auditorStandards[$key]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                }
            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $auditor = Auditor::findOrFail($request->id);
            $auditor->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
        }
        return response()->json(['status' => 'success', 'data' => $auditor->auditorStandards]);
    }
}
