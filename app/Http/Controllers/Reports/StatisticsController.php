<?php

namespace App\Http\Controllers\Reports;

use App\IASCertificateError;
use App\IASCertificateStats;
use App\IASCertificateSuccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{
    public function statisticsCount(Request $request)
    {

        ini_set('max_execution_time', '600');
        $records = IASCertificateStats::orderBy('id', 'desc');
        if (isset($request->date) && !is_null($request->date)) {
            $records = $records->whereDate('upload_date', $request->date);
        }
        $records = $records->get();
        return view('reports.ias.stats')->with(['records' => $records]);

    }

    public function statisticsError(Request $request)
    {

        ini_set('max_execution_time', '600');
        $records = IASCertificateError::orderBy('id', 'desc');
        if (isset($request->date) && !is_null($request->date)) {
            $records = $records->whereDate('upload_date', $request->date);
        }
        $records = $records->with('company', 'standard')->get();
        return view('reports.ias.stats-error')->with(['records' => $records]);

    }

    public function statisticsSuccess(Request $request)
    {

        ini_set('max_execution_time', '600');
        $records = IASCertificateSuccess::orderBy('id', 'desc');
        if (isset($request->date) && !is_null($request->date)) {
            $records = $records->whereDate('upload_date', $request->date);
        }
        $records = $records->with('company', 'standard','aj_standard_stage')->get();
        return view('reports.ias.stats-success')->with(['records' => $records]);

    }
}
