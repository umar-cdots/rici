<?php

namespace App\Http\Controllers\Reports;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandards;
use App\Countries;
use App\Regions;
use App\RegionsCountries;
use App\Standards;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyJobNumberController extends Controller
{
    public function __invoke(Request $request)
    {
        ini_set('max_execution_time', '600');
        $issue_from = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '0', '10'));
        $issue_to = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '13'));


        $due_from = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '0', '10'));
        $due_to = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '13'));


        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        })->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {

                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit', function ($q) {
                                        $q->where('status', '!=', 'approved');
                                    })
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->get();
                            }

                            $auditTypes = ['stage_1'];

                            if (!is_null($request->aj_status)) {

                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }


                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })

                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'applied', 'rejected'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })

                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'applied', 'rejected'])->get();
                                    }
                                }

                            }


                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->get();
                            }


                            $auditTypes = ['stage_1'];

                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        })->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->get();
                            }


                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->get();
                            }
                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                        })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                        })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                        ->get();

                    $auditTypes = ['stage_1'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                        whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['applied', 'rejected', 'approved'])->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);


            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        })->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->get();
                            }

                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                            })->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->get();
                                    }
                                }
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->get();
                            }


                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        }))
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->get();
                                    }
                                }
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        })->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->get();
                            }


                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->get();
                                    }
                                }
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->get();
                            }
                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)

//                                            ->whereIn('status', ['approved', 'rejected', 'applied'])
                                            ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->get();


                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->get();

                    $auditTypes = ['stage_1'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                        whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                            ->whereIn('audit_type', $auditTypes)

                            ->get();
                    }


                }

            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        })->where('region_id', auth()->user()->region_id)->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->get();
                            }

                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->get();
                            }


                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        })->where('region_id', auth()->user()->region_id)->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->get();
                            }


                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                                $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                            })->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
//                                    ->where('status', 'approved')
                                    ->get();
                            }
                            $auditTypes = ['stage_1'];
                            if (!is_null($request->aj_status)) {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->where('status', $request->aj_status)->get();
                                    }
                                }
                            } else {
                                if (!is_null($request->aj_due_date)) {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                                    }
                                } else {
                                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                                        whereHas('ajStandard', function ($q) use ($companyStd) {
                                            $q->where('standard_id', $companyStd->standard_id)
                                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                    $q->where('company_id', $companyStd->company_id)
                                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                                });
                                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['approved', 'rejected', 'applied'])->get();
                                    }
                                }
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])
                    ->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                        ->get();

                    $auditTypes = ['stage_1'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::
                        whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })
//                        whereHas('postAudit', function ($q) use ($companyStd) {
//                            $q->where('company_id', $companyStd->company_id)
//                                ->where('standard_id', $companyStd->standard_id);
//                        })
                            ->whereIn('audit_type', $auditTypes)->whereIn('status', ['applied', 'rejected', 'approved'])->get();
                    }


                }
            }
        }


        return view('reports.company.post-audit.job-number.index')->with(['companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }
}
