<?php

namespace App\Http\Controllers\Reports;

use App\Invoice;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class InvoiceController extends Controller
{

    public function index(Request $request)
    {

        if (auth()->user()->user_type === 'operation_manager' && auth()->user()->region->title === 'Pakistan Region') {
            ini_set('max_execution_time', '600');
            $invoice_date_from = $request->invoice_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->invoice_date, '0', '10'));
            $invoice_date_to = $request->invoice_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->invoice_date, '13'));

            $due_date_from = $request->due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->due_date, '0', '10'));
            $due_date_to = $request->due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->due_date, '13'));

            if (!empty($request->all())) {
                $invoices = Invoice::orderBy('id', 'desc')
                    ->where(function ($query) use ($request, $invoice_date_from, $invoice_date_to, $due_date_from, $due_date_to) {
                        if (!empty($request->invoice_status)) {
                            $query->where('invoice_status', $request->invoice_status === '1' ? 1 : 0);
                        }
                        if (!is_null($request->invoice_date)) {
                            $query->whereBetween('invoice_date', [$invoice_date_from->format('Y-m-d'), $invoice_date_to->format('Y-m-d')]);
                        }
                        if (!is_null($request->due_date)) {
                            $query->whereBetween('due_date', [$due_date_from->format('Y-m-d'), $due_date_to->format('Y-m-d')]);
                        }

                    })->with('company')->get();
            } else {
                $invoices = Invoice::orderBy('id', 'desc')->get();

            }

            return view('reports.invoices.index')->with(['invoices' => $invoices]);
        }
        return redirect()->back()->with([
            'status' => 'success',
            'message' => 'You are unauthorized for this request.'
        ]);

    }
}
