<?php

namespace App\Http\Controllers\Reports;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandards;
use App\Countries;
use App\RegionalOperationCoordinatorCities;
use App\Regions;
use App\RegionsCountries;
use App\Standards;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveillanceAndReauditDueController extends Controller
{
    public function __invoke(Request $request)
    {
        ini_set('max_execution_time', '600');
        $due_from = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '0', '10'));
        $due_to = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '13'));
        $expectedDateLessThan = $request->date_less_than_aj_due_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->date_less_than_aj_due_date);


        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }
                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();
                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);


            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();


                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();


                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('reportCertificate', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage:: whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                    }


                }
            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();


                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                    }


                }
            }
        }
        return view('reports.company.post-audit.surveillance-and-reaudit-due.index')->with(['companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }

    public function newReport(Request $request)
    {
        $title = 'Surveillance and Reaudit Due Report';
        ini_set('max_execution_time', '600');
        $due_from = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '0', '10'));
        $due_to = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '13'));
        $expectedDateLessThan = $request->date_less_than_aj_due_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->date_less_than_aj_due_date);


        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }
                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();
                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type === 'management') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else if (auth()->user()->user_type == 'management') {
                $scheme_manager_id = 1;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);


            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();


                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();


                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('reportCertificate', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage:: whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', date('Y-m-d'))->get();
                    }


                }

            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();


                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {

                if (auth()->user()->user_type == 'operation_coordinator') {
                    $cityOperationsCoordinator = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', auth()->user()->operation_coordinator->id)
                        ->whereNull('deleted_at')->pluck('city_id')->toArray();

                    $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)
                        ->whereIn('city_id', $cityOperationsCoordinator)
                        ->with(['region', 'country', 'city', 'primaryContact'])
                        ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();
                } else {
                    $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])
                        ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();
                }


                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', date('Y-m-d'))->get();
                    }


                }
            }
        }
        return view('reports.company.post-audit.surveillance-and-reaudit-due-actual-audit.index')->with(['title' => $title, 'companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }

    public function notCertifiedClients(Request $request)
    {
        ini_set('max_execution_time', '600');
        $due_from = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '0', '10'));
        $due_to = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '13'));
        $expectedDateLessThan = $request->date_less_than_aj_due_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->date_less_than_aj_due_date);


        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereStatus('approved')
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->get();

                        $auditTypes = ['stage_1', 'stage_2'];

                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->get();
                                }
                            }

                        }

                    }
                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    });
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereStatus('approved')
                            ->whereIn('type', ['base', 'single'])
                            ->get();
                        $auditTypes = ['stage_1', 'stage_2'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereStatus('approved')
                        ->whereIn('type', ['base', 'single'])
                        ->get();

                    $auditTypes = ['stage_1', 'stage_2'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type === 'management') {

            $authUser = auth()->user();
            $scheme_manager_id = $authUser->user_type === 'scheme_coordinator'
                ? $authUser->scheme_coordinator->scheme_manager->id
                : ($authUser->user_type === 'management'
                    ? 1
                    : $authUser->scheme_manager->id);


// Preload necessary data
            $get_companies = Company::where('name', '<>', '')
                ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                    return $q->where('scheme_manager_id', $scheme_manager_id);
                })
                ->get();

            $get_regions = Regions::all();
            $get_countries = Countries::all();

            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                return $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = $get_standards->pluck('id')->toArray();

            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)
                ->groupBy('unique_code')
                ->get(['unique_code']);

// Process request if data is provided
            if (!empty($request->all())) {
                $companies = Company::where('name', '<>', '')
                    ->with(['region', 'country', 'city', 'primaryContact'])
                    ->when(!empty($request->region_id), function ($query) use ($request) {
                        return $query->whereIn('region_id', $request->region_id);
                    })
                    ->when(!empty($request->country_id), function ($query) use ($request) {
                        return $query->whereIn('country_id', $request->country_id);
                    })
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        return $q->where('scheme_manager_id', $scheme_manager_id);
                    })
                    ->get();

                foreach ($companies as $company) {
                    $companyStandardsQuery = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereStatus('approved')
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            return $q->where('scheme_manager_id', $scheme_manager_id);
                        });

                    if (!empty($request->standard_id)) {
                        $companyStandardsQuery->whereIn('standard_id', $request->standard_id);
                    }

                    $company->companyStandards = $companyStandardsQuery->get();

                    $auditTypes = ['stage_1', 'stage_2'];
                    foreach ($company->companyStandards as $companyStandard) {
                        $ajStandardStageQuery = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStandard) {
                            $q->where('standard_id', $companyStandard->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStandard) {
                                    $q->where('company_id', $companyStandard->company_id)
                                        ->where('standard_id', $companyStandard->standard_id)
                                        ->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes);

                        if (!is_null($request->aj_due_date)) {
                            $ajStandardStageQuery->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                        } elseif (!is_null($request->date_less_than_aj_due_date)) {
                            $ajStandardStageQuery->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'));
                        }

                        $companyStandard->ajStandardStage = $ajStandardStageQuery->get();
                    }
                }
            } else {
                $companies = $get_companies->load(['region', 'country', 'city', 'primaryContact']);
                foreach ($companies as $company) {
                    $company->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereStatus('approved')
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            return $q->where('scheme_manager_id', $scheme_manager_id);
                        })
                        ->get();

                    $auditTypes = ['stage_1', 'stage_2'];
                    foreach ($company->companyStandards as $companyStandard) {
                        $companyStandard->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStandard) {
                            $q->where('standard_id', $companyStandard->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStandard) {
                                    $q->where('company_id', $companyStandard->company_id)
                                        ->where('standard_id', $companyStandard->standard_id)
                                        ->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->get();
                    }
                }
            }




        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereStatus('approved')
                            ->get();


                        $auditTypes = ['stage_1', 'stage_2'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereStatus('approved')
                            ->get();

                        $auditTypes = ['stage_1', 'stage_2'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereStatus('approved')
                        ->get();

                    $auditTypes = ['stage_1', 'stage_2'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->get();
                    }


                }
            }
        }
        return view('reports.company.post-audit.not-certified-clients.index')->with(['companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);

    }

    public function newReportApprovedTrue(Request $request)
    {
        $title = 'Surveillance and Reaudit Conducted Report';
        ini_set('max_execution_time', '600');
        ini_set('memory_limit', '-1');
        $due_from = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '0', '10'));
        $due_to = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '13'));
        $expectedDateLessThan = $request->date_less_than_aj_due_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->date_less_than_aj_due_date);



        $expected_due_from = $request->date_less_than_aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->date_less_than_aj_due_date, '0', '10'));
        $expected_due_to = $request->date_less_than_aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->date_less_than_aj_due_date, '13'));
        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereStatus('approved')
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                            })->get();

                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                            , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                            'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                            'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                        ];

                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
                                        ->whereBetween('excepted_date', [$expected_due_from->format('Y-m-d'), $expected_due_to->format('Y-m-d')])->get();
//                                        ->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }
                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                            })->get();
                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                            , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                            'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                            'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                        ];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
//                                    ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])
                                    ->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
                                        ->whereBetween('excepted_date', [$expected_due_from->format('Y-m-d'), $expected_due_to->format('Y-m-d')])->get();
//                                        ->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                        })->get();

                    $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                        , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                        'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                        'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                    ];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);


            if (!empty($request->all())) {

                $companies = Company::query();
                $companies = $companies->whereRaw('name <> ""')
                    ->select('id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify')
                    ->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })
//                    ->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
//                        $q->where('report_status', 'new');
//                        if (!is_null($request->certificate_status)) {
//                            $q->where('certificate_status', $request->certificate_status);
//                        } else {
//                            $q->whereIn('certificate_status', ['certified', 'suspended']);
//                        }
//
//                    })
                    ->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request) {
                        $q->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })
//                            ->whereHas('reportCertificate', function ($q) use ($request) {
//                                $q->where('report_status', 'new');
//                                if (!is_null($request->certificate_status)) {
//                                    $q->where('certificate_status', $request->certificate_status);
//                                } else {
//                                    $q->whereIn('certificate_status', ['certified', 'suspended']);
//                                }
//                            })
                            ->whereIn('type', ['base', 'single'])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get();


                $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                    , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                    'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                    'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                ];
                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage =
                            AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)
                                    ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                        $q->where('company_id', $companyStd->company_id)
                                            ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                    });
                            })->where(function ($query) use ($request, $auditTypes, $expectedDateLessThan, $expected_due_from, $expected_due_to, $due_from, $due_to) {
                                if (!is_null($request->audit_type)) {
                                    $query->where('audit_type', $request->audit_type);
                                } else {
                                    $query->whereIn('audit_type', $auditTypes);
                                }
                                if (!is_null($request->date_less_than_aj_due_date)) {
                                    $query->whereBetween('excepted_date', [$expected_due_from->format('Y-m-d'), $expected_due_to->format('Y-m-d')]);
//                                    $query->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'));
                                }
                                if (!is_null($request->aj_due_date)) {
                                    $query->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                        $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                    });
                                }
                                if (!is_null($request->aj_approved_status)) {

                                    $query->where('aj_approved_status', $request->aj_approved_status === 'true' ? true : false);
                                } else {
                                    $query->whereIn('aj_approved_status', [true, false]);
                                }
                            })->get();


                    }
                }


            } else {
                $companies = Company::query();

                $companies = $companies->whereRaw('name <> ""')
                    ->select('id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify')
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request) {
                        $q->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('reportCertificate', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                            })->whereIn('type', ['base', 'single'])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get();


                $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                    'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                    'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                    'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                ];
                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage =
                            AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)
                                    ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                        $q->where('company_id', $companyStd->company_id)
                                            ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                    });
                            })->whereIn('audit_type', $auditTypes)->whereIn('aj_approved_status', [true, false])->get();


                    }
                }

            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                            })->get();


                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                            , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                            'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                            'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                        ];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
//                                    ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])
                                    ->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
                                        ->whereBetween('excepted_date', [$expected_due_from->format('Y-m-d'), $expected_due_to->format('Y-m-d')])->get();
//                                        ->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                            })->get();

                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                            , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                            'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                            'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                        ];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
//                                    ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])
                                    ->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
                                        ->whereBetween('excepted_date', [$expected_due_from->format('Y-m-d'), $expected_due_to->format('Y-m-d')])->get();
//                                        ->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended','withdrawn']);
                        })->get();

                    $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'
//                        , 'stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'surveillance_3_verification', 'surveillance_4_verification', 'surveillance_5_verification', 'reaudit_verification',
//                        'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'surveillance_3_scope_extension', 'surveillance_4_scope_extension', 'surveillance_5_scope_extension', 'reaudit_scope_extension',
//                        'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'surveillance_3_special_transition', 'surveillance_5_special_transition', 'surveillance_5_special_transition', 'reaudit_special_transition'
                    ];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                    }


                }
            }
        }
        return view('reports.company.post-audit.surveillance-and-reaudit-due-actual-audit-conducted.index')->with(['title' => $title, 'companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }


    public function withdrawnReport(Request $request)
    {
        $title = 'Surveillance and Reaudit Due Report';
        ini_set('max_execution_time', '600');
        $due_from = $request->withdrawn_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->withdrawn_due_date, '0', '10'));
        $due_to = $request->withdrawn_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->withdrawn_due_date, '13'));
        $expectedDateLessThan = $request->date_less_than_aj_due_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->date_less_than_aj_due_date);


        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    })->with(['companyStandards.certificates' => function ($q) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    }])->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                            })->get();

                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }
                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    })->with(['companyStandards.certificates' => function ($q) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    }])->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                            })->get();
                        $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    })->with(['companyStandards.certificates' => function ($q) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    }])->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                        })->get();

                    $auditTypes = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', false)->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);


            if (!empty($request->all())) {

                $companies = Company::whereRaw('name <> ""')->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code', 'primaryContact'])
                    ->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    })->with(['companyStandards.certificates' => function ($q) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    }])->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request, $due_from, $due_to) {
                        $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date', 'initial_certification_currency', 'initial_certification_amount', 'surveillance_amount', 're_audit_amount','main_remarks')
                            ->where(function ($query) use ($request) {
                                $query->whereIn('type', ['base', 'single']);
                            })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id, $due_from, $due_to) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('certificates', function ($q) use ($request, $due_from, $due_to) {
                                $q->where('report_status', 'new')->where(function ($query) use ($request, $due_from, $due_to) {
                                    $query->whereIn('certificate_status', ['withdrawn']);
                                    if (!is_null($request->withdrawn_due_date)) {
                                        $query->whereBetween('certificate_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                    }
                                });
                            })->with(['certificates' => function ($q) use ($request, $due_from, $due_to) {
                                $q->where('report_status', 'new')->where(function ($query) use ($request, $due_from, $due_to) {
                                    $query->whereIn('certificate_status', ['withdrawn']);
                                    if (!is_null($request->withdrawn_due_date)) {
                                        $query->whereBetween('certificate_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                    }
                                });
                            }])->with('certificateSuspension.ajStandardStage');

                    }])->get(['id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify']);




            } else {
                $companies = Company::query();
                $companies = $companies->whereRaw('name <> ""')
                    ->select('id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify')
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code', 'primaryContact'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    })->with(['companyStandards.certificates' => function ($q) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                    }])->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request) {
                        $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date', 'initial_certification_currency', 'initial_certification_amount', 'surveillance_amount', 're_audit_amount','main_remarks')
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('reportCertificate', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                            })->with('certificateSuspension.ajStandardStage');

                    }])->get();



            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {


            $companies = Company::query();
            if (auth()->user()->user_type == 'operation_coordinator') {
                $cityOperationsCoordinator = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', auth()->user()->operation_coordinator->id)
                    ->whereNull('deleted_at')->pluck('city_id')->toArray();
                $companies = $companies->where('region_id', auth()->user()->region_id)->whereIn('city_id', $cityOperationsCoordinator);
            } else {
                $companies = $companies->where('region_id', auth()->user()->region_id);
            }

            $companies = $companies->whereRaw('name <> ""')
                ->select('id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify')
                ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code', 'primaryContact'])
                ->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                })->with(['companyStandards.certificates' => function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                }])->with(['companyStandards' => function ($q) use ($request) {
                    $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date', 'initial_certification_currency', 'initial_certification_amount', 'surveillance_amount', 're_audit_amount', 'grade')
                        ->whereIn('type', ['base', 'single'])->whereHas('reportCertificate', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['withdrawn']);
                        })->with('certificateSuspension.ajStandardStage');

                }])->get();
        }

        return view('reports.company.post-audit.surveillance-and-reaudit-due-actual-audit.withdrawn.index')->with(['title' => $title, 'companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }

    public function newReportApprovedTrueSpecial(Request $request)
    {
        $title = 'Surveillance and Reaudit Conducted Report';
        ini_set('max_execution_time', '6000');
        ini_set('memory_limit', '-1');
        $due_from = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '0', '10'));
        $due_to = $request->aj_due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->aj_due_date, '13'));
        $expectedDateLessThan = $request->date_less_than_aj_due_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->date_less_than_aj_due_date);


        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }
                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();
                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
//                                    ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])
                                    ->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                    }


                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);


            if (!empty($request->all())) {

                $companies = Company::query();
                $companies = $companies->whereRaw('name <> ""')
                    ->select('id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify')
                    ->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })
//                    ->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
//                        $q->where('report_status', 'new');
//                        if (!is_null($request->certificate_status)) {
//                            $q->where('certificate_status', $request->certificate_status);
//                        } else {
//                            $q->whereIn('certificate_status', ['certified', 'suspended']);
//                        }
//
//                    })
                    ->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request) {
                        $q->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })
//                            ->whereHas('reportCertificate', function ($q) use ($request) {
//                                $q->where('report_status', 'new');
//                                if (!is_null($request->certificate_status)) {
//                                    $q->where('certificate_status', $request->certificate_status);
//                                } else {
//                                    $q->whereIn('certificate_status', ['certified', 'suspended']);
//                                }
//                            })
                            ->whereIn('type', ['base', 'single'])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get();


                $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage =
                            AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)
                                    ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                        $q->where('company_id', $companyStd->company_id)
                                            ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                    });
                            })->where(function ($query) use ($request, $auditTypes, $expectedDateLessThan, $due_from, $due_to) {
                                if (!is_null($request->audit_type)) {
                                    $query->where('audit_type', $request->audit_type);
                                } else {
                                    $query->whereIn('audit_type', $auditTypes);
                                }
                                if (!is_null($request->date_less_than_aj_due_date)) {
                                    $query->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'));
                                }
                                if (!is_null($request->aj_due_date)) {
                                    $query->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                        $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                    });
                                }
                                if (!is_null($request->aj_approved_status)) {

                                    $query->where('aj_approved_status', $request->aj_approved_status === 'true' ? true : false);
                                } else {
                                    $query->whereIn('aj_approved_status', [true, false]);
                                }
                            })->with(['ajStageTeams:id,member_type,member_name,aj_standard_stage_id'])->get();


                    }
                }


            } else {
                $companies = Company::query();

                $companies = $companies->whereRaw('name <> ""')
                    ->select('id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify')
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('companyStandards.reportCertificate', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request) {
                        $q->whereIn('type', ['base', 'single'])
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('reportCertificate', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->whereIn('type', ['base', 'single'])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get();


                $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage =
                            AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)
                                    ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                        $q->where('company_id', $companyStd->company_id)
                                            ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                    });
                            })->whereIn('audit_type', $auditTypes)->whereIn('aj_approved_status', [true, false])->with(['ajStageTeams:id,member_type,member_name,aj_standard_stage_id'])->get();


                    }
                }

            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereIn('standard_id', $request->standard_id)
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();


                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
//                                    ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])
                                    ->get();
                            }
                        } else {
                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->where('region_id', auth()->user()->region_id)->get();
                    foreach ($companies as $key => $company) {

                        $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                            ->where('company_id', $company->id)
                            ->whereIn('type', ['base', 'single'])
                            ->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                            })->get();

                        $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                        if (!is_null($request->aj_due_date)) {
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereHas('postAudit', function ($q) use ($due_from, $due_to) {
                                    $q->whereBetween('actual_audit_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')]);
                                })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)
//                                    ->whereBetween('excepted_date', [$due_from->format('Y-m-d'), $due_to->format('Y-m-d')])
                                    ->get();
                            }
                        } else {

                            if (!is_null($request->date_less_than_aj_due_date)) {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->whereDate('excepted_date', '<=', $expectedDateLessThan->format('Y-m-d'))->get();
                                }
                            } else {
                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id)
                                            ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                                $q->where('company_id', $companyStd->company_id)
                                                    ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                            });
                                    })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                                }
                            }

                        }

                    }


                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('aj_approved_status', true)->get();
                    }


                }
            }
        }
        return view('reports.company.post-audit.surveillance-and-reaudit-due-actual-audit-conducted.special.index')->with(['title' => $title, 'companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }
}
