<?php

namespace App\Http\Controllers\Reports;

use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardWitnessEvaluation;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Facades\Storage;

class AuditorEvaluationScheduleController extends Controller
{
    public function __invoke(Request $request)
    {

        $due_date = $request->due_date;
        $actual_date = $request->actual_date;
        $due_date1 = $request->due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->due_date, '0', '10'));
        $due_date2 = $request->due_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->due_date, '13'));
        $actual_date1 = $request->actual_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_date, '0', '10'));
        $actual_date2 = $request->actual_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_date, '13'));



        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {

            if (!empty($request->all())) {


                $auditors = Auditor::where('role', 'auditor')->whereHas('auditorWitnessEvaluation', function ($q_due_date) use ($request, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                    if (isset($request->due_date)) {
                        $q_due_date->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')])->whereNull('deleted_at');
                    }
                    if (isset($request->actual_date)) {
                        $q_due_date->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereNull('deleted_at');
                    }
                })->where(function ($query) use ($request) {
                    if (isset($request->auditor)) {
                        $query->where('id', $request->auditor);
                    }
                })->where(function ($query) use ($request) {
                    if (isset($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
    
                })->with(['region', 'country'])->get();
            } else {
                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->get();
    
            }
    
    
            if (isset($request->due_date) && isset($request->actual_date)) {
    
                if (isset($request->standard)) {
                    $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
                        $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                            $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                        })->get();
                    });
                } else {
                    $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
                        $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                            $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                        })->get();
                    });
                }
    
            } else {
                if (isset($request->due_date)) {
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                            })->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                            })->get();
                        });
                    }
    
    
                } elseif (isset($request->actual_date)) {
    
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')]);
                            })->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')]);
                            })->get();
                        });
                    }
    
    
                } else {
    
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $request) {
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $request) {
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->get();
                        });
                    }
    
                }
            }
            
        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            if (!empty($request->all())) {


                $auditors = Auditor::where('role', 'auditor')->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->whereHas('auditorWitnessEvaluation', function ($q_due_date) use ($request, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                    if (isset($request->due_date)) {
                        $q_due_date->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')])->whereNull('deleted_at');
                    }
                    if (isset($request->actual_date)) {
                        $q_due_date->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereNull('deleted_at');
                    }
                })->where(function ($query) use ($request) {
                    if (isset($request->auditor)) {
                        $query->where('id', $request->auditor);
                    }
                })->where(function ($query) use ($request) {
                    if (isset($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
    
                })->with(['region', 'country'])->get();
            } else {
                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get();
            }
    
    
            if (isset($request->due_date) && isset($request->actual_date)) {
    
                if (isset($request->standard)) {
                    $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request,$scheme_manager_id) {
                        $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                            $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                        })->get();
                    });
                } else {
                    $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request,$scheme_manager_id) {
                        $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                            $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                        })->get();
                    });
                }
    
            } else {
                if (isset($request->due_date)) {
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request,$scheme_manager_id) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                            })->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request,$scheme_manager_id) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                                $q->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                            })->get();
                        });
                    }
    
    
                } elseif (isset($request->actual_date)) {
    
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request,$scheme_manager_id) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                                $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')]);
                            })->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request,$scheme_manager_id) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                                $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')]);
                            })->get();
                        });
                    }
    
    
                } else {
    
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $request,$scheme_manager_id) {
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->where('standard_id', $request->standard)->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $request,$scheme_manager_id) {
                            $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->where('auditor_id', $auditor->id)->get();
                        });
                    }
    
                }
            }
            
        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            if (!empty($request->all())) {
                $auditors = Auditor::where('role', 'auditor')->where('region_id', auth()->user()->region_id)->whereHas('auditorWitnessEvaluation', function ($q_due_date) use ($request, $due_date1, $due_date2, $actual_date1, $actual_date2) {
                    if (isset($request->due_date)) {
                        $q_due_date->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')])->whereNull('deleted_at');
                    }
                    if (isset($request->actual_date)) {
                        $q_due_date->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereNull('deleted_at');
                    }
                })->where(function ($query) use ($request) {
                    if (isset($request->auditor)) {
                        $query->where('id', $request->auditor);
                    }
                })->where(function ($query) use ($request) {
                    if (isset($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
    
                })->with(['region', 'country'])->get();
            } else {
                $auditors = Auditor::where('role', 'auditor')->where('region_id', auth()->user()->region_id)->where('auditor_status', 'active')->get();
            }
            if (isset($request->due_date) && isset($request->actual_date)) {
    
                if (isset($request->standard)) {
                    $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
                        $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                            $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                        })->get();
                    });
                } else {
                    $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
                        $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                            $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')])->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                        })->get();
                    });
                }
    
            } else {
                if (isset($request->due_date)) {
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                            })->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('due_date', [$due_date1->format('Y-m-d'), $due_date2->format('Y-m-d')]);
                            })->get();
                        });
                    }
    
    
                } elseif (isset($request->actual_date)) {
    
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')]);
                            })->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2, $request) {
    
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereHas('auditorStandardWitnessEvaluation', function ($q) use ($auditors, $due_date1, $due_date2, $actual_date1, $actual_date2) {
    
                                $q->whereBetween('actual_date', [$actual_date1->format('Y-m-d'), $actual_date2->format('Y-m-d')]);
                            })->get();
                        });
                    }
    
    
                } else {
    
                    if (isset($request->standard)) {
                        $auditors->each(function ($auditor, $key) use ($auditors, $request) {
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $request->standard)->get();
                        });
                    } else {
                        $auditors->each(function ($auditor, $key) use ($auditors, $request) {
                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->get();
                        });
                    }
    
                }
            }

        }
//        dd($due_date,$actual_date);

        $pdf = \Barryvdh\DomPDF\Facade::loadView('reports.auditor-evaluation-schedule.list-of-auditor-evaluation-pdf', compact('auditors', 'due_date1', 'due_date2', 'actual_date1', 'actual_date2', 'actual_date', 'due_date'));
        Storage::put('public/uploads/documents/list-of-auditor-evaluation.pdf', $pdf->output());


        return view('reports.auditor-evaluation-schedule.index', compact('auditors', 'due_date1', 'due_date2', 'actual_date1', 'actual_date2', 'actual_date', 'due_date'));
    }
}
