<?php

namespace App\Http\Controllers\Reports;

use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandards;
use App\Countries;
use App\Regions;
use App\RegionsCountries;
use App\Standards;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListOfCompanyController extends Controller
{
    public function __invoke(Request $request)
    {
        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();


            if (!empty($request->all())) {
                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {
                            $query->where('id', $request->name);
                        }
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards', function ($q) use ($request) {
                        $q->whereIn('standard_id', $request->standard_id)->where('status', 'approved')->whereNull('deleted_at')->whereHas('standard', function ($q) use ($request) {
                            if (!is_null($request->standard_status)) {
                                $status = ($request->standard_status == 'active') ? true : false;
                                $q->where('status', $status);
                            }
                        });
                    })->get();

                    if (!empty($companies) && count($companies) > 0) {
                        foreach ($companies as $key => $company) {
                            $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            }])->where('status', 'approved')->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();
                        }
                    }

                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {
                            $query->where('id', $request->name);
                        }
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }

                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard', function ($q) use ($request) {
                        if (!is_null($request->standard_status)) {
                            $status = ($request->standard_status == 'active') ? true : false;
                            $q->where('status', $status);
                        }
                    })->get();

                    if (!empty($companies) && count($companies) > 0) {
                        foreach ($companies as $key => $company) {
                            $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            }])->where('status', 'approved')->where('company_id', $company->id)->get();
                        }
                    }
                }

            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->get();

                if (!empty($companies) && count($companies) > 0) {
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                            if (!is_null($request->standard_status)) {
                                $status = ($request->standard_status == 'active') ? true : false;
                                $q->where('status', $status);
                            }
                        }])->where('status', 'approved')->where('company_id', $company->id)->get();
                    }
                }
            }

        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();

            if (!empty($request->all())) {
                if (!empty($request->standard_id)) {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {
                            $query->where('id', $request->name);
                        }
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards', function ($q) use ($request) {
                        $q->whereIn('standard_id', $request->standard_id)
                            ->whereNull('deleted_at')
                            ->whereHas('standard', function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            });
                    })->get();
                    if (!empty($companies) && count($companies) > 0) {
                        foreach ($companies as $key => $company) {
                            $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            }])->where('status', 'approved')->where('company_id', $company->id)
                                ->whereIn('standard_id', $request->standard_id)
                                ->whereHas('standard', function ($q) use ($request) {
                                    if (!is_null($request->standard_status)) {
                                        $status = ($request->standard_status == 'active') ? true : false;
                                        $q->where('status', $status);
                                    }
                                })->get();
                        }
                    }


                } else {

                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {
                            $query->where('id', $request->name);
                        }
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }

                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard', function ($q) use ($request) {
                        if (!is_null($request->standard_status)) {
                            $status = ($request->standard_status == 'active') ? true : false;
                            $q->where('status', $status);
                        }
                    })->get();
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->where('status', 'approved')->where('company_id', $company->id)
                            ->whereHas('standard', function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            })->get();

                    }

                }
            } else {
                $companies = Company::whereRaw('name <> ""')
                    ->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where('status', 'approved')->where('company_id', $company->id)->get();

                }
            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            if (!empty($request->all())) {
                if (!empty($request->standard_id)) {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {
                            $query->where('id', $request->name);
                        }
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->where('region_id', auth()->user()->region_id)->whereHas('companyStandards', function ($q) use ($request) {
                        $q->whereIn('standard_id', $request->standard_id)
                            ->whereNull('deleted_at')
                            ->whereHas('standard', function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            });
                    })->get();

                    if (!empty($companies) && count($companies) > 0) {
                        foreach ($companies as $key => $company) {
                            $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            }])->where('status', 'approved')->where('company_id', $company->id)
                                ->whereIn('standard_id', $request->standard_id)
                                ->whereHas('standard', function ($q) use ($request) {
                                    if (!is_null($request->standard_status)) {
                                        $status = ($request->standard_status == 'active') ? true : false;
                                        $q->where('status', $status);
                                    }
                                })->get();
                        }
                    }
                } else {
                    $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {
                            $query->where('id', $request->name);
                        }
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }

                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })->whereHas('companyStandards.standard', function ($q) use ($request) {
                        if (!is_null($request->standard_status)) {
                            $status = ($request->standard_status == 'active') ? true : false;
                            $q->where('status', $status);
                        }
                    })->where('region_id', auth()->user()->region_id)->get();

                    if (!empty($companies) && count($companies) > 0) {
                        foreach ($companies as $key => $company) {
                            $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                                if (!is_null($request->standard_status)) {
                                    $status = ($request->standard_status == 'active') ? true : false;
                                    $q->where('status', $status);
                                }
                            }])->where('status', 'approved')->where('company_id', $company->id)->get();
                        }
                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])->get();
                if (!empty($companies) && count($companies) > 0) {
                    foreach ($companies as $key => $company) {
                        $companies[$key]->companyStandards = CompanyStandards::with(['standard' => function ($q) use ($request) {
                            if (!is_null($request->standard_status)) {
                                $status = ($request->standard_status == 'active') ? true : false;
                                $q->where('status', $status);
                            }
                        }])->where('status', 'approved')->where('company_id', $company->id)->get();
                    }
                }


            }
        }
        return view('reports.company.list-of-companies.index')->with(['companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }
}
