<?php

namespace App\Http\Controllers\Reports;

use App\Auditor;
use App\AuditorStandard;
use App\Regions;
use App\Standards;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Facades\Storage;
use DB;

class ListOfTechnicalExpertController extends Controller
{
    public function __invoke(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $regions = Regions::all();
            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'technical_expert')->with(['user', 'region', 'country'])->where(function ($query) use ($request) {
                    if (!is_null($request->name)) {

                        $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->name . "%");
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();

                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }

            } else {

                $auditors = Auditor::where('role', 'technical_expert')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }

            }

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            if (!empty($request->all())) {
                $auditors = Auditor::where('role', 'technical_expert')
                    ->with(['user', 'region', 'country'])
                    ->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where(function ($query) use ($request) {
                        if (!is_null($request->name)) {

                            $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->name . "%");
                        }

                        if (!is_null($request->region_id)) {
                            $query->where('region_id', $request->region_id);
                        }

                        if (!is_null($request->country_id)) {
                            $query->where('country_id', $request->country_id);
                        }

                        if (!is_null($request->status)) {
                            $query->where('auditor_status', $request->status);
                        }
                    })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where('auditor_id', $auditor->id)->get();
                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }

            } else {

                $auditors = Auditor::where('role', 'technical_expert')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where('auditor_id', $auditor->id)->get();
                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }

            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $regions = Regions::where('id', auth()->user()->region_id)->get();

            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'technical_expert')->with(['user', 'region', 'country'])->where('region_id', auth()->user()->region_id)->where(function ($query) use ($request) {
                    if (!is_null($request->name)) {

                        $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->name . "%");
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }

            } else {

                $auditors = Auditor::where('role', 'technical_expert')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->where('region_id', auth()->user()->region_id)->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }
            }

        }

        $pdf = \Barryvdh\DomPDF\Facade::loadView('reports.list-of-technical-expert.list-of-technical-expert-pdf', compact('regions', 'auditors'));
        Storage::put('public/uploads/documents/list-of-technical-expert.pdf', $pdf->output());
        return view('reports/list-of-technical-expert.index', compact('regions', 'auditors'));
    }

    public function listOfTechnicalExpertWithApprovalDate(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $auditors = [];
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $regions = Regions::all();
            $auditors = [];

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $auditors = Auditor::where(function ($query) use ($request) {
                    if (!empty($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }
                    if (!empty($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }
                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                    $query->where('auditor_status', 'active')->where('role', 'technical_expert');
                })
                ->with(['user', 'region', 'country'])->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get(['id', 'first_name', 'last_name', 'job_status', 'auditor_status', 'region_id', 'country_id']);
            foreach ($auditors as $key => $auditor) {
                $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->where('auditor_id', $auditor->id)->get(['id', 'status', 'standard_id']);
                foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                    $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first(['id', 'name']);
                }
            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $auditors = [];

        }
        return view('reports.list-of-technical-expert.list-of-technical-experts-with-approval-date', compact('regions', 'auditors'));
    }
}
