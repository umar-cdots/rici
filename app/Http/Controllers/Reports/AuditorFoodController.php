<?php

namespace App\Http\Controllers\Reports;

use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardFoodCode;
use App\Regions;
use App\Standards;
use App\SchemeManager;
use App\StandardsFamily;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use function foo\func;

class AuditorFoodController extends Controller
{
    public function __invoke(Request $request)
    {
        $standard_family_id = 23;

        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $regions = Regions::all();
            $food_auditors = Auditor::where('role', 'auditor')->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                $q->where('id', $standard_family_id);
            })->get();
            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'auditor')->with(['user', 'region', 'country'])->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                    $q->where('id', $standard_family_id);
                })->where(function ($query) use ($request) {
                    if (!is_null($request->auditor)) {
                        $query->where('id', $request->auditor);
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('auditorStandardFoodCode')->whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                        $q->where('id', $standard_family_id);
                    })->where('auditor_id', $auditor->id)->get();

                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }

            } else {

                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                    $q->where('id', $standard_family_id);
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('auditorStandardFoodCode')->whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                        $q->where('id', $standard_family_id);
                    })->where('auditor_id', $auditor->id)->get();

                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }
            }

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $check_scheme_manger = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                $q->where('id', $standard_family_id);
            })->get();

            if (count($check_scheme_manger) > 0) {
                $food_auditors = Auditor::where('role', 'auditor')->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                    $q->where('id', $standard_family_id);
                })->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get();
                if (!empty($request->all())) {
                    //            dd($request->all());
                    $auditors = Auditor::where('role', 'auditor')->with(['user', 'region', 'country'])->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                        $q->where('id', $standard_family_id);
                    })->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where(function ($query) use ($request) {
                        if (!is_null($request->auditor)) {
                            $query->where('id', $request->auditor);
                        }

                        if (!is_null($request->region_id)) {
                            $query->where('region_id', $request->region_id);
                        }

                        if (!is_null($request->country_id)) {
                            $query->where('country_id', $request->country_id);
                        }

                        if (!is_null($request->status)) {
                            $query->where('auditor_status', $request->status);
                        }
                    })->get();
                    foreach ($auditors as $key => $auditor) {
                        $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                            $q->where('id', $standard_family_id);
                        })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->where('auditor_id', $auditor->id)->get();
                        foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                            $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                        }
                    }

                } else {

                    $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                        $q->where('id', $standard_family_id);
                    })->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->get();
                    foreach ($auditors as $key => $auditor) {
                        $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                            $q->where('id', $standard_family_id);
                        })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->where('auditor_id', $auditor->id)->get();
                        foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                            $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                        }
                    }

                }
            } else {
                $auditors = [];
                $food_auditors = [];
            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $food_auditors = Auditor::where('role', 'auditor')->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                $q->where('id', $standard_family_id);
            })->get();
            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'auditor')->with(['user', 'region', 'country'])->where('region_id', auth()->user()->region_id)->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                    $q->where('id', $standard_family_id);
                })->where(function ($query) use ($request) {
                    if (!is_null($request->auditor)) {
                        $query->where('id', $request->auditor);
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('auditorStandardFoodCode')->whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                        $q->where('id', $standard_family_id);
                    })->where('auditor_id', $auditor->id)->get();

                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }

            } else {

                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->where('region_id', auth()->user()->region_id)->whereHas('auditorStandards.standardFamily', function ($q) use ($standard_family_id) {
                    $q->where('id', $standard_family_id);
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('auditorStandardFoodCode')->whereHas('standard.standardFamily', function ($q) use ($standard_family_id) {
                        $q->where('id', $standard_family_id);
                    })->where('auditor_id', $auditor->id)->get();

                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }
            }

        }
        $pdf = \Barryvdh\DomPDF\Facade::loadView('reports.auditor-food.food-auditors-pdf', compact('auditors', 'regions', 'food_auditors'));
        Storage::put('public/uploads/documents/food-auditors.pdf', $pdf->output());


        return view('reports.auditor-food.index', compact('auditors', 'regions', 'food_auditors'));
    }
}
