<?php

namespace App\Http\Controllers\Reports;

use App\Auditor;
use App\AuditorStandard;
use App\Regions;
use App\Standards;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Facades\Storage;
use DB;

class ListOfAuditorController extends Controller
{
    public function __invoke(Request $request)
    {

        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $regions = Regions::all();
            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'auditor')->with(['user', 'region', 'country'])->where(function ($query) use ($request) {
                    if (!is_null($request->name)) {

                        $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->name . "%");
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();

                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }

            } else {

                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }

            }

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'auditor')->with(['user', 'region', 'country'])->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->where(function ($query) use ($request) {
                    if (!is_null($request->name)) {

                        $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->name . "%");
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where('auditor_id', $auditor->id)->get();
                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }

            } else {

                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->where('auditor_id', $auditor->id)->get();
                    foreach ($auditors[$key]->auditorStandards as $key1 => $auditorStandard) {
                        $auditors[$key]->auditorStandards[$key1]->standard = Standards::where('id', $auditorStandard->standard_id)->first();
                    }
                }

            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $regions = Regions::where('id', auth()->user()->region_id)->get();
            if (!empty($request->all())) {
                //            dd($request->all());
                $auditors = Auditor::where('role', 'auditor')->with(['user', 'region', 'country'])->where('region_id', auth()->user()->region_id)->where(function ($query) use ($request) {
                    if (!is_null($request->name)) {

                        $query->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->name . "%");
                    }

                    if (!is_null($request->region_id)) {
                        $query->where('region_id', $request->region_id);
                    }

                    if (!is_null($request->country_id)) {
                        $query->where('country_id', $request->country_id);
                    }

                    if (!is_null($request->status)) {
                        $query->where('auditor_status', $request->status);
                    }
                })->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }

            } else {

                $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->with(['user', 'region', 'country'])->where('region_id', auth()->user()->region_id)->get();
                foreach ($auditors as $key => $auditor) {
                    $auditors[$key]->auditorStandards = AuditorStandard::with('standard')->where('auditor_id', $auditor->id)->get();
                }
            }

        }
        $pdf = \Barryvdh\DomPDF\Facade::loadView('reports.list-of-auditor.list-of-auditors-pdf', compact('regions', 'auditors'));
        Storage::put('public/uploads/documents/list-of-auditors.pdf', $pdf->output());

        return view('reports/list-of-auditor.index', compact('regions', 'auditors'));
    }


}
