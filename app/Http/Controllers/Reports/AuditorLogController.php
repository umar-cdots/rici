<?php

namespace App\Http\Controllers\Reports;

use App\Company;
use App\Countries;
use App\Regions;
use App\Auditor;
use App\RegionsCountries;
use App\Standards;
use App\CompanyStandardCode;
use App\CompanyStandards;
use App\PostAudit;
use App\AJStageTeam;
use DateTime;
use App\AJStandardStage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditorLogController extends Controller
{
    public function __invoke(Request $request)
    {
        $issue_from = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '0', '10'));
        $issue_to = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '13'));
        if (auth()->user()->user_type == 'admin') {
            $get_auditors = Auditor::where('auditor_status', 'active')->where('role', 'auditor')->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {
                if (!is_null($request->issue_from)) {
                    if (!empty($request->standard_id)) {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
//                                $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
//                                $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
//                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    } else {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($standard) {
                                        $q->where('standard_id', $standard->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    }

                } else {
                    if (!empty($request->standard_id)) {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    } else {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($standard) {
                                        $q->where('standard_id', $standard->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $companies = [];
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_auditors = Auditor::where('auditor_status', 'active')->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->where('role', 'auditor')->get();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardCode::whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {
                if (!is_null($request->issue_from)) {
                    if (!empty($request->standard_id)) {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
//                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    } else {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->standard = Standards::where('id', $standard->standard_id)->first();
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($standard) {
                                        $q->where('standard_id', $standard->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes'])->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->standard = Standards::where('id', $companyStd->standard_id)->first();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    }

                } else {
                    if (!empty($request->standard_id)) {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    } else {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->standard = Standards::where('id', $standard->standard_id)->first();
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($standard) {
                                        $q->where('standard_id', $standard->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('auditJustifications.ajStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes'])->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->standard = Standards::where('id', $companyStd->standard_id)->first();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $companies = [];
            }
        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_auditors = Auditor::where('auditor_status', 'active')->where('role', 'auditor')->where('region_id', auth()->user()->region_id)->get();
            $get_standards = Standards::all();
            $get_company_standard_unique_codes = CompanyStandardCode::groupBy('unique_code')->get(['unique_code']);
            if (!empty($request->all())) {
                if (!is_null($request->issue_from)) {
                    if (!empty($request->standard_id)) {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    } else {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($standard) {
                                        $q->where('standard_id', $standard->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id);
                                    })->whereHas('postAudit', function ($q) use ($request, $issue_from, $issue_to) {
                                        //                                        $q->whereBetween('actual_audit_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                        $q->whereDate('actual_audit_date', '>=', $issue_from->format('Y-m-d'))->whereDate('actual_audit_date_to', '<=', $issue_to->format('Y-m-d'));

                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    }

                } else {
                    if (!empty($request->standard_id)) {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards', function ($q) use ($request) {
                                $q->whereIn('standard_id', $request->standard_id);
                            })->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($request) {
                                        $q->whereIn('standard_id', $request->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    } else {
                        if (!empty($request->unique_codes)) {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['standard'])->whereHas('companyStandardCodes', function ($q) use ($request) {
                                    $q->whereIn('unique_code', $request->unique_codes);
                                })->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                    $companies[$key]->companyStandards[$key1]->companyStandardCodes = CompanyStandardCode::where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($standard) {
                                        $q->where('standard_id', $standard->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        } else {
                            $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])->whereHas('auditJustifications.ajStandards.ajStandardStages.ajStageTeams', function ($q) use ($request) {
                                $q->where('auditor_id', $request->name);
                            })->where('region_id', auth()->user()->region_id)->get();

                            foreach ($companies as $key => $company) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes', 'standard'])->where('company_id', $company->id)->get();

                                foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                    $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStageTeams', function ($q) use ($request) {
                                        $q->where('auditor_id', $request->name);
                                    })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                        $q->where('standard_id', $companyStd->standard_id);
                                    })->where('status', 'approved')->whereNull('deleted_at')->get();
                                    foreach ($companies[$key]->companyStandards[$key1]->ajStandardStage as $key2 => $stage) {
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->postAudit = PostAudit::where('aj_standard_stage_id', $stage->id)->first();
                                        $companies[$key]->companyStandards[$key1]->ajStandardStage[$key2]->ajStageTeam = AJStageTeam::where('aj_standard_stage_id', $stage->id)->where('auditor_id', $request->name)->whereNull('deleted_at')->first();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $companies = [];
            }
        }
        return view('reports.auditor-log.index')->with(['companies' => $companies, 'get_auditors' => $get_auditors, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes]);
    }
}
