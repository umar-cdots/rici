<?php

namespace App\Http\Controllers\Reports;

use App\Company;
use App\CompanyStandardEnergyCode;
use App\Countries;
use App\Regions;
use App\RegionsCountries;
use App\Standards;
use App\CompanyStandards;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EnergyCompanyStandardController extends Controller
{
    public function __invoke(Request $request)
    {
        $standard_family_id = 20; //Energy Id
        if (auth()->user()->user_type == 'admin') {
            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                $q->where('standards_family_id', $standard_family_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::where('standards_family_id', $standard_family_id)->get();
            $get_company_standard_unique_codes = CompanyStandardEnergyCode::whereHas('standard', function ($q) use ($standard_family_id) {
                $q->where('standards_family_id', $standard_family_id);
            })->groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {
                if (!empty($request->standard_id)) {
                    if (!empty($request->unique_codes)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->where('status', 'approved')->get();
                            }

                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardEnergyCodes.standard'])->where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->whereIn('standard_id', $request->standard_id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardEnergyCodes.standard'])->where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->whereIn('standard_id', $request->standard_id)->where('status', 'approved')->get();
                            }
                        }
                    }
                } else {
                    if (!empty($request->unique_codes)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('status', 'approved')->get();
                            }

                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact', 'companyStandards.companyStandardEnergyCodes.standard'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }

                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('status', 'approved')->get();
                            }

                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->get();
                            }
                        }
                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                    $q->where('standards_family_id', $standard_family_id);
                })->get();
                foreach ($companies as $key => $company) {

                    if (!is_null($request->status)) {
                        $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->whereHas('certificates', function ($q) use ($request) {
                            $q->where('certificate_status', $request->status)->where('report_status', 'new');
                        })->where('status', 'approved')->get();
                    } else {

                        $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where('status', 'approved')->get();
                    }

                    foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                        $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->get();
                    }
                }
            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $get_companies = Company::whereRaw('name <> ""')->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                $q->where('standards_family_id', $standard_family_id);
            })->get();
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->where('standards_family_id', $standard_family_id)->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->where('standards_family_id', $standard_family_id)->pluck('id')->toArray();
            $get_company_standard_unique_codes = CompanyStandardEnergyCode::whereHas('standard', function ($q) use ($standard_family_id) {
                $q->where('standards_family_id', $standard_family_id);
            })->whereIn('standard_id', $get_standards_ids)->groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {
                if (!empty($request->standard_id)) {
                    if (!empty($request->unique_codes)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {

                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->where('status', 'approved')->get();
                            }
                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardEnergyCodes.standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardEnergyCodes.standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->where('status', 'approved')->get();
                            }
                        }
                    }
                } else {
                    if (!empty($request->unique_codes)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('company_id', $company->id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('status', 'approved')->where('company_id', $company->id)->get();
                            }
                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }

                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->get();
                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('company_id', $company->id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                    $q->where('scheme_manager_id', $scheme_manager_id);
                                })->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('status', 'approved')->where('company_id', $company->id)->get();
                            }

                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->get();
                            }
                        }
                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                    $q->where('scheme_manager_id', $scheme_manager_id);
                })->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                    $q->where('standards_family_id', $standard_family_id);
                })->get();
                foreach ($companies as $key => $company) {

                    if (!is_null($request->status)) {
                        $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where('company_id', $company->id)->whereHas('certificates', function ($q) use ($request) {
                            $q->where('certificate_status', $request->status)->where('report_status', 'new');
                        })->where('status', 'approved')->get();
                    } else {
                        $companies[$key]->companyStandards = CompanyStandards::whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where('status', 'approved')->where('company_id', $company->id)->get();
                    }


                    foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                        $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->get();
                    }
                }
            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                $q->where('standards_family_id', $standard_family_id);
            })->get();
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::where('standards_family_id', $standard_family_id)->get();
            $get_company_standard_unique_codes = CompanyStandardEnergyCode::whereHas('standard', function ($q) use ($standard_family_id) {
                $q->where('standards_family_id', $standard_family_id);
            })->groupBy('unique_code')->get(['unique_code']);

            if (!empty($request->all())) {
                if (!empty($request->standard_id)) {
                    if (!empty($request->unique_codes)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->where('status', 'approved')->get();
                            }


                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardEnergyCodes.standard'])->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardEnergyCodes.standard'])->where('status', 'approved')->where('company_id', $company->id)->whereIn('standard_id', $request->standard_id)->get();
                            }

                        }
                    }
                } else {
                    if (!empty($request->unique_codes)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('status', 'approved')->get();
                            }

                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->whereIn('unique_code', $request->unique_codes)->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact',])->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where(function ($query) use ($request) {
                            if (!is_null($request->name)) {
                                $query->where('id', $request->name);
                            }
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }

                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->where('region_id', auth()->user()->region_id)->get();

                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->whereHas('certificates', function ($q) use ($request) {
                                    $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                })->where('status', 'approved')->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                                    $q->where('standards_family_id', $standard_family_id);
                                })->where('status', 'approved')->get();
                            }


                            foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                                $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->get();
                            }
                        }
                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where('region_id', auth()->user()->region_id)->whereHas('companyStandards.standard', function ($q) use ($standard_family_id) {
                    $q->where('standards_family_id', $standard_family_id);
                })->get();
                foreach ($companies as $key => $company) {

                    if (!is_null($request->status)) {
                        $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->whereHas('certificates', function ($q) use ($request) {
                            $q->where('certificate_status', $request->status)->where('report_status', 'new');
                        })->where('status', 'approved')->get();
                    } else {
                        $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)->whereHas('standard', function ($q) use ($standard_family_id) {
                            $q->where('standards_family_id', $standard_family_id);
                        })->where('status', 'approved')->get();
                    }

                    foreach ($companies[$key]->companyStandards as $key1 => $standard) {
                        $companies[$key]->companyStandards[$key1]->companyStandardEnergyCodes = CompanyStandardEnergyCode::with(['standard'])->where('company_id', $company->id)->where('company_standard_id', $standard->id)->get();
                    }
                }

            }
        }

//        return $companies;
        return view('reports.company.standards.energy.index')->with(['companies' => $companies, 'get_companies' => $get_companies, 'get_standards' => $get_standards, 'get_company_standard_unique_codes' => $get_company_standard_unique_codes, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }
}
