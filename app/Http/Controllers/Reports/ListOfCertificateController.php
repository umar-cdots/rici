<?php

namespace App\Http\Controllers\Reports;

use App\AJStandard;
use App\AJStandardStage;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandards;
use App\Countries;
use App\RegionalOperationCoordinatorCities;
use App\Regions;
use App\RegionsCountries;
use App\Standards;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ListOfCertificateController extends Controller
{
    public function __invoke(Request $request)
    {
        ini_set('max_execution_time', '600');
        $issue_from = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '0', '10'));
        $issue_to = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '13'));


        if (auth()->user()->user_type == 'admin') {

            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();

            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }

                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }
                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->get();

                    $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id);
                        })->whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                    }


                }

            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();


            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }

                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }
                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                $count = 0;
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id);
                        })->whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();

//                        $companies[$key]->companyStandards[$key1]->ajStandardStage = DB::select(DB::raw("select * from `aj_standard_stages` where exists (select * from `post_audits`
//                            where `aj_standard_stages`.`id` = `post_audits`.`aj_standard_stage_id` and `company_id` = '$companyStd->company_id'
//                            and `standard_id` = '$companyStd->standard_id'  and `post_audits`.`deleted_at` is null)
//                            and exists (select * from `aj_standards` where `aj_standard_stages`.`aj_standard_id` = `aj_standards`.`id`
//                            and `standard_id` = '$companyStd->standard_id' and
//                            exists (select * from `audit_justifications` where `aj_standards`.`audit_justification_id` = `audit_justifications`.`id`
//                            and exists (select * from `companies`  where `audit_justifications`.`company_id` = `companies`.`id` and
//                            exists (select * from `company_standards` where `companies`.`id` = `company_standards`.`company_id`
//                            and `company_id` = $companyStd->company_id and `standard_id` = '$companyStd->standard_id' and
//                            `type` in ('base', 'single')) and `companies`.`deleted_at` is null) and `audit_justifications`.`deleted_at` is null) and
//                            `aj_standards`.`deleted_at` is null)
//                            and `audit_type` in ('stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit')
//                            and `status` = 'approved' and `aj_standard_stages`.`deleted_at` is null"));

                    }


                }

            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {


            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->where('region_id', auth()->user()->region_id)->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }

                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->where('region_id', auth()->user()->region_id)->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                                    })->get();
                            }
                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended']);
                        })->get();

                    $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id);
                        })->whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                    }


                }
            }
        }
        return view('reports.company.post-audit.certificates.index')->with(['companies' => $companies, 'get_standards' => $get_standards, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }

    public function indexold(Request $request)
    {

        ini_set('max_execution_time', '600');
        $issue_from = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '0', '10'));
        $issue_to = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '13'));


        if (auth()->user()->user_type == 'admin') {
            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::all();


            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }

                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }
                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])
                    ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                    })->get();
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                        ->where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->with(['certificates' => function ($q) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        }])->get();

                    $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id);
                        })->whereHas('ajStandard', function ($q) use ($companyStd) {
                            $q->where('standard_id', $companyStd->standard_id)
                                ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)
                                        ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                        })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                    }


                }

            }
        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();
            $get_standards_ids = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->pluck('id')->toArray();


            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }

                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                        $q->where('scheme_manager_id', $scheme_manager_id);
                                    })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }
                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }
                }
            } else {
                $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                    })->get(['id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides']);
                $count = 0;
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                        ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                            $q->where('scheme_manager_id', $scheme_manager_id);
                        })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->with(['certificates' => function ($q) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        }])->with(['companyStandardCodes.standard'])->get(['company_id', 'standard_id', 'type', 'status', 'is_ims', 'id']);

                    $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereIn('audit_type', $auditTypes)
                            ->where('status', 'approved')
                            ->whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                            })->whereHas('postAudit', function ($q) use ($companyStd) {
                                $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                            })->get(['audit_type', 'status', 'aj_standard_id', 'job_number', 'id']);

                    }


                }


            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            if (!empty($request->all())) {

                if (!empty($request->standard_id)) {


                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->where('region_id', auth()->user()->region_id)->get();


                        foreach ($companies as $key => $company) {

                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }

                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->where('status', 'approved')
                                    ->whereIn('standard_id', $request->standard_id)
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }


                } else {
                    if (!is_null($request->issue_from)) {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->where('region_id', auth()->user()->region_id)->get();

                        foreach ($companies as $key => $company) {


                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                        $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }


                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')])->where('print_required', 'YES');
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }
                    } else {
                        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact'])->where(function ($query) use ($request) {
                            if (!empty($request->region_id)) {
                                $query->whereIn('region_id', $request->region_id);
                            }
                            if (!empty($request->country_id)) {
                                $query->whereIn('country_id', $request->country_id);
                            }
                        })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->where('region_id', auth()->user()->region_id)->get();
                        foreach ($companies as $key => $company) {
                            if (!is_null($request->status)) {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('certificate_status', $request->status)->where('report_status', 'new');
                                    })->with(['certificates' => function ($q) use ($request) {
                                        $q->where('report_status', 'new')->where('certificate_status', $request->status);
                                    }])->get();
                            } else {
                                $companies[$key]->companyStandards = CompanyStandards::with(['companyStandardCodes.standard'])
                                    ->where('company_id', $company->id)
                                    ->whereIn('type', ['base', 'single'])
                                    ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                        $q->where('print_required', 'YES');
                                    })->whereHas('certificates', function ($q) use ($request) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    })->with(['certificates' => function ($q) {
                                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }])->get();
                            }
                            $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('postAudit', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                                })->whereHas('ajStandard', function ($q) use ($companyStd) {
                                    $q->where('standard_id', $companyStd->standard_id)
                                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                            $q->where('company_id', $companyStd->company_id)
                                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                        });
                                })->whereIn('audit_type', $auditTypes)->where('status', 'approved')->get();
                            }
                        }

                    }
                }
            } else {

                if (auth()->user()->user_type == 'operation_coordinator') {
                    $cityOperationsCoordinator = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', auth()->user()->operation_coordinator->id)
                        ->whereNull('deleted_at')->pluck('city_id')->toArray();


                    $companies = Company::whereRaw('name <> ""')
                        ->where('region_id', auth()->user()->region_id)
                        ->whereIn('city_id', $cityOperationsCoordinator)
                        ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->with(['region', 'country', 'city'])->get(['id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides']);
                } else {
                    $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->with(['region', 'country', 'city'])
                        ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->get(['id', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides']);
                }


                $count = 0;
                foreach ($companies as $key => $company) {
                    $companies[$key]->companyStandards = CompanyStandards::where('company_id', $company->id)
                        ->whereIn('type', ['base', 'single'])
                        ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                            $q->where('print_required', 'YES');
                        })->whereHas('certificates', function ($q) use ($request) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        })->with(['certificates' => function ($q) {
                            $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                        }])->with(['companyStandardCodes.standard'])->get(['company_id', 'standard_id', 'type', 'status', 'is_ims', 'id']);

                    $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereIn('audit_type', $auditTypes)
                            ->where('status', 'approved')
                            ->whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                            })->whereHas('postAudit', function ($q) use ($companyStd) {
                                $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                            })->get(['audit_type', 'status', 'aj_standard_id', 'job_number', 'id']);

                    }


                }


            }
        }


        return view('reports.company.post-audit.certificates.index')->with(['companies' => $companies, 'get_standards' => $get_standards, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);
    }

    public function index(Request $request)
    {

        ini_set('max_execution_time', '600');
        $issue_from = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '0', '10'));
        $issue_to = $request->issue_from == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->issue_from, '13'));


        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type === 'management') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else if (auth()->user()->user_type == 'management') {
                $scheme_manager_id = 1;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $get_regions = Regions::all();
            $get_countries = Countries::all();
            $get_standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();

            if (!is_null($request->issue_from)) {
                $auditTypes = ['stage_2'];
            } else {
                $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
            }

            if (!empty($request->all())) {


                $companies = Company::whereRaw('name <> ""')->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                        $q->where('print_required', 'YES');
                        if (!is_null($request->issue_from)) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        }

                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->where(function ($query) use ($request) {
                            if (!empty($request->status)) {
                                $query->where('certificate_status', $request->status);
                            } else {
                                $query->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                            }
                        });
                    })->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request, $issue_from, $issue_to) {
                        $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date', 'initial_certification_currency', 'initial_certification_amount', 'surveillance_amount', 're_audit_amount', 'proposed_complexity', 'general_remarks')

                            ->where(function ($query) use ($request) {
                                if (!empty($request->standard_id)) {
                                    $query->whereIn('standard_id', $request->standard_id);
                                }else{
                                    $query->whereIn('type', ['base', 'single']);
                                }
                            })
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id, $issue_from, $issue_to) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                $q->where('print_required', 'YES');
                                if (!is_null($request->issue_from)) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                }

                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->where(function ($query) use ($request) {
                                    if (!empty($request->status)) {
                                        $query->where('certificate_status', $request->status);
                                    } else {
                                        $query->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }
                                });
                            })->with(['certificates' => function ($q) use ($request) {
                                $q->where('report_status', 'new')->where(function ($query) use ($request) {
                                    if (!empty($request->status)) {
                                        $query->where('certificate_status', $request->status);
                                    } else {
                                        $query->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }
                                });
                            }])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get(['id', 'company_id', 'shift', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides','out_of_country_region_name']);
                
                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereIn('audit_type', $auditTypes)
                            ->where('status', 'approved')
                            ->whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                            })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                $q->where('print_required', 'YES');
                                if (!is_null($request->issue_from)) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                }
                            })
                            ->whereHas('postAudit', function ($q) use ($companyStd) {
                                $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                            })->get(['audit_type', 'status', 'aj_standard_id', 'job_number', 'id']);

                    }
//
//
                }
            } else {
                $companies = Company::query();
                $companies = $companies->whereRaw('name <> ""')
                    ->select('id', 'company_id', 'shift', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides','out_of_country_region_name')
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                        $q->where('scheme_manager_id', $scheme_manager_id);
                    })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                    })->with(['companyStandards' => function ($q) use ($scheme_manager_id, $request) {
                        $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date', 'initial_certification_currency', 'initial_certification_amount', 'surveillance_amount', 're_audit_amount','grade','proposed_complexity','general_remarks')->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                            ->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                                $q->where('scheme_manager_id', $scheme_manager_id);
                            })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                $q->where('print_required', 'YES');
                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                            })->with(['certificates' => function ($q) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                            }])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get();

                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereIn('audit_type', $auditTypes)
                            ->where('status', 'approved')
                            ->whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                            })->whereHas('postAudit', function ($q) use ($companyStd) {
                                $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                            })->get(['audit_type', 'status', 'aj_standard_id', 'job_number', 'id']);

                    }
                }


            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $get_regions = Regions::where('id', auth()->user()->region_id)->get();
            $countries_id = RegionsCountries::where('region_id', auth()->user()->region_id)->pluck('country_id')->toArray();
            $get_countries = Countries::whereIn('id', $countries_id)->get();
            $get_standards = Standards::all();
            if (!is_null($request->issue_from)) {
                $auditTypes = ['stage_2'];
            } else {
                $auditTypes = ['stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
            }
            if (!empty($request->all())) {


                $companies = Company::whereRaw('name <> ""')->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->where(function ($query) use ($request) {
                        if (!empty($request->region_id)) {
                            $query->whereIn('region_id', $request->region_id);
                        }
                        if (!empty($request->country_id)) {
                            $query->whereIn('country_id', $request->country_id);
                        }
                    })
                    ->where('region_id', auth()->user()->region_id)
                    ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                        $q->where('print_required', 'YES');
                        if (!is_null($request->issue_from)) {
                            $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                        }

                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->where(function ($query) use ($request) {
                            if (!empty($request->status)) {
                                $query->where('certificate_status', $request->status);
                            } else {
                                $query->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                            }
                        });
                    })->with(['companyStandards' => function ($q) use ($request, $issue_from, $issue_to) {
                        $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date','initial_certification_currency','initial_certification_amount','surveillance_amount','re_audit_amount', 'proposed_complexity', 'general_remarks')
                            ->whereIn('type', ['base', 'single'])
                            ->where(function ($query) use ($request) {
                                if (!empty($request->standard_id)) {
                                    $query->whereIn('standard_id', $request->standard_id);
                                }
                            })->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                $q->where('print_required', 'YES');
                                if (!is_null($request->issue_from)) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                }

                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->where(function ($query) use ($request) {
                                    if (!empty($request->status)) {
                                        $query->where('certificate_status', $request->status);
                                    } else {
                                        $query->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }
                                });
                            })->with(['certificates' => function ($q) use ($request) {
                                $q->where('report_status', 'new')->where(function ($query) use ($request) {
                                    if (!empty($request->status)) {
                                        $query->where('certificate_status', $request->status);
                                    } else {
                                        $query->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                                    }
                                });
                            }])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get(['id', 'company_id', 'shift', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides','out_of_country_region_name']);

                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereIn('audit_type', $auditTypes)
                            ->where('status', 'approved')
                            ->whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                            })->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request, $issue_from, $issue_to) {
                                $q->where('print_required', 'YES');
                                if (!is_null($request->issue_from)) {
                                    $q->whereBetween('approval_date', [$issue_from->format('Y-m-d'), $issue_to->format('Y-m-d')]);
                                }
                            })
                            ->whereHas('postAudit', function ($q) use ($companyStd) {
                                $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                            })->get(['audit_type', 'status', 'aj_standard_id', 'job_number', 'id']);

                    }
//
//
                }
            } else {
                $companies = Company::query();
                $companies = $companies->whereRaw('name <> ""')
                    ->where(function ($query) use ($request) {
                        if (auth()->user()->user_type == 'operation_coordinator') {
                            $cityOperationsCoordinator = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', auth()->user()->operation_coordinator->id)
                                ->whereNull('deleted_at')->pluck('city_id')->toArray();
                            $query->whereIn('city_id', $cityOperationsCoordinator);

                        }
                    })
                    ->select('id', 'company_id', 'shift','name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides','out_of_country_region_name')
                    ->with(['region:id,title', 'country:id,name,iso', 'city:id,name,post_code'])
                    ->where('region_id', auth()->user()->region_id)
                    ->whereHas('postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                        $q->where('print_required', 'YES');
                    })->whereHas('companyStandards.certificates', function ($q) use ($request) {
                        $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                    })->with(['companyStandards' => function ($q) use ($request) {
                        $q->select('company_id', 'standard_id', 'type', 'status', 'is_ims', 'id', 'transfer_year', 'is_transfer_standard', 'last_certificate_issue_date','initial_certification_currency','initial_certification_amount','surveillance_amount','re_audit_amount', 'proposed_complexity', 'general_remarks')->whereIn('type', ['base', 'single'])
//                        ->where('status', 'approved')
                            ->whereHas('standard.postAudit.postAuditSchemeInfo', function ($q) use ($request) {
                                $q->where('print_required', 'YES');
                            })->whereHas('certificates', function ($q) use ($request) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                            })->with(['certificates' => function ($q) {
                                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                            }])->with(['companyStandardCodes.standard:id,name,number,status,standards_family_id']);

                    },])->get(['id', 'company_id', 'shift', 'name', 'city_id', 'region_id', 'country_id', 'address', 'scope_to_certify','multiple_sides','out_of_country_region_name']);

                foreach ($companies as $key => $company) {

                    foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {

                        $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereIn('audit_type', $auditTypes)
                            ->where('status', 'approved')
                            ->whereHas('ajStandard', function ($q) use ($companyStd) {
                                $q->where('standard_id', $companyStd->standard_id)->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                                    $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                                });
                            })->whereHas('postAudit', function ($q) use ($companyStd) {
                                $q->where('company_id', $companyStd->company_id)->where('standard_id', $companyStd->standard_id);
                            })->get(['audit_type', 'status', 'aj_standard_id', 'job_number', 'id']);

                    }
                }


            }

        }

        return view('reports.company.post-audit.certificates.index')->with(['companies' => $companies, 'get_standards' => $get_standards, 'get_regions' => $get_regions, 'get_countries' => $get_countries]);

    }
}
