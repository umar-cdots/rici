<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\CompanyStandards;
use App\EnergyCode;
use App\FoodCategory;
use App\FoodSubCategory;
use App\IAF;
use App\IAS;
use App\Mail\OutSourceRequest\OutsourceRequesTaskMail;
use App\Mail\OutSourceRequest\OutsourceRequestToTaskMail;
use App\Notification;
use App\OutSourceRequest;
use App\OutSourceRequestAuditor;
use App\OutSourceRequestRemarks;
use App\OutSourceRequestStandard;
use App\OutSourceRequestStandardAccreditation;
use App\OutSourceRequestStandardEnergyCode;
use App\OutSourceRequestStandardFoodCode;
use App\OutSourceRequestStandardIAFCode;
use App\OutSourceRequestStandardIASCode;
use App\Standards;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\AccessRights;
use DateTime;

class OutsourceRequestController extends ParentController
{
    use AccessRights;

    public function index()
    {
        if (auth()->user()->user_type == 'scheme_manger' || auth()->user()->user_type == 'admin') {

            $outsourceRequests = OutSourceRequest::all();

        } else {
            $outsourceRequests = OutSourceRequest::whereRequestedBy(Auth::user()->id)->get();

        }

        return view('outsource-request.index', compact('outsourceRequests'));
    }

    public function create()
    {
        $standards = Standards::all();
        $accreditations = Accreditation::all();
        $iaf_codes = IAF::all();
        $ias_codes = IAS::all();
        $food_categories = FoodCategory::all();
        $energy_codes = EnergyCode::all();
        $single_standards = $standards->filter(function ($value, $key) {
            return $value->standardFamily->name != 'Food' && $value->standardFamily->name != 'Occupational Health and Safety';
        });


        return view('outsource-request.create', compact('standards', 'accreditations', 'iaf_codes', 'single_standards', 'food_categories', 'energy_codes', 'ias_codes'));
    }

    public function searchAuditor(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());


        $standardId = $request->standard_id;
        $standard = Standards::findOrFail($request->standard_id);
        $standard_family = $standard->standardFamily->name;
        if (strtolower(str_slug($standard_family)) == 'food') {
            $getAccreditationsIds = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();
            $getFoodCategoryIds = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('food_category_id')->pluck('food_category_id')->toArray();

            $accreditations = $getAccreditationsIds;
            $foodCategory = $getFoodCategoryIds;

            $auditorStandardFoodCodes = (new AuditorStandardFoodCode)->newQuery();

            if (!empty($foodCategory)) {
                $auditorStandardFoodCodes->whereIn('food_category_code', $foodCategory);
            }

            if (!empty($accreditations)) {
                $auditorStandardFoodCodes->whereIn('accreditation_id', $accreditations);
            }


            $getAuditorStandardFoodCodes = $auditorStandardFoodCodes->get();
            $auditorStandards = AuditorStandard::where('standard_id', $standardId)
                ->whereIn('id', $getAuditorStandardFoodCodes->pluck('auditor_standard_id')->toArray())
                ->with(['auditor'])->get();

        } elseif (strtolower(str_slug($standard_family)) == 'energy-management') {
            $getAccreditationsIds = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();
            $getEnergyCategoryIds = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('energy_id')->pluck('energy_id')->toArray();

            $accreditations = $getAccreditationsIds;
            $energyCategory = $getEnergyCategoryIds;

            $auditorStandardEnergyCodes = (new AuditorStandardEnergyCode())->newQuery();

            if (!empty($energyCategory)) {
                $auditorStandardEnergyCodes->whereIn('energy_code', $energyCategory);
            }

            if (!empty($accreditations)) {
                $auditorStandardEnergyCodes->whereIn('accreditation_id', $accreditations);
            }


            $getAuditorStandardEnergyCodes = $auditorStandardEnergyCodes->get();
            $auditorStandards = AuditorStandard::where('standard_id', $standardId)
                ->whereIn('id', $getAuditorStandardEnergyCodes->pluck('auditor_standard_id')->toArray())
                ->with(['auditor'])->get();

               


        } else {

            $getIafIds = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('iaf_id')->pluck('iaf_id')->toArray();
            $getIasIds = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('ias_id')->pluck('ias_id')->toArray();
            $getAccreditationsIds = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();


            $accreditations = $getAccreditationsIds;
            $iaf_id = $getIafIds;
            $ias_id = $getIasIds;
            $auditorStandardCodes = (new AuditorStandardCode)->newQuery();

            if (!empty($iaf_id)) {
                $auditorStandardCodes->whereIn('iaf_id', $iaf_id);
            }

            if (!empty($ias_id)) {
                $auditorStandardCodes->whereIn('ias_id', $ias_id);
            }
            if (!empty($accreditations)) {
                $auditorStandardCodes->whereIn('accreditation_id', $accreditations);
            }


            $getAuditorStandardCodes = $auditorStandardCodes->get();
            $auditorStandards = AuditorStandard::where('standard_id', $standardId)
                ->whereIn('id', $getAuditorStandardCodes->pluck('auditor_standard_id')->toArray())
                ->whereHas('auditor',function($q){
                    $q->where('role','auditor')->whereNull('deleted_at');
                })->with('auditor')->get();
         

//            ->with(['auditorStandardCodes', 'auditor', 'standard'])->get();


        }
        $response['status'] = 'success';
        $data = view('outsource-request.search-result-auditor', compact('auditorStandards'))->render();
        // $response['message'] 	= "Returning Data.";
        $response['data'] = [
            'auditors' => $data,
        ];

        return $response;


    }

    public function searchIMSAuditor(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        $companyStandards = CompanyStandards::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->first();
        $companyStandardIds = explode(',', $companyStandards->ims_standard_ids);


        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $request->company_id)->pluck('standard_id')->toArray();
        $getIafIds = CompanyStandardCode::where('company_id', $request->company_id)->whereIn('standard_id', $activeImsStandards)->groupBy('iaf_id')->pluck('iaf_id')->toArray();


        $getIasIds = CompanyStandardCode::where('company_id', $request->company_id)->whereIn('standard_id', $activeImsStandards)->groupBy('ias_id')->pluck('ias_id')->toArray();
        $getAccreditationsIds = CompanyStandardCode::where('company_id', $request->company_id)->whereIn('standard_id', $activeImsStandards)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

        $accreditations = $getAccreditationsIds;
        $iaf_id = $getIafIds;
        $ias_id = $getIasIds;
        $auditorStandardCodes = (new AuditorStandardCode)->newQuery();

        if (!empty($iaf_id)) {
            $auditorStandardCodes->whereIn('iaf_id', $iaf_id);
        }

        if (!empty($ias_id)) {
            $auditorStandardCodes->whereIn('ias_id', $ias_id);
        }
        if (!empty($accreditations)) {
            $auditorStandardCodes->whereIn('accreditation_id', $accreditations);
        }


        $getAuditorStandardCodes = $auditorStandardCodes->get();


        $auditorStandards = AuditorStandard::whereIn('id', $getAuditorStandardCodes->pluck('auditor_standard_id')->toArray())
            ->whereHas('auditor', function ($q) {
                $q->where('role', 'auditor');
            })->groupBy('auditor_id')->get();


        foreach ($auditorStandards as $key => $auditorStandard) {
            $auditorStandards[$key]->auditor = Auditor::where('id', $auditorStandard->auditor_id)->where('role', 'auditor')->first();
        }

        $response['status'] = 'success';
        $data = view('outsource-request.search-result-auditor', compact('auditorStandards'))->render();
        $response['data'] = [
            'auditors' => $data,
        ];

        return $response;


    }

    public function show($id)
    {
//        $outsourceRequest = OutSourceRequest::whereId($id)->whereRequestedBy(Auth::user()->id)->first();
        $outsourceRequest = OutSourceRequest::whereId($id)->first();

        return view('outsource-request.show', compact('outsourceRequest'));

    }

    public function makeOutsourceRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
//            'standard_id' => 'required|exists:standards,id',
            'required_date' => 'required|string',
//            'accreditations.*' => 'required|integer|exists:accreditations,id',
//            'iaf_ids.*' => 'required|integer|exists:i_a_fs,id',
//            'ias_ids.*' => 'required|integer|exists:i_a_s,id',
//            'food_subcategory' => Rule::requiredIf(function () use ($request) {
//                $standard = Standards::find($request->standard_id);
//                if (strtolower(str_slug($standard->standardFamily)) == 'food') {
//                    return $standard->standardFamily;
//                }
//            }),
//            'technical_area' => Rule::requiredIf(function () use ($request) {
//                $standard = Standards::find($request->standard_id);
//                if (strtolower(str_slug($standard->standardFamily)) == 'energy-management') {
//                    return $standard->standardFamily;
//                }
//            }),
            'auditor_id' => 'required|integer|exists:auditors,id',
            'om_remarks' => 'required',
            'company_id' => 'required|integer',
        ]);

        $fromDate = str_replace('/', '-', substr($request->required_date, '0', '10'));
        $from = date("Y-m-d", strtotime($fromDate));
        $toDate = str_replace('/', '-', substr($request->required_date, '13'));
        $to = date("Y-m-d", strtotime($toDate));

        $auditor_user = Auditor::find($request->auditor_id);
        $user = \App\User::find($auditor_user->user->id);
        $regional_manager = \App\User::where('region_id', $user->region_id)->where('user_type', 'operation_manager')->first();


        $auditor_standard_ids = AuditorStandard::where('standard_id', $request->standard_id)->where('auditor_id', $request->auditor_id)->pluck('id')->toArray();
        $auditor_standard_iaf_ids = AuditorStandardCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('iaf_id')->pluck('iaf_id')->toArray();
        $auditor_standard_ias_ids = AuditorStandardCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('ias_id')->pluck('ias_id')->toArray();

        if (!$validator->fails()) {
            $outsource_request = new OutSourceRequest();


            if (Auth()->user()->user_type == 'operation_manager') {
                $outsource_request->operation_manager_id = Auth()->user()->operation_manager->id;
            } elseif (Auth()->user()->user_type == 'operation_coordinator') {
                $outsource_request->operation_coordinator_id = Auth()->user()->operation_coordinator->id;
            }
            $outsource_request->operation_approval_manager_id = $regional_manager->operation_manager->id;
            $outsource_request->from = $from;
            $outsource_request->to = $to;
            $outsource_request->status = 'request';
            $outsource_request->requested_by = Auth::user()->id;

            $outsource_request->save();

            OutSourceRequestRemarks::create([
                'outsource_request_id' => $outsource_request->id,
                'operations_remarks' => $request->om_remarks
            ]);

            $standard = Standards::findOrFail($request->standard_id);
            $standard_family = $standard->standardFamily->name;


            if (strtolower(str_slug($standard_family)) == 'food') {

                $type = 'food';
                $auditor_standard_food_category_ids = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('food_category_code')->pluck('food_category_code')->toArray();
                $auditor_standard_accreditation_ids = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

            } elseif (strtolower(str_slug($standard_family)) == 'energy-management') {

                $type = 'energy-management';
                $auditor_standard_energy_ids = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('energy_code')->pluck('energy_code')->toArray();
                $auditor_standard_accreditation_ids = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

            } else {

                $type = 'generic';
                $auditor_standard_accreditation_ids = AuditorStandardCode::whereIn('auditor_standard_id', $auditor_standard_ids)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

            }


            $outsource_request_standard = OutSourceRequestStandard::create([
                'outsource_request_id' => $outsource_request->id,
                'standard_id' => $request->standard_id,
                'type' => $type,
                'company_id' => $request->company_id
            ]);


            if ($type == 'food') {
                if (!is_null($auditor_standard_food_category_ids) && count($auditor_standard_food_category_ids) > 0) {
                    foreach ($auditor_standard_food_category_ids as $food_category) {
                        OutSourceRequestStandardFoodCode::create([
                            'outsource_request_standard_id' => $outsource_request_standard->id,
                            'food_code_id' => $food_category
                        ]);
                    }
                }
            } else if ($type == 'energy-management') {
                if (!is_null($auditor_standard_energy_ids) && count($auditor_standard_energy_ids) > 0) {
                    foreach ($auditor_standard_energy_ids as $energy_code) {
                        OutSourceRequestStandardEnergyCode::create([
                            'outsource_request_standard_id' => $outsource_request_standard->id,
                            'energy_code_id' => $energy_code
                        ]);
                    }
                }
            } else {
                if (!is_null($auditor_standard_iaf_ids) && count($auditor_standard_iaf_ids) > 0) {
                    foreach ($auditor_standard_iaf_ids as $iaf_id) {
                        OutSourceRequestStandardIAFCode::create([
                            'outsource_request_standard_id' => $outsource_request_standard->id,
                            'iaf_id' => $iaf_id
                        ]);
                    }
                }
                if (!is_null($auditor_standard_ias_ids) && count($auditor_standard_ias_ids) > 0) {
                    foreach ($auditor_standard_ias_ids as $ias_id) {
                        OutSourceRequestStandardIASCode::create([
                            'outsource_request_standard_id' => $outsource_request_standard->id,
                            'ias_id' => $ias_id
                        ]);
                    }
                }

            }
            OutSourceRequestAuditor::create([
                'outsource_request_id' => $outsource_request->id,
                'auditor_id' => $request->auditor_id
            ]);

            if (!is_null($auditor_standard_accreditation_ids) && count($auditor_standard_accreditation_ids) > 0) {
                foreach ($auditor_standard_accreditation_ids as $accreditation) {
                    OutSourceRequestStandardAccreditation::create([
                        'accreditation_id' => $accreditation,
                        'outsource_request_standard_id' => $outsource_request_standard->id
                    ]);
                }
            }
            if (Auth()->user()->user_type == 'scheme_manager' || Auth()->user()->user_type == 'admin') {


            } else {
                if (Auth()->user()->user_type == 'operation_manager') {
                    $sentBY = Auth()->user()->id;
                } elseif (Auth()->user()->user_type == 'operation_coordinator') {
                    $sentBY = Auth()->user()->id;
                }

                $sentMessage = '';
                $company = Company::where('id', $request->company_id)->first();
                $companyStandard = CompanyStandards::where('company_id', $request->company_id)->where('standard_id', $standard->id)->first();
                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                $ims_heading = $companyStandard->standard->name;
                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                        if ($companyStandard->id == $imsCompanyStandard->id) {
                            continue;
                        } else {
                            $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                        }

                    }
                    $sentMessage = 'New for ' . $ims_heading . ' - ' . $auditor_user->fullName();

                } else {
                    $sentMessage = 'New for ' . $standard->name . ' - ' . $auditor_user->fullName();
                }


                //add a notification for manager
                Notification::create([
                    'type' => 'outsource_request',
                    'sent_by' => $sentBY,
                    'sent_to' => $regional_manager->id,
                    'icon' => 'message',
                    'body' => $request->om_remarks,
                    'url' => '/outsource-request/' . $outsource_request->id . '/show',
                    'is_read' => 0,
                    'type_id' => $auditor_user->user->id,
                    'request_id' => $outsource_request->id,
                    'status' => 'unapproved',
                    'sent_message' => $sentMessage

                ]);

//                Mail::to($outsource_request->operation_approval_manager->user->email)->send(new OutsourceRequesTaskMail($outsource_request));

            }
            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Outsource has been sent to scheme manager');

        } else {

            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function makeMultiStandardOutsourceRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'standard_id.0' => 'required|exists:standards,id',
            'required_date' => 'required|string',
            'accreditations.0' => 'required|integer|exists:accreditations,id',
            'iaf_ids.0' => 'required|integer|exists:i_a_fs,id',
            'ias_ids.0' => 'required|integer|exists:i_a_s,id',
            'auditor_id' => 'required|integer|exists:auditors,id'
        ], [
            'standard_id.0.required' => 'Standard is required',
            'accreditations.0.required' => 'Accreditation is required',
            'iaf_ids.0.required' => 'IAF code is required',
            'ias_ids.0.required' => 'IAS code is required'
        ]);


        $auditor_user = Auditor::find($request->auditor_id);
        $regional_manager = User::where('region_id', $auditor_user->user->region_id)->where('user_type', 'operation_manager')->first();

        if (!$validator->fails()) {
            $outsource_request = new OutSourceRequest();


            if (Auth()->user()->user_type == 'operation_manager') {
                $outsource_request->operation_manager_id = Auth()->user()->operation_manager->id;
            } elseif (Auth()->user()->user_type == 'operation_coordinator') {
                $outsource_request->operation_coordinator_id = Auth()->user()->operation_coordinator->id;
            }
            $outsource_request->operation_approval_manager_id = $regional_manager->operation_manager->id;
            $outsource_request->from = $request->required_date;
            $outsource_request->to = $request->required_date;
            $outsource_request->status = 'request';

            $outsource_request->save();
            OutSourceRequestRemarks::create([
                'outsource_request_id' => $outsource_request->id,
                'operations_remarks' => $request->om_remarks
            ]);


            foreach ($request->standard_id as $standard_id) {
                $standard = Standards::findOrFail($standard_id);
                $standard_family = $standard->standardFamily->name;
                if (strtolower(str_slug($standard_family)) == 'food') {
                    $type = 'food';
                } elseif (strtolower(str_slug($standard_family)) == 'energy-management') {
                    $type = 'energy-management';
                } else {
                    $type = 'generic';
                }
                $outsource_request_standard = OutSourceRequestStandard::create([
                    'outsource_request_id' => $outsource_request->id,
                    'standard_id' => $standard_id,
                    'type' => $type
                ]);


            }
            OutSourceRequestAuditor::create([
                'outsource_request_id' => $outsource_request->id,
                'auditor_id' => $request->auditor_id
            ]);

            foreach ($request->accreditations as $accreditation) {
                OutSourceRequestStandardAccreditation::create([
                    'accreditation_id' => $accreditation,
                    'outsource_request_standard_id' => $outsource_request_standard->id
                ]);
            }

            foreach ($request->iaf_ids as $iaf_id) {
                OutSourceRequestStandardIAFCode::create([
                    'outsource_request_standard_id' => $outsource_request_standard->id,
                    'iaf_id' => $iaf_id
                ]);
            }

            foreach ($request->ias_ids as $ias_id) {
                OutSourceRequestStandardIASCode::create([
                    'outsource_request_standard_id' => $outsource_request_standard->id,
                    'ias_id' => $ias_id
                ]);
            }


            Mail::to($outsource_request->operation_approval_manager->user->email)->send(new OutsourceRequesTaskMail($outsource_request));

            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Outsource request has been sent to scheme manager');
        } else {

            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function outSourceRequestApprovedOrReject(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'sm_remarks' => 'required'
        ], [
            'sm_remarks.required' => 'Operational Manager remarks is required'
        ]);
        if (!$validator->fails()) {

            $outsourceRequest = OutSourceRequest::find($request->outsource_request_id);

            OutSourceRequestRemarks::create([
                'outsource_request_id' => $outsourceRequest->id,
                'manager_remarks' => $request->sm_remarks
            ]);

            $outsourceRequest->status = $request->status;
            $outsourceRequest->auditor_status = true;

            if ($request->status == 'approved') {
                $outsourceRequest->approved_date = Date('Y-m-d H:i:s');
                $outsourceRequest->expired_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 10 days'));
            }

            $outsourceRequest->save();


            $outsourceRequestAuditor = OutSourceRequestAuditor::where('outsource_request_id', $request->outsource_request_id)->first();


            if ($outsourceRequest->operation_manager_id != NULL) {
                //add a notification for manager

                $sentTo = $outsourceRequest->operation_manager->user->id;
//                Mail::to($outsourceRequest->operation_manager->user->email)->send(new OutsourceRequestToTaskMail($outsourceRequest));
            } else {
                $sentTo = $outsourceRequest->operation_coordinator->user->id;
//                Mail::to($outsourceRequest->operation_coordinator->user->email)->send(new OutsourceRequestToTaskMail($outsourceRequest));

            }

            $sentMessage = '';
            $company = Company::where('id', $outsourceRequest->outsource_request_standards[0]->company_id)->first();
            $companyStandard = CompanyStandards::where('company_id', $outsourceRequest->outsource_request_standards[0]->company_id)->where('standard_id', $outsourceRequest->outsource_request_standards[0]->standard->id)->first();
            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
            $ims_heading = $companyStandard->standard->name;
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    if ($companyStandard->id == $imsCompanyStandard->id) {
                        continue;
                    } else {
                        $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                    }
                }
                if ($request->status == 'approved') {

                    $sentMessage = 'Approved for ' . $ims_heading . ' - ' . $outsourceRequestAuditor->auditor->fullName();
                } else {
                    $sentMessage = 'Rejected for ' . $ims_heading . ' - ' . $outsourceRequestAuditor->auditor->fullName();
                }

            } else {
                if ($request->status == 'approved') {

                    $sentMessage = 'Approved for ' . $outsourceRequest->outsource_request_standards[0]->standard->name . ' - ' . $outsourceRequestAuditor->auditor->fullName();
                } else {
                    $sentMessage = 'Rejected for ' . $outsourceRequest->outsource_request_standards[0]->standard->name . ' - ' . $outsourceRequestAuditor->auditor->fullName();
                }
            }


            if ($request->status == 'approved') {


                $operation_manager = Notification::where('type_id', (int)$outsourceRequestAuditor->auditor->user->id)->where('type', 'outsource_request')->where('request_id', (int)$outsourceRequest->id)->where('status', 'unapproved')->first();


                Notification::create([
                    'type' => 'outsource_request',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $sentTo,
                    'icon' => 'message',
                    'body' => $request->sm_remarks,
                    'url' => '/outsource-request/' . $outsourceRequest->id . '/show',
                    'is_read' => 0,
                    'type_id' => $outsourceRequestAuditor->auditor->user->id,
                    'request_id' => $outsourceRequest->id,
                    'status' => 'approved',
                    'sent_message' => $sentMessage
                ]);

                $operation_manager->is_read = true;
                $operation_manager->save();


                $outsourceRequestAuditor->status = 'allocated';
                $outsourceRequestAuditor->save();

                $data = [
                    'status' => 'success'
                ];
                $response = (new ApiMessageController())->successResponse($data, 'Request has been approved successfully.');
            } elseif ($request->status == 'rejected') {
                $operation_manager = Notification::where('type_id', (int)$outsourceRequestAuditor->auditor->user->id)->where('type', 'outsource_request')->where('request_id', (int)$outsourceRequest->id)->where('status', 'unapproved')->first();


                Notification::create([
                    'type' => 'outsource_request',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $sentTo,
                    'icon' => 'message',
                    'body' => $request->sm_remarks,
                    'url' => '/outsource-request/' . $outsourceRequest->id . '/show',
                    'is_read' => 0,
                    'type_id' => $outsourceRequestAuditor->auditor->user->id,
                    'request_id' => $outsourceRequest->id,
                    'status' => 'rejected',
                    'sent_message' => $sentMessage
                ]);

                $operation_manager->is_read = true;
                $operation_manager->save();

                $outsourceRequestAuditor->status = 'suggested';
                $outsourceRequestAuditor->save();
                $data = [
                    'status' => 'error'
                ];
                $response = (new ApiMessageController())->successResponse($data, 'Request has been rejected.');
            }

        } else {

            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }
}
