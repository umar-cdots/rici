<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use App\Traits\AccessRights;
use Spatie\Permission\Models\Role;
use App\User;

class PermissionController extends Controller
{
    use AccessRights;

    public function index()
    {


        $permissions = Permission::all();
        $roles = Role::all();

        return view('permission.index', compact('permissions', 'roles'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('permission.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'role_id' => 'required',
            'permissions.0' => 'required',
        ], [
            'role_id.required' => 'Role is required',
            'permissions.0.required' => 'At least one Permission is required',

        ]);


        if ($request->has('permissions') && $request->has('role_id')) {
            foreach ($request->permissions as $permission) {
                $check_exist_record = Permission::where('name', $permission)->first();
                if ($check_exist_record) {
                    $role = Role::findById($request->role_id);
                    if (!is_null($role)) {
                        $role->givePermissionTo($check_exist_record);
                        $get_all_user = DB::table('model_has_roles')->where('role_id', $role->id)->get();
                        if (!empty($get_all_user) && count($get_all_user) > 0) {
                            foreach ($get_all_user as $user_id) {
                                $user = User::where('id', $user_id->model_id)->first();
                                if (!is_null($user)) {
                                    $user->givePermissionTo($check_exist_record);
                                }

                            }
                        }

                    }


                } else {
                    $data_permission = Permission::create([
                        'name' => $permission,
                        'role_id' => $request->role_id,
                        'guard_name' => 'web',
                        'heading' => ucwords(str_replace("_", " ", $permission)),
                    ]);

                    $role = Role::findById($request->role_id);
                    if (!is_null($role)) {
                        $role->givePermissionTo($data_permission);
                        $get_all_user = DB::table('model_has_roles')->where('role_id', $role->id)->get();
                        if (!empty($get_all_user) && count($get_all_user) > 0) {
                            foreach ($get_all_user as $user_id) {
                                $user = User::where('id', $user_id->model_id)->first();
                                if (!is_null($user)) {
                                    $user->givePermissionTo($data_permission);
                                }
                            }
                        }
                    }
                }

            }
        }

        return redirect()->route('permissions.index')->with(['flash_status' => 'success', 'flash_message' => 'Permission has been added successfully']);
    }

    public function edit($id)
    {
        $permission = Permission::findById($id);

        return view('permission.edit', compact('permission'));
    }

    public function update(Request $request, $id)
    {
        $this->validator($request->all())->validate();
        $permission = Permission::findById($id);
        $permission->update([
            'name' => $request->permission,
            'guard_name' => 'web'
        ]);

        return back()->with('success', 'Permission has been updated successfully');
    }

    public function destroy($id)
    {
        $permission = Permission::findById($id);
        $permission->delete();

        return back()->with('success', 'Permission has been deleted successfully');
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'permission' => 'required|string'
        ]);
    }
}
