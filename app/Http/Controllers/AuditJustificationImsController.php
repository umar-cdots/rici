<?php

namespace App\Http\Controllers;

use App\AJRemarks;
use App\AjStageChange;
use App\AJStageTeam;
use App\AJStandard;
use App\AJStandardStage;
use App\AuditJustification;
use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\Certificate;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\CompanyStandards;
use App\CompanyStandardsAnswers;
use App\FoodCategory;
use App\FoodSubCategory;
use App\Grades;
use App\Notification;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\StandardsQuestions;
use App\TechnicalExpertStandard;
use App\Traits\GeneralHelperTrait;
use App\Traits\MandayTrait;
use App\User;
use App\OutSourceRequest;
use App\OutSourceRequestAuditor;
use App\OutSourceRequestStandard;
use App\AuditorStandardGrade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
use Mockery\Matcher\Not;
use App\Accreditation;
use App\IAS;
use App\IAF;

use App\EnergyCode;


class AuditJustificationImsController extends Controller
{
    use MandayTrait, GeneralHelperTrait;

    public function index($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::findOrFail($company_id);

        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);
        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }

        if ($ims != null) {
            //for ims
        } else {

            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();

            $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

            //calculate manday of food family,energy family and generic family

            if (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Food') {

                $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->where('company_standards_id', $c_std->id)->get();

//                dd($standardQuestions);
                foreach ($standardQuestions as $index => $questions) {

                    if ($index === 0) {
                        $haccp = $questions->answer;
                    } elseif ($index === 1) {
                        $certified_management = $questions->answer;
                    }
//                    elseif ($index === 2) {
//                        $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
////                        $food_category_id = FoodCategory::where('code', json_decode($questions->answer))->first()->id;
//                        $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
//                    }
                }

                $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
                $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
//                dd([$standard_number->id, $food_category_id, $haccp, $company->effective_employees, $certified_management, $company->childCompanies->count(), $c_std->surveillance_frequency]);
                $manday = $this->generateFoodSafetyTable($standard_number->id, $food_category_id, $haccp, $company->effective_employees, $certified_management, $company->childCompanies->count() + 1, $c_std->surveillance_frequency);

            } elseif (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Energy Management') {
                $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->where('company_standards_id', $c_std->id)->get();

                foreach ($standardQuestions as $index => $questions) {

                    if ($index === 1) {
                        $energy_consumption_in_tj = $questions->answer;
                    } elseif ($index === 3) {
                        $no_of_energy_resources = $questions->answer;
                    } elseif ($index === 4) {
                        $no_of_significant_energy = $questions->answer;
                    }
                }
                $manday = $this->generateEnergyTable($standard_number->id, $energy_consumption_in_tj, $no_of_energy_resources, $no_of_significant_energy, $company->effective_employees, $c_std->surveillance_frequency);
            } else {

                $companyStandardIds = explode(',', $c_std->ims_standard_ids);
                $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->pluck('standard_id')->toArray();
                $activeImsComplexity = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->pluck('proposed_complexity')->toArray();


                $manday = $this->generateIMSTable($activeImsStandards, $activeImsComplexity, $company->effective_employees, $c_std->surveillance_frequency);

            }


            if (empty($manday)) {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Man days not available for this standard');
            } else {

                $calc_manday = [];

                $calc_manday = $manday;


                $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];

                $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();
                if (!empty($scheme_manager_standards)) {
                    if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) { //true

                        if (!empty($company->companyStandards)) {

                            foreach ($company->companyStandards as $standard) {
                                $company_standards[] = $standard->standard_id;
                            }

                            $getAllImsStandardsData = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
                            $ims_standard_ids = explode(',', $getAllImsStandardsData->ims_standard_ids);
                            $activeImsStandards = CompanyStandards::whereIn('id', $ims_standard_ids)->where('company_id', $company->id)->pluck('standard_id')->toArray();

                            if (count($activeImsStandards) > 2) {
                                $auditor_standards = DB::table('auditor_standards')
                                    ->select('auditor_id', DB::raw('GROUP_CONCAT(standard_id) as standardCount'))
                                    ->whereIn('standard_id', $activeImsStandards)
                                    ->whereNull('deleted_at')
                                    ->groupBy('auditor_id')
                                    ->havingRaw('COUNT(*) = 3')
                                    ->get();
                            } else {
                                $auditor_standards = DB::table('auditor_standards')
                                    ->select('auditor_id', DB::raw('GROUP_CONCAT(standard_id) as standardCount'))
                                    ->whereIn('standard_id', $activeImsStandards)
                                    ->whereNull('deleted_at')
                                    ->groupBy('auditor_id')
                                    ->havingRaw('COUNT(*) = 2')
                                    ->get();
                            }
                            if ($auditor_standards->isNotEmpty()) {
                                //checking the stage1 and stage2 combine aj

                                $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                if ($audit_type == 'stage_1|stage_2') {
                                    $combine_tracker = 1;
                                } else {
                                    $combine_tracker = 0;
                                }


                                //check the company already have any aj
//                                $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                if (!empty($aj_obj)) {


                                    //check which standards are in company aj
                                    $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                        ->where('standard_id', $standard_number->id)
                                        ->first();


                                    if (!empty($aj_standards_obj)) {


                                        if ($combine_tracker == 1) {

                                            if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                $standardStagesObject = AJStandardStage::whereIn('audit_type', ['stage_1', 'stage_2'])->where('aj_standard_id', (int)$aj_standards_obj->id)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;


                                            } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;
//
                                            }

                                        } else {

                                            if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $audit_type)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;


                                            } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                $aj_standard_stage = $standardStagesObject;
                                                $aj_stages_manday = $standardStagesObject;
//
                                            }
                                        }
                                        $job_number = $standardStagesObject[0]->job_number;
                                        //return to the edit page
                                        return view('audit-justification.ims.create', compact('job_number', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'combine_tracker', 'ims'));


                                    } else {
                                        $audit_justification = AuditJustification::updateOrCreate(
                                            [
                                                'company_id' => $company->id
                                            ],
                                            [
                                                'company_id' => $company->id
                                            ]
                                        );


                                        foreach ($activeImsStandards as $standard) {
                                            $aj_stardard = $audit_justification->ajStandards()->create(
                                                [
                                                    'standard_id' => $standard
                                                ]
                                            );

                                        }


                                        if ($c_std->surveillance_frequency === 'biannual') {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                                        } else {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                        }
                                        $i = 0;
//                                        $job_number = $this->jobNumber(auth()->user()->country->iso);
                                        $job_number = null;

                                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                        foreach ($aj_audit_types as $aj_audit_type) {
                                            foreach ($activeImsStandards as $standard) {
                                                $aj_stardard = AJStandard::where('audit_justification_id', $aj_obj->id)->where('standard_id', $standard)->whereNull('deleted_at')->first();
                                                $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                    'audit_type' => $aj_audit_type,
                                                    'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                    'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                    'job_number' => $job_number,

                                                ]);
                                            }
                                        }


                                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                        if ($audit_type == 'stage_1|stage_2') {
                                            $combine_tracker = 1;
                                        } else {
                                            $combine_tracker = 0;
                                        }

                                        if (!empty($aj_obj)) {

                                            //check which standards are in company aj
                                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                                ->where('standard_id', $standard_number->id)
                                                ->first();


                                            if (!empty($aj_standards_obj)) {
                                                if ($combine_tracker == 1) {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::whereIn('audit_type', ['stage_1', 'stage_2'])->where('aj_standard_id', (int)$aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;


                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                        //get the standard stage stage_1

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;
//
                                                    }

                                                } else {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $audit_type)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;


                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                        //get the standard stage stage_1

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;
//
                                                    }


                                                }
                                            }
                                        }

                                        return view('audit-justification.ims.create', compact('job_number', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'combine_tracker', 'ims'));
//
                                    }

                                } else {

                                    $audit_justification = AuditJustification::updateOrCreate(
                                        [
                                            'company_id' => $company->id
                                        ],
                                        [
                                            'company_id' => $company->id
                                        ]
                                    );
                                    if (!$audit_justification->ajStandards->isNotEmpty()) {


                                        foreach ($activeImsStandards as $standard) {
                                            $aj_stardard = $audit_justification->ajStandards()->create(
                                                [
                                                    'standard_id' => $standard
                                                ]
                                            );
                                        }


                                        if ($c_std->surveillance_frequency === 'biannual') {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

                                        } else {
                                            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'reaudit'];
                                        }

                                        $i = 0;
//                                        $job_number = $this->jobNumber(auth()->user()->country->iso);
                                        $job_number = null;

                                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                                        foreach ($aj_audit_types as $aj_audit_type) {


                                            foreach ($activeImsStandards as $standard) {
                                                $aj_stardard = AJStandard::where('audit_justification_id', $aj_obj->id)->where('standard_id', $standard)->whereNull('deleted_at')->first();
                                                $aj_standard_stages_obj = $aj_stardard->ajStandardStages()->create([
                                                    'audit_type' => $aj_audit_type,
                                                    'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                                    'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                                    'job_number' => $job_number,

                                                ]);
                                            }
                                            $i++;
                                        }

                                        if ($audit_type == 'stage_1|stage_2') {
                                            $combine_tracker = 1;
                                        } else {
                                            $combine_tracker = 0;
                                        }


                                        if (!empty($aj_obj)) {

                                            //check which standards are in company aj
                                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                                ->where('standard_id', $standard_number->id)
                                                ->first();

                                            if (!empty($aj_standards_obj)) {

                                                if ($combine_tracker == 1) {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::whereIn('audit_type', ['stage_1', 'stage_2'])->where('aj_standard_id', (int)$aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;


                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;
//
                                                    }

                                                } else {

                                                    if ((auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator')) {

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $audit_type)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;

                                                    } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                                        //get the standard stage stage_1

                                                        $standardStagesObject = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                                        $aj_standard_stage = $standardStagesObject;
                                                        $aj_stages_manday = $standardStagesObject;

                                                    }


                                                }
                                            }

                                            return view('audit-justification.ims.create', compact('job_number', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'combine_tracker', 'ims'));
                                        }
                                    }

                                }
                            } else {
                                return back()->with('flash_status', 'warning')
                                    ->with('flash_message', 'Auditor standard not available');
                            }
                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Company does not have this standard');
                        }
                    }
                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Scheme manager not available for this standard');
                }
            }
        }

    }

    public function updateStageManday(Request $request)
    {
        $mandayStage = AJStandardStage::findOrFail((int)$request->id);
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();


        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
            foreach ($activeImsStandards as $standard) {
                $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();


                if (!is_null($ajStandard)) {
                    $ajStandardStage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', $mandayStage->recycle)->first();
                    if (!is_null($ajStandardStage)) {
                        $ajStandardStage->update([
                            $request->site => $request->value
                        ]);
                    }
                }

            }
        }


        $data = [
            'status' => 'success',
            'message' => 'Manday Value has been edit successfully'
        ];
        return response()->json($data);
    }

    public function store(Request $request)
    {

        $checkTeamMember = $this->checkTeam($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);
            return $response;
        }


        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'offsite' => 'required|string',
            'supervisor_involved.stage1' => 'required|string',
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam.*.required' => 'Please select the auditor member',
            ]);


        if (!$validator->fails()) {
            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }
            if (!empty($request->perposedTeam)) {
                foreach ($request->standard_stage_id as $key => $standard_stage_id) {

                    $mandayStage = AJStandardStage::findOrFail((int)$standard_stage_id);
                    $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
                    $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();

                    if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                        foreach ($activeImsStandards as $keyIndex => $standard) {
                            $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                            if (!is_null($ajStandard)) {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->first();

                                $expectedDate = $request->expected_date[0] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[0]);
                                $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);


                                if ($request->status == 'created') {
                                    $status = $request->status;
                                } elseif ($request->status == 'approved_scheme') {
                                    $status = 'approved';
                                } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                                    $status = 'applied';
                                } else {
                                    $status = 'created';
                                }

                                if ($request->status == 'approved_scheme' && $approvalDate != null && $standard_stage->approval_date != $approvalDate) {
                                    $lastApprovalDate = $standard_stage->approval_date;
                                } else {
                                    $lastApprovalDate = $standard_stage->last_approval_date;
                                }
                                $standard_stage->update([


                                    'certificate_scope' => $request->certificate_scope,
                                    'manday_remarks' => $request->manday_remarks,
                                    'excepted_date' => $expectedDate,
//                                    'supervisor_involved' => $request->supervisor_involved[str_replace('_', '', $stages[$keyIndex])],
                                    'supervisor_involved' => $request->supervisor_involved[str_replace('_', '', $mandayStage->audit_type)],
                                    'evaluator_name' => $request->evaluator_name,
                                    'approval_date' => $approvalDate,
                                    'last_approval_date' => $lastApprovalDate,
                                    'status' => $status,
                                    'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                                    'job_number' => $request->job_number
                                ]);

//                                if ($standard_stage->job_number != $request->job_number) {
//                                    $job_number = DB::table('job_numbers')->where('id', 1)->update([
//                                        'country_code' => substr($request->job_number, 0, 3),
//                                        'year' => substr($request->job_number, 3, 2),
//                                        'code' => substr($request->job_number, 5, 4),
//                                        'created_at' => date('Y-m-d H:i:s'),
//                                    ]);
//                                }

                                $standard_stage->ajStageTeams()->delete();


                                if (!empty($request->perposedTeam)) {


                                    $supervisor = 0;
                                    if ($request['supervisor_involved']['stage1'] == "yes") {
                                        $supervisor = 1;
                                    }

                                    foreach ($request->perposedTeam as $key => $a_member) {


                                        $standard_stage->ajStageTeams()->create([
                                            'auditor_id' => $a_member,
                                            'supervisor_involved' => $supervisor,
                                            'grade_id' => $request->perposed_team_grade_id[$key],
                                            'member_name' => $request->perposed_team_name[$key],
                                            'member_type' => $request->perposed_team_type[$key],
                                        ]);

                                        $auditor = Auditor::find((int)$a_member)->update([
                                            'is_available' => 0
                                        ]);

                                    }

                                    $aj_standard = AJStandard::findOrFail((int)$request->aj_standard_id);
                                    $standard_stage = AJStandardStage::findOrFail((int)$request->standard_stage_id);

                                }

                            }
                        }
                    }

                }

                $company = Company::find($request->company_id);
                $company->is_edit_access_to_operations = false;
                $company->save();
                $data = [
                    'aj_id' => $standard_stage,
                    'status' => 'success',
                ];

                $response = (new ApiMessageController())->successResponse($data, 'AJ has been created');
                return $response;
            } else {
                $data = [
                    'flash_status' => 'warning',
                    'flash_message' => 'Team not available for this standard'
                ];
                $response = $data;
                return $response;
            }
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }

    public function store_multiple(Request $request)
    {

        $checkTeamMember = $this->checkTeamForMultiple($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);
            return $response;
        }


        $aj_date = $request->aj_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->aj_date);


        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'frequency' => 'required|string',
            'offsite' => 'required|string',
//            'job_number' => 'required|max:9|unique:aj_standard_stages',
//            'manday_remarks' => 'string|sometimes',
            'supervisor_involved.stage1' => 'required|string',
//            'expected_date' => Rule::requiredIf(function () use ($request) {
//                if ($request['supervisor_involved']['stage1'] == "yes") {
//                    return true;
//                }
//            }),
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam1.*.required' => 'Please select the auditor member',
            ]);

        if (!$validator->fails()) {
            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }


            if (count($request->perposedTeam1) == 2) {
                $standard_stage_return = [];

                foreach ($stages as $stage) {
                    foreach ($request->standard_stage_id as $key => $standard_stage_id) {
                        $mandayStage = AJStandardStage::findOrFail((int)$standard_stage_id);
                        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
                        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();
                        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                            foreach ($activeImsStandards as $key1 => $standard) {
                                $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                                if (!is_null($ajStandard)) {
                                    $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $stage)->first();
//                                    if ($standard_stage->job_number != $request->job_number) {
//                                        $job_number = DB::table('job_numbers')->where('id', 1)->update([
//                                            'country_code' => substr($request->job_number, 0, 3),
//                                            'year' => substr($request->job_number, 3, 2),
//                                            'code' => substr($request->job_number, 5, 4),
//                                            'created_at' => date('Y-m-d H:i:s'),
//                                        ]);
//                                    }


                                    $expectedDate = $request->expected_date[str_replace('_', '', $stage)] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[str_replace('_', '', $stage)]);
                                    $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);

                                    if ($request->status == 'created') {
                                        $status = $request->status;
                                    } elseif ($request->status == 'approved_scheme') {
                                        $status = 'approved';
                                    } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                                        $status = 'applied';
                                    } else {
                                        $status = 'created';
                                    }

                                    $standard_stage->update([


                                        'certificate_scope' => $request->certificate_scope,
                                        'aj_date' => $aj_date->format('Y-m-d'),
                                        'manday_remarks' => $request->manday_remarks,
                                        'excepted_date' => $expectedDate,
                                        'supervisor_involved' => $request->supervisor_involved[str_replace('_', '', $stage)],
                                        'evaluator_name' => $request->evaluator_name,
                                        'approval_date' => $approvalDate,
                                        'last_approval_date' => $standard_stage->approval_date,
                                        'status' => $status,
                                        'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                                        'job_number' => $request->job_number
                                    ]);
                                    if ($key1 == 0) {
                                        $standard_stage_return[str_replace('_', '', $stage)] = $standard_stage;
                                    }


                                    foreach ($standard_stage->ajStageTeams as $ajStageTeam) {
                                        $ajStageTeam->auditor->update([
                                            'is_available' => 1
                                        ]);
                                    }

                                    $standard_stage->ajStageTeams()->delete();
                                    if (!empty($request->perposedTeam1)) {
                                        $supervisor = 0;
                                        if ($request['supervisor_involved'][str_replace('_', '', $stage)] == "yes") {
                                            $supervisor = 1;
                                        }

                                        foreach ($request->perposedTeam1[str_replace('_', '', $stage)] as $key => $a_member) {

                                            $standard_stage->ajStageTeams()->create([
                                                'auditor_id' => (int)$a_member,
                                                'supervisor_involved' => $supervisor,
                                                'grade_id' => (int)$request->perposed_team_grade_id[str_replace('_', '', $stage)][$key],
                                                'member_name' => $request->perposed_team_name[str_replace('_', '', $stage)][$key],
                                                'member_type' => $request->perposed_team_type[str_replace('_', '', $stage)][$key],
                                            ]);
                                            $auditor = Auditor::find((int)$a_member)->update([
                                                'is_available' => 0
                                            ]);
                                        }
                                    }

                                    if ($stage == 'stage_2') {

                                        if ($request->expected_date[str_replace('_', '', $stage)]) {

                                            $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->get();
                                            $frequency = $request->frequency;
                                            if ($frequency === 'biannual') {
                                                $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                                $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                                $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                                $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                                $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                                $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
                                                $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                                $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];

                                            } else {

                                                $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                                $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                                $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($request->expected_date[str_replace('_', '', $stage)])));
                                                $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
                                                $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];

                                            }
                                            foreach ($aj_audit_types as $key => $aj_audit_type) {
                                                foreach ($next_stages as $nextStage) {
                                                    if ($nextStage->audit_type == $aj_audit_type) {
                                                        $standard_stage = AJStandardStage::findOrFail((int)$nextStage->id);
                                                        $standard_stage->update([
                                                            'excepted_date' => $due_date[$key],
                                                        ]);
                                                    }
                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                $data = [
                    'aj_id' => $standard_stage_return,
                    'status' => 'success',
                ];
                $response = (new ApiMessageController())->successResponse($data, 'AJ has been created');
                return $response;
            } else {
                $data = [
                    'flash_status' => 'warning',
                    'flash_message' => 'Team not available for this standard'
                ];
                $response = $data;
                return $response;
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }


    public function edit($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::findOrFail($company_id);
        $audit_type = str_replace(' ', '_', strtolower($audit_type));
        $standard_number = Standards::where('name', $standard_number)->first();
        $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
        if ($audit_type === 'stage_1') {
            $checkStandardStage = AJStandardStage::where(['audit_type' => $audit_type, 'id' => $aj_standard_id])->first();

            if (!is_null($checkStandardStage) && ($checkStandardStage->status === 'Not Applied' || $checkStandardStage->status === 'rejected')) {
                $this->updateMandays($audit_type, $standard_number, $c_std, $company, $checkStandardStage);
            }
        }


        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        }
        if (is_null($role)) {
            $role = auth()->user()->user_type;
        }

        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {
            $job_number = null;
//            $job_number = $this->jobNumber(auth()->user()->country->iso);
            $ims = 0;


            $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
            $company_standards = [];
            $auditors = [];

            $certificateStatus = Certificate::where('company_standard_id', $c_std->id)->orderBy('id', 'desc')->first();

            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];
            $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();
            if (!empty($scheme_manager_standards)) {
                if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                    if (!empty($company->companyStandards)) {

                        foreach ($company->companyStandards as $standard) {
                            $company_standards[] = $standard->standard_id;
                        }

//                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                        $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();

                        if (!empty($auditor_standards)) {
                            foreach ($auditor_standards as $auditor_standard) {
                                foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

//                                    if ($auditor_standard->auditor->is_available == 1) {
                                    $members[] = $auditor_standard;
                                    $standard_grades[] = $standard_grade;

                                }
//                                }
                                foreach ($company->companyIAFCodes as $key => $companyIaf) {

                                    $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->where('ias_id', '=', $companyIaf->ias_id)->where('accreditation_id', '=', $companyIaf->accreditation_id)->get();
                                }

                            }
                            //checking the stage1 and stage2 combine aj
                            if ($audit_type == 'stage_1|stage_2') {

                                $combine_tracker = 1;
                            } else {

                                $combine_tracker = 0;
                            }

                            //check the company already have any aj
                            $aj_obj = AuditJustification::where('company_id', $company->id)->first();

                            $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                            $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                            if (!empty($aj_obj)) {
                                //check which standards are in company aj
                                $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                    ->where('standard_id', $standard_number->id)
                                    ->first();

                                $team_members_stage1 = [];
                                if ($audit_type == 'stage_2') {
                                    $aj_standard_stages_obj_stage1 = AJStandardStage::where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standards_obj->id)->first();
                                    $team_members_stage1 = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stages_obj_stage1->id)->get();
                                }
//

                                if (!empty($aj_standards_obj)) {
                                    if ($combine_tracker == 1) {
                                        //get the standard stage stage_1
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                    } else {
                                        $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    }

                                    if (!empty($aj_standard_stages_obj)) {
                                        //now we want the single audit stage which want to edit
                                        $aj_standard_stage = $aj_standard_stages_obj;
                                        //also need all other audit stages for manday editing


                                        if (!empty($c_std)) {
                                            $outSourceRequestStandardId = OutSourceRequestStandard::where('standard_id', $standard_number->id)->where('company_id', $company->id)->pluck('id')->toArray();
                                            $outsourceRequestsId = OutSourceRequest::whereIn('id', $outSourceRequestStandardId)->where('status', 'approved')->where('expiry_status', 'not expired')->pluck('id')->toArray();
                                            $outsourceRequestAuditorId = OutSourceRequestAuditor::whereIn('outsource_request_id', $outsourceRequestsId)->where('status', 'allocated')->pluck('auditor_id')->toArray();
                                            $auditorId = Auditor::whereIn('id', $outsourceRequestAuditorId)->where('auditor_status', 'active')->pluck('id')->toArray();
                                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditorId)->where('standard_id', $standard_number->id)->where('status', 'active')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                                            if ($auditorStandardId) {
                                                $auditors = Auditor::whereIn('id', $auditorStandardId)->where('auditor_status', 'active')->get();
                                                $auditors->each(function ($auditor, $key) use ($auditors, $standard_number) {
                                                    $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('standard_id', $standard_number->id)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                                    $auditorStandards = $auditors[$key]->auditorStandards;

                                                    $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards) {

                                                        $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                                    });
                                                });
                                            }
                                        }


//                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
//
//
//                                            $mandays = $aj_standards_obj->ajStandardStages;
//
//                                        } else {
//                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->where('audit_type', $audit_type)->first();
//                                        }


                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                            if ($audit_type == 'stage_2') {
                                                $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                                $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                            } else {
                                                $mandays_stage_1 = [];
                                                $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                            }

                                        } else {
                                            if ($audit_type == 'stage_2') {
                                                $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                                $mandays = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', $audit_type)->first();
                                            } else {
                                                $mandays_stage_1 = [];
                                                $mandays = AJStandardStage::where('id', (int)$aj_standard_id)->where('audit_type', $audit_type)->first();
                                            }

                                        }

                                        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
                                            $aj_stages_manday[] = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->get();
                                        } else {
                                            $aj_stages_manday[] = $mandays;
                                        }

//                                        $aj_stages_manday[] = $mandays;
                                        $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();
                                        $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();
                                        //return to the edit page
                                        $checkApproved = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                        if ($checkApproved->status == 'approved') {
                                            $approvalPerson = Notification::where('request_id', (int)$aj_standard_id)->where('status', 'approved')->where('type', 'audit_justification')->first();
                                            $defaultSchemeManager = SchemeManager::whereHas('standards', function ($q) use ($standard_number) {
                                                $q->where('standard_id', $standard_number->id);
                                            })->with('user')->first();
                                            $pdf = \Barryvdh\DomPDF\Facade::loadView('audit-justification.AjPrintIMSPreview.index', compact('aj_standard_change', 'certificateStatus', 'defaultSchemeManager', 'mandays_stage_1', 'approvalPerson', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');
                                            Storage::put('public/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf', $pdf->output());
                                            $href = 'storage/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf';

                                        } else {
                                            $href = '#';
                                        }


//                                        update approved aj stages

                                        if ($aj_standard_stage->status == 'approved') {

                                            $checkLastRecord = Notification::where('request_id', $aj_standard_stage->id)->where('type', 'audit_justification')->get()->last();


                                            if (!is_null($checkLastRecord) && $checkLastRecord->status == 'approved') {
                                                $notificationUpdate = Notification::where('request_id', $aj_standard_stage->id)->where('type', 'audit_justification')->update([
                                                    'is_read' => true
                                                ]);
                                            }

                                        }


                                        if (auth()->user()->user_type == 'scheme_manager') {

                                            if ($aj_standard_stage->audit_type == 'stage_2') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Stage 1 First',

                                                    ]);
                                                }

                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_1') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'stage_2')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Stage 2 First',

                                                    ]);
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_2') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_1')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Surveillance 1 First',

                                                    ]);
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_3' && $c_std->surveillance_frequency == 'biannual') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_2')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Surveillance 2 First',

                                                    ]);
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_4' && $c_std->surveillance_frequency == 'biannual') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_3')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Surveillance 3 First',

                                                    ]);
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'surveillance_5' && $c_std->surveillance_frequency == 'biannual') {
                                                $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_4')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                if (!is_null($checkApprovedAj)) {
                                                    return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                } else {
                                                    return redirect()->back()->with([
                                                        'flash_status' => 'warning',
                                                        'flash_message' => 'Please Approved Surveillance 4 First',

                                                    ]);
                                                }
                                            } elseif ($aj_standard_stage->audit_type == 'reaudit') {
                                                if ($c_std->surveillance_frequency == 'biannual') {
                                                    $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_5')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        return redirect()->back()->with([
                                                            'flash_status' => 'warning',
                                                            'flash_message' => 'Please Approved Surveillance 5 First',

                                                        ]);
                                                    }
                                                } elseif ($c_std->surveillance_frequency == 'annual') {
                                                    $checkApprovedAj = AJStandardStage::where('status', 'approved')->where('audit_type', 'surveillance_2')->where('aj_standard_id', $aj_standard_stage->aj_standard_id)->first();
                                                    if (!is_null($checkApprovedAj)) {
                                                        return view('audit-justification.ims.edit', compact('certificateStatus', 'href', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                                    } else {
                                                        return redirect()->back()->with([
                                                            'flash_status' => 'warning',
                                                            'flash_message' => 'Please Approved Surveillance 2 First',

                                                        ]);
                                                    }
                                                }

                                            } else {

                                                return view('audit-justification.ims.edit', compact('href', 'certificateStatus', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                            }
                                        } else {

                                            return view('audit-justification.ims.edit', compact('href', 'certificateStatus', 'notifications', 'job_number', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims', 'standard_codes', 'auditors'));
                                        }


//

                                    }
                                }
                            }

                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Auditor standard not available');
                        }
                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Company does not have this standard');
                    }
                }
            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Scheme manager not available for this standard');
            }
        }


    }

    public function update(Request $request)
    {


        $aj_date = $request->aj_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->aj_date);

        $checkTeamMember = $this->checkTeam($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);

            return $response;
        }

        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required|string',
            'offsite' => 'required|string',
//            'job_number' => 'required|string',
            'manday_remark' => 'string|sometimes',
            'supervisor_involved.stage1' => 'required|string',
            'std_number' => Rule::requiredIf(function () use ($request) {
                return $request['ch_std'] == "yes";
            }),
            'expected_date' => Rule::requiredIf(function () use ($request) {
                return $request['supervisor_involved[stage1]'] == "yes";
            }),
//           'evaluator_name' => 'required',
//           'approval_date' => 'required',
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam.*.required' => 'Please select the auditor member',
            ]);

        if (!$validator->fails()) {


            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }

            if (!empty($request->perposedTeam)) {

                $mandayStage = AJStandardStage::findOrFail((int)$request->standard_stage_id);
                $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
                $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();

                if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                    foreach ($activeImsStandards as $key => $standard) {
                        $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                        if (!is_null($ajStandard)) {


                            if (!is_null($request->recycle)) {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$request->recycle)->first();
                            } else {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                            }
                            if (!is_null($standard_stage)) {

//                                if ($key == 0 && $standard_stage->job_number != $request->job_number) {
//
//                                    $jobNumber = AJStandardStage::where('job_number', $request->job_number)->first();
//
//                                    if (!empty($jobNumber)) {
//                                        $data = [
//                                            'flash_status' => 'warning',
//                                            'flash_message' => 'job number already exist'
//                                        ];
//                                        $response = $data;
//                                        return $response;
//                                    } else {
//
//                                    }
//
//
//                                }

//                                if ($key == 0 && $standard_stage->job_number != $request->job_number && auth()->user()->user_type == 'scheme_manager') {
//
//                                    $jobNumber = AJStandardStage::where('job_number', $request->job_number)->first();
//
//                                    if (!empty($jobNumber)) {
//                                        $data = [
//                                            'flash_status' => 'warning',
//                                            'flash_message' => 'job number already exist'
//                                        ];
//                                        $response = $data;
//                                        return $response;
//                                    } else {
//
////                        if ($standard_stage->audit_type == 'stage_1') {
//                                        $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                                            'job_number' => $request->job_number
//                                        ]);
////                        }
//
//                                    }
//
//
//                                } else {
////                    if ($standard_stage->audit_type == 'stage_1') {
//                                    $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                                        'job_number' => $request->job_number
//                                    ]);
////                    }
//                                }

//                                if ($key == 0) {
//                                    if ($standard_stage->job_number != $request->job_number && auth()->user()->user_type == 'scheme_manager') {
//
//                                        $jobNumber = AJStandardStage::where('job_number', $request->job_number)->where('aj_standard_id', '!=', $standard_stage->aj_standard_id)->first();
//
//
//                                        if (!is_null($jobNumber)) {
//                                            $data = [
//                                                'flash_status' => 'warning',
//                                                'flash_message' => 'job number already exist'
//                                            ];
//                                            $response = $data;
//                                            return $response;
//                                        } else {
//
////                        if ($standard_stage->audit_type == 'stage_1') {
//                                            $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                                                'job_number' => $request->job_number
//                                            ]);
////                        }
//
//                                        }
//
//
//                                    } else {
////                    if ($standard_stage->audit_type == 'stage_1') {
//                                        $ajStages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->update([
//                                            'job_number' => $request->job_number
//                                        ]);
////                    }
//                                    }
//                                }

                                $expectedDate = $request->expected_date[0] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[0]);
                                $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);
                                $actual_expected_date = $request->actual_expected_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->actual_expected_date);
                                $valid_till_date = $request->valid_till_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->valid_till_date);


                                if ($request->status == 'created') {
                                    $status = $request->status;
                                } elseif ($request->status == 'approved_scheme') {
                                    $status = 'approved';
                                } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                                    $status = 'applied';
                                } else {
                                    $status = 'created';
                                }

                                $company = Company::where('id', $request->company_id)->first();
                                if (auth()->user()->user_type == 'scheme_manager' && $request->status == 'rejected') {
                                    $certificationScope = $company->scope_to_certify;
                                } else {
                                    $certificationScope = $request->certificate_scope;
                                    $company->scope_to_certify = $request->certificate_scope;
                                    $company->save();
                                }
                                if ($request->status == 'approved_scheme' && $approvalDate != null && $standard_stage->approval_date != $approvalDate) {
                                    $lastApprovalDate = $standard_stage->approval_date;
                                } else {
                                    $lastApprovalDate = $standard_stage->last_approval_date;
                                }

                                $standard_stage->update([

                                    'certificate_scope' => $certificationScope,
                                    'aj_date' => $aj_date->format('Y-m-d'),
                                    'manday_remarks' => $request->manday_remarks,
                                    'excepted_date' => $expectedDate,
                                    'actual_expected_date' => $actual_expected_date,
                                    'valid_till_date' => $valid_till_date,
                                    'supervisor_involved' => $request->supervisor_involved['stage1'],
                                    'evaluator_name' => $request->evaluator_name,
                                    'approval_date' => $approvalDate,
                                    'last_approval_date' => $lastApprovalDate,
                                    'status' => $status,
                                    'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                                    'job_number' => $request->job_number,


                                ]);
                                if ($request['audit_type'] === 'surveillance_1' || $request['audit_type'] === 'surveillance_2' || $request['audit_type'] === 'surveillance_3' || $request['audit_type'] === 'surveillance_4' || $request['audit_type'] === 'surveillance_5' || $request['audit_type'] === 'reaudit') {

                                    if (!is_null($request->std_number0) && $key == 0) {

                                        $standard_stage->ajStageChange()->updateOrCreate([
                                            'name' => $request->ch_name,
                                            'scope' => $request->ch_scp,
                                            'location' => $request->ch_loc,
                                            'version' => $request->ch_std,
                                            'standards' => $request->std_number0,
                                            'remarks' => $request->remarks,
                                        ]);
                                    }
                                    if (!is_null($request->std_number1) && $key == 1) {
                                        $standard_stage->ajStageChange()->updateOrCreate([
                                            'name' => $request->ch_name,
                                            'scope' => $request->ch_scp,
                                            'location' => $request->ch_loc,
                                            'version' => $request->ch_std,
                                            'standards' => $request->std_number1,
                                            'remarks' => $request->remarks,
                                        ]);
                                    }
                                    if ((count($activeImsStandards) > 2) && !is_null($request->std_number2) && $key == 2) {
                                        $standard_stage->ajStageChange()->updateOrCreate([
                                            'name' => $request->ch_name,
                                            'scope' => $request->ch_scp,
                                            'location' => $request->ch_loc,
                                            'version' => $request->ch_std,
                                            'standards' => $request->std_number2,
                                            'remarks' => $request->remarks,
                                        ]);
                                    }

                                    if (is_null($request->std_number0) && is_null($request->std_number1) && is_null($request->std_number2)) {
                                        $standard_stage->ajStageChange()->delete();
                                        $standard_stage->ajStageChange()->updateOrCreate([
                                            'name' => $request->ch_name,
                                            'scope' => $request->ch_scp,
                                            'location' => $request->ch_loc,
                                            'version' => $request->ch_std,
                                            'standards' => $request->std_number,
                                            'remarks' => $request->remarks,
                                        ]);
                                    }

                                }
                                foreach ($standard_stage->ajStageTeams as $ajStageTeam) {
                                    $ajStageTeam->auditor->update([
                                        'is_available' => 1
                                    ]);
                                }

                                $standard_stage->ajStageTeams()->delete();
                                if (!empty($request->perposedTeam)) {

                                    $supervisor = 0;
                                    if ($request['supervisor_involved']['stage1'] == "yes") {
                                        $supervisor = 1;
                                    }
                                    foreach ($request->perposedTeam as $key => $a_member) {
                                        $standard_stage->ajStageTeams()->create([
                                            'auditor_id' => (int)$a_member,
                                            'supervisor_involved' => $supervisor,
                                            'grade_id' => (int)$request->perposed_team_grade_id[$key],
                                            'member_name' => $request->perposed_team_name[$key],
                                            'member_type' => $request->perposed_team_type[$key],
                                        ]);
                                        $auditor = Auditor::find((int)$a_member)->update([
                                            'is_available' => 0
                                        ]);
                                    }

                                }
                                if (!is_null($request->recycle)) {
                                    $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->where('recycle', (int)$request->recycle)->get();
                                } else {
                                    $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->whereNull('recycle')->get();

                                }
                                $frequency = $request->frequency;
                                if ($request->expected_date[0] && $request->audit_type == "stage_2" && $request->status == 'approved') {

                                    if ($frequency === 'biannual') {
                                        $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($request->expected_date[0])));
                                        $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                        $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                        $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                        $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                        $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
                                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];

                                    } else {

                                        $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($request->expected_date[0])));
                                        $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($request->expected_date[0])));
                                        $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($request->expected_date[0])));
                                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
                                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];

                                    }


                                    foreach ($aj_audit_types as $key => $aj_audit_type) {
                                        foreach ($next_stages as $stage) {
                                            if ($stage->audit_type == $aj_audit_type) {
                                                $standard_stage1 = AJStandardStage::findOrFail((int)$stage->id);
                                                $standard_stage1->update([
                                                    'excepted_date' => $due_date[$key],
                                                ]);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                $standard_stage = AJStandardStage::findOrFail((int)$request->standard_stage_id);

                $data = [
                    'aj_id' => $standard_stage,
                    'status' => 'success',
                ];
                $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated');
                return $response;
            } else {
                $data = [
                    'flash_status' => 'warning',
                    'flash_message' => 'Team not available for this standard'
                ];
                $response = $data;
                return $response;
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }


    public function show($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $audit_type = str_replace(' ', '_', strtolower($audit_type));

        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);

        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        }
        if (is_null($role)) {
            $role = auth()->user()->user_type;
        }
        //get all users of the required role


        if ($ims != null) {
            //for ims
        } else {
            $ims = 0;
            $standard_number = Standards::where('name', $standard_number)->first();


            $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
            $company_standards = [];
            $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
            $certificateStatus = Certificate::where('company_standard_id', $c_std->id)->orderBy('id', 'desc')->first();

            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];

            if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) {

                if (!empty($company->companyStandards)) {

                    foreach ($company->companyStandards as $standard) {
                        $company_standards[] = $standard->standard_id;
                    }
                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->groupBy('auditor_id')->get();
                    if (!empty($auditor_standards)) {
                        foreach ($auditor_standards as $auditor_standard) {
                            foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {
                                $members[] = $auditor_standard;
                                $standard_grades[] = $standard_grade;
                            }
                            foreach ($auditor_standard->auditorStandardCodes()->where('status', '!=', 'full')->get() as $standard_code) {

                                $standard_codes[] = $standard_code;

                            }
                        }
                        //checking the stage1 and stage2 combine aj

                        if ($audit_type == 'stage_1|stage_2') {

                            $combine_tracker = 1;
                        } else {

                            $combine_tracker = 0;
                        }

                        //check the company already have any aj
                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                        $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                        $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();


                        if (!empty($aj_obj)) {
                            //check which standards are in company aj
                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                ->where('standard_id', $standard_number->id)
                                ->first();
                            $team_members_stage1 = [];
                            if ($audit_type == 'stage_2') {
                                $aj_standard_stages_obj_stage1 = AJStandardStage::where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standards_obj->id)->first();
                                $team_members_stage1 = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stages_obj_stage1->id)->get();
                            }

                            if (!empty($aj_standards_obj)) {
                                if ($combine_tracker == 1) {
                                    //get the standard stage stage_1
                                    $aj_standard_stages_obj = AJStandardStage::where('audit_type', 'stage_1')->where('id', $aj_standard_id)->first();
                                } else {
                                    $aj_standard_stages_obj = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                }

                                if (!empty($aj_standard_stages_obj)) {
                                    //now we want the single audit stage which want to edit
                                    $aj_standard_stage = $aj_standard_stages_obj;
                                    //also need all other audit stages for manday editing


//                                    if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
//
//                                        $mandays = $aj_standards_obj->ajStandardStages;
//                                    } else {
//                                        $mandays = AJStandardStage::where('id', $aj_standard_id)->where('audit_type', $audit_type)->first();
//                                    }
//
//                                    $aj_stages_manday[] = $mandays;


                                    if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {

                                        if ($audit_type == 'stage_2') {
                                            $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                        } else {
                                            $mandays_stage_1 = [];
                                            $mandays = AJStandardStage::where('id', $aj_standard_id)->get();
                                        }

                                    } else {
                                        if ($audit_type == 'stage_2') {
                                            $mandays_stage_1 = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', 'stage_1')->get();
                                            $mandays = AJStandardStage::where('aj_standard_id', (int)$aj_standard_stages_obj->aj_standard_id)->where('audit_type', $audit_type)->first();
                                        } else {
                                            $mandays_stage_1 = [];
                                            $mandays = AJStandardStage::where('id', (int)$aj_standard_id)->where('audit_type', $audit_type)->first();
                                        }

                                    }

                                    $aj_stages_manday[] = $mandays;


                                    $aj_remarks = AJRemarks::where('aj_standard_stage_id', $aj_standard_id)->get();

                                    $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();

                                    $checkApproved = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                    if ($checkApproved->status == 'approved') {
                                        $approvalPerson = Notification::where('request_id', (int)$aj_standard_id)->where('status', 'approved')->where('type', 'audit_justification')->first();
                                        $defaultSchemeManager = SchemeManager::whereHas('standards', function ($q) use ($standard_number) {
                                            $q->where('standard_id', $standard_number->id);
                                        })->with('user')->first();
                                        $pdf = \Barryvdh\DomPDF\Facade::loadView('audit-justification.AjPrintIMSPreview.index', compact('aj_standard_change', 'certificateStatus', 'defaultSchemeManager', 'mandays_stage_1', 'approvalPerson', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');
                                        Storage::put('public/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf', $pdf->output());
                                        $href = 'storage/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf';

                                    } else {
                                        $href = '#';
                                    }

                                    //return to the edit page
                                    return view('audit-justification.ims.show', compact('certificateStatus', 'href', 'notifications', 'aj_standard_change', 'standard_number', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                }
                            }
                        }

                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Auditor standard not available');
                    }
                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Company does not have this standard');
                }
            }
        }

    }

    public function partialRemarks(Request $request)
    {

        $partial_remarks = AuditorStandardCode::where('auditor_standard_id', $request->auditor_standard_id)->where('status', 'partial')->with('iaf')->get();
        $data = [
            'status' => 'success',
            'partial_remarks' => $partial_remarks
        ];

        $response = (new ApiMessageController())->successResponse($data, '');
        return $response;
    }

    public function deleteIMSAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {


        $standard_stage = AJStandardStage::find((int)$aj_standard_id);


        $standard_stage->update([
            'certificate_scope' => NULL,
            'aj_date' => NULL,
            'manday_remarks' => NULL,
            'supervisor_involved' => 'no',
            'evaluator_name' => NULL,
            'approval_date' => NULL,
            'status' => 'Not Applied',
            'user_id' => NULL,

        ]);


        $notifications = Notification::where('request_id', $standard_stage->id)->get();
        if (!empty($notifications) && count($notifications) > 0) {
            foreach ($notifications as $notification) {
                $notification->delete();
            }
        }
//        $standard_stage->delete();
        $standard_stage->ajStageChange()->delete();
        $standard_stage->AJRemarks()->delete();
        $standard_stage->ajStageTeams()->delete();
        return back()->with(['flash_status' => 'success', 'flash_message' => 'AJ has been deleted successfully']);
    }


    public function addRemarks(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'remark' => 'required|string',
            'aj_id.*' => 'required'
        ]);


        if (!$validator->fails()) {
            $get_company = Company::where('id', $request->company_id)->first();
            foreach ($request['aj_id'] as $aj_id) {

                $aj = AJStandardStage::with('ajStandard.standard.scheme_manager.user')->where('id', $aj_id)->first();
                $aj->status = $request->status;
                $this->updateJobNumber($request, $aj);
                $aj->save();
                $aj->AJRemarks()->create([
                    'model' => AJStandardStage::class,
                    'user_id' => Auth()->user()->id,
                    'remarks' => $request['remark']
                ]);
                $sentMessage = '';
                $companyStandard = CompanyStandards::where('company_id', $request->company_id)->where('standard_id', $aj->ajStandard->standard->id)->first();
                $imsCompanyStandards = $get_company->companyStandards()->where('is_ims', true)->get();
                $ims_heading = $companyStandard->standard->name;
                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                        if ($companyStandard->id == $imsCompanyStandard->id) {
                            continue;
                        } else {
                            $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                        }
                    }
                    if ($request->status == 'applied') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(New)';
                    } elseif ($request->status == 'approved') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Approved)';
                    } elseif ($request->status == 'resent') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Re-Submitted)';
                    } elseif ($request->status == 'rejected') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Rejected)';
                    }

                } else {
                    if ($request->status == 'applied') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(New)';
                    } elseif ($request->status == 'approved') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Approved)';
                    } elseif ($request->status == 'resent') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Re-Submitted)';
                    } elseif ($request->status == 'rejected') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Rejected)';
                    }

                }


                if (auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'scheme_coordinator') {
                    if ($request->status == 'applied') {
                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'unapproved',
                            'sent_message' => $sentMessage

                        ]);
                    } elseif ($request->status == 'resent') {
                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'resent',
                            'sent_message' => $sentMessage

                        ]);
                        if (!is_null($notification)) {
                            $notification->is_read = true;
                            $notification->save();
                        }
                    }


                } else if (auth()->user()->user_type == 'scheme_manager') {
                    if ($request->status == 'rejected') {

                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $notification->sent_by,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'rejected',
                            'sent_message' => $sentMessage

                        ]);
                        $notification->is_read = true;
                        $notification->save();
                    } elseif ($request->status == 'approved') {

                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $notification->sent_by,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'approved',
                            'sent_message' => $sentMessage

                        ]);
                        $notification->is_read = true;
                        $notification->save();
                    }


                }

            }

            $data = [
                'status' => 'success'
            ];

            $response = (new ApiMessageController())->successResponse($data, 'AJ has been created and notification has been sent.');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function updateRemarks(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'remark' => 'required|string',
            'aj_id.*' => 'required'
        ]);


        if (!$validator->fails()) {
            $get_company = Company::where('id', $request->company_id)->first();
            foreach ($request['aj_id'] as $aj_id) {

                $aj = AJStandardStage::with('ajStandard.standard.scheme_manager.user')->where('id', $aj_id)->first();
                $aj->status = $request->status;
                $this->updateJobNumber($request, $aj);
                $aj->save();
                $aj->AJRemarks()->create([
                    'model' => AJStandardStage::class,
                    'user_id' => Auth()->user()->id,
                    'remarks' => $request['remark']
                ]);
                $sentMessage = '';
                $companyStandard = CompanyStandards::where('company_id', $request->company_id)->where('standard_id', $aj->ajStandard->standard->id)->first();
                $imsCompanyStandards = $get_company->companyStandards()->where('is_ims', true)->get();
                $ims_heading = $companyStandard->standard->name;
                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                        if ($companyStandard->id == $imsCompanyStandard->id) {
                            continue;
                        } else {
                            $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                        }
                    }
                    if ($request->status == 'applied') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(New)';
                    } elseif ($request->status == 'approved') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Approved)';
                    } elseif ($request->status == 'resent') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Re-Submitted)';
                    } elseif ($request->status == 'rejected') {
                        $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Rejected)';
                    }

                } else {
                    if ($request->status == 'applied') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(New)';
                    } elseif ($request->status == 'approved') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Approved)';
                    } elseif ($request->status == 'resent') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Re-Submitted)';
                    } elseif ($request->status == 'rejected') {
                        $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Rejected)';
                    }

                }


                if (auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'scheme_coordinator') {
                    if ($request->status == 'applied') {
                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj_id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'unapproved',
                            'sent_message' => $sentMessage

                        ]);
                    } elseif ($request->status == 'resent') {
                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj_id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'resent',
                            'sent_message' => $sentMessage

                        ]);
                        if (!is_null($notification)) {
                            $notification->is_read = true;
                            $notification->save();
                        }
                    }


                } else if (auth()->user()->user_type == 'scheme_manager') {
                    if ($request->status == 'rejected') {

                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $notification->sent_by,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj_id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'rejected',
                            'sent_message' => $sentMessage

                        ]);
                        $notification->is_read = true;
                        $notification->save();
                    } elseif ($request->status == 'approved') {

                        $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                        Notification::create([
                            'type' => 'audit_justification',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $notification->sent_by,
                            'icon' => 'message',
                            'body' => $request['remark'],
                            'url' => ' /edit/ims-audit-justification/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj_id,
                            'is_read' => 0,
                            'type_id' => $aj->ajStandard->standard->id,
                            'request_id' => $aj->id,
                            'status' => 'approved',
                            'sent_message' => $sentMessage

                        ]);
                        $notification->is_read = true;
                        $notification->save();
                    }


                }

            }


            $data = [
                'status' => 'success'
            ];

            $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated and notification has been sent . ');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors . ');
        }

        return $response;

    }

    public function addChangeVersionComapnyStandard(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        foreach ($request->version_data as $versionIds) {
            if (!is_null($versionIds['standard_id'])) {
                $company_standard = CompanyStandards::where('standard_id', $versionIds['standard_id'])->where('company_id', $versionIds['company_id'])->first();
                if (isset($company_standard) && !is_null($company_standard)) {
                    $response['status'] = false;
                    $response['message'] = "Your Standard Already used in company";
                } else {
                    $company_old_standard = CompanyStandards::where('standard_id', $versionIds['old_standard_id'])->where('company_id', $versionIds['company_id'])->first();

                    $company_standard = new CompanyStandards();
                    $company_standard->company_id = $versionIds['company_id'];
                    $company_standard->standard_id = $versionIds['standard_id'];
                    $company_standard->client_type = $company_old_standard->client_type;
                    if ($company_old_standard->client_type == 'transfered') {
                        $company_standard->old_cb_name = $company_old_standard->old_cb_name;
                        $company_standard->old_cb_certificate_issue_date = $company_old_standard->old_cb_certificate_issue_date;
                        $company_standard->old_cb_certificate_expiry_date = $company_old_standard->old_cb_certificate_expiry_date;
                        $company_standard->old_cb_surveillance_frequency = $company_old_standard->old_cb_surveillance_frequency;
                        $company_standard->old_cb_audit_activity_stage_id = $company_old_standard->old_cb_audit_activity_stage_id;
                        $company_standard->old_cb_audit_activity_dates = $company_old_standard->old_cb_audit_activity_dates;
                        $company_standard->old_cb_last_audit_report_doc_media_id = $company_old_standard->old_cb_last_audit_report_doc_media_id;
                        $company_standard->old_cb_certificate_copy_doc_media_id = $company_old_standard->old_cb_certificate_copy_doc_media_id;
                        $company_standard->old_cb_others_doc_media_id = $company_old_standard->old_cb_others_doc_media_id;
                    }
                    $company_standard->proposed_complexity = $company_old_standard->proposed_complexity;
                    $company_standard->general_remarks = $company_old_standard->general_remarks;
                    $company_standard->is_ims = $company_old_standard->is_ims === true ? true : false;
                    $company_standard->audit_activity_stage_id = $company_old_standard->audit_activity_stage_id;
                    $company_standard->initial_certification_currency = $company_old_standard->initial_certification_currency;//$request->initial_certification_currency;
                    $company_standard->initial_certification_amount = $company_old_standard->initial_certification_amount;
                    $company_standard->surveillance_currency = $company_old_standard->surveillance_currency;//$request->surveillance_currency;
                    $company_standard->surveillance_amount = $company_old_standard->surveillance_amount;
                    $company_standard->re_audit_currency = $company_old_standard->re_audit_currency;//$request->re_audit_currency;
                    $company_standard->re_audit_amount = $company_old_standard->re_audit_amount ?? '';
                    $company_standard->surveillance_frequency = $company_old_standard->surveillance_frequency;
                    $company_standard->status = 'pending';
                    $company_standard->save();

                    $this->checkStandardCodes($company_old_standard->id, $company_standard->id, $versionIds['company_id'], $company_standard->standard_id, $company_standard->standard->standards_family_id);


                    $standardQuestions = StandardsQuestions::where('standard_id', $versionIds['old_standard_id'])->get();
                    if (!empty($standardQuestions) && count($standardQuestions) > 0) {
                        foreach ($standardQuestions as $questions) {
                            $standard_questions = new StandardsQuestions();
                            $standard_questions->standard_id = $company_standard->standard_id;
                            $standard_questions->question = $questions->question;
                            $standard_questions->field_type = $questions->field_type;
                            $standard_questions->default_answer = $questions->default_answer;
                            $standard_questions->options = $questions->options;
                            $standard_questions->min_length = $questions->min_length;
                            $standard_questions->max_length = $questions->max_length;
                            $standard_questions->is_required = $questions->is_required;
                            $standard_questions->has_dependent_fields = $questions->has_dependent_fields;
                            $standard_questions->right_answer_for_dependent_fields = $questions->right_answer_for_dependent_fields;
                            $standard_questions->dependent_fields_ids = $questions->dependent_fields_ids;
                            $standard_questions->is_dependent_field = $questions->is_dependent_field;
                            $standard_questions->sort = $questions->sort;
                            $standard_questions->question_format = $questions->question_format;
                            $standard_questions->company_standards_question_id = $questions->company_standards_question_id;
                            $standard_questions->save();


                            $comapnyStandardQuestions = CompanyStandardsAnswers::where('standard_id', $versionIds['old_standard_id'])->where('standard_id', $company_old_standard->id)->get();
                            if (!empty($comapnyStandardQuestions) && count($comapnyStandardQuestions) > 0) {
                                foreach ($comapnyStandardQuestions as $comapnyStandardQuestion) {

                                    if ($comapnyStandardQuestion->id == $questions->id) {
                                        $company_standard_answer = new CompanyStandardsAnswers;
                                        $company_standard_answer->standard_id = $company_standard->standard_id;
                                        $company_standard_answer->standard_question_id = $standard_questions->id;
                                        $company_standard_answer->company_standards_id = $company_standard->id;
                                        $company_standard_answer->answer = $comapnyStandardQuestion->answer;
                                        $company_standard_answer->save();
                                    }


                                }
                            }


                        }
                        $dependentIdDiscussion = StandardsQuestions::where('question', 'Description')->where('standard_id', $company_standard->standard_id)->first();
                        if (!is_null($dependentIdDiscussion)) {
                            $excluisonData = StandardsQuestions::where('question', 'Exclusion')->where('standard_id', $company_standard->standard_id)->first();
                            $excluisonData->dependent_fields_ids = $dependentIdDiscussion->id;
                            $excluisonData->save();
                        }

                    }

                    $response['status'] = true;
                    $response['message'] = "New standard Added Sucessfully.";

                }
            }
        }
        return $response;
    }


    private function checkStandardCodes($company_old_standard, $company_standard_id, $company_id, $standard_id, $standards_family_id)
    {
        //Generic Company Standard
        $checkCodes = CompanyStandardCode::where([
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id
        ])->whereNull('deleted_at')->get();
        if (count($checkCodes) === 0) {
            $companyStandardOldCodes = CompanyStandardCode::where([
                'company_id' => $company_id,
                'company_standard_id' => $company_old_standard
            ])->whereNull('deleted_at')->get();
            if (!empty($companyStandardOldCodes) && count($companyStandardOldCodes) > 0) {
                foreach ($companyStandardOldCodes as $companyStandardOldCode) {
                    $codes = new CompanyStandardCode();
                    $codes->company_id = $companyStandardOldCode->company_id;
                    $codes->company_standard_id = $company_standard_id;
                    $codes->standard_id = $standard_id;
                    $codes->iaf_id = $companyStandardOldCode->iaf_id;
                    $codes->ias_id = $companyStandardOldCode->ias_id;
                    $codes->accreditation_id = $companyStandardOldCode->accreditation_id;
                    $codes->unique_code = $companyStandardOldCode->unique_code;
                    $codes->save();
                }
            }
        }


        //food company Standard
        $checkFoodCodes = CompanyStandardFoodCode::where([
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id
        ])->get();
        if (count($checkFoodCodes) === 0) {
            if ($standards_family_id == 23) {
                $companyStandardOldFoodCodes = CompanyStandardFoodCode::where([
                    'company_id' => $company_id,
                    'company_standard_id' => $company_old_standard
                ])->get();
                if (!empty($companyStandardOldFoodCodes) && count($companyStandardOldFoodCodes) > 0) {
                    foreach ($companyStandardOldFoodCodes as $companyStandardOldFoodCode) {
                        $codes = new CompanyStandardFoodCode();
                        $codes->company_id = $companyStandardOldFoodCode->company_id;
                        $codes->company_standard_id = $company_standard_id;
                        $codes->standard_id = $standard_id;
                        $codes->food_category_id = $companyStandardOldFoodCode->food_category_id;
                        $codes->food_sub_category_id = $companyStandardOldFoodCode->food_sub_category_id;
                        $codes->accreditation_id = $companyStandardOldFoodCode->accreditation_id;
                        $codes->unique_code = $companyStandardOldFoodCode->unique_code;
                        $codes->save();
                    }
                }
            }
        }

        //energy company Standard
        $checkEnergyCodes = CompanyStandardEnergyCode::where([
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id
        ])->get();
        if (count($checkEnergyCodes) === 0) {
            if ($standards_family_id == 20) {
                $companyStandardOldEnergyCodes = CompanyStandardEnergyCode::where([
                    'company_id' => $company_id,
                    'company_standard_id' => $company_old_standard
                ])->get();
                if (!empty($companyStandardOldEnergyCodes) && count($companyStandardOldEnergyCodes) > 0) {
                    foreach ($companyStandardOldEnergyCodes as $companyStandardOldEnergyCode) {
                        $codes = new CompanyStandardEnergyCode();
                        $codes->company_id = $companyStandardOldEnergyCode->company_id;
                        $codes->company_standard_id = $company_standard_id;
                        $codes->standard_id = $standard_id;
                        $codes->energy_id = $companyStandardOldEnergyCode->energy_id;
                        $codes->accreditation_id = $companyStandardOldEnergyCode->accreditation_id;
                        $codes->unique_code = $companyStandardOldEnergyCode->unique_code;
                        $codes->save();
                    }
                }
            }
        }

//
//                    $newStandardAccreditation = Accreditation::where('standard_id', $company_standard->standard_id)->get();
//                    if ($newStandardAccreditation->count() > 0) {
//                        foreach ($newStandardAccreditation as $accreditation) {
//                            $newStandardIafCode = IAF::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $accreditation->id)->get();
//                            if ($newStandardIafCode->count() > 0) {
//                                foreach ($newStandardIafCode as $iaf) {
//                                    $newStandardIasCode = IAS::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $accreditation->id)->where('iaf_id', $iaf->id)->get();
//                                    if ($newStandardIasCode->count() > 0) {
//                                        foreach ($newStandardIasCode as $ias) {
//                                            $codes = new CompanyStandardCode();
//                                            $codes->company_id = $company_standard->company_id;
//                                            $codes->company_standard_id = $company_standard->id;
//                                            $codes->standard_id = $company_standard->standard_id;
//                                            $codes->iaf_id = $iaf->id;
//                                            $codes->ias_id = $ias->id;
//                                            $codes->accreditation_id = $accreditation->id;
//                                            $codes->unique_code = $accreditation->name . "/" . $iaf->code . "/" . $ias->code;
//                                            $codes->save();
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    if ($company_standard->standard->standards_family_id == 23) {
//                        //food company Standard
//
//                        $newStandardFoodAccreditation = Accreditation::where('standard_id', $company_standard->standard_id)->get();
//                        if ($newStandardFoodAccreditation->count() > 0) {
//                            foreach ($newStandardFoodAccreditation as $food_accreditation) {
//                                $newStandardFoodCategory = FoodCategory::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $food_accreditation->id)->get();
//                                if ($newStandardFoodCategory->count() > 0) {
//                                    foreach ($newStandardFoodCategory as $food_category) {
//                                        $newStandardFoodSubCategory = FoodSubCategory::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $food_accreditation->id)->where('food_category_id', $food_category->id)->get();
//                                        if ($newStandardFoodSubCategory->count() > 0) {
//                                            foreach ($newStandardFoodSubCategory as $food_sub_category) {
//                                                $codes = new CompanyStandardFoodCode();
//                                                $codes->company_id = $company_standard->company_id;
//                                                $codes->company_standard_id = $company_standard->id;
//                                                $codes->standard_id = $company_standard->standard_id;
//                                                $codes->food_category_id = $food_category->id;
//                                                $codes->food_sub_category_id = $food_sub_category->id;
//                                                $codes->accreditation_id = $food_accreditation->id;
//                                                $codes->unique_code = $food_accreditation->name . "/" . $food_category->code . "/" . $food_sub_category->code;
//                                                $codes->save();
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    if ($company_standard->standard->standards_family_id == 20) {
//                        //energy Standard
//
//                        $newStandardEnergyAccreditation = Accreditation::where('standard_id', $company_standard->standard_id)->get();
//                        if ($newStandardEnergyAccreditation->count() > 0) {
//                            foreach ($newStandardEnergyAccreditation as $energy_accreditation) {
//                                $newStandardEnergyCategory = EnergyCode::where('standard_id', $company_standard->standard_id)->where('accreditation_id', $energy_accreditation->id)->get();
//                                if ($newStandardEnergyCategory->count() > 0) {
//                                    foreach ($newStandardEnergyCategory as $energy_category) {
//
//                                        $codes = new CompanyStandardEnergyCode();
//                                        $codes->company_id = $company_standard->company_id;
//                                        $codes->company_standard_id = $company_standard->id;
//                                        $codes->standard_id = $company_standard->standard_id;
//                                        $codes->energy_id = $energy_category->id;
//                                        $codes->accreditation_id = $energy_accreditation->id;
//                                        $codes->unique_code = $energy_accreditation->name . "/" . $energy_category->code;
//                                        $codes->save();
//                                    }
//                                }
//                            }
//                        }
//                    }

    }

    private function in_array_any($needles, $haystack)
    {
        return (bool)array_intersect($needles, $haystack);
        // echo in_array_any( array(3,9), array(5,8,3,1,2) ); // true, since 3 is present
        // echo in_array_any( array(4,9), array(5,8,3,1,2) ); // false, neither 4 nor 9 is present
    }

    private function checkTeam($request)
    {
        if (!empty($request->perposed_team_grade_id) && count($request->perposed_team_grade_id) > 0) {
            $numArray = array_map('intval', $request->perposed_team_grade_id);
            if (in_array(1, $numArray) && in_array(7, $numArray)) {
                //both of them are in $arg

                $standardFamilyCheck = Standards::where('id', $request->standard_number)->first();
                if ($standardFamilyCheck->standards_family_id == 20) {
                    //Energy
                    $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardEnergyCodes = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardEnergyCodes, $companyStandardEnergyCodes)) {
                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } elseif ($standardFamilyCheck->standards_family_id == 23) {
                    //Food
                    $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardFoodCodes = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardFoodCodes, $companyStandardFoodCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } else {
                    //Generic
                    $companyStandardCodes = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardCodes = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardCodes, $companyStandardCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];

                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }

                }
            } else {
                $data = [
                    'status' => 'warning',
                    'message' => 'For Audit LA and TE is required.'
                ];

                $response = $data;
                return $response;
            }


        } else {
            $data = [
                'status' => 'warning',
                'message' => 'Please Select Team Members for Audit.'
            ];

            $response = $data;
            return $response;
        }
    }

    private function checkTeamForMultiple($request)
    {
        if (strpos($request['audit_type'], '|') !== false) {
            $stages = explode('|', $request->audit_type);
        } else {
            $stages = [$request['audit_type']];
        }


        if (isset($request->perposedTeam1) && !empty($request->perposedTeam1) && count($request->perposedTeam1) == 2) {
            foreach ($stages as $stage) {

                if (!empty($request->perposed_team_grade_id[str_replace('_', '', $stage)]) && count($request->perposed_team_grade_id[str_replace('_', '', $stage)]) > 0) {
                    $numArray = array_map('intval', $request->perposed_team_grade_id[str_replace('_', '', $stage)]);
                    if (in_array(1, $numArray) && in_array(7, $numArray)) {
                        //both of them are in $arg

                        $standardFamilyCheck = Standards::where('id', $request->standard_number)->first();
                        if ($standardFamilyCheck->standards_family_id == 20) {
                            //Energy
                            $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                            $auditorsArray = array_map('intval', $request->perposedTeam1[str_replace('_', '', $stage)]);
                            $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                                $q->where('grade_id', 7);
                            })->whereNull('deleted_at')->pluck('id')->toArray();
                            $auditorStandardEnergyCodes = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            if ($this->in_array_any($auditorStandardEnergyCodes, $companyStandardEnergyCodes)) {
                                $data = [
                                    'status' => 'success',
                                    'message' => 'Team Member Found for ' . str_replace('_', '', $stage)
                                ];
                                $response = $data;
                                return $response;
                            } else {
                                $data = [
                                    'status' => 'warning',
                                    'message' => 'TE does not have all Company Codes for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;

                            }
                        } elseif ($standardFamilyCheck->standards_family_id == 23) {
                            //Food
                            $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                            $auditorsArray = array_map('intval', $request->perposedTeam1[str_replace('_', '', $stage)]);
                            $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                                $q->where('grade_id', 7);
                            })->whereNull('deleted_at')->pluck('id')->toArray();
                            $auditorStandardFoodCodes = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            if ($this->in_array_any($auditorStandardFoodCodes, $companyStandardFoodCodes)) {

                                $data = [
                                    'status' => 'success',
                                    'message' => 'Team Member Found for ' . str_replace('_', '', $stage)
                                ];
                                $response = $data;
                                return $response;
                            } else {
                                $data = [
                                    'status' => 'warning',
                                    'message' => 'TE does not have all Company Codes for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;

                            }
                        } else {
                            //Generic
//                            dd($request->perposedTeam1[$stage]);
                            $companyStandardCodes = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            $auditorsArray = array_map('intval', $request->perposedTeam1[str_replace('_', '', $stage)]);
                            $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                            $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                                $q->where('grade_id', 7);
                            })->whereNull('deleted_at')->pluck('id')->toArray();
                            $auditorStandardCodes = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                            if ($this->in_array_any($auditorStandardCodes, $companyStandardCodes)) {

                                $data = [
                                    'status' => 'success',
                                    'message' => 'Team Member Found for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;
                            } else {
                                $data = [
                                    'status' => 'warning',
                                    'message' => 'TE does not have all Company Codes for ' . str_replace('_', '', $stage)
                                ];

                                $response = $data;
                                return $response;

                            }

                        }
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'For Audit LA and TE is required for ' . str_replace('_', '', $stage)
                        ];

                        $response = $data;
                        return $response;
                    }


                } else {
                    $data = [
                        'status' => 'warning',
                        'message' => 'Please Select Team Members for Audit for ' . str_replace('_', '', $stage)
                    ];

                    $response = $data;
                    return $response;
                }


            }
        }
    }

    private function updateJobNumber($request, $stage)
    {
        if ($request->status == 'approved' && $stage->audit_type == 'stage_1') {

            $auditJustification = AuditJustification::where('id', $stage->ajStandard->audit_justification_id)->first();
            $company = Company::where('id', $auditJustification->company_id)->with('country')->first(['id', 'country_id']);
            AJStandardStage::where('aj_standard_id', $stage->aj_standard_id)->update([
                'job_number' => $this->jobNumber($company->country->iso)
            ]);
        }
    }

    private function updateMandays($audit_type, $standard_number, $c_std, $company, $checkStandardStage)
    {
        if (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Food') {
        } elseif (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Energy Management') {
        } else {

            $companyStandardIds = explode(',', $c_std->ims_standard_ids);
            $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->pluck('standard_id')->toArray();
            $activeImsComplexity = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->pluck('proposed_complexity')->toArray();
            $manday = $this->generateIMSTable($activeImsStandards, $activeImsComplexity, $company->effective_employees, $c_std->surveillance_frequency);

        }

        $calc_manday = [];

        $calc_manday = $manday;
        if ($c_std->surveillance_frequency === 'biannual') {
            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];

        } else {
            $aj_audit_types = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'reaudit'];
        }
        foreach ($aj_audit_types as $aj_audit_type) {
            AJStandardStage::where(['audit_type' => $aj_audit_type, 'aj_standard_id' => $checkStandardStage->aj_standard_id])->update([
                'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
            ]);
        }

    }

}
