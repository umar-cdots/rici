<?php

namespace App\Http\Controllers;

use App\Cities;
use App\Countries;
use App\Regions;
use App\ResidentCity;
use App\ResidentCountry;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\User;
use App\Zone;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use libphonenumber\CountryCodeToRegionCodeMap;
use PHPUnit\Framework\Constraint\Count;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Traits\GeneralHelperTrait;
use App\Traits\AccessRights;


class SchemeManagerController
{
    use GeneralHelperTrait;
    use AccessRights;

    public function index()
    {

        if (auth()->user()->user_type == 'admin') {
            $scheme_managers = User::where('user_type', 'scheme_manager')->get();
        } elseif (auth()->user()->user_type == 'scheme_manager') {
            $scheme_managers = User::where('user_type', 'scheme_manager')->where('id', auth()->user()->id)->get();
        }
        return view('scheme-manager.index', compact('scheme_managers'));
    }

    protected function alreadyTakenFamilies(): array
    {
        $already_taken = [];

        $scheme_manager_standards = SchemeManagerStandards::groupBy('standard_family_id')->get();
        foreach ($scheme_manager_standards as $scheme_manager_standard) {
            $already_taken[] = $scheme_manager_standard->standard_family_id;
        }

        return $already_taken;
    }

    public function create()
    {
        $already_taken_families = $this->alreadyTakenFamilies();
        if (!empty($already_taken_families)) {
            $standard_families = StandardsFamily::whereNotIn('id', $already_taken_families)->get();
        } else {
            $standard_families = StandardsFamily::all();
        }
        $standards = Standards::all();
//        $countries = ResidentCountry::all();
        $countries = Countries::all();
        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::where('role_id', '2')->get();

        $regions = Regions::all();
        return view('scheme-manager.create', compact('standards', 'countries', 'roles', 'permissions', 'regions', 'standard_families'));
    }

    public function store(Request $request)
    {

//        dd($request->standard_codes[$request->standard_id]);
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
            'standard_family_id.0' => 'required|integer|exists:standards_families,id',
            //'region_id' =>'required|integer',
//            'username' => 'required|string|unique:users',
            'username' => ['required', 'string',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('username', $request->username)->whereNull('deleted_at');
                })
            ],
            'email' => ['required', 'string', 'email',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('email', $request->email)->whereNull('deleted_at');
                })
            ],
//            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
            'remarks' => 'required|string',
            'signature_file' => 'required|mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,docx,xlsx',
            'roles.*' => 'required|integer',
            'permissions.*' => 'integer'
        ], [
            'standard_family_id.0.required' => "Standard Family is Required"
        ]);


        if (!$validator->fails()) {


            //uploading the images
            if ($request->has('signature_file')) {
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/scheme_managers/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $signature_file_with_time = $name . '_' . time();
                $destinationPath = public_path('/uploads/scheme_managers');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;
            } else {
                $signature_file = null;
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/scheme_managers/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_managers');
                $profile_image_time = $name . '_' . time();
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;
            } else {
                $profile_image = null;
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/scheme_managers/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_managers');
                $additional_files_with_time = $name . '_' . time();
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = null;
            }

            $user = User::create([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                //'region_id' => $request->region_id,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'scheme_manager'
            ]);
            //add record in scheme manager table
            $scheme_manager = new SchemeManager();
            $scheme_manager->user_id = $user->id;
            $scheme_manager->full_name = $request->fullname;
            $scheme_manager->joining_date = date("Y/m/d", strtotime($request->joining_date));
//            $scheme_manager->standard_id = $request->standard_id;
            $scheme_manager->landline = $request->landline;
            $scheme_manager->signature_image = $signature_file;
            $scheme_manager->profile_image = $profile_image;
            $scheme_manager->additional_files = $additional_files;
            $scheme_manager->save();

            foreach ($request->standard_family_id as $standard_family) {
                $standards = Standards::where('standards_family_id', $standard_family)->get();
                foreach ($standards as $standard) {
                    SchemeManagerStandards::create([
                        'scheme_manager_id' => $scheme_manager->id,
                        'standard_family_id' => $standard_family,
                        'standard_id' => $standard->id
                    ]);
                }
            }

//            //assigning roles and permissions to user
            $role = Role::where('id', 2)->first();
            $user->assignRole($role->name);
            foreach ($role->permissions as $permission) {
                $user->givePermissionTo($permission->name);
            }
            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Scheme Manager has been added successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $already_taken_families = $this->alreadyTakenFamilies();

        $scheme_manager_families = SchemeManagerStandards::where('scheme_manager_id', $user->scheme_manager->id)->groupBy('standard_family_id')->get();
        if (!empty($already_taken_families)) {

            $standard_families = StandardsFamily::whereNotIn('id', $already_taken_families)->get();
        } else {
            $standard_families = StandardsFamily::all();
        }
        $countries = Countries::all();
        $standards = Standards::all();

        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::whereIn('role_id', $user->roles->pluck('id')->toArray())->get();
        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }


//        $cities = ResidentCity::where('country_id', $user->country_id)->get();
        $regions = Regions::all();

        return view('scheme-manager.show', compact('user', 'countries', 'standards', 'roles', 'permissions', 'cities', 'regions', 'standard_families', 'scheme_manager_families'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $already_taken_families = $this->alreadyTakenFamilies();

        $scheme_manager_families = SchemeManagerStandards::where('scheme_manager_id', $user->scheme_manager->id)->groupBy('standard_family_id')->get();
        if (!empty($already_taken_families)) {

            $standard_families = StandardsFamily::whereNotIn('id', $already_taken_families)->get();
        } else {
            $standard_families = StandardsFamily::all();
        }
        $countries = Countries::all();
        $standards = Standards::all();
        $roles = Role::all();
        $permissions = Permission::where('role_id', '2')->get();

        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }


//        $cities = ResidentCity::where('country_id', $user->country_id)->get();
        $regions = Regions::all();

        return view('scheme-manager.edit', compact('user', 'countries', 'standards', 'roles', 'permissions', 'cities', 'regions', 'standard_families', 'scheme_manager_families'));
    }

    public function update(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
            'standard_family_id.0' => 'required|integer|exists:standards_families,id',
            'username' => 'required|string',
            'email' => 'required|string|email|max:255',
            'remarks' => 'required|string',
            'signature_file' => 'mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,docx,xlsx',
            'roles.*' => 'integer',
            'permissions.*' => 'integer'
        ], [
            'standard_family_id.0.required' => "Standard Family is Required"
        ]);
        if (!$validator->fails()) {
            $user = User::findOrFail($request->user_id);
            //uploading the images
            if ($request->has('signature_file')) {


                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/scheme_managers/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $signature_file_with_time = $name . '_' . time();
                $destinationPath = public_path('/uploads/scheme_managers');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;

            } else {
                $signature_file = $user->scheme_manager['signature_image'];
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/scheme_managers/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_managers');
                $profile_image_time = $name . '_' . time();
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;
            } else {
                $profile_image = $user->scheme_manager['profile_image'];
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/scheme_managers/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_managers');
                $additional_files_with_time = $name . '_' . time();
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = $user->scheme_manager['additional_files'];
            }

            if (isset($request->password) && !is_null($request->password)) {
                $password = Hash::make($request->password);
            } else {
                $password = $user->password;
            }

            $user->update([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'password' => $password,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'scheme_manager'
            ]);
            //add record in scheme manager table
            $user->scheme_manager->update([
                'full_name' => $request->fullname,
                'joining_date' => date("Y/m/d", strtotime($request->joining_date)),
                'landline' => $request->landline,
//               'standard_id' => $request->standard_id,
                'signature_image' => $signature_file,
                'profile_image' => $profile_image,
                'additional_files' => $additional_files
            ]);

            SchemeManagerStandards::where('scheme_manager_id', $user->scheme_manager->id)->delete();

            foreach ($request->standard_family_id as $standard_family) {
                $standards = Standards::where('standards_family_id', $standard_family)->get();
                foreach ($standards as $standard) {
                    SchemeManagerStandards::create([
                        'scheme_manager_id' => $user->scheme_manager->id,
                        'standard_family_id' => $standard_family,
                        'standard_id' => $standard->id
                    ]);
                }
            }

//            //assigning roles and permissions to user

            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Scheme Manager has been updated successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function destroy($id)
    {

        $user = User::findOrFail($id);

        if ($user->scheme_manager->scheme_coordinators->count() == 0) {
            $user->delete();
            $user->scheme_manager()->delete();
            SchemeManagerStandards::where('scheme_manager_id', $user->scheme_manager->id)->delete();

            return back()->with(['flash_status' => 'success', 'flash_message' => 'Scheme Manager has been deleted successfully']);

        }

        return back()->with(['flash_status' => 'success', 'flash_message' => 'Sorry the system can not able to delete this record.']);

    }

    public function getFamiliesStandards(Request $request)
    {
        $standards = Standards::whereIn('standards_family_id', $request->standard_families)->get();
        return response()->json(['status' => 'success', 'message' => 'Standards Loaded', 'standards' => $standards]);
    }
}
