<?php

namespace App\Http\Controllers;

use App\CompanyStandardDocument;
use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\AccessRights;
use Spatie\MediaLibrary\Models\Media;

class DocumentController extends Controller
{
    use AccessRights;
    public function index()
    {
        // $this->VerifyRights();
        $documentCategories = collect(['auditor', 'company', 'operations', 'all']);//Document::getEnumValues()->get('category');
        $documents = Document::orderBy('category', 'ASC')->get();
        $documentByCategory = $documents->groupBy(function ($item, $key) {
            return $item->category;
        });
        // if($this->redirectUrl){return redirect($this->redirectUrl);}
        return view('documents.index', compact('documentCategories', 'documents', 'documentByCategory'),  ['check_rights' => $this->check_employee_rights]);
    }

    public function uploadDocument(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file_name' => 'required',
            'document_file' => 'file|required',
            'category' => ['required', Rule::in(Document::getEnumValues()->get('category'))],

        ], [
            'document_file.file' => "Document Must be a file!",
        ]);

        if (!$validator->fails()) {

            $documentModel = new Document();
            $documentModel->file_name = $request->file_name;
            $documentModel->category = $request->category;

            if($request->has('document_file')){
                $image = $request->file('document_file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/documents');
                $imagePath = $destinationPath . "/" . $name;
                $docuementFile = time().'_'.$name;
                $image->move($destinationPath, $docuementFile);


                $documentModel->file = $docuementFile;
            }


            $documentModel->save();


            if ($documentModel)

                $response = (new ApiMessageController())->saveresponse('Document Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function removeMediaDocument(Request $request)
    {

        $media = Document::findOrFail($request->get('id'));
        $filename = public_path('/uploads/documents/') . $media->file;

        if (File::exists($filename)) {
            File::delete($filename);  // or unlink($filename);
        }
        $media->delete();
        $response['status'] = 'success';
        $response['message'] = "Document deleted Successfully.";
        return $response;

    }
}
