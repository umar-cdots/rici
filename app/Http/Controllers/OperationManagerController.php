<?php

namespace App\Http\Controllers;

use App\Cities;
use App\Countries;
use App\OperationManager;
use App\RegionalOperationCoordinatorCities;
use App\RegionalOperationManager;
use App\Regions;
use App\ResidentCity;
use App\ResidentCountry;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\User;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Traits\GeneralHelperTrait;
use App\Traits\AccessRights;

class OperationManagerController extends Controller
{
    use GeneralHelperTrait;

//    use AccessRights;
    public function index()
    {
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'scheme_manager') {
            $operation_managers = User::where('user_type', 'operation_manager')->get();

        } elseif (auth()->user()->user_type == 'operation_manager') {
            $operation_managers = User::where('user_type', 'operation_manager')->where('id', auth()->user()->id)->get();
        }

        return view('operation-manager.index', compact('operation_managers'));
    }

    protected function alreadyTakenRegion(): array
    {
        $already_taken = [];
        $regional_operation_managers = RegionalOperationManager::all();
        if (!empty($regional_operation_managers)) {
            foreach ($regional_operation_managers as $op) {
                $already_taken[] = $op->region_id;
            }
        }

        return $already_taken;
    }

    public function create()
    {
        $countries = Countries::all();
        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::where('role_id', '4')->get();

        $already_taken_regions = $this->alreadyTakenRegion();
        if (!empty($already_taken_regions)) {
            $regions = Regions::whereNotIn('id', $already_taken_regions)->get();
        } else {
            $regions = Regions::all();
        }


        return view('operation-manager.create', compact('countries', 'roles', 'permissions', 'regions'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
            'username' => ['required', 'string',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('username', $request->username)->whereNull('deleted_at');
                })
            ],
            'email' => ['required', 'string', 'email',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('email', $request->email)->whereNull('deleted_at');
                })
            ],
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
            'remarks' => 'required|string',
            'signature_file' => 'required|mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,docx,xlsx',
            'roles.*' => 'integer',
            'permissions.*' => 'integer',
            'region_id' => 'required'
        ], [
            'region_id.required' => 'The region field is required.'
        ]);
        if (!$validator->fails()) {
            //uploading the images
            if ($request->has('signature_file')) {
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_manager/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $signature_file_with_time = time() . '_' . $name;
                $destinationPath = public_path('/uploads/operation_manager');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;
            } else {
                $signature_file = null;
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_manager/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $destinationPath = public_path('/uploads/operation_manager');
                $profile_image_time = time() . '_' . $name;
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;
            } else {
                $profile_image = null;
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_manager/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $destinationPath = public_path('/uploads/operation_manager');
                $additional_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = null;
            }

            $user = User::create([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'country_id' => $request->country_id,
                'region_id' => (int)$request->region_id,
                'city_id' => $request->city_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'operation_manager'
            ]);
            //add record in scheme manager table
            $operation_manger = new OperationManager();
            $operation_manger->user_id = $user->id;
            $operation_manger->full_name = $request->fullname;
            $operation_manger->joining_date = date("Y/m/d", strtotime($request->joining_date));
            $operation_manger->landline = $request->landline;
            $operation_manger->signature_image = $signature_file;
            $operation_manger->profile_image = $profile_image;
            $operation_manger->additional_files = $additional_files;
            $operation_manger->save();

            //            //assigning roles and permissions to user
            $role = Role::where('id', 4)->first();
            $user->assignRole($role->name);
            foreach ($role->permissions as $permission) {
                $user->givePermissionTo($permission->name);
            }

            RegionalOperationManager::create([
                'region_id' => (int)$request->region_id,
                'operation_manager_id' => $operation_manger->id

            ]);


            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Operation Manager has been added successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $resident_countries = Countries::all();
        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $resident_cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $resident_cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }

//        $resident_cities = ResidentCity::where('country_id', $user->country_id)->get();
        $permissions = Permission::all();
        $already_taken_regions = $this->alreadyTakenRegion();
        if (!empty($already_taken_regions)) {
            $regions = Regions::whereNotIn('id', $already_taken_regions)->get();
        } else {
            $regions = Regions::all();
        }
        $selected_region = Regions::where('id', $user->region_id)->first();

        return view('operation-manager.show', compact('selected_region', 'user', 'resident_countries', 'resident_cities', 'regions', 'roles', 'permissions'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $resident_countries = Countries::all();
//        $resident_cities = ResidentCity::where('country_id', $user->country_id)->get();
        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $resident_cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $resident_cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
        // $permissions = Permission::all();
        $permissions = Permission::whereIn('role_id', $user->roles->pluck('id')->toArray())->get();

        $selected_region = Regions::where('id', $user->region_id)->first();

        $already_taken_regions = $this->alreadyTakenRegion();

        if (!empty($already_taken_regions)) {
            $regions = Regions::whereNotIn('id', $already_taken_regions)->get();
        } else {
            $regions = Regions::all();
        }

        return view('operation-manager.edit', compact('selected_region', 'user', 'resident_countries', 'resident_cities', 'regions', 'roles', 'permissions'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
            'username' => 'required|string',
            'email' => 'required|string|email|max:255',
            'remarks' => 'required|string',
            'region_id' => 'required|integer',
            'signature_file' => 'mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,docx,xlsx',
            'roles.*' => 'integer',
            'permissions.*' => 'integer'
        ], [
            'region_id.required' => 'The region field is required.'
        ]);
        if (!$validator->fails()) {
            $user = User::findOrFail($request->user_id);
            //uploading the images
            if ($request->has('signature_file')) {
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_manager/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $signature_file_with_time = time() . '_' . $name;
                $destinationPath = public_path('/uploads/operation_manager');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;
            } else {
                $signature_file = $user->operation_manager['signature_image'];
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_manager/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/operation_manager');
                $profile_image_time = time() . '_' . $name;
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;

            } else {
                $profile_image = $user->operation_manager['profile_image'];
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename = public_path('uploads/operation_manager/') . $name;

                if (File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/operation_manager');
                $additional_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = $user->operation_manager['additional_files'];
            }
            if (isset($request->password) && !is_null($request->password)) {
                $password = Hash::make($request->password);
            } else {
                $password = $user->password;
            }
            $user->update([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'password' => $password,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'region_id' => $request->region_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'operation_manager',
                'is_enabled' => $request->is_enabled == 'true' ? true : false
            ]);
            //add record in scheme manager table
            $user->operation_manager->update([
                'full_name' => $request->fullname,
                'joining_date' => date("Y/m/d", strtotime($request->joining_date)),
                'landline' => $request->landline,
                'signature_image' => $signature_file,
                'profile_image' => $profile_image,
                'additional_files' => $additional_files
            ]);

            //assigning roles and permissions to user
            if ($request->has('permissions')) {
                $user->syncPermissions($request->permissions);
            }

            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }

            RegionalOperationManager::where('operation_manager_id', $user->operation_manager->id)->delete();


            RegionalOperationManager::create([
                'region_id' => (int)$request->region_id,
                'operation_manager_id' => $user->operation_manager->id

            ]);
            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Operation Manager has been updated successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->operation_manager->operation_coordinators->count() == 0) {
            $user->operation_manager()->delete();
            $user->delete();

            return back()->with(['flash_status' => 'success', 'flash_message' => 'Operation manager has been deleted.']);
        }

        return back()->with(['flash_status' => 'error', 'flash_message' => 'Sorry the system can not able to delete this record.']);

    }

    public function getSchemeManagerStandards(Request $request)
    {
        $standards = SchemeManagerStandards::with('standard_family')->where('scheme_manager_id', $request->id)->get();

        return response()->json(['data' => $standards, 'status' => 'success']);
    }

}
