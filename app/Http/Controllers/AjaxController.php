<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\AJStandardStage;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardWitnessEvaluation;
use App\Certificate;
use App\Cities;
use App\CompanyStandardCode;
use App\CompanyStandardFoodCode;
use App\DataEntryIAF;
use App\DateEntryIAS;
use App\EnergyCode;
use App\FoodCategory;
use App\FoodFamilySheet;
use App\IAF;
use App\IAS;
use App\Notification;
use App\OperationCoordinator;
use App\OperationManager;
use App\PermissionName;
use App\PostAudit;
use App\PostAuditQuestionFile;
use App\SchemeCoordinator;
use App\SchemeInfoPostAudit;
use App\SchemeManager;
use App\User;
use App\TechnicalExpertStandard;

//use App\Imports\AreasImport;
//use App\Imports\IafImport;
use App\RegionsCountries;
use App\ResidentCountry;
use App\Standards;
use App\StandardsFamily;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Company;
use App\Regions;
use App\Countries;
use App\CompanyStandards;
use App\Auditor;
use App\AuditorStandardGrade;
use App\AuditorStandardFoodCode;
use App\AuditorStandardEnergyCode;
use App\CompanyStandardEnergyCode;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use function foo\func;

//use Maatwebsite\Excel\Facades\Excel;

class AjaxController extends Controller
{
    public function regionCountries(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'region_id' => 'required|exists:regions,id'
        ], [
            'region_id.required' => "Region is Required.",
            'region_id.exists' => "Invalid Region Selected."
        ]);

        if (!$validator->fails()) {
            $region = Regions::find($request->region_id);

            $response['status'] = 'success';
            // $response['message'] 	= "Returning Data.";
            $response['data'] = [
                'region' => $region,
                'region_countries' => $region->regionCountries()->with('country')->get(),
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function countryCities(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'country_id' => 'required|exists:countries,id'
        ], [
            'country_id.required' => "Country is Required.",
            'country_id.exists' => "Invalid Country Selected."
        ]);

        if (!$validator->fails()) {
            $country = Countries::find($request->country_id);


            $response['status'] = 'success';
            // $response['message'] 	= "Returning Data.";
            $response['data'] = [
                'country' => $country,
                'country_cities' => $country->cities()->orderBy('name', 'ASC')->get()
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function countryCitiesResidentials(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'country_id' => 'required|exists:countries,id'
        ], [
            'country_id.required' => "Country is Required.",
            'country_id.exists' => "Invalid Country Selected."
        ]);

        if (!$validator->fails()) {
            $country = Countries::find($request->country_id);

            $country_zones = Zone::where('country_id', $request->country_id)->pluck('id')->toArray();

            if (count($country_zones) > 0) {
                $country_cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
            } else {
                $country_cities = Cities::where('zoneable_id', $request->country_id)->where('zoneable_type', 'App\Countries')->get();
            }

            $response['status'] = 'success';
            $response['data'] = [
                'country' => $country,
                'country_cities' => $country_cities
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function residentCountryCities(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'country_id' => 'required|exists:resident_countries,id'
        ], [
            'country_id.required' => "Country is Required.",
            'country_id.exists' => "Invalid Country Selected."
        ]);

        if (!$validator->fails()) {
            $country = ResidentCountry::find($request->country_id);

            $response['status'] = 'success';
            // $response['message'] 	= "Returning Data.";
            $response['data'] = [
                'country' => $country,
                'country_cities' => $country->cities()->orderBy('name', 'ASC')->get()
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function companyNameExist(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'country_id' => 'required|exists:countries,id'
        ], [
            'name.required' => "Company Name is Required."
        ]);

        if (!$validator->fails()) {
            $company = Company::where(['name' => $request->name, 'country_id' => $request->country_id])->first();

            $response['status'] = 'success';
            // $response['message'] 	= "Returning Data.";
            if (!empty($company)) {
                $response['data']['already_exist'] = true;
                $response['data']['company'] = $company;
                $response['data']['company_standards'] = \App\CompanyStandards::where('company_id', $company->id)->with('standard')->get();
            } else {
                $response['data']['already_exist'] = false;
            }
        } else {
            $response['status'] = 'error';
            // $response['message'] 	= "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function ias(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        // $validator  = Validator::make($request->all(), [
        //     'iaf_id.*'  => 'required|json'
        // ], [
        //     'iaf_id.*.required'   => "IAF is Required.",
        //     'iaf_id.*.json'       => "Invalid IAF Selected."
        // ]);

        $validator = Validator::make($request->all(), [
            'iaf_id' => 'required|exists:i_a_fs,id'
        ], [
            'iaf_id.required' => "IAF is Required.",
            'iaf_id.exists' => "Invalid IAF Selected."
        ]);


//        dd($request->all());

        if (!$validator->fails()) {
            $id = (int)$request->iaf_id;
            //$iases    = \App\IAS::whereIn('iaf_id', json_decode($request->iaf_id, true))->orderBy('iaf_id')->get();
            $iases = \App\IAS::where('iaf_id', (int)$request->iaf_id)->where('standard_id', (int)$request->standard_id)->orderBy('code')->get();


            $response['status'] = 'success';
            // $response['message']        = "Returning Data.";
            $response['data']['iases'] = $iases->toArray();
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function companyProgress(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',

        ], [
            'company_id.required' => "Something Went Wrong.",
            'company_id.exists' => "Something Went Wrong.",
        ]);

        if (!$validator->fails()) {
            $company = Company::find($request->company_id);
            $company->company_form_progress -= 20;
            $company->save();

            $response['status'] = 'success';
            // $response['message']            = "Returning Data.";
            $response['data']['company'] = $company->toArray();
        } else {
            $response['status'] = 'error';
            $response['message'] = "Something Went Wrong.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function ipInfo(Request $request)
    {
        $ip = $request->ip();

        tryAgainLabel:

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://ipapi.co/$ip/json");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);

        $response = json_decode(curl_exec($ch));


        curl_close($ch);

        if (isset($response->reserved) && $response->reserved) {
            $ip = "103.217.177.122";
            goto tryAgainLabel;
        }
//        dd([$count,$response]);
        return response()
            ->json($response)
            ->withCallback($request->input('callback'));
    }

    public function foodCategory(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $foodSubCategories = [];
        $response['status'] = 'error';
        $response['data']['foodCategories'] = $foodSubCategories;

        if (AuditorStandardCode::where('auditor_standard_id', $request->auditor_standard_id)->count() > 0) {

            $auditorStandardCodes = AuditorStandardCode::where('auditor_standard_id', $request->auditor_standard_id)->get();
            $accreditation_ids = $auditorStandardCodes->pluck('accreditation_id')->toArray();
            $iaf_ids = $auditorStandardCodes->pluck('iaf_id')->toArray();
            $iaS_ids = $auditorStandardCodes->pluck('ias_id')->toArray();

            $foodCategories = FoodCategory::whereIn('accreditation_id', $accreditation_ids)
                ->whereIn('iaf_id', $iaf_ids)
                ->whereIn('ias_id', $iaS_ids)
                ->orderBy('code')->get();

            $response['status'] = 'success';
            $response['data']['foodCategories'] = $foodCategories->toArray();
        }

        return $response;
    }

    public function foodSubCategory(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        // $validator  = Validator::make($request->all(), [
        //     'iaf_id.*'  => 'required|json'
        // ], [
        //     'iaf_id.*.required'   => "IAF is Required.",
        //     'iaf_id.*.json'       => "Invalid IAF Selected."
        // ]);

        $validator = Validator::make($request->all(), [
            'food_category_id' => 'required|exists:food_categories,id'
        ], [
            'food_category_id.required' => "Food Code is Required.",
            'food_category_id.exists' => "Invalid Food Code Selected."
        ]);

        if (!$validator->fails()) {

            $foodSubCategories = \App\FoodSubCategory::where('food_category_id', $request->food_category_id, true)->orderBy('code')->get();

            $response['status'] = 'success';
            // $response['message']        = "Returning Data.";
            $response['data']['foodsubcategories'] = $foodSubCategories->toArray();
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function regionCountriesCities(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'region_id' => 'required|exists:regions,id'
        ], [
            'region_id.required' => "Region is Required.",
            'region_id.exists' => "Invalid Region Selected."
        ]);

        if (!$validator->fails()) {
            $region = Regions::where('id', $request->region_id)->with(['countries', 'countries.cities', 'countries.zones', 'countries.zones.cities'])->get();
            $response['status'] = 'success';
            // $response['message'] 	= "Returning Data.";
            $response['data'] = [
                'region' => $region,
//                'region_countries'	=> $region->regionCountries()->with('country')->get(),
//                'country_cities'    => $cities
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

//    public function uploadIAFFile(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'file' => 'required|mimes:xls,xlsx'
//        ]);
//        if (!$validator->fails()) {
//
//            Excel::import(new IafImport(), $request->file('file'));
//
//            $status = 'success';
//            $message = 'IAF File Imported Successfully.';
//        } else {
//            $status = 'error';
//            $message = 'File Format is not correct or Incorrect Data.';
//        }
//
//        return redirect()->route('data.entry.index')
//            ->with([
//                'flash_status' => $status,
//                'flash_message' => $message
//            ]);
//    }


    public function saveDataEntryIAF(Request $request)
    {

//        dd($request->all());


        $validator = Validator::make($request->all(), [
            'code' => ['required',
                Rule::unique('data_entry_iaf')->where(function ($query) use ($request) {
                    $query->where('code', $request->code)->whereNull('deleted_at');
                })
            ],
            'name' => ['required',
                Rule::unique('data_entry_iaf')->where(function ($query) use ($request) {
                    $query->where('name', $request->name)->whereNull('deleted_at');
                })
            ],

        ]);

        if (!$validator->fails()) {

            $model = new DataEntryIAF();
            $model->code = $request->code;
            $model->name = $request->name;
            $saveModel = $model->save();


            if ($saveModel) {
                $status = 'success';
                $message = 'IAF Code Added Successfully!';
//                $response = (new ApiMessageController())->saveresponse('IAF Code Added Successfully!');
            } else {
                $status = 'error';
                $message = 'Unable to save IAF Code. Try Again!';
            }

        } else {

            $status = 'error';
            $message = 'Form has some errors!';
        }
        return redirect()->route('data.entry.index')
            ->with([
                'flash_status' => $status,
                'flash_message' => $message
            ]);
    }

    public function deleteDataEntryIAF($modelId)
    {
        try {

            $model = DataEntryIAF::findOrFail($modelId);
            $iaf = IAF::where('iaf_id', $model->id)->get();
            if ($iaf->count() == 0) {
                $deleteItem = $model->delete();
                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("IAF Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry this IAF code is already in use.So the system not able to delete.");
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function updateDataEntryIAFStatus(Request $request)
    {
        try {
            $iafRecord = IAF::where('id', $request->iaf_id)->update([
                'iaf_code_status' => $request->status === "1" ? true : false
            ]);

            if ($iafRecord) {
                $response = (new ApiMessageController())->saveresponse("IAF Code Status Updated Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to update Status Data");
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditDataEntryIAFModal($modelId)
    {
        try {

            $data['iaf'] = DataEntryIAF::find($modelId);


            return view('dataentry.codes.edit-iaf-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateDataEntryIAF(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'iaf_id' => 'required|exists:data_entry_iaf,id',


        ], [
            'iaf_id.required' => "Please Create IAF Code First",
        ]);

        if (!$validator->fails()) {

            $model = DataEntryIAF::find($request->iaf_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('IAF Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save IAF. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function saveDataEntryIAS(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => ['required',
                Rule::unique('data_entry_ias')->where(function ($query) use ($request) {
                    $query->where('code', $request->code)->whereNull('deleted_at');
                })
            ],
            'name' => ['required',
                Rule::unique('data_entry_ias')->where(function ($query) use ($request) {
                    $query->where('name', $request->name)->whereNull('deleted_at');
                })
            ],

        ]);

        if (!$validator->fails()) {

            $model = new DateEntryIAS();
            $model->code = $request->code;
            $model->name = $request->name;
            $saveModel = $model->save();


            if ($saveModel) {
                $status = 'success';
                $message = 'IAS Code Added Successfully!';
//                $response = (new ApiMessageController())->saveresponse('IAF Code Added Successfully!');
            } else {
                $status = 'error';
                $message = 'Unable to save IAS Code. Try Again!';
            }

        } else {

            $status = 'error';
            $message = 'Form has some errors!';
        }
        return redirect()->route('data.entry.index')
            ->with([
                'flash_status' => $status,
                'flash_message' => $message
            ]);
    }

    public function deleteDataEntryIAS($modelId)
    {
        try {

            $model = DateEntryIAS::findOrFail($modelId);

            $ias = IAS::where('ias_id', $model->id)->get();


            if ($ias->count() == 0) {
                $deleteItem = $model->delete();
                if ($deleteItem) {
                    $response = (new ApiMessageController())->saveresponse("IAS Code Deleted Successfully");
                } else {
                    $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
                }
            } else {
                $response = (new ApiMessageController())->failedresponse("Sorry this IAS code is already in use.So the system not able to delete.");
            }


        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

    public function getEditDataEntryIASModal($modelId)
    {
        try {

            $data['ias'] = DateEntryIAS::find($modelId);


            return view('dataentry.dataentry-codes.edit-ias-modal', $data);

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateDataEntryIAS(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'ias_id' => 'required|exists:data_entry_ias,id',


        ], [
            'ias_id.required' => "Please Create IAS Code First",
        ]);

        if (!$validator->fails()) {

            $model = DateEntryIAS::find($request->ias_id);
            $model->code = $request->code;
            $model->name = $request->name;
            $saveModel = $model->save();


            if ($saveModel) {
                $response = (new ApiMessageController())->saveresponse('IAS Updated Successfully!');
            } else {
                $response = (new ApiMessageController())->saveresponse('Unable to save IAS. Try Again!');
            }

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function foodCategoryByAccreditation(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'accreditation_id' => 'required'
        ], [
            'accreditation_id.required' => "Accreditation is Required.",
            'accreditation_id.exists' => "Invalid Accreditation Selected."
        ]);

        if (!$validator->fails()) {
            $food_category = FoodCategory::where('accreditation_id', $request->accreditation_id)->orderBy('name', 'asc')->get();
            $response['status'] = 'success';
            $response['data'] = [
                'food_category' => $food_category,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function energyByAccreditation(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'accreditation_id' => 'required'
        ], [
            'accreditation_id.required' => "Accreditation is Required.",
            'accreditation_id.exists' => "Invalid Accreditation Selected."
        ]);

        if (!$validator->fails()) {
            $energy_codes = EnergyCode::where('accreditation_id', $request->accreditation_id)->orderBy('name', 'asc')->get();
            $response['status'] = 'success';
            $response['data'] = [
                'energy_codes' => $energy_codes,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function accreditationByAuditorStandardId(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required'
        ], [
            'auditor_standard_id.required' => "Auditor Standard is Required.",
        ]);

        if (!$validator->fails()) {
            $accreditations = Accreditation::where('standard_id', AuditorStandard::where('id', $request->auditor_standard_id)->first()->standard_id)->get();
            $response['status'] = 'success';
            $response['data'] = [
                'accreditations' => $accreditations,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function updateAuditorStandardStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required'
        ], [
            'auditor_standard_id.required' => "Auditor Standard is Required.",
        ]);

        if (!$validator->fails()) {

            $auditor_standard = AuditorStandard::find($request->auditor_standard_id);
            $auditor_standard->status = $request->auditor_status;
            $auditor_standard->save();

            $response['status'] = 'success';
            $response['data'] = [
                'auditor_standard' => $auditor_standard,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function companyAccreditationByStandardId(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'standard_id' => 'required'
        ], [
            'standard_id.required' => "Standard is Required.",
        ]);

        if (!$validator->fails()) {
            $accreditations = Accreditation::where('standard_id', $request->standard_id)->get();
            $response['status'] = 'success';
            $response['data'] = [
                'accreditations' => $accreditations,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function companyIAFByAccreditation(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'accreditation_id' => 'required'
        ], [
            'accreditation_id.required' => "Accreditation is Required.",
        ]);

        if (!$validator->fails()) {
            $iafs = IAF::where('accreditation_id', $request->accreditation_id)->where('iaf_code_status', 1)->get();
            $response['status'] = 'success';
            $response['data'] = [
                'iafs' => $iafs,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function mandayCalculatorStandardBasedFoodCode(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'standard_id' => 'required'
        ], [
            'standard_id.required' => "Standard is Required.",
        ]);

        if (!$validator->fails()) {
            $food_category_id = FoodFamilySheet::where('standard_id', $request->standard_id)->pluck('food_category_id')->toArray();
            $foodCategory = FoodCategory::whereIn('id', $food_category_id)->orderBy('code', 'asc')->get();


            $response['status'] = 'success';
            $response['data'] = [
                'foodCategory' => $foodCategory,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function companyAccreditationByStandardIdCompanyId(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'standard_id' => 'required',
            'company_id' => 'required',
        ], [
            'standard_id.required' => "Standard is Required.",
            'company_id.required' => "Company is Required.",
        ]);

        if (!$validator->fails()) {

            $accreditationsId = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->pluck('accreditation_id')->toArray();

            $accreditations = Accreditation::whereIn('id', $accreditationsId)->get();
            $response['status'] = 'success';
            $response['data'] = [
                'accreditations' => $accreditations,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function auditorStandaradInformation(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'auditor_standard_id' => 'required',
        ], [
            'auditor_standard_id.required' => "Auditor Standard is Required.",
        ]);

        if (!$validator->fails()) {

            $auditorStandards = AuditorStandard::where('id', $request->auditor_standard_id)->first();

            if ($request->type == 'auditor') {
                $data = view('reports.list-of-auditor.auditor-standards-modal', compact('auditorStandards'))->render();
            } else if ($request->type == 'technical-expert') {
                $data = view('reports.list-of-technical-expert.technical-expert-auditor-modal', compact('auditorStandards'))->render();
            } elseif ($request->type == 'evaluation-schedule') {
                $auditorStandardWithnessEvaluation = AuditorStandardWitnessEvaluation::where('auditor_standard_id', $request->auditor_standard_id)->get();
                $data = view('reports.auditor-evaluation-schedule.auditor-evaluation-modal', compact('auditorStandards', 'auditorStandardWithnessEvaluation'))->render();
            }


            // $data =view('reports.list-of-auditor.auditor-standards-modal', compact('auditorStandards', 'TechnicalExpertStandard'))->render();
            $response['status'] = 'success';
            $response['data'] = [
                'html' => $data,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function getIasCode(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'standard_id' => 'required',
            'accedation_id' => 'required',
            'iaf_id' => 'required',


        ]);
        if (!$validator->fails()) {

            $iases = IAS::where(
                [
                    'iaf_id' => (int)$request->iaf_id,
                    'accreditation_id' => (int)$request->accedation_id,
                    'standard_id' => (int)$request->standard_id,
                ])->orderBy('code')->get();

            $response['status'] = 'success';
            $response['data'] = $iases;
            $response['message'] = "Ias Code Successfully get";
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }
        return $response;
    }

    public function getIafCode(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'standard_id' => 'required',
            'accreditation_id' => 'required',

        ]);
        if (!$validator->fails()) {

            $iases = IAF::where(['accreditation_id' => (int)$request->accreditation_id, 'standard_id' => (int)$request->standard_id])->orderBy('code')->get();

            $response['status'] = 'success';
            $response['data'] = $iases;
            $response['message'] = "Iaf Code Successfully get";
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }
        return $response;
    }

    public function getAccreditationByStanadardId(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'standard_id' => 'required',

        ]);
        if (!$validator->fails()) {

            $accreditation = Accreditation::where('standard_id', (int)$request->standard_id)->get();

            $response['status'] = 'success';
            $response['data'] = $accreditation;
            $response['message'] = "Accreditation  Successfully get";
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }
        return $response;
    }

    public function auditorStandaradTeam(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
//            'standard_id' => 'required',
            'grade_id' => 'required',
        ], [
            'company_id.required' => "Something Went Wrong.",
            'company_id.exists' => "Something Went Wrong.",
        ]);

        if (!$validator->fails()) {
            //  $request->standard_id = 27 23;
//        dd($request->version_change);


            $company = Company::whereRaw('name <> ""')->where('id', $request->company_id)->first();
            if (!isset($request->version_change) || $request->version_change == "false") {
                $companyStandards = CompanyStandards::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->first();
            } else {
                $companyStandards = CompanyStandards::where('company_id', $request->company_id)->whereIn('standard_id', [$request->baseStandardId])->first();
            }


            if (isset($companyStandards) && $companyStandards->is_ims == true) {
//                $family_name = $companyStandards->standard->standardFamily->name;
                if ($companyStandards) {
//                    $family_name = $companyStandards->standard->standardFamily->name;
                    $companyStandardIds = explode(',', $companyStandards->ims_standard_ids);
                    if ($request->version_change == "false") {
                        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $request->company_id)->pluck('standard_id')->toArray();
                    } else {
                        $activeImsStandards = $request->standard_id;
                    }


                    if ($request->grade_id == 7) {

                        $uniqueCode = CompanyStandardCode::where('company_id', $request->company_id)->whereIn('standard_id', $activeImsStandards)->pluck('unique_code')->toArray();
                        if (count($activeImsStandards) > 2) {
                            $auditor_standards = DB::table('auditor_standards')
                                ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                ->whereIn('standard_id', $activeImsStandards)
                                ->whereNull('deleted_at')
                                ->where('status', 'active')
                                ->orWhere('status', 'suspended')
                                ->groupBy('auditor_id')
//                                ->havingRaw('COUNT(*) = 3')
                                ->pluck('auditor_standard_id')->toArray();
                        } else {
                            $auditor_standards = DB::table('auditor_standards')
                                ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                ->whereIn('standard_id', $activeImsStandards)
                                ->whereNull('deleted_at')
                                ->where('status', 'active')
                                ->orWhere('status', 'suspended')
                                ->groupBy('auditor_id')
//                                ->havingRaw('COUNT(*) = 2')
                                ->pluck('auditor_standard_id')->toArray();
                        }
                        $auditorStandardId = [];
                        foreach ($auditor_standards as $standards) {
                            $arrayData = explode(',', $standards);
                            foreach ($arrayData as $data) {
                                array_push($auditorStandardId, $data);
                            }
                        }

                        $codeAuditorStandardId = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('unique_code', $uniqueCode)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();
                        $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $codeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();
                        $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('status', 'active')->orWhere('status', 'suspended')->where('auditor_standard_status', 'approved')->groupBy('auditor_id')->pluck('auditor_id')->toArray();


                        $auditors = Auditor::where('auditor_status', 'active')->whereIn('id', $auditorId);


                        if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                            $auditors->where('region_id', $company->region_id);
                        }
//                        if ($request->perposedTeam) {
//                            $auditors->whereNotIn('id', $request->perposedTeam);
//                        }
                        if ($request->perposedTeam) {
                            $result = array();
                            foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                if ($val2 != 1 && $val2 != 2 && $val2 != 3 && $val2 != 4 && $val2 != 5) {
                                    $result[$key] = $val; // combine 'em
                                }
                            }
                            $auditors->whereNotIn('id', $result);

                        }

                        $auditors = $auditors->get();
//                        dd($auditors);

                        $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                            $auditorStandards = $auditors[$key]->auditorStandards;

                            $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $auditorStandard->id)->where('grade_id', $request->grade_id)->with('grade')->get();

                            });
                        });


                    } elseif ($request->grade_id == 6) {

                        if (count($activeImsStandards) > 2) {
                            $auditor_standards = DB::table('auditor_standards')
                                ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                ->whereIn('standard_id', $activeImsStandards)
                                ->whereNull('deleted_at')
                                ->where('status', 'active')
                                ->orWhere('status', 'suspended')
                                ->groupBy('auditor_id')
                                ->havingRaw('COUNT(*) = 3')
                                ->pluck('auditor_standard_id')->toArray();
                        } else {
                            $auditor_standards = DB::table('auditor_standards')
                                ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                ->whereIn('standard_id', $activeImsStandards)
                                ->whereNull('deleted_at')
                                ->where('status', 'active')
                                ->orWhere('status', 'suspended')
                                ->groupBy('auditor_id')
                                ->havingRaw('COUNT(*) = 2')
                                ->pluck('auditor_standard_id')->toArray();
                        }
                        $auditorStandardId = [];
                        foreach ($auditor_standards as $standards) {
                            $arrayData = explode(',', $standards);
                            foreach ($arrayData as $data) {
                                array_push($auditorStandardId, $data);
                            }
                        }
                        $accreditationsId = CompanyStandardCode::where('company_id', $request->company_id)->whereIn('standard_id', $activeImsStandards)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                        $codeAuditorStandardId = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                        $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $codeAuditorStandardId)->where('grade_id', 1)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                        $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('status', 'active')->orWhere('status', 'suspended')->where('auditor_standard_status', 'approved')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                        $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);

                        if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                            $auditors->where('region_id', $company->region_id);
                        }

                        if ($request->perposedTeam) {
                            $auditors->whereNotIn('id', $request->perposedTeam);
                        }

                        $auditors = $auditors->get();

                        $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                            $auditorStandards = $auditors[$key]->auditorStandards;

                            $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', 1)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                            });
                        });
                    } else {

                        if ($request->grade_id == 1) {


                            if (count($activeImsStandards) > 2) {
                                $auditor_standards = DB::table('auditor_standards')
                                    ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                    ->whereIn('standard_id', $activeImsStandards)
                                    ->whereNull('deleted_at')
                                    ->where('status', 'active')
                                    ->orWhere('status', 'suspended')
                                    ->groupBy('auditor_id')
                                    ->havingRaw('COUNT(*) = 3') //3
                                    ->pluck('auditor_standard_id')->toArray();
                            } else {
                                $auditor_standards = DB::table('auditor_standards')
                                    ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                    ->whereIn('standard_id', $activeImsStandards)
                                    ->whereNull('deleted_at')
                                    ->where('status', 'active')
                                    ->orWhere('status', 'suspended')
                                    ->groupBy('auditor_id')
                                    ->havingRaw('COUNT(*) = 2') // 2
                                    ->pluck('auditor_standard_id')->toArray();
                            }
                        } else {
                            if (count($activeImsStandards) > 2) {
                                $auditor_standards = DB::table('auditor_standards')
                                    ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                    ->whereIn('standard_id', $activeImsStandards)
                                    ->whereNull('deleted_at')
                                    ->where('status', 'active')
                                    ->orWhere('status', 'suspended')
                                    ->groupBy('auditor_id')
//                                    ->havingRaw('COUNT(*) = 3') //3
                                    ->pluck('auditor_standard_id')->toArray();
                            } else {
                                $auditor_standards = DB::table('auditor_standards')
                                    ->select('auditor_id', DB::raw('GROUP_CONCAT(id) as auditor_standard_id'))
                                    ->whereIn('standard_id', $activeImsStandards)
                                    ->whereNull('deleted_at')
                                    ->where('status', 'active')
                                    ->orWhere('status', 'suspended')
                                    ->groupBy('auditor_id')
//                                    ->havingRaw('COUNT(*) = 2') // 2
                                    ->pluck('auditor_standard_id')->toArray();
                            }
                        }


                        $auditorStandardId = [];
                        foreach ($auditor_standards as $standards) {
                            $arrayData = explode(',', $standards);
                            foreach ($arrayData as $data) {
                                array_push($auditorStandardId, $data);
                            }
                        }


                        $accreditationsId = CompanyStandardCode::where('company_id', $request->company_id)->whereIn('standard_id', $activeImsStandards)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();


                        $codeAuditorStandardId = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                        $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $codeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();
                        $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                        $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);
                        if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                            $auditors->where('region_id', $company->region_id);
                        }

//                        if ($request->perposedTeam) {
//
//                            $auditors->whereNotIn('id', $request->perposedTeam);
//                        }

                        if ($request->perposedTeam) {

                            if ($request->grade_id == 1 || $request->grade_id == 2 || $request->grade_id == 3 || $request->grade_id == 4 || $request->grade_id == 5) {
                                $result = array();
                                foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                    $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                    if ($val2 != 7) {
                                        $result[$key] = $val; // combine 'em
                                    }
                                }
                                $auditors->whereNotIn('id', $result);
                            } else {
                                $auditors->whereNotIn('id', $request->perposedTeam);
                            }
                        }

                        $auditors = $auditors->get();


//                        dd($auditors);


                        $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                            $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                            $auditorStandards = $auditors[$key]->auditorStandards;

                            $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                            });
                        });
                    }
                    $response['status'] = 'success';
                    $response['data'] = [
                        'auditors' => $auditors,
                    ];
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "company standard not found";
                    $response['data'] = array();
                }
            } else {
//                $family_name = $companyStandards->standard->standardFamily->name;
                if ($companyStandards) {
                    $family_name = $companyStandards->standard->standardFamily->name;

                    if ($request->grade_id == 7) {


                        if ($family_name == 'Food') {

                            $uniqueCode = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->pluck('unique_code')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $FoodCodeAuditorStandardId = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('unique_code', $uniqueCode)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $FoodCodeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();


                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::whereIn('role', ['auditor', 'technical_expert'])->where('auditor_status', 'active')->whereIn('id', $auditorId);
                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {
                                $result = array();
                                foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                    $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                    if ($val2 != 1 && $val2 != 2 && $val2 != 3 && $val2 != 4 && $val2 != 5) {
                                        $result[$key] = $val; // combine 'em
                                    }
                                }
                                $auditors->whereNotIn('id', $result);

                            }

                            $auditors = $auditors->get();

                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });


                        } elseif ($family_name == 'Energy Management') {


                            $uniqueCode = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->pluck('unique_code')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $energyCodeAuditorStandardId = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('unique_code', $uniqueCode)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $energyCodeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('auditor_standard_status', 'approved')->where('deleted_at', null)->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::whereIn('role', ['auditor', 'technical_expert'])->where('auditor_status', 'active')->whereIn('id', $auditorId);

                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {
                                $result = array();
                                foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                    $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                    if ($val2 != 1 && $val2 != 2 && $val2 != 3 && $val2 != 4 && $val2 != 5) {
                                        $result[$key] = $val; // combine 'em
                                    }
                                }
                                $auditors->whereNotIn('id', $result);

                            }

                            $auditors = $auditors->get();


                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });

                        } else {


                            $uniqueCode = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->pluck('unique_code')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $codeAuditorStandardId = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('unique_code', $uniqueCode)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $codeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();


                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();


                            $auditors = Auditor::where('auditor_status', 'active')->whereIn('role', ['auditor', 'technical_expert'])->whereIn('id', $auditorId);

                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {
                                $result = array();
                                foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                    $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                    if ($val2 != 1 && $val2 != 2 && $val2 != 3 && $val2 != 4 && $val2 != 5) {
                                        $result[$key] = $val; // combine 'em
                                    }
                                }

                                $auditors->whereNotIn('id', $result);

                            }

                            $auditors = $auditors->get();


                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });

                        }


                    }
                    elseif ($request->grade_id == 6) {


                        if ($family_name == 'Food') {

                            $accreditationsId = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $FoodCodeAuditorStandardId = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $FoodCodeAuditorStandardId)->where('grade_id', 1)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);
                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {
                                $auditors->whereNotIn('id', $request->perposedTeam);
                            }

                            $auditors = $auditors->get();

                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', 1)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });


                        }
                        elseif ($family_name == 'Energy Management') {

                            $accreditationsId = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $energyCodeAuditorStandardId = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $energyCodeAuditorStandardId)->where('grade_id', 1)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);
                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {
                                $auditors->whereNotIn('id', $request->perposedTeam);
                            }

                            $auditors = $auditors->get();

                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', 1)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });

                        }
                        else {

                            $accreditationsId = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $codeAuditorStandardId = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $codeAuditorStandardId)->where('grade_id', 1)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);
                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {
                                $auditors->whereNotIn('id', $request->perposedTeam);
                            }

                            $auditors = $auditors->get();

                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', 1)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });

                        }


                    }
                    else {

                        if ($family_name == 'Food') {


                            $accreditationsId = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $FoodCodeAuditorStandardId = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();
                            //11
                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $FoodCodeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId)->where('region_id', $company->region_id);


                            if ($request->perposedTeam) {

                                if ($request->grade_id == 1 || $request->grade_id == 2 || $request->grade_id == 3 || $request->grade_id == 4 || $request->grade_id == 5) {
                                    $result = array();
                                    foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                        $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                        if ($val2 != 7) {
                                            $result[$key] = $val; // combine 'em
                                        }
                                    }
                                    $auditors->whereNotIn('id', $result);
                                } else {
                                    $auditors->whereNotIn('id', $request->perposedTeam);
                                }
                            }

                            $auditors = $auditors->get();

                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });


                        }
                        elseif ($family_name == 'Energy Management') {

                            $accreditationsId = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $energyCodeAuditorStandardId = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $energyCodeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                            $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);
                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {

                                if ($request->grade_id == 1 || $request->grade_id == 2 || $request->grade_id == 3 || $request->grade_id == 4 || $request->grade_id == 5) {
                                    $result = array();
                                    foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                        $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                        if ($val2 != 7) {
                                            $result[$key] = $val; // combine 'em
                                        }
                                    }
                                    $auditors->whereNotIn('id', $result);
                                } else {
                                    $auditors->whereNotIn('id', $request->perposedTeam);
                                }
                            }

                            $auditors = $auditors->get();

                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });

                        }
                        else {

                            $accreditationsId = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->groupBy('accreditation_id')->pluck('accreditation_id')->toArray();

                            $auditorStandardId = AuditorStandard::where(['standard_id' => $request->standard_id, 'deleted_at' => null])->where('status', 'active')->orWhere('status', 'suspended')->pluck('id')->toArray();

                            $codeAuditorStandardId = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereIn('accreditation_id', $accreditationsId)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $gradeAuditorStandardId = AuditorStandardGrade::whereIn('auditor_standard_id', $codeAuditorStandardId)->where('grade_id', $request->grade_id)->groupBy('auditor_standard_id')->pluck('auditor_standard_id')->toArray();

                            $auditorId = AuditorStandard::whereIn('id', $gradeAuditorStandardId)->where('deleted_at', null)->where('auditor_standard_status', 'approved')->where('status', 'active')->orWhere('status', 'suspended')->groupBy('auditor_id')->pluck('auditor_id')->toArray();


                            $auditors = Auditor::where('role', 'auditor')->where('auditor_status', 'active')->whereIn('id', $auditorId);
                            if (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
                                $auditors->where('region_id', $company->region_id);
                            }

                            if ($request->perposedTeam) {

                                if ($request->grade_id == 1 || $request->grade_id == 2 || $request->grade_id == 3 || $request->grade_id == 4 || $request->grade_id == 5) {
                                    $result = array();
                                    foreach ($request->perposedTeam as $key => $val) { // Loop though one array
                                        $val2 = $request->perposed_team_grade_id[$key]; // Get the values from the other array
                                        if ($val2 != 7) {
                                            $result[$key] = $val; // combine 'em
                                        }
                                    }
                                    $auditors->whereNotIn('id', $result);
                                } else {
                                    $auditors->whereNotIn('id', $request->perposedTeam);
                                }
                            }

                            $auditors = $auditors->get();

                            // dd($auditors);
                            $auditors->each(function ($auditor, $key) use ($auditors, $request, $gradeAuditorStandardId) {

                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereIn('id', $gradeAuditorStandardId)->where('status', 'active')->orWhere('status', 'suspended')->get();
                                $auditorStandards = $auditors[$key]->auditorStandards;

                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards, $request) {

                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('grade_id', $request->grade_id)->where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                });
                            });

                        }
                    }
                    $response['status'] = 'success';
                    $response['data'] = [
                        'auditors' => $auditors,
                    ];
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "company standard not found";
                    $response['data'] = array();
                }
            }


        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function deletepermission(Request $request)
    {
        $response = array('status' => '', 'data' => []);
        $get_all_user = DB::table('model_has_roles')->where('role_id', $request->roleId)->get();
        $role = Role::where('id', $request->roleId)->first();
        $permission = Permission::where('id', $request->permissionId)->first();
        if (!is_null($permission)) {
            $role->revokePermissionTo($permission);
        }

        foreach ($get_all_user as $user_id) {
            $user = User::where('id', $user_id->model_id)->first();
            if (!is_null($user)) {
                $user->revokePermissionTo($permission);
            }

        }
        $response['status'] = 'success';
        $response['data'] = $role->permissions;
        return $response;
    }

    public function isRead(Request $request)
    {
        $response = array('status' => '', 'data' => []);


        $notication = Notification::where('id', (int)$request->notification_id)->first();

        if ($notication->type == 'outsource_request' && ($notication->status == 'approved' || $notication->status == 'rejected')) {
            $notication->is_read = true;
            $notication->save();
            $response['status'] = 'success';
            $response['action'] = 'true';
            $response['data'] = $notication->id;
        } elseif ($notication->type == 'auditor' && ($notication->status == 'approved')) {
            $notication->is_read = true;
            $notication->save();
            $response['status'] = 'success';
            $response['action'] = 'true';
            $response['data'] = $notication->id;
        } elseif ($notication->type == 'technical_expert' && ($notication->status == 'approved')) {
            $notication->is_read = true;
            $notication->save();
            $response['status'] = 'success';
            $response['action'] = 'true';
            $response['data'] = $notication->id;
//        } elseif ($notication->type == 'draft_for_printing' && ($notication->status == 'approved')) {
//            $notication->is_read = true;
//            $notication->save();
//            $response['status'] = 'success';
//            $response['action'] = 'true';
//            $response['data'] = $notication->id;
        } elseif ($notication->type == 'audit_justification' && ($notication->status == 'approved')) {
            $notication->is_read = true;
            $notication->save();
            $response['status'] = 'success';
            $response['action'] = 'true';
            $response['data'] = $notication->id;
        } elseif ($notication->type == 'post_audit' && ($notication->status == 'approved')) {
            $notication->is_read = true;
            $notication->save();
            $response['status'] = 'success';
            $response['action'] = 'true';
            $response['data'] = $notication->id;
        } else {
            $response['status'] = 'success';
            $response['action'] = 'false';
        }

        return $response;
    }

    public function getRemainingPermissions(Request $request)
    {


        $response = array('status' => '', 'data' => []);
        $role = Role::where('id', (int)$request->role_id)->first();
        if (!is_null($role)) {
            if (!empty($role->permissions) && count($role->permissions)) {
                $permissionNames = PermissionName::whereNotIn('name', $role->permissions->pluck('name')->toArray())->get();
            } else {
                $permissionNames = PermissionName::all();
            }
        }
        $response['status'] = 'success';
        $response['data'] = $permissionNames;
        return $response;
    }

    public function removeAdditionalFile(Request $request)
    {
        $response = array('status' => '');
        if ($request->user_type == 'scheme_manager') {
            $scheme_manager = SchemeManager::where('user_id', (int)$request->user_id)->first();
            $scheme_manager->additional_files = null;
            $scheme_manager->save();
        }
        if ($request->user_type == 'scheme_coordinator') {
            $scheme_coordinator = SchemeCoordinator::where('user_id', (int)$request->user_id)->first();
            $scheme_coordinator->additional_files = null;
            $scheme_coordinator->save();
        }
        if ($request->user_type == 'operation_manager') {
            $operation_manager = OperationManager::where('user_id', (int)$request->user_id)->first();
            $operation_manager->additional_files = null;
            $operation_manager->save();
        }
        if ($request->user_type == 'operation_coordinator') {
            $operation_coordinator = OperationCoordinator::where('user_id', (int)$request->user_id)->first();
            $operation_coordinator->additional_files = null;
            $operation_coordinator->save();
        }
        $response['status'] = 'success';
        return $response;
    }

    public function getCompanyStandard(Request $request)
    {
        $response = array('status' => '', 'data' => []);

        $companyStandards = CompanyStandards::where('company_id', $request->company_id)->whereNull('deleted_at')->with('standard')->get();
        $response['status'] = 'success';
        $response['data'] = $companyStandards;
        return $response;
    }

    public function deletePostAuditQuestionFile(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());
        $file = PostAuditQuestionFile::find($request->file_id);
        if ($request->type === 'ims') {
            $filename = public_path('/uploads/post-audit-ims/' . $file->post_audit_question_id . '/' . $file->file_path);
        } elseif ($request->type === 'single') {
            $filename = public_path('/uploads/post-audit/' . $file->post_audit_question_id . '/' . $file->file_path);
        } elseif ($request->type === 'postAudit') {
            $postAudit = PostAudit::find($request->file_id);
            $filename = public_path('uploads/post-audit') . $postAudit->q4_file;
            $postAudit->q4_file = NULL;
            $postAudit->is_q4 = NULL;
            $postAudit->save();
        }


        if (File::exists($filename)) {
            File::delete($filename);  // or unlink($filename);
        }

        if ($request->type !== 'postAudit') {
            $file->delete();
        }


        $response['status'] = 'success';


        return $response;
    }

    public function approvedAjStandardStageStatus(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());
        $ajStandardStage = AJStandardStage::find($request->id);
        if ($request->aj_approved_status == 1) {
            $ajStandardStage->aj_approved_status = true;
        } else {
            $ajStandardStage->aj_approved_status = false;
        }
        $ajStandardStage->save();

        $response['status'] = 'success';


        return $response;
    }


    public function updateOriginalIssueDate(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'scheme_manager_post_audit_id' => 'required',
        ], [
            'scheme_manager_post_audit_id.required' => "Scheme Manager Post Audit Id Required.",
        ]);

        if (!$validator->fails()) {

            $scheme_manager_post_audit = SchemeInfoPostAudit::where('id', $request->scheme_manager_post_audit_id)->first();
            $scheme_manager_post_audit_id = $request->scheme_manager_post_audit_id;
            $data = view('certificate-modals.original-issue-date', compact('scheme_manager_post_audit', 'scheme_manager_post_audit_id'))->render();


            $response['status'] = 'success';
            $response['data'] = [
                'html' => $data,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function updateOriginalIssueDateStore(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'scheme_manager_post_audit_id' => 'required',
            'original_issue_date' => 'required',
        ], [
            'scheme_manager_post_audit_id.required' => "Scheme Manager Post Audit Id is Required.",
            'original_issue_date.required' => "Original Issue Date is Required.",
        ]);

        if (!$validator->fails()) {

            $scheme_manager_post_audit = SchemeInfoPostAudit::where('id', $request->scheme_manager_post_audit_id)->first();
            $scheme_manager_post_audit->original_issue_date = date("Y/m/d", strtotime($request->original_issue_date));
            $scheme_manager_post_audit->save();
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function companyStandardRemarks(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required',
        ], [
            'company_standard_id.required' => "Company Standard Id Required.",
        ]);

        if (!$validator->fails()) {

            $companyStandard = CompanyStandards::where('id', $request->company_standard_id)->first();
            $company_standard_id = $request->company_standard_id;
            $data = view('certificate-modals.company-standard-remarks', compact('company_standard_id', 'companyStandard'))->render();


            $response['status'] = 'success';
            $response['data'] = [
                'html' => $data,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function companyStandardRemarksStore(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required',
        ], [
            'company_standard_id.required' => "Company Standard Id is Required.",
        ]);

        if (!$validator->fails()) {

            $companyStandard = CompanyStandards::where('id', $request->company_standard_id)->first();
            $companyStandard->main_remarks = $request->planning_remarks;
//            if(isset($request->scheme_manager_remarks)){
            $companyStandard->scheme_manager_remarks = (!is_null($request->scheme_manager_remarks) && isset($request->scheme_manager_remarks)) ? $request->scheme_manager_remarks : '';
//            }

            $companyStandard->save();
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


    public function updateNotificationBell(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        if (auth()->check()) {
            if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator') {
                $notifications = Notification::where('sent_to', auth()->user()->id)->where('is_read', false)->where('is_final_read', false)->orderBy('id', 'desc')->limit(20)->get();
                $notificationsCount = Notification::where('sent_to', auth()->user()->id)->where('is_read', false)->where('is_final_read', false)->count();
            } else if (auth()->user()->user_type == 'admin') {
                $notifications = Notification::where('is_read', false)->where('is_final_read', false)->orderBy('id', 'desc')->limit(20)->get();
                $notificationsCount = Notification::where('is_read', false)->where('is_final_read', false)->count();
            } else if (auth()->user()->user_type == 'management') {
                $notifications = Notification::where('is_read', false)->where('type', '=', 'draft_for_printing')->where('is_final_read', false)->orderBy('id', 'desc')->limit(20)->get();
                $notificationsCount = Notification::where('is_read', false)->where('type', '=', 'draft_for_printing')->where('is_final_read', false)->count();
            } else {
                $notifications = null;
                $notificationsCount = 0;
            }
        } else {
            $notifications = null;
            $notificationsCount = 0;
        }

        $data = view('includes.notification', compact('notifications', 'notificationsCount'))->render();


        $response['status'] = 'success';
        $response['data'] = [
            'html' => $data,
        ];

        return $response;
    }

    public function updateNotificationBellAJ(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        if (auth()->check()) {

            if (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
                $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->limit(20)->get();
                $receivedNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->count();

            } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

                $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->limit(20)->get();
                $receivedNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->count();
            } else if (auth()->user()->user_type == 'admin') {

                $receivedNotifications = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_read', false)->where('is_final_read', false)->limit(20)->get();
                $receivedNotificationsCount = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_read', false)->where('is_final_read', false)->count();
            } else {

                $receivedNotifications = null;
                $receivedNotificationsCount = 0;
            }
        } else {

            $receivedNotifications = null;
            $receivedNotificationsCount = 0;
        }

        $data = view('includes.notification-aj', compact('receivedNotifications', 'receivedNotificationsCount'))->render();


        $response['status'] = 'success';
        $response['data'] = [
            'html' => $data,
        ];

        return $response;
    }

    public function updateNotificationBellPostAudit(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        if (auth()->check()) {

            if (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
                $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->limit(20)->get();
                $receivedPostAuditNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->count();

            } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

                $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->limit(20)->get();
                $receivedPostAuditNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->count();
            } else if (auth()->user()->user_type == 'admin') {

                $receivedPostAuditNotifications = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_read', false)->where('is_final_read', false)->limit(20)->get();
                $receivedPostAuditNotificationsCount = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_read', false)->where('is_final_read', false)->count();
            } else {

                $receivedPostAuditNotifications = null;
                $receivedPostAuditNotificationsCount = 0;
            }
        } else {

            $receivedPostAuditNotifications = null;
            $receivedPostAuditNotificationsCount = 0;
        }

        $data = view('includes.notification-pack', compact('receivedPostAuditNotifications', 'receivedPostAuditNotificationsCount'))->render();


        $response['status'] = 'success';
        $response['data'] = [
            'html' => $data,
        ];

        return $response;
    }

    public function addGradeToCompanyStandard(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());
        $companyStandard = CompanyStandards::find($request->companyStandardId);
        $companyStandard->grade = $request->grade;
        $companyStandard->save();

        $response['status'] = 'success';


        return $response;
    }

    public function companyStandardUpdateContractPrice(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());
        $company_standard = CompanyStandards::find($request->companyStandardId);
        $company_standard->initial_certification_currency = $request->currency_unit;
        $company_standard->re_audit_currency = $request->currency_unit;
        $company_standard->surveillance_currency = $request->currency_unit;

        $company_standard->initial_certification_amount = $request->client_type == 'transfered' ? 0 : $request->initial_certification_amount;
        $company_standard->surveillance_amount = $request->surveillance_amount;
        $company_standard->re_audit_amount = $request->re_audit_amount ?? '';

        $company_standard->save();

        $response['status'] = 'success';


        return $response;
    }
}
