<?php

namespace App\Http\Controllers;

use App\AjStageChange;
use App\AJStageTeam;
use App\AJStandard;
use App\AJStandardStage;
use App\AuditJustification;
use App\Auditor;
use App\AuditorStandard;
use App\AuditorStandardCode;
use App\AuditorStandardEnergyCode;
use App\AuditorStandardFoodCode;
use App\Certificate;
use App\Company;
use App\CompanyStandardCode;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\CompanyStandards;
use App\CompanyStandardsAnswers;
use App\FoodCategory;
use App\Grades;
use App\Notification;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\Traits\GeneralHelperTrait;
use App\Traits\MandayTrait;
use App\User;
use App\OutSourceRequest;
use App\OutSourceRequestAuditor;
use App\OutSourceRequestStandard;
use App\AuditorStandardGrade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class AuditJustificationSpecialIMSController extends Controller
{
    use MandayTrait, GeneralHelperTrait;

    /* Special Audit Justification */
    public function specialAJ($company_id = null, $standard_number = null)
    {
        return view('audit-justification.ims.special-audit.special-aj', compact('company_id', 'standard_number'));
    }

    public function addSpecialAJ(Request $request)
    {

        $company = Company::findOrFail($request->company_id);


        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);
        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }

//        $job_number = $this->jobNumber(Auth()->user()->country->iso);
        $standard_number = Standards::where('name', $request->standard_number)->first();


        $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
        $company_standards = [];
        $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
        //calculate manday of food family,energy family and generic family

        if (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Food') {

            $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->where('company_standards_id', $c_std->id)->get();

            foreach ($standardQuestions as $index => $questions) {

                if ($index === 0) {
                    $haccp = $questions->answer;
                } elseif ($index === 1) {
                    $certified_management = $questions->answer;
                }
//                elseif ($index === 2) {
//                    $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
////                        $food_category_id = FoodCategory::where('code', json_decode($questions->answer))->first()->id;
//                    $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
//                }
            }

            $companyStandFoodCodes = CompanyStandardFoodCode::where('company_id', $company->id)->where('standard_id', $standard_number->id)->first();
            $food_category_id = FoodCategory::where('code', $companyStandFoodCodes->foodcategory->code)->first()->id;
            $manday = $this->generateFoodSafetyTable($standard_number->id, $food_category_id, $haccp, $company->effective_employees, $certified_management, $company->childCompanies->count() + 1, $c_std->surveillance_frequency);

        } elseif (StandardsFamily::where('id', $standard_number->standards_family_id)->first()->name === 'Energy Management') {
            $standardQuestions = CompanyStandardsAnswers::where('standard_id', $standard_number->id)->where('company_standards_id', $c_std->id)->get();

            foreach ($standardQuestions as $index => $questions) {

                if ($index === 1) {
                    $energy_consumption_in_tj = $questions->answer;
                } elseif ($index === 3) {
                    $no_of_energy_resources = $questions->answer;
                } elseif ($index === 4) {
                    $no_of_significant_energy = $questions->answer;
                }
            }
            $manday = $this->generateEnergyTable($standard_number->id, $energy_consumption_in_tj, $no_of_energy_resources, $no_of_significant_energy, $company->effective_employees, $c_std->surveillance_frequency);

        } else {
            $manday = $this->generateSimpleTable($standard_number->id, $c_std->proposed_complexity, $company->effective_employees, $c_std->surveillance_frequency);
        }


        if (!empty($manday)) {
            $calc_manday = [];
            $calc_manday[$request->add_stage . '_offsite'] = $manday[$request->add_stage . '_offsite'];
            $calc_manday[$request->add_stage . '_onsite'] = $manday[$request->add_stage . '_onsite'];
            $audit_type_array = ['stage_1', 'stage_2', 'surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit', 'stage_1|stage_2'];
            $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();
            if (!empty($scheme_manager_standards)) {
                if (!empty($standard_number) && in_array($request->add_stage, $audit_type_array)) { //true

                    if (!empty($company->companyStandards)) {

                        foreach ($company->companyStandards as $standard) {
                            $company_standards[] = $standard->standard_id;
                        }

//                    $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->get();
                        $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();

                        if ($auditor_standards->isNotEmpty()) {

                            foreach ($auditor_standards as $auditor_standard) {

                                foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

//                                    if ($auditor_standard->auditor->is_available == 1) {
//                                        $members[] = $auditor_standard;
//                                        $standard_grades[] = $standard_grade;
//
//                                    }
                                    $members[] = $auditor_standard;
                                    $standard_grades[] = $standard_grade;
                                }
                                foreach ($company->companyIAFs as $key => $companyIaf) {
                                    $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                                }

                            }
                            //checking the stage1 and stage2 combine aj
                            if ($request->add_stage == 'stage_1|stage_2') {

                                $combine_tracker = 1;
                            } else {
                                $combine_tracker = 0;
                            }

                            //check the company already have any aj
                            $aj_obj = AuditJustification::where('company_id', $company->id)->first();


                            if (!empty($aj_obj)) {

                                //check which standards are in company aj
                                $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                    ->where('standard_id', $standard_number->id)
                                    ->first();

//
                                if (!empty($aj_standards_obj)) {

                                    if ($combine_tracker == 1) {
                                        //get the standard stage stage_1
                                        $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', 'stage_1')->first();
                                        $job_number = $aj_standard_stages_obj ? $aj_standard_stages_obj->job_number : '';

                                    } else {
                                        $check_Stage = $aj_standards_obj->ajStandardStages->where('audit_type', $request->add_stage . '_' . $request->special_audit_type)->first();
                                        $job_number = $check_Stage ? $check_Stage->job_number : '';

                                    }
                                    if (!empty($check_Stage)) {


                                        //now we want the single audit stage which want to edit
                                        $aj_standard_stage = $check_Stage;
                                        $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stage->id)->get();

                                        $aj_remarks = $aj_standard_stage->AJRemarks()->get();

                                        //also need all other audit stages for manday editing
                                        $aj_stages_manday[] = $aj_standard_stage;
                                        $audit_type = $aj_standard_stage->audit_type;
                                        $team_audit_type = $aj_standard_stage->audit_type;
                                        //return to the edit page
                                        $ims = 0;
                                        $aj_standard_stages_obj_for_job_number = $aj_standards_obj->ajStandardStages->where('audit_type', $request->add_stage)->first();
                                        $job_number =  $aj_standard_stages_obj_for_job_number ?  $aj_standard_stages_obj_for_job_number->job_number : '';
                                        return view('audit-justification.ims.special-audit.create', compact('job_number', 'team_audit_type', 'team_members', 'aj_remarks', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));


                                    } else {

                                        $getAllImsStandardsData = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();
                                        $ims_standard_ids = explode(',', $getAllImsStandardsData->ims_standard_ids);
                                        $activeImsStandards = CompanyStandards::whereIn('id', $ims_standard_ids)->where('company_id', $company->id)->pluck('standard_id')->toArray();


//                                    dd($aj_standards_obj->ajStandardStages);
                                        $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', $request->add_stage)->first();
                                        if (!empty($aj_standard_stages_obj)) {
                                            $job_number = $aj_standard_stages_obj->job_number;
                                            foreach ($activeImsStandards as $key => $standard) {
                                                $aj_stardard_data = AJStandard::where('audit_justification_id', $aj_obj->id)->where('standard_id', $standard)->whereNull('deleted_at')->first();
                                                //now we want the single audit stage which want to edit
                                                if (!is_null($aj_stardard_data)) {

                                                    $aj_standard_stage = $aj_stardard_data->ajStandardStages()->create([
                                                        'audit_type' => $request->add_stage . '_' . $request->special_audit_type,
                                                        'special_aj_remarks' => $request->special_audit_remarks,
                                                        'status' => 'created',
                                                        'job_number' => $job_number
                                                    ]);

                                                }

                                            }
                                            $firstStandard = AJStandardStage::where('aj_standard_id', $aj_standards_obj->id)->where('audit_type', $request->add_stage . '_' . $request->special_audit_type)->first();
                                            $aj_standard_stage = $firstStandard;
                                            $team_members = AJStageTeam::where('aj_standard_stage_id', $firstStandard->id)->get();
                                            $aj_remarks = $firstStandard->AJRemarks()->get();
                                            //also need all other audit stages for manday editing
                                            $aj_stages_manday[] = $firstStandard;
                                            $audit_type = $firstStandard->audit_type;
                                            $team_audit_type = $firstStandard->audit_type;
                                            //return to the edit page
                                            $ims = 0;

                                            return view('audit-justification.ims.special-audit.create', compact('job_number', 'team_audit_type', 'team_members', 'aj_remarks', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                        } else {
                                            return back()->with('flash_status', 'warning')
                                                ->with('flash_message', 'Stage not Exist for this standard');
                                        }
                                    }
                                } else {
                                    return back()->with('flash_status', 'warning')
                                        ->with('flash_message', 'Audit Justification Standard not available');
                                }

                            } else {
                                return back()->with('flash_status', 'warning')
                                    ->with('flash_message', 'Audit Justification not available');
                            }

                        } else {
                            return back()->with('flash_status', 'warning')
                                ->with('flash_message', 'Auditor standard not available');
                        }
                    } else {
                        return back()->with('flash_status', 'warning')
                            ->with('flash_message', 'Company does not have this standard');
                    }
                }
            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Scheme manager not available for this standard');
            }

        } else {
            return back()->with('flash_status', 'warning')
                ->with('flash_message', 'Man days not available for this standard');
        }


    }

    public function editSpecialAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::findOrFail($company_id);


        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);
        //now getting the role of the whom we want to send aj

        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }

        $standard_number = Standards::where('name', $standard_number)->first();

        $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
        $company_standards = [];
        $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

//        $job_number = $this->jobNumber(Auth()->user()->country->iso);
        $audit_type_array = ['stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'reaudit_verification', 'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'reaudit_scope_extension', 'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'reaudit_special_transition'];
        $scheme_manager_standards = SchemeManagerStandards::where('standard_family_id', $standard_number->standardFamily->id)->get();
        if (!empty($scheme_manager_standards)) {
            if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) { //true

                if (!empty($company->companyStandards)) {

                    foreach ($company->companyStandards as $standard) {
                        $company_standards[] = $standard->standard_id;
                    }

//                $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->get();
                    $auditor_standards = AuditorStandard::where('standard_id', $standard_number->id)->groupBy('auditor_id')->get();

                    if ($auditor_standards->isNotEmpty()) {

                        foreach ($auditor_standards as $auditor_standard) {

                            foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {

//                                if ($auditor_standard->auditor->is_available == 1) {
                                $members[] = $auditor_standard;
                                $standard_grades[] = $standard_grade;

//                                }
                            }
                            foreach ($company->companyIAFs as $key => $companyIaf) {
                                $standard_codes[] = $auditor_standard->auditorStandardCodes()->where('iaf_id', '=', $companyIaf->iaf_id)->get();
                            }

                        }
                        //checking the stage1 and stage2 combine aj
                        if ($audit_type == 'stage_1|stage_2') {

                            $combine_tracker = 1;
                        } else {
                            $combine_tracker = 0;
                        }

                        //check the company already have any aj
                        $aj_obj = AuditJustification::where('company_id', $company->id)->first();

                        $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();
//                    $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_id)->get();
                        if (!empty($aj_obj)) {

                            //check which standards are in company aj
                            $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                                ->where('standard_id', $standard_number->id)
                                ->first();

//
                            if (!empty($aj_standards_obj)) {

                                if ($combine_tracker == 1) {
                                    //get the standard stage stage_1
                                    $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', 'stage_1')->first();

                                } else {
                                    $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', $audit_type)->first();

                                }

                                if (!empty($aj_standard_stages_obj)) {

                                    if (!empty($c_std)) {
                                        $outSourceRequestStandardId = OutSourceRequestStandard::where('standard_id', $standard_number->id)->where('company_id', $company->id)->pluck('id')->toArray();
                                        $outsourceRequestsId = OutSourceRequest::whereIn('id', $outSourceRequestStandardId)->where('status', 'approved')->where('expiry_status', 'not expired')->pluck('id')->toArray();
                                        $outsourceRequestAuditorId = OutSourceRequestAuditor::whereIn('outsource_request_id', $outsourceRequestsId)->where('status', 'allocated')->pluck('auditor_id')->toArray();
                                        $auditorId = Auditor::whereIn('id', $outsourceRequestAuditorId)->where('auditor_status', 'active')->pluck('id')->toArray();
                                        $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditorId)->where('standard_id', $standard_number->id)->where('status', 'active')->groupBy('auditor_id')->pluck('auditor_id')->toArray();

                                        if ($auditorStandardId) {
                                            $auditors = Auditor::whereIn('id', $auditorStandardId)->where('auditor_status', 'active')->get();
                                            $auditors->each(function ($auditor, $key) use ($auditors, $standard_number) {
                                                $auditors[$key]->auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->where('status', 'active')->where('standard_id', $standard_number->id)->orWhere('status', 'suspended')->get();
                                                $auditorStandards = $auditors[$key]->auditorStandards;

                                                $auditorStandards->each(function ($auditorStandard, $key1) use ($auditorStandards) {

                                                    $auditorStandards[$key1]->auditorStandardGrades = AuditorStandardGrade::where('auditor_standard_id', $auditorStandard->id)->with('grade')->get();

                                                });
                                            });
                                        }
                                    }

                                    //now we want the single audit stage which want to edit
                                    $aj_standard_stage = $aj_standard_stages_obj;
                                    $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stage->id)->get();
                                    $aj_remarks = $aj_standard_stage->AJRemarks()->get();
                                    //also need all other audit stages for manday editing
                                    $aj_stages_manday[] = $aj_standard_stage;
                                    $audit_type = $aj_standard_stage->audit_type;
                                    $team_audit_type = $aj_standard_stage->audit_type;
                                    $job_number = $aj_standard_stages_obj->job_number;
                                    $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();

                                    //return to the edit page
                                    $ims = 0;
                                    $check = 0;
                                    return view('audit-justification.ims.special-audit.edit', compact('check', 'notifications', 'aj_standard_change', 'team_audit_type', 'job_number', 'team_members', 'aj_remarks', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                                } else {
                                    return back()->with('flash_status', 'warning')
                                        ->with('flash_message', 'Stage nto Exist for this standard');
                                }
                            }
                        }

                    }

                } else {
                    return back()->with('flash_status', 'warning')
                        ->with('flash_message', 'Auditor standard not available');
                }
            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Company does not have this standard');
            }
        } else {
            return back()->with('flash_status', 'warning')
                ->with('flash_message', 'Scheme manager not available for this standard');
        }
    }

    public function showSpecialAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::findOrFail($company_id);
        //first checking type of user/manager
        $user = User::find(Auth()->user()->id);
        //now getting the role of the whom we want to send aj


        if (in_array('scheme manager', $user->getRoleNames()->toArray())) {
            $role = 'scheme manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_manager')->get();
        } else if (in_array('super admin', $user->getRoleNames()->toArray())) {
            $role = 'super admin';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation manager', $user->getRoleNames()->toArray())) {
            $role = 'operation manager';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_manager')->get();
        } else if (in_array('operation coordinator', $user->getRoleNames()->toArray())) {
            $role = 'operation coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'operation_coordinator')->get();
        } else if (in_array('scheme coordinator', $user->getRoleNames()->toArray())) {
            $role = 'scheme coordinator';
            $role = str_replace(' ', '_', $role);
            $users = User::where('user_type', 'scheme_coordinator')->get();
        }

        $standard_number = Standards::where('name', $standard_number)->first();

        $auditor_grades = Grades::orderBy('sort_by', 'asc')->get();
        $company_standards = [];
        $c_std = CompanyStandards::where('standard_id', $standard_number->id)->where('company_id', $company->id)->first();

        $audit_type_array = ['stage_2_verification', 'surveillance_1_verification', 'surveillance_2_verification', 'reaudit_verification', 'stage_2_scope_extension', 'surveillance_1_scope_extension', 'surveillance_2_scope_extension', 'reaudit_scope_extension', 'stage_2_special_transition', 'surveillance_1_special_transition', 'surveillance_2_special_transition', 'reaudit_special_transition'];

        if (!empty($standard_number) && in_array($audit_type, $audit_type_array)) { //true

            if (!empty($company->companyStandards)) {

                foreach ($company->companyStandards as $standard) {
                    $company_standards[] = $standard->standard_id;
                }

                $auditor_standards = AuditorStandard::whereIn('standard_id', $company_standards)->get();


                if ($auditor_standards->isNotEmpty()) {

                    foreach ($auditor_standards as $auditor_standard) {

                        foreach ($auditor_standard->auditorStandardGrades()->where('status', '!=', 'withdrawn')->get() as $standard_grade) {
                            $members[] = $auditor_standard;

                            $standard_grades[] = $standard_grade;
                        }

                    }
                    //checking the stage1 and stage2 combine aj
                    if ($audit_type == 'stage_1|stage_2') {

                        $combine_tracker = 1;
                    } else {
                        $combine_tracker = 0;
                    }

                    //check the company already have any aj
                    $aj_obj = AuditJustification::where('company_id', $company->id)->first();
                    $aj_standard_change = AjStageChange::where('aj_standard_stage_id', $aj_standard_id)->first();
                    if (!empty($aj_obj)) {

                        //check which standards are in company aj
                        $aj_standards_obj = AJStandard::where('audit_justification_id', $aj_obj->id)
                            ->where('standard_id', $standard_number->id)
                            ->first();
                        $team_members_stage1 = [];
                        if ($audit_type == 'stage_2') {
                            $aj_standard_stages_obj_stage1 = AJStandardStage::where('audit_type', 'stage_1')->where('aj_standard_id', $aj_standards_obj->id)->first();
                            $team_members_stage1 = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stages_obj_stage1->id)->get();
                        }
                        if (!empty($aj_standards_obj)) {

                            if ($combine_tracker == 1) {
                                //get the standard stage stage_1
                                $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', 'stage_1')->first();

                            } else {
                                $aj_standard_stages_obj = $aj_standards_obj->ajStandardStages->where('audit_type', $audit_type)->first();

                            }

                            if (!empty($aj_standard_stages_obj)) {

                                //now we want the single audit stage which want to edit
                                $aj_standard_stage = $aj_standard_stages_obj;
                                $team_members = AJStageTeam::where('aj_standard_stage_id', $aj_standard_stage->id)->get();
                                $aj_remarks = $aj_standard_stage->AJRemarks()->get();
                                //also need all other audit stages for manday editing
                                $aj_stages_manday[] = $aj_standard_stage;
                                $audit_type = $aj_standard_stage->audit_type;
                                $team_audit_type = $aj_standard_stage->audit_type;


                                //return to the edit page
                                $ims = 0;
                                $certificateStatus = Certificate::where('company_standard_id', $c_std->id)->orderBy('id', 'desc')->first();
                                $notifications = Notification::where('type_id', (int)$standard_number->id)->where('request_id', (int)$aj_standard_id)->where('type', 'audit_justification')->get();
                                $checkApproved = AJStandardStage::where('audit_type', $audit_type)->where('id', $aj_standard_id)->first();
                                if ($checkApproved->status == 'approved') {
                                    $approvalPerson = Notification::where('request_id', (int)$aj_standard_id)->where('status', 'approved')->where('type', 'audit_justification')->first();
                                    $pdf = \Barryvdh\DomPDF\Facade::loadView('audit-justification.AjPrintPreview.index', compact('approvalPerson', 'certificateStatus', 'notifications', 'aj_standard_change', 'standard_number', 'team_members_stage1', 'aj_remarks', 'team_members', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'))->setPaper('A4', 'Landscape');
                                    Storage::put('public/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf', $pdf->output());
                                    $href = 'storage/uploads/print/' . (int)$standard_number->id . '/' . (int)$aj_standard_id . '.pdf';

                                } else {
                                    $href = '#';
                                }
                                return view('audit-justification.ims.special-audit.show', compact('href', 'notifications', 'aj_standard_change', 'team_audit_type', 'team_members', 'aj_remarks', 'standard_number', 'company', 'role', 'users', 'audit_type', 'auditor_grades', 'aj_standard_stage', 'aj_stages_manday', 'members', 'standard_grades', 'combine_tracker', 'ims'));

                            } else {
                                return back()->with('flash_status', 'warning')
                                    ->with('flash_message', 'Stage nto Exist for this standard');
                            }
                        }
                    }

                }

            } else {
                return back()->with('flash_status', 'warning')
                    ->with('flash_message', 'Auditor standard not available');
            }
        } else {
            return back()->with('flash_status', 'warning')
                ->with('flash_message', 'Company does not have this standard');
        }
    }

    public function updateSpecialAJ(Request $request)
    {


        $aj_date = $request->aj_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->aj_date);

        $checkTeamMember = $this->checkTeam($request);
        if ($checkTeamMember['status'] == "warning") {
            $response = (new ApiMessageController())->successResponse($checkTeamMember, $checkTeamMember['message']);
            return $response;
        }

        $validator = Validator::make($request->all(), [
            'standard.*' => 'required',
            'certificate_scope' => 'string',
            'onsite' => 'required',
            'offsite' => 'required',
            'job_number' => 'required|string',
            'manday_remark' => 'string|sometimes',
            'supervisor_involved.stage1' => 'required|string',
            'std_number' => Rule::requiredIf(function () use ($request) {
                return $request['ch_std'] == "yes";
            }),
            'expected_date' => Rule::requiredIf(function () use ($request) {
                if ($request['supervisor_involved']['stage1'] == "yes") {
                    return true;
                }
            }),
            'auditor_type.*' => 'required|string',
        ],
            [
                'standard.*.required' => 'Company standards not found',
                'auditor_type.*.required' => 'Please select the auditor type',
                'perposedTeam.*.required' => 'Please select the auditor member',
            ]);


        if (!$validator->fails()) {
            //for single stage
            if (strpos($request['audit_type'], '|') !== false) {
                $stages = explode('|', $request->audit_type);
            } else {
                $stages = [$request['audit_type']];
            }

            if (!empty($request->perposedTeam)) {
                if (!empty($request->perposedTeam)) {

                    $mandayStage = AJStandardStage::findOrFail((int)$request->standard_stage_id);
                    $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
                    $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();

                    if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                        foreach ($activeImsStandards as $key => $standard) {
                            $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                            if (!is_null($ajStandard)) {


                                if ($request->status == 'created') {
                                    $status = $request->status;
                                } elseif ($request->status == 'approved_scheme') {
                                    $status = 'approved';
                                } elseif ($request->status == 'approved' || $request->status == 'rejected') {
                                    $status = 'applied';
                                } else {
                                    $status = '';
                                }

                                $expectedDate = $request->expected_date[0] == null ? null : \DateTime::createFromFormat('d-m-Y', $request->expected_date[0]);
                                $approvalDate = $request->approval_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->approval_date);
                                $actual_expected_date = $request->actual_expected_date == null ? null : \DateTime::createFromFormat('d-m-Y', $request->actual_expected_date);


                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->first();
                                if ($request->status == 'approved_scheme' && $approvalDate != null && $standard_stage->approval_date != $approvalDate) {
                                    $lastApprovalDate = $standard_stage->approval_date;
                                } else {
                                    $lastApprovalDate = $standard_stage->last_approval_date;
                                }
                                if (!is_null($standard_stage)) {
                                    $standard_stage->update([

                                        'certificate_scope' => $request->certificate_scope,
                                        'manday_remarks' => $request->manday_remarks,
                                        'excepted_date' => $expectedDate,
                                        'supervisor_involved' => $request->supervisor_involved['stage1'],
                                        'evaluator_name' => $request->evaluator_name,
                                        'special_aj_remarks' => $request->special_aj_remarks,
                                        'approval_date' => $approvalDate,
                                        'last_approval_date' => $lastApprovalDate,
                                        'actual_expected_date' => $actual_expected_date,
                                        'status' => $status,
                                        'user_id' => ($request->status == 'approved' || $request->status == 'rejected') ? Auth()->user()->id : NULL,
                                        'job_number' => $request->job_number,
                                        'onsite' => $request->onsite,
                                        'offsite' => $request->offsite,
                                    ]);


                                    if ($request['audit_type'] === 'surveillance_1_special_transition' || $request['audit_type'] === 'surveillance_2_special_transition' || $request['audit_type'] === 'surveillance_3_special_transition' || $request['audit_type'] === 'surveillance_4_special_transition' || $request['audit_type'] === 'surveillance_5_special_transition' || $request['audit_type'] === 'reaudit_special_transition'
                                        || $request['audit_type'] === 'surveillance_1_scope_extension' || $request['audit_type'] === 'surveillance_2_scope_extension' || $request['audit_type'] === 'surveillance_3_scope_extension' || $request['audit_type'] === 'surveillance_4_scope_extension' || $request['audit_type'] === 'surveillance_5_scope_extension' || $request['audit_type'] === 'reaudit_scope_extension'
                                    ) {
                                        $standard_stage->ajStageChange()->updateOrCreate([
                                            'name' => $request->ch_name,
                                            'scope' => $request->ch_scp,
                                            'location' => $request->ch_loc,
                                            'version' => $request->ch_std,
                                            'standards' => $request->std_number,
                                            'remarks' => $request->remarks,
                                        ]);
                                    }

                                    foreach ($standard_stage->ajStageTeams as $ajStageTeam) {
                                        $ajStageTeam->auditor->update([
                                            'is_available' => 1
                                        ]);
                                    }

                                    $standard_stage->ajStageTeams()->delete();


                                    if (!empty($request->perposedTeam)) {
                                        $supervisor = 0;
                                        if ($request['supervisor_involved']['stage1'] == "yes") {
                                            $supervisor = 1;
                                        }
                                        foreach ($request->perposedTeam as $key1 => $a_member) {
                                            $standard_stage->ajStageTeams()->create([
                                                'auditor_id' => (int)$a_member,
                                                'supervisor_involved' => $supervisor,
                                                'grade_id' => (int)$request->perposed_team_grade_id[$key1],
                                                'member_name' => $request->perposed_team_name[$key1],
                                                'member_type' => $request->perposed_team_type[$key1],
                                            ]);

                                            $auditor = Auditor::find((int)$a_member)->update([
                                                'is_available' => 0
                                            ]);


                                        }
                                    }
                                }
                            }
                        }
                    }
                    $standard_stage = AJStandardStage::findOrFail((int)$request->standard_stage_id);
                    $data = [
                        'aj_id' => $standard_stage,
                        'status' => 'success',
                    ];

                    $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated');

                    return $response;
                }


            } else {
                $data = [
                    'flash_status' => 'warning',
                    'flash_message' => 'Team not available for this standard'
                ];
                $response = $data;
                return $response;
            }
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }

    public function updateSpecialAJRemarks(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'remark' => 'required|string',
            'aj_id.*' => 'required'
        ]);


        if (!$validator->fails()) {
            $get_company = Company::where('id', $request->company_id)->first();
            foreach ($request['aj_id'] as $aj_id) {

                $aj = AJStandardStage::with('ajStandard.standard.scheme_manager.user')->where('id', $aj_id)->first();
                $aj->status = $request->status;
                $aj->save();

                $aj->AJRemarks()->create([
                    'model' => AJStandardStage::class,
                    'user_id' => Auth()->user()->id,
                    'remarks' => $request['remark']
                ]);
            }

            $sentMessage = '';
            $companyStandard = CompanyStandards::where('company_id', $request->company_id)->where('standard_id', $aj->ajStandard->standard->id)->first();
            $imsCompanyStandards = $get_company->companyStandards()->where('is_ims', true)->get();
            $ims_heading = $companyStandard->standard->name;
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    if ($companyStandard->id == $imsCompanyStandard->id) {
                        continue;
                    } else {
                        $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                    }
                }
                if ($request->status == 'applied') {
                    $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(New)';
                } elseif ($request->status == 'approved') {
                    $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Approved)';
                } elseif ($request->status == 'resent') {
                    $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Re-Submitted)';
                } elseif ($request->status == 'rejected') {
                    $sentMessage = '' . $get_company->name . ' - ' . $ims_heading . '(Rejected)';
                }

            } else {
                if ($request->status == 'applied') {
                    $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(New)';
                } elseif ($request->status == 'approved') {
                    $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Approved)';
                } elseif ($request->status == 'resent') {
                    $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Re-Submitted)';
                } elseif ($request->status == 'rejected') {
                    $sentMessage = '' . $get_company->name . ' - ' . $aj->ajStandard->standard->name . '(Rejected)';
                }

            }


            if (auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'scheme_coordinator') {
                if ($request->status == 'applied') {
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/special-ims/audit-justification/edit/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'unapproved',
                        'sent_message' => $sentMessage

                    ]);
                } elseif ($request->status == 'resent') {
                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();
                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $aj->ajStandard->standard->scheme_manager[0]->user->id,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/special-ims/audit-justification/edit/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'resent',
                        'sent_message' => $sentMessage

                    ]);
                    if (!is_null($notification)) {
                        $notification->is_read = true;
                        $notification->save();
                    }

                }


            } else if (auth()->user()->user_type == 'scheme_manager') {
                if ($request->status == 'rejected') {

                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();

                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $notification->sent_by,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/special-ims/audit-justification/edit/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'rejected',
                        'sent_message' => $sentMessage

                    ]);

                    $notification->is_read = true;
                    $notification->save();
                } elseif ($request->status == 'approved') {

                    $notification = Notification::where('type_id', (int)$aj->ajStandard->standard->id)->where('request_id', (int)$aj->id)->where('is_read', false)->where('type', 'audit_justification')->first();


                    Notification::create([
                        'type' => 'audit_justification',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $notification->sent_by,
                        'icon' => 'message',
                        'body' => $request['remark'],
                        'url' => '/special-ims/audit-justification/edit/' . $request->company_id . '/' . $aj->audit_type . '/' . $aj->ajStandard->standard->name . '/' . $aj->id,
                        'is_read' => 0,
                        'type_id' => $aj->ajStandard->standard->id,
                        'request_id' => $aj->id,
                        'status' => 'approved',
                        'sent_message' => $sentMessage

                    ]);
                    $notification->is_read = true;
                    $notification->save();
                }


            }

            $data = [
                'status' => 'success'
            ];

            $response = (new ApiMessageController())->successResponse($data, 'AJ has been updated and notification has been sent.');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function deleteSpecialAJ($company_id = null, $audit_type = null, $standard_number = null, $aj_standard_id = null, $standard_stage_id = null, $ims = null)
    {

        $standard_stage = AJStandardStage::find((int)$aj_standard_id);

        $notifications = Notification::where('request_id', $standard_stage->id)->get();
        if (!empty($notifications) && count($notifications) > 0) {
            foreach ($notifications as $notification) {
                $notification->delete();
            }
        }
        $standard_stage->ajStageChange()->delete();
        $standard_stage->AJRemarks()->delete();
        $standard_stage->ajStageTeams()->delete();
        $standard_stage->delete();

        return back()->with(['flash_status' => 'success', 'flash_message' => 'Special AJ has been deleted successfully']);
    }

    private function in_array_any($needles, $haystack)
    {
        return (bool)array_intersect($needles, $haystack);
        // echo in_array_any( array(3,9), array(5,8,3,1,2) ); // true, since 3 is present
        // echo in_array_any( array(4,9), array(5,8,3,1,2) ); // false, neither 4 nor 9 is present
    }

    private function checkTeam($request)
    {
        if (!empty($request->perposed_team_grade_id) && count($request->perposed_team_grade_id) > 0) {
            $numArray = array_map('intval', $request->perposed_team_grade_id);
            if (in_array(1, $numArray) && in_array(7, $numArray)) {
                //both of them are in $arg

                $standardFamilyCheck = Standards::where('id', $request->standard_number)->first();
                if ($standardFamilyCheck->standards_family_id == 20) {
                    //Energy
                    $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardEnergyCodes = AuditorStandardEnergyCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardEnergyCodes, $companyStandardEnergyCodes)) {
                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } elseif ($standardFamilyCheck->standards_family_id == 23) {
                    //Food
                    $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardFoodCodes = AuditorStandardFoodCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardFoodCodes, $companyStandardFoodCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];
                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }
                } else {
                    //Generic
                    $companyStandardCodes = CompanyStandardCode::where('company_id', $request->company_id)->where('standard_id', $request->standard_number)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    $auditorsArray = array_map('intval', $request->perposedTeam);
                    $auditors = Auditor::whereIn('id', $auditorsArray)->pluck('id')->toArray();
                    $auditorStandardId = AuditorStandard::whereIn('auditor_id', $auditors)->where('standard_id', $request->standard_number)->whereHas('auditorStandardGrades', function ($q) {
                        $q->where('grade_id', 7);
                    })->whereNull('deleted_at')->pluck('id')->toArray();
                    $auditorStandardCodes = AuditorStandardCode::whereIn('auditor_standard_id', $auditorStandardId)->whereNull('deleted_at')->pluck('unique_code')->toArray();
                    if ($this->in_array_any($auditorStandardCodes, $companyStandardCodes)) {

                        $data = [
                            'status' => 'success',
                            'message' => 'Team Member Found'
                        ];

                        $response = $data;
                        return $response;
                    } else {
                        $data = [
                            'status' => 'warning',
                            'message' => 'TE does not have all Company Codes'
                        ];

                        $response = $data;
                        return $response;

                    }

                }
            } else {
                $data = [
                    'status' => 'warning',
                    'message' => 'For Audit LA and TE is required.'
                ];

                $response = $data;
                return $response;
            }


        } else {
            $data = [
                'status' => 'warning',
                'message' => 'Please Select Team Members for Audit.'
            ];

            $response = $data;
            return $response;
        }
    }
}
