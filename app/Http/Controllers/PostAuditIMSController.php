<?php

namespace App\Http\Controllers;

use App\AjStageChange;
use App\AJStandard;
use App\AJStandardStage;
use App\Certificate;
use App\Company;
use App\CertificateFile;
use App\CompanyStandards;
use App\Mail\DraftMail;
use App\Mail\OutSourceRequest\OutsourceRequesTaskMail;
use App\Notification;
use App\PostAudit;
use App\PostAuditQueestion;
use App\PostAuditQuestionFile;
use App\Question;
use App\SchemeInfoPostAudit;
use App\Standards;
use App\Traits\GeneralHelperTrait;
use App\Traits\MandayTrait;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\True_;
use function foo\func;

class PostAuditIMSController extends Controller
{
    use MandayTrait, GeneralHelperTrait;

    public function index($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::where('id', $company_id)->first();
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();

        $companyStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }


        $questions = Question::limit(3)->get(['id', 'title_for_oc_om']);


        $checkAuditType = str_replace(' ', '_', lcfirst($audit_type));
        $checkAuditType = $checkAuditType . '_verification';


        if ($audit_type == 'Stage 1') {
            $approvedVerificationCount = 0;
        } elseif (preg_match('/Scope-Extension/', $audit_type) || preg_match('/Special-Transition/', $audit_type)) {
            $approvedVerificationCount = 1;
        } else {
            $approvedVerificationCount = AJStandardStage::where('aj_standard_id', $standardStage->aj_standard_id)->where('status', 'approved')->where('audit_type', $checkAuditType)->count();
        }
        return view('post-audit.ims.index', compact('company', 'standard', 'standardStage', 'audit_type', 'questions', 'approvedVerificationCount', 'ims_heading'));
    }

    public function store(Request $request)
    {


        $q3Files = $request->file('q3_files');
        ini_set('memory_limit', '-1');
        $actualAuditDatefrom = $request->actual_audit_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_audit_date, '0', '10'));
        $actualAuditDateTo = $request->actual_audit_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_audit_date, '13'));

        $mandayStage = AJStandardStage::findOrFail((int)$request->aj_standard_stage_id);
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();


        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
            foreach ($activeImsStandards as $key => $standard) {


                if ($key == 0) {

                    $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                    if (!is_null($ajStandard)) {
                        if (!is_null($mandayStage->recycle)) {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                        } else {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                        }
                        if (!is_null($standard_stage)) {
                            $postAudit = new PostAudit();

                            $postAudit->company_id = $request->company_id;
                            $postAudit->standard_id = $standard;
                            $postAudit->aj_standard_stage_id = $standard_stage->id;
                            $postAudit->actual_audit_date = $actualAuditDatefrom->format('Y-m-d');
                            $postAudit->actual_audit_date_to = $actualAuditDateTo->format('Y-m-d');
                            $postAudit->receiver_id = $request->receiver_id;
                            $postAudit->sender_id = auth()->user()->id;

                            if ($request->status == 'save') {
                                if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'scheme_coordinator') {
//                                    $postAudit->status = 'approved';
                                    $postAudit->status = $request->status;
                                } else {
                                    $postAudit->status = $request->status;
                                }
                            } else {
                                $postAudit->status = $request->status;
                            }


                            if ($request->q4_name === 'NO') {
                                $postAudit->is_q4 = 'NO';
                            } else {
                                $postAudit->is_q4 = 'YES';
                                if ($request->hasfile('q4_files')) {

                                    $postAudit->is_q4 = 'YES';
                                    foreach ($request->file('q4_files') as $image) {
                                        $name = $image->getClientOriginalName();
                                        $explode_name = explode('.', $name)[0];
                                        $extension = $image->getClientOriginalExtension();
                                        $final_name = $explode_name . '_' . time() . '.' . $extension;
                                        $destinationPath = public_path('/uploads/post-audit/communication/' . $postAudit->id . '/');
                                        $image->move($destinationPath, $final_name);
                                        $postAudit->q4_file = $final_name;

                                    }
                                }
                            }
                            $postAudit->save();
//
                            if (isset($request->q1_name)) {
                                if ($request->q1_name == 'YES') {
                                    $postAuditQuestion1 = new PostAuditQueestion();
                                    $postAuditQuestion1->post_audit_id = $postAudit->id;
                                    $postAuditQuestion1->question_id = $request->questions_one_id;
                                    $postAuditQuestion1->value = $request->q1_name;
                                    $postAuditQuestion1->save();

                                    if ($request->hasfile('q1_files')) {


                                        foreach ($request->file('q1_files') as $image) {
                                            $name = $image->getClientOriginalName();
                                            $explode_name = explode('.', $name)[0];
                                            $extension = $image->getClientOriginalExtension();
                                            $final_name = $explode_name . '_' . time() . '.' . $extension;
                                            $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion1->id . '/');
                                            $image->move($destinationPath, $final_name);
                                            $postAuditQuestion1File = new PostAuditQuestionFile();
                                            $postAuditQuestion1File->post_audit_question_id = $postAuditQuestion1->id;
                                            $postAuditQuestion1File->file_name = $explode_name;
                                            $postAuditQuestion1File->file_path = $final_name;
                                            $postAuditQuestion1File->save();

                                        }
                                    }


//                                foreach ($request->question_ones_filename as $key => $q1File) {
//                                    $postAuditQuestion1File = new PostAuditQuestionFile();
//                                    $postAuditQuestion1File->post_audit_question_id = $postAuditQuestion1->id;
//                                    $postAuditQuestion1File->file_name = $q1File;
//                                    $postAuditQuestion1File->file_path = $request->question_ones_file[$key];
//                                    $postAuditQuestion1File->save();
//                                }
                                } else {
                                    $postAuditQuestion1 = new PostAuditQueestion();
                                    $postAuditQuestion1->post_audit_id = $postAudit->id;
                                    $postAuditQuestion1->question_id = $request->questions_one_id;
                                    $postAuditQuestion1->value = $request->q1_name;
                                    $postAuditQuestion1->save();
                                }
                            }

                            if (isset($request->q2_name)) {
                                if ($request->q2_name == 'YES') {
                                    $postAuditQuestion2 = new PostAuditQueestion();
                                    $postAuditQuestion2->post_audit_id = $postAudit->id;
                                    $postAuditQuestion2->question_id = $request->questions_two_id;
                                    $postAuditQuestion2->value = $request->q2_name;
                                    $postAuditQuestion2->save();
//                                foreach ($request->question_two_filename as $key => $q2File) {
//                                    $postAuditQuestion2File = new PostAuditQuestionFile();
//                                    $postAuditQuestion2File->post_audit_question_id = $postAuditQuestion2->id;
//                                    $postAuditQuestion2File->file_name = $q2File;
//                                    $postAuditQuestion2File->file_path = $request->question_two_file[$key];
//                                    $postAuditQuestion2File->save();
//                                }

                                    if ($request->hasfile('q2_files')) {


                                        foreach ($request->file('q2_files') as $image) {
                                            $name = $image->getClientOriginalName();
                                            $explode_name = explode('.', $name)[0];
                                            $extension = $image->getClientOriginalExtension();
                                            $final_name = $explode_name . '_' . time() . '.' . $extension;
                                            $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion2->id . '/');
                                            $image->move($destinationPath, $final_name);
                                            $postAuditQuestion2File = new PostAuditQuestionFile();
                                            $postAuditQuestion2File->post_audit_question_id = $postAuditQuestion2->id;
                                            $postAuditQuestion2File->file_name = $explode_name;
                                            $postAuditQuestion2File->file_path = $final_name;
                                            $postAuditQuestion2File->save();

                                        }
                                    }
                                } else {
                                    $postAuditQuestion2 = new PostAuditQueestion();
                                    $postAuditQuestion2->post_audit_id = $postAudit->id;
                                    $postAuditQuestion2->question_id = $request->questions_two_id;
                                    $postAuditQuestion2->value = $request->q2_name;
                                    $postAuditQuestion2->save();
                                }
                            }


                            if (isset($request->q3_name)) {
                                if ($request->q3_name == 'YES') {
                                    $postAuditQuestion3 = new PostAuditQueestion();
                                    $postAuditQuestion3->post_audit_id = $postAudit->id;
                                    $postAuditQuestion3->question_id = $request->questions_three_id;
                                    $postAuditQuestion3->value = $request->q3_name;
                                    $postAuditQuestion3->save();
//                                foreach ($request->question_three_filename as $key => $q3File) {
//                                    $postAuditQuestion3File = new PostAuditQuestionFile();
//                                    $postAuditQuestion3File->post_audit_question_id = $postAuditQuestion3->id;
//                                    $postAuditQuestion3File->file_name = $q3File;
//                                    $postAuditQuestion3File->file_path = $request->question_three_file[$key];
//                                    $postAuditQuestion3File->save();
//                                }
                                    if ($request->hasfile('q3_files')) {


                                        foreach ($request->q3_files as $image) {
                                            if ($key == 1) {
//                                            $name = $image->getClientOriginalName();
//                                            $explode_name = explode('.', $name)[0];
//                                            $extension = $image->getClientOriginalExtension();
//                                            $final_name1 = $explode_name . '_' . time() . '.' . $extension;
//                                            $destinationPath1 = public_path('/uploads/post-audit-ims-new/' . $postAuditQuestion3->id . '' . rand() . '/');
//                                            $image->move($destinationPath1, $final_name1);
                                            } else {
                                                $name = $image->getClientOriginalName();

                                                $explode_name = explode('.', $name)[0];
                                                $extension = $image->getClientOriginalExtension();
                                                $final_name = $explode_name . '_' . time() . '.' . $extension;
                                                $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion3->id . '/');
                                                $image->move($destinationPath, $final_name);
                                                $postAuditQuestion3File = new PostAuditQuestionFile();
                                                $postAuditQuestion3File->post_audit_question_id = $postAuditQuestion3->id;
                                                $postAuditQuestion3File->file_name = $explode_name;
                                                $postAuditQuestion3File->file_path = $final_name;
                                                $postAuditQuestion3File->save();
                                            }


                                            sleep('2');

                                        }

                                    }
                                } else {
                                    $postAuditQuestion3 = new PostAuditQueestion();
                                    $postAuditQuestion3->post_audit_id = $postAudit->id;
                                    $postAuditQuestion3->question_id = $request->questions_three_id;
                                    $postAuditQuestion3->value = $request->q3_name;
                                    $postAuditQuestion3->save();
                                }
                            }


                        }
                    }
                }


            }

        }


        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'scheme_coordinator') {


        } else {


            $standard = Standards::where('id', $request->standard_id)->first();
            $company = Company::where('id', $request->company_id)->first();
            if ($request->status == 'unapproved') {
                //add a notification for manager
                Notification::create([
                    'type' => 'post_audit',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $request->receiver_id,
                    'icon' => 'message',
                    'body' => $request->remarks,
                    'url' => '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                    'is_read' => 0,
                    'type_id' => $request->aj_standard_stage_id,
                    'request_id' => $postAudit->id,
                    'status' => 'unapproved',
                    'sent_message' => 'New for ' . $request->ims_heading . ' - ' . $company->name

                ]);
            }


        }

        if ($request->status == 'save') {
            return redirect()->route('company.show', $request->company_id)->with([
                'flash_status' => 'success',
                'flash_message' => 'Post Audit successfully.'
            ]);
        } else {
            return redirect()->route('company.show', $request->company_id)->with([
                'flash_status' => 'success',
                'flash_message' => 'Post Audit updated and successfully send to Concerned Person.'
            ]);

        }


    }

    public function edit($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::where('id', $company_id)->first();
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();
        $questions = Question::limit(3)->get(['id', 'title_for_oc_om']);


        $companyStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }


        $checkAuditType = str_replace(' ', '_', lcfirst($audit_type));
        $checkAuditType = $checkAuditType . '_verification';

        if ($audit_type == 'Stage 1') {
            $approvedVerificationCount = 0;
        } elseif (preg_match('/Scope-Extension/', $audit_type) || preg_match('/Special-Transition/', $audit_type)) {
            $approvedVerificationCount = 1;
        } else {
            $approvedVerificationCount = AJStandardStage::where('aj_standard_id', $standardStage->aj_standard_id)->where('status', 'approved')->where('audit_type', $checkAuditType)->count();
        }


        $postAudit = PostAudit::where('company_id', $company_id)->where('standard_id', $standard->id)->where('aj_standard_stage_id', $standard_stage_id)->first();


        $postAuditQuestionsOne = PostAuditQueestion::where('question_id', $questions[0]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsTwo = PostAuditQueestion::where('question_id', $questions[1]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsThree = PostAuditQueestion::where('question_id', $questions[2]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();

        $notifications = Notification::where('type_id', (int)$standardStage->id)->where('request_id', (int)$postAudit->id)->where('type', 'post_audit')->get();

        return view('post-audit.ims.edit', compact('ims_heading', 'notifications', 'company', 'standard', 'standardStage', 'audit_type', 'questions', 'postAudit', 'postAuditQuestionsOne', 'postAuditQuestionsTwo', 'postAuditQuestionsThree', 'approvedVerificationCount'));
    }

    public function update(Request $request)
    {

//        dd($request->all());
        $postAudit1 = PostAudit::where('id', $request->post_audit_id)->first();
        $company = Company::where('id', $request->company_id)->first();
        $standard = Standards::where('id', $request->standard_id)->first();

        $actualAuditDatefrom = $request->actual_audit_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_audit_date, '0', '10'));
        $actualAuditDateTo = $request->actual_audit_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_audit_date, '13'));
//        $actualAuditDate = date("Y/m/d", strtotime($request->actual_audit_date));

        $mandayStage = AJStandardStage::findOrFail((int)$request->aj_standard_stage_id);
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();


        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
            foreach ($activeImsStandards as $key => $standard) {
                if ($key == 0) {
                    $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                    if (!is_null($ajStandard)) {
                        if (!is_null($mandayStage->recycle)) {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                        } else {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                        }

                        if (!is_null($standard_stage)) {
                            $postAudit = PostAudit::where('company_id', $request->company_id)->where('standard_id', $standard)->where('aj_standard_stage_id', $standard_stage->id)->first();
//                            $postAudit->company_id = $request->company_id;
//                            $postAudit->standard_id = $standard;
                            $postAudit->aj_standard_stage_id = $standard_stage->id;
                            $postAudit->actual_audit_date = $actualAuditDatefrom->format('Y-m-d');
                            $postAudit->actual_audit_date_to = $actualAuditDateTo->format('Y-m-d');
                            $postAudit->receiver_id = $request->receiver_id;
                            $postAudit->sender_id = auth()->user()->id;
                            if ($request->q4_name === 'NO') {
                                $postAudit->is_q4 = 'NO';
                            } else {
                                $postAudit->is_q4 = 'YES';
                                if ($request->hasfile('q4_files')) {

                                    $filename = public_path('/uploads/post-audit/communication/' . $postAudit->id . '/' . $postAudit->q4_file);
//                                    $filename = public_path('uploads/post-audit') . $postAudit->q4_file;

                                    if (File::exists($filename)) {
                                        File::delete($filename);  // or unlink($filename);
                                    }
                                    foreach ($request->file('q4_files') as $image) {
                                        $name = $image->getClientOriginalName();
                                        $explode_name = explode('.', $name)[0];
                                        $extension = $image->getClientOriginalExtension();
                                        $final_name = $explode_name . '_' . time() . '.' . $extension;
                                        $destinationPath = public_path('/uploads/post-audit/communication/' . $postAudit->id . '/');
                                        $image->move($destinationPath, $final_name);
                                        $postAudit->q4_file = $final_name;

                                    }
                                }
                            }

                            if ($request->status == 'save') {
                                if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'scheme_coordinator') {
//                                    $postAudit->status = 'approved';
                                    $postAudit->status = $request->status;
                                } else {
                                    $postAudit->status = $request->status;
                                }
                            } else {
                                $postAudit->status = $request->status;
                            }

                            $postAudit->save();


                            if (isset($request->q1_name)) {
                                if ($request->q1_name == 'YES') {
                                    $postAuditQuestion1 = PostAuditQueestion::where('id', $request->post_audit_question_one_id)->first();
                                    $postAuditQuestion1->post_audit_id = $postAudit->id;
                                    $postAuditQuestion1->question_id = $request->questions_one_id;
                                    $postAuditQuestion1->value = $request->q1_name;
                                    $postAuditQuestion1->save();

//                                $postAuditQuestion1->postAuditQuestionFile()->delete();
//                                foreach ($request->question_ones_filename as $key => $q1File) {
//                                    $postAuditQuestion1File = new PostAuditQuestionFile();
//                                    $postAuditQuestion1File->post_audit_question_id = $postAuditQuestion1->id;
//                                    $postAuditQuestion1File->file_name = $q1File;
//                                    $postAuditQuestion1File->file_path = $request->question_ones_file[$key];
//                                    $postAuditQuestion1File->save();
//                                }

//                                    if (isset($request->question_one_file_id) && !empty($request->question_one_file_id) && count($request->question_one_file_id) > 0) {
//
//
//                                        $questionFiles = PostAuditQuestionFile::where('post_audit_question_id', $postAuditQuestion1->id)->get();
//                                        if (!empty($questionFiles) && count($questionFiles) > 0) {
//                                            foreach ($request->question_one_file_id as $question_one_file_id) {
//                                                foreach ($questionFiles as $file) {
//                                                    if ($file->id == $question_one_file_id) {
//                                                    } else {
//                                                        PostAuditQuestionFile::where('id', $file->id)->delete();
//
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }


                                    if ($request->hasfile('q1_files')) {

//                    $postAuditQuestion1->postAuditQuestionFile()->delete();
                                        foreach ($request->file('q1_files') as $image) {
                                            $name = $image->getClientOriginalName();
                                            $explode_name = explode('.', $name)[0];
                                            $extension = $image->getClientOriginalExtension();
                                            $final_name = $explode_name . '_' . time() . '.' . $extension;
                                            $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion1->id . '/');
                                            $image->move($destinationPath, $final_name);
                                            $postAuditQuestion1File = new PostAuditQuestionFile();
                                            $postAuditQuestion1File->post_audit_question_id = $postAuditQuestion1->id;
                                            $postAuditQuestion1File->file_name = $explode_name;
                                            $postAuditQuestion1File->file_path = $final_name;
                                            $postAuditQuestion1File->save();

                                        }
                                    }


                                } else {
                                    $postAuditQuestion1 = PostAuditQueestion::where('id', $request->post_audit_question_one_id)->first();
                                    $postAuditQuestion1->post_audit_id = $postAudit->id;
                                    $postAuditQuestion1->question_id = $request->questions_one_id;
                                    $postAuditQuestion1->value = $request->q1_name;
                                    $postAuditQuestion1->save();
                                }
                            }


                            if (isset($request->q2_name)) {
                                if ($request->q2_name == 'YES') {
                                    $postAuditQuestion2 = PostAuditQueestion::where('id', $request->post_audit_question_two_id)->first();
                                    $postAuditQuestion2->post_audit_id = $postAudit->id;
                                    $postAuditQuestion2->question_id = $request->questions_two_id;
                                    $postAuditQuestion2->value = $request->q2_name;
                                    $postAuditQuestion2->save();

//                                $postAuditQuestion2->postAuditQuestionFile()->delete();
//                                foreach ($request->question_two_filename as $key => $q2File) {
//                                    $postAuditQuestion2File = new PostAuditQuestionFile();
//                                    $postAuditQuestion2File->post_audit_question_id = $postAuditQuestion2->id;
//                                    $postAuditQuestion2File->file_name = $q2File;
//                                    $postAuditQuestion2File->file_path = $request->question_two_file[$key];
//                                    $postAuditQuestion2File->save();
//                                }

//                                    if (isset($request->question_two_file_id) && !empty($request->question_two_file_id) && count($request->question_two_file_id) > 0) {
//
//
//                                        $questionFiles = PostAuditQuestionFile::where('post_audit_question_id', $postAuditQuestion2->id)->get();
//                                        if (!empty($questionFiles) && count($questionFiles) > 0) {
//                                            foreach ($request->question_two_file_id as $question_two_file_id) {
//                                                foreach ($questionFiles as $file) {
//                                                    if ($file->id == $question_two_file_id) {
//                                                    } else {
//                                                        PostAuditQuestionFile::where('id', $file->id)->delete();
//
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
                                    if ($request->hasfile('q2_files')) {


                                        foreach ($request->file('q2_files') as $image) {
                                            $name = $image->getClientOriginalName();
                                            $explode_name = explode('.', $name)[0];
                                            $extension = $image->getClientOriginalExtension();
                                            $final_name = $explode_name . '_' . time() . '.' . $extension;
                                            $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion2->id . '/');
                                            $image->move($destinationPath, $final_name);
                                            $postAuditQuestion2File = new PostAuditQuestionFile();
                                            $postAuditQuestion2File->post_audit_question_id = $postAuditQuestion2->id;
                                            $postAuditQuestion2File->file_name = $explode_name;
                                            $postAuditQuestion2File->file_path = $final_name;
                                            $postAuditQuestion2File->save();

                                        }
                                    }

                                } else {
                                    $postAuditQuestion2 = PostAuditQueestion::where('id', $request->post_audit_question_two_id)->first();
                                    $postAuditQuestion2->post_audit_id = $postAudit->id;
                                    $postAuditQuestion2->question_id = $request->questions_two_id;
                                    $postAuditQuestion2->value = $request->q2_name;
                                    $postAuditQuestion2->save();
                                }
                            }


                            if (isset($request->q3_name)) {
                                if ($request->q3_name == 'YES') {
                                    $postAuditQuestion3 = PostAuditQueestion::where('id', $request->post_audit_question_three_id)->first();
                                    $postAuditQuestion3->post_audit_id = $postAudit->id;
                                    $postAuditQuestion3->question_id = $request->questions_three_id;
                                    $postAuditQuestion3->value = $request->q3_name;
                                    $postAuditQuestion3->save();

//                                $postAuditQuestion3->postAuditQuestionFile()->delete();
//                                foreach ($request->question_three_filename as $key => $q3File) {
//                                    $postAuditQuestion3File = new PostAuditQuestionFile();
//                                    $postAuditQuestion3File->post_audit_question_id = $postAuditQuestion3->id;
//                                    $postAuditQuestion3File->file_name = $q3File;
//                                    $postAuditQuestion3File->file_path = $request->question_three_file[$key];
//                                    $postAuditQuestion3File->save();
//                                }
//                                    if (isset($request->question_three_file_id) && !empty($request->question_three_file_id) && count($request->question_three_file_id) > 0) {
//
//
//                                        $questionFiles = PostAuditQuestionFile::where('post_audit_question_id', $postAuditQuestion3->id)->get();
//                                        if (!empty($questionFiles) && count($questionFiles) > 0) {
//                                            foreach ($request->question_three_file_id as $question_three_file_id) {
//                                                foreach ($questionFiles as $file) {
//                                                    if ($file->id == $question_three_file_id) {
//                                                    } else {
//                                                        PostAuditQuestionFile::where('id', $file->id)->delete();
//
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }

                                    if ($request->hasfile('q3_files')) {


                                        foreach ($request->file('q3_files') as $image) {
                                            $name = $image->getClientOriginalName();
                                            $explode_name = explode('.', $name)[0];
                                            $extension = $image->getClientOriginalExtension();
                                            $final_name = $explode_name . '_' . time() . '.' . $extension;
                                            $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion3->id . '/');
                                            $image->move($destinationPath, $final_name);
                                            $postAuditQuestion3File = new PostAuditQuestionFile();
                                            $postAuditQuestion3File->post_audit_question_id = $postAuditQuestion3->id;
                                            $postAuditQuestion3File->file_name = $explode_name;
                                            $postAuditQuestion3File->file_path = $final_name;
                                            $postAuditQuestion3File->save();

                                        }
                                    }

                                } else {
                                    $postAuditQuestion3 = PostAuditQueestion::where('id', $request->post_audit_question_three_id)->first();
                                    $postAuditQuestion3->post_audit_id = $postAudit->id;
                                    $postAuditQuestion3->question_id = $request->questions_three_id;
                                    $postAuditQuestion3->value = $request->q3_name;
                                    $postAuditQuestion3->save();
                                }
                            }

                        }
                    }
                }

            }
        }
        $standard = Standards::where('id', $request->standard_id)->first();
        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'scheme_coordinator') {

        } else {
            if ($request->status == 'unapproved') {
                //add a notification for manager
                Notification::create([
                    'type' => 'post_audit',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $request->receiver_id,
                    'icon' => 'message',
                    'body' => $request->remarks,
                    'url' => '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                    'is_read' => 0,
                    'type_id' => $request->aj_standard_stage_id,
                    'request_id' => $postAudit1->id,
                    'status' => 'unapproved',
                    'sent_message' => 'New for ' . $request->ims_heading . ' - ' . $company->name

                ]);
            } elseif ($request->status == 'resent') {
                $notification = Notification::where('type_id', (int)$request->aj_standard_stage_id)->where('request_id', (int)$postAudit1->id)->where('is_read', false)->where('type', 'post_audit')->first();


                Notification::create([
                    'type' => 'post_audit',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $request->receiver_id,
                    'icon' => 'message',
                    'body' => $request->remarks,
                    'url' => '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                    'is_read' => 0,
                    'type_id' => $request->aj_standard_stage_id,
                    'request_id' => $postAudit1->id,
                    'status' => 'resent',
                    'sent_message' => 'Re-Submitted for ' . $request->ims_heading . ' - ' . $company->name

                ]);

                if (!is_null($notification)) {
                    $notification->is_read = true;
                    $notification->save();
                }
//
            }


        }

        if ($request->status == 'save') {
            return redirect()->route('company.show', $request->company_id)->with([
                'flash_status' => 'success',
                'flash_message' => 'Post Audit updated successfully.'
            ]);
        } else {
            return redirect()->route('company.show', $request->company_id)->with([
                'flash_status' => 'success',
                'flash_message' => 'Post Audit updated and successfully send to Concerned Person.'
            ]);
        }


    }

    public function show($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $company = Company::where('id', $company_id)->first();
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();
        $questions = Question::limit(3)->get(['id', 'title_for_oc_om']);

        $companyStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }


        $checkAuditType = str_replace(' ', '_', lcfirst($audit_type));
        $checkAuditType = $checkAuditType . '_verification';

        if ($audit_type == 'Stage 1') {
            $approvedVerificationCount = 0;
        } else {
            $approvedVerificationCount = AJStandardStage::where('aj_standard_id', $standardStage->aj_standard_id)->where('status', 'approved')->where('audit_type', $checkAuditType)->count();
        }


        $postAudit = PostAudit::where('company_id', $company_id)->where('standard_id', $standard->id)->where('aj_standard_stage_id', $standard_stage_id)->first();


        $postAuditQuestionsOne = PostAuditQueestion::where('question_id', $questions[0]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsTwo = PostAuditQueestion::where('question_id', $questions[1]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsThree = PostAuditQueestion::where('question_id', $questions[2]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();

        $notifications = Notification::where('type_id', (int)$standardStage->id)->where('request_id', (int)$postAudit->id)->where('type', 'post_audit')->get();


        return view('post-audit.ims.show', compact('ims_heading', 'notifications', 'approvedVerificationCount', 'company', 'standard', 'standardStage', 'audit_type', 'questions', 'postAudit', 'postAuditQuestionsOne', 'postAuditQuestionsTwo', 'postAuditQuestionsThree'));
    }

    public function delete($company_id = null, $post_audit_id = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {

        $postAudit = PostAudit::where('id', $post_audit_id)->first();
        $postAudit->delete();
        return redirect()->route('company.show', $company_id)->with([
            'flash_status' => 'success',
            'flash_message' => 'Post Audit deleted successfully.'
        ]);
    }

    public function postAuditScheme($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $getschemeinfo = null;
        $getTranferStageInfo = null;

        $company = Company::where('id', $company_id)->first();
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();
        $companyStandard = CompanyStandards::where('company_id', $company_id)->where('standard_id', $standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }


        $questions = Question::all();
        $postAudit = PostAudit::where('company_id', $company_id)->where('standard_id', $standard->id)->where('aj_standard_stage_id', $standard_stage_id)->first();
        $aj_standard_stage = AJStandardStage::where('id', $standard_stage_id)->first();
        if ($aj_standard_stage) {
            $getTranferStageInfo = CompanyStandards::whereHas('company.postAudit.AjStandardStage', function ($q) use ($aj_standard_stage) {
                $q->where('aj_standard_id', $aj_standard_stage->aj_standard_id);
            })->where('client_type', 'transfered')->where('company_id', $company_id)->where('standard_id', $standard->id)->first();
            $getschemeinfo = SchemeInfoPostAudit::whereHas('postAudit.AjStandardStage', function ($q) use ($aj_standard_stage) {
                $q->where('aj_standard_id', $aj_standard_stage->aj_standard_id);
            })->orderBy('id', 'desc')->first();


        } else {
            $getschemeinfo = null;
            $getTranferStageInfo = null;
        }


        if ($audit_type == 'Stage 1') {
            $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])->orderBy('question_id', 'asc')->get();
        } elseif ($audit_type == 'Stage 2' || $audit_type == 'Surveillance 1' || $audit_type == 'Surveillance 2' || $audit_type == 'Surveillance 3' || $audit_type == 'Surveillance 4' || $audit_type == 'Surveillance 5' || $audit_type == 'Stage 2 + Special-Transition' || $audit_type == 'Surveillance 1 + Special-Transition' || $audit_type == 'Surveillance 2 + Special-Transition' || $audit_type == 'Surveillance 3 + Special-Transition' || $audit_type == 'Surveillance 4 + Special-Transition' || $audit_type == 'Surveillance 5 + Special-Transition' || $audit_type == 'Stage 2 + Scope-Extension' || $audit_type == 'Surveillance 1 + Scope-Extension' || $audit_type == 'Surveillance 2 + Scope-Extension' || $audit_type == 'Surveillance 3 + Scope-Extension' || $audit_type == 'Surveillance 4 + Scope-Extension' || $audit_type == 'Surveillance 5 + Scope-Extension') {
            if ($audit_type === 'Stage 2') {
                if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                    $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 17])->orderBy('question_id', 'asc')->get();
                } else {
                    $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14])->orderBy('question_id', 'asc')->get();
                }
            } else {
                if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                    $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17])->orderBy('question_id', 'asc')->get();
                } else {
                    $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15])->orderBy('question_id', 'asc')->get();
                }
            }
        } elseif ($audit_type == 'Reaudit' || $audit_type == 'Reaudit + Special-Transition' || $audit_type == 'Reaudit + Scope-Extension') {
            if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17])->orderBy('question_id', 'asc')->get();
            } else {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16])->orderBy('question_id', 'asc')->get();
            }
        }


//        if ($audit_type == 'Stage 2') {
//            $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereNotIn('question_id', [3, 15])->orderBy('question_id', 'asc')->get();
//        } else {
//            $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereNotIn('question_id', [3])->orderBy('question_id', 'asc')->get();
//        }

        $checkAuditType = str_replace(' ', '_', lcfirst($audit_type));
        $checkAuditType = $checkAuditType . '_verification';

        if ($audit_type == 'Stage 1') {
            $approvedVerificationCount = 0;
        } else {
            $approvedVerificationCount = AJStandardStage::where('aj_standard_id', $standardStage->aj_standard_id)->where('status', 'approved')->where('audit_type', $checkAuditType)->count();
        }


        $postAuditQuestionsOne = PostAuditQueestion::where('question_id', $questions[0]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsTwo = PostAuditQueestion::where('question_id', $questions[1]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsThree = PostAuditQueestion::where('question_id', $questions[2]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();

        $notifications = Notification::where('type_id', (int)$standardStage->id)->where('request_id', (int)$postAudit->id)->where('type', 'post_audit')->get();


        return view('post-audit.ims.schemeViews.index', compact('ims_heading', 'companyStandard', 'company', 'standard', 'standardStage', 'audit_type', 'questions', 'postAudit', 'postAuditQuestionsOne', 'postAuditQuestionsTwo', 'postAuditQuestionsThree', 'approvedVerificationCount', 'postAuditQuestions', 'notifications', 'getschemeinfo', 'getTranferStageInfo'));

    }

    public function postAuditSchemePrintOld($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();
        $companyStandard = CompanyStandards::where('company_id', $company_id)->where('standard_id', $standard->id)->first();
        $questions = Question::all();
        $postAudit = PostAudit::where('company_id', $company_id)->where('standard_id', $standard->id)->where('aj_standard_stage_id', $standard_stage_id)->first();
        $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereNotIn('question_id', [3])->orderBy('question_id', 'asc')->get();
        $postAuditQuestionsOne = PostAuditQueestion::where('question_id', $questions[0]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsTwo = PostAuditQueestion::where('question_id', $questions[1]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $notification = Notification::where('type_id', (int)$standardStage->id)->where('request_id', (int)$postAudit->id)->where('type', 'post_audit')->orderBy('id', 'desc')->first();
        $company = Company::where('id', $company_id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }

        if ($audit_type == 'Stage 1') {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/decision_sheets/decision_sheet_stage1.docx'));
            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);

            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);
            $my_template->setValue('q1', $questions[0]->title_for_sc_sm);
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value)) {
                $my_template->setValue('q1Answer', $postAuditQuestions[0]->scheme_value);
            } else {
                if (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES') {
                    $my_template->setValue('q1Answer', 'No');
                } else {
                    $my_template->setValue('q1Answer', 'N/A');
                }
            }
            $my_template->setValue('q2', $questions[1]->title_for_sc_sm);
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[1]->scheme_value)) {
                $my_template->setValue('q2Answer', $postAuditQuestions[1]->scheme_value);
            } else {
                if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                    $my_template->setValue('q2Answer', 'No');
                } else {
                    $my_template->setValue('q2Answer', 'N/A');
                }
            }
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0 || $key == 1) {
                    } else {
                        if ($audit_type == 'Stage 1') {
                            if ($postAuditQuestion->question_id == 14) {

                            } else {
                                $my_template->setValue('q' . ($key + 1) . '', $postAuditQuestion->question->title_for_sc_sm);
                                $my_template->setValue('q' . ($key + 1) . 'Answer', $postAuditQuestion->scheme_value);
                            }
                        }
                    }

                }

            }
            $my_template->setValue('approvalDate', (isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->approval_date) ? date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->approval_date)) : '');
            $my_template->setValue('comments', $notification ? $notification->body : 'N/A');
            $my_template->setValue('fullname', auth()->user()->fullName());

            try {
                $my_template->saveAs(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));
            } catch (Exception $e) {
                //handle exception
            }

            return response()->download(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));

        } elseif ($audit_type == 'Stage 2' || $audit_type == 'Surveillance 1' || $audit_type == 'Surveillance 2' || $audit_type == 'Surveillance 3' || $audit_type == 'Surveillance 4' || $audit_type == 'Surveillance 5') {

            if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/decision_sheets/decision_sheet_stage2_health_and_safety.docx'));
            } else {
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/decision_sheets/decision_sheet_stage2.docx'));
            }

            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);
            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);
            $my_template->setValue('q1', $questions[0]->title_for_sc_sm);
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value)) {
                $my_template->setValue('q1Answer', $postAuditQuestions[0]->scheme_value);
            } else {
                if (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES') {
                    $my_template->setValue('q1Answer', 'No');
                } else {
                    $my_template->setValue('q1Answer', 'N/A');
                }
            }
            $my_template->setValue('q2', $questions[1]->title_for_sc_sm);
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[1]->scheme_value)) {
                $my_template->setValue('q2Answer', $postAuditQuestions[1]->scheme_value);
            } else {
                if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                    $my_template->setValue('q2Answer', 'No');
                } else {
                    $my_template->setValue('q2Answer', 'N/A');
                }
            }
            $index = 3;
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0 || $key == 1) {
                    } else {
                        if ($postAuditQuestion->question_id == 13) {

                        } else {
                            $my_template->setValue('q' . ($index) . '', $postAuditQuestion->question->title_for_sc_sm);
                            $my_template->setValue('q' . ($index) . 'Answer', $postAuditQuestion->scheme_value);
                            $index++;
                        }
                    }
                }

            }

        } elseif ($audit_type == 'Reaudit') {
            if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/decision_sheets/decision_sheet_reaudit_health_and_safety.docx'));
            } else {
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/decision_sheets/decision_sheet_reaudit.docx'));
            }

            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);
            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);
            $my_template->setValue('q1', $questions[0]->title_for_sc_sm);
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[0]->scheme_value)) {
                $my_template->setValue('q1Answer', $postAuditQuestions[0]->scheme_value);
            } else {
                if (isset($postAuditQuestionsOne) && $postAuditQuestionsOne->value == 'YES') {
                    $my_template->setValue('q1Answer', 'No');
                } else {
                    $my_template->setValue('q1Answer', 'N/A');
                }
            }
            $my_template->setValue('q2', $questions[1]->title_for_sc_sm);
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 0 && !is_null($postAuditQuestions[1]->scheme_value)) {
                $my_template->setValue('q2Answer', $postAuditQuestions[1]->scheme_value);
            } else {
                if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                    $my_template->setValue('q2Answer', 'No');
                } else {
                    $my_template->setValue('q2Answer', 'N/A');
                }
            }
            $index = 3;
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0 || $key == 1) {
                    } else {
                        if ($postAuditQuestion->question_id == 13) {

                        } else {
                            $my_template->setValue('q' . ($index) . '', $postAuditQuestion->question->title_for_sc_sm);
                            $my_template->setValue('q' . ($index) . 'Answer', $postAuditQuestion->scheme_value);
                            $index++;
                        }
                    }
                }

            }

        }
        $my_template->setValue('approvalDate', (isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->approval_date) ? date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->approval_date)) : '');
        $my_template->setValue('comments', $notification ? $notification->body : 'N/A');
        $my_template->setValue('fullname', auth()->user()->fullName());

        try {
            $my_template->saveAs(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));


    }

    public function postAuditSchemeStore(Request $request)
    {
        $scheme_questions = array_values($request->scheme_questions);


        $ajStandardChange = AjStageChange::where('aj_standard_stage_id', $request->aj_standard_stage_id)->first();
        $company = Company::where('id', $request->company_id)->first();
        $standard = Standards::where('id', $request->standard_id)->first();
        $companyStandard = CompanyStandards::where('company_id', $request->company_id)->where('standard_id', $request->standard_id)->first();
        if (auth()->user()->user_type == 'scheme_manager') {
            $companyStandard->last_certificate_issue_date = (isset($request->last_certificate_issue_date)) ? date("Y/m/d", strtotime($request->last_certificate_issue_date)) : null;
            $companyStandard->save();
            $company->is_send_to_ias = false;
            $company->save();
        }
        $mandayStage = AJStandardStage::findOrFail((int)$request->aj_standard_stage_id);
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }
        $postAudit = PostAudit::where('id', $request->post_audit_id)->first();
        if (auth()->user()->user_type == 'scheme_manager') {

            $actualAuditDatefrom = $request->actual_audit_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_audit_date, '0', '10'));
            $actualAuditDateTo = $request->actual_audit_date == null ? null : DateTime::createFromFormat('d-m-Y', substr($request->actual_audit_date, '13'));
            $postAudit->actual_audit_date = $actualAuditDatefrom->format('Y-m-d');
            $postAudit->actual_audit_date_to = $actualAuditDateTo->format('Y-m-d');
        }
        if($request->key_change === 1 || $request->key_change === '1'){
            $postAudit->key_change = true;
            $postAudit->key_remarks = $request->key_remarks;
        }else{
            $postAudit->key_change = false;
        }

        if ($request->status == 'rejected') {
            $postAudit->status = 'rejected';
        } else if ($request->status == 'accepted') {
            $postAudit->status = 'accepted';
        } else if ($request->status == 'resent') {
            $postAudit->status = 'resent';
        } else if ($request->status == 'approved') {
            $postAudit->status = 'approved';
        } else if ($request->status == 'save') {
        }
        if(isset($request->comments)){
            $postAudit->comments = $request->comments;
        }
        $postAudit->save();
        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
            foreach ($activeImsStandards as $key1 => $standard) {

                if ($key1 == 0) {
                    $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();

                    if (!is_null($ajStandard)) {
                        if (!is_null($mandayStage->recycle)) {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                        } else {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                        }
                        if (!is_null($standard_stage)) {


//                            if ($request->audit_type == 'Stage 2') {
//                                $questions = Question::whereNotIn('id', [3, 15])->get();
//                            } else {
//                                $questions = Question::whereNotIn('id', [3])->get();
//                            }

                            if ($request->audit_type == 'Stage 1') {
                                $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])->get();
                            } elseif ($request->audit_type == 'Stage 2' || $request->audit_type == 'Surveillance 1' || $request->audit_type == 'Surveillance 2' || $request->audit_type == 'Surveillance 3' || $request->audit_type == 'Surveillance 4' || $request->audit_type == 'Surveillance 5' || $request->audit_type == 'Surveillance 1 + Scope-Extension' || $request->audit_type == 'Surveillance 2 + Scope-Extension' || $request->audit_type == 'Surveillance 3 + Scope-Extension' || $request->audit_type == 'Surveillance 4 + Scope-Extension' || $request->audit_type == 'Surveillance 5 + Scope-Extension') {

                                if ($request->audit_type == 'Stage 2') {
                                    if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                                        $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 17])->get();
                                    } else {
                                        $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14])->get();
                                    }
                                } else {
                                    if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                                        $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17])->get();
                                    } else {
                                        $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15])->get();
                                    }
                                }

                            } elseif ($request->audit_type == 'Reaudit') {
                                if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {

                                    $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17])->get();
                                } else {
                                    $questions = Question::whereIn('id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16])->get();
                                }
                            }
                            if (!is_null($postAudit->q4_file) && !is_null($postAudit->is_q4) && $postAudit->is_q4 === 'YES' && $request->hasfile('q4_files')) {

//                                $destinationPath = public_path('/uploads/post-audit/communication/' . $postAudit->id . '/');
                                $filename = public_path('/uploads/post-audit/communication/' . $postAudit->id . '/'. $postAudit->q4_file);

                                if (File::exists($filename)) {
                                    File::delete($filename);  // or unlink($filename);
                                }
                                foreach ($request->file('q4_files') as $image) {
                                    $name = $image->getClientOriginalName();
                                    $explode_name = explode('.', $name)[0];
                                    $extension = $image->getClientOriginalExtension();
                                    $final_name = $explode_name . '_' . time() . '.' . $extension;
                                    $destinationPath = public_path('/uploads/post-audit/communication/' . $postAudit->id . '/');
                                    $image->move($destinationPath, $final_name);
                                    $postAudit->q4_file = $final_name;

                                }
                            }
                            if (!empty($questions) && count($questions) > 0) {
                                foreach ($questions as $key => $question) {

                                    $postAuditQuestion = PostAuditQueestion::where('post_audit_id', $postAudit->id)->where('question_id', $question->id)->first();

                                    if ($postAuditQuestion) {
                                        $postAuditQuestion->post_audit_id = $postAudit->id;
                                        $postAuditQuestion->question_id = $question->id;
                                        $postAuditQuestion->scheme_value = $scheme_questions[$key];
                                        $postAuditQuestion->save();
                                    } else {
                                        $postAuditData = new PostAuditQueestion();
                                        $postAuditData->post_audit_id = $postAudit->id;
                                        $postAuditData->question_id = $question->id;
                                        $postAuditData->scheme_value = $scheme_questions[$key];
                                        $postAuditData->save();
                                    }

                                }
                            }

                            $postAuditQuestion1 = PostAuditQueestion::where('id', $request->post_audit_question_one_id)->first();


                            if ($request->hasfile('q1_files')) {

                                foreach ($request->file('q1_files') as $image) {
                                    $name = $image->getClientOriginalName();
                                    $explode_name = explode('.', $name)[0];
                                    $extension = $image->getClientOriginalExtension();
                                    $final_name = $explode_name . '_' . time() . '.' . $extension;
                                    $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion1->id . '/');
                                    $image->move($destinationPath, $final_name);
                                    $postAuditQuestion1File = new PostAuditQuestionFile();
                                    $postAuditQuestion1File->post_audit_question_id = $postAuditQuestion1->id;
                                    $postAuditQuestion1File->file_name = $explode_name;
                                    $postAuditQuestion1File->file_path = $final_name;
                                    $postAuditQuestion1File->save();

                                }
                            }


                            $postAuditQuestion2 = PostAuditQueestion::where('id', $request->post_audit_question_two_id)->first();


                            if ($request->hasfile('q2_files')) {


                                foreach ($request->file('q2_files') as $image) {
                                    $name = $image->getClientOriginalName();
                                    $explode_name = explode('.', $name)[0];
                                    $extension = $image->getClientOriginalExtension();
                                    $final_name = $explode_name . '_' . time() . '.' . $extension;
                                    $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion2->id . '/');
                                    $image->move($destinationPath, $final_name);
                                    $postAuditQuestion2File = new PostAuditQuestionFile();
                                    $postAuditQuestion2File->post_audit_question_id = $postAuditQuestion2->id;
                                    $postAuditQuestion2File->file_name = $explode_name;
                                    $postAuditQuestion2File->file_path = $final_name;
                                    $postAuditQuestion2File->save();

                                }
                            }


                            $postAuditQuestion3 = PostAuditQueestion::where('id', $request->post_audit_question_three_id)->first();


                            if ($request->hasfile('q3_files')) {


                                foreach ($request->file('q3_files') as $image) {
                                    $name = $image->getClientOriginalName();
                                    $explode_name = explode('.', $name)[0];
                                    $extension = $image->getClientOriginalExtension();
                                    $final_name = $explode_name . '_' . time() . '.' . $extension;
                                    $destinationPath = public_path('/uploads/post-audit-ims/' . $postAuditQuestion3->id . '/');
                                    $image->move($destinationPath, $final_name);
                                    $postAuditQuestion3File = new PostAuditQuestionFile();
                                    $postAuditQuestion3File->post_audit_question_id = $postAuditQuestion3->id;
                                    $postAuditQuestion3File->file_name = $explode_name;
                                    $postAuditQuestion3File->file_path = $final_name;
                                    $postAuditQuestion3File->save();

                                }
                            }


                        }
                    }
                }

            }
        }


        if (auth()->user()->user_type == 'scheme_manager') {
            if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                foreach ($activeImsStandards as $key => $standard) {
                    if ($key == 0) {
                        $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                        if (!is_null($ajStandard)) {
                            if (!is_null($mandayStage->recycle)) {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                            } else {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                            }
                            if (!is_null($standard_stage)) {
                                $postAudit = PostAudit::where('company_id', $request->company_id)->where('standard_id', $standard)->where('aj_standard_stage_id', $standard_stage->id)->first();
                                if ($postAudit && !is_null($postAudit)) {
                                    $schemeInfo = SchemeInfoPostAudit::where('post_audit_id', $postAudit->id)->first();

                                    if (!is_null($schemeInfo)) {
                                        $schemeInfo->post_audit_id = $postAudit->id;
                                        $schemeInfo->current_certificate_status_id = $postAudit->id;
                                        $schemeInfo->pack_closed = ($request->pack_closed == 'TRUE') ? 'YES' : 'NO';
                                        $schemeInfo->print_required = ($request->print_required == 'TRUE') ? 'YES' : 'NO';
                                        $schemeInfo->certificate_validity = (isset($request->certificate_validity) ? $request->certificate_validity : $schemeInfo->certificate_validity);
                                        $schemeInfo->print_assigned_to = $request->print_assigned_to;
                                        $schemeInfo->approval_date = (isset($request->approval_date)) ? date("Y/m/d", strtotime($request->approval_date)) : null;
                                        if (auth()->user()->user_type == 'scheme_manager') {
                                            $schemeInfo->last_certificate_issue_date = (isset($request->last_certificate_issue_date)) ? date("Y/m/d", strtotime($request->last_certificate_issue_date)) : null;
                                        }
                                        $schemeInfo->new_expiry_date = (isset($request->new_expiry_date)) ? date("Y/m/d", strtotime($request->new_expiry_date)) : $schemeInfo->new_expiry_date;
                                        $schemeInfo->original_issue_date = (isset($request->original_issue_date)) ? date("Y/m/d", strtotime($request->original_issue_date)) : $schemeInfo->original_issue_date;
                                        $schemeInfo->save();
                                    } else {
                                        $schemeInfo = new SchemeInfoPostAudit();
                                        $schemeInfo->post_audit_id = $postAudit->id;
                                        $schemeInfo->current_certificate_status_id = $postAudit->id;
                                        $schemeInfo->pack_closed = ($request->pack_closed == 'TRUE') ? 'YES' : 'NO';
                                        $schemeInfo->print_required = ($request->print_required == 'TRUE') ? 'YES' : 'NO';
                                        $schemeInfo->certificate_validity = (isset($request->certificate_validity) ? $request->certificate_validity : null);
                                        $schemeInfo->print_assigned_to = $request->print_assigned_to;
                                        $schemeInfo->approval_date = (isset($request->approval_date)) ? date("Y/m/d", strtotime($request->approval_date)) : null;
                                        $schemeInfo->last_certificate_issue_date = (isset($request->last_certificate_issue_date)) ? date("Y/m/d", strtotime($request->last_certificate_issue_date)) : null;
                                        $schemeInfo->new_expiry_date = (isset($request->new_expiry_date)) ? date("Y/m/d", strtotime($request->new_expiry_date)) : null;
                                        $schemeInfo->original_issue_date = (isset($request->original_issue_date)) ? date("Y/m/d", strtotime($request->original_issue_date)) : null;
                                        $schemeInfo->save();
                                    }
                                }
                            }
                        }
                    }

                }
            }


            $standard = Standards::where('id', $request->standard_id)->first();

            $notification = Notification::where('type_id', (int)$request->aj_standard_stage_id)->where('request_id', (int)$request->post_audit_id)->where('is_read', false)->where('type', 'post_audit')->first();


            if (!is_null($notification)) {

                if (!is_null($postAudit->sentToUser) && $postAudit->sentToUser->user_type == 'scheme_manager') {
                    $sentBy = $postAudit->sender_id;
                    $url = '/ims-post-audit/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id;
                } else {
                    $sentBy = $postAudit->receiver_id;
                    $url = '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id;
                }


            } else {
                if (!is_null($postAudit->sentToUser) && $postAudit->sentToUser->user_type == 'scheme_manager') {
                    $sentBy = $postAudit->sender_id;
                    $url = '/ims-post-audit/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id;
                } else {
                    $sentBy = $postAudit->receiver_id;
                    $url = '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id;
                }

            }
            if ($request->status == 'rejected') {
                //add a notification for manager
                Notification::create([
                    'type' => 'post_audit',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $sentBy,
                    'icon' => 'message',
                    'body' => $request->remarks,
                    'url' => $url,
                    'is_read' => 0,
                    'type_id' => $request->aj_standard_stage_id,
                    'request_id' => $request->post_audit_id,
                    'status' => 'rejected',
                    'sent_message' => 'Rejected for ' . $request->ims_heading . ' - ' . $company->name

                ]);
                if (!is_null($notification)) {
                    $notification->is_read = true;
                    $notification->save();
                }
            } else if ($request->status == 'approved') {
                $ajStandardStatusUpdate = AJStandardStage::find($request->aj_standard_stage_id);
                if (!is_null($ajStandardStatusUpdate)) {
                    $ajStandardStatusUpdate->aj_approved_status = true;
                    $ajStandardStatusUpdate->save();
                }

                if ($request->print_assigned_to == "scheme_coordinator" && $request->audit_type != 'Stage 1' && $request->print_required == 'TRUE') {
                    Notification::create([
                        'type' => 'draft_for_printing',
                        'sent_by' => auth()->user()->id,
//                        'sent_to' => $notification->sent_by,
                        'sent_to' => auth()->user()->scheme_manager->scheme_coordinators[0]->user_id,
                        'icon' => 'message',
                        'body' => $request->remarks,
                        'url' => '/ims-post-audit/draft-print/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                        'is_read' => 0,
                        'type_id' => $request->aj_standard_stage_id,
                        'request_id' => $request->post_audit_id,
                        'status' => 'approved',
                        'sent_message' => 'Approved for ' . $request->ims_heading . ' - ' . $company->name
                    ]);
                    $user = auth()->user()->scheme_manager->scheme_coordinators[0]->user;
                    $subject = 'Approved for ' . $request->ims_heading . ' - ' . $company->name;
                    $url = '/ims-post-audit/draft-print/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id;
                    Mail::to($user->email)->cc('scheme2@ricionline.com')->send(new DraftMail($subject, $url, $user));
                }

//                if ($postAudit->sentToUser->user_type != 'scheme_manager') {
//                    Notification::create([
//                        'type' => 'post_audit',
//                        'sent_by' => auth()->user()->id,
//                        'sent_to' => $notification->sent_by,
//                        'icon' => 'message',
//                        'body' => $request->remarks,
//                        'url' => '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
//                        'is_read' => 0,
//                        'type_id' => $request->aj_standard_stage_id,
//                        'request_id' => $request->post_audit_id,
//                        'status' => 'approved',
//                        'sent_message' => 'Approved for ' . $request->ims_heading . ' - ' . $company->name
//                    ]);
//                }

                Notification::create([
                    'type' => 'post_audit',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $postAudit->sender_id,
                    'icon' => 'message',
                    'body' => $request->remarks,
                    'url' => '/ims-post-audit/show/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                    'is_read' => 0,
                    'type_id' => $request->aj_standard_stage_id,
                    'request_id' => $request->post_audit_id,
                    'status' => 'approved',
                    'sent_message' => 'Approved for ' . $request->ims_heading . ' - ' . $company->name

                ]);
                if ($request->audit_type !== 'stage_1') {
                    $companyStandard->main_remarks = null;
                }
                $companyStandard->save();
                if (!is_null($notification)) {
                    $notification->is_read = true;
                    $notification->save();
                }


                if (!is_null($request->recycle)) {
                    $standard_stage1 = AJStandardStage::where('id', (int)$request->aj_standard_stage_id)->where('recycle', (int)$request->recycle)->first();
                } else {
                    $standard_stage1 = AJStandardStage::where('id', (int)$request->aj_standard_stage_id)->whereNull('recycle')->first();
                }


                $postAudit = PostAudit::findOrFail((int)$request->post_audit_id);

                if ($standard_stage1->audit_type == 'stage_2') {
                    $this->approvedStageTwoUpdateDates($standard_stage1->audit_type, $standard_stage1, $postAudit->actual_audit_date_to, $companyStandard);
                }
                if (!is_null($standard_stage->recycle)) {
                    $nextCycle = (int)$standard_stage->recycle + 1;
                } else {
                    $nextCycle = 1;
                }

                $checkNextCycle = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('recycle', $nextCycle)->first();
                if (is_null($checkNextCycle)) {
                    if (isset($ajStandardChange) && !is_null($ajStandardChange) && $ajStandardChange->version == 'yes' && !is_null($ajStandardChange->standards)) {

                        $this->newStandardStagesAdd($company, $standard->id, $ajStandardChange->standards, $standard_stage1->aj_standard_id, $standard_stage1);
                        $this->newStages($company, $standard->id, $ajStandardChange->standards, $standard_stage1->id);

                    } else {

                        $this->newStages($company, $standard->id, 0, $standard_stage->id);

                    }
                }


            }
        } else if (auth()->user()->user_type == 'scheme_coordinator') {
            $standard = Standards::where('id', $request->standard_id)->with('scheme_manager')->first();

            $notification = Notification::where('type_id', (int)$request->aj_standard_stage_id)->where('request_id', (int)$postAudit->id)->where('is_read', false)->where('type', 'post_audit')->first();

            if ($request->status == 'rejected') {
                //add a notification for manager
                Notification::create(['type' => 'post_audit',
                    'sent_by' => auth()->user()->id,
                    'sent_to' => $notification->sent_by,
                    'icon' => 'message',
                    'body' => $request->remarks,
                    'url' => '/ims-post-audit/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                    'is_read' => 0,
                    'type_id' => $request->aj_standard_stage_id,
                    'request_id' => $postAudit->id,
                    'status' => 'rejected',
                    'sent_message' => 'Rejected for ' . $request->ims_heading . ' - ' . $company->name]);
                if (!is_null($notification)) {
                    $notification->is_read = true;
                    $notification->save();
                }
            } else
                if ($request->status == 'accepted') {
                    Notification::create([
                        'type' => 'post_audit',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $standard->scheme_manager[0]->user_id,
                        'icon' => 'message',
                        'body' => $request->remarks,
                        'url' => '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                        'is_read' => 0,
                        'type_id' => $request->aj_standard_stage_id,
                        'request_id' => $postAudit->id,
                        'status' => 'accepted',
                        'sent_message' => 'Accepted for ' . $request->ims_heading . ' - ' . $company->name

                    ]);
                    if (!is_null($notification)) {
                        $notification->is_read = true;
                        $notification->save();
                    }
                } else if ($request->status == 'resent') {
                    Notification::create([
                        'type' => 'post_audit',
                        'sent_by' => auth()->user()->id,
                        'sent_to' => $standard->scheme_manager[0]->user_id,
                        'icon' => 'message',
                        'body' => $request->remarks,
                        'url' => '/ims-post-audit/scheme/edit/' . $request->company_id . '/' . $request->audit_type . '/' . $standard->id . '/' . $request->aj_standard_stage_id,
                        'is_read' => 0,
                        'type_id' => $request->aj_standard_stage_id,
                        'request_id' => $postAudit->id,
                        'status' => 'resent',
                        'sent_message' => 'Re-Submitted for ' . $request->ims_heading . ' - ' . $company->name

                    ]);
                    if (!is_null($notification)) {
                        $notification->is_read = true;
                        $notification->save();
                    }
                }


        }


        return redirect()->route('company.show', $request->company_id)->with([
            'flash_status' => 'success',
            'flash_message' => 'Post Audit updated and successfully send to Concerned Person.'
        ]);


    }

    public function newStandardStagesAdd($company, $standardId, $newStandardId, $ajStandardStageId, $stdStage)
    {


        $imsIdsArray = [];
        $imsOldIdsArray = [];
        $imsString = '';


        $standard = Standards::where('id', $standardId)->first();
        $companyStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $standardId)->first();
        if (!is_null($stdStage->recycle)) {
            $mandayStage = AJStandardStage::where('id', (int)$stdStage->id)->where('recycle', (int)$stdStage->recycle)->first();
        } else {
            $mandayStage = AJStandardStage::where('id', (int)$stdStage->id)->whereNull('recycle')->first();
        }

        $imsStandardIds = $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first();
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->orderBy('type', 'asc')->pluck('standard_id')->toArray();
        $ajStandardObjectId = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->audit_justification_id)->whereIn('standard_id', $activeImsStandards)->pluck('id')->toArray();
        if (!is_null($mandayStage->recycle)) {
            $ajStandardStageObject = AJStandardStage::whereIn('aj_standard_id', $ajStandardObjectId)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->get();
        } else {
            $ajStandardStageObject = AJStandardStage::whereIn('aj_standard_id', $ajStandardObjectId)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->get();
        }


        $ajStandardChangeObject = [];
        if (!empty($ajStandardStageObject) && count($ajStandardStageObject) > 0) {
            foreach ($ajStandardStageObject as $ajStage) {
                $ajChange = AjStageChange::where('aj_standard_stage_id', $ajStage->id)->whereNull('deleted_at')->first();

                if (!is_null($ajChange) && $ajChange->version == 'yes' && !is_null($ajChange->standards)) {
                    if (!is_null($mandayStage->recycle)) {
                        $standard_stage = AJStandardStage::where('aj_standard_id', $ajStage->aj_standard_id)->where('recycle', (int)$mandayStage->recycle)->where('status', 'Not Applied')->get();
                    } else {
                        $standard_stage = AJStandardStage::where('aj_standard_id', $ajStage->aj_standard_id)->whereNull('recycle')->where('status', 'Not Applied')->get();
                    }


                    if (!empty($standard_stage) && count($standard_stage)) {
                        if ($newStandardId == 0) {
                            $aj_standard = $ajStage;
                        } else {
                            $aj_standard = new AJStandard();
                            $aj_standard->audit_justification_id = $mandayStage->ajStandard->audit_justification_id;
                            $aj_standard->standard_id = $ajChange->standards;
                            $aj_standard->save();

                            if (!empty($standard_stage) && count($standard_stage) > 0) {
                                foreach ($standard_stage as $stage) {
                                    $stage->aj_standard_id = $aj_standard->id;
                                    $stage->save();
                                }

                                $companyOldStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $ajStage->ajStandard->standard_id)->first();
                                $companyNewStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $ajChange->standards)->first();

                                array_push($imsIdsArray, $companyNewStandard->id);
                                array_push($imsOldIdsArray, $companyOldStandard->id);

                                if ($imsString == '') {
                                    $imsString .= $companyNewStandard->id;
                                } else {
                                    $imsString .= ',' . $companyNewStandard->id;
                                }

                                $companyOldStandardPrefix = explode(':', $standard->name);
                                $companyNewStandardPrefix = explode(':', $ajChange->ajStandardStage->ajStandard->standard->name);
                                $companyNewStandard->status = 'approved';
                                $companyNewStandard->is_ims = true;

                                if ($companyOldStandardPrefix[0] == $companyNewStandardPrefix[0]) {
                                    $companyNewStandard->type = 'base';
                                } else {
                                    $companyNewStandard->type = 'child';
                                }

                                $companyNewStandard->save();


                                $companyOldStandard->status = 'disabled';
                                $companyOldStandard->is_ims = false;
                                $companyOldStandard->type = null;
                                $companyOldStandard->save();

                                $checkCompanyStandardLatest = Certificate::where('company_standard_id', $companyOldStandard->id)->where('report_status', 'new')->first();
                                $checkCompanyStandard = Certificate::where('company_standard_id', $companyOldStandard->id)->where('report_status', 'new')->get();

                                foreach ($checkCompanyStandard as $reportStatus) {
                                    $reportStatus->report_status = 'old';
                                    $reportStatus->certificate_status = 'version_change';
                                    $reportStatus->save();
                                }


                                $certificate = new Certificate();
                                $certificate->company_standard_id = $companyNewStandard->id;
                                $certificate->certificate_status = $checkCompanyStandardLatest->certificate_status;
                                $certificate->certificate_date = $checkCompanyStandardLatest->certificate_date;
                                $certificate->report_status = 'new';
                                $certificate->save();

                            }


                        }
                    }
                }
            }


//            foreach ($companyStandardIds as $c_std) {
//                if (count($imsIdsArray) == 1) {
//
//                    if ($c_std == $imsOldIdsArray[0]) {
//                        $newImsStandardIds = str_replace($c_std, $imsIdsArray[0], $imsStandardIds);
//                        $companyStandardIds = explode(',', $newImsStandardIds);
//                        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->get();
//
//                        foreach ($activeImsStandards as $activeImsStandard) {
//
//                            $activeImsStandard->ims_standard_ids = $newImsStandardIds;
//                            $activeImsStandard->save();
//
//                        }
//                    }
//                } elseif (count($imsIdsArray) == 2) {
//                    if ($c_std == $imsOldIdsArray[0]) {
//                        $newImsStandardIds = str_replace($c_std, $imsIdsArray[0], $imsStandardIds);
//                    }
//                    if ($c_std == $imsOldIdsArray[1]) {
//                        $newImsStandardIds = str_replace($c_std, $imsIdsArray[1], $newImsStandardIds);
//                        $companyStandardIds = explode(',', $newImsStandardIds);
//                        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->get();
//                        foreach ($activeImsStandards as $activeImsStandard) {
//                            $activeImsStandard->ims_standard_ids = $newImsStandardIds;
//                            $activeImsStandard->save();
//
//                        }
//                    }
//                }
//            }
            $newImsStandardIds = '';
            if (count($imsIdsArray) == 2) {
                foreach ($imsStandardIds as $key => $ids) {
                    if ($key == 0) {
                        $newImsStandardIds = str_replace($imsOldIdsArray[0], $imsIdsArray[0], $imsStandardIds);
                    } else {
                        $newImsStandardIds = str_replace($imsIdsArray, $imsIdsArray[0], $imsStandardIds);
                        $companyStandardIds = explode(',', $imsString);
                        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->get();
                        foreach ($activeImsStandards as $activeImsStandard) {
                            $activeImsStandard->ims_standard_ids = $imsString;
                            $activeImsStandard->save();

                        }
                    }

                }


            }
            if (count($imsIdsArray) == 3) {

                $companyStandardIds = explode(',', $imsString);
                $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $company->id)->get();
                foreach ($activeImsStandards as $activeImsStandard) {
                    $activeImsStandard->ims_standard_ids = $imsString;
                    $activeImsStandard->save();

                }

            }
        }


    }

    public function newStages($company, $standardId, $newStandardId, $ajStandardStageId)
    {


        $standard = Standards::where('id', $standardId)->first();
        $companyStandard = CompanyStandards::where('company_id', $company->id)->where('standard_id', $standardId)->first();

        $companyStandardIdss = explode(',', $companyStandard->ims_standard_ids);
        $activeImsStandardss = CompanyStandards::whereIn('id', $companyStandardIdss)->where('company_id', $company->id)->pluck('standard_id')->toArray();
        $activeImsComplexitys = CompanyStandards::whereIn('id', $companyStandardIdss)->where('company_id', $company->id)->pluck('proposed_complexity')->toArray();


        $manday = $this->generateIMSTable($activeImsStandardss, $activeImsComplexitys, $company->effective_employees, $companyStandard->surveillance_frequency);

//        $manday = $this->generateSimpleTable($standard->id, $companyStandard->proposed_complexity, $company->effective_employees, $companyStandard->surveillance_frequency);
        $calc_manday = $manday;
        $standard_stage = AJStandardStage::findOrFail((int)$ajStandardStageId);
        $aj_standard1 = AJStandard::findOrFail((int)$standard_stage->aj_standard_id);

        if ($standard_stage->audit_type == 'reaudit' && $standard_stage->status == 'approved') {
            $mandayStage = AJStandardStage::findOrFail((int)$standard_stage->id);
            $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
            $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();


            if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                foreach ($activeImsStandards as $key => $standard) {
                    if ($key == 0) {
                        $ajStandardcheck = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();

                        if (!is_null($ajStandardcheck)) {
                            if (!is_null($mandayStage->recycle)) {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandardcheck->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                            } else {
                                $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandardcheck->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                            }

                            if (!is_null($standard_stage)) {
                                if ($newStandardId == 0) {
                                    $aj_standard = $standard_stage;
                                } else {
                                    $aj_standard = new AJStandard();
                                    $aj_standard->audit_justification_id = $aj_standard1->audit_justification_id;
                                    $aj_standard->standard_id = $standard;
                                    $aj_standard->save();
                                }

                                $recycle_date = $standard_stage->excepted_date;
                                if ($companyStandard->frequency === 'biannual') {
                                    $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($recycle_date)));
                                    $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                    $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                    $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                    $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                    $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
                                    $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                    $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];

                                } else {
                                    $effectiveDate = date('Y-m-d', strtotime("+1 year", strtotime($recycle_date)));
                                    $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($recycle_date)));
                                    $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($recycle_date)));
                                    $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
                                    $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];
                                }

                                if ($standard_stage->recycle == null) {
                                    $next_recycle = 1;

                                    $all_standard_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->get();
                                } else {
                                    $next_recycle = $standard_stage->recycle + 1;
                                    $all_standard_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->get();
                                }


                                foreach ($aj_audit_types as $key1 => $aj_audit_type) {
                                    $aj_standard_stages_obj = $ajStandardcheck->ajStandardStages()->create([
                                        'audit_type' => $aj_audit_type,
                                        'onsite' => $calc_manday[$aj_audit_type . '_onsite'],
                                        'offsite' => $calc_manday[$aj_audit_type . '_offsite'],
                                        'excepted_date' => $due_date[$key1],
                                        'recycle' => $next_recycle,
                                        'job_number' => $standard_stage->job_number,
                                    ]);

                                }


                            }
                        }
                    }

                }
            }
        }
        return true;

    }

    public function approvedStageTwoUpdateDates($stage, $standard_stage, $actual_audit_date_to, $companyStandard)
    {
        if ($stage == 'stage_2') {

            if (isset($actual_audit_date_to) && !is_null($actual_audit_date_to)) {
                $mandayStage = AJStandardStage::findOrFail((int)$standard_stage->id);
                $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
                $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();

                if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
                    foreach ($activeImsStandards as $key => $standard) {

                        if ($key == 0) {
                            $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                            if (!is_null($ajStandard)) {
                                if (!is_null($mandayStage->recycle)) {
                                    $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                                } else {
                                    $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                                }

                                if (!is_null($standard_stage)) {
                                    $next_stages = AJStandardStage::where('aj_standard_id', $standard_stage->aj_standard_id)->where('status', 'Not Applied')->get();
                                    $frequency = $companyStandard->frequency;
                                    if ($frequency === 'biannual') {
                                        $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($actual_audit_date_to)));
                                        $effectiveDate1 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate)));
                                        $effectiveDate2 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate1)));
                                        $effectiveDate3 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate2)));
                                        $effectiveDate4 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate3)));
                                        $effectiveDate5 = date('Y-m-d', strtotime("+6 months", strtotime($effectiveDate4)));
                                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'reaudit'];
                                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2, $effectiveDate3, $effectiveDate4, $effectiveDate5];

                                    } else {

                                        $effectiveDate = date('Y-m-d', strtotime("+11 months", strtotime($actual_audit_date_to)));
                                        $effectiveDate1 = date('Y-m-d', strtotime("+2 year", strtotime($actual_audit_date_to)));
                                        $effectiveDate2 = date('Y-m-d', strtotime("+3 year", strtotime($actual_audit_date_to)));
                                        $aj_audit_types = ['surveillance_1', 'surveillance_2', 'reaudit'];
                                        $due_date = [$effectiveDate, $effectiveDate1, $effectiveDate2];
                                    }
                                    foreach ($aj_audit_types as $key1 => $aj_audit_type) {
                                        foreach ($next_stages as $nextStage) {
                                            if ($nextStage->audit_type == $aj_audit_type) {
                                                $standard_stage = AJStandardStage::findOrFail((int)$nextStage->id);
                                                $standard_stage->update([
                                                    'excepted_date' => $due_date[$key1],
                                                ]);
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    }
                }
            }
        }
    }

    public function postAuditDraftPrint($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {

        $company = Company::where('id', $company_id)->first();
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();
        $questions = Question::all();
        $postAudit = PostAudit::where('company_id', $company_id)->where('standard_id', $standard->id)->where('aj_standard_stage_id', $standard_stage_id)->first();
        $companyStandard = CompanyStandards::where('company_id', $company_id)->where('standard_id', $standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }


        $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereNotIn('question_id', [3])->orderBy('question_id', 'asc')->get();


        $checkAuditType = str_replace(' ', '_', lcfirst($audit_type));
        $checkAuditType = $checkAuditType . '_verification';

        if ($audit_type == 'Stage 1') {
            $approvedVerificationCount = 0;
        } else {
            $approvedVerificationCount = AJStandardStage::where('aj_standard_id', $standardStage->aj_standard_id)->where('status', 'approved')->where('audit_type', $checkAuditType)->count();
        }


        $postAuditQuestionsOne = PostAuditQueestion::where('question_id', $questions[0]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsTwo = PostAuditQueestion::where('question_id', $questions[1]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();
        $postAuditQuestionsThree = PostAuditQueestion::where('question_id', $questions[2]->id)->where('post_audit_id', $postAudit->id)->with('postAuditQuestionFile')->first();

        $notifications = Notification::where('type_id', (int)$standardStage->id)->where('request_id', (int)$postAudit->id)->where('type', 'post_audit')->get();


        return view('post-audit.ims.schemeViews.draft_for_printing', compact('imsCompanyStandards', 'ims_heading', 'companyStandard', 'company', 'standard', 'standardStage', 'audit_type', 'questions', 'postAudit', 'postAuditQuestionsOne', 'postAuditQuestionsTwo', 'postAuditQuestionsThree', 'approvedVerificationCount', 'postAuditQuestions', 'notifications'));

    }

    public function postAuditDraftPrintStore(Request $request)
    {
        $mandayStage = AJStandardStage::findOrFail((int)$request->aj_standard_stage_id);
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();

        $notication = Notification::where('request_id', (int)$request->post_audit_id)->where('type_id', $request->aj_standard_stage_id)->where('type', 'draft_for_printing')->whereIsRead(0)->orderBy('id', 'desc')->first();

        if (!is_null($notication)) {
            $notication->is_read = true;
            $notication->save();
        }

        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
            foreach ($activeImsStandards as $key => $standard) {
                if ($key == 0) {
                    $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                    if (!is_null($ajStandard)) {
                        if (!is_null($mandayStage->recycle)) {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                        } else {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                        }
                        if (!is_null($standard_stage)) {
                            $postAudit = PostAudit::where('company_id', $request->company_id)->where('standard_id', $standard)->where('aj_standard_stage_id', $standard_stage->id)->first();
                            if (isset($request->certificate_filename)) {
                                $postAudit->postAuditCertificateFile()->delete();
                                foreach ($request->certificate_filename as $key1 => $cFile) {
                                    $certificate_file = new CertificateFile();
                                    $certificate_file->post_audit_id = $postAudit->id;
                                    $certificate_file->file_name = $cFile;
                                    $certificate_file->file_path = $this->convertBase64ToImage($request->certificate_file[$key1],$cFile,$postAudit->id);
                                    $certificate_file->file_type = 'file';
                                    $certificate_file->save();
                                }
                            }
                            if ($postAudit && !is_null($postAudit)) {
                                $schemeInfo = SchemeInfoPostAudit::where('post_audit_id', $postAudit->id)->first();

                                if (!is_null($schemeInfo)) {
                                    $schemeInfo->uci_number = $request->uci_number == null ? $schemeInfo->uci_number : $request->uci_number;
                                    $schemeInfo->save();
                                }
                            }
                        }
                    }
                }

            }
        }
        return redirect()->route('company.show', $request->company_id)->with([
            'flash_status' => 'success',
            'flash_message' => 'Draft Printing updated successfully.'
        ]);


    }

    public function postAuditDraftPrintUpdate(Request $request)
    {
        $mandayStage = AJStandardStage::findOrFail((int)$request->aj_standard_stage_id);
        $companyStandardIds = explode(',', $mandayStage->ajStandard->ajJustification->company->companyStandards->where('standard_id', $mandayStage->ajStandard->standard_id)->pluck('ims_standard_ids')->first());
        $activeImsStandards = CompanyStandards::whereIn('id', $companyStandardIds)->where('company_id', $mandayStage->ajStandard->ajJustification->company_id)->pluck('standard_id')->toArray();

        if (!empty($activeImsStandards) && count($activeImsStandards) > 0) {
            foreach ($activeImsStandards as $key => $standard) {
                if ($key == 0) {
                    $ajStandard = AJStandard::where('audit_justification_id', $mandayStage->ajStandard->ajJustification->id)->where('standard_id', $standard)->first();
                    if (!is_null($ajStandard)) {
                        if (!is_null($mandayStage->recycle)) {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->where('recycle', (int)$mandayStage->recycle)->first();
                        } else {
                            $standard_stage = AJStandardStage::where('aj_standard_id', $ajStandard->id)->where('audit_type', $mandayStage->audit_type)->whereNull('recycle')->first();

                        }
                        if (!is_null($standard_stage)) {
                            $postAudit = PostAudit::where('company_id', $request->company_id)->where('standard_id', $standard)->where('aj_standard_stage_id', $standard_stage->id)->first();
                            if (isset($request->certificate_filename)) {
                                foreach ($request->certificate_filename as $key1 => $cFile) {
                                    $certificate_file = CertificateFile::where('id', $request->certificate_file_id)->first();
                                    $certificate_file->post_audit_id = $postAudit->id;
                                    $certificate_file->file_name = $cFile;
                                    $certificate_file->file_path = $this->convertBase64ToImage($request->certificate_file[$key1],$cFile,$postAudit->id);
                                    $certificate_file->file_type = 'file';
                                    $certificate_file->save();
                                }
                            }
                        }
                    }
                }

            }
        }
        return redirect()->route('company.show', $request->company_id)->with([
            'flash_status' => 'success',
            'flash_message' => 'Draft Printing File updated successfully.'
        ]);


    }

    public function convertBase64ToImage($base64Image, $fileName, $post_audit_id)
    {
        $image_64 = $base64Image; // your base64 encoded data

        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf

        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);

        // find substring for replace here e.g., data:image/png;base64,

        $image = str_replace($replace, '', $image_64);

        $image = str_replace(' ', '+', $image);

        $imageName = time() . '_' . $fileName . '.' . $extension;

        // Get the public path
        $publicPath = public_path('/uploads/certificateFiles');

        // Store the file using file_put_contents
        file_put_contents($publicPath . '/' . $imageName, base64_decode($image));
        return $imageName;
    }

    public function postAuditSchemePrint($company_id = null, $audit_type = null, $standard_number = null, $standard_stage_id = null, $ims = null)
    {
        $standard = Standards::where('id', $standard_number)->with(['scheme_manager.user', 'scheme_manager.scheme_coordinators.user'])->first();
        $standardStage = AJStandardStage::where('id', $standard_stage_id)->first();
        $companyStandard = CompanyStandards::where('company_id', $company_id)->where('standard_id', $standard->id)->first();
        $postAudit = PostAudit::where('company_id', $company_id)->where('standard_id', $standard->id)->where('aj_standard_stage_id', $standard_stage_id)->first();
        $notification = Notification::where('type_id', (int)$standardStage->id)->where('request_id', (int)$postAudit->id)->where('type', 'post_audit')->orderBy('id', 'desc')->first();
        $company = Company::where('id', $company_id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard->standard->name;
        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= " & " . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $standard->name;
        }

        if ($audit_type == 'Stage 1') {
            $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])->orderBy('question_id', 'asc')->get();
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/stage1.docx'));
            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);

            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);

            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0) {
                        $my_template->setValue('q1', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q1Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestion) && $postAuditQuestion->value == 'YES') {
                                $my_template->setValue('q1Answer', 'No');
                            } else {
                                $my_template->setValue('q1Answer', 'N/A');
                            }
                        }

                    } elseif ($key === 1) {
                        $my_template->setValue('q2', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q2Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                                $my_template->setValue('q2Answer', 'No');
                            } else {
                                $my_template->setValue('q2Answer', 'N/A');
                            }
                        }
                    } else {
                        $my_template->setValue('q' . ($key + 1) . '', $postAuditQuestion->question->title_for_sc_sm);
                        $my_template->setValue('q' . ($key + 1) . 'Answer', $postAuditQuestion->scheme_value);
                    }
                }
            }
            $my_template->setValue('approvalDate', (isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->approval_date) ? date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->approval_date)) : '');
            $my_template->setValue('comments', $notification ? $notification->body : 'N/A');
            $my_template->setValue('fullname', auth()->user()->fullName());

            try {
                $my_template->saveAs(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));
            } catch (Exception $e) {
                //handle exception
            }

            return response()->download(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));

        } elseif ($audit_type == 'Stage 2') {


            if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 17])->orderBy('question_id', 'asc')->get();
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/stage2_health_and_saftey.docx'));
            } else {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14])->orderBy('question_id', 'asc')->get();
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/stage2.docx'));
            }

            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);
            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);
            $index = 3;
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0) {
                        $my_template->setValue('q1', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q1Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestion) && $postAuditQuestion->value == 'YES') {
                                $my_template->setValue('q1Answer', 'No');
                            } else {
                                $my_template->setValue('q1Answer', 'N/A');
                            }
                        }

                    } elseif ($key === 1) {
                        $my_template->setValue('q2', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q2Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                                $my_template->setValue('q2Answer', 'No');
                            } else {
                                $my_template->setValue('q2Answer', 'N/A');
                            }
                        }
                    } else {
                        $my_template->setValue('q' . ($key + 1) . '', $postAuditQuestion->question->title_for_sc_sm);
                        $my_template->setValue('q' . ($key + 1) . 'Answer', $postAuditQuestion->scheme_value);
                    }
                }

            }

        } elseif ($audit_type == 'Surveillance 1' || $audit_type == 'Surveillance 2' || $audit_type == 'Surveillance 3' || $audit_type == 'Surveillance 4' || $audit_type == 'Surveillance 5') {

            if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17])->orderBy('question_id', 'asc')->get();
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/surveilliance_health_and_saftey.docx'));
            } else {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15])->orderBy('question_id', 'asc')->get();
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/suveilliance.docx'));
            }

            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);
            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);
            $index = 3;
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0) {
                        $my_template->setValue('q1', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q1Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestion) && $postAuditQuestion->value == 'YES') {
                                $my_template->setValue('q1Answer', 'No');
                            } else {
                                $my_template->setValue('q1Answer', 'N/A');
                            }
                        }

                    } elseif ($key === 1) {
                        $my_template->setValue('q2', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q2Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                                $my_template->setValue('q2Answer', 'No');
                            } else {
                                $my_template->setValue('q2Answer', 'N/A');
                            }
                        }
                    } else {
                        $my_template->setValue('q' . ($key + 1) . '', $postAuditQuestion->question->title_for_sc_sm);
                        $my_template->setValue('q' . ($key + 1) . 'Answer', $postAuditQuestion->scheme_value);
                    }
                }
            }

        } elseif ($audit_type == 'Reaudit') {
            if (strpos($ims_heading, 'ISO 45001:2018') !== false || strpos($ims_heading, 'OHSAS 18001:2007') !== false) {
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17])->orderBy('question_id', 'asc')->get();
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/reaudit_health_and_saftey.docx'));
            } else {
                $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/new_decision_sheets/reaudit.docx'));
                $postAuditQuestions = PostAuditQueestion::where('post_audit_id', $postAudit->id)->whereIn('question_id', [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16])->orderBy('question_id', 'asc')->get();
            }


            \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
            $my_template->setValue('clientname', $companyStandard->company->name);
            $my_template->setValue('audittype', $audit_type);
            $my_template->setValue('standard', $ims_heading);
            $my_template->setValue('jobnumber', $standardStage->job_number);
            $my_template->setValue('auditdate', date("d-m-Y", strtotime($postAudit->actual_audit_date)) . ' to ' . date("d-m-Y", strtotime($postAudit->actual_audit_date_to)));
            $my_template->setValue('country', $companyStandard->company->country->name);
            $index = 3;
            if (!empty($postAuditQuestions) && count($postAuditQuestions) > 2) {
                foreach ($postAuditQuestions as $key => $postAuditQuestion) {
                    if ($key == 0) {
                        $my_template->setValue('q1', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q1Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestion) && $postAuditQuestion->value == 'YES') {
                                $my_template->setValue('q1Answer', 'No');
                            } else {
                                $my_template->setValue('q1Answer', 'N/A');
                            }
                        }

                    } elseif ($key === 1) {
                        $my_template->setValue('q2', $postAuditQuestion->question->title_for_sc_sm);
                        if (!is_null($postAuditQuestion->scheme_value)) {
                            $my_template->setValue('q2Answer', $postAuditQuestion->scheme_value);
                        } else {
                            if (isset($postAuditQuestionsTwo) && $postAuditQuestionsTwo->value == 'YES') {
                                $my_template->setValue('q2Answer', 'No');
                            } else {
                                $my_template->setValue('q2Answer', 'N/A');
                            }
                        }
                    } else {
                        $my_template->setValue('q' . ($key + 1) . '', $postAuditQuestion->question->title_for_sc_sm);
                        $my_template->setValue('q' . ($key + 1) . 'Answer', $postAuditQuestion->scheme_value);
                    }
                }
            }

        }
        $my_template->setValue('approvalDate', (isset($postAudit->postAuditSchemeInfo) && $postAudit->postAuditSchemeInfo->approval_date) ? date("d-m-Y", strtotime($postAudit->postAuditSchemeInfo->approval_date)) : '');
        $my_template->setValue('comments', $notification ? $notification->body : 'N/A');
        $my_template->setValue('fullname', auth()->user()->fullName());

        try {
            $my_template->saveAs(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalDecisionSheet/' . $company_id . '-' . $audit_type . '-' . $standard_number . '-' . $standard_stage_id . '.docx'));


    }


}
