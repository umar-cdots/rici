<?php

namespace App\Http\Controllers;

use App\AJStandardStage;
use App\AuditorStandard;
use App\AuditorStandardWitnessEvaluation;
use App\Certificate;
use App\Company;
use App\CompanyStandards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CertificateController extends Controller
{
    public function index(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required',
        ], [
            'company_standard_id.required' => "Company Standard is Required.",
        ]);

        if (!$validator->fails()) {

            $certificates = Certificate::where('company_standard_id', $request->company_standard_id)->get();
            $company_standard_id = $request->company_standard_id;
            $certificate_current_status = Certificate::where('company_standard_id', $request->company_standard_id)->orderBy('id', 'desc')->first();
            $companyStandard = CompanyStandards::where('id', $company_standard_id)->whereIn('type', ['base', 'single'])->first(['standard_id', 'company_id']);
            $ajStandardStages = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStandard) {
                $q->where('standard_id', $companyStandard->standard_id)->whereHas('ajJustification', function ($q) use ($companyStandard) {
                    $q->where('company_id', $companyStandard->company_id);
                });
            })->doesntHave('postAudit')
                ->orWhereHas('postAudit', function ($query) use ($companyStandard) {
                    $query->where('status', 'rejected')
                        ->where('standard_id', $companyStandard->standard_id)
                        ->where('company_id', $companyStandard->company_id)
                        ->whereNull('deleted_at');
                })->get(['id', 'audit_type', 'recycle']);

            $data = view('certificate-modals.index', compact('certificates', 'certificate_current_status', 'company_standard_id', 'ajStandardStages'))->render();


            $response['status'] = 'success';
            $response['data'] = [
                'html' => $data,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function store(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required',
            'certificate_status' => 'required',
            'certificate_date' => 'required',
//            'aj_standard_id' => 'required',
        ], [
            'company_standard_id.required' => "Company Standard is Required.",
            'certificate_status.required' => "Certificate Status is Required.",
            'certificate_date.required' => "Certificate Date is Required.",
//            'aj_standard_id.required' => "AJ Standard Id is Required.",
        ]);

        if (!$validator->fails()) {
            if ($request->certificate_status == 'withdrawn') {
                $checkSuspension = Certificate::where('company_standard_id', $request->company_standard_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $withdrawnDate = date("Y-m-d", strtotime($request->certificate_date));

                if (!$checkSuspension) {
                    $response['status'] = 'no_suspension_found';
                    return $response;
                }

                $suspensionDate = date("Y-m-d", strtotime($checkSuspension->certificate_date));

                if ($checkSuspension->certificate_status != 'suspended') {
                    $response['status'] = 'withdrawn_error';
                    return $response;
                } elseif ($withdrawnDate <= $suspensionDate) {
                    $response['status'] = 'withdrawn_date_error';
                    return $response;
                }

// Additional logic here if needed

            }

            $companyStandard = CompanyStandards::where('id', $request->company_standard_id)->first();
            if (!is_null($companyStandard->ims_standard_ids)) {
                $companyStandardIds = explode(',', $companyStandard->ims_standard_ids);
                if (!empty($companyStandardIds) && count($companyStandardIds) > 0) {
                    foreach ($companyStandardIds as $key => $cstd) {
                        $checkCompanyStandard = Certificate::where('company_standard_id', $cstd)->where('report_status', 'new')->get();
                        foreach ($checkCompanyStandard as $reportStatus) {
                            $reportStatus->report_status = 'old';
                            $reportStatus->save();
                        }
                        $certificate = new Certificate();
                        $certificate->company_standard_id = $cstd;
                        if ($request->certificate_status == 'withdrawn') {
                            $certificate->aj_standard_id = (isset($request->aj_standard_id) && (!is_null($request->aj_standard_id))) ? $request->aj_standard_id : $reportStatus->aj_standard_id;
                        } else {
                            $certificate->aj_standard_id = $request->aj_standard_id;

                        }
                        if ($request->certificate_status == 'suspended') {
                            $certificate->suspension_reason = $request->suspension_reason;
                        }
                        $certificate->certificate_status = $request->certificate_status;
                        $certificate->certificate_date = date("Y/m/d", strtotime($request->certificate_date));
                        $certificate->report_status = 'new';
                        $certificate->is_print_letter = isset($request->is_print_letter) ? $request->is_print_letter === '1' ? true : false : false;
                        $certificate->save();
                    }
                }
            } else {
                $checkCompanyStandard = Certificate::where('company_standard_id', $request->company_standard_id)->where('report_status', 'new')->get();
                foreach ($checkCompanyStandard as $reportStatus) {
                    $reportStatus->report_status = 'old';
                    $reportStatus->save();
                }

                $certificate = new Certificate();
                $certificate->company_standard_id = $request->company_standard_id;
                if ($request->certificate_status == 'withdrawn') {
                    $certificate->aj_standard_id = (isset($request->aj_standard_id) && (!is_null($request->aj_standard_id))) ? $request->aj_standard_id : $reportStatus->aj_standard_id;
                } else {
                    $certificate->aj_standard_id = $request->aj_standard_id;

                }
                if ($request->certificate_status == 'suspended') {
                    $certificate->suspension_reason = $request->suspension_reason;
                }

                $certificate->certificate_status = $request->certificate_status;
                $certificate->certificate_date = date("Y/m/d", strtotime($request->certificate_date));
                $certificate->report_status = 'new';
                $certificate->is_print_letter = isset($request->is_print_letter) ? $request->is_print_letter === '1' ? true : false : false;
                $certificate->save();
            }
            $company = Company::find($companyStandard->company_id);
            $company->is_send_to_ias = false;
            $company->save();


            $response['status'] = 'success';
            $response['data'] = [
                'certificate' => $certificate,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function delete(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        $certificate = Certificate::where('id', $request->id)->first();
        $companyStandardId = $certificate->company_standard_id;
        $companyStandard = CompanyStandards::whereId($companyStandardId)->first(['id', 'is_ims', 'company_id']);
        if ($companyStandard->is_ims == true) {
            $company = Company::whereId($companyStandard->company_id)->first(['id']);
            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get(['id', 'is_ims', 'company_id']);
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    $certificate = Certificate::where('company_standard_id', $imsCompanyStandard->id)->where('report_status', 'new')->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
                    $oldToNewCertificate = Certificate::where('company_standard_id', $imsCompanyStandard->id)->where('report_status', 'old')->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
                    $oldToNewCertificate->report_status = 'new';
                    $oldToNewCertificate->save();
                    $certificate->delete();
                }
            }
        } else {
            $oldToNewCertificate = Certificate::where('company_standard_id', $companyStandardId)->where('report_status', 'old')->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
            $oldToNewCertificate->report_status = 'new';
            $oldToNewCertificate->save();
            $certificate->delete();
        }


        $response['status'] = 'success';

        return $response;
    }

    public function directory(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'company_standard_id' => 'required',
            'directory_updated' => 'required',
        ], [
            'company_standard_id.required' => "Company Standard is Required.",
            'directory_updated.required' => "Directory is Required.",
        ]);

        if (!$validator->fails()) {

            $checkCompanyStandard = CompanyStandards::where('id', $request->company_standard_id)->first();
            if (!is_null($checkCompanyStandard->ims_standard_ids)) {
                $companyStandardIds = explode(',', $checkCompanyStandard->ims_standard_ids);
                if (!empty($companyStandardIds) && count($companyStandardIds) > 0) {
                    foreach ($companyStandardIds as $key => $cstd) {
                        $companyStandard = CompanyStandards::where('id', $cstd)->first();
                        $companyStandard->directory_updated = $request->directory_updated;
                        $companyStandard->save();
                    }
                }
            } else {
                $companyStandard = CompanyStandards::where('id', $request->company_standard_id)->first();
                $companyStandard->directory_updated = $request->directory_updated;
                $companyStandard->save();
            }

            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }


}
