<?php

namespace App\Http\Controllers;

use App\Cities;
use App\Countries;
use App\OperationManager;
use App\Regions;
use App\ResidentCity;
use App\ResidentCountry;
use App\SchemeCoordinator;
use App\SchemeManager;
use App\SchemeManagerStandards;
use App\Standards;
use App\StandardsFamily;
use App\User;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Traits\GeneralHelperTrait;
use App\Traits\AccessRights;

class SchemeCoordinatorController extends Controller
{
    use GeneralHelperTrait;
    use AccessRights;

    public function index()
    {
        if (auth()->user()->user_type == 'admin') {

            $scheme_coordinators = User::where('user_type', 'scheme_coordinator')->get();
        } elseif (auth()->user()->user_type == 'scheme_manager') {
            $scheme_coordinators = User::whereHas('scheme_coordinator', function ($q) {
                $q->where('scheme_manager_id', auth()->user()->scheme_manager->id);
            })->where('user_type', 'scheme_coordinator')->get();
        } else if (auth()->user()->user_type == 'scheme_coordinator') {
            $scheme_coordinators = User::where('user_type', 'scheme_coordinator')->where('id', auth()->user()->id)->get();
        }


        return view('scheme-coordinator.index', compact('scheme_coordinators'));
    }

    public function create()
    {
        $standards = Standards::all();
        $countries = Countries::all();
        $scheme_managers = User::where('user_type', 'scheme_manager')->doesnthave('scheme_manager.scheme_coordinators')->get();
        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::where('role_id', '3')->get();
        return view('scheme-coordinator.create', compact('standards', 'countries', 'scheme_managers', 'roles', 'permissions'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
            'scheme_manager_id' => 'required|integer',
            'username' => ['required', 'string',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('username', $request->username)->whereNull('deleted_at');
                })
            ],
            'email' => ['required', 'string', 'email',
                Rule::unique('users')->where(function ($query) use ($request) {
                    $query->where('email', $request->email)->whereNull('deleted_at');
                })
            ],
            'password' => 'required|string|min:6|confirmed',
            'remarks' => 'required|string',
            'signature_file' => 'required|mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf,xlsx,docx',
            'roles.*' => 'integer',
            'permissions.*' => 'integer'
        ]);
        if (!$validator->fails()) {
            //uploading the images
            if ($request->has('signature_file')) {
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename  = public_path('uploads/scheme_coordinator/').$name;

                if(File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }


                $signature_file_with_time = $name . '_' . time();
                $destinationPath = public_path('/uploads/scheme_coordinator');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;
            } else {
                $signature_file = null;
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename  = public_path('uploads/scheme_coordinator/').$name;

                if(File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_coordinator');
                $profile_image_time = $name . '_' . time();
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;
            } else {
                $profile_image = null;
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename  = public_path('uploads/scheme_coordinator/').$name;

                if(File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_coordinator');
                $additional_files_with_time = $name . '_' . time();
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = null;
            }

            $user = User::create([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'scheme_coordinator',
            ]);
            //add record in scheme manager table
            $scheme_coordinator = new SchemeCoordinator();
            $scheme_coordinator->user_id = $user->id;
            $scheme_coordinator->scheme_manager_id = $request->scheme_manager_id;
            $scheme_coordinator->full_name = $request->fullname;
            $scheme_coordinator->joining_date = date("Y/m/d", strtotime($request->joining_date));
            $scheme_coordinator->landline = $request->landline;
            $scheme_coordinator->signature_image = $signature_file;
            $scheme_coordinator->profile_image = $profile_image;
            $scheme_coordinator->additional_files = $additional_files;
            $scheme_coordinator->save();

            //            //assigning roles and permissions to user
            $role = Role::where('id', 3)->first();
            $user->assignRole($role->name);
            foreach ($role->permissions as $permission) {
                $user->givePermissionTo($permission->name);
            }
            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Scheme Coordinator has been added successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function show($id)
    {

        $scheme_managers = SchemeManager::all();
        $user = User::findOrFail($id);
        $countries = Countries::all();
        $standards = StandardsFamily::all(); //standards
        $roles = Role::all();
        $permissions = Permission::all();
        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
//        $cities = ResidentCity::where('country_id', $user->country_id)->get();
        $regions = Regions::all();

        return view('scheme-coordinator.show', compact('user', 'countries', 'standards', 'roles', 'permissions', 'cities', 'regions', 'scheme_managers'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $scheme_managers = User::where('user_type', 'scheme_manager')->doesnthave('scheme_manager.scheme_coordinators')->get();
//        dd($user->scheme_coordinator->scheme_manager_id);
        $scheme_manager_current = User::where('user_type', 'scheme_manager')->where('id',$user->scheme_coordinator->scheme_manager->user_id)->first();

        $countries = Countries::all();
        $standards = StandardsFamily::all(); //standards
        $roles = Role::all();
        // $permissions = Permission::all();
        $permissions = Permission::whereIn('role_id', $user->roles->pluck('id')->toArray())->get();
        $country_zones = Zone::where('country_id', $user->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $user->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
//        $cities = ResidentCity::where('country_id', $user->country_id)->get();
        return view('scheme-coordinator.edit', compact('user', 'countries', 'standards', 'roles', 'permissions', 'cities', 'scheme_managers','scheme_manager_current'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'dob' => 'required',
            'cell_no' => 'required|string',
            'landline' => 'required|string',
            'joining_date' => 'required',
            'address' => 'required|string',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'r3' => 'required|string',
            'scheme_manager_id' => 'required|integer|exists:scheme_manager,id',
            'username' => 'required|string',
            'email' => 'required|string|email|max:255',
            'remarks' => 'required|string',
            'signature_file' => 'mimes:png,jpg,jpeg,pdf',
            'profile_image' => 'mimes:png,jpg,jpeg,pdf',
            'additional_files' => 'mimes:png,jpg,jpeg,pdf',
            'roles.*' => 'integer',
            'permissions.*' => 'integer'
        ]);
        if (!$validator->fails()) {
            $user = User::findOrFail($request->user_id);
            //uploading the images

            if ($request->has('signature_file')) {
                $image = $request->file('signature_file');
                $name = $image->getClientOriginalName();
                $filename  = public_path('uploads/scheme_coordinator/').$name;

                if(File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $signature_file_with_time = $name . '_' . time();
                $destinationPath = public_path('/uploads/scheme_coordinator');
                $image->move($destinationPath, $signature_file_with_time);
                $signature_file = $signature_file_with_time;

            } else {
                $signature_file = $user->scheme_coordinator['signature_image'];
            }
            if ($request->has('profile_image')) {
                $image = $request->file('profile_image');
                $name = $image->getClientOriginalName();
                $filename  = public_path('uploads/scheme_coordinator/').$name;

                if(File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }

                $destinationPath = public_path('/uploads/scheme_coordinator');
                $profile_image_time = $name . '_' . time();
                $image->move($destinationPath, $profile_image_time);
                $profile_image = $profile_image_time;

            } else {
                $profile_image = $user->scheme_coordinator['profile_image'];
            }
            if ($request->has('additional_files')) {
                $image = $request->file('additional_files');
                $name = $image->getClientOriginalName();
                $filename  = public_path('uploads/scheme_coordinator/').$name;

                if(File::exists($filename)) {
                    File::delete($filename);  // or unlink($filename);
                }
                $destinationPath = public_path('/uploads/scheme_coordinator');
                $additional_files_with_time = $name . '_' . time();
                $image->move($destinationPath, $additional_files_with_time);
                $additional_files = $additional_files_with_time;
            } else {
                $additional_files = $user->scheme_coordinator['additional_files'];
            }

            if (isset($request->password) && !is_null($request->password)) {
                $password = Hash::make($request->password);
            } else {
                $password = $user->password;
            }
            $user->update([
                'first_name' => $this->getFirstName($request->fullname),
                'middle_name' => $this->getMiddleName($request->fullname),
                'last_name' => $this->getLastName($request->fullname),
                'username' => $request->username,
                'password' => $password,
                'dob' => date("Y/m/d", strtotime($request->dob)),
                'working_experience' => '',
                'main_education' => '',
                'nationality_id' => 1,
                'remarks' => $request->remarks,
                'phone_number' => $request->cell_no,
                'email' => $request->email,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'status' => $request->r3,
                'address' => $request->address,
                'user_type' => 'scheme_coordinator'
            ]);
            //add record in scheme manager table
            $user->scheme_coordinator->update([
                'full_name' => $request->fullname,
                'joining_date' => date("Y/m/d", strtotime($request->joining_date)),
                'scheme_manager_id' => $request->scheme_manager_id,
                'landline' => $request->landline,
                'signature_image' => $signature_file,
                'profile_image' => $profile_image,
                'additional_files' => $additional_files
            ]);

            //assigning roles and permissions to user
            if ($request->has('permissions')) {
                $user->syncPermissions($request->permissions);
            }

            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }
            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Scheme Coordinator has been updated successfully');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function destroy($id)
    {
        $user = User::findOrfail($id);
        $user->scheme_coordinator()->delete();
        $user->delete();

        return back()->with(['flash_status' => 'success', 'flash_message' => 'Scheme Coordinator has been deleted.']);

    }

    public function getSchemeManagerStandards(Request $request)
    {
        $standards = SchemeManagerStandards::with('standard', 'standard_family')->where('scheme_manager_id', $request->id)->get();

        return response()->json(['data' => $standards, 'status' => 'success']);
    }
}
