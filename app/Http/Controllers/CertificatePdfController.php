<?php

namespace App\Http\Controllers;

use App\CompanyStandards;
use App\Certificate;
use App\Company;
use App\LetterName;
use App\PostAudit;
use App\Standards;
use App\AJStandard;
use App\AJStandardStage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CertificatePdfController extends Controller
{
    public function welcomeold($company_standard_id, $company_standard_stage_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->first();


        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

        $current_stage = AJStandardStage::find($company_standard_stage_id);

        if ($current_stage->audit_type == 'reaudit') {

            $recycle = $current_stage->recycle == null ? 1 : $current_stage->recycle;

            $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'audit_type' => 'surveillance_1', 'recycle' => $recycle])->get();

        } else {
            if ($companyStandard->is_ims == true) {
                $ajStandardStage = AJStandardStage::where('id', '>', $current_stage->id)->orderBy('id', 'asc')->where('aj_standard_id', $current_stage->aj_standard_id)->first();
            } else {
                $ajStandardStage = $current_stage->next();
            }
//            AJStandardStage::where('id', '>', $current_stage_1->id)->orderBy('id','asc')->first();
//            $ajStandardStage = $current_stage->next();


        }


        $current_stage_1 = AJStandardStage::find($company_standard_stage_id);


        $customPaper = array(0, 0, 567.00, 283.80);
        $pdf = \Barryvdh\DomPDF\Facade::loadView('certificates.welcome-letter', compact('companyStandard', 'ajStandardStage', 'current_stage_1'))->setPaper('A4', 'portrait');
        Storage::put('public/uploads/certificates/welcome-letter_' . $company_standard_id . '_' . $company_standard_stage_id . '.pdf', $pdf->output());

        $pathToFile = 'storage/uploads/certificates/welcome-letter_' . $company_standard_id . '_' . $company_standard_stage_id . '.pdf';

        return response()->file($pathToFile);

    }

    public function welcome2old($company_standard_id, $company_standard_stage_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

        $current_stage = AJStandardStage::find($company_standard_stage_id);
        $current_stage_1 = AJStandardStage::find($company_standard_stage_id);

        if ($current_stage->audit_type == 'reaudit') {

            $recycle = $current_stage->recycle == null ? 1 : $current_stage->recycle;

            $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'audit_type' => 'surveillance_1', 'recycle' => $recycle])->first();


        } else {

            if ($companyStandard->is_ims == true) {
                $ajStandardStage = AJStandardStage::where('id', '>', $current_stage->id)->orderBy('id', 'asc')->where('aj_standard_id', $current_stage->aj_standard_id)->first();
            } else {
                $ajStandardStage = $current_stage->next();
            }


//            $ajStandardStage = $current_stage->next();
//            $ajStandardStage = AJStandardStage::where('id', '>', $current_stage->next()->id)->orderBy('id', 'asc')->first();
        }

        $customPaper = array(0, 0, 567.00, 283.80);
        $pdf = \Barryvdh\DomPDF\Facade::loadView('certificates.welcome-letter2', compact('companyStandard', 'ajStandardStage', 'current_stage_1'))->setPaper('A4', 'portrait');

        Storage::put('public/uploads/certificates/welcome-letter2_' . $company_standard_id . '_' . $company_standard_stage_id . '.pdf', $pdf->output());

        $pathToFile = 'storage/uploads/certificates/welcome-letter2_' . $company_standard_id . '_' . $company_standard_stage_id . '.pdf';

        return response()->file($pathToFile);

    }

    public function suspensionold($company_standard_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->with(['certificateSuspension' => function ($query) {
            $query->where('report_status', 'new');
        }])->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();


        $ajStandardStage = AJStandardStage::where('id', $companyStandard->certificates->aj_standard_id)->first();

        if (is_null($ajStandardStage)) {
            if ($companyStandard->type == 'base') {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next()->next()->next();

            } else {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next();

            }
        }
        $customPaper = array(0, 0, 567.00, 283.80);
        $pdf = \Barryvdh\DomPDF\Facade::loadView('certificates.suspension-letter', compact('companyStandard', 'ajStandardStage'))->setPaper('A4', 'portrait');

        Storage::put('public/uploads/certificates/suspension-letter_' . $company_standard_id . '.pdf', $pdf->output());

        $pathToFile = 'storage/uploads/certificates/suspension-letter_' . $company_standard_id . '.pdf';

        // dd('certificates.welcome-letter');
        return response()->file($pathToFile);

    }

    public function withdrawold($company_standard_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->with(['certificateWithdrawn' => function ($query) {
            $query->where('report_status', 'new');
        }])->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

//        $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])->where('audit_type', '!=', 'stage_2 + Transfer')->orderBy('id', 'desc')->first();


        $ajStandardStage = AJStandardStage::where('id', $companyStandard->certificates->aj_standard_id)->first();

        if (is_null($ajStandardStage)) {
            if ($companyStandard->type == 'base') {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next()->next()->next();

            } else {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next();

            }
        }
        $customPaper = array(0, 0, 567.00, 283.80);
        $pdf = \Barryvdh\DomPDF\Facade::loadView('certificates.withdraw', compact('companyStandard', 'ajStandardStage'))->setPaper('A4', 'portrait');

        Storage::put('public/uploads/certificates/withdraw_' . $company_standard_id . '.pdf', $pdf->output());

        $pathToFile = 'storage/uploads/certificates/withdraw_' . $company_standard_id . '.pdf';

        // dd('certificates.welcome-letter');
        return response()->file($pathToFile);

    }

    public function certificate($company_standard_id, $company_standard_stage_id)
    {
        $companyStandard = CompanyStandards::where('id', $company_standard_id)->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

        $ajStandardStage = AJStandardStage::find($company_standard_stage_id);

        $iafCodes = [];

        if ($companyStandard->standard->standardFamily->name == 'Food') {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificates/stage1Food.docx'));
        } elseif (strtolower(str_replace(' ', '_', $companyStandard->standard->standardFamily->name)) == 'information_security') {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificates/stage1InformationSecurity.docx'));
        } else {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificates/stage1.docx'));
            if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                foreach ($companyStandard->companyStandardCodes as $companyStandardCodes) {
                    if (!is_null($companyStandardCodes->iaf)) {
                        array_push($iafCodes, $companyStandardCodes->iaf->code);
                    }

                }
            }
            if (!empty($iafCodes) && count($iafCodes) > 0) {
                $iafCodes = implode(',', $iafCodes);
                $my_template->setValue('iafCode', $iafCodes);
            } else {
                $my_template->setValue('iafCode', '');
            }
        }


        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $childAddresses = '';
        if (!empty($companyStandard->company->childCompanies) && count($companyStandard->company->childCompanies) > 0) {
            foreach ($companyStandard->company->childCompanies as $key => $child_company) {
                $childAddresses .= $child_company->address . ', ' . $child_company->city->name . ', ' . $child_company->country->name . '<w:br/>';
            }
        } else {
            $childAddresses = '';
        }


        $companyStandard1 = CompanyStandards::where('id', $company_standard_id)->first();
        $company = Company::where('id', $companyStandard1->company_id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard1->standard->name;


        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard1->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $ajStandards->standard->name;
        }


        if ($companyStandard->company->sister_company_name != '' && $companyStandard->company->sister_company_name != ' ') {
            $sisterCompany = ' - ' . $companyStandard->company->sister_company_name;
        } else {
            $sisterCompany = '';
        }

        $foodCode = '';
        $foodDescription = '';
        if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
            foreach ($companyStandard->companyStandardFoodCodes as $key => $companyStandardFoodCode) {
                $foodCode .= $companyStandardFoodCode->foodsubcategory->code;
                $foodDescription .= $companyStandardFoodCode->foodsubcategory->name;
                if ($key >= 0 && $key < count($companyStandard->companyStandardFoodCodes) - 1) {
                    $foodCode .= ' + ';
                    $foodDescription .= ' + ';
                }

            }
        }

        if ($companyStandard->standard->standardFamily->name == 'Food') {
            $my_template->setValue('subCategoryCode', $foodCode);
            $my_template->setValue('subCategoryDescription', $foodDescription);
        }
        if (strtolower(str_replace(' ', '_', $companyStandard->standard->standardFamily->name)) == 'information_security') {
            $my_template->setValue('soaVersion', $companyStandard->SOA ?? 'N/A');
            $ims_heading = str_replace(' ', '/IEC ', $ims_heading);
        }

        if ($company->out_of_country_region_name === '' || is_null($company->out_of_country_region_name)) {
            $companyAddress = $companyStandard->company->address . ', ' . $companyStandard->company->city->name . ', ' . $companyStandard->company->country->name;
        } else {
            $companyAddress = $companyStandard->company->address . ', ' . $company->out_of_country_region_name;
        }

        $my_template->setValue('companyName', $companyStandard->company->name . '' . $sisterCompany);
        $my_template->setValue('address', $companyAddress);
        $my_template->setValue('childAddresses', $childAddresses);
        $my_template->setValue('standard', $ims_heading);
        $my_template->setValue('scope', $companyStandard->company->scope_to_certify);
        $my_template->setValue('jobNumber', $ajStandardStage->job_number);
        $my_template->setValue('approvalDate', date('d-m-Y', strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)));
        $my_template->setValue('expiryDate', date('d-m-Y', strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->new_expiry_date)));
        $my_template->setValue('uciNumber', !is_null($ajStandardStage->postAudit->postAuditSchemeInfo->uci_number) ? $ajStandardStage->postAudit->postAuditSchemeInfo->uci_number : '');
        try {
            $my_template->saveAs(public_path('uploads/finalCertificates/' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '-' . str_replace("_", '-', $ajStandardStage->audit_type) . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalCertificates/' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '-' . str_replace("_", '-', $ajStandardStage->audit_type) . '.docx'));

    }

    public function certificateOthers($company_standard_id, $company_standard_stage_id)
    {
        $companyStandard = CompanyStandards::where('id', $company_standard_id)->with('companyStandardCodes.iaf')->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

        $ajStandardStage = AJStandardStage::find($company_standard_stage_id);


        $companyStandard1 = CompanyStandards::where('id', $company_standard_id)->first();
        $company = Company::where('id', $companyStandard1->company_id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        $ims_heading = $companyStandard1->standard->name;

        $iafCodes = [];
        if ($companyStandard1->standard->standardFamily->name == 'Food') {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificates/surveillanceFood.docx'));

        } elseif (strtolower(str_replace(' ', '_', $companyStandard1->standard->standardFamily->name)) == 'information_security') {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificates/surveillanceInformationSecurity.docx'));

        } else {
            $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificates/surveillance.docx'));

            if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                foreach ($companyStandard->companyStandardCodes as $companyStandardCodes) {
                    if (!is_null($companyStandardCodes->iaf)) {
                        array_push($iafCodes, $companyStandardCodes->iaf->code);
                    }

                }
            }
            if (!empty($iafCodes) && count($iafCodes) > 0) {
                $iafCodes = implode(',', $iafCodes);
                $my_template->setValue('iafCode', $iafCodes);
            } else {
                $my_template->setValue('iafCode', '');
            }
        }

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);


        $childAddresses = '';
        if (!empty($companyStandard->company->childCompanies) && count($companyStandard->company->childCompanies) > 0) {
            foreach ($companyStandard->company->childCompanies as $key => $child_company) {
                $childAddresses .= $child_company->address . ', ' . $child_company->city->name . ', ' . $child_company->country->name . '<w:br/>';
            }
        } else {
            $childAddresses = '';
        }


        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                if ($companyStandard1->id == $imsCompanyStandard->id) {
                    continue;
                } else {
                    $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                }
            }
        } else {
            $ims_heading = $ajStandards->standard->name;
        }


        if ($companyStandard->company->sister_company_name != '' && $companyStandard->company->sister_company_name != ' ') {
            $sisterCompany = ' - ' . $companyStandard->company->sister_company_name;
        } else {
            $sisterCompany = '';
        }

        $foodCode = '';
        $foodDescription = '';
        if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
            foreach ($companyStandard->companyStandardFoodCodes as $key => $companyStandardFoodCode) {
                $foodCode .= $companyStandardFoodCode->foodsubcategory->code;
                $foodDescription .= $companyStandardFoodCode->foodsubcategory->name;
                if ($key >= 0 && $key < count($companyStandard->companyStandardFoodCodes) - 1) {
                    $foodCode .= ' + ';
                    $foodDescription .= ' + ';
                }

            }
        }
        if ($companyStandard1->standard->standardFamily->name == 'Food') {
            $my_template->setValue('subCategoryCode', $foodCode);
            $my_template->setValue('subCategoryDescription', $foodDescription);
        }
        if (strtolower(str_replace(' ', '_', $companyStandard1->standard->standardFamily->name)) == 'information_security') {
            $my_template->setValue('soaVersion', $companyStandard->SOA ?? 'N/A');
            $ims_heading = str_replace(' ', '/IEC ', $ims_heading);
        }


        if ($company->out_of_country_region_name === '' || is_null($company->out_of_country_region_name)) {
            $companyAddress = $companyStandard->company->address . ', ' . $companyStandard->company->city->name . ', ' . $companyStandard->company->country->name;
        } else {
            $companyAddress = $companyStandard->company->address . ', ' . $company->out_of_country_region_name;
        }

        $my_template->setValue('companyName', $companyStandard->company->name . '' . $sisterCompany);
        $my_template->setValue('address', $companyAddress);
        $my_template->setValue('childAddresses', $childAddresses);
        $my_template->setValue('standard', $ims_heading);
        $my_template->setValue('scope', $companyStandard->company->scope_to_certify);
        $my_template->setValue('jobNumber', $ajStandardStage->job_number);
        $my_template->setValue('approvalDate', date('d-m-Y', strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->approval_date)));
        $my_template->setValue('expiryDate', date('d-m-Y', strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->new_expiry_date)));
        $my_template->setValue('originalIssueDate', date('d-m-Y', strtotime($ajStandardStage->postAudit->postAuditSchemeInfo->original_issue_date)));
        $my_template->setValue('uciNumber', !is_null($ajStandardStage->postAudit->postAuditSchemeInfo->uci_number) ? $ajStandardStage->postAudit->postAuditSchemeInfo->uci_number : '');
        try {
            $my_template->saveAs(public_path('uploads/finalCertificates/' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '-' . str_replace("_", '-', $ajStandardStage->audit_type) . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalCertificates/' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '-' . str_replace("_", '-', $ajStandardStage->audit_type) . '.docx'));

    }


    public function welcome($company_standard_id, $company_standard_stage_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->first();


        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

        $current_stage = AJStandardStage::find($company_standard_stage_id);

        if ($current_stage->audit_type == 'reaudit') {

            $recycle = $current_stage->recycle == null ? 1 : $current_stage->recycle;

            $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'audit_type' => 'surveillance_1', 'recycle' => $recycle])->get();

        } else {
            if ($companyStandard->is_ims == true) {
                $ajStandardStage = AJStandardStage::where('id', '>', $current_stage->id)->orderBy('id', 'asc')->where('aj_standard_id', $current_stage->aj_standard_id)->first();
            } else {
                $ajStandardStage = $current_stage->next();
            }
        }


        $current_stage_1 = AJStandardStage::find($company_standard_stage_id);
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificate_letters/welcome_letter_stage_2.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $postAuditApprovalDate = PostAudit::where('company_id', $companyStandard->company_id)->where('aj_standard_stage_id', $current_stage_1->id)->where('standard_id', $companyStandard->standard_id)->with('postAuditSchemeInfo')->first();
        $letter = LetterName::first();

        $riciName = $letter->rici_name ?? '';
        $riciAddress = $letter->rici_address ?? '';
        $doc_one = $letter->doc_name_one ?? '';
        $doc_two = $letter->doc_name_two ?? '';

        $ims_heading = [];
        $company = Company::where('id', $companyStandard->company_id)->first();
        $companyStandard = CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        if ($companyStandard->is_ims == false) {
            array_push($ims_heading, $companyStandard->standard->name);
        } else {
            array_push($ims_heading, $companyStandard->standard->name);
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    if ($companyStandard->id == $imsCompanyStandard->id) {
                        continue;
                    } else {
                        array_push($ims_heading, $imsCompanyStandard->standard->name);

                    }
                }
            }
        }


        $text = new \PhpOffice\PhpWord\Element\TextRun();
        foreach ($ims_heading as $key => $std) {
            $text->addText(' ' . $std);
            if ($key >= 0 && $key < count($ims_heading) - 1) {
                $text->addTextBreak(1);
            }
        }
        $my_template->setComplexBlock('list', $text);

        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('postAuditApprovalDate', ($postAuditApprovalDate && $postAuditApprovalDate->postAuditSchemeInfo) ? date("d F Y", strtotime($postAuditApprovalDate->postAuditSchemeInfo->approval_date)) : '');
        $my_template->setValue('title', $companyStandard->company->primaryContact->title);
        $my_template->setValue('name', ucfirst($companyStandard->company->primaryContact->name));
        $my_template->setValue('position', $companyStandard->company->primaryContact->position);
        $my_template->setValue('companyName', $companyStandard->company->name);
        $my_template->setValue('address', $companyStandard->company->address);
        $my_template->setValue('cityName', $companyStandard->company->city->name);
        $my_template->setValue('countryName', $companyStandard->company->country->name);
        $my_template->setValue('auditType', str_replace("_", " ", ucfirst($ajStandardStage->audit_type)));
        $my_template->setValue('expectedDate', date("F Y", strtotime($ajStandardStage->excepted_date)));
        $my_template->setValue('docOne', $doc_one);
        $my_template->setValue('docTwo', $doc_two);
        $my_template->setValue('schemeManagerName', $companyStandard->standard->scheme_manager[0]->full_name);
        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('riciAddress', $riciAddress);
        try {
            $my_template->saveAs(public_path('uploads/finalCertificates/welcome-letter-stage2-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalCertificates/welcome-letter-stage2-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));


    }

    public function welcome2($company_standard_id, $company_standard_stage_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();

        $current_stage = AJStandardStage::find($company_standard_stage_id);
        $current_stage_1 = AJStandardStage::find($company_standard_stage_id);

        if ($current_stage->audit_type == 'reaudit') {

            $recycle = $current_stage->recycle == null ? 1 : ($current_stage->recycle + 1);

            $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'audit_type' => 'surveillance_1', 'recycle' => $recycle])->first();


        } else {

            if ($companyStandard->is_ims == true) {
                $ajStandardStage = AJStandardStage::where('id', '>', $current_stage->id)->orderBy('id', 'asc')->where('aj_standard_id', $current_stage->aj_standard_id)->first();
            } else {
                $ajStandardStage = $current_stage->next();
            }

        }
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificate_letters/welcome_letter_surv_and_reaudit.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $postAuditApprovalDate = PostAudit::where('company_id', $companyStandard->company_id)->where('aj_standard_stage_id', $current_stage_1->id)->where('standard_id', $companyStandard->standard_id)->with('postAuditSchemeInfo')->first();

        $letter = LetterName::first();

        $riciName = $letter->rici_name ?? '';
        $riciAddress = $letter->rici_address ?? '';
        $doc_one = $letter->doc_name_one ?? '';

        $ims_heading = [];
        $company = \App\Company::where('id', $companyStandard->company_id)->first();
        $companyStandard = \App\CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        if ($companyStandard->is_ims == false) {
            array_push($ims_heading, $companyStandard->standard->name);
        } else {
            array_push($ims_heading, $companyStandard->standard->name);
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    if ($companyStandard->id == $imsCompanyStandard->id) {
                        continue;
                    } else {
                        array_push($ims_heading, $imsCompanyStandard->standard->name);

                    }
                }
            }
        }


        $text = new \PhpOffice\PhpWord\Element\TextRun();
        foreach ($ims_heading as $key => $std) {
            $text->addText(' ' . $std);
            if ($key >= 0 && $key < count($ims_heading) - 1) {
                $text->addTextBreak(1);
            }
        }
        $my_template->setComplexBlock('list', $text);

        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('postAuditApprovalDate', ($postAuditApprovalDate && $postAuditApprovalDate->postAuditSchemeInfo) ? date("d F Y", strtotime($postAuditApprovalDate->postAuditSchemeInfo->approval_date)) : '');
        $my_template->setValue('title', $companyStandard->company->primaryContact->title);
        $my_template->setValue('name', ucfirst($companyStandard->company->primaryContact->name));
        $my_template->setValue('position', $companyStandard->company->primaryContact->position);
        $my_template->setValue('companyName', $companyStandard->company->name);
        $my_template->setValue('address', $companyStandard->company->address);
        $my_template->setValue('cityName', $companyStandard->company->city->name);
        $my_template->setValue('countryName', $companyStandard->company->country->name);
        $my_template->setValue('auditType', str_replace("_", " ", ucfirst($ajStandardStage->audit_type)));
        $my_template->setValue('expectedDate', date("F Y", strtotime($ajStandardStage->excepted_date)));
        $my_template->setValue('docOne', $doc_one);
        $my_template->setValue('schemeManagerName', $companyStandard->standard->scheme_manager[0]->full_name);
        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('riciAddress', $riciAddress);
        try {
            $my_template->saveAs(public_path('uploads/finalCertificates/welcome-letter-surveillance-and-reaudit-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalCertificates/welcome-letter-surveillance-and-reaudit-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));


    }

    public function suspension($company_standard_id)
    {

        $companyStandard = CompanyStandards::where('id', $company_standard_id)->with(['certificateSuspension' => function ($query) {
            $query->where('report_status', 'new');
        }])->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();


        $ajStandardStage = AJStandardStage::where('id', $companyStandard->certificates->aj_standard_id)->first();

        if (is_null($ajStandardStage)) {
            if ($companyStandard->type == 'base') {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next()->next()->next();

            } else {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next();

            }
        }
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificate_letters/suspension_letter.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $letter = LetterName::first();

        $riciName = $letter->rici_name ?? '';
        $riciAddress = $letter->rici_address ?? '';
        $ims_heading = [];
        $company = \App\Company::where('id', $companyStandard->company_id)->first();
        $companyStandard = \App\CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        if ($companyStandard->is_ims == false) {
            array_push($ims_heading, $companyStandard->standard->name);
        } else {
            array_push($ims_heading, $companyStandard->standard->name);
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    if ($companyStandard->id == $imsCompanyStandard->id) {
                        continue;
                    } else {
                        array_push($ims_heading, $imsCompanyStandard->standard->name);

                    }
                }
            }
        }


        $text = new \PhpOffice\PhpWord\Element\TextRun();
        foreach ($ims_heading as $key => $std) {
            $text->addText(' ' . $std);
            if ($key >= 0 && $key < count($ims_heading) - 1) {
                $text->addTextBreak(1);
            }
        }
        $my_template->setComplexBlock('list', $text);

        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('postAuditApprovalDate', $companyStandard->certificates && $companyStandard->certificateSuspension ? date("d F Y", strtotime($companyStandard->certificateSuspension->certificate_date)) : '');
        $my_template->setValue('title', $companyStandard->company->primaryContact->title);
        $my_template->setValue('name', ucfirst($companyStandard->company->primaryContact->name));
        $my_template->setValue('position', $companyStandard->company->primaryContact->position);
        $my_template->setValue('companyName', $companyStandard->company->name);
        $my_template->setValue('address', $companyStandard->company->address);
        $my_template->setValue('cityName', $companyStandard->company->city->name);
        $my_template->setValue('countryName', $companyStandard->company->country->name);
        $my_template->setValue('auditType', str_replace("_", " ", ucfirst($ajStandardStage->audit_type)));
        $my_template->setValue('expectedDate', date("d/m/Y", strtotime($ajStandardStage->excepted_date)));
        $my_template->setValue('schemeManagerName', $companyStandard->standard->scheme_manager[0]->full_name);
        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('riciAddress', $riciAddress);
        try {
            $my_template->saveAs(public_path('uploads/finalCertificates/suspension-letter-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalCertificates/suspension-letter-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));


    }

    public function withdraw($company_standard_id)
    {
        $companyStandard = CompanyStandards::where('id', $company_standard_id)->with(['certificateWithdrawn' => function ($query) {
            $query->where('report_status', 'new');
        }])->first();

        $ajStandards = AJStandard::where('audit_justification_id', $companyStandard->company->auditJustifications[0]->id)->where('standard_id', $companyStandard->standard_id)->first();


        $ajStandardStage = AJStandardStage::where('id', $companyStandard->certificates->aj_standard_id)->first();

        if (is_null($ajStandardStage)) {
            if ($companyStandard->type == 'base') {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next()->next()->next();

            } else {
                $ajStandardStage = AJStandardStage::where(['aj_standard_id' => $ajStandards->id, 'status' => 'approved', 'special_aj_remarks' => null])
                    ->where('audit_type', '!=', 'stage_2 + Transfer')->whereHas('postAudit', function ($q) {
                        $q->where('status', 'approved');
                    })->orderBy('id', 'desc')->first()->next();

            }
        }
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('uploads/certificate_letters/withdrawn_letter.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $letter = LetterName::first();

        $riciName = $letter->rici_name ?? '';
        $riciAddress = $letter->rici_address ?? '';
        $ims_heading = [];
        $company = \App\Company::where('id', $companyStandard->company_id)->first();
        $companyStandard = \App\CompanyStandards::where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard->id)->first();
        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
        if ($companyStandard->is_ims == false) {
            array_push($ims_heading, $companyStandard->standard->name);
        } else {
            array_push($ims_heading, $companyStandard->standard->name);
            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                    if ($companyStandard->id == $imsCompanyStandard->id) {
                        continue;
                    } else {
                        array_push($ims_heading, $imsCompanyStandard->standard->name);

                    }
                }
            }
        }


        $text = new \PhpOffice\PhpWord\Element\TextRun();
        foreach ($ims_heading as $key => $std) {
            $text->addText(' ' . $std);
            if ($key >= 0 && $key < count($ims_heading) - 1) {
                $text->addTextBreak(1);
            }
        }
        $my_template->setComplexBlock('list', $text);

        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('postAuditApprovalDate', $companyStandard->certificates && $companyStandard->certificateWithdrawn ? date("d F Y", strtotime($companyStandard->certificateWithdrawn->certificate_date)) : '');
        $my_template->setValue('title', $companyStandard->company->primaryContact->title);
        $my_template->setValue('name', ucfirst($companyStandard->company->primaryContact->name));
        $my_template->setValue('position', $companyStandard->company->primaryContact->position);
        $my_template->setValue('companyName', $companyStandard->company->name);
        $my_template->setValue('address', $companyStandard->company->address);
        $my_template->setValue('cityName', $companyStandard->company->city->name);
        $my_template->setValue('countryName', $companyStandard->company->country->name);
        $my_template->setValue('auditType', str_replace("_", " ", ucfirst($ajStandardStage->audit_type)));
        $my_template->setValue('expectedDate',$companyStandard->certificates && $companyStandard->certificateSuspension ? date("d/m/Y",strtotime($companyStandard->certificateSuspension->certificate_date)) : '');
        $my_template->setValue('schemeManagerName', $companyStandard->standard->scheme_manager[0]->full_name);
        $my_template->setValue('riciName', $riciName);
        $my_template->setValue('riciAddress', $riciAddress);
        try {
            $my_template->saveAs(public_path('uploads/finalCertificates/withdrawn-letter-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        return response()->download(public_path('uploads/finalCertificates/withdrawn-letter-' . $companyStandard->id . '-' . $companyStandard->company->id . '-' . $ajStandardStage->id . '.docx'));


    }


}
