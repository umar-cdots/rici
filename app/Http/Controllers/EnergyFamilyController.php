<?php

namespace App\Http\Controllers;

use App\EnergyCode;
use App\EnergyFamilySheet;
use App\EnergyEnms;
use App\EnergyWeightage;
use App\Imports\EnergyImport;
use App\Imports\MandaysImport;
use App\ManDaySheet;
use App\StandardFamilySheet;
use App\Standards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class EnergyFamilyController extends Controller
{
    public function importExcel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if (!$validator->fails()) {
            $energy_sheet_family = new StandardFamilySheet();
            EnergyFamilySheet::truncate();
            if ($request->hasFile('file')) {
                $image = $request->file('file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/sheet_documents');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                StandardFamilySheet::where('name', 'energy_family')->delete();

                $energy_sheet_family->name = 'energy_family';
                $energy_sheet_family->file = $name;
                $energy_sheet_family->save();
            }

            $file = $request->file('file');
            Excel::import(new EnergyImport(), public_path('/uploads/sheet_documents/sample_energy_family_sheet.xlsx'));
            $sheets = EnergyFamilySheet::all();

            $records = view('manday-calculator.sheets-partials.energy-sheet', compact('sheets'))->render();
            $data = [
                'status' => 200,
                'records' => $records,
                'energy_sheet_family' => $energy_sheet_family->file,

            ];
            $response = $data;
        } else {
            EnergyFamilySheet::truncate();
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function storeWeightage(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
//           'standard_id' => 'required|integer|exists:standards,id',
            'from1' => 'required',
            'to1' => 'required',
            'from2' => 'required',
            'to2' => 'required',
            'from3' => 'required',
            'to3' => 'required',
            'ec' => 'required',
            'es' => 'required',
            'seu' => 'required',
        ]);
        if (!$validator->fails()) {
            //deleting previous records
            EnergyEnms::truncate();

            EnergyEnms::insert([
                [
                    'from' => $request['from1'],
                    'to' => $request['to1'],
                    'complexity' => 'low'
                ],
                [
                    'from' => $request['from2'],
                    'to' => $request['to2'],
                    'complexity' => 'medium'
                ],
                [
                    'from' => $request['from3'],
                    'to' => $request['to3'],
                    'complexity' => 'high'
                ],
            ]);

            //deleting previous records
            EnergyWeightage::truncate();
            EnergyWeightage::insert([
                [
                    'name' => 'EC',
                    'weightage' => $request['ec']
                ],
                [
                    'name' => 'ES',
                    'weightage' => $request['es']
                ],
                [
                    'name' => 'SEU',
                    'weightage' => $request['seu']
                ]
            ]);

            $data = [
                'status' => 'success'
            ];
            $response = (new ApiMessageController())->successResponse($data, 'Energy Sheet Parameters has been added successfully.');
        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }

        return $response;
    }

    public function filter(Request $request)
    {
        $sheets = EnergyFamilySheet::where('standard_id', $request->id)->with('standard')->get();
        $energy_emns = EnergyEnms::where('standard_id', $request->id)->get();
        $energy_weightage = EnergyWeightage::where('standard_id', $request->id)->get();

        return response()->json([
            'status' => 'success',
            'sheets' => $sheets,
            'energy_emns' => $energy_emns,
            'energy_weightage' => $energy_weightage
        ]);
    }
}
