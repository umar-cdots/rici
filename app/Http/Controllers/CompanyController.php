<?php

namespace App\Http\Controllers;

use App\AJStandardStage;
use App\AuditJustification;
use App\Auditor;
use App\CompanyStandardAccreditation;
use App\CompanyStandardCode;
use App\CompanyStandardEnergyCode;
use App\CompanyStandardFoodCode;
use App\EffectiveEmployeesFormula;
use App\IAF;
use App\IAS;
use App\IASCertificateError;
use App\IASCertificateStats;
use App\RegionalOperationCoordinatorCities;
use App\ResidentCity;
use App\ResidentCountry;
use App\SchemeInfoPostAudit;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Company;
use App\CompanyContacts;
use App\Regions;
use App\Countries;
use App\Cities;
use App\StandardsFamily;
use App\Standards;
use App\Accreditation;
use App\CompanyStandards;
use App\AuditActivityStages;
use App\CompanyStandardsAnswers;
use App\CompanyStandardDocument;
use Propaganistas\LaravelPhone\PhoneNumber;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Models\Permission;
use function foo\func;

class CompanyController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:show_company', ['only' => ['show']]);
        $this->middleware('permission:create_company', ['only' => ['create']]);
        $this->middleware('permission:edit_company', ['only' => ['edit']]);
        $this->middleware('permission:delete_company', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companyStandards = [];
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'auditor' || auth()->user()->user_type == 'management') {
            $companies = Company::where('is_parent_company', true)->get(['city_id', 'region_id', 'country_id', 'is_parent_company', 'company_form_status', 'name', 'id','is_edit_access_to_operations']);


            foreach ($companies as $key => $company) {
                $companyStandards[$key] = CompanyStandards::where('company_id', $company->id)->with(['standard', 'certificates'])->where('deleted_at', NULL)->get(['id', 'company_id', 'standard_id', 'is_ims']);
            }

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $companies = Company::whereRaw('name <> ""')->where('is_parent_company', true)->whereHas('companyStandards.standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get(['city_id', 'region_id', 'country_id', 'is_parent_company', 'company_form_status', 'name', 'id','is_edit_access_to_operations']);

            foreach ($companies as $key => $company) {

                $companyStandards[$key] = CompanyStandards::where('company_id', $company->id)->with(['standard', 'certificates'])->where('deleted_at', NULL)->get(['id', 'company_id', 'is_ims', 'standard_id']);
            }

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            if (auth()->user()->user_type == 'operation_coordinator') {

                $cityOperationsCoordinator = RegionalOperationCoordinatorCities::where('regional_operation_coordinator_id', auth()->user()->operation_coordinator->id)->whereNull('deleted_at')->pluck('city_id')->toArray();
                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->whereIn('city_id', $cityOperationsCoordinator)->where('is_parent_company', true)->get(['city_id', 'region_id', 'country_id', 'is_parent_company', 'company_form_status', 'name', 'id','is_edit_access_to_operations']);

                foreach ($companies as $key => $company) {
                    $companyStandards[$key] = CompanyStandards::where('company_id', $company->id)->with(['standard', 'certificates'])->where('deleted_at', NULL)->get(['id', 'company_id', 'is_ims', 'standard_id']);
                }
            } else {

                $companies = Company::whereRaw('name <> ""')->where('region_id', auth()->user()->region_id)->where('is_parent_company', true)->get(['city_id', 'region_id', 'country_id', 'is_parent_company', 'company_form_status', 'name', 'id','is_edit_access_to_operations']);
                foreach ($companies as $key => $company) {
                    $companyStandards[$key] = CompanyStandards::where('company_id', $company->id)->with(['standard', 'certificates'])->where('deleted_at', NULL)->get(['id', 'company_id', 'is_ims', 'standard_id']);
                }
            }
        }
        return view('company.index')->with(['companies' => $companies, 'companyStandards' => $companyStandards]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->user_type == 'admin') {
            $regions = Regions::all();
            $standards_families = StandardsFamily::orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $standards_families = StandardsFamily::orderBy('sort')->get();
        }


        $countries = Countries::all();
        $shifts = Company::getEnumValues()->get('shift');
        $iafs = \App\IAF::all();

        $currencies = Countries::orderBy('currency_code')->pluck('currency_name', 'currency_code');
        $surveillance_frequencies = CompanyStandards::getEnumValues()->get('surveillance_frequency');
        $accreditations = Accreditation::orderBy('name')->get();
        $audit_activity_stages = AuditActivityStages::orderBy('id')->get();
        $proposed_complexities = CompanyStandards::getEnumValues()->get('proposed_complexity');
        $contact_titles = CompanyContacts::getEnumValues()->get('title');
        $effective_employees = EffectiveEmployeesFormula::first();


        return view('company.create')->with(['countries' => $countries, 'effective_employees' => $effective_employees, 'regions' => $regions, 'shifts' => $shifts, 'iafs' => $iafs, 'standards_families' => $standards_families, 'currencies' => $currencies, 'surveillance_frequencies' => $surveillance_frequencies, 'accreditations' => $accreditations, 'audit_activity_stages' => $audit_activity_stages, 'proposed_complexities' => $proposed_complexities, 'contact_titles' => $contact_titles, 'certificate_types' => Company::CERTIFICATE_TYPES]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $response = array('status' => '', 'message' => "", 'data' => array());

        if ($request->step == 1) {
            $validator = Validator::make($request->all(), [
                'company_id' => 'sometimes|required|exists:companies,id',
                'name' => 'required',
                'region_id' => 'required|exists:regions,id',
                'country_id' => 'required|exists:countries,id',
                'address' => 'required',
                'city_id' => 'required|exists:cities,id',
                'website' => 'sometimes',
                'total_employees' => 'required|numeric|min:1',
                'key_process' => 'required',
                'child_company.address.*' => 'sometimes|required',
                'child_company.city_id.*' => 'sometimes|required|exists:cities,id',
                'child_company.key_process.*' => 'sometimes|required',
                'child_company.total_employees.*' => 'sometimes|required|numeric|min:1',
                'permanent_employees' => 'required|numeric|min:0',
                'part_time_employees' => 'required|numeric|min:0',
                'repeatative_employees' => 'required|numeric|min:0',
                'shift' => ['required', Rule::in(Company::getEnumValues()->get('shift'))],
                'sister_company_name' => 'string',
                'general_remarks' => 'sometimes',
                'scope_to_certify' => 'required',
                'outsourced_process_details' => 'required_if:enable_outsourced_process_details,1'
            ], [
                'region_id.required' => "Region Required.",
                'region_id.exists' => "Invalid Region Selected.",
                'country_id.required' => "Country Required.",
                'country_id.exists' => "Invalid Country Selected.",
                'city_id.required' => "City Required.",
                'city_id.exists' => "Invalid City Selected.",
                'child_company.address.*.required' => "Address Required.",
                'child_company.country_id.*.required' => "Country Required.",
                'child_company.country_id.*.exists' => "Country Required.",
                'child_company.city_id.*.required' => "City Required.",
                'child_company.city_id.*.exists' => "City Required.",
                'child_company.key_process.*.required' => 'Key Process Required.',
                'child_company.total_employees.*.required' => 'Employees Required.',
            ]);

            if (!$validator->fails()) {
                if (!empty($request->company_id)) {
                    $company = Company::find($request->company_id);
                } else {
                    $company = new Company;
                }
                if (is_object($company) && $company instanceof Company) {
                    $company->name = $request->name;
                    $company->region_id = $request->region_id;
                    $company->country_id = $request->country_id;
                    $company->address = $request->address;
                    $company->city_id = $request->city_id;
                    $company->website = $request->website ?? ' ';
                    $company->multiple_sides = $request->multiple_sides ?? 0;
                    $company->out_of_country_region_name = $request->out_of_country_region_name ?? '';
                    $company->total_employees = $request->total_employees;
                    $company->key_process = $request->key_process;
                    $company->permanent_employees = $request->permanent_employees;
                    $company->part_time_employees = $request->part_time_employees;
                    $company->repeatative_employees = $request->repeatative_employees;
                    $company->effective_employees = $request->effective_employees;
                    $company->shift = $request->shift;
                    $company->sister_company_name = $request->sister_company_name ?? ' ';
                    $company->is_parent_company = true;
                    $company->company_form_progress = 25;
                    $company->company_form_status = "processing";
                    $company->general_remarks = $request->general_remarks ?? ' ';
                    $company->confidential_certified_entity = $request->confidential_certified_entity ?? 'No';
                    $company->shift_remarks = $request->shift_remarks ?? 'N/A';
                    $company->scope_to_certify = $request->scope_to_certify;
                    // $company->iaf_id                        = $request->iaf_id;
                    // $company->ias_id                        = $request->ias_id;
                    $company->outsourced_process_details = $request->outsourced_process_details;
                    $company->is_send_to_ias = false;
                    $company->is_edit_access_to_operations = true;
                    $company->save();

                    if (!empty($request->company_id)) {
                        Company::where('company_id', $request->company_id)->delete();
                    }

                    $child_companies = array();
                    if (isset($request->child_company['address']) && is_array($request->child_company['address'])) {
                        foreach ($request->child_company['address'] as $i => $v) {
                            $child_company = new Company;
                            $child_company->address = $request->child_company['address'][$i] ?? ' ';
                            $child_company->country_id = $request->child_company['country_id'][$i] ?? ' ';
                            $child_company->city_id = $request->child_company['city_id'][$i] ?? ' ';
                            $child_company->key_process = $request->child_company['key_process'][$i] ?? ' ';
                            $child_company->total_employees = $request->child_company['total_employees'][$i] ?? ' ';
                            $child_company->is_parent_company = false;
                            $child_company->company_id = $company->id;
                            $child_company->save();
                            $child_companies[] = $child_company;
                        }
                    }

                    $response['status'] = 'success';
                    $response['message'] = isset($request->company_id) ? "Company Profile Updated." : "Company Profile Saved.";
                    $response['data'] = [
                        'company' => $company,
                        'child_companies' => $child_companies,
                        'current_step' => 1,
                        'next_step' => 2
                    ];
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "Unable to Update Company Profile.";
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
            }
        } elseif ($request->step == 2) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'bail|required|exists:companies,id',
                'scope_to_certify' => 'required',
                'iaf_id.0' => 'required|exists:i_a_fs,id',
                'ias_id.0' => 'required|exists:i_a_s,id',
                'outsourced_process_details' => 'required_if:enable_outsourced_process_details,1'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First.",
                'iaf_id.0.required' => "IAF Code is Required.",
                'iaf_id.0.exists' => "Invalid IAF Code Selected.",
                'ias_id.0.required' => "IAF Code is Required.",
                'ias_id.0.exists' => "Invalid IAF Code Selected."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $company->scope_to_certify = $request->scope_to_certify;
                // $company->iaf_id                        = $request->iaf_id;
                // $company->ias_id                        = $request->ias_id;
                $company->outsourced_process_details = $request->outsourced_process_details;
                $company->company_form_progress += 20;
                $company->save();

                $company->companyIAFs()->forceDelete();
                $company->companyIASs()->forceDelete();
                $iafs = [];
                $iass = [];
                if (!empty($request->iaf_id)) {
                    foreach ($request->iaf_id as $key => $iaf) {
                        $iafs[] = new \App\CompanyIAF(['iaf_id' => $iaf]);
                    }
                }

                if (!empty($request->iaf_id)) {
                    foreach ($request->ias_id as $key => $ias) {
                        $iass[] = new \App\CompanyIAS(['ias_id' => $ias]);
                    }
                }

                $company->companyIAFs()->saveMany($iafs);
                $company->companyIASs()->saveMany($iass);

                if ($request->action == 'update') {
                    $company->company_form_progress += 20;
                    $company->is_send_to_ias = false;
                    $company->save();
                }

                $response['status'] = 'success';
                $response['message'] = "Company Profile Saved.";
                $response['data'] = [
                    'company' => $company,
                    'current_step' => 2,
                    'next_step' => 3
                ];
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
                if (isset($response['data']['company_id'])) {
                    $response['message'] = $response['data']['company_id'][0];
                }
            }
        } elseif ($request->step == 3) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'bail|required|exists:companies,id',
                'title.*' => 'required|in:Mr,Mrs,Ms,Dr',
                'name.*' => 'required',
                'position.*' => 'required',
                'contact.*' => 'required',
                'landline.*' => 'sometimes',
                'email.*' => 'sometimes|nullable|email',
                'primary' => 'required|numeric',
                // 'primary_name'      => 'required|min:4',
                // 'primary_position'  => 'sometimes',
                // 'primary_contact'   => 'required',
                // 'primary_email'     => 'sometimes|nullable|email',
                // 'secondary_name.*'      => 'required|min:4',
                // 'secondary_position.*'  => 'sometimes',
                // 'secondary_contact.*'   => 'sometimes|required',
                // 'secondary_email.*'     => 'sometimes|nullable|email',
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First.",
                // 'primary_name.required'         => "Name is Required.",
                // 'primary_name.min'              => "Minimum 4 Characters Required.",
                // 'primary_contact.required'      => "Contact Number is Required.",
                // 'primary_email.email'           => "Email Format is Invalid.",
                // 'secondary_name.*.required'     => "Name is Required.",
                // 'secondary_name.*.min'          => "Minimum 4 Characters Required.",
                // 'secondary_contact.*.required'  => "Contact Number is Required.",
                // 'secondary_email.*.email'       => "Email Format is Invalid.",
                'title.*.required' => "Title is Required.",
                'title.*.in' => "Invalid Title Selected.",
                'name.*.required' => "Name is Required.",
                // 'name.*.min'          => "Minimum 4 Characters Required.",
                'position.*.required' => "Position is Required.",
                'contact.*.required' => "Contact Number is Required.",
                'email.*.email' => "Email Format is Invalid.",
                'primary.required' => "Primary Contact Required.",
                'primary.numeric' => "Primary Contact Required.",
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);

                $validator = Validator::make($request->all(), [
                    // 'primary_contact'       => 'phone:' . $company->country->iso,
                    // 'secondary_contact.*'   => 'phone:' . $company->country->iso,
                    // 'contact.*'         => 'phone:' . $company->country->iso,
                ], [
                    // 'primary_contact.phone'     => "Phone Number Format is Invalid.",
                    // 'secondary_contact.*.phone' => "Phone Number Format is Invalid.",
                    // 'contact.*.phone'   => "Phone Number Format is Invalid.",
                ]);

                if (!$validator->fails()) {
                    $company->contacts()->forceDelete();
                    $company_contacts = [];

                    // $company_contacts[]             = new CompanyContacts(['name' => $request->primary_name, 'position' => $request->primary_position, 'primary_contact' => PhoneNumber::make($request->primary_contact, $company->country->iso)->formatE164(), 'primary_email' => $request->primary_email, 'type' => "primary"]);



                    foreach ($request->name as $key => $name) {
                        $company_contacts[] = new CompanyContacts([
                            'name' => $name,
                            'title' => $request->title[$key],
                            'position' => $request->position[$key],
                            'contact' => $request->contact[$key],
                            'landline' => $request->landline[$key],
                            'email' => isset($request->email[$key]) ? $request->email[$key] : '',
                            'type' =>  ($request->primary === '1' || $request->primary === 1)  ? "primary" : "secondary"
                        ]);
                    }

                    $company->contacts()->saveMany($company_contacts);
                    if ($request->action == 'update') {
                        $company->company_form_progress += 25;
                        $company->is_send_to_ias = false;
                        $company->save();
                    }

                    $response['status'] = 'success';
                    $response['message'] = "Company Profile Saved.";
                    $response['data'] = [
                        'company' => $company,
                        'company_contacts' => $company->contacts->toArray(),
                        'current_step' => 3,
                        'next_step' => 4
                    ];
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "Form Has Some Errors.";
                    $response['data'] = $validator->errors()->toArray();
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
                if (isset($response['data']['company_id'])) {
                    $response['message'] = $response['data']['company_id'][0];
                }
            }
        } elseif ($request->step == 4) {

            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'required|exists:companies,id'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);


                if (!empty($request->standard_id)) {
                    $validator = Validator::make($request->all(), [
                        'standard_id' => 'required|exists:standards,id',
                        'standard_action' => 'sometimes'
                    ], [
                        'standard_id.required' => "Please Complete Step 1 First.",
                        'standard_id.exists' => "Please Complete Step 1 First.",
                    ]);

                    if (!$validator->fails()) {
                        $company = Company::find($request->company_id);
                        $company->is_send_to_ias = false;
                        $company->save();
                        if ($request->standard_action == 'remove') {

                            $standard = Standards::find($request->standard_id);
                            $company_standard = CompanyStandards::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->whereNull('deleted_at')->first();
                            if (!is_null($company_standard)) {
                                CompanyStandardCode::where('company_standard_id', $company_standard->id)->delete();
                                CompanyStandardEnergyCode::where('company_standard_id', $company_standard->id)->delete();
                                CompanyStandardFoodCode::where('company_standard_id', $company_standard->id)->delete();

                                $company_standard->delete();
                                CompanyStandardsAnswers::where('company_standards_id', $company_standard->id)->delete();
                                $response['status'] = 'success';
                                $response['message'] = "{$standard->name} Removed From Company Profile.";
                            } else {
                                CompanyStandardCode::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->delete();
                                CompanyStandardEnergyCode::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->delete();
                                CompanyStandardFoodCode::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->delete();
                                $response['status'] = 'success';
                                $response['message'] = "{$standard->name} Removed From Company Profile";
                            }

                        } else {
                            $standard = Standards::find($request->standard_id);

                            $validator = Validator::make($request->all(), [
                                'company_standard_id' => 'sometimes',
                                'client_type' => 'sometimes|in:' . collect(CompanyStandards::getEnumValues()->get('client_type'))->implode(','),
                                'old_cb_name' => 'required_if:client_type,transfered|min:3',
                                'old_cb_certificate_issue_date' => 'required_if:client_type,transfered|date',
                                'old_cb_certificate_expiry_date' => 'required_if:client_type,transfered|date|after:old_cb_certificate_issue_date',
                                'old_cb_surveillance_frequency' => 'required_if:client_type,transfered|in:' . collect(CompanyStandards::getEnumValues()->get('surveillance_frequency'))->implode(','),
                                'old_cb_audit_activity_stage_id' => 'required_if:client_type,transfered|in:' . AuditActivityStages::all()->pluck('id')->implode(','),
                                'old_cb_last_audit_report_attached' => 'sometimes',
                                'old_cb_last_audit_report_doc' => !empty($request->company_standard_action) ? 'sometimes|nullable|file' : 'required_if:client_type,transfered|file',
                                'old_cb_certificate_copy_attached' => 'sometimes',
                                'old_cb_certificate_copy_doc' => !empty($request->company_standard_action) ? 'sometimes|nullable|file' : 'required_if:client_type,transfered|file',
                                'old_cb_others_attached' => 'sometimes',
                                'old_cb_others_doc' => 'sometimes|nullable|file',
                                'old_cb_audit_activity_date.*' => 'sometimes|date',
                                'general_remarks' => 'sometimes',
//                                'proposed_complexity' => (!$standard->auto_calculate_proposed_complexity ? 'required' : 'sometimes|nullable') . '|in:' . collect(CompanyStandards::getEnumValues()->get('proposed_complexity'))->implode(','),
                                'surveillance_frequency' => 'required|in:' . collect(CompanyStandards::getEnumValues()->get('surveillance_frequency'))->implode(','),
                                'currency_unit' => 'required|exists:countries,currency_code',
                                'initial_certification_amount' => [Rule::requiredIf(function () use ($request) {
                                    if ($request->client_type != 'transfered') {
                                        return true;
                                    }
                                })
                                ],
                                'transfer_year' => [Rule::requiredIf(function () use ($request) {
                                    if ($request->is_transfer_standard == "1") {
                                        return true;
                                    }
                                })
                                ],
                                'surveillance_amount' => 'required|numeric',

                            ], [
                                'client_type.in' => "Invalid Client Type.",
                                'old_cb_name.min' => "Minimum 3 Characters Allowed.",
                                'old_cb_certificate_issue_date.date' => "Invalid Certification Issue Date.",
                                'old_cb_certificate_expiry_date.date' => "Invalid Certification Issue Date.",
                                'old_cb_certificate_expiry_date.after' => "Expiry Date Cannot be Shorter Than Issue Date.",
                                'old_cb_surveillance_frequency.in' => "Invalid Surveillance Frequency",
                                'old_cb_audit_activity_stage_id.in' => "Invalid Activity Stage.",
                                'old_cb_last_audit_report_doc.required_if' => "Last Audit Report Required.",
                                'old_cb_last_audit_report_doc.file' => "Last Audit Report Should Must be a File.",
                                'old_cb_certificate_copy_doc.required_if' => "Certificate Copy Required.",
                                'old_cb_certificate_copy_doc.file' => "Certificate Copy Should Must be a File.",
                                'old_cb_others_doc.required_if' => "Other Document Required.",
                                'old_cb_others_doc.file' => "Other Document Should Must be a File.",
                                'old_cb_audit_activity_date.date' => "Invalid Activity Date.",
//                                'proposed_complexity.required' => "Proposed Complexity is Required for this Standard.",
//                                'proposed_complexity.in' => "Invalid Proposed Complexity.",
                                'surveillance_frequency.required' => "Invalid Surveillance Frequency.",
                                'surveillance_frequency.in' => "Invalid Surveillance Frequency.",
                                'currency_unit.required' => "Currency Required.",
                                'currency_unit.exists' => "Invalid Currency.",
                                'initial_certification_amount.required' => "Invalid Amount.",
                                'initial_certification_amount.numeric' => "Amount Must be Numeric.",
                                'surveillance_amount.required' => "Invalid Amount.",
                                'surveillance_amount.numeric' => "Amount Must be Numeric.",
                                'surveillance_amount.required' => "Transfer Year is required",
                            ]);

                            if (!$validator->fails()) {
                                if (empty($request->company_standard_id)) {
                                    $company_standard = new CompanyStandards;
                                } else {
                                    $company_standard = CompanyStandards::find($request->company_standard_id);
                                    if (empty($company_standard)) {
                                        $company_standard = new CompanyStandards;
                                    }
                                }


                                $company_standard->company_id = $company->id;
                                $company_standard->standard_id = $standard->id;
                                $company_standard->client_type = $request->client_type == 'transfered' ? 'transfered' : 'new';
                                if ($request->client_type == 'transfered') {
                                    $company_standard->old_cb_name = $request->old_cb_name;
                                    $company_standard->old_cb_certificate_issue_date = Carbon::parse($request->old_cb_certificate_issue_date)->format('Y-m-d');
                                    $company_standard->old_cb_certificate_expiry_date = Carbon::parse($request->old_cb_certificate_expiry_date)->format('Y-m-d');
                                    $company_standard->old_cb_surveillance_frequency = $request->old_cb_surveillance_frequency;
                                    $company_standard->old_cb_audit_activity_stage_id = $request->old_cb_audit_activity_stage_id;
                                    $company_standard->old_cb_audit_activity_dates = isset($request->old_cb_audit_activity_date) ? json_encode($request->old_cb_audit_activity_date) : NULL;
                                }
                                if ($request->is_transfer_standard == "1") {
                                    $company_standard->is_transfer_standard = $request->is_transfer_standard == "1" ? true : false;
                                    $company_standard->transfer_year = $request->transfer_year;
                                }else{
                                    $company_standard->is_transfer_standard = false;
                                }
                                $company_standard->proposed_complexity = $request->proposed_complexity ?? 'medium';
                                $company_standard->general_remarks = $request->general_remarks;
//                                $company_standard->is_ims = $request->is_ims === true ? true : false;
                                $company_standard->audit_activity_stage_id = AuditActivityStages::where('name', "Stage 1")->first()->id;
                                $company_standard->initial_certification_currency = $request->currency_unit;//$request->initial_certification_currency;
                                $company_standard->initial_certification_amount = $request->client_type == 'transfered' ? 0 : $request->initial_certification_amount;
                                $company_standard->surveillance_currency = $request->currency_unit;//$request->surveillance_currency;
                                $company_standard->surveillance_amount = $request->surveillance_amount;
                                $company_standard->re_audit_currency = $request->currency_unit;//$request->re_audit_currency;
                                $company_standard->re_audit_amount = $request->re_audit_amount ?? '';
                                $company_standard->surveillance_frequency = $request->surveillance_frequency;
                                $company_standard->save();

//                                $company_standard->accreditations()->sync($request->accreditation_ids);

                                //Generic Company Standard

                                $companyStandardCodes = CompanyStandardCode::where('company_id', $company_standard->company_id)->where('standard_id', $company_standard->standard_id)->get();
                                if ($companyStandardCodes->count() > 0) {
                                    foreach ($companyStandardCodes as $companyStandardCode) {
                                        $codes = CompanyStandardCode::find($companyStandardCode->id);
                                        $codes->company_standard_id = $company_standard->id;
                                        $codes->save();
                                    }
                                }
                                if ($company_standard->standard->standards_family_id == 23) {
                                    //food company Standard

                                    $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $company_standard->company_id)->where('standard_id', $company_standard->standard_id)->get();
                                    if ($companyStandardFoodCodes->count() > 0) {
                                        foreach ($companyStandardFoodCodes as $companyStandardFoodCode) {
                                            $food_codes = CompanyStandardFoodCode::find($companyStandardFoodCode->id);
                                            $food_codes->company_standard_id = $company_standard->id;
                                            $food_codes->save();
                                        }
                                    }

                                }
                                if ($company_standard->standard->standards_family_id == 20) {
                                    //energy Standard

                                    $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $company_standard->company_id)->where('standard_id', $company_standard->standard_id)->get();
                                    if ($companyStandardEnergyCodes->count() > 0) {
                                        foreach ($companyStandardEnergyCodes as $companyStandardEnergyCode) {
                                            $energy_codes = CompanyStandardEnergyCode::find($companyStandardEnergyCode->id);
                                            $energy_codes->company_standard_id = $company_standard->id;
                                            $energy_codes->save();
                                        }
                                    }
                                }

//                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_last_audit_report_doc'))) {
//                                    $org_filename = $request->file('old_cb_last_audit_report_doc')->getClientOriginalName();
//                                    $path_to_file = storage_path('app/' . $request->file('old_cb_last_audit_report_doc')->storeAs('public', $org_filename));
//                                    $company_standard->old_cb_last_audit_report_doc_media_id = $company_standard->addMedia($path_to_file)->toMediaCollection()->id;
//                                }
//
//                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_certificate_copy_doc'))) {
//                                    $org_filename = $request->file('old_cb_certificate_copy_doc')->getClientOriginalName();
//                                    $path_to_file = storage_path('app/' . $request->file('old_cb_certificate_copy_doc')->storeAs('public', $org_filename));
//                                    $company_standard->old_cb_certificate_copy_doc_media_id = $company_standard->addMedia($path_to_file)->toMediaCollection()->id;
//                                }
//
//                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_others_doc'))) {
//                                    $org_filename = $request->file('old_cb_others_doc')->getClientOriginalName();
//                                    $path_to_file = storage_path('app/' . $request->file('old_cb_others_doc')->storeAs('public', $org_filename));
//                                    $company_standard->old_cb_others_doc_media_id = $company_standard->addMedia($path_to_file)->toMediaCollection()->id;
//                                }


                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_last_audit_report_doc'))) {
//                                    if ($request->hasFile('old_cb_last_audit_report_doc')) {
                                    $image = $request->file('old_cb_last_audit_report_doc');
                                    $name = $image->getClientOriginalName();
                                    $destinationPath = public_path('/uploads/company_audit_report');
                                    $imagePath = $destinationPath . "/" . $name;
                                    $old_cb_last_audit_report_doc_media_id = time() . '_' . $name;
                                    $image->move($destinationPath, $name);

//
                                    $company_standard->old_cb_last_audit_report_doc_media_id = $old_cb_last_audit_report_doc_media_id;
//                                    }


//                                    $org_filename = $request->file('old_cb_last_audit_report_doc')->getClientOriginalName();
//                                    $path_to_file = storage_path('app/' . $request->file('old_cb_last_audit_report_doc')->storeAs('public', $org_filename));
//                                    $company_standard->old_cb_last_audit_report_doc_media_id = $company_standard->addMedia($path_to_file)->toMediaCollection()->id;
                                }

                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_certificate_copy_doc'))) {
//                                    $org_filename = $request->file('old_cb_certificate_copy_doc')->getClientOriginalName();
//                                    $path_to_file = storage_path('app/' . $request->file('old_cb_certificate_copy_doc')->storeAs('public', $org_filename));
//                                    $company_standard->old_cb_certificate_copy_doc_media_id = $company_standard->addMedia($path_to_file)->toMediaCollection()->id;


                                    $image = $request->file('old_cb_certificate_copy_doc');
                                    $name = $image->getClientOriginalName();
                                    $destinationPath = public_path('/uploads/company_certificate_copy');
                                    $imagePath = $destinationPath . "/" . $name;
                                    $image->move($destinationPath, $name);
                                    $old_cb_certificate_copy_doc_media_id = time() . '_' . $name;
                                    $company_standard->old_cb_certificate_copy_doc_media_id = $old_cb_certificate_copy_doc_media_id;
                                }

                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_others_doc'))) {
//                                    $org_filename = $request->file('old_cb_others_doc')->getClientOriginalName();
//                                    $path_to_file = storage_path('app/' . $request->file('old_cb_others_doc')->storeAs('public', $org_filename));
//                                    $company_standard->old_cb_others_doc_media_id = $company_standard->addMedia($path_to_file)->toMediaCollection()->id;

                                    $image = $request->file('old_cb_others_doc');
                                    $name = $image->getClientOriginalName();
                                    $destinationPath = public_path('/uploads/company_other_doc');
                                    $imagePath = $destinationPath . "/" . $name;
                                    $image->move($destinationPath, $name);
                                    $old_cb_others_doc_media_id = time() . '_' . $name;
                                    $company_standard->old_cb_others_doc_media_id = $old_cb_others_doc_media_id;
                                }

                                $company_standard->save();

                                CompanyStandardsAnswers::where(['company_standards_id' => $company_standard->id, 'standard_id' => $standard->id])->delete();
                                if (is_array($request->question)) {
                                    foreach ($request->question as $question_id => $answer) {
                                        if (empty($answer))
                                            continue;
                                        $company_standard_answer = new CompanyStandardsAnswers;
                                        $company_standard_answer->standard_id = $standard->id;
                                        $company_standard_answer->standard_question_id = $question_id;
                                        $company_standard_answer->company_standards_id = $company_standard->id;
                                        $company_standard_answer->answer = is_array($answer) ? json_encode($answer) : $answer;
                                        $company_standard_answer->save();
                                    }
                                }

                                if ($standard->auto_calculate_proposed_complexity) {
                                    /*
                                     * Auto Complexity Calulation Here
                                     */
                                }

                                $response['status'] = 'success';
                                $response['message'] = "{$standard->name} " . (empty($request->company_standard_id) ? "Added." : "Updated.");
                                $response['data'] = [
                                    'company' => $company,
                                    'company_standard' => CompanyStandards::with('standard')->where('standard_id', $company_standard->standard_id)->where('company_id', $company_standard->company_id)->first()
                                ];
                            } else {
                                $response['status'] = 'error';
                                $response['message'] = "Validation Errors.";
                                $response['data'] = $validator->errors()->toArray();
                            }
                        }
                    } else {
                        $response['status'] = 'error';
                        $response['message'] = "Validation Errors.";
                        $response['data'] = $validator->errors()->toArray();
                    }
                } else {
                    $validator = Validator::make($request->all(), [
                        //'action'                => 'update',
                        'ims_standard_ids.*' => 'sometimes|exists:standards,id',
                        // 'unchecked_ims_standard_ids.*'  => 'sometimes|exists:standards,id',
                        'default_ims_surveillance_frequency' => 'sometimes|nullable|in:' . collect(CompanyStandards::getEnumValues()->get('surveillance_frequency'))->implode(','),
                        'default_ims_accreditation_ids.*' => 'sometimes|exists:accreditations,id',
                    ]);

                    if (!$validator->fails()) {
                        if (!empty($request->ims_standard_ids) && count($request->ims_standard_ids) > 1) {

                            CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->get('ims_standard_ids'))->update(['is_ims' => true, /*'accreditation_ids' => json_encode($request->accreditation_ids), */ 'surveillance_frequency' => $request->default_ims_surveillance_frequency]);
                            $company_standards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->get('ims_standard_ids'))->get();

                            $company_standards->each(function ($item, $key) use ($request) {

                                for ($i = 0; $i < sizeof($request->default_ims_accreditation_ids); $i++) {

                                    $company_standard_accreditation = CompanyStandardAccreditation::where('company_standard_id', $item->id)->where('accreditation_id', $request->default_ims_accreditation_ids[$i])->first();
                                    if (!empty($company_standard_accreditation)) {
                                        $company_standard_accreditation->accreditation_id = $request->default_ims_accreditation_ids[$i];
                                        $company_standard_accreditation->save();
                                    } else {
                                        $com_accre = new CompanyStandardAccreditation();
                                        $com_accre->company_standard_id = $item->id;
                                        $com_accre->accreditation_id = $request->default_ims_accreditation_ids[$i];
                                        $com_accre->save();
                                    }


                                }


//                                $item->accreditations()->sync($request->accreditation_ids);
                            });


                        } else {
//                            CompanyStandards::where(['company_id' => $company->id, 'is_ims' => true])->update(['is_ims' => false]);
                            // CompanyStandards::where('company_id', $company->id)->whereIn('id', $request->only('unchecked_ims_standard_ids'))->update(['is_ims' => false]);
                        }

                        if ($request->action == 'update') {
                            $company->company_form_progress += 25;
                            $company->is_send_to_ias = false;
                            $company->save();

                        }

                        $response['status'] = 'success';
                        $response['message'] = "Company Profile Saved.";
                        $response['data'] = [
                            'company' => $company,
                            'company_standards' => CompanyStandards::with('standard')->where('company_id', $company->id)->get(),
                            'current_step' => 4,
                            'next_step' => 5
                        ];
                    } else {
                        $response['status'] = 'error';
                        $response['message'] = "Something Went Wrong.";
                        $response['data'] = $validator->errors()->toArray();
                    }
                }
            } else {

                $response['status'] = 'error';
                $response['message'] = "Something Went Wrong.";
                $response['data'] = $validator->errors()->toArray();
                if (isset($response['data']['company_id'])) {
                    $response['message'] = $response['data']['company_id'][0];
                }
            }
        } elseif ($request->step == 5) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'required|exists:companies,id'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $validator = Validator::make($request->all(), [
                    'step_action' => 'required|in:upload_documents,remove_document',
                    'documents' => 'required_if:step_action,upload_documents|array',
                    'document_ids' => 'required_if:step_action,remove_document',
                    'documents.*' => 'required_with:documents|file',
                    'date' => 'required_with:documents|date',
                    'document_type' => ['required_with:documents', Rule::in(CompanyStandardDocument::getEnumValues()->get('document_type'))],
                    'standards' => ['required_with:documents', 'array', Rule::in(CompanyStandards::where('company_id', $request->company_id)->get()->pluck('standard_id'))]
                ], [
                    'step_action.required' => "Action Required.",
                    'step_action.in' => "Unknown Action.",
                    'documents.required' => "Documents Required.",
                    'documents.array' => "Something Went Wrong.",
                    'documents.*.file' => "Documents Required.",
                    'date.required' => "Date Required.",
                    'date.date' => "Invalid Date Format.",
                    'document_type.required' => "Document Type Required.",
                    'document_type.in' => "Invalid Document Type.",
                    'standards.*' => "Standards Required.",
                ]);

                if (!$validator->fails()) {
                    if ($request->step_action == 'upload_documents') {
                        $documents_saved = [];
                        $documents_models = [];
                        $documents = $request->file('documents');
//                        dd($documents);
                        foreach ($documents as $request_document) {
                            $org_filename = $request_document->getClientOriginalName();
                            // $filename       = $request->document_type . '_' . rand(999, 99999) . '_' . $org_filename;
                            $path = storage_path('app/' . $request_document->storeAs('public', $org_filename));
                            foreach ($request->standards as $standard) {
                                $document = new CompanyStandardDocument;
                                $document->company_standard_id = CompanyStandards::where(['company_id' => $request->company_id, 'standard_id' => $standard])->first()->id;
                                $document->date = Carbon::parse($request->date)->format('Y-m-d');
                                $document->document_type = $request->document_type;
                                $document->save();
                                $document->media_id = $document->addMedia($path)
                                    ->withCustomProperties(['document_type' => $request->document_type])
                                    ->toMediaCollection()->id;
                                $document->save();
                                $documents_saved[] = $document->id;
                                $path = storage_path('app/' . $request_document->storeAs('public', $org_filename));
                            }
                            $documents_models[] = $document;
                            @unlink($path);
                        }
                        $response['status'] = 'success';
                        $response['message'] = "Documents Uploaded.";
                        $company->company_form_progress = 100;
                        $company->company_form_status = "completed";
//                        $company->is_send_to_ias = false;
                        $company->save();
                        $response['data'] = [
                            'company' => $company,
                            'documents' => $documents_saved,
                            'uploaded_doc_card' => view('company.ajax.uploaded_doc_card', ['request' => $request, 'documents' => $documents_models, 'documents_saved' => $documents_saved])->render()
                        ];
                    } else {
                        CompanyStandardDocument::whereIn('id', explode(',', $request->document_ids))->delete();
                        $response['status'] = 'success';
                        $response['message'] = "Documents Removed.";
                    }
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "Form Has Some Errors.";
                    $response['data'] = $validator->errors()->toArray();
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
            }
        } elseif ($request->step == 6) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'required|exists:companies,id'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $company->company_form_progress = 100;
                $company->company_form_status = "completed";
                $company->is_send_to_ias = false;
                $company->save();

                $response['status'] = 'success';
                $response['message'] = "Company Saved Successfully.";
                $response['data'] = $company;
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Something Went Wrong.";
        }
//        $company = Company::find($request->company_id);

        $company->save();
        return $response;
    }

    public function continue(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        $standard_documents = [];
        $initial_inquiry_form = [];

        foreach ($company->companyStandards as $key => $documents) {
            $standard_documents[$key] = CompanyStandardDocument::where('company_standard_id', $documents->id)->get();
            foreach ($standard_documents as $inqueryDocuemts) {
                foreach ($inqueryDocuemts as $index => $data) {
                    $initial_inquiry_form[$index] = Media::where('id', $data->media_id)->get();
                }
            }
        }

        $ias_array = [];
        if (!empty($company->companyIAFs)) {
            foreach ($company->companyIAFs as $key => $ias) {

                $ias_array[$key] = \App\IAS::where('iaf_id', $ias->iaf_id)->get();
            }

        } else {
            $ias_array = IAS::all();
        }
        $regions = Regions::all();
        $countries = Regions::find($company->region_id)->regionscountries;
        $cities = Cities::where('zoneable_id', $company->country_id)->where('zoneable_type', 'App\Countries')->get();
        $shifts = Company::getEnumValues()->get('shift');
        $iafs = \App\IAF::all();
        $iass = $ias_array;
        $standards_families = StandardsFamily::orderBy('sort')->get();
        $currencies = Countries::orderBy('currency_code')->pluck('currency_name', 'currency_code');
        $surveillance_frequencies = CompanyStandards::getEnumValues()->get('surveillance_frequency');
        $accreditations = Accreditation::orderBy('name')->get();
        $audit_activity_stages = AuditActivityStages::orderBy('id')->get();
        $proposed_complexities = CompanyStandards::getEnumValues()->get('proposed_complexity');
        $contact_titles = CompanyContacts::getEnumValues()->get('title');


        return view('company.continue')->with(['initial_inquiry_form' => $initial_inquiry_form, 'company' => $company, 'regions' => $regions, 'countries' => $countries, 'cities' => $cities, 'shifts' => $shifts, 'iafs' => $iafs, 'iass' => $iass, 'standards_families' => $standards_families, 'currencies' => $currencies, 'surveillance_frequencies' => $surveillance_frequencies, 'accreditations' => $accreditations, 'audit_activity_stages' => $audit_activity_stages, 'proposed_complexities' => $proposed_complexities, 'contact_titles' => $contact_titles, 'certificate_types' => Company::CERTIFICATE_TYPES]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $standard_documents = [];
        $initial_inquiry_form = [];
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $company = Company::with('region', 'auditJustifications', 'country', 'city', 'companyIAFs', 'companyIAFs.IAF', 'companyIASs', 'companyIASs.IAS', 'parentCompany', 'childCompanies', 'childCompanies.city', 'childCompanies.country', 'contacts'/*, 'accreditation'*/, 'companyStandards', 'companyStandards.accreditations', 'companyStandards.auditActivityStage', 'companyStandards.oldCbAuditActivityStage', 'companyStandards.standard', 'companyStandards.standard.standardFamily', 'companyStandards.companyStandardsAnswers', 'companyStandards.companyStandardsAnswers.question', 'companyStandards.companyStandardCodes.accreditation', 'companyStandards.companyStandardCodes.iaf', 'companyStandards.companyStandardCodes.ias', 'companyStandards.companyStandardFoodCodes.accreditation', 'companyStandards.companyStandardFoodCodes.foodcategory', 'companyStandards.companyStandardFoodCodes.foodsubcategory', 'companyStandards.companyStandardEnergyCodes.accreditation', 'companyStandards.companyStandardEnergyCodes.energycode')->findOrFail($id);
            $standards_families = StandardsFamily::orderBy('sort')->get();
            $companyStandards = CompanyStandards::where('status', 'approved')->where('company_id', $company->id)->whereNull('deleted_at')->get();


        } elseif (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'scheme_coordinator') {
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $company = Company::with('region', 'auditJustifications', 'country', 'city', 'companyIAFs', 'companyIAFs.IAF', 'companyIASs', 'companyIASs.IAS', 'parentCompany', 'childCompanies', 'childCompanies.city', 'childCompanies.country', 'contacts'/*, 'accreditation'*/, 'companyStandards', 'companyStandards.accreditations', 'companyStandards.auditActivityStage', 'companyStandards.oldCbAuditActivityStage', 'companyStandards.standard', 'companyStandards.standard.standardFamily', 'companyStandards.companyStandardsAnswers', 'companyStandards.companyStandardsAnswers.question', 'companyStandards.companyStandardCodes.accreditation', 'companyStandards.companyStandardCodes.iaf', 'companyStandards.companyStandardCodes.ias', 'companyStandards.companyStandardFoodCodes.accreditation', 'companyStandards.companyStandardFoodCodes.foodcategory', 'companyStandards.companyStandardFoodCodes.foodsubcategory', 'companyStandards.companyStandardEnergyCodes.accreditation', 'companyStandards.companyStandardEnergyCodes.energycode')->findOrFail($id);
            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();


            $company->companyStandards = CompanyStandards::where('company_id', '=', $company->id)->whereNull('deleted_at')->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();

            $companyStandards = CompanyStandards::where('company_id', '=', $company->id)->whereNull('deleted_at')->whereHas('standard.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->where('status', 'approved')->get();
        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $company = Company::with('region', 'auditJustifications', 'country', 'city', 'companyIAFs', 'companyIAFs.IAF', 'companyIASs', 'companyIASs.IAS', 'parentCompany', 'childCompanies', 'childCompanies.city', 'childCompanies.country', 'contacts'/*, 'accreditation'*/, 'companyStandards', 'companyStandards.accreditations', 'companyStandards.auditActivityStage', 'companyStandards.oldCbAuditActivityStage', 'companyStandards.standard', 'companyStandards.standard.standardFamily', 'companyStandards.companyStandardsAnswers', 'companyStandards.companyStandardsAnswers.question', 'companyStandards.companyStandardCodes.accreditation', 'companyStandards.companyStandardCodes.iaf', 'companyStandards.companyStandardCodes.ias', 'companyStandards.companyStandardFoodCodes.accreditation', 'companyStandards.companyStandardFoodCodes.foodcategory', 'companyStandards.companyStandardFoodCodes.foodsubcategory', 'companyStandards.companyStandardEnergyCodes.accreditation', 'companyStandards.companyStandardEnergyCodes.energycode')->findOrFail($id);
            $standards_families = StandardsFamily::orderBy('sort')->get();
            $companyStandards = CompanyStandards::where('status', 'approved')->where('company_id', $company->id)->whereNull('deleted_at')->get();
        }


        if ($request->ajax()) {
            $company->uploaded_docs = view('company.ajax.uploaded_docs', ['company' => $company])->render();
            return $company;
        }


        foreach ($company->companyStandards as $key => $documents) {
            $newDocs = CompanyStandardDocument::where('company_standard_id', $documents->id)->where('media_id', '!=', 0)->get();

            if (!empty($newDocs) && count($newDocs) > 0) {
                $standard_documents[$key] = $newDocs;
            }


            foreach ($standard_documents as $inqueryDocuemts) {
                foreach ($inqueryDocuemts as $index => $data) {
                    $initial_inquiry_form[$index] = Media::where('id', $data->media_id)->get();
                }
            }
        }

//        dd($standard_documents);

        $currencies = Countries::orderBy('currency_code')->pluck('currency_name', 'currency_code');
        $surveillance_frequencies = CompanyStandards::getEnumValues()->get('surveillance_frequency');
        $accreditations = Accreditation::orderBy('name')->get();
        $audit_activity_stages_annually = AuditActivityStages::orderBy('id')->get();
//        $audit_activity_stages_biannually = AuditActivityStages::orderBy('id')->get();
        $audit_activity_stages = AuditActivityStages::orderBy('id')->get();
        $proposed_complexities = CompanyStandards::getEnumValues()->get('proposed_complexity');

        return view('company.show')->with(['companyStandards' => $companyStandards, 'audit_activity_stages' => $audit_activity_stages, 'standard_documents' => $standard_documents, 'company' => $company, 'standards_families' => $standards_families, 'currencies' => $currencies, 'surveillance_frequencies' => $surveillance_frequencies, 'accreditations' => $accreditations, 'audit_activity_stages_annually' => $audit_activity_stages_annually, 'proposed_complexities' => $proposed_complexities]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (auth()->user()->user_type == 'admin') {
            $regions = Regions::all();
            $standards_families = StandardsFamily::orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $standards_families = StandardsFamily::orderBy('sort')->get();
        }


        $company = Company::findOrFail($id);
        $standard_documents = [];
        $initial_inquiry_form = [];

        foreach ($company->companyStandards as $key => $documents) {
            $newDocs = CompanyStandardDocument::where('company_standard_id', $documents->id)->where('media_id', '!=', 0)->get();

            if (!empty($newDocs) && count($newDocs) > 0) {
                $standard_documents[$key] = $newDocs;
            }

            foreach ($standard_documents as $inqueryDocuemts) {
                foreach ($inqueryDocuemts as $index => $data) {
                    $initial_inquiry_form[$index] = Media::where('id', $data->media_id)->get();
                }
            }
        }
//        return ($standard_documents);

        $ias_array = [];

        $ias_array = [];
        if (!empty($company->companyIAFs)) {
            foreach ($company->companyIAFs as $key => $ias) {

                $ias_array[$key] = \App\IAS::where('iaf_id', $ias->iaf_id)->get();
            }

        } else {
            $ias_array = IAS::all();
        }

        $regionCountry = Regions::where('id', $company->region_id)->first();


        $countries = $regionCountry->regionCountries()->with('country')->get();
        $allCountries = Countries::all();
        $allCities = Cities::all();

        $country_zones = Zone::where('country_id', $company->country_id)->pluck('id')->toArray();
        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $company->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
        $shifts = Company::getEnumValues()->get('shift');
        $iafs = \App\IAF::all();
        $iass = $ias_array;
        $currencies = Countries::orderBy('currency_code')->pluck('currency_name', 'currency_code');
        $surveillance_frequencies = CompanyStandards::getEnumValues()->get('surveillance_frequency');
        $accreditations = Accreditation::orderBy('name')->get();
        $audit_activity_stages = AuditActivityStages::orderBy('id')->get();
        $proposed_complexities = CompanyStandards::getEnumValues()->get('proposed_complexity');
        $contact_titles = CompanyContacts::getEnumValues()->get('title');
        $effective_employees = EffectiveEmployeesFormula::first();

        return view('company.edit')->with(['allCountries' => $allCountries, 'allCities' => $allCities, 'effective_employees' => $effective_employees, 'initial_inquiry_form' => $initial_inquiry_form, 'company' => $company, 'regions' => $regions, 'countries' => $countries, 'cities' => $cities, 'shifts' => $shifts, 'iafs' => $iafs, 'iass' => $iass, 'standards_families' => $standards_families, 'currencies' => $currencies, 'surveillance_frequencies' => $surveillance_frequencies, 'accreditations' => $accreditations, 'audit_activity_stages' => $audit_activity_stages, 'proposed_complexities' => $proposed_complexities, 'contact_titles' => $contact_titles, 'standard_documents' => $standard_documents, 'certificate_types' => Company::CERTIFICATE_TYPES]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());

        if ($request->step == 1) {
            $validator = Validator::make($request->all(), [
                'company_id' => 'sometimes|required|exists:companies,id',
                'name' => 'required',
                'region_id' => 'required|exists:regions,id',
                'country_id' => 'required|exists:countries,id',
                'address' => 'required',
                'city_id' => 'required|exists:cities,id',
                'website' => 'sometimes',
                'total_employees' => 'required|numeric|min:1',
                'key_process' => 'required',
                'child_company.address.*' => 'sometimes|required',
                'child_company.city_id.*' => 'sometimes|required|exists:cities,id',
                'child_company.key_process.*' => 'sometimes|required',
                'child_company.total_employees.*' => 'sometimes|required|numeric|min:1',
                'permanent_employees' => 'required|numeric|min:0',
                'part_time_employees' => 'required|numeric|min:0',
                'repeatative_employees' => 'required|numeric|min:0',
                'shift' => ['required', Rule::in(Company::getEnumValues()->get('shift'))],
                'sister_company_name' => 'string',
                'general_remarks' => 'sometimes',
                'scope_to_certify' => 'required',
                'outsourced_process_details' => 'required_if:enable_outsourced_process_details,1'
            ], [
                'region_id.required' => "Region Required.",
                'region_id.exists' => "Invalid Region Selected.",
                'country_id.required' => "Country Required.",
                'country_id.exists' => "Invalid Country Selected.",
                'city_id.required' => "City Required.",
                'city_id.exists' => "Invalid City Selected.",
                'child_company.address.*.required' => "Address Required.",
                'child_company.country_id.*.required' => "Country Required.",
                'child_company.country_id.*.exists' => "Country Required.",
                'child_company.city_id.*.required' => "City Required.",
                'child_company.city_id.*.exists' => "City Required.",
                'child_company.key_process.*.required' => 'Key Process Required.',
                'child_company.total_employees.*.required' => 'Employees Required.',
            ]);

            if (!$validator->fails()) {

                if (!empty($request->company_id)) {
                    $company = Company::find($request->company_id);

                } else {
                    $company = new Company;
                }
                if (is_object($company) && $company instanceof Company) {
                    $company->name = $request->name;
                    $company->region_id = $request->region_id;
                    $company->country_id = $request->country_id;
                    $company->address = $request->address;
                    $company->city_id = $request->city_id;
                    $company->website = $request->website ?? ' ';
                    $company->multiple_sides = $request->multiple_sides ?? 0;
                    $company->out_of_country_region_name = $request->out_of_country_region_name ?? '';
                    $company->total_employees = $request->total_employees;
                    $company->key_process = $request->key_process;
                    $company->permanent_employees = $request->permanent_employees;
                    $company->part_time_employees = $request->part_time_employees;
                    $company->repeatative_employees = $request->repeatative_employees;
                    $company->effective_employees = $request->effective_employees;
                    $company->shift = $request->shift;
                    $company->sister_company_name = $request->sister_company_name ?? ' ';
                    $company->confidential_certified_entity = $request->confidential_certified_entity ?? 'No';
                    $company->shift_remarks = $request->shift_remarks ?? 'N/A';
                    $company->scope_to_certify = $request->scope_to_certify;
                    // $company->iaf_id                        = $request->iaf_id;
                    // $company->ias_id                        = $request->ias_id;
                    $company->outsourced_process_details = $request->outsourced_process_details;
                    $company->is_parent_company = true;
//                    $company->company_form_progress = 20;
//                    $company->company_form_status   = "processing";
                    $company->general_remarks = $request->general_remarks ?? ' ';
                    $company->is_send_to_ias = false;
                    $company->save();

                    if (!empty($request->company_id)) {
                        Company::where('company_id', $request->company_id)->delete();
                    }

                    $child_companies = array();
                    if (isset($request->child_company['address']) && is_array($request->child_company['address'])) {
                        foreach ($request->child_company['address'] as $i => $v) {
                            $child_company = new Company;
                            $child_company->address = $request->child_company['address'][$i] ?? ' ';
                            $child_company->country_id = $request->child_company['country_id'][$i] ?? ' ';
                            $child_company->city_id = $request->child_company['city_id'][$i] ?? ' ';
                            $child_company->key_process = $request->child_company['key_process'][$i] ?? ' ';
                            $child_company->total_employees = $request->child_company['total_employees'][$i] ?? ' ';
                            $child_company->is_parent_company = false;
                            $child_company->company_id = $company->id;
                            $child_company->save();
                            $child_companies[] = $child_company;
                        }
                    }

                    $response['status'] = 'success';
                    $response['message'] = isset($request->company_id) ? "Company Profile Updated." : "Company Profile Saved.";
                    $response['data'] = [
                        'company' => $company,
                        'child_companies' => $child_companies,
                        'current_step' => 1,
                        'next_step' => 3
                    ];
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "Unable to Update Company Profile.";
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
            }
        } elseif ($request->step == 2) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'bail|required|exists:companies,id',
                'scope_to_certify' => 'required',
                'iaf_id.*' => 'required|exists:i_a_fs,id',
                'ias_id.*' => 'required|exists:i_a_s,id',
                'outsourced_process_details' => 'required_if:enable_outsourced_process_details,1'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First.",
                'iaf_id.*.required' => "IAF Code is Required.",
                'iaf_id.*.exists' => "Invalid IAF Code Selected.",
                'ias_id.*.required' => "IAF Code is Required.",
                'ias_id.*.exists' => "Invalid IAF Code Selected."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $company->scope_to_certify = $request->scope_to_certify;
                // $company->iaf_id                        = $request->iaf_id;
                // $company->ias_id                        = $request->ias_id;
                $company->outsourced_process_details = $request->outsourced_process_details;
//                $company->company_form_progress         += 20;
                $company->is_send_to_ias = false;
                $company->save();

                $company->companyIAFs()->forceDelete();
                $company->companyIASs()->forceDelete();
                $iafs = [];
                $iass = [];

                if (!empty($request->iaf_id)) {
                    foreach ($request->iaf_id as $key => $iaf) {
                        $iafs[] = new \App\CompanyIAF(['iaf_id' => $iaf]);
                    }
                }

                if (!empty($request->iaf_id)) {
                    foreach ($request->ias_id as $key => $ias) {
                        $iass[] = new \App\CompanyIAS(['ias_id' => $ias]);
                    }
                }

                $company->companyIAFs()->saveMany($iafs);
                $company->companyIASs()->saveMany($iass);

//                if($request->action == 'update')
//                {
//                    $company->company_form_progress     += 20;
//                    $company->save();
//                }

                $response['status'] = 'success';
                $response['message'] = "Company Profile Saved.";
                $response['data'] = [
                    'company' => $company,
                    'current_step' => 2,
                    'next_step' => 3
                ];
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
                if (isset($response['data']['company_id'])) {
                    $response['message'] = $response['data']['company_id'][0];
                }
            }
        } elseif ($request->step == 3) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'bail|required|exists:companies,id',
                'title.*' => 'required|in:Mr,Mrs,Ms,Dr',
                'name.*' => 'required',
                'position.*' => 'required',
                'contact.*' => 'required',
                'landline.*' => 'sometimes',
                'email.*' => 'sometimes|nullable|email',
                'primary' => 'required|numeric',
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First.",
                'title.*.required' => "Title is Required.",
                'title.*.in' => "Invalid Title Selected.",
                'name.*.required' => "Name is Required.",
                'position.*.required' => "Position is Required.",
                'contact.*.required' => "Contact Number is Required.",
                'email.*.email' => "Email Format is Invalid.",
                'primary.required' => "Primary Contact Required.",
                'primary.numeric' => "Primary Contact Required.",
            ]);


            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $company->is_send_to_ias = false;
                $company->save();
                $validator = Validator::make($request->all(), [

                ], [
                ]);

                if (!$validator->fails()) {
                    $company->contacts()->forceDelete();
                    $company_contacts = [];


                    foreach ($request->name as $key => $name) {


                        if (!is_null($request->primary_index)) {
                            if (($key == $request->primary_index)) {
                                $type = "primary";
                            } else {
                                $type = "secondary";
                            }
                        } else {
                            if ($request->primary === '1' || $request->primary === 1) {
                                $type = "primary";
                            } else {
                                $type = "secondary";
                            }
                        }
                        $company_contacts[] = new CompanyContacts(
                            [
                                'name' => $name,
                                'title' => $request->title[$key],
                                'position' => $request->position[$key],
                                'contact' => $request->contact[$key],
                                'landline' => $request->landline[$key],
                                'email' => isset($request->email[$key]) ? $request->email[$key] : '',
//                                'type' => $key + 1 == $request->primary ? "primary" : "secondary"
                                'type' => $type
                            ]
                        );
                    }


                    $company->contacts()->saveMany($company_contacts);

                    $response['status'] = 'success';
                    $response['message'] = "Company Profile Saved.";
                    $response['data'] = [
                        'company' => $company,
                        'company_contacts' => $company->contacts->toArray(),
                        'current_step' => 3,
                        'next_step' => 4
                    ];
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "Form Has Some Errors.";
                    $response['data'] = $validator->errors()->toArray();
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
                if (isset($response['data']['company_id'])) {
                    $response['message'] = $response['data']['company_id'][0];
                }
            }
        } elseif ($request->step == 4) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'required|exists:companies,id'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);

                if (!empty($request->standard_id)) {
                    $validator = Validator::make($request->all(), [
                        'standard_id' => 'required|exists:standards,id',
                        'standard_action' => 'sometimes'
                    ], [
                        'standard_id.required' => "Please Complete Step 1 First.",
                        'standard_id.exists' => "Please Complete Step 1 First.",
                    ]);


                    if (!$validator->fails()) {
                        if ($request->standard_action == 'remove') {


                            $standard = Standards::find($request->standard_id);

                            $company_standard = CompanyStandards::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->first();

                            if (!is_null($company_standard)) {
                                CompanyStandardCode::where('company_standard_id', $company_standard->id)->delete();
                                CompanyStandardEnergyCode::where('company_standard_id', $company_standard->id)->delete();
                                CompanyStandardFoodCode::where('company_standard_id', $company_standard->id)->delete();

                                $company_standard->delete();
                                CompanyStandardsAnswers::where('company_standards_id', $company_standard->id)->delete();
                                $response['status'] = 'success';
                                $response['message'] = "{$standard->name} Removed From Company Profile.";
                            } else {
                                CompanyStandardCode::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->delete();
                                CompanyStandardEnergyCode::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->delete();
                                CompanyStandardFoodCode::where(['company_id' => $request->company_id, 'standard_id' => $request->standard_id])->delete();
                                $response['status'] = 'success';
                                $response['message'] = "{$standard->name} Removed From Company Profile";
                            }


                        } else {
                            $standard = Standards::find($request->standard_id);
                            $validator = Validator::make($request->all(), [
                                'company_standard_id' => 'sometimes',
                                'client_type' => 'sometimes|in:' . collect(CompanyStandards::getEnumValues()->get('client_type'))->implode(','),
                                'old_cb_name' => 'required_if:client_type,transfered|min:3',
                                'old_cb_certificate_issue_date' => 'required_if:client_type,transfered|date',
                                'old_cb_certificate_expiry_date' => 'required_if:client_type,transfered|date|after:old_cb_certificate_issue_date',
                                'old_cb_surveillance_frequency' => 'required_if:client_type,transfered|in:' . collect(CompanyStandards::getEnumValues()->get('surveillance_frequency'))->implode(','),
                                'old_cb_audit_activity_stage_id' => 'required_if:client_type,transfered|in:' . AuditActivityStages::all()->pluck('id')->implode(','),
                                'old_cb_last_audit_report_attached' => 'sometimes',
                                'old_cb_last_audit_report_doc' => !empty($request->company_standard_action) ? 'sometimes|nullable|file' : 'required_if:client_type,transfered|file',
                                'old_cb_certificate_copy_attached' => 'sometimes',
                                'old_cb_certificate_copy_doc' => !empty($request->company_standard_action) ? 'sometimes|nullable|file' : 'required_if:client_type,transfered|file',
                                'old_cb_others_attached' => 'sometimes',
                                'old_cb_others_doc' => 'sometimes|nullable|file',
                                'old_cb_audit_activity_date.*' => 'sometimes|date',
                                'general_remarks' => 'sometimes',
//                                'proposed_complexity' => (!$standard->auto_calculate_proposed_complexity ? 'required' : 'sometimes|nullable') . '|in:' . collect(CompanyStandards::getEnumValues()->get('proposed_complexity'))->implode(','),
                                'surveillance_frequency' => 'required|in:' . collect(CompanyStandards::getEnumValues()->get('surveillance_frequency'))->implode(','),
                                'currency_unit' => 'required|exists:countries,currency_code',
                                'initial_certification_amount' => [Rule::requiredIf(function () use ($request) {
                                    if ($request->client_type != 'transfered') {
                                        return true;
                                    }
                                })
                                ],
                                'transfer_year' => [Rule::requiredIf(function () use ($request) {
                                    if ($request->is_transfer_standard == "1") {
                                        return true;
                                    }
                                })
                                ],
                                'surveillance_amount' => 'required|numeric',
                            ], [
                                'client_type.in' => "Invalid Client Type.",
                                'old_cb_name.min' => "Minimum 3 Characters Allowed.",
                                'old_cb_certificate_issue_date.date' => "Invalid Certification Issue Date.",
                                'old_cb_certificate_expiry_date.date' => "Invalid Certification Issue Date.",
                                'old_cb_certificate_expiry_date.after' => "Expiry Date Cannot be Shorter Than Issue Date.",
                                'old_cb_surveillance_frequency.in' => "Invalid Surveillance Frequency",
                                'old_cb_audit_activity_stage_id.in' => "Invalid Activity Stage.",
                                'old_cb_last_audit_report_doc.required_if' => "Last Audit Report Required.",
                                'old_cb_last_audit_report_doc.file' => "Last Audit Report Should Must be a File.",
                                'old_cb_certificate_copy_doc.required_if' => "Certificate Copy Required.",
                                'old_cb_certificate_copy_doc.file' => "Certificate Copy Should Must be a File.",
                                'old_cb_others_doc.required_if' => "Other Document Required.",
                                'old_cb_others_doc.file' => "Other Document Should Must be a File.",
                                'old_cb_audit_activity_date.date' => "Invalid Activity Date.",
//                                'proposed_complexity.required' => "Proposed Complexity is Required for this Standard.",
//                                'proposed_complexity.in' => "Invalid Proposed Complexity.",
                                'surveillance_frequency.required' => "Invalid Surveillance Frequency.",
                                'surveillance_frequency.in' => "Invalid Surveillance Frequency.",
                                'currency_unit.required' => "Currency Required.",
                                'currency_unit.exists' => "Invalid Currency.",
                                'initial_certification_amount.required' => "Invalid Amount.",
                                'initial_certification_amount.numeric' => "Amount Must be Numeric.",
                                'surveillance_amount.required' => "Invalid Amount.",
                                'surveillance_amount.numeric' => "Amount Must be Numeric.",
                                'transfer_year.numeric' => "Transfer Year.",

                            ]);

                            if (!$validator->fails()) {

                                if (empty($request->company_standard_id)) {
                                    $company_standard = new CompanyStandards;
                                } else {
                                    $company_standard = CompanyStandards::where('id', $request->company_standard_id)->where('company_id', $company->id)->where('standard_id', $standard->id)->first();
                                    if (empty($company_standard)) {
                                        $company_standard = new CompanyStandards;
                                    }
                                }
                                $company_standard->company_id = $company->id;
                                $company_standard->standard_id = $standard->id;

                                $company_standard->client_type = $request->client_type == 'transfered' ? 'transfered' : 'new';
                                if ($request->client_type == 'transfered') {
                                    $company_standard->old_cb_name = $request->old_cb_name;
                                    $company_standard->old_cb_certificate_issue_date = Carbon::parse($request->old_cb_certificate_issue_date)->format('Y-m-d');
                                    $company_standard->old_cb_certificate_expiry_date = Carbon::parse($request->old_cb_certificate_expiry_date)->format('Y-m-d');
                                    $company_standard->old_cb_surveillance_frequency = $request->old_cb_surveillance_frequency;
                                    $company_standard->old_cb_audit_activity_stage_id = $request->old_cb_audit_activity_stage_id;
                                    $company_standard->old_cb_audit_activity_dates = isset($request->old_cb_audit_activity_date) ? json_encode($request->old_cb_audit_activity_date) : NULL;
                                }
                                if ($request->is_transfer_standard == "1") {
                                    $company_standard->is_transfer_standard = $request->is_transfer_standard == "1" ? true : false;
                                    $company_standard->transfer_year = $request->transfer_year;
                                }else{
                                    $company_standard->is_transfer_standard = false;
                                }
                                $company_standard->proposed_complexity = $request->proposed_complexity ?? 'medium';
                                $company_standard->general_remarks = $request->general_remarks;
                                $company_standard->audit_activity_stage_id = AuditActivityStages::where('name', "Stage 1")->first()->id;
                                $company_standard->initial_certification_currency = $request->currency_unit;//$request->initial_certification_currency;
                                $company_standard->initial_certification_amount = $request->client_type == 'transfered' ? 0 : $request->initial_certification_amount;
                                $company_standard->surveillance_currency = $request->currency_unit;//$request->surveillance_currency;
                                $company_standard->surveillance_amount = $request->surveillance_amount;
                                $company_standard->re_audit_currency = $request->currency_unit;//$request->re_audit_currency;
                                $company_standard->re_audit_amount = $request->re_audit_amount ?? '';
                                $company_standard->surveillance_frequency = $request->surveillance_frequency;
                                $company_standard->save();


                                //Generic Company Standard

                                $companyStandardCodes = CompanyStandardCode::where('company_id', $company_standard->company_id)->where('standard_id', $company_standard->standard_id)->get();
                                if ($companyStandardCodes->count() > 0) {
                                    foreach ($companyStandardCodes as $companyStandardCode) {
                                        $codes = CompanyStandardCode::find($companyStandardCode->id);
                                        $codes->company_standard_id = $company_standard->id;
                                        $codes->save();
                                    }
                                }
                                if ($company_standard->standard->standards_family_id == 23) {
                                    //food company Standard

                                    $companyStandardFoodCodes = CompanyStandardFoodCode::where('company_id', $company_standard->company_id)->where('standard_id', $company_standard->standard_id)->get();
                                    if ($companyStandardFoodCodes->count() > 0) {
                                        foreach ($companyStandardFoodCodes as $companyStandardFoodCode) {
                                            $food_codes = CompanyStandardFoodCode::find($companyStandardFoodCode->id);
                                            $food_codes->company_standard_id = $company_standard->id;
                                            $food_codes->save();
                                        }
                                    }

                                }
                                if ($company_standard->standard->standards_family_id == 20) {
                                    //energy Standard

                                    $companyStandardEnergyCodes = CompanyStandardEnergyCode::where('company_id', $company_standard->company_id)->where('standard_id', $company_standard->standard_id)->get();
                                    if ($companyStandardEnergyCodes->count() > 0) {
                                        foreach ($companyStandardEnergyCodes as $companyStandardEnergyCode) {
                                            $energy_codes = CompanyStandardEnergyCode::find($companyStandardEnergyCode->id);
                                            $energy_codes->company_standard_id = $company_standard->id;
                                            $energy_codes->save();
                                        }
                                    }
                                }

                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_last_audit_report_doc'))) {
//                                    if ($request->hasFile('old_cb_last_audit_report_doc')) {
                                    $image = $request->file('old_cb_last_audit_report_doc');
                                    $name = $image->getClientOriginalName();
                                    $destinationPath = public_path('/uploads/company_audit_report');
                                    $imagePath = $destinationPath . "/" . $name;
                                    $image->move($destinationPath, $name);
                                    $old_cb_last_audit_report_doc_media_id = $name . '_' . time();
                                    $company_standard->old_cb_last_audit_report_doc_media_id = $old_cb_last_audit_report_doc_media_id;

                                }

                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_certificate_copy_doc'))) {

                                    $image = $request->file('old_cb_certificate_copy_doc');
                                    $name = $image->getClientOriginalName();
                                    $destinationPath = public_path('/uploads/company_certificate_copy');
                                    $imagePath = $destinationPath . "/" . $name;
                                    $image->move($destinationPath, $name);
                                    $old_cb_certificate_copy_doc_media_id = $name . '_' . time();
                                    $company_standard->old_cb_certificate_copy_doc_media_id = $old_cb_certificate_copy_doc_media_id;
                                }

                                if ($request->client_type == 'transfered' && !empty($request->file('old_cb_others_doc'))) {

                                    $image = $request->file('old_cb_others_doc');
                                    $name = $image->getClientOriginalName();
                                    $destinationPath = public_path('/uploads/company_other_doc');
                                    $imagePath = $destinationPath . "/" . $name;
                                    $image->move($destinationPath, $name);
                                    $old_cb_others_doc_media_id = $name . '_' . time();
                                    $company_standard->old_cb_others_doc_media_id = $old_cb_others_doc_media_id;
                                }

                                $company_standard->save();
                                if (is_array($request->question)) {


                                    CompanyStandardsAnswers::where(['company_standards_id' => $company_standard->id, 'standard_id' => $standard->id])->delete();
                                    foreach ($request->question as $question_id => $answer) {
                                        if (empty($answer))
                                            continue;
                                        $company_standard_answer = CompanyStandardsAnswers::where('standard_question_id', $question_id)->first();
                                        if (empty($company_standard_answer)) {
                                            $company_standard_answer = new CompanyStandardsAnswers;
                                        }


                                        $company_standard_answer->standard_id = $standard->id;
                                        $company_standard_answer->standard_question_id = $question_id;
                                        $company_standard_answer->company_standards_id = $company_standard->id;
                                        $company_standard_answer->answer = is_array($answer) ? json_encode($answer) : $answer;
                                        $company_standard_answer->save();
                                    }
                                }

                                if ($standard->auto_calculate_proposed_complexity) {
                                    /*
                                     * Auto Complexity Calulation Here
                                     */
                                }
                                $company->is_send_to_ias = false;
                                $company->save();

                                $response['status'] = 'success';
                                $response['message'] = "{$standard->name} " . (empty($request->company_standard_id) ? "Added." : "Updated.");
                                $response['data'] = [
                                    'company' => $company,
                                    'company_standard' => CompanyStandards::with('standard')->where('standard_id', $company_standard->standard_id)->where('company_id', $company_standard->company_id)->first()
                                ];
                            } else {
                                $response['status'] = 'error';
                                $response['message'] = "Validation Errors.";
                                $response['data'] = $validator->errors()->toArray();
                            }
                        }
                    } else {
                        $response['status'] = 'error';
                        $response['message'] = "Validation Errors.";
                        $response['data'] = $validator->errors()->toArray();
                    }
                } else {
                    $validator = Validator::make($request->all(), [

                        'ims_standard_ids.*' => 'sometimes|exists:standards,id',
                        'default_ims_surveillance_frequency' => 'sometimes|nullable|in:' . collect(CompanyStandards::getEnumValues()->get('surveillance_frequency'))->implode(','),
                        'default_ims_accreditation_ids.*' => 'sometimes|exists:accreditations,id',
                    ]);

                    if (!$validator->fails()) {
                        if (!empty($request->ims_standard_ids) && count($request->ims_standard_ids) > 1) {
                            $company_standards = CompanyStandards::where('company_id', $company->id)->whereIn('standard_id', $request->get('ims_standard_ids'))->get();

                            $company_standards->each(function ($item, $key) use ($request) {
                                for ($i = 0; $i < sizeof($request->default_ims_accreditation_ids); $i++) {
                                    $company_standard_accreditation = CompanyStandardAccreditation::where('company_standard_id', $item->id)->where('accreditation_id', $request->default_ims_accreditation_ids[$i])->first();
                                    if (!empty($company_standard_accreditation)) {
                                        $company_standard_accreditation->accreditation_id = $request->default_ims_accreditation_ids[$i];
                                        $company_standard_accreditation->save();
                                    } else {
                                        $com_accre = new CompanyStandardAccreditation();
                                        $com_accre->company_standard_id = $item->id;
                                        $com_accre->accreditation_id = $request->default_ims_accreditation_ids[$i];
                                        $com_accre->save();
                                    }


                                }


                            });
                        } else {
                        }


                        $response['status'] = 'success';
                        $response['message'] = "Company Profile Saved.";
                        $response['data'] = [
                            'company' => $company,
                            'company_standards' => CompanyStandards::with('standard')->where('company_id', $company->id)->get(),
                            'current_step' => 4,
                            'next_step' => 6
                        ];
                    } else {
                        $response['status'] = 'error';
                        $response['message'] = "Something Went Wrong.";
                        $response['data'] = $validator->errors()->toArray();
                    }
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Something Went Wrong.";
                $response['data'] = $validator->errors()->toArray();
                if (isset($response['data']['company_id'])) {
                    $response['message'] = $response['data']['company_id'][0];
                }
            }
        } elseif ($request->step == 5) {

            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'required|exists:companies,id'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $validator = Validator::make($request->all(), [
                    'step_action' => 'required|in:upload_documents,remove_document',
                    'documents' => 'required_if:step_action,upload_documents|array',
                    'document_ids' => 'required_if:step_action,remove_document',
                    'documents.*' => 'required_with:documents|file',
                    'date' => 'required_with:documents|date',
                    'document_type' => ['required_with:documents', Rule::in(CompanyStandardDocument::getEnumValues()->get('document_type'))],
                    'standards' => ['required_with:documents', 'array', Rule::in(CompanyStandards::where('company_id', $request->company_id)->get()->pluck('standard_id'))]
                ], [
                    'step_action.required' => "Action Required.",
                    'step_action.in' => "Unknown Action.",
                    'documents.required' => "Documents Required.",
                    'documents.array' => "Something Went Wrong.",
                    'documents.*.file' => "Documents Required.",
                    'date.required' => "Date Required.",
                    'date.date' => "Invalid Date Format.",
                    'document_type.required' => "Document Type Required.",
                    'document_type.in' => "Invalid Document Type.",
                    'standards.*' => "Standards Required.",
                ]);

                if (!$validator->fails()) {
                    if ($request->step_action == 'upload_documents') {
                        $documents_saved = [];
                        $documents_models = [];
                        $documents = $request->file('documents');
                        foreach ($documents as $request_document) {
                            $org_filename = $request_document->getClientOriginalName();
                            // $filename       = $request->document_type . '_' . rand(999, 99999) . '_' . $org_filename;
                            $path = storage_path('app/' . $request_document->storeAs('public', $org_filename));
                            foreach ($request->standards as $standard) {
                                $document = new CompanyStandardDocument;
                                $document->company_standard_id = CompanyStandards::where(['company_id' => $request->company_id, 'standard_id' => $standard])->first()->id;
                                $document->date = Carbon::parse($request->date)->format('Y-m-d');
                                $document->document_type = $request->document_type;
                                $document->save();
                                $document->media_id = $document->addMedia($path)
                                    ->withCustomProperties(['document_type' => $request->document_type])
                                    ->toMediaCollection()->id;
                                $document->save();
                                $documents_saved[] = $document->id;
                                $path = storage_path('app/' . $request_document->storeAs('public', $org_filename));
                            }
                            $documents_models[] = $document;
                            @unlink($path);
                        }
                        $company->company_form_progress = 100;
                        $company->company_form_status = "completed";
                        $response['status'] = 'success';
                        $response['message'] = "Documents Uploaded.";
                        $response['data'] = [
                            'company' => $company,
                            'documents' => $documents_saved,
                            'uploaded_doc_card' => view('company.ajax.uploaded_doc_card', ['request' => $request, 'documents' => $documents_models, 'documents_saved' => $documents_saved])->render()
                        ];
                    } else {
                        CompanyStandardDocument::whereIn('id', explode(',', $request->document_ids))->delete();
                        $response['status'] = 'success';
                        $response['message'] = "Documents Removed.";
                    }
                } else {
                    $response['status'] = 'error';
                    $response['message'] = "Form Has Some Errors.";
                    $response['data'] = $validator->errors()->toArray();
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
            }
        } elseif ($request->step == 6) {
            $validator = Validator::make($request->all(), [
                'action' => 'sometimes',
                'company_id' => 'required|exists:companies,id'
            ], [
                'company_id.required' => "Please Complete Step 1 First.",
                'company_id.exists' => "Please Complete Step 1 First."
            ]);

            if (!$validator->fails()) {
                $company = Company::find($request->company_id);
                $company->company_form_progress = 100;
                $company->company_form_status = "completed";
                $company->save();

                $response['status'] = 'success';
                $response['message'] = "Company Updated Successfully.";
                $response['data'] = $company;
            } else {
                $response['status'] = 'error';
                $response['message'] = "Form Has Some Errors.";
                $response['data'] = $validator->errors()->toArray();
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Something Went Wrong.";
        }
//        $company = Company::find($request->company_id);

//        $company->save();

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $company = Company::findOrFail($id);

        if (!empty($company->companyStandards) && count($company->companyStandards) > 0) {
            foreach ($company->companyStandards as $companyStandard) {
                CompanyStandardCode::where('company_standard_id', $companyStandard->id)->delete();
                CompanyStandardEnergyCode::where('company_standard_id', $companyStandard->id)->delete();
                CompanyStandardFoodCode::where('company_standard_id', $companyStandard->id)->delete();
                CompanyStandardsAnswers::where('company_standards_id', $companyStandard->id)->delete();

                $companyStandard->delete();
            }
        }

        $company->delete();

        return redirect()->route('company.index')->with(['flash_status' => "success", 'flash_message' => "Company Successfully Removed."]);
    }

    /**
     * viw standard the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function viewStandard(Request $request)
    {

        $company_standards = CompanyStandards::where('company_id', $request->get('company_id'))->where('standard_id', $request->get('standard_id'))->first();

        $response['status'] = 'success';
        $response['data'] = [
            'company_standards' => $company_standards,
        ];

        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function removeMediaDocument(Request $request)
    {

        $media = Media::findOrFail($request->get('id'));
        $media->delete();
        $company_standard_document = CompanyStandardDocument::where('media_id', $request->get('id'))->delete();
        $response['status'] = 'success';
        $response['message'] = "Document deleted Successfully.";
        return $response;

    }

    public function checkStandard(Request $request)
    {
        $response = array('status' => '', 'message' => "");

        $standard = Standards::where('id', $request->standard_id)->first();
        $prefixStandard = explode(':', $standard->name);
        $activeStandards = Standards::where('name', 'like', '%' . $prefixStandard[0] . '%')->where('status', true)->get();


        /*Check If AJ Approved for Stage 1 */

        $auditJustification = AuditJustification::where('company_id', $request->company_id)->whereHas('ajStandards', function ($q) use ($standard) {
            $q->where('standard_id', $standard->id)->whereNull('deleted_at')->whereHas('ajStandardStages', function ($query) {
                $query->where('audit_type', 'stage_1')->where('status', 'approved');
            });
        })->first();

        if (is_null($auditJustification)) {

        }


        if (!empty($activeStandards) && count($activeStandards) > 0) {
            foreach ($activeStandards as $data) {
                $company_standards = CompanyStandards::where('company_id', $request->get('company_id'))->where('standard_id', $data->id)->first();
                if (!is_null($company_standards)) {
                    if ($data->number > $standard->number) {
                        $response['status'] = 'warning';
                        $response['message'] = 'You have already Selected new Standard.';
                    } else {
                        $response['status'] = 'warning';
                        $response['message'] = 'If you need Latest Version of this Standard.Please Remove this previous standard first.';

                    }

                } else {
                    $response['status'] = 'success';
                    $response['message'] = 'You have selected one Standard.';
                }
            }

        }
        return $response;
    }

    public function checkStandardApproved(Request $request)
    {
        $response = array('status' => '', 'message' => "");
        $standard = Standards::where('id', $request->standard_id)->first();


        /*Check If AJ Approved for Stage 1 */

        $auditJustification = AuditJustification::where('company_id', $request->company_id)->whereHas('ajStandards', function ($q) use ($standard) {
            $q->where('standard_id', $standard->id)->whereNull('deleted_at')->whereHas('ajStandardStages', function ($query) {
                $query->where('audit_type', 'stage_1')->where('status', 'approved');
            });
        })->first();

        if (is_null($auditJustification)) {
            $response['status'] = 'sucesss';
            $response['message'] = '';
        } else {
            $response['status'] = 'warning';
            $response['message'] = 'Aj is approved.Standard will not delete.';
        }
        return $response;
    }

    public function sendToIAS(Request $request)
    {

        if(isset($request->type) && $request->type === 'reset'){
            $company = Company::where('id',$request->company_id)->first();
            $company->is_send_to_ias = true;
            $company->save();
            return redirect()->back()->with(['flash_status' => "success", 'flash_message' => "Successfully Reset the record.It will not send to IAS System"]);
        }
        $companies = Company::whereId($request->company_id)->whereRaw('name <> ""')->where('is_send_to_ias', false)->with(['region', 'country', 'city', 'primaryContact', 'childCompanies'])
            ->whereHas('companyStandards.certificates', function ($q) {
                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
            })->get();
        foreach ($companies as $key => $company) {
            $companies[$key]->companyStandards = CompanyStandards::
            where('company_id', $company->id)
                ->whereIn('type', ['base', 'single'])
                ->whereHas('certificates', function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                })->with(['companyStandardCodes', 'companyStandardFoodCodes', 'companyStandardEnergyCodes', 'standard.standardFamily', 'certificates' => function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                }])->get();

            $auditTypes = ['surveillance_1'];
            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                    $q->where('standard_id', $companyStd->standard_id)
                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                        });
                })->whereIn('audit_type', $auditTypes)->get();
            }

            $this->curlRequest($company);
        }
        return redirect()->back()->with(['flash_status' => "success", 'flash_message' => "Add Updated to IAS System successfully."]);
    }

    public function curlRequest($company)
    {
        $job_number = '';
        $findIAS = false;
        if (!empty($company->companyStandards) && count($company->companyStandards) > 0) {
            foreach ($company->companyStandards as $companyStandard) {
                $isIAS = false;
                if ($companyStandard->is_ims == true) {
                    if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {
                        $job_number = $companyStandard->ajStandardStage[0]->job_number;
                        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                            $standardData = [];

                            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                $standardNameData = [];
                                if ($imsCompanyStandard->standard->standardFamily->name == 'Food') {
                                    if (!empty($imsCompanyStandard->companyStandardFoodCodes) && count($imsCompanyStandard->companyStandardFoodCodes) > 0) {

                                        foreach ($imsCompanyStandard->companyStandardFoodCodes as $code) {
                                            if (str_contains($code, 'IAS')) {
                                                $isIAS = true;
                                                if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                    $standardName = 'ISO/IEC 27001:2013';
                                                } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                    $standardName = 'ISO/IEC 27001:2022';
                                                } else {
                                                    $standardName = $imsCompanyStandard->standard->name;
                                                }
                                               $schemaName = $this->standardSchemaName($standardName);
                                                if ($isIAS == true) {
                                                    array_push($standardNameData, [
                                                        "standards_name" => $standardName
                                                    ]);
                                                    $data = [
                                                        "scheme_name" => $schemaName,
                                                        "standard_list" => $standardNameData
                                                    ];
                                                    array_push($standardData, $data);

                                                }
                                            }
                                            break;
                                        }
                                    }

                                } elseif ($imsCompanyStandard->standard->standardFamily->name == 'Energy Management') {
                                    if (!empty($imsCompanyStandard->companyStandardEnergyCodes) && count($imsCompanyStandard->companyStandardEnergyCodes) > 0) {
                                        $standardNameData = [];
                                        foreach ($imsCompanyStandard->companyStandardEnergyCodes as $code) {
                                            if (str_contains($code, 'IAS')) {
                                                $isIAS = true;
                                                if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                    $standardName = 'ISO/IEC 27001:2013';
                                                } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                    $standardName = 'ISO/IEC 27001:2022';
                                                } else {
                                                    $standardName = $imsCompanyStandard->standard->name;
                                                }
                                                $schemaName = $this->standardSchemaName($standardName);
                                                if ($isIAS == true) {
                                                    array_push($standardNameData, [
                                                        "standards_name" => $standardName
                                                    ]);
                                                    $data = [
                                                        "scheme_name" => $schemaName,
                                                        "standard_list" => $standardNameData
                                                    ];
                                                    array_push($standardData, $data);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                } else {
                                    if (!empty($imsCompanyStandard->companyStandardCodes) && count($imsCompanyStandard->companyStandardCodes) > 0) {
                                        $standardNameData = [];
                                        foreach ($imsCompanyStandard->companyStandardCodes as $code) {
                                            if (str_contains($code, 'IAS')) {
                                                $isIAS = true;
                                                if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                    $standardName = 'ISO/IEC 27001:2013';
                                                } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                    $standardName = 'ISO/IEC 27001:2022';
                                                } else {
                                                    $standardName = $imsCompanyStandard->standard->name;
                                                }
                                                $schemaName = $this->standardSchemaName($standardName);
                                                if ($isIAS == true) {
                                                    array_push($standardNameData, [
                                                        "standards_name" => $standardName
                                                    ]);
                                                    $data = [
                                                        "scheme_name" => $schemaName,
                                                        "standard_list" => $standardNameData
                                                    ];
                                                    array_push($standardData, $data);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }


                            }

                        }


                    }
                } else {
                    if ($companyStandard->standard->standardFamily->name == 'Food') {
                        if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
                            foreach ($companyStandard->companyStandardFoodCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                                break;
                            }
                        }

                    } elseif ($companyStandard->standard->standardFamily->name == 'Energy Management') {
                        if (!empty($companyStandard->companyStandardEnergyCodes) && count($companyStandard->companyStandardEnergyCodes) > 0) {
                            foreach ($companyStandard->companyStandardEnergyCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                                break;
                            }
                        }
                    } else {
                        if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                            foreach ($companyStandard->companyStandardCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                                break;

                            }
                        }
                    }


                    if ($isIAS === true) {
                        $standardData = [];
                        $standardNameData = [];
                        if ($companyStandard->standard->name === 'ISO 27001:2013') {
                            $standardName = 'ISO/IEC 27001:2013';
                        } else if ($companyStandard->standard->name === 'ISO 27001:2022') {
                            $standardName = 'ISO/IEC 27001:2022';
                        } else {
                            $standardName = $companyStandard->standard->name;
                        }
                        $schemaName = $this->standardSchemaName($standardName);

                        array_push($standardNameData, [
                            "standards_name" => $standardName
                        ]);
                        $data = [
                            "scheme_name" => $schemaName,
                            "standard_list" => $standardNameData
                        ];
                        array_push($standardData, $data);

                    }
                }


                if ($isIAS === true) {
                    if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {

                        $sites = [];
                        $job_number = $companyStandard->ajStandardStage[0]->job_number;
                        $findIAS = true;

                        $status = '';
                        if ($companyStandard->certificates->certificate_status === 'certified') {
                            $status = 'active';
                        } elseif ($companyStandard->certificates->certificate_status === 'suspended') {
                            $status = 'suspended';
                        } elseif ($companyStandard->certificates->certificate_status === 'withdrawn') {
                            $status = 'withdrawn';
                        }
                        $latest_dates = SchemeInfoPostAudit::whereHas('postAudit', function ($q) use ($companyStandard) {
                            $q->where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard_id);
                        })->latest()->first();
                        if (!is_null($latest_dates)) {
                            $original_issue_date = date('Ymd', strtotime(($latest_dates->original_issue_date)));
                        } else {
                            $original_issue_date = '';
                        }
                        if (!is_null($companyStandard->last_certificate_issue_date)) {
                            $issue_date = date("Ymd", strtotime($companyStandard->last_certificate_issue_date));
                        } else {
                            $issue_date = '';
                        }

                        if (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) {
                            $expiry_date = date('Ymd', strtotime(($latest_dates->new_expiry_date)));
                        } else {
                            $expiry_date = '';
                        }
                        if (!empty($company->childCompanies) && count($company->childCompanies) > 0) {
                            foreach ($company->childCompanies as $keyData => $child_company) {
                                $data = [
                                    "street" => $child_company->address,
                                    "city" => str_replace(' ', '', $child_company->city->name),
                                    "state" => "-",
                                    "country" => $child_company->country ? $this->countryName($child_company->country->name) : $this->countryName($company->country->name),
                                    "postcode" => $child_company->city->post_code,
                                    "scope_description" => ""
                                ];
                                array_push($sites, $data);
                            }
                        } else {
//                            $data = [
//                                "street" => "-",
//                                "city" => "-",
//                                "state" => "-",
//                                "country" => "-",
//                                "postcode" => "-",
//                                "scope_description" => "-"
//                            ];
//                            array_push($sites, $data);
                        }

                        $headers = [
                            'x-http-authorization: ' . config('app.IAF')['KEY'],
                            'Content-Type: application/json'
                        ];
                        $curl = curl_init();

                        $postData = [
                            "certificate_number" => $job_number,
                            "certificate_identity_number" => 'CIN-'.$job_number,
                            "certification_status" => $status,
                            "certificate_accreditation_status" => "Accredited",
                            "certification_type" => "Management System",
                            "certification_scope" => $company->scope_to_certify,
                            "certification_original_issue_date" => $original_issue_date,
                            "certification_issue_date" => $issue_date,
                            "certification_expiry_date" => $expiry_date,
                            "certified_entity_name" => $company->name,
                            "certified_entity_english_name" => "-",
                            "certified_entity_trading_name" => "-",
                            "certified_entity_unique_id" => 'CEUID-'.$company->id,
                            "certified_entity_street_address" => $company->address,
                            "certified_entity_street_city" => $company->city->name,
                            "certified_entity_state" => "-",
                            "certified_entity_post_code" => $company->city->post_code,
                            "certified_entity_country" => $this->countryName($company->country->name),
                            "certified_entity_website" => "-",
                            "accreditation_body_name" => "International Accreditation Services",
                            "accreditation_body_acronym_name" => "IAS",
                            "schemes" => $standardData,
                            "sites" => $sites
                        ];
//                        array_push($this->finalData, $postData);

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://api.iafcertsearch.org/api/client/v1/cb/upload-cert',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => json_encode($postData),
                            CURLOPT_HTTPHEADER => $headers,
                        ));

                        $response = curl_exec($curl);

                        curl_close($curl);
//                        echo $response;

                        $response = json_decode($response);
                        if (isset($response) && isset($response->data) && isset($response->data->company_certifications_id) && $response->data->company_certifications_id !== '') {

                            $company = Company::where('id', $company->id)->first();
                            $company->is_send_to_ias = true;
                            $company->save();

                            $record = IASCertificateStats::whereDate('upload_date', date('Y-m-d'))->first();
                            if (!is_null($record)) {
                                $record->success_count = $record->success_count + 1;
                                $record->save();
                            } else {
                                IASCertificateStats::create([
                                    'success_count' => 1,
                                    'error_count' => 0,
                                    'upload_date' => date('Y-m-d')
                                ]);
                            }
                        } else {
                            $record = IASCertificateStats::whereDate('upload_date', date('Y-m-d'))->first();
                            if (!is_null($record)) {
                                $record->error_count = $record->error_count + 1;
                                $record->save();
                            } else {
                                IASCertificateStats::create([
                                    'success_count' => 0,
                                    'error_count' => 1,
                                    'upload_date' => date('Y-m-d')
                                ]);
                            }
                            IASCertificateError::create([
                                'upload_date' => date('Y-m-d'),
                                'error_response' => json_encode($response),
                                'company_id' => $company->id,
                                'standard_id' => $companyStandard->standard_id,
                                'company_standard_id' => $companyStandard->id,
                                'aj_standard_id' => $companyStandard->ajStandardStage[0]->aj_standard_id,
                                'aj_standard_stage_id' => $companyStandard->ajStandardStage[0]->id,
                            ]);
                        }
                    }
                }
            }
        }


    }

    public function standardSchemaName($standard)
    {
        $name = '';
        if ($standard == 'ISO 9001:2015') {
            $name = 'Quality Management Systems';
        } elseif ($standard == 'ISO 27001:2013' || $standard == 'ISO 27001:2022' || $standard == 'ISO/IEC 27001:2013' || $standard == 'ISO/IEC 27001:2022') {
            $name = 'Information Security Management Systems';
        } elseif ($standard == 'ISO 14001:2015') {
            $name = 'Environmental Management Systems';
        } elseif ($standard == 'ISO 45001:2018') {
            $name = 'Occupational Health and Safety Management Systems';
        } elseif ($standard == 'ISO 22000:2018') {
            $name = 'Food Safety Management Systems';
        } elseif ($standard == 'ISO 50001:2018') {
            $name = 'Energy Management Systems';
        } elseif ($standard == 'ISO 29993:2017') {
            $name = 'Learning Services outside formal Education Service Requirements';
        } elseif ($standard == 'ISO 22301:2019') {
            $name = 'Business Continuity Management Systems';
        }

        return $name;

    }

    public function countryName($countryName)
    {
        $name = '';
        if (strtolower($countryName) == strtolower('Bangladesh')) {
            $name = 'Bangladesh';
        } elseif (strtolower($countryName) == strtolower('Kingdom of Bahrain')) {
            $name = 'Bahrain';
        } elseif (strtolower($countryName) == strtolower('Kingdom of Saudi Arabia')) {
            $name = 'Saudi Arabia';
        } elseif (strtolower($countryName) == strtolower('Kuwait')) {
            $name = 'Kuwait';
        } elseif (strtolower($countryName) == strtolower('Oman')) {
            $name = 'Oman';
        } elseif (strtolower($countryName) == strtolower('Pakistan')) {
            $name = 'Pakistan';
        } elseif (strtolower($countryName) == strtolower('United Arab Emirates')) {
            $name = 'United Arab Emirates';
        } elseif (strtolower($countryName) == strtolower('USA')) {
            $name = 'United States';
        }
        return $name;
    }
}
