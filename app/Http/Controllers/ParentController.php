<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Cookie;


class ParentController extends Controller
{

    public $check_employee_rights;
    public $redirectUrl = null;
    
    public function VerifyRights() {
        $this->getAccRights();
        if(!DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id . ' and name = "/'.explode('/', url()->current())[3].'"')->first()){
         
        DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id . ' and name = "company" ')->first() ? $this->redirectUrl = "company" : (DB::table('model_has_permissions')->whereRaw('model_id = '. Auth::user()->id)->first() ? $this->redirectUrl = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id)->first()->name : $this->redirectUrl = "logout");
        
        }
}


    public function getAccRights() {
        $this->check_employee_rights = DB::table('model_has_permissions')->where('model_id', Auth::user()->id)->get();
        //  dd($this->check_employee_rights);
    }
}
