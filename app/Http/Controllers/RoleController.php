<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use App\Traits\AccessRights;
class RoleController extends Controller
{
    use AccessRights;
    public function index(){
                // $this->VerifyRights();
        $roles = Role::all();
                // if($this->redirectUrl){return redirect($this->redirectUrl);}
        return view('role.index', compact('roles'), ['check_rights' => $this->check_employee_rights]);
    }

    public function create(){
        return view('role.create');
    }

    public function store(Request $request){
        $this->validator($request->all())->validate();
        Role::create([
            'name' => $request->role,
            'guard_name' => 'web'
        ]);

        return redirect()->route('roles.index')->with('success', 'Role has been created successfully');
    }

    public function edit($id){
        $role = Role::findById($id);

        return view('role.edit', compact('role'));
    }

    public function update(Request $request, $id){
        $role = Role::findById($id);
        $this->validator($request->all())->validate();
        $role->update([
           'name' => $request->role,
           'guard_name' => 'web'
        ]);

        return back()->with('success', 'Role has been updated successfully');
    }

    public function destroy($id){
        $permission = Role::findById($id);
        $permission->delete();

        return back()->with('success', 'Role has been deleted successfully');
    }

    public function validator(array $data){
        return Validator::make($data, [
           'role' => 'required|string'
        ]);
    }
}
