<?php

namespace App\Http\Controllers;

use App\Imports\MandaysImport;
use App\ManDaySheet;
use App\StandardFamilySheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ManDayTableController extends Controller
{
    public function importExcel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if (!$validator->fails()) {
            $generic_sheet_family = new StandardFamilySheet();
            ManDaySheet::truncate();
            if ($request->hasFile('file')) {
                $image = $request->file('file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/sheet_documents');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                StandardFamilySheet::where('name', 'generic_family')->delete();

                $generic_sheet_family->name = 'generic_family';
                $generic_sheet_family->file = $name;
                $generic_sheet_family->save();
            }
            $file = $request->file('file');
            Excel::import(new MandaysImport(), public_path('/uploads/sheet_documents/sample_generic_manday_sheet.xlsx'));
            $sheets= ManDaySheet::all();
            $data = [
                'status' => 200,
                'generic_sheet_family' => $generic_sheet_family->file,
                'records' => $sheets
            ];

            return response()->json($data);
        } else {

            ManDaySheet::truncate();
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }

    }

    public function filterSheet(Request $request)
    {
        $filter_records = ManDaySheet::where(function ($query) use ($request) {

            if ($request->has('standard') && $request->standard != NULL) {
                $query->where('standard_id', $request->standard);
            }
            if ($request->has('from') && $request->has('to')) {
                if ($request->from != NULL && $request->to != NULL) {

                    $query->where('effective_employes_on', (int)$request->from);
                    $query->where('effective_employes_off', (int)$request->to);
                }
            }
            if ($request->has('complexity') && $request->complexity != NULL) {
                $query->where('complexity', $request->complexity);
            }
        })->get();


//        $filter_records = ManDaySheet::where('standard_id', $request->standard)
//                            ->orWhere("effective_employes_on", $request->from)
//                            ->orWhere("effective_employes_off", $request->to)
//                            ->orWhere('complexity', $request->complexity)
//                            ->get();
        if (!empty($filter_records)) {

            return view('dataentry.MandayFilterSheet', compact('filter_records'));
        } else {

            $data = [
                'status' => 'fail',
                'message' => 'Something went wrong'
            ];

            return response()->json($data);
        }

    }
}
