<?php

namespace App\Http\Controllers;

use App\Accreditation;
use App\Auditor;
use App\AuditorConfidentialityAgreement;
use App\AuditorDocument;
use App\AuditorEducation;
use App\AuditorEmploymentHistory;
use App\AuditorStandard;
use App\Cities;
use App\Company;
use App\CompanyContacts;
use App\CompanyStandards;
use App\CompanyStandardsAnswers;
use App\Countries;
use App\IAF;
use App\IAS;
use App\Languages;
use App\Notification;
use App\Regions;
use App\ResidentCity;
use App\ResidentCountry;
use App\Standards;
use App\StandardsFamily;
use App\TechnicalExpert;
use App\TechnicalExpertConfidentialityAgreement;
use App\TechnicalExpertEducation;
use App\TechnicalExpertLanguages;
use App\TechnicalExpertStandard;
use App\TechnicalExpertStandardGrade;
use App\User;
use App\UserLanguages;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\AccessRights;
use Spatie\Permission\Models\Permission;


class TechnicalExpertController extends Controller
{

    use AccessRights;

    public function index()
    {
        if (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'management') {
            $technicalExperts = Auditor::where('role', 'technical_expert')->get();

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }
            $technicalExperts = Auditor::where('role', 'technical_expert')->whereHas('auditorStandards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->get();

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $technicalExperts = Auditor::where('role', 'technical_expert')->where('region_id', auth()->user()->region_id)->get();

        } else {
            $technicalExperts = Auditor::where('role', 'technical_expert')->where('user_id', auth()->user()->id)->get();
        }


        $technicalExpertStandards = [];
        foreach ($technicalExperts as $key => $technicalExpert) {
            $technicalExpertStandards[$key] = AuditorStandard::where('auditor_id', $technicalExpert->id)->where('deleted_at', NULL)->get();
        }


        return view('technical-experts.index', compact('technicalExperts', 'technicalExpertStandards'));
    }

    public function create()
    {
        if (auth()->user()->user_type == 'admin') {
            $regions = Regions::all();
            $standards_families = StandardsFamily::orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $standards_families = StandardsFamily::orderBy('sort')->get();
        }


        $languages = Languages::all();
        $countries = Countries::all();


        $data = array(
            'regions' => $regions,
            'languages' => $languages,
            'countries' => $countries,
            'standards_families' => $standards_families
        );
        return view('technical-experts.create')->with($data);
    }

    public function deleteTechnicalExpert($auditorId)
    {
        $deleteAuditor = Auditor::find($auditorId)->delete();

        if ($deleteAuditor) {

            return redirect()->back()->with(['status' => 'success', 'message' => 'Technical Expert Deleted Successfully']);

        } else {
            return redirect()->back()->with('error', 'Failed to delete Technical Experts');
        }

    }

    public function show($auditorId)
    {

        $auditor = Auditor::find($auditorId);

        $user_language = TechnicalExpertLanguages::select()
            ->where('technical_expert_id', $auditor->id)
            ->groupBy('language_id')
            ->get();
        $auditor_language = [];
        foreach ($user_language as $key => $language) {
            $auditor_language[$key] = Languages::where('id', $language->language_id)->get();

        }

        $technicalExpertEducation = AuditorEducation::where('auditor_id', $auditorId)->get();
        $technicalExpertAgreement = AuditorConfidentialityAgreement::where('auditor_id', $auditorId)->get();

        return view('technical-experts.show', compact('auditor', 'auditor_language', 'technicalExpertEducation', 'technicalExpertAgreement'));

    }

    public function edit($auditorId)
    {
        $auditor = Auditor::find($auditorId);
        if (auth()->user()->user_type == 'admin') {
            $regions = Regions::all();
            $standards_families = StandardsFamily::orderBy('sort')->get();
            $standards = Standards::orderBy('name')->get();
            $auditorStandardCount = AuditorStandard::where('auditor_id', '=', $auditorId)->where('auditor_standard_status', 'unapproved')->count();
            $notification_count = Notification::where('request_id', '=', $auditorId)->where('status', 'unapproved')->where('type', 'technical_expert')->count();


        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
            $regions = Regions::all();
            if (auth()->user()->user_type == 'scheme_coordinator') {
                $scheme_manager_id = auth()->user()->scheme_coordinator->scheme_manager->id;
            } else {
                $scheme_manager_id = auth()->user()->scheme_manager->id;
            }

            $standards_families = StandardsFamily::whereHas('standards.scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('sort')->get();

            $standards = Standards::whereHas('scheme_manager', function ($q) use ($scheme_manager_id) {
                $q->where('scheme_manager_id', $scheme_manager_id);
            })->orderBy('name')->get();
            $auditorStandardCount = AuditorStandard::where('auditor_id', '=', $auditorId)->where('auditor_standard_status', 'unapproved')->count();
            $notification_count = Notification::where('request_id', '=', $auditorId)->where('status', 'unapproved')->where('type', 'technical_expert')->count();


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {
            $regions = Regions::where('id', auth()->user()->region_id)->get();
            $standards_families = StandardsFamily::orderBy('sort')->get();
            $standards = Standards::orderBy('name')->get();
            $auditorStandardCount = AuditorStandard::where('auditor_id', '=', $auditorId)->where('auditor_standard_status', 'unapproved')->count();
            $notification_count = Notification::where('request_id', '=', $auditorId)->where('status', 'unapproved')->where('type', 'technical_expert')->count();
        }


        $languages = Languages::all();
        $countries = Countries::all();

        $country_zones = Zone::where('country_id', $auditor->country_id)->pluck('id')->toArray();

        if (count($country_zones) > 0) {
            $cities = Cities::whereIn('zoneable_id', $country_zones)->where('zoneable_type', 'App\Zone')->get();
        } else {
            $cities = Cities::where('zoneable_id', $auditor->country_id)->where('zoneable_type', 'App\Countries')->get();
        }
//        $cities             = ResidentCity::where('country_id', $auditor->country_id)->get();
        $accreditations = Accreditation::orderBy('name')->get();
        $iafs = IAF::orderBy('code')->get();
        $iass = IAS::orderBy('code')->get();

        $data = array(
            'regions' => $regions,
            'languages' => $languages,
            'countries' => $countries,
            'standards_families' => $standards_families,
            'cities' => $cities,
            'auditor' => $auditor,
            'standards' => $standards,
            'accreditations' => $accreditations,
            'iafs' => $iafs,
            'auditorStandardCount' => $auditorStandardCount,
            'notification_count' => $notification_count,
        );


        return view('technical-experts.edit')->with($data);

    }

    public function updateTechnicalExpertStatus($auditorId)
    {
        $response = array();
        try {

            $auditor = Auditor::find($auditorId);

            if ($auditor) {
                $toggleStatus = 'active';

                $auditStatus = $auditor->auditor_status;
                if ($auditStatus == 'active') {
                    $toggleStatus = 'inactive';
                }


                $updateAuditorStatus = $auditor->update([
                    'auditor_status' => $toggleStatus
                ]);

                $response = (new ApiMessageController())->saveresponse('Technical Expert Status Updated Successfully');

            } else {
                $response = (new ApiMessageController())->failedresponse('Technical Expert Does Not Exist');
            }


        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function store(Request $request)
    {

        $response = array();

        if ($request->step == 1) {
            if ($request->has('auditor_id')) {
                $validator = Validator::make($request->all(), [
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
//                    'email' => 'email',
                    'phone' => 'required|numeric',
                    'region_id' => 'required|exists:regions,id',
                    'country_id' => 'required|exists:countries,id',
                    'city_id' => 'required|exists:cities,id',
                    'working_experience' => 'required|min:0.1',
                    'main_education' => 'required|string',
                    'nationality_id' => 'required|exists:countries,id',
                    'job_status' => ['required', Rule::in(Auditor::getEnumValues()->get('job_status'))],
                ], [
                    'region_id.required' => "Region Required.",
                    'region_id.exists' => "Invalid Region Selected.",
                    'country_id.required' => "Country Required.",
                    'country_id.exists' => "Invalid Country Selected.",
                    'city_id.required' => "City Required.",
                    'city_id.exists' => "Invalid City Selected.",
                    'nationality_id.required' => "Nationality Required.",
                    'nationality_id.exists' => "Invalid Nationality Selected.",
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
//                    'email' => 'email',
                    'phone' => 'required|numeric',
                    'region_id' => 'required|exists:regions,id',
                    'country_id' => 'required|exists:countries,id',
                    'city_id' => 'required|exists:cities,id',
                    'working_experience' => 'required|numeric|min:0',
                    'main_education' => 'required',
                    'nationality_id' => 'required|exists:countries,id',
                    'job_status' => [Rule::in(Auditor::getEnumValues()->get('job_status'))],

                ], [
                    'region_id.required' => "Region Required.",
                    'region_id.exists' => "Invalid Region Selected.",
                    'country_id.required' => "Country Required.",
                    'country_id.exists' => "Invalid Country Selected.",
                    'city_id.required' => "City Required.",
                    'city_id.exists' => "Invalid City Selected.",
                    'nationality_id.required' => "Nationality Required.",
                    'nationality_id.exists' => "Invalid Nationality Selected.",

                ]);
            }

            if (!$validator->fails()) {
                if ($request->has('auditor_id')) {
                    $auditor = Auditor::find($request['auditor_id']);
                } else {
                    $auditor = new Auditor();
                    $auditor->auditor_status = $request->auditor_status;
                }
                $auditor->first_name = $request->first_name;
                $auditor->last_name = $request->last_name;
                $auditor->email = $request->email;
                $auditor->phone = $request->phone;
                $auditor->landline = $request->landline;
                $auditor->postal_address = $request->postal_address;
                $auditor->region_id = $request->region_id;
                $auditor->country_id = $request->country_id;
                $auditor->city_id = $request->city_id;
                $auditor->dob = date("Y/m/d", strtotime($request->dob));
                $auditor->working_experience = $request->working_experience;
                $auditor->main_education = $request->main_education;
                $auditor->nationality_id = $request->nationality_id;
                $auditor->job_status = $request->job_status;
                $auditor->main_education = $request->main_education;
                $auditor->profile_remarks = $request->profile_remarks;
                $auditor->form_progress_step = $request->step;

                if ($request->has('auditor_id')) {

//                    $auditor->form_progress = 100;
//                    $auditor->form_current_progress = 'completed';
                } else {
                    $auditor->form_progress = 25;

                }
                $auditor->role = 'technical_expert';
                $auditor->save();


                //this languages are only for technical expert
                if ($request->has('language_id') || !empty($request->language_id)) {
                    foreach ($request->language_id as $lang) {
                        $technicalExpertLang = new TechnicalExpertLanguages();
                        $technicalExpertLang->technical_expert_id = $auditor->id;
                        $technicalExpertLang->language_id = $lang;
                        $technicalExpertLang->save();
                    }

                }

                if ($auditor)

                    $data = [
                        'auditor_id' => $auditor->id,
                        'full_name' => $auditor->fullName(),
                        'current_step' => 1,
                        'next_step' => 2
                    ];
                if ($request->has('auditor_id')) {
                    $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Profile Updated Successfully!');
                } else {
                    $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Profile Created Successfully!');
                }

            } else {

                $data = $validator->errors()->toArray();

                $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Something Went Wrong.";
        }

        return $response;
    }


//    Formal Education Start
    public function getTechnicalExpertFormalEducationView($auditor_id = '')
    {
        $auditorEducations = array();

        if (!empty($auditor_id)) {
            $auditorEducations = (new AuditorEducation())->where('auditor_id', '=', $auditor_id)->get();
        }

        return view('technical-experts.partials.formal-education', compact('auditorEducations', 'auditor_id'));
    }

    public function saveTechnicalExpertEducation(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'year_passing' => 'required|numeric',
            'institution' => 'required',
            'degree_diploma' => 'required',
            'major_subjects' => 'required',
            'certificate' => 'file|required',
        ], [
            'auditor_id.required' => "Create Technical Expert First",
            'auditor_id.exists' => "Technical Expert Does Not Exist",
            'certificate.file' => "Profile Image Must be a file!",

        ]);

        if (!$validator->fails()) {

            $auditorEducation = new AuditorEducation();
            $auditorEducation->auditor_id = $request->auditor_id;
            $auditorEducation->year_passing = $request->year_passing;
            $auditorEducation->institution = $request->institution;
            $auditorEducation->degree_diploma = $request->degree_diploma;
            $auditorEducation->major_subjects = $request->major_subjects;
            if ($request->hasFile('certificate')) {

                $image = $request->file('certificate');

                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/technical_expert_education_certificate');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorEducation->education_document = $auditor_files_with_time;

            }

            $auditorEducation->save();

            $auditor = Auditor::find($request['auditor_id']);
            $auditor->form_progress_step = 2;
            $auditor->form_progress = 50;
            $auditor = $auditor->save();


            if ($auditorEducation && $auditor)

                $data = [
                    'status' => 'success',
                    'auditor_id' => $request->auditor_id,
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Education Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function getEditTechnicalExpertEducationModal($educationId)
    {
        try {

            $auditorEducation = (new AuditorEducation())->where('id', '=', $educationId)->first();

            return view('technical-experts.partials.edit-form-education-modal', compact('auditorEducation'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateTechnicalExpertEducation(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'year_passing' => 'required|numeric',
            'institution' => 'required',
            'degree_diploma' => 'required',
            'major_subjects' => 'required',
            'auditor_education_id' => 'required|exists:auditor_educations,id',
        ]);

        if (!$validator->fails()) {

            $auditorEducation = AuditorEducation::find($request->auditor_education_id);
            $auditorEducation->year_passing = $request->year_passing;
            $auditorEducation->institution = $request->institution;
            $auditorEducation->degree_diploma = $request->degree_diploma;
            $auditorEducation->major_subjects = $request->major_subjects;
            if ($request->hasFile('certificate')) {
                $image = $request->file('certificate');

                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/technical_expert_education_certificate');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorEducation->education_document = $auditor_files_with_time;
            }

            $auditorEducation->save();

            if ($auditorEducation)

                $data = [
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Education Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteTechnicalExpertEducation($auditorEducationId)
    {

        try {
            $education = AuditorEducation::findOrFail($auditorEducationId);
            $deleteItem = $education->delete();

            if ($deleteItem) {
                $response = (new ApiMessageController())->saveresponse("Technical Expert Education Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;

    }

//   Formal Education End


//
    public function getTechnicalExpertAgreementView($auditor_id = '')
    {
        $auditorAgreements = array();

        if (!empty($auditor_id)) {
            $auditorAgreements = (new AuditorConfidentialityAgreement())->where('auditor_id', '=', $auditor_id)->get();
        }

        return view('technical-experts.partials.tech-expert-confidentiality-agreement', compact('auditorAgreements', 'auditor_id'));
    }

    public function saveTechnicalExpertAgreement(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_id' => 'required|exists:auditors,id',
            'date_signed' => 'required',
            'document_file' => 'file|required',
        ], [
            'auditor_id.required' => "Create Technical Expert First",
            'auditor_id.exists' => "Technical Expert Does Not Exist",
            'document_file.file' => "Document Must be a file!",
        ]);

        if (!$validator->fails()) {

            $auditorAgreement = new AuditorConfidentialityAgreement();
            $auditorAgreement->auditor_id = $request->auditor_id;
            $auditorAgreement->date_signed = $request->date_signed;
            if ($request->hasFile('document_file')) {
                $image = $request->file('document_file');

                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/technical_expert_confidentially_agreement');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorAgreement->agreement_document = $auditor_files_with_time;
            }
            $auditorAgreement->save();


            $auditor = Auditor::find($request['auditor_id']);
            $auditor->form_progress_step = 3;
            $auditor->form_progress = 75;
            $auditor = $auditor->save();


            if ($auditorAgreement && $auditor)
                $data = [
                    'status' => 'success',
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Document Added Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }

    public function deleteTechnicalExpertAgreement($auditorAgreementId)
    {

        try {

            $agreement = AuditorConfidentialityAgreement::findOrFail($auditorAgreementId);
            $deleteItem = $agreement->delete();

            if ($deleteItem) {
                $response = (new ApiMessageController())->saveresponse("Technical Expert Agreement Deleted Successfully");
            } else {
                $response = (new ApiMessageController())->failedresponse("Failed to delete Data");
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }

        return $response;


    }

    public function getEditTechnicalExpertAgreementModal($agreementId)
    {
        try {

            $auditorAgreement = (new AuditorConfidentialityAgreement())->where('id', '=', $agreementId)->first();


            return view('technical-experts.partials.edit-agreement-modal', compact('auditorAgreement'));

        } catch
        (\Illuminate\Database\QueryException $ex) {
            $response = (new ApiMessageController())->queryexception($ex);
        }
        return $response;

    }

    public function updateTechnicalExpertAgreement(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'auditor_agreement_id' => 'required|exists:auditor_confidentiality_agreements,id',
            'date_signed' => 'required',
        ], [
            'auditor_agreement_id.required' => "Add Technical Expert Agreement First",
            'auditor_agreement_id.exists' => "Technical Expert Agreement Does Not Exist",
        ]);

        if (!$validator->fails()) {

            $auditorAgreement = AuditorConfidentialityAgreement::find($request->auditor_agreement_id);
            $auditorAgreement->date_signed = $request->date_signed;
            if ($request->hasFile('document_file')) {
                $image = $request->file('document_file');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/technical_expert_confidentially_agreement');
                $auditor_files_with_time = time() . '_' . $name;
                $image->move($destinationPath, $auditor_files_with_time);
                $auditorAgreement->agreement_document = $auditor_files_with_time;
            }
            $auditorAgreement->save();
            if ($auditorAgreement)
                $data = [
                    'current_step' => 2,
                    'next_step' => 2
                ];

            $response = (new ApiMessageController())->successResponse($data, 'Technical Expert Agreement Updated Successfully!');

        } else {

            $data = $validator->errors()->toArray();

            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
        }
        return $response;
    }


    public function finish(Request $request)
    {
        $auditor = Auditor::find($request['auditor_id']);
        $auditor->form_progress_step = 4;
        $auditor->form_progress = 100;

        if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'admin') {
            $auditorStandards = AuditorStandard::where('auditor_id', $auditor->id)->whereNull('deleted_at')->get();

            foreach ($auditorStandards as $auditorStandard) {

                $auditorStandard->auditor_standard_status = 'approved';
                $auditorStandard->save();
                $notification = Notification::where('type_id', (int)$auditorStandard->standard_id)->where('type', 'technical_expert')->where('request_id', (int)$auditor->id)->where('is_read', false)->first();
                if(!is_null($notification)){
                    $notification->is_read = true;
                    $notification->save();
                }
            }
        }
        $auditor->save();

        if ($request->type = "edit") {
            $response = (new ApiMessageController())->saveresponse('Technical Expert Profile Updated Successfully');
        } else {
            $response = (new ApiMessageController())->saveresponse('Technical Expert Profile Created Successfully');
        }
        return $response;
    }

    public function sendForApproval(Request $request)
    {
        $auditor = Auditor::find($request['auditor_id']);
        $auditor->form_progress_step = 4;
        $auditor->form_progress = 100;

        $auditor = $auditor->save();

        if ($request->auditorStandardStatus == 'unapproved') {
            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('auditor_standard_status', $request->auditorStandardStatus)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();
            if (!empty($auditorStandards) && count($auditorStandards) > 0) {


                $auditor = Auditor::find($request['auditor_id']);
                foreach ($auditorStandards as $auditorStandard) {


                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'technical_expert',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $auditorStandard->standard->scheme_manager[0]->user->id,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/technical-expert/edit-technical-expert/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'New for ' . $auditorStandard->standard->name . ' - ' . $auditor->fullName()

                        ]);
                    }


                }
                if ($request->type = "edit" && $request->auditorStandardStatus == 'unapproved') {
                    $response = (new ApiMessageController())->saveresponse('Technical Expert Standard Save and Notifications sent Successfully');
                }
            }


        } elseif ($request->auditorStandardStatus == 'rejected') {

            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('standard_id', (int)$request->standardId)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();

            if (!empty($auditorStandards) && count($auditorStandards) > 0) {

                $auditor = Auditor::find($request['auditor_id']);
                $operation_manager = Notification::where('type_id', (int)$request->standardId)->where('type', 'technical_expert')->where('request_id', (int)$request->auditor_id)->where('is_read', false)->first();


                foreach ($auditorStandards as $auditorStandard) {
                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'technical_expert',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $operation_manager->sent_by,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/technical-expert/edit-technical-expert/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'Rejected for ' . $auditorStandard->standard->name . ' - ' . $auditor->fullName()

                        ]);
                    }
                    $auditorStandard->auditor_standard_status = 'rejected';
                    $auditorStandard->save();

                }
                $operation_manager->is_read = true;
                $operation_manager->save();
                if ($request->type = "edit" && $request->auditorStandardStatus == 'rejected') {

                    $response = (new ApiMessageController())->saveresponse('Technical Expert Save and Notifications sent Successfully');
                }
            }
        } elseif ($request->auditorStandardStatus == 'resent') {

            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('standard_id', (int)$request->standardId)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();

            if (!empty($auditorStandards) && count($auditorStandards) > 0) {

                $auditor = Auditor::find($request['auditor_id']);
                $operation_manager = Notification::where('type_id', (int)$request->standardId)->where('type', 'technical_expert')->where('request_id', (int)$request->auditor_id)->where('is_read', false)->first();


                foreach ($auditorStandards as $auditorStandard) {
                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'technical_expert',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $operation_manager->sent_by,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/technical-expert/edit-technical-expert/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'Re-Submitted for ' . $auditorStandard->standard->name . ' - ' . $auditor->fullName()

                        ]);
                    }
                    $auditorStandard->auditor_standard_status = 'resent';
                    $auditorStandard->save();

                }
                $operation_manager->is_read = true;
                $operation_manager->save();
                if ($request->type = "edit" && $request->auditorStandardStatus == 'resent') {

                    $response = (new ApiMessageController())->saveresponse('Technical Expert Save and Notifications sent Successfully');
                }
            }
        } elseif ($request->auditorStandardStatus == 'approved') {

            $auditorStandards = AuditorStandard::where('auditor_id', (int)$request->auditor_id)->where('standard_id', (int)$request->standardId)->whereNull('deleted_at')->with('standard.scheme_manager.user')->get();

            if (!empty($auditorStandards) && count($auditorStandards) > 0) {

                $auditor = Auditor::find($request['auditor_id']);
                $operation_manager = Notification::where('type_id', (int)$request->standardId)->where('type', 'technical_expert')->where('request_id', (int)$request->auditor_id)->where('is_read', false)->first();


                foreach ($auditorStandards as $auditorStandard) {
                    if (count($auditorStandard->standard->scheme_manager) > 0) {
                        Notification::create([
                            'type' => 'technical_expert',
                            'sent_by' => auth()->user()->id,
                            'sent_to' => $operation_manager->sent_by,
                            'icon' => 'message',
                            'body' => $request->remarks,
                            'url' => '/technical-expert/edit-technical-expert/' . $auditor->id . '#standardsTab',
                            'is_read' => 0,
                            'type_id' => $auditorStandard->standard->id,
                            'request_id' => $auditor->id,
                            'status' => $request->auditorStandardStatus,
                            'sent_message' => 'Approved for ' . $auditorStandard->standard->name . ' - ' . $auditor->fullName()

                        ]);
                    }
                    $auditorStandard->auditor_standard_status = 'approved';
                    $auditorStandard->save();

                }
                $operation_manager->is_read = true;
                $operation_manager->save();
                if ($request->type = "edit" && $request->auditorStandardStatus == 'approved') {

                    $response = (new ApiMessageController())->saveresponse('Technical Expert Save and Notifications sent Successfully');
                }
            }
        }
        return $response;
    }

}
