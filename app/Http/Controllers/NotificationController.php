<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function index()
    {
        if (auth()->user()->user_type == 'admin') {

            $sentNotifications = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_final_read',false)->get();

            $sentPostAuditNotifications = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_final_read',false)->get();

            $sentPostAuditApprovedNotifications = null;

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            $sentNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', '!=', 'post_audit')->get();

            $sentPostAuditNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();


            if (auth()->user()->user_type == 'scheme_manager') {

                $sentPostAuditApprovedNotifications = DB::table('notifications')->select('*')->where('sent_by', auth()->user()->id)->where('is_final_read',false)->where('status', 'approved')->where('type', 'post_audit')->groupBy(DB::raw('(request_id)'))->groupBy('request_id')->get();
            } elseif (auth()->user()->user_type == 'scheme_coordinator') {

                $sentPostAuditApprovedNotifications = null;
            } else {
                $sentPostAuditApprovedNotifications = null;
            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $sentNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read',false)->where('type', '!=', 'post_audit')->get();

            $sentPostAuditNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();

            $sentPostAuditApprovedNotifications = null;


        } else {
            $sentNotifications = null;
            $sentPostAuditNotifications = null;
            $sentPostAuditApprovedNotifications = null;
        }
        return view('notifications.index', compact('sentPostAuditApprovedNotifications', 'sentNotifications', 'sentPostAuditNotifications'));
    }

    public function sentOthers()
    {
        if (auth()->user()->user_type == 'admin') {

            $sentNotifications = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_final_read',false)->get();

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            $sentNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', '!=', 'post_audit')->get();
        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $sentNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('is_read', false)->where('type', '!=', 'post_audit')->get();
        } else {
            $sentNotifications = null;
        }
        return view('notifications.sentothers', compact('sentNotifications'));
    }

    public function sendPostAudit()
    {
        if (auth()->user()->user_type == 'admin') {

            $sentPostAuditNotifications = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_final_read',false)->get();

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            $sentPostAuditNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $sentPostAuditNotifications = Notification::where('sent_by', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();
        } else {
            $sentPostAuditNotifications = null;
        }
        return view('notifications.sent-post-audit', compact('sentPostAuditNotifications'));
    }

    public function sendPostAuditApproved()
    {
        if (auth()->user()->user_type == 'admin') {

            $sentPostAuditApprovedNotifications = null;

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            if (auth()->user()->user_type == 'scheme_manager') {

                $sentPostAuditApprovedNotifications = DB::table('notifications')->select('*')->where('sent_by', auth()->user()->id)->where('is_final_read',false)->where('status', 'approved')->where('type', 'post_audit')->groupBy(DB::raw('(request_id)'))->groupBy('request_id')->get();
            } elseif (auth()->user()->user_type == 'scheme_coordinator') {

                $sentPostAuditApprovedNotifications = null;
            } else {
                $sentPostAuditApprovedNotifications = null;
            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $sentPostAuditApprovedNotifications = null;


        } else {
            $sentPostAuditApprovedNotifications = null;
        }
        return view('notifications.sent-post-audit-approved', compact('sentPostAuditApprovedNotifications'));
    }

    public function received()
    {
        if (auth()->user()->user_type == 'admin') {

            $receivedNotifications = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_final_read',false)->get();

            $receivedPostAuditNotifications = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_final_read',false)->get();

            $receivedPostAuditApprovedNotifications = null;

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('is_read', false)->where('type', '!=', 'post_audit')->get();

            $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();


            if (auth()->user()->user_type == 'scheme_manager') {

                $receivedPostAuditApprovedNotifications = null;

            } elseif (auth()->user()->user_type == 'scheme_coordinator') {

                $receivedPostAuditApprovedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('status', 'approved')->where('type', 'post_audit')->get();

            } else {

                $receivedPostAuditApprovedNotifications = null;
            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', '!=', 'post_audit')->get();

            $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();

            $receivedPostAuditApprovedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('status', 'approved')->where('type', 'post_audit')->get();


        } else {

            $receivedNotifications = null;
            $receivedPostAuditNotifications = null;
            $receivedPostAuditApprovedNotifications = null;
        }
        return view('notifications.received', compact('receivedPostAuditApprovedNotifications', 'receivedNotifications', 'receivedPostAuditNotifications'));
    }

    public function receivedOthers()
    {
        if (auth()->user()->user_type == 'admin') {

            $receivedNotifications = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_final_read',false)->get();

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read',false)->where('type', '!=', 'post_audit')->get();

        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', '!=', 'post_audit')->get();

        } else {

            $receivedNotifications = null;
        }
        return view('notifications.received-others', compact('receivedNotifications'));
    }

    public function receivedPostAudit()
    {
        if (auth()->user()->user_type == 'admin') {

            $receivedPostAuditNotifications = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_final_read',false)->get();

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {


            $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->get();

        } else {

            $receivedPostAuditNotifications = null;
        }
        return view('notifications.received-post-audit', compact('receivedPostAuditNotifications'));
    }

    public function receivedPostAuditApproved()
    {
        if (auth()->user()->user_type == 'admin') {

            $receivedPostAuditApprovedNotifications = null;

        } elseif (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {

            if (auth()->user()->user_type == 'scheme_manager') {

                $receivedPostAuditApprovedNotifications = null;

            } elseif (auth()->user()->user_type == 'scheme_coordinator') {

                $receivedPostAuditApprovedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('status', 'approved')->where('type', 'post_audit')->get();

            } else {

                $receivedPostAuditApprovedNotifications = null;
            }


        } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

            $receivedPostAuditApprovedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_final_read',false)->where('status', 'approved')->where('type', 'post_audit')->get();


        } else {

            $receivedPostAuditApprovedNotifications = null;
        }
        return view('notifications.received-post-audit-approved', compact('receivedPostAuditApprovedNotifications'));
    }

    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();

        return back()->with(['flash_status' => 'success', 'flash_message' => 'Notification has been deleted.']);

    }
}
