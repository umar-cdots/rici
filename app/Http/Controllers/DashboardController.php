<?php

namespace App\Http\Controllers;

use App\Auditor;
use App\AuditorStandard;
use App\Certificate;
use App\Company;
use App\Countries;
use App\Standards;
use App\PostAudit;
use App\AJStandardStage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        ini_set('max_execution_time', '6000');
        $countriesId = Countries::pluck('id')->toArray();
        $standards = Standards::all();
        $auditorsCount = Auditor::where('role', 'auditor')->whereNull('deleted_at')->count();
        $no_of_auditors_count = 0;
        $no_of_technical_experts_count = 0;
        $auditor_evaluation_due_schedule_count = 0;
        $no_of_certified_clients_count = 0;
        $no_of_suspended_clients_count = 0;
        $no_of_withdrawn_clients_count = 0;
        $no_of_certified_per_year_count = 0;
        $no_of_withdrawn_per_year_count = 0;
        $no_of_certified_clients_standard_count = 0;
        $no_of_suspended_clients_standard_count = 0;
        $no_of_withdrawn_clients_standard_count = 0;
        $no_of_audit_conducted = 0;
        $certifiedCompanyPerYearCountsStandards = 0;
        $withdrawnCompanyPerYearCountsStandards = 0;

        foreach ($standards as $standard) {
            $auditorId = Auditor::whereIn('country_id', $countriesId)->where('role', 'auditor')->whereHas('auditorStandards', function ($query) use ($standard) {
                $query->where('standard_id', $standard->id)->whereNull('auditor_standards.deleted_at');
            })->count();

            $no_of_auditors_count = $no_of_auditors_count + ($auditorId);


            $technicalExpertId = Auditor::whereIn('country_id', $countriesId)->where('role', 'technical_expert')->whereHas('auditorStandards', function ($query) use ($standard) {
                $query->where('standard_id', $standard->id)->whereNull('auditor_standards.deleted_at');
            })->count();
            $no_of_technical_experts_count = $no_of_technical_experts_count + ($technicalExpertId);


            $auditorStandardId = AuditorStandard::where('standard_id', $standard->id)->whereHas('auditorStandardWitnessEvaluation', function ($query) {
                $query->whereNull('actual_date')->whereNull('deleted_at');
            })->pluck('auditor_id')->toArray();
            $evalutaionDue = Auditor::whereIn('id', $auditorStandardId)->whereIn('country_id', $countriesId)->where('role', 'auditor')->count();

            $auditor_evaluation_due_schedule_count = $auditor_evaluation_due_schedule_count + ($evalutaionDue);

//            $companyCounts = Company::whereRaw('name <> ""')->whereIn('country_id', $countriesId)->whereHas('companyStandards', function ($query) use ($standard) {
//                $query->where('standard_id', $standard->id);
//            })->whereHas('companyStandards.certificates', function ($query) use ($standard) {
//                $query->whereIn('certificate_status', ['certified', 'suspended'])->where('report_status', 'new');
//            })->get();
            $companyCounts = Company::whereIn('country_id', $countriesId)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->whereIn('certificate_status', ['certified'])->where('report_status', 'new');
                    });
                })->count();

            $no_of_certified_clients_standard_count = $no_of_certified_clients_standard_count + ($companyCounts);

            $companyCounts = Company::whereIn('country_id', $countriesId)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->whereIn('certificate_status', ['suspended'])->where('report_status', 'new');
                    });
                })->count();

            $no_of_suspended_clients_standard_count = $no_of_suspended_clients_standard_count + ($companyCounts);

            $companyCounts = Company::whereIn('country_id', $countriesId)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->whereIn('certificate_status', ['withdrawn'])->where('report_status', 'new');
                    });
                })->count();

            $no_of_withdrawn_clients_standard_count = $no_of_withdrawn_clients_standard_count + ($companyCounts);


            $companyCounts = Company::whereIn('country_id', $countriesId)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->whereIn('certificate_status', ['certified'])->where('report_status', 'new')->whereYear('certificate_date', date('Y'));
                    });
                })->count();

            $certifiedCompanyPerYearCountsStandards = $certifiedCompanyPerYearCountsStandards + ($companyCounts);


            $companyCounts = Company::whereIn('country_id', $countriesId)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->whereIn('certificate_status', ['withdrawn'])->where('report_status', 'new')->whereYear('certificate_date', date('Y'));
                    });
                })->count();

            $withdrawnCompanyPerYearCountsStandards = $withdrawnCompanyPerYearCountsStandards + ($companyCounts);


        }

        $companyCounts = Company::whereRaw('name <> ""')
            ->whereHas('companyStandards.certificates', function ($query) use ($standard) {
                $query->whereIn('certificate_status', ['certified'])->where('report_status', 'new');
            })->get();

        $no_of_certified_clients_count = $no_of_certified_clients_count + count($companyCounts);


        $suspendedCompanyCounts = Company::whereRaw('name <> ""')
            ->whereHas('companyStandards.certificates', function ($query) use ($standard) {
                $query->where('certificate_status', 'suspended')->where('report_status', 'new');
            })->get();
        $no_of_suspended_clients_count = $no_of_suspended_clients_count + count($suspendedCompanyCounts);


        $withdrawnCompanyCounts = Company::whereRaw('name <> ""')
            ->whereHas('companyStandards.certificates', function ($query) use ($standard) {
                $query->where('certificate_status', 'withdrawn')->where('report_status', 'new');
            })->get();
        $no_of_withdrawn_clients_count = $no_of_withdrawn_clients_count + count($withdrawnCompanyCounts);


        $certifiedCompanyPerYearCounts = Company::whereRaw('name <> ""')
            ->whereHas('companyStandards.certificates', function ($query) use ($standard) {
                $query->where('certificate_status', 'certified')->where('report_status', 'new')->whereYear('certificate_date', date('Y'));
            })->get();
        $no_of_certified_per_year_count = $no_of_certified_per_year_count + count($certifiedCompanyPerYearCounts);


        $withdrawnCompanyPerYearCounts = Company::whereRaw('name <> ""')
            ->whereHas('companyStandards.certificates', function ($query) use ($standard) {
                $query->where('certificate_status', 'withdrawn')->where('report_status', 'new')->whereYear('certificate_date', date('Y'));
            })->get();
        $no_of_withdrawn_per_year_count = $no_of_withdrawn_per_year_count + count($withdrawnCompanyPerYearCounts);


        $PostAuditCountStageWise = AJStandardStage::whereHas('postAudit', function ($q) {
            $q->whereYear('actual_audit_date', date('Y'));
        })->whereIn('audit_type', ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5', 'stage_1', 'stage_2', 'reaudit'])->get();


        $no_of_audit_conducted = $no_of_audit_conducted + count($PostAuditCountStageWise);


        return view('dashboard.index')->with(['withdrawnCompanyPerYearCountsStandards' => $withdrawnCompanyPerYearCountsStandards, 'certifiedCompanyPerYearCountsStandards' => $certifiedCompanyPerYearCountsStandards, 'no_of_suspended_clients_standard_count' => $no_of_suspended_clients_standard_count, 'no_of_withdrawn_clients_standard_count' => $no_of_withdrawn_clients_standard_count, 'no_of_certified_clients_standard_count' => $no_of_certified_clients_standard_count, 'auditorsCount' => $auditorsCount, 'no_of_auditors_count' => $no_of_auditors_count, 'no_of_technical_experts_count' => $no_of_technical_experts_count, 'auditor_evaluation_due_schedule_count' => $auditor_evaluation_due_schedule_count, 'no_of_certified_clients_count' => $no_of_certified_clients_count, 'no_of_suspended_clients_count' => $no_of_suspended_clients_count, 'no_of_withdrawn_clients_count' => $no_of_withdrawn_clients_count, 'no_of_certified_per_year_count' => $no_of_certified_per_year_count, 'no_of_withdrawn_per_year_count' => $no_of_withdrawn_per_year_count, 'no_of_audit_conducted' => $no_of_audit_conducted]);
    }

    public function noOfAuditors()
    {
        $countries = Countries::all();
        $standards = Standards::all();
        $countriesId = \App\Countries::pluck('id')->toArray();


        return view('statics.no_of_auditors')->with(['countries' => $countries, 'standards' => $standards, 'countriesId' => $countriesId]);
    }

    public function noOfTechnicalExperts()
    {
        $countries = Countries::all();
        $standards = Standards::all();
        $countriesId = \App\Countries::pluck('id')->toArray();


        return view('statics.no_of_technical_experts')->with(['countries' => $countries, 'standards' => $standards, 'countriesId' => $countriesId]);
    }

    public function auditorEvaluationsDue()
    {
        $countries = Countries::all();
        $standards = Standards::all();
        $countriesId = \App\Countries::pluck('id')->toArray();


        return view('statics.auditor_evaluations_due')->with(['countries' => $countries, 'standards' => $standards, 'countriesId' => $countriesId]);
    }

    public function noOfAuditorsGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        $standard_id = $request->standard_id;
        $standard = Standards::where('id', $request->standard_id)->first();
        foreach ($countries as $country) {
            $auditorcount = \App\Auditor::where('country_id', $country->id)->where('role', 'auditor')->whereHas('auditorStandards', function ($query) use ($standard_id) {
                $query->where('standard_id', $standard_id)->whereNull('auditor_standards.deleted_at');
            })->get();

            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($auditorcount));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
            'standard_name' => $standard->name,
        ]);
    }

    public function noOfTechnicalExpertGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        $standard_id = $request->standard_id;
        $standard = Standards::where('id', $request->standard_id)->first();
        foreach ($countries as $country) {
            $technicalExpertId = Auditor::where('country_id', $country->id)->where('role', 'technical_expert')->whereHas('auditorStandards', function ($query) use ($standard_id) {
                $query->where('standard_id', $standard_id)->whereNull('auditor_standards.deleted_at');
            })->get();

            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($technicalExpertId));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
            'standard_name' => $standard->name,
        ]);
    }

    public function auditorEvaluationDueGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        $standard_id = $request->standard_id;
        $standard = Standards::where('id', $request->standard_id)->first();
        foreach ($countries as $country) {
            $auditorStandardId = AuditorStandard::where('standard_id', $standard_id)->whereHas('auditorStandardWitnessEvaluation', function ($query) {
                $query->whereNull('actual_date')->whereNull('deleted_at');
            })->pluck('auditor_id')->toArray();
            $auditorId = \App\Auditor::whereIn('id', $auditorStandardId)->where('country_id', $country->id)->where('role', 'auditor')->get();

            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($auditorId));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
            'standard_name' => $standard->name,
        ]);
    }


    public function certifiedClient()
    {
        $countries = Countries::all();
        $standards = Standards::all();
        $countriesId = \App\Countries::pluck('id')->toArray();


        return view('statics.no_of_certified_clients')->with(['countries' => $countries, 'standards' => $standards, 'countriesId' => $countriesId]);
    }

    public function certifiedClientGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        $standard_id = $request->standard_id;
        $standard = Standards::where('id', $request->standard_id)->first();
        foreach ($countries as $country) {

            $companyCounts = \App\Company::where('country_id', $country->id)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->whereIn('certificate_status', ['certified', 'suspended'])->where('report_status', 'new');
                    });
                })->get();


            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($companyCounts));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
            'standard_name' => $standard->name,
        ]);
    }


    public function suspendedClient()
    {
        $countries = Countries::all();
        $standards = Standards::all();
        $countriesId = \App\Countries::pluck('id')->toArray();


        return view('statics.no_of_suspended_clients')->with(['countries' => $countries, 'standards' => $standards, 'countriesId' => $countriesId]);
    }

    public function suspendedClientGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        $standard_id = $request->standard_id;
        $standard = Standards::where('id', $request->standard_id)->first();
        foreach ($countries as $country) {

            $companyCounts = \App\Company::where('country_id', $country->id)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->where('certificate_status', 'suspended')->where('report_status', 'new');
                    });
                })->get();


            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($companyCounts));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
            'standard_name' => $standard->name,
        ]);
    }


    public function withdrawnClient()
    {
        $countries = Countries::all();
        $standards = Standards::all();
        $countriesId = \App\Countries::pluck('id')->toArray();


        return view('statics.no_of_withdrawn_clients')->with(['countries' => $countries, 'standards' => $standards, 'countriesId' => $countriesId]);
    }

    public function withdrawnClientGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        $standard_id = $request->standard_id;
        $standard = Standards::where('id', $request->standard_id)->first();
        foreach ($countries as $country) {

            $companyCounts = \App\Company::where('country_id', $country->id)->whereRaw('name <> ""')
                ->whereHas('companyStandards', function ($q) use ($standard) {
                    $q->where('standard_id', $standard->id)->whereHas('certificates', function ($query) use ($standard) {
                        $query->where('certificate_status', 'withdrawn')->where('report_status', 'new');
                    });
                })->get();


            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($companyCounts));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
            'standard_name' => $standard->name,
        ]);
    }


    public function certifiedPerYear()
    {
        $countries = Countries::all();
        $countriesId = \App\Countries::pluck('id')->toArray();
        $years = Certificate::distinct('certificate_date')->get([DB::raw('YEAR(certificate_date) as year')]);

        return view('statics.no_of_certified_per_year')->with(['countries' => $countries, 'countriesId' => $countriesId, 'years' => $years]);
    }

    public function certifiedPerYearGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();
        foreach ($countries as $country) {

            $companyCounts = Company::where('country_id', $country->id)->whereRaw('name <> ""')
                ->whereHas('companyStandards.certificates', function ($query) use ($request) {
                    $query->where('certificate_status', 'certified')->where('report_status', 'new')->whereYear('certificate_date', $request->year);
                })->get();


            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($companyCounts));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
        ]);
    }


    public function withdrawnPerYear()
    {
        $countries = Countries::all();
        $countriesId = \App\Countries::pluck('id')->toArray();
        $years = Certificate::distinct('created_at')->get([DB::raw('YEAR(created_at) as year')]);


        return view('statics.no_of_withdrawn_per_year')->with(['countries' => $countries, 'countriesId' => $countriesId, 'years' => $years]);
    }

    public function withdrawnPerYearGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_country = [];
        $countries = Countries::all();

        foreach ($countries as $country) {

            $companyCounts = Company::where('country_id', $country->id)->whereRaw('name <> ""')
                ->whereHas('companyStandards.certificates', function ($query) use ($request) {
                    $query->where('certificate_status', 'withdrawn')->where('report_status', 'new')->whereYear('certificate_date', $request->year);
                })->get();


            array_push($graph_data_country, $country->iso);
            array_push($graph_data_count, count($companyCounts));

        }
        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_country' => $graph_data_country,
        ]);
    }


    public function auditConducted()
    {
        $years = PostAudit::distinct('actual_audit_date')->get([DB::raw('YEAR(actual_audit_date) as year')]);
        return view('statics.audit_conducted')->with(['years' => $years]);
    }

    public function auditConductedGraph(Request $request)
    {
        $graph_data_count = [];
        $graph_data_stage = [];
        $PostAuditCountStage1 = \App\AJStandardStage::whereHas('postAudit', function ($query) use ($request) {
            $query->whereYear('actual_audit_date', $request->year);
        })->where('audit_type', 'stage_1')->get();

        array_push($graph_data_stage, 'Stage 1');
        array_push($graph_data_count, count($PostAuditCountStage1));

        $PostAuditCountStage2 = \App\AJStandardStage::whereHas('postAudit', function ($query) use ($request) {
            $query->whereYear('actual_audit_date', $request->year);
        })->where('audit_type', 'stage_2')->get();

        array_push($graph_data_stage, 'Stage 2');
        array_push($graph_data_count, count($PostAuditCountStage2));

        $PostAuditCountSurveillance = \App\AJStandardStage::whereHas('postAudit', function ($query) use ($request) {
            $query->whereYear('actual_audit_date', $request->year);
        })->whereIn('audit_type', ['surveillance_1', 'surveillance_2', 'surveillance_3', 'surveillance_4', 'surveillance_5'])->get();

        array_push($graph_data_stage, 'Surveillance');
        array_push($graph_data_count, count($PostAuditCountSurveillance));


        $PostAuditCountReaudit = \App\AJStandardStage::whereHas('postAudit', function ($query) use ($request) {
            $query->whereYear('actual_audit_date', $request->year);
        })->where('audit_type', 'reaudit')->get();

        array_push($graph_data_stage, 'Reaudit');
        array_push($graph_data_count, count($PostAuditCountReaudit));

        return response()->json([
            'graph_data_count' => $graph_data_count,
            'graph_data_stage' => $graph_data_stage,
        ]);
    }

}
