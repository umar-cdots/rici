<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserRoleController extends Controller
{


    public function index(){
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function create($id){
        $user = User::findOrFail($id);
        $roles = Role::all();
        $permissions = Permission::all();

        return view('users.assign-role-to-user', compact('permissions', 'user', 'roles'));
    }

    public function store(Request $request){
        //dd($request->all());
        $this->validator($request->all())->validate();
        $user = User::findOrFail($request->user_id);
        $user->syncRoles($request->roles);
        if($request->has('permissions')){
            $user->syncPermissions($request->permissions);
        }

        return redirect()->route('users.index')->with('success', 'Roles and permissions are assigned to users');
    }

    public function validator(array $data){
        return Validator::make($data, [
           'roles.*' => 'required|integer',
           'permissions.*' => 'integer'
        ]);
    }
}
