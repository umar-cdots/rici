<?php

namespace App\Http\Controllers;

use App\LetterName;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index(){
        return view('profiles/index');
    }

    public function update(Request $request){
        if(isset($request->comments)){
            $letter = LetterName::first();
            $letter->comments = $request->comments === '1' || $request->comments === 1 ? 1 : 0;
            if(auth()->user()->user_type === 'scheme_manager'){
                $letter->pk_days = $request->pk_days;
                $letter->ksa_days = $request->ksa_days;
                $letter->cut_of_days = $request->cut_of_days;
            }
            $letter->save();

        }else{
            $letter = LetterName::first();
            $letter->comments = 0;
            $letter->save();
        }
        $currentPassword = $request->get('current-password');
        if(isset($currentPassword) && !is_null($currentPassword)){


            if (!(Hash::check($request->get('current-password'), auth()->user()->password))) {
                // The passwords matches
                return redirect()->back()->with(
                    [
                        'flash_status'=>'error',
                        'flash_message'=>'Your current password does not matches with the password you provided. Please try again.'
                    ]);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same

                return redirect()->back()->with(
                    [
                        'flash_status'=>'error',
                        'flash_message'=>'New Password cannot be same as your current password. Please choose a different password..'
                    ]);
            }

            $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6',
            ]);

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user = Auth::user();
                $user->password = bcrypt($request->get('new-password'));
                if(!is_null($request->email)){
                    $user->email =$request->email;
                }
                if(!is_null($request->username)){
                    $user->username = $request->username;
                }

                $user->save();




                return redirect()->back()
                    ->with([
                        'flash_status' => 'success',
                        'flash_message' => 'Password Changed successfully.'
                    ]);

            } else {
                return redirect()->back()
                    ->with([
                        'flash_status' => 'error',
                        'flash_message' => 'New Password must be same as your confirm password.'
                    ]);
            }
        }else{

            return redirect()->back()
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Profile updated successfully.'
                ]);
        }

    }
}
