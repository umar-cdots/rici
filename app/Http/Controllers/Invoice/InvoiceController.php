<?php

namespace App\Http\Controllers\Invoice;


use App\AJStandardStage;
use App\Company;
use App\CompanyStandards;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\InvoiceDataEntry;
use App\InvoiceNotification;
use App\Mail\InvoiceMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;


class InvoiceController extends Controller
{
    public function index($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id)
    {
        $invoice = Invoice::where(['company_id' => $company_id, 'company_standard_id' => $company_standard_id, 'aj_standard_id' => $aj_standard_id, 'aj_standard_stage_id' => $aj_standard_stage_id])->first();
        return view('invoice.index', compact('company_id', 'company_standard_id', 'aj_standard_id', 'aj_standard_stage_id', 'invoice'));
    }

    public function letterCreate($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type)
    {
        $company = Company::whereId($company_id)->with('primaryContact','city')->first(['id', 'name','city_id']);
        $companyStandard = CompanyStandards::whereId($company_standard_id)->first();
        $aj = AJStandardStage::whereId($aj_standard_stage_id)->first();
        $invoiceSettings = InvoiceDataEntry::first();
        $taxAuthorities = explode(',', $invoiceSettings->tax_authorities);
        $taxPercentages = explode(',', $invoiceSettings->tax_percentage);
        $inflationPercentages = explode(',', $invoiceSettings->inflation_percentage);
        return view('invoice.letter-create', compact('company_id', 'company_standard_id', 'aj_standard_id', 'aj_standard_stage_id', 'letter_type', 'company', 'companyStandard', 'aj', 'taxAuthorities', 'taxPercentages','inflationPercentages'));
    }

    public function letterStore(Request $request, $company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type)
    {


        if ($request->has('attached_file')) {
            $image = $request->file('attached_file');
            $name = $image->getClientOriginalName();
            $filename = public_path('invoiceUploads/attachments') . $name;

            if (File::exists($filename)) {
                File::delete($filename);  // or unlink($filename);
            }

            $destinationPath = public_path('/invoiceUploads/attachments');
            $additional_files_with_time = time() . '_' . $name;
            $image->move($destinationPath, $additional_files_with_time);
            $attached_file = $additional_files_with_time;
        } else {
            return redirect()->back()->with('flash_status', 'error')
                ->with('flash_message', 'File attachment is required');
        }


        if (isset($request->standard) && !is_null($request->standard) && $request->standard === 'ISO 27001:2013') {
            $standardName = 'ISO 27001:2022';
        } else {
            $standardName = $request->standard;
        }
        $invoice = Invoice::create([
            'user_id' => Auth::id(),
            'company_id' => $company_id,
            'company_standard_id' => $company_standard_id,
            'aj_standard_id' => $aj_standard_id,
            'aj_standard_stage_id' => $aj_standard_stage_id,
            'invoice_number' => $request->invoice_number,
            'audit_type' => $request->audit_type,
            'standard' => $standardName,
            'company_name' => $request->company_name,
            'contact_name' => $request->title . '. ' . $request->contact_name,
            'designation' => $request->designation,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'invoice_unit' => $request->invoice_unit,
            'invoice_amount' => $request->invoice_amount,
            'tax_authority' => $request->tax_authority,
            'tax_percentage' => $request->tax_percentage,
            'inflation_percentage' => $request->inflation_percentage,
            'invoice_date' => date('Y-m-d', strtotime($request->invoice_date)),
            'due_date' => date('Y-m-d', strtotime($request->due_date)),
            'letter_one_print' => ($letter_type === 'letter1') ? 1 : 0,
            'letter_one_date' => ($letter_type === 'letter1') ? date('Y-m-d', strtotime($request->invoice_date)) : null,
            'letter_two_print' => ($letter_type === 'letter2') ? 1 : 0,
            'letter_two_date' => ($letter_type === 'letter2') ? date('Y-m-d', strtotime($request->invoice_date)) : null,
            'letter_three_print' => ($letter_type === 'letter3') ? 1 : 0,
            'letter_three_date' => ($letter_type === 'letter3') ? date('Y-m-d', strtotime($request->invoice_date)) : null,
            'attached_file' => $attached_file,
            'is_attached_audit_plan' => $request->is_attached_audit_plan == '1' || $request->is_attached_audit_plan == 1 ? true : false,
        ]);
        if ($letter_type === 'letter1') {
            $message = 'Letter 1 has been saved successfully.';
        } elseif ($letter_type === 'letter2') {
            $message = 'Letter 2 has been saved successfully.';
        } else {
            $message = 'Letter 3 has been saved successfully.';
        }

        return redirect()->route('invoice.letter.edit', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice->id, 'create'])->with('flash_status', 'success')
            ->with('flash_message', $message);
    }

    public function letterEdit($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id, $type)
    {
        $company = Company::whereId($company_id)->with('primaryContact','city')->first(['id', 'name','city_id']);
        $companyStandard = CompanyStandards::whereId($company_standard_id)->first();
        $aj = AJStandardStage::whereId($aj_standard_stage_id)->first();
        $invoice = Invoice::whereId($invoice_id)->first();

        $notifications = InvoiceNotification::where('invoice_id', $invoice_id)->orderBy('id', 'desc')->get();
        if ($letter_type === 'letter1') {
            $checkMail = InvoiceNotification::where('invoice_id', $invoice_id)->where('letter1_email', 1)->latest()->first();
        } elseif ($letter_type === 'letter2') {
            $checkMail = InvoiceNotification::where('invoice_id', $invoice_id)->where('letter2_email', 1)->latest()->first();
        } elseif ($letter_type === 'letter3') {
            $checkMail = InvoiceNotification::where('invoice_id', $invoice_id)->where('letter3_email', 1)->latest()->first();
        } else {
            $checkMail = null;
        }
        $invoiceSettings = InvoiceDataEntry::first();
        $taxAuthorities = explode(',', $invoiceSettings->tax_authorities);
        $taxPercentages = explode(',', $invoiceSettings->tax_percentage);
        $inflationPercentages = explode(',', $invoiceSettings->inflation_percentage);
        return view('invoice.letter-edit', compact('company_id', 'company_standard_id', 'aj_standard_id', 'aj_standard_stage_id', 'letter_type', 'company', 'companyStandard', 'aj', 'invoice', 'type', 'notifications', 'checkMail', 'taxAuthorities', 'taxPercentages','inflationPercentages'));

    }

    public function letterUpdate(Request $request, $company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id)
    {

        $invoice = Invoice::whereId($invoice_id)->first();

        if ($letter_type === 'letter2') {
            $invoice->email = $request->email;
            $invoice->letter_two_print = 1;
            $invoice->letter_two_date = date('Y-m-d', strtotime($request->letter_two_date));
        } elseif ($letter_type === 'letter3') {
            $invoice->email = $request->email;
            $invoice->letter_three_print = 1;
            $invoice->letter_three_date = date('Y-m-d', strtotime($request->letter_three_date));
        }

        $invoice->is_attached_audit_plan =  $request->is_attached_audit_plan == '1' || $request->is_attached_audit_plan == 1 ? true : false;
        $invoice->save();
        if ($letter_type === 'letter1') {
            $message = 'Letter 1 has been saved successfully.';
        } elseif ($letter_type === 'letter2') {
            $message = 'Letter 2 has been saved successfully.';
        } else {
            $message = 'Letter 3 has been saved successfully.';
        }
        return redirect()->route('invoice.letter.edit', [$company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice->id, 'edit'])->with('flash_status', 'success')
            ->with('flash_message', $message);
    }

    public function letterPaid(Request $request, $company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id)
    {
        $invoice = Invoice::where(['company_id' => $company_id, 'company_standard_id' => $company_standard_id, 'aj_standard_id' => $aj_standard_id, 'aj_standard_stage_id' => $aj_standard_stage_id])->first();
        $company = Company::whereId($company_id)->with('primaryContact')->first(['id', 'name']);
        $companyStandard = CompanyStandards::whereId($company_standard_id)->first();
        $aj = AJStandardStage::whereId($aj_standard_stage_id)->first();
        if (!is_null($invoice)) {
            $invoice->invoice_status = 1;
            $invoice->save();
        } else {

            if ($companyStandard->is_ims == true) {
                $companyStandard1 = CompanyStandards::where('id', $companyStandard->id)->first();
                $company = \App\Company::where('id', $company->id)->first();
                $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                $ims_heading = $companyStandard1->standard->name;
                if ($aj->audit_type === 'reaudit') {
                    $invoiceAmount = $companyStandard->re_audit_amount;
                } else {
                    $invoiceAmount = $companyStandard->surveillance_amount;
                }
                if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                    foreach ($imsCompanyStandards as $imsCompanyStandard) {
                        if ($companyStandard1->id == $imsCompanyStandard->id) {
                            continue;
                        } else {
                            $ims_heading .= ', ' . $imsCompanyStandard->standard->name;
                            if ($aj->audit_type === 'reaudit') {
                                $invoiceAmount = $invoiceAmount + $imsCompanyStandard->re_audit_amount;
                            } else {
                                $invoiceAmount = $invoiceAmount + $imsCompanyStandard->surveillance_amount;
                            }
                        }
                    }
                }

                $standardName = $ims_heading;
            } else {
                $standardName = $companyStandard->standard->name;
                if($standardName === 'ISO 27001:2013'){
                    $standardName = 'ISO 27001:2022';
                }
                if ($aj->audit_type === 'reaudit') {
                    $invoiceAmount = $companyStandard->re_audit_amount;
                } else {
                    $invoiceAmount = $companyStandard->surveillance_amount;
                }
            }

            $invoice = Invoice::create([
                'user_id' => Auth::id(),
                'company_id' => $company_id,
                'company_standard_id' => $company_standard_id,
                'aj_standard_id' => $aj_standard_id,
                'aj_standard_stage_id' => $aj_standard_stage_id,
                'invoice_number' => substr($aj->job_number, 4) . '/' . date('d/m/y'),
                'audit_type' => str_replace('_', ' ', ucfirst($aj->audit_type)),
                'standard' => $standardName,
                'company_name' => $company->name,
                'contact_name' => $company->primaryContact->title . '. ' . $company->primaryContact->name,
                'designation' => $company->primaryContact->position,
                'phone_number' => $company->primaryContact->contact,
                'email' => $company->primaryContact->email,
                'invoice_unit' => $companyStandard->surveillance_currency,
                'invoice_amount' => $invoiceAmount,
                'tax_authority' => null,
                'tax_percentage' => null,
                'inflation_percentage' => null,
                'invoice_date' => date('Y-m-d'),
                'due_date' => date('Y-m-d', strtotime($aj->excepted_date)),
                'letter_one_print' => 0,
                'letter_one_date' => null,
                'letter_two_print' => 0,
                'letter_two_date' => null,
                'letter_three_print' => 0,
                'letter_three_date' => null,
                'invoice_status' => 1,
            ]);;
        }


        return redirect()->back()->with('flash_status', 'success')
            ->with('flash_message', 'Your invoice has been paid successfully');
    }


    public function printLetter($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id)
    {

        if ($letter_type === 'letter1') {
            return response()->download($this->printLetter1($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id));
        } elseif ($letter_type === 'letter2') {
            return response()->download($this->printLetter2($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id));
        } elseif ($letter_type === 'letter3') {
            return response()->download($this->printLetter3($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id));
        } elseif ($letter_type === 'invoice') {
            return response()->download($this->printInvoice($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id));
        } else {
            return redirect()->back()->with('flash_status', 'warning')
                ->with('flash_message', 'Something went wrong. Please try again');
        }

    }

    public function printLetter1($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id)
    {

        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('invoiceUploads/invoices/Letter1.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);


        $invoiceSettings = InvoiceDataEntry::first();


        $other_details_name = $invoiceSettings->other_details_name;
        $other_details_address = $invoiceSettings->other_details_address;
        $other_details_email = $invoiceSettings->other_details_email;
        $other_details_phone_number = $invoiceSettings->other_details_phone_number;
        $other_user_name = $invoiceSettings->other_user_name;
        $other_user_designation = $invoiceSettings->other_user_designation;
        $other_user_email = $invoiceSettings->other_user_email;
        $other_user_phone_number = $invoiceSettings->other_user_phone_number;

        $invoice = Invoice::whereId($invoice_id)->first();
        if (isset($invoice->standard) && !is_null($invoice->standard) && $invoice->standard === 'ISO 27001:2013') {
            $standardName = 'ISO 27001:2022';
        } else {
            $standardName = $invoice->standard;
        }
        $company = Company::whereId($company_id)->first(['id', 'city_id', 'country_id', 'address']);


        $my_template->setValue('contact_name', $invoice->contact_name);
        $my_template->setValue('designation', $invoice->designation);
        $my_template->setValue('company_name', $invoice->company_name);
        $my_template->setValue('address', $company->address);
        $my_template->setValue('city', $company->city ? $company->city->name : '');
        $my_template->setValue('country', $company->country ? $company->country->name : '');
        $my_template->setValue('audit_type', $invoice->audit_type);
        $my_template->setValue('standard', $standardName);
        $my_template->setValue('due_date', date("F, y", strtotime($invoice->due_date)));
        $my_template->setValue('audit', strtolower($invoice->audit_type) === 'reaudit' ? 'Reaudit' : 'Surveillance');
        $my_template->setValue('invoice_number', $invoice->invoice_number);
        $my_template->setValue('invoice_date', date('d/m/Y', strtotime($invoice->invoice_date)));

        $my_template->setValue('other_details_name', $other_details_name);
        $my_template->setValue('other_details_address', $other_details_address);
        $my_template->setValue('other_details_email', $other_details_email);
        $my_template->setValue('other_details_phone_number', $other_details_phone_number);
        $my_template->setValue('other_user_name', $other_user_name);
        $my_template->setValue('other_user_designation', $other_user_designation);
        $my_template->setValue('other_user_email', $other_user_email);
        $my_template->setValue('other_user_phone_number', $other_user_phone_number);
        $my_template->setValue('main_user_name', Auth::user()->fullName());
        $my_template->setValue('main_user_designation', 'Operations Manager');
        $my_template->setValue('main_user_email', Auth::user()->email);
        $my_template->setValue('main_user_phone_number', Auth::user()->phone_number);

        try {
            $my_template->saveAs(public_path('invoiceUploads/finalInvoices/Letter1-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        $public_path = public_path('invoiceUploads/finalInvoices/Letter1-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx');
        return $public_path;

    }

    public function printLetter2($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id)
    {
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('invoiceUploads/invoices/Letter2.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);


        $invoiceSettings = InvoiceDataEntry::first();


        $other_details_name = $invoiceSettings->other_details_name;
        $other_details_address = $invoiceSettings->other_details_address;
        $other_details_email = $invoiceSettings->other_details_email;
        $other_details_phone_number = $invoiceSettings->other_details_phone_number;
        $other_user_name = $invoiceSettings->other_user_name;
        $other_user_designation = $invoiceSettings->other_user_designation;
        $other_user_email = $invoiceSettings->other_user_email;
        $other_user_phone_number = $invoiceSettings->other_user_phone_number;

        $invoice = Invoice::whereId($invoice_id)->first();
        if (isset($invoice->standard) && !is_null($invoice->standard) && $invoice->standard === 'ISO 27001:2013') {
            $standardName = 'ISO 27001:2022';
        } else {
            $standardName = $invoice->standard;
        }
        $company = Company::whereId($company_id)->first(['id', 'city_id', 'country_id', 'address']);


        $my_template->setValue('contact_name', $invoice->contact_name);
        $my_template->setValue('designation', $invoice->designation);
        $my_template->setValue('company_name', $invoice->company_name);
        $my_template->setValue('address', $company->address);
        $my_template->setValue('city', $company->city ? $company->city->name : '');
        $my_template->setValue('country', $company->country ? $company->country->name : '');
        $my_template->setValue('audit_type', $invoice->audit_type);
        $my_template->setValue('standard', $standardName);
        $my_template->setValue('due_date', date("F, y", strtotime($invoice->due_date)));
        $my_template->setValue('audit', strtolower($invoice->audit_type) === 'reaudit' ? 'Reaudit' : 'Surveillance');
        $my_template->setValue('invoice_number', $invoice->invoice_number);
        $my_template->setValue('invoice_date', date('d/m/Y', strtotime($invoice->letter_two_date)));
        $my_template->setValue('letter_one_date', date('d/m/Y', strtotime($invoice->letter_one_date)));

        $my_template->setValue('other_details_name', $other_details_name);
        $my_template->setValue('other_details_address', $other_details_address);
        $my_template->setValue('other_details_email', $other_details_email);
        $my_template->setValue('other_details_phone_number', $other_details_phone_number);
        $my_template->setValue('other_user_name', $other_user_name);
        $my_template->setValue('other_user_designation', $other_user_designation);
        $my_template->setValue('other_user_email', $other_user_email);
        $my_template->setValue('other_user_phone_number', $other_user_phone_number);
        $my_template->setValue('main_user_name', Auth::user()->fullName());
        $my_template->setValue('main_user_designation', 'Operations Manager');
        $my_template->setValue('main_user_email', Auth::user()->email);
        $my_template->setValue('main_user_phone_number', Auth::user()->phone_number);

        try {
            $my_template->saveAs(public_path('invoiceUploads/finalInvoices/Letter2-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }

        $public_path = public_path('invoiceUploads/finalInvoices/Letter2-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx');
        return $public_path;


    }

    public function printLetter3($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id)
    {
        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('invoiceUploads/invoices/Letter3.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);


        $invoiceSettings = InvoiceDataEntry::first();


        $other_details_name = $invoiceSettings->other_details_name;
        $other_details_address = $invoiceSettings->other_details_address;
        $other_details_email = $invoiceSettings->other_details_email;
        $other_details_phone_number = $invoiceSettings->other_details_phone_number;
        $other_user_name = $invoiceSettings->other_user_name;
        $other_user_designation = $invoiceSettings->other_user_designation;
        $other_user_email = $invoiceSettings->other_user_email;
        $other_user_phone_number = $invoiceSettings->other_user_phone_number;

        $invoice = Invoice::whereId($invoice_id)->first();
        if (isset($invoice->standard) && !is_null($invoice->standard) && $invoice->standard === 'ISO 27001:2013') {
            $standardName = 'ISO 27001:2022';
        } else {
            $standardName = $invoice->standard;
        }
        $company = Company::whereId($company_id)->first(['id', 'city_id', 'country_id', 'address']);


        $my_template->setValue('contact_name', $invoice->contact_name);
        $my_template->setValue('designation', $invoice->designation);
        $my_template->setValue('company_name', $invoice->company_name);
        $my_template->setValue('address', $company->address);
        $my_template->setValue('city', $company->city ? $company->city->name : '');
        $my_template->setValue('country', $company->country ? $company->country->name : '');
        $my_template->setValue('audit_type', $invoice->audit_type);
        $my_template->setValue('standard', $standardName);
        $my_template->setValue('due_date', date("F, y", strtotime($invoice->due_date)));
        $my_template->setValue('audit', strtolower($invoice->audit_type) === 'reaudit' ? 'Reaudit' : 'Surveillance');
        $my_template->setValue('invoice_number', $invoice->invoice_number);
        $my_template->setValue('invoice_date', date('d/m/Y', strtotime($invoice->letter_three_date)));
        $my_template->setValue('letter_one_date', date('d/m/Y', strtotime($invoice->letter_one_date)));
        $my_template->setValue('letter_two_date', date('d/m/Y', strtotime($invoice->letter_two_date)));

        $my_template->setValue('other_details_name', $other_details_name);
        $my_template->setValue('other_details_address', $other_details_address);
        $my_template->setValue('other_details_email', $other_details_email);
        $my_template->setValue('other_details_phone_number', $other_details_phone_number);
        $my_template->setValue('other_user_name', $other_user_name);
        $my_template->setValue('other_user_designation', $other_user_designation);
        $my_template->setValue('other_user_email', $other_user_email);
        $my_template->setValue('other_user_phone_number', $other_user_phone_number);
        $my_template->setValue('main_user_name', Auth::user()->fullName());
        $my_template->setValue('main_user_designation', 'Operations Manager');
        $my_template->setValue('main_user_email', Auth::user()->email);
        $my_template->setValue('main_user_phone_number', Auth::user()->phone_number);

        try {
            $my_template->saveAs(public_path('invoiceUploads/finalInvoices/Letter3-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }
        $public_path = public_path('invoiceUploads/finalInvoices/Letter3-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx');
        return $public_path;


    }

    public function printInvoice($company_id, $company_standard_id, $aj_standard_id, $aj_standard_stage_id, $letter_type, $invoice_id)
    {

        $my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('invoiceUploads/invoices/Invoice.docx'));

        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);


        $invoice = Invoice::whereId($invoice_id)->first();
        $company = Company::whereId($company_id)->first(['id', 'city_id', 'country_id', 'address']);


        $invoiceSettings = InvoiceDataEntry::first();

        $remarks_one = $invoiceSettings->invoice_remarks_one;
        $remarks_two = $invoiceSettings->invoice_remarks_two;

        $other_details_name = $invoiceSettings->other_details_name;
        $other_details_address = $invoiceSettings->other_details_address;
        $other_details_email = $invoiceSettings->other_details_email;
        $other_details_phone_number = $invoiceSettings->other_details_phone_number;

        $inflationAmount = (int)$invoice->invoice_amount * ((int)$invoice->inflation_percentage / 100);
        $invoice_amount = (int)$invoice->invoice_amount + $inflationAmount;
        $taxAmount = (int)$invoice_amount * ((int)$invoice->tax_percentage / 100);

        $totalAmount = $taxAmount + (int)$invoice->invoice_amount + $inflationAmount;

        if (isset($invoice->standard) && !is_null($invoice->standard) && $invoice->standard === 'ISO 27001:2013') {
            $standardName = 'ISO 27001:2022';
        } else {
            $standardName = $invoice->standard;
        }
        $my_template->setValue('company_name', $invoice->company_name);
        $my_template->setValue('invoice_number', $invoice->invoice_number);
        $my_template->setValue('invoice_date', date('d/m/Y', strtotime($invoice->invoice_date)));
        $my_template->setValue('address', $company->address);
        $my_template->setValue('city', $company->city ? $company->city->name : '');
        $my_template->setValue('country', $company->country ? $company->country->name : '');
        $my_template->setValue('audit_type', $invoice->audit_type);
        $my_template->setValue('standard', $standardName);
        $my_template->setValue('invoice_amount', $invoice->invoice_unit . ' ' . $invoice_amount);
        $my_template->setValue('tax_amount', $invoice->invoice_unit . ' ' . $taxAmount);
        $my_template->setValue('total_amount', $invoice->invoice_unit . ' ' . $totalAmount);
        $my_template->setValue('tax_authority', $invoice->tax_authority);
        $my_template->setValue('tax_percentage', $invoice->tax_percentage);
        $my_template->setValue('remarks_one', $remarks_one);
        $my_template->setValue('remarks_two', $remarks_two);
        $my_template->setValue('other_details_name', $other_details_name);
        $my_template->setValue('other_details_address', $other_details_address);
        $my_template->setValue('other_details_email', $other_details_email);
        $my_template->setValue('other_details_phone_number', $other_details_phone_number);

        try {
            $my_template->saveAs(public_path('invoiceUploads/finalInvoices/Invoice-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx'));
        } catch (Exception $e) {
            //handle exception
        }


        $public_path = public_path('invoiceUploads/finalInvoices/Invoice-' . $invoice->id . '-' . $company_id . '-' . $company_standard_id . '-' . $aj_standard_stage_id . '.docx');
        return $public_path;


    }

    public function sendInvoiceEmail(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());
        $invoice = Invoice::whereId($request->invoice_id)->first();

        if (isset($invoice->standard) && !is_null($invoice->standard) && $invoice->standard === 'ISO 27001:2013') {
            $standardName = 'ISO 27001:2022';
        } else {
            $standardName = $invoice->standard;
        }

        if ($request->email_type === 'resend') {
            $subject = 'Re-Send: ' . $invoice->audit_type . ' Audit Date Confirmation for ' . $standardName . ' Certification of ' . $invoice->company_name;
        } else {
            $subject = $invoice->audit_type . ' Audit Date Confirmation for ' . $standardName . ' Certification of ' . $invoice->company_name;
        }

        $invoice->email = $request->email;
        if (!is_null($invoice->last_email_date)) {
            $invoice->last_email_date = date('Y-m-d');
            $invoice->second_last_email_date = date('Y-m-d', strtotime($invoice->last_email_date));
        } else {
            $invoice->last_email_date = date('Y-m-d');
            $invoice->second_last_email_date = date('Y-m-d');
        }
        $invoice->save();
        $company = Company::whereId($invoice->company_id)->with('primaryContact')->first(['id', 'name']);
        $companyStandard = CompanyStandards::whereId($invoice->company_standard_id)->first();
        $aj = AJStandardStage::whereId($invoice->aj_standard_stage_id)->first();

        if ($request->letter_type === 'letter1') {
            $auditConfirmationLetter = $this->printLetter1($invoice->company_id, $invoice->company_standard_id, $invoice->aj_standard_id, $invoice->aj_standard_stage_id, $request->letter_type, $invoice->id);
        } elseif ($request->letter_type === 'letter2') {
            $auditConfirmationLetter = $this->printLetter2($invoice->company_id, $invoice->company_standard_id, $invoice->aj_standard_id, $invoice->aj_standard_stage_id, $request->letter_type, $invoice->id);
        } elseif ($request->letter_type === 'letter3') {
            $auditConfirmationLetter = $this->printLetter3($invoice->company_id, $invoice->company_standard_id, $invoice->aj_standard_id, $invoice->aj_standard_stage_id, $request->letter_type, $invoice->id);
        }
        $auditInvoice = $this->printInvoice($invoice->company_id, $invoice->company_standard_id, $invoice->aj_standard_id, $invoice->aj_standard_stage_id, $request->letter_type, $invoice->id);

        $settings = InvoiceDataEntry::first(['other_user_email']);
        Mail::to($invoice->email)
            ->bcc([Auth::user()->email, $settings->other_user_email])
//            ->cc([Auth::user()->email, $settings->other_user_email])
            ->send(new InvoiceMail($subject, $invoice, $company, $companyStandard, $aj, $auditConfirmationLetter, $auditInvoice));


        if ($request->letter_type === 'letter1') {
            $letterType = 'Letter 1';
            InvoiceNotification::create([
                'user_id' => Auth::id(),
                'invoice_id' => $invoice->id,
                'letter1_email' => 1,
                'message' => $letterType . ': ' . ucfirst($request->email_type)
            ]);
        } elseif ($request->letter_type === 'letter2') {
            $letterType = 'Letter 2';
            InvoiceNotification::create([
                'user_id' => Auth::id(),
                'invoice_id' => $invoice->id,
                'letter2_email' => 1,
                'message' => $letterType . ': ' . ucfirst($request->email_type)
            ]);
        } elseif ($request->letter_type === 'letter3') {
            $letterType = 'Letter 3';
            InvoiceNotification::create([
                'user_id' => Auth::id(),
                'invoice_id' => $invoice->id,
                'letter3_email' => 1,
                'message' => $letterType . ': ' . ucfirst($request->email_type)
            ]);
        }


        $response['status'] = 'success';
        $response['message'] = 'Email ' . ucfirst($request->email_type) . ' successfully.';

        return $response;
    }


}
