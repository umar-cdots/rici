<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2019-03-11 18:35:36
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-03-11 20:00:58
 */
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Contracts\Container\Container;
use App\Menu\Menu;

class MenuComposer
{
	public function __construct()
	{
		app()->singleton(Menu::class, function (Container $app) {
            return new Menu(
                $app['config']['menu.filters'],
                // $app['events'],
                $app
            );
        });
	}
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {//dd(resolve(Menu::class)->menu());
        $view->with('menu', resolve(Menu::class));
    }
}