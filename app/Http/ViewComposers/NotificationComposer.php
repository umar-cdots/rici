<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2019-03-11 18:35:36
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-03-11 20:00:58
 */

namespace App\Http\ViewComposers;

use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class NotificationComposer
{
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {

        $receivedNotificationsDraft = null;
        $receivedNotificationsDraftCount = 0;
        $sentPostAuditNotifications = null;
        $sentPostAuditNotificationsCount = 0;
        if (auth()->check()) {
            if (auth()->user()->user_type == 'scheme_manager' || auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator' || auth()->user()->user_type == 'scheme_coordinator' ) {
                $notifications = Notification::where('sent_to', auth()->user()->id)->where('is_read', false)->where('is_final_read', false)->orderBy('id', 'desc')->limit(20)->get();
                $notificationsCount = Notification::where('sent_to', auth()->user()->id)->where('is_read', false)->where('is_final_read', false)->count();
                if (auth()->user()->user_type == 'scheme_coordinator' || auth()->user()->user_type == 'scheme_manager') {
                    $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->limit(20)->get();
                    $receivedNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->count();

                    $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->limit(20)->get();
                    $receivedPostAuditNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->count();

                    $receivedNotificationsDraft = Notification::where('sent_to', 46)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->limit(20)->get();
                    $receivedNotificationsDraftCount = Notification::where('sent_to', 46)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->count();

                    $sentPostAuditNotifications = Notification::where('sent_to', 46)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->limit(20)->get();
//                    $sentPostAuditNotificationsCount = Notification::where('sent_to', 46)->orderBy('id', 'desc')->where('is_final_read',false)->where('type', 'post_audit')->count();

                } elseif (auth()->user()->user_type == 'operation_manager' || auth()->user()->user_type == 'operation_coordinator') {

                    $receivedNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->limit(20)->get();
                    $receivedNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', '!=', 'post_audit')->count();

                    $receivedPostAuditNotifications = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->limit(20)->get();
                    $receivedPostAuditNotificationsCount = Notification::where('sent_to', auth()->user()->id)->orderBy('id', 'desc')->where('is_read', false)->where('is_final_read', false)->where('type', 'post_audit')->count();

                }


            } else if (auth()->user()->user_type == 'admin') {
                $notifications = Notification::where('is_read', false)->where('is_final_read', false)->orderBy('id', 'desc')->limit(20)->get();
                $notificationsCount = Notification::where('is_read', false)->where('is_final_read', false)->count();
                $receivedNotifications = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_read', false)->where('is_final_read', false)->limit(20)->get();
                $receivedNotificationsCount = Notification::orderBy('id', 'desc')->where('type', '!=', 'post_audit')->where('is_read', false)->where('is_final_read', false)->count();

                $receivedPostAuditNotifications = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_read', false)->where('is_final_read', false)->limit(20)->get();
                $receivedPostAuditNotificationsCount = Notification::orderBy('id', 'desc')->where('type', 'post_audit')->where('is_read', false)->where('is_final_read', false)->count();
            }elseif( auth()->user()->user_type == 'management'){
                $notifications = Notification::where('is_read', false)->where('type', '=', 'draft_for_printing')->where('is_final_read', false)->orderBy('id', 'desc')->limit(20)->get();
                $notificationsCount = Notification::where('is_read', false)->where('type', '=', 'draft_for_printing')->where('is_final_read', false)->count();
                $receivedNotifications = null;
                $receivedNotificationsCount = 0;
                $receivedPostAuditNotifications = null;
                $receivedPostAuditNotificationsCount = 0;
            } else {
                $notifications = null;
                $notificationsCount = 0;
                $receivedNotifications = null;
                $receivedNotificationsCount = 0;
                $receivedPostAuditNotifications = null;
                $receivedPostAuditNotificationsCount = 0;
            }
            $view->with(['notifications' => $notifications, 'notificationsCount' => $notificationsCount, 'receivedNotifications' => $receivedNotifications, 'receivedNotificationsCount' => $receivedNotificationsCount, 'receivedPostAuditNotifications' => $receivedPostAuditNotifications, 'receivedPostAuditNotificationsCount' => $receivedPostAuditNotificationsCount, 'receivedNotificationsDraft' => $receivedNotificationsDraft, 'receivedNotificationsDraftCount' => $receivedNotificationsDraftCount, 'sentPostAuditNotifications' => $sentPostAuditNotifications, 'sentPostAuditNotificationsCount' => $sentPostAuditNotificationsCount]);
        } else {
            $notifications = null;
            $notificationsCount = 0;
            $receivedNotifications = null;
            $receivedNotificationsCount = 0;
            $receivedPostAuditNotifications = null;
            $receivedPostAuditNotificationsCount = 0;
            $view->with(['notifications' => $notifications, 'notificationsCount' => $notificationsCount, 'receivedNotifications' => $receivedNotifications, 'receivedNotificationsCount' => $receivedNotificationsCount, 'receivedPostAuditNotifications' => $receivedPostAuditNotifications, 'receivedPostAuditNotificationsCount' => $receivedPostAuditNotificationsCount, 'receivedNotificationsDraft' => $receivedNotificationsDraft, 'receivedNotificationsDraftCount' => $receivedNotificationsDraftCount, 'sentPostAuditNotifications' => $sentPostAuditNotifications, 'sentPostAuditNotificationsCount' => $sentPostAuditNotificationsCount]);
        }


    }
}
