<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoices";
    protected $guarded = [];

    public function company(){
        return $this->belongsTo(Company::class,'company_id','id')->select('id','city_id')->with('city');
    }
}
