<?php

namespace App\Exports;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandards;
use App\IASCertificateError;
use App\IASCertificateStats;
use App\SchemeInfoPostAudit;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IASCertificateRetrieveExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */


    public function __construct()
    {
    }

    public function collection()
    {
        $companies = $this->curlRequest();
        $dataArray = [];
        $indexValue = 0;
        foreach ($companies as $key => $company) {

            $dataArray[$indexValue] = array(
                [
                    "company_name" => $company->company_name,
                    "certificate_number" => $company->certification_number,
                    "certification_status" => ucfirst($company->status),
                    "certificate_accreditation_status" => ucfirst($company->accreditation_status),
                    "certification_type" => $company->type,
                    "certification_scope" => $company->scope_description,
                    "certification_original_issue_date" => date('d-m-Y',strtotime($company->original_issue_date)),
                    "certification_issue_date" => date('d-m-Y',strtotime($company->issue_date)),
                    "certification_expiry_date" => date('d-m-Y',strtotime($company->expiry_date)),
                    "certified_entity_name" => $company->company_name,
                    "certified_entity_unique_id" =>$company->certification_number,
                ]);
            $indexValue++;

        }

        return collect($dataArray);
    }

    public function headings(): array
    {
        return [
            [
                'Certificate Company',
                'Certificate number',
                'Certification status',
                'Certificate accreditation status',
                'Certification type',
                'Certification scope',
                'Certification original issue date',
                'Certification issue date',
                'Certification expiry date',
                'Certified entity name',
                'Certified entity unique id',
            ]
        ];
    }

    public function title(): string
    {
        return "IAF Retrieve Certificate Report";
    }

    public function curlRequest()
    {

        $headers = [
            'x-http-authorization: ' . config('app.IAF')['KEY'],
            'Content-Type: application/json'
        ];
        $curl = curl_init();

        $postData = [
            "limit" => '2000',
        ];

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.iafcertsearch.org/api/client/v1/cb/cert',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_HTTPHEADER => $headers,
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($response);
        return $data->data;


    }

}
