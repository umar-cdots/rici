<?php

namespace App\Exports;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandards;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IASCertificateStandardSchemaExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */


    public function __construct()
    {
    }

    public function collection()
    {
        $dataArray = [];
        $indexValue = 0;
        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact', 'childCompanies'])
            ->whereHas('companyStandards.certificates', function ($q) {
                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
            })->get();
        foreach ($companies as $key => $company) {

            $companies[$key]->companyStandards = CompanyStandards::
            where('company_id', $company->id)
                ->whereIn('type', ['base', 'single'])
                ->whereHas('certificates', function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                })->with(['companyStandardCodes', 'companyStandardFoodCodes', 'companyStandardEnergyCodes', 'standard.standardFamily', 'certificates' => function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                }])->get();

            $auditTypes = ['surveillance_1'];
            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                    $q->where('standard_id', $companyStd->standard_id)
                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                        });
                })->whereIn('audit_type', $auditTypes)->get();
            }

            if (!empty($company->companyStandards) && count($company->companyStandards) > 0) {
                foreach ($company->companyStandards as $companyStandard) {
                    $isIAS = false;
                    if ($companyStandard->is_ims == true) {
                        if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {
                            $job_number = $companyStandard->ajStandardStage[0]->job_number;
                            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                                foreach ($imsCompanyStandards as $imsCompanyStandard) {

                                    if ($imsCompanyStandard->standard->standardFamily->name == 'Food') {
                                        if (!empty($imsCompanyStandard->companyStandardFoodCodes) && count($imsCompanyStandard->companyStandardFoodCodes) > 0) {
                                            foreach ($imsCompanyStandard->companyStandardFoodCodes as $code) {
                                                if (str_contains($code, 'IAS')) {
                                                    $isIAS = true;
                                                    if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                        $standardName = 'ISO/IEC 27001:2013';
                                                    } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                        $standardName = 'ISO/IEC 27001:2022';
                                                    } else {
                                                        $standardName = $imsCompanyStandard->standard->name;
                                                    }
                                                    $schemaName = $this->standardSchemaName($standardName);
                                                    if ($isIAS == true) {
                                                        $status = '';
                                                        if ($imsCompanyStandard->certificates->certificate_status === 'certified') {
                                                            $status = 'active';
                                                        } elseif ($imsCompanyStandard->certificates->certificate_status === 'suspended') {
                                                            $status = 'suspended';
                                                        } elseif ($imsCompanyStandard->certificates->certificate_status === 'withdrawn') {
                                                            $status = 'withdrawn';
                                                        }

                                                        $dataArray[$indexValue] = array(
                                                            [
                                                                "company_name" => $company->name,
                                                                "certificate_identity_number" => $job_number,
                                                                "scheme_name" => $schemaName,
                                                                "standards_name" => $standardName,
                                                                "status" => $status,
                                                            ]);
                                                        $indexValue++;
                                                    }
                                                }
                                                break;
                                            }
                                        }

                                    } elseif ($imsCompanyStandard->standard->standardFamily->name == 'Energy Management') {
                                        if (!empty($imsCompanyStandard->companyStandardEnergyCodes) && count($imsCompanyStandard->companyStandardEnergyCodes) > 0) {
                                            foreach ($imsCompanyStandard->companyStandardEnergyCodes as $code) {
                                                if (str_contains($code, 'IAS')) {
                                                    $isIAS = true;
                                                    if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                        $standardName = 'ISO/IEC 27001:2013';
                                                    } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                        $standardName = 'ISO/IEC 27001:2022';
                                                    } else {
                                                        $standardName = $imsCompanyStandard->standard->name;
                                                    }
                                                    $schemaName = $this->standardSchemaName($standardName);
                                                    if ($isIAS == true) {
                                                        $status = '';
                                                        if ($imsCompanyStandard->certificates->certificate_status === 'certified') {
                                                            $status = 'active';
                                                        } elseif ($imsCompanyStandard->certificates->certificate_status === 'suspended') {
                                                            $status = 'suspended';
                                                        } elseif ($imsCompanyStandard->certificates->certificate_status === 'withdrawn') {
                                                            $status = 'withdrawn';
                                                        }
                                                        $dataArray[$indexValue] = array(
                                                            [
                                                                "company_name" => $company->name,
                                                                "certificate_identity_number" => $job_number,
                                                                "scheme_name" => $schemaName,
                                                                "standards_name" => $standardName,
                                                                "status" => $status,
                                                            ]);
                                                        $indexValue++;
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    } else {
                                        if (!empty($imsCompanyStandard->companyStandardCodes) && count($imsCompanyStandard->companyStandardCodes) > 0) {
                                            foreach ($imsCompanyStandard->companyStandardCodes as $code) {
                                                if (str_contains($code, 'IAS')) {
                                                    $isIAS = true;
                                                    if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                        $standardName = 'ISO/IEC 27001:2013';
                                                    } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                        $standardName = 'ISO/IEC 27001:2022';
                                                    } else {
                                                        $standardName = $imsCompanyStandard->standard->name;
                                                    }
                                                    $schemaName = $this->standardSchemaName($standardName);
                                                   if ($isIAS == true) {
                                                        $status = '';
                                                        if ($imsCompanyStandard->certificates->certificate_status === 'certified') {
                                                            $status = 'active';
                                                        } elseif ($imsCompanyStandard->certificates->certificate_status === 'suspended') {
                                                            $status = 'suspended';
                                                        } elseif ($imsCompanyStandard->certificates->certificate_status === 'withdrawn') {
                                                            $status = 'withdrawn';
                                                        }
                                                        $dataArray[$indexValue] = array(
                                                            [
                                                                "company_name" => $company->name,
                                                                "certificate_identity_number" => $job_number,
                                                                "scheme_name" => $schemaName,
                                                                "standards_name" => $standardName,
                                                                "status" => $status,
                                                            ]);
                                                        $indexValue++;
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }


                                }

                            }


                        }
                    } else {
                        if ($companyStandard->standard->standardFamily->name == 'Food') {
                            if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
                                foreach ($companyStandard->companyStandardFoodCodes as $code) {
                                    if (str_contains($code, 'IAS')) {
                                        $isIAS = true;
                                    }
                                    break;
                                }
                            }

                        } elseif ($companyStandard->standard->standardFamily->name == 'Energy Management') {
                            if (!empty($companyStandard->companyStandardEnergyCodes) && count($companyStandard->companyStandardEnergyCodes) > 0) {
                                foreach ($companyStandard->companyStandardEnergyCodes as $code) {
                                    if (str_contains($code, 'IAS')) {
                                        $isIAS = true;
                                    }
                                    break;
                                }
                            }
                        } else {
                            if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                                foreach ($companyStandard->companyStandardCodes as $code) {
                                    if (str_contains($code, 'IAS')) {
                                        $isIAS = true;
                                    }
                                    break;
                                }
                            }
                        }


                        if ($isIAS === true) {
                            if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {
                                $job_number = $companyStandard->ajStandardStage[0]->job_number;
                                $status = '';
                                if ($companyStandard->certificates->certificate_status === 'certified') {
                                    $status = 'active';
                                } elseif ($companyStandard->certificates->certificate_status === 'suspended') {
                                    $status = 'suspended';
                                } elseif ($companyStandard->certificates->certificate_status === 'withdrawn') {
                                    $status = 'withdrawn';
                                }
                                if ($companyStandard->standard->name === 'ISO 27001:2013') {
                                    $standardName = 'ISO/IEC 27001:2013';
                                } else if ($companyStandard->standard->name === 'ISO 27001:2022') {
                                    $standardName = 'ISO/IEC 27001:2022';
                                } else {
                                    $standardName = $companyStandard->standard->name;
                                }
                                $dataArray[$indexValue] = array(
                                    [
                                        "company_name" => $company->name,
                                        "certificate_identity_number" => $job_number,
                                        "scheme_name" => $this->standardSchemaName($standardName),
                                        "standards_name" => $standardName,
                                        "status" => $status,
                                    ]);
                                $indexValue++;
                            }

                        }
                    }


                }

            }


        }
        return collect($dataArray);
    }

    public function headings(): array
    {
        return [
            [
                'Certificate Company Name',
                'Certificate Identity number',
                'Schema Name',
                'Standard Name',
                'Certification status',
            ]
        ];
    }

    public function title(): string
    {
        return "Standard Schema Export";
    }

    public function standardSchemaName($standard)
    {

        $name = '';
        if ($standard == 'ISO 9001:2015') {
            $name = 'Quality Management Systems';
        } elseif ($standard == 'ISO 27001:2013' || $standard == 'ISO 27001:2022' || $standard == 'ISO/IEC 27001:2013' || $standard == 'ISO/IEC 27001:2022') {
            $name = 'Information Security Management Systems';
        } elseif ($standard == 'ISO 14001:2015') {
            $name = 'Environmental Management Systems';
        } elseif ($standard == 'ISO 45001:2018') {
            $name = 'Occupational Health and Safety Management Systems';
        } elseif ($standard == 'ISO 22000:2018') {
            $name = 'Food Safety Management Systems';
        } elseif ($standard == 'ISO 50001:2018') {
            $name = 'Energy Management Systems';
        } elseif ($standard == 'ISO 29993:2017') {
            $name = 'Learning Services outside formal Education Service Requirements';
        } elseif ($standard == 'ISO 22301:2019') {
            $name = 'Business Continuity Management Systems';
        }

        return $name;

    }

}
