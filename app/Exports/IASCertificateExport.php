<?php

namespace App\Exports;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandards;
use App\SchemeInfoPostAudit;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IASCertificateExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */


    public function __construct()
    {
    }

    public function collection()
    {
        $dataArray = [];
        $indexValue = 0;
        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact', 'childCompanies'])
            ->whereHas('companyStandards.certificates', function ($q) {
                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
            })->get();
        foreach ($companies as $key => $company) {
            $companies[$key]->companyStandards = CompanyStandards::
            where('company_id', $company->id)
                ->whereIn('type', ['base', 'single'])
                ->whereHas('certificates', function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                })->with(['companyStandardCodes', 'companyStandardFoodCodes', 'companyStandardEnergyCodes', 'standard.standardFamily', 'certificates' => function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                }])->get();

            $auditTypes = ['surveillance_1'];
            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                    $q->where('standard_id', $companyStd->standard_id)
                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                        });
                })->whereIn('audit_type', $auditTypes)->get();
            }

            if (!empty($company->companyStandards) && count($company->companyStandards) > 0) {
                foreach ($company->companyStandards as $companyStandard) {
                    $isIAS = false;
                    if ($companyStandard->is_ims == true) {
                        if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {
                            $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                            if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {

                                foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                    if ($imsCompanyStandard->standard->standardFamily->name == 'Food') {
                                        if (!empty($imsCompanyStandard->companyStandardFoodCodes) && count($imsCompanyStandard->companyStandardFoodCodes) > 0) {

                                            foreach ($imsCompanyStandard->companyStandardFoodCodes as $code) {
                                                if (str_contains($code, 'IAS')) {
                                                    $isIAS = true;
                                                }
                                                break;
                                            }
                                        }

                                    } elseif ($imsCompanyStandard->standard->standardFamily->name == 'Energy Management') {
                                        if (!empty($imsCompanyStandard->companyStandardEnergyCodes) && count($imsCompanyStandard->companyStandardEnergyCodes) > 0) {
                                            foreach ($imsCompanyStandard->companyStandardEnergyCodes as $code) {
                                                if (str_contains($code, 'IAS')) {
                                                    $isIAS = true;
                                                }
                                                break;
                                            }
                                        }
                                    } else {
                                        if (!empty($imsCompanyStandard->companyStandardCodes) && count($imsCompanyStandard->companyStandardCodes) > 0) {
                                            foreach ($imsCompanyStandard->companyStandardCodes as $code) {
                                                if (str_contains($code, 'IAS')) {
                                                    $isIAS = true;
                                                }
                                                break;
                                            }
                                        }
                                    }


                                }

                            }


                        }
                    } else {
                        if ($companyStandard->standard->standardFamily->name == 'Food') {
                            if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
                                foreach ($companyStandard->companyStandardFoodCodes as $code) {
                                    if (str_contains($code, 'IAS')) {
                                        $isIAS = true;
                                    }
                                    break;
                                }
                            }

                        } elseif ($companyStandard->standard->standardFamily->name == 'Energy Management') {
                            if (!empty($companyStandard->companyStandardEnergyCodes) && count($companyStandard->companyStandardEnergyCodes) > 0) {
                                foreach ($companyStandard->companyStandardEnergyCodes as $code) {
                                    if (str_contains($code, 'IAS')) {
                                        $isIAS = true;
                                    }
                                    break;
                                }
                            }
                        } else {
                            if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                                foreach ($companyStandard->companyStandardCodes as $code) {
                                    if (str_contains($code, 'IAS')) {
                                        $isIAS = true;
                                    }
                                    break;

                                }
                            }
                        }
                    }

                    if ($isIAS === true) {


                        if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {

                            $status = '';
                            if ($companyStandard->certificates->certificate_status === 'certified') {
                                $status = 'Active';
                            } elseif ($companyStandard->certificates->certificate_status === 'suspended') {
                                $status = 'Suspended';
                            } elseif ($companyStandard->certificates->certificate_status === 'withdrawn') {
                                $status = 'Withdrawn';
                            }
                            $latest_dates = SchemeInfoPostAudit::whereHas('postAudit', function ($q) use ($companyStandard) {
                                $q->where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard_id);
                            })->latest()->first();
                            if (!is_null($latest_dates)) {
                                $original_issue_date = date('Ymd', strtotime(($latest_dates->original_issue_date)));
                            } else {
                                $original_issue_date = '';
                            }
                            if (!is_null($companyStandard->last_certificate_issue_date)) {
                                $issue_date = date("Ymd", strtotime($companyStandard->last_certificate_issue_date));
                            } else {
                                $issue_date = '';
                            }

                            if (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) {
                                $expiry_date = date('Ymd', strtotime(($latest_dates->new_expiry_date)));
                            } else {
                                $expiry_date = '';
                            }
                            $dataArray[$indexValue] = array(
                                [
                                    "company_name" => $company->name,
                                    "certificate_number" => $companyStandard->ajStandardStage[0]->job_number,
                                    "certification_status" => $status,
                                    "certificate_accreditation_status" => "Accredited",
                                    "certification_type" => "Management System",
                                    "certification_scope" => $company->scope_to_certify,
                                    "certification_original_issue_date" => $original_issue_date,
                                    "certification_issue_date" => $issue_date,
                                    "certification_expiry_date" => $expiry_date,
                                    "certified_entity_name" => $company->name,
                                    "certified_entity_english_name" => "..",
                                    "certified_entity_trading_name" => "..",
                                    "certified_entity_unique_id" => $companyStandard->ajStandardStage[0]->job_number,
                                    "certified_entity_street_address" => $company->address,
                                    "certified_entity_street_city" => $company->city->name,
                                    "certified_entity_state" => "..",
                                    "certified_entity_post_code" => (int)$company->city->post_code,
                                    "certified_entity_country" => $company->country->name,
                                    "certified_entity_website" => "..",
                                    "accreditation_body_name" => "International Accreditation Services",
                                    "accreditation_body_acronym_name" => "IAS",
                                    'primary_contact_name' => ucfirst($company->primaryContact->name),
                                    'primary_contact_email' => ucfirst($company->primaryContact->email),
                                    'primary_contact_phone_number' => ucfirst($company->primaryContact->contact),
                                ]);
                            $indexValue++;

                        }
                    }

                }


            }


        }

        return collect($dataArray);
    }

    public function headings(): array
    {
        return [
            [
                'Certificate Company',
                'Certificate number',
                'Certification status',
                'Certificate accreditation status',
                'Certification type',
                'Certification scope',
                'Certification original issue date',
                'Certification issue date',
                'Certification expiry date',
                'Certified entity name',
                'Certified entity english name',
                'Certified entity trading name',
                'Certified entity unique id',
                'Certified entity street address',
                'Certified entity street city',
                'Certified entity state',
                'Certified entity post code',
                'Certified entity country',
                'Certified entity website',
                'Accreditation body name',
                'Accreditation body acronym name',
                'Primary Contact Name',
                'Primary Contact Email',
                'Primary Contact Phone number',
            ]
        ];
    }

    public function title(): string
    {
        return "IAS Certificate Report";
    }

}
