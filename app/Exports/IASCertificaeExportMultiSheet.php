<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class IASCertificaeExportMultiSheet implements WithMultipleSheets
{
    use Exportable;

    public function __construct()
    {
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets['Certifications'] = new IASCertificateExport();
        $sheets['Certifications Standard & Schema'] = new IASCertificateStandardSchemaExport();
        $sheets['Certifications Additional Sites'] = new IASCertificateAdditionalSitesExport();

        return $sheets;
    }


}
