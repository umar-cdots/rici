<?php

namespace App\Exports;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandards;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IASCertificateAdditionalSitesExport implements FromCollection, WithHeadings, WithTitle, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */


    public function __construct()
    {
    }

    public function collection()
    {
        $dataArray = [];
        $indexValue = 0;
        $companies = Company::whereRaw('name <> ""')->with(['region', 'country', 'city', 'primaryContact', 'childCompanies'])
            ->whereHas('companyStandards.certificates', function ($q) {
                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
            })->get();
        foreach ($companies as $key => $company) {
            $job_number = '';
            $findIAS = false;
            $companies[$key]->companyStandards = CompanyStandards::
            where('company_id', $company->id)
                ->whereIn('type', ['base', 'single'])
                ->whereHas('certificates', function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                })->with(['companyStandardCodes', 'companyStandardFoodCodes', 'companyStandardEnergyCodes', 'standard.standardFamily', 'certificates' => function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                }])->get();

            $auditTypes = ['surveillance_1'];
            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                    $q->where('standard_id', $companyStd->standard_id)
                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                        });
                })->whereIn('audit_type', $auditTypes)->get();
            }

            if (!empty($company->companyStandards) && count($company->companyStandards) > 0) {
                foreach ($company->companyStandards as $companyStandard) {
                    $isIAS = false;
                    if ($companyStandard->standard->standardFamily->name == 'Food') {
                        if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
                            foreach ($companyStandard->companyStandardFoodCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                            }
                        }

                    } elseif ($companyStandard->standard->standardFamily->name == 'Energy Management') {
                        if (!empty($companyStandard->companyStandardEnergyCodes) && count($companyStandard->companyStandardEnergyCodes) > 0) {
                            foreach ($companyStandard->companyStandardEnergyCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                            }
                        }
                    } else {
                        if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                            foreach ($companyStandard->companyStandardCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                            }
                        }
                    }
                    if ($isIAS === true) {
                        if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {
                            $job_number = $companyStandard->ajStandardStage[0]->job_number;
                            $findIAS = true;

                        }
                    }

                }

            }
            if ($findIAS === true) {
                if (!empty($company->childCompanies) && count($company->childCompanies) > 0) {
                    foreach ($company->childCompanies as $keyData => $child_company) {
                        $dataArray[$indexValue] = array(
                            [
                                "company_name" => $company->name,
                                "certificate_identity_number" => $job_number,
                                "street" => $child_company->address,
                                "city" => $child_company->city->name,
                                "state" => "..",
                                "country" => $child_company->country ? $child_company->country->name : $company->country->name,
                                "postcode" => (int)$child_company->city->post_code,
                                "scope_description" => $company->scope_to_certify
                            ]);
                        $indexValue++;
                    }
                }
            }


        }
        return collect($dataArray);
    }

    public function headings(): array
    {
        return [
            [
                'Certificate Company Name',
                'Certificate Identity number',
                'Certified entity street address',
                'Certified entity street city',
                'Certified entity state',
                'Certified entity country',
                'Certified entity post code',
                'Certified entity scope description',
            ]
        ];
    }

    public function title(): string
    {
        return "Additional Sites";
    }

}
