<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class AuditorStandardTraining extends Model implements HasMedia
{
    use EnumValuesTrait;
    use SoftDeletes;
    use HasMediaTrait;
}
