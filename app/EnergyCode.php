<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnergyCode extends Model
{
   use SoftDeletes;
   protected $guarded = [];
   
   public function energySheet(){
       return $this->hasMany(EnergyFamilySheet::class, 'energy_code_id', 'id');
   }
    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
