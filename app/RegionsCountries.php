<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class RegionsCountries extends Model
{
    use SoftDeletes;
    use LogsActivity; 

    protected $table        = "region_countries";
    protected $primaryKey   = "region_id";

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    public function region()
    {
    	return $this->belongsTo(Regions::class);
    }

    public function country()
    {
        return $this->belongsTo(Countries::class);
    }
}
