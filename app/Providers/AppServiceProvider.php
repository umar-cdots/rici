<?php

namespace App\Providers;

use App\LetterName;
use App\Models\MetaTag;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $letter = LetterName::first();

        $riciName = $letter->rici_name ?? '';
        $riciAddress = $letter->rici_address ?? '';
        $doc_one = $letter->doc_name_one ?? '';
        $doc_two = $letter->doc_name_two ?? '';
        $isComments = $letter->comments ?? 0;
        $pk_days = $letter->pk_days ?? 0;
        $ksa_days = $letter->ksa_days ?? 0;
        $cut_of_days = $letter->cut_of_days ?? 0;

        view()->composer('*', function ($view) use ($riciAddress, $riciName, $doc_one, $doc_two, $isComments, $pk_days, $ksa_days, $cut_of_days) {
            $view->with(
                [
                    'riciName' => $riciName,
                    'riciAddress' => $riciAddress,
                    'doc_one' => $doc_one,
                    'doc_two' => $doc_two,
                    'isComments' => $isComments,
                    'pk_days' => $pk_days,
                    'ksa_days' => $ksa_days,
                    'cut_of_days' => $cut_of_days,
                ]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
