<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutSourceRequestStandard extends Model
{
    use SoftDeletes;

    protected $table = 'out_source_request_standards';
    protected $guarded = [];

    public function outsource_request()
    {
        return $this->belongsTo(OutSourceRequest::class, 'outsource_request_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function outsource_request_standard_accreditations()
    {
        return $this->hasMany(OutSourceRequestStandardAccreditation::class, 'outsource_request_standard_id', 'id');
    }

    public function outsource_request_standard_iaf_codes()
    {
        return $this->hasMany(OutSourceRequestStandardIAFCode::class, 'outsource_request_standard_id', 'id');
    }

    public function outsource_request_standard_ias_codes()
    {
        return $this->hasMany(OutSourceRequestStandardIASCode::class, 'outsource_request_standard_id', 'id');
    }

    public function outsource_request_standard_food_codes()
    {
        return $this->hasMany(OutSourceRequestStandardFoodCode::class, 'outsource_request_standard_id', 'id');
    }

    public function outsource_request_standard_energy_codes()
    {
        return $this->hasMany(OutSourceRequestStandardEnergyCode::class, 'outsource_request_standard_id', 'id');
    }


//    public function setTypeAttribute($value)
//    {
//        $standard = Standards::find($value);
//        $this->attributes['type'] = strtotime(str_slug($standard->standardFamily->name));
//    }

}
