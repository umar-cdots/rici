<?php

namespace App\Menu\Filters;

use Illuminate\Contracts\Auth\Access\Gate;
use App\Menu\Builder;
use Illuminate\Support\Facades\Auth;

class GateFilter implements FilterInterface
{
    protected $gate;

    public function __construct(Gate $gate)
    {
        $this->gate = $gate;

    }

    public function transform($item, Builder $builder)
    {
        if (!$this->isVisible($item)) {
            return false;
        }

        return $item;
    }

    protected function isVisible($item)
    {

//        if (!isset($item['can'])) {
//            return true;
//        }
//
//        if (isset($item['model'])) {
//            return $this->gate->allows($item['can'], $item['model']);
//        }
//
//        return $this->gate->allows($item['can']);


        // check if user is a member of specified role(s)
        if (isset($item['roles'])) {

            if (!(Auth::user())->hasAnyRole($item['roles'])) {
                // not a member of any valid roles; check if user has been granted explicit permission
                if (isset($item['can']) && (Auth::user())->can($item['can'])) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            // valid roles not defined; check if user has been granted explicit permission
            if (isset($item['can'])) {
                // permissions are defined
                if ((Auth::user())->can($item['can'])) {
                    return true;
                } else {
                    return false;
                }
            } else {
                // no valid roles or permissions defined; allow for all users
                return true;
            }
        }
    }
}
