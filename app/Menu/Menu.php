<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2019-03-11 18:38:50
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-03-13 18:35:04
 */

namespace App\Menu;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Container\Container;
use App\Menu\Builder;

// use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class Menu
{
    protected $menu;

    protected $filters;

    // protected $events;

    protected $container;

    public function __construct(
        array     $filters,
        // Dispatcher $events,
        Container $container
    )
    {
        $this->filters = $filters;
        // $this->events = $events;
        $this->container = $container;
    }

    public function menu()
    {
        if (!$this->menu) {
            $this->menu = $this->buildMenu();
        }
// dd($this->menu);
        return $this->menu;
    }

    protected function buildMenu()
    {
        $builder = new Builder($this->buildFilters());

        // $this->events->fire(new BuildingMenu($builder));

        $builder->add(config('menu.items'));

        return $builder->menu;
    }

    protected function buildFilters()
    {
        return array_map([$this->container, 'make'], $this->filters);
    }
}
