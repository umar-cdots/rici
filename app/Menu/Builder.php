<?php

namespace App\Menu;

class Builder
{
    public $menu = [];

    /**
     * @var array
     */
    private $filters;

    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    public function add()
    {
        $items = $this->transformItems(func_get_args());

        foreach ($items as $item) {
            array_push($this->menu, $item);
        }
    }

    public function transformItems($items)
    {
        return array_filter(array_map([$this, 'applyFilters'], $items[0]));
    }

    protected function applyFilters($item)
    {
        if (is_string($item)) {
            return $item;
        }

        foreach ($this->filters as $filter) {
            $item = $filter->transform($item, $this);

        }

        if (isset($item['header'])) {
            $item = $item['header'];
        }

        return $item;
    }
//    protected function applyFilters($item)
//    {
//        // Filters are only applied to array type menu items.
//
//        if (! is_array($item)) {
//            return $item;
//        }
//
//        // If the item is a submenu, transform all the submenu items first.
//        // These items need to be transformed first because some of the submenu
//        // filters (like the ActiveFilter) depends on these results.
//
//        if (isset($item['submenu'])) {
//            $item['submenu'] = $this->transformItems($item['submenu']);
//        }
//
//        // Now, apply all the filters on the item.
//
//        foreach ($this->filters as $filter) {
//
//            // If the item is not allowed to be shown, there is no sense to
//            // continue applying the filters.
//
//            if (!isset($item)) {
//                return $item;
//            }
//
//            $item = $filter->transform($item);
//        }
//
//        return $item;
//    }
}
