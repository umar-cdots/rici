<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutSourceRequestRemarks extends Model
{
    protected $table = 'outsource_request_remarks';
    protected $guarded = [];


    public function outsource_request()
    {
        return $this->belongsTo(OutSourceRequest::class, 'outsource_request_id', 'id');
    }
}
