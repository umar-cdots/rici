<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicalExpertStandardEnergyCode extends Model
{
    use SoftDeletes;

    public function energycode()
    {
        return $this->belongsTo(EnergyCode::class, 'energy_code')->withDefault();
    }
}
