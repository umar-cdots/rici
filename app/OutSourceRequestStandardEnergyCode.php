<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutSourceRequestStandardEnergyCode extends Model
{
    use SoftDeletes;

    protected $table = 'out_source_request_standard_energy_codes';
    protected $guarded = [];

    public function outsource_request_standard()
    {
        return $this->belongsTo(OutSourceRequestStandard::class, 'outsource_request_standard_id', 'id');
    }


    public function energy_category()
    {
        return $this->belongsTo(EnergyCode::class, 'energy_code_id', 'id');
    }
}
