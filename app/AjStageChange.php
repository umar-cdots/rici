<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AjStageChange extends Model
{
    use SoftDeletes;
    protected $table = "aj_stage_change";
    protected $guarded = [];

    public function ajStandardStage(){
        return $this->belongsTo(AJStandardStage::class, 'aj_standard_stage_id', 'id');
    }
}
