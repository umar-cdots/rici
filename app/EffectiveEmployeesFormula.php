<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EffectiveEmployeesFormula extends Model
{
    protected $table = "effective_employees_formula";
    protected $guarded = [];
}
