<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CompanyStandardFoodCode extends Model
{

    public function foodcategory()
    {
        return $this->belongsTo(FoodCategory::class, 'food_category_id')->withDefault();
    }

    public function foodsubcategory()
    {
        return $this->belongsTo(FoodSubCategory::class, 'food_sub_category_id')->withDefault();
    }

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function companyStandard()
    {
        return $this->belongsTo(CompanyStandards::class, 'company_standard_id' . 'id');
    }

}
