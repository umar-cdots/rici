<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Countries extends Model
{
    use SoftDeletes;
    use LogsActivity;

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    public function states()
    {
        return $this->hasMany(States::class, 'country_id');
    }



//    public function cities()
//    {
//    	return $this->hasMany(Cities::class, 'country_id');
//    }


    public function cities()
    {
        return $this->morphMany(Cities::class, 'zoneable');
    }

    public function zones()
    {
        return $this->hasMany(Zone::class, 'country_id');
    }
}
