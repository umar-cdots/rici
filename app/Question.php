<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;
    protected $table = "questions";
    protected $guarded = [];


    public function postAuditQuestions()
    {
        return $this->belongsToMany(PostAuditQueestion::class, 'post_audits', 'question_id', 'post_audit_id', 'id');
    }


}
