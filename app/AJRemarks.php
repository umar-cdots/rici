<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AJRemarks extends Model
{
    use SoftDeletes;
    protected $table = "aj_remarks";
    protected $guarded = [];

    public function ajStandardStages(){
        return $this->belongsTo(AJStandardStage::class, 'aj_standard_stage_id', 'id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

}
