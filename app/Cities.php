<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Cities extends Model
{
    use SoftDeletes;
    use LogsActivity; 

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    public function country()
    {
    	return $this->belongsTo(Countries::class, 'country_id')->withDefault();
    }

    public function region()
    {
    	return $this->belongsTo(States::class, 'state_id');
    }

    public function zoneable()
    {
        return $this->morphTo();
    }

    public function zone(){
        return $this->belongsTo(Zone::class);
    }




}
