<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodSubCategory extends Model
{
    use SoftDeletes;
    public function foodCategories()
    {
        return $this->belongsTo(FoodCategory::class, 'food_category_id')->withDefault();
    }


    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
