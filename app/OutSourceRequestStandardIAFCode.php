<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutSourceRequestStandardIAFCode extends Model
{
    protected $guarded = [];

    public function outsource_request_standard()
    {
        return $this->belongsTo(OutSourceRequestStandard::class, 'outsource_request_standard_id', 'id');
    }


    public function iaf_code()
    {
        return $this->belongsTo(IAF::class, 'iaf_id', 'id');
    }
}
