<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutSourceRequestStandardFoodCode extends Model
{
    use SoftDeletes;

    protected $table = 'out_source_request_standard_food_codes';
    protected $guarded = [];

    public function outsource_request_standard()
    {
        return $this->belongsTo(OutSourceRequestStandard::class, 'outsource_request_standard_id', 'id');
    }


    public function food_category()
    {
        return $this->belongsTo(FoodCategory::class, 'food_code_id', 'id');
    }

}
