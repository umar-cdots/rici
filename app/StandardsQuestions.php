<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class StandardsQuestions extends Model
{
    use SoftDeletes;
    use LogsActivity;

    function standard()
    {
    	return $this->hasOne(Standards::class);
    }

    function answer()
    {
    	return $this->hasOne(CompanyStandardsAnswers::class);
    }

    function parentQuestion()
    {
        return $this->hasOne(StandardsQuestions::class);
    }

    function dependentFields()
    {
        return isset($this->dependent_fields_ids) ? self::whereIn('id', explode(',', $this->dependent_fields_ids))->get() : [];
    }
}
