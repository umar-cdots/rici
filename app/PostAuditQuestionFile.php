<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostAuditQuestionFile extends Model
{
    use SoftDeletes;
    protected $table = "post_audit_question_files";
    protected $guarded = [];


    public function postAuditQuestion(){
        return $this->belongsTo(PostAuditQueestion::class,'post_audit_question_id','id');
    }
}
