<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditorStandardEnergyCode extends Model
{
    use SoftDeletes;

    public function energycode()
    {
        return $this->belongsTo(EnergyCode::class, 'energy_code')->withDefault();
    }
    public function auditorStandard(){
        return $this->belongsTo(AuditorStandard::class, 'auditor_standard_id', 'id');
    }
}
