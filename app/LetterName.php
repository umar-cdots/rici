<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterName extends Model
{
    protected $table = "letter_names";
    protected $guarded = [];
}
