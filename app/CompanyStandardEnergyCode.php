<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyStandardEnergyCode extends Model
{
    public function energycode()
    {
        return $this->belongsTo(EnergyCode::class, 'energy_id')->withDefault();
    }

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function companyStandard()
    {
        return $this->belongsTo(CompanyStandards::class, 'company_standard_id'.'id');
    }

}
