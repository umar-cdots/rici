<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class AJStandardStage extends Model
{
    use SoftDeletes;
    use EnumValuesTrait;

    protected $table = 'aj_standard_stages';
    protected $guarded = [];

    public function ajStandard()
    {
        return $this->belongsTo(AJStandard::class, 'aj_standard_id', 'id');
    }

    public function ajStageTeams()
    {
        return $this->hasMany(AJStageTeam::class, 'aj_standard_stage_id', 'id');
    }

    public function ajStageChange()
    {
        return $this->hasMany(AjStageChange::class, 'aj_standard_stage_id', 'id');
    }

    public function getAuditType()
    {
        if ($this->audit_type == 'stage_1') {
            return 'Stage 1';
        } elseif ($this->audit_type == 'stage_2') {
            return 'Stage 2';
        } elseif ($this->audit_type == 'surveillance_1') {
            return 'Surveillance 1';
        } elseif ($this->audit_type == 'surveillance_2') {
            return 'Surveillance 2';
        } elseif ($this->audit_type == 'surveillance_3') {
            return 'Surveillance 3';
        } elseif ($this->audit_type == 'surveillance_4') {
            return 'Surveillance 4';
        }elseif ($this->audit_type == 'surveillance_5') {
            return 'Surveillance 5';
        } elseif ($this->audit_type == 'stage_2_verification') {
            return 'Stage 2 + Verification';
        } elseif ($this->audit_type == 'surveillance_1_verification') {
            return 'Surveillance 1 + Verification';
        } elseif ($this->audit_type == 'surveillance_2_verification') {
            return 'Surveillance 2 + Verification';
        } elseif ($this->audit_type == 'surveillance_3_verification') {
            return 'Surveillance 3 + Verification';
        } elseif ($this->audit_type == 'surveillance_4_verification') {
            return 'Surveillance 4 + Verification';
        } elseif ($this->audit_type == 'surveillance_5_verification') {
            return 'Surveillance 5 + Verification';
        } elseif ($this->audit_type == 'reaudit_verification') {
            return 'Reaudit + Verification';
        } elseif ($this->audit_type == 'stage_2_scope_extension') {
            return 'Stage 2 + Scope-Extension';
        } elseif ($this->audit_type == 'surveillance_1_scope_extension') {
            return 'Surveillance 1 + Scope-Extension';
        } elseif ($this->audit_type == 'surveillance_2_scope_extension') {
            return 'Surveillance 2 + Scope-Extension';
        } elseif ($this->audit_type == 'surveillance_3_scope_extension') {
            return 'Surveillance 3 + Scope-Extension';
        } elseif ($this->audit_type == 'surveillance_4_scope_extension') {
            return 'Surveillance 4 + Scope-Extension';
        } elseif ($this->audit_type == 'surveillance_5_scope_extension') {
            return 'Surveillance 5 + Scope-Extension';
        } elseif ($this->audit_type == 'reaudit_scope_extension') {
            return 'Reaudit + Scope-Extension';
        } elseif ($this->audit_type == 'stage_2_special_transition') {
            return 'Stage 2 + Special-Transition';
        } elseif ($this->audit_type == 'surveillance_1_special_transition') {
            return 'Surveillance 1 + Special-Transition';
        } elseif ($this->audit_type == 'surveillance_2_special_transition') {
            return 'Surveillance 2 + Special-Transition';
        } elseif ($this->audit_type == 'surveillance_3_special_transition') {
            return 'Surveillance 3 + Special-Transition';
        } elseif ($this->audit_type == 'surveillance_4_special_transition') {
            return 'Surveillance 4 + Special-Transition';
        } elseif ($this->audit_type == 'surveillance_5_special_transition') {
            return 'Surveillance 5 + Special-Transition';
        } elseif ($this->audit_type == 'reaudit_special_transition') {
            return 'Reaudit + Special-Transition';
        } elseif ($this->audit_type == 'stage_2 + Transfer') {
            return 'Stage 2 + Transfer';
        } elseif ($this->audit_type == 'surveillance_1 + Transfer') {
            return 'Surveillance 1 + Transfer';
        } elseif ($this->audit_type == 'surveillance_2 + Transfer') {
            return 'Surveillance 2 + Transfer';
        } elseif ($this->audit_type == 'surveillance_3 + Transfer') {
            return 'Surveillance 3 + Transfer';
        } elseif ($this->audit_type == 'surveillance_4 + Transfer') {
            return 'Surveillance 4 + Transfer';
        } elseif ($this->audit_type == 'surveillance_5 + Transfer') {
            return 'Surveillance 5 + Transfer';
        } else {
            return 'Reaudit';
        }
    }


//    Relationships for aj remarks

    public function AJRemarks()
    {
        return $this->hasMany(AJRemarks::class, 'aj_standard_stage_id', 'id');
    }

    public function postAudit()
    {
        return $this->hasOne(PostAudit::class, 'aj_standard_stage_id', 'id');
    }

    public function previous()
    {
        return $this->find(--$this->id);
    }

    public function next()
    {
        return $this->find(++$this->id);
    }


}
