<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CompanyStandardsAnswers extends Model
{
    use SoftDeletes;
    use LogsActivity; 

    function standard()
    {
    	return $this->belongsTo(Standards::class);
    }

    function question()
    {
    	return $this->belongsTo(StandardsQuestions::class, 'standard_question_id');
    }

    function companyStandard()
    {
    	return $this->belongsTo(CompanyStandards::class);
    }
}
