<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IAS extends Model
{
    use SoftDeletes;
    protected $table = "i_a_s";
    protected $guarded = [];

    public function iaf()
    {
        return $this->belongsTo(IAF::class, 'iaf_id', 'id');
    }

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function companies()
    {
        return $this->hasMany(Company::class, 'ias_id');
    }

    public function outsource_request_standard_ias_codes()
    {
        return $this->hasMany(OutSourceRequestStandardIASCode::class, 'ias_id', 'id');
    }
}
