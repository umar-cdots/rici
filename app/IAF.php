<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IAF extends Model
{
    use SoftDeletes;
    protected $table = "i_a_fs";
    protected $guarded = [];

    public function ias()
    {
        return $this->hasMany(IAS::class ,'iaf_id','id');
    }

    public function companies()
    {
        return $this->hasMany(Company::class, 'iaf_id');
    }

    public function nameWithCode()
    {
        return $this->code . ' | ' . $this->name;
    }

    public function outsource_request_standard_iaf_codes()
    {
        return $this->hasMany(OutSourceRequestStandardIAFCode::class, 'iaf_id', 'id');
    }

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
