<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class AuditorStandardGrade extends Model implements HasMedia
{
    use EnumValuesTrait;
    use SoftDeletes;
    use HasMediaTrait;

    public function grade()
    {
        return $this->belongsTo(Grades::class);
    }
}
