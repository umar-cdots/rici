<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperationCoordinator extends Model
{
    use SoftDeletes;

    protected $table = 'operation_coordinators';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function standard(){
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function operation_manager(){
        return $this->belongsTo(OperationManager::class, 'operation_manager_id', 'id');
    }

    public function scheme_manager(){
        return $this->belongsTo(SchemeManager::class, 'scheme_manager_id', 'id');
    }

    public function outsource_requests(){
        return $this->hasMany(OutSourceRequest::class);

    }
}
