<?php
namespace App\Library;

use App\Accreditation;

abstract class Helper
{
	public static function getAuditActivityStageName($id)
	{
		return \App\AuditActivityStages::find($id)->name ?? null;
	}

	public static function getStandardNumberById($id)
	{
		if(is_array($id))
		{
			return \App\Standards::whereIn('id', $id)->pluck('number')->implode(', ');
		}

		return \App\Standards::find($id)->number;
	}

	public static function getStandardDocuments($company_standard_id, $date, $document_type)
	{
		return \App\CompanyStandardDocument::where(['company_standard_id' => $company_standard_id, 'date' => $date, 'document_type' => $document_type])->get();
	}

	public static function getAccreditations($accreditationIds)
    {
        $accreditationId = explode(',', $accreditationIds);

        $accreditationName = '';
        foreach ($accreditationId as $item)
        {
            $getAccreditation = Accreditation::find($item);
            if ($getAccreditation){
                $accreditationName .= $getAccreditation->name.'<br>';
            }
        }

        return $accreditationName;
    }
}