<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicalExpertStandardFoodCode extends Model
{

    use SoftDeletes;

    public function foodcategory()
    {
        return $this->belongsTo(FoodCategory::class, 'food_category_code')->withDefault();
    }
    public function foodsubcategory()
    {
        return $this->belongsTo(FoodSubCategory::class, 'food_sub_category_code')->withDefault();
    }

    public function technicalExpertStandard(){
        return $this->belongsTo(TechnicalExpertStandard::class, 'auditor_standard_id', 'id');
    }

}
