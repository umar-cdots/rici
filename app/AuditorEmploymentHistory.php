<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditorEmploymentHistory extends Model
{
    use SoftDeletes;
}
