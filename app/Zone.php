<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Zone extends Model
{
    use LogsActivity;

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    public function cities()
    {
        return $this->morphMany(Cities::class, 'zoneable');
    }

    public function country()
    {
        return $this->belongsTo(Countries::class, 'country_id')->withDefault();
    }
    public function city(){
        return $this->hasMany(Cities::class);
    }
}
