<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManDaySheet extends Model
{
    protected $table = "manday_sheets";
    protected $guarded = [];

    public function standard(){
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
