<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutSourceRequestStandardIASCode extends Model
{
    protected $guarded = [];
    public function outsource_request_standard()
    {
        return $this->belongsTo(OutSourceRequestStandard::class, 'outsource_request_standard_id', 'id');
    }


    public function ias_code()
    {
        return $this->belongsTo(IAS::class, 'ias_id', 'id');
    }
}
