<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accreditation extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    function companies()
    {
        return $this->hasMany(Company::class);
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function outsource_request_standard_accreditations()
    {
        return $this->hasMany(OutSourceRequestStandardAccreditation::class, 'accreditation_id', 'id');
    }

    public function iafs()
    {
        return $this->hasMany(IAF::class, 'accreditation_id', 'id');
    }
}
