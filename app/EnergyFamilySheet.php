<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnergyFamilySheet extends Model
{
    protected $table = 'energy_family_sheets';
    protected $guarded = [];
    
    public function energy_code(){
        return $this->belongsTo(EnergyCode::class, 'energy_code_id', 'id');
    }
}
