<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class TechnicalExpertEducation extends Model implements HasMedia
{

    use SoftDeletes;
    use HasMediaTrait;

    protected  $table='technical_expert_educations';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'auditor_id', 'year_passing', 'institution',
        'degree_diploma', 'major_subjects', 'education_document',
    ];
    public function technical_expert()
    {
        return $this->belongsTo(TechnicalExpert::class);
    }
}
