<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class States extends Model
{
    use SoftDeletes;
    use LogsActivity; 

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;
    
    public function country()
    {
    	return $this->belongsTo(Countries::class, 'country_id');
    }

    public function cities()
    {
    	return $this->hasMany(Cities::class, 'state_id');
    }
}
