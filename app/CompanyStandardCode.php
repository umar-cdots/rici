<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyStandardCode extends Model
{
    use EnumValuesTrait;
    use SoftDeletes;

    public function iaf()
    {
        return $this->belongsTo(IAF::class, 'iaf_id')->withDefault();
    }

    public function ias()
    {
        return $this->belongsTo(IAS::class, 'ias_id')->withDefault();
    }

    function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function companyStandard()
    {
        return $this->belongsTo(CompanyStandards::class, 'company_standard_id' . 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

}
