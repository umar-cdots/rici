<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutSourceRequest extends Model
{

    use SoftDeletes;

    protected $table = 'out_source_requests';
    protected $guarded = [];

//    protected $dates = ['from','to'];


    public function operation_manager()
    {
        return $this->belongsTo(OperationManager::class, 'operation_manager_id', 'id');
    }

    public function operation_coordinator()
    {
        return $this->belongsTo(OperationCoordinator::class, 'operation_coordinator_id', 'id');
    }

    public function operation_approval_manager(){
        return $this->belongsTo(OperationManager::class, 'operation_approval_manager_id', 'id');
    }

    public function outsource_request_standards()
    {
        return $this->hasMany(OutSourceRequestStandard::class, 'outsource_request_id', 'id');
    }

    public function outsource_request_remarks()
    {
        return $this->hasMany(OutSourceRequestRemarks::class, 'outsource_request_id', 'id');
    }

    public function outsource_request_auditor()
    {
        return $this->hasOne(OutSourceRequestAuditor::class, 'outsource_request_id', 'id');
    }

//    public function setFromAttribute($value)
//    {
//        $this->attributes['from'] = substr($value, '0', '10');
//    }
//
//    public function setToAttribute($value)
//    {
//        $this->attributes['to'] = substr($value, '13');
//    }


}
