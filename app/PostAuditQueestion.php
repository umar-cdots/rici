<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostAuditQueestion extends Model
{
    use SoftDeletes;
    protected $table = "post_audit_queestions";
    protected $guarded = [];


    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id', 'id');
    }

    public function postAudit()
    {
        return $this->belongsTo(PostAudit::class, 'post_audit_id', 'id');
    }

    public function postAuditQuestionFile()
    {
        return $this->hasMany(PostAuditQuestionFile::class, 'post_audit_question_id', 'id');
    }


    // this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();

        static::deleting(function($question) { // before delete() method call this
            $question->postAuditQuestionFile()->delete();
            // do the rest of the cleanup...
        });
    }
}
