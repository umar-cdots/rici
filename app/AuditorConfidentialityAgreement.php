<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class AuditorConfidentialityAgreement extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
}
