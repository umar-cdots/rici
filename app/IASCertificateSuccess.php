<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IASCertificateSuccess extends Model
{
    protected $table = "i_a_s_certificate_success";
    protected $guarded = [];


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function aj_standard_stage()
    {
        return $this->belongsTo(AJStandardStage::class, 'aj_standard_stage_id', 'id');
    }
}
