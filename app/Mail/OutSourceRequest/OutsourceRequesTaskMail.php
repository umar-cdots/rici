<?php

namespace App\Mail\OutSourceRequest;

use App\OutSourceRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class OutsourceRequesTaskMail extends Mailable
{
    use Queueable, SerializesModels;


    public $outsource_request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OutSourceRequest $outsource_request)
    {
        $this->outsource_request = $outsource_request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->outsource_request->operation_manager_id != NULL) {
            return $this->from($this->outsource_request->operation_manager->user->email)->view('emails.outsource.taskmail');
        } else {
            return $this->from($this->outsource_request->operation_coordinator->user->email)->view('emails.outsource.taskmail');
        }

    }
}
