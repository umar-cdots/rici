<?php

namespace App\Mail\OutSourceRequest;

use App\OutSourceRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class OutsourceRequestToTaskMail extends Mailable
{
    use Queueable, SerializesModels;


    public $outsource_request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OutSourceRequest $outsource_request)
    {
        $this->outsource_request = $outsource_request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from($this->outsource_request->operation_approval_manager->user->email)->view('emails.outsource.totaskmail');
    }
}
