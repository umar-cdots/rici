<?php

namespace App\Mail;

use App\InvoiceDataEntry;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;


    public $subject;
    public $invoice;
    public $company;
    public $companyStandard;
    public $aj;
    public $auditConfirmationLetter;
    public $auditInvoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $invoice, $company, $companyStandard, $aj, $auditConfirmationLetter, $auditInvoice)
    {
        $this->subject = $subject;
        $this->invoice = $invoice;
        $this->company = $company;
        $this->companyStandard = $companyStandard;
        $this->aj = $aj;
        $this->auditConfirmationLetter = $auditConfirmationLetter;
        $this->auditInvoice = $auditInvoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

//        dd($this->auditConfirmationLetter,$this->auditInvoice);




        if (str_contains($this->invoice['attached_file'], '.docx')) {
            $path = public_path() . '/invoiceUploads/attachments/' . $this->invoice['attached_file'];
            $mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            $fileName = 'Audit-Plan.docx';
        } elseif (str_contains($this->invoice['attached_file'], '.pdf')) {
            $path = public_path() . '/invoiceUploads/attachments/' . $this->invoice['attached_file'];
            $mimeType = 'application/pdf';
            $fileName = 'Audit-Plan.pdf';
        }
        $attachments = [
            // first attachment
            '' . $this->auditConfirmationLetter . '' => [
                'as' => 'Audit-Confirmation-Letter.docx',
                'mime' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            ],
            '' . $path . '' => [
                'as' => $fileName,
                'mime' => $mimeType,
            ]
        ];
        if ($this->invoice['is_attached_audit_plan'] === '1' || $this->invoice['is_attached_audit_plan'] === 1) {
            $attachments['' . $this->auditInvoice . ''] = [
                'as' => 'Audit-Invoice.docx',
                'mime' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            ];
        }


        $settings = InvoiceDataEntry::first(['other_user_name', 'other_user_designation', 'other_user_email', 'other_user_phone_number', 'other_details_name']);
        $user = User::whereId($this->invoice['user_id'])->first(['first_name', 'middle_name', 'email', 'phone_number']);
        $email = $this->subject($this->subject)->view('emails.invoice')->with([
            'invoice' => $this->invoice,
            'company' => $this->company,
            'companyStandard' => $this->companyStandard,
            'aj' => $this->aj,
            'settings' => $settings,
            'user' => $user
        ]);

        foreach ($attachments as $filePath => $fileParameters) {
            $email->attach($filePath, $fileParameters);
        }
        return $email;
    }
}
