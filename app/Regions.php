<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Regions extends Model
{
    use SoftDeletes;
    use LogsActivity; 

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    public function regionCountries()
    {
    	return $this->hasMany(RegionsCountries::class, 'region_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'region_id', 'id');
    }


    public function countries()
    {
        return $this->belongsToMany(Countries::class, 'region_countries', 'region_id', 'country_id');
    }

    public function officeCountry()
    {
        return $this->belongsTo(Countries::class);
    }

    public function officeState()
    {
        return $this->belongsTo(States::class);
    }

    public function officeCity()
    {
        return $this->belongsTo(Cities::class);
    }

    public function regionscountries()
    {
        return $this->belongsToMany(Countries::class, 'region_countries', 'region_id', 'country_id');
    }
}
