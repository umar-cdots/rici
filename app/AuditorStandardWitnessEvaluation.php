<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class AuditorStandardWitnessEvaluation extends Model implements HasMedia
{
    use EnumValuesTrait;
    use SoftDeletes;
    use HasMediaTrait;
    protected $table = "auditor_standard_witness_evaluations";
    protected $guarded = [];

    public function auditorStandard()
    {
        return $this->belongsTo(AuditorStandard::class,'auditor_standard_id','id');
    }

    public function auditor(){
        return $this->belongsTo(Auditor::class,'auditor_id','id');
    }




}
