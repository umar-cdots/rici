<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AJStageTeam extends Model
{
    use SoftDeletes;

    protected $table = 'aj_stage_team';
    protected $guarded = [];
    
    public function ajStandardStage(){
        return $this->belongsTo(AJStandardStage::class, 'aj_standard_stage_id', 'id');
    }


    public function auditor(){
        return $this->belongsTo(Auditor::class, 'auditor_id', 'id');
    }
}
