<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutSourceRequestAuditor extends Model
{
//    use SoftDeletes;

    protected $table = 'out_source_request_auditors';
    protected $guarded = [];


    public function outsource_request()
    {
        return $this->belongsTo(OutSourceRequest::class, 'outsource_request_id', 'id');
    }

    public function auditor()
    {
        return $this->belongsTo(Auditor::class, 'auditor_id', 'id');
    }
}
