<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Standards extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function standardFamily()
    {
        return $this->belongsTo(StandardsFamily::class, 'standards_family_id');
    }


    public function questions()
    {
        return $this->hasMany(StandardsQuestions::class, 'standard_id');
    }

    public function mandaySheets()
    {
        return $this->hasMany(ManDaySheet::class, 'standard_id', 'id');
    }

    public function foodSheets()
    {
        return $this->hasMany(FoodFamilySheet::class, 'standard_id', 'id');
    }

    public function energySheets()
    {
        return $this->hasMany(EnergyFamilySheet::class, 'standard_id', 'id');
    }

//    public function auditorStandardCodes()
//    {
//        return $this->belongsTo(AuditorStandardCode::class);
//    }
//
//    public function auditorStandardGrades()
//    {
//        return $this->belongsTo(AuditorStandardGrade::class);
//    }

    public function company()
    {
        return $this->hasMany(CompanyStandards::class, 'company_id', 'id');
    }

    public function scheme_manager()
    {
        return $this->belongsToMany(SchemeManager::class, 'scheme_manager_standards', 'standard_id','scheme_manager_id');
    }

    public function auditors()
    {
        return $this->belongsToMany(Auditor::class, 'auditor_standards', 'auditor_id', 'standard_id');
    }

    public function outsource_request_standards()
    {
        return $this->hasMany(OutSourceRequestStandard::class);
    }


    public function companyStandardCodes()
    {
        return $this->hasMany(CompanyStandardCode::class, 'standard_id', 'id');
    }

    public function ajStandard(){
        return $this->hasOne(AJStandard::class,'standard_id','id');
    }


    public function postAudit()
    {
        return $this->hasMany(PostAudit::class, 'standard_id', 'id');
    }


}
