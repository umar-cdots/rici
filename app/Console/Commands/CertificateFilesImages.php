<?php

namespace App\Console\Commands;

use App\CertificateFile;
use Illuminate\Console\Command;

class CertificateFilesImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'certificate:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '100G');
        $certificateFiles = CertificateFile::where('file_type', 'base64')->get();
        foreach ($certificateFiles as $certificateFile){
            $certificateFile->file_path = $this->convertBase64ToImage($certificateFile->file_path,$certificateFile->file_name,$certificateFile->post_audit_id);
            $certificateFile->file_type = 'file';
            $certificateFile->save();
        }
    }


    public function convertBase64ToImage($base64Image, $fileName, $post_audit_id)
    {
        $image_64 = $base64Image; // your base64 encoded data

        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf

        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);

        // find substring for replace here e.g., data:image/png;base64,

        $image = str_replace($replace, '', $image_64);

        $image = str_replace(' ', '+', $image);

        $imageName = time() . '_' . explode('.', $fileName)[0] . '.' . $extension;

        // Get the public path
        $publicPath = public_path('/uploads/certificateFiles');

        // Store the file using file_put_contents
        file_put_contents($publicPath . '/' . $imageName, base64_decode($image));
        return $imageName;
    }
}
