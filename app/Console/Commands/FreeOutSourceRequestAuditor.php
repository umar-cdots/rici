<?php

namespace App\Console\Commands;

use App\OutSourceRequest;
use DateTime;
use Illuminate\Console\Command;

class FreeOutSourceRequestAuditor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'free:auditor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Free the approved outsource request auditor after 7 days ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $approved_auditors = OutSourceRequest::where('auditor_status', true)->where('status', 'approved')->get();

        foreach ($approved_auditors as $auditor) {

            $current_date = now();
            $approved_date = new DateTime($auditor->approved_date);
            $current_date = new DateTime($current_date);
            $interval = $approved_date->diff($current_date);
            $days = $interval->format('%a');//now do whatever you like with $days

            if ($days >= 10) {
                $auditor->update([
                    'auditor_status' => false,
                    'expiry_status' => 'expired'
                ]);
            }
        }
    }
}
