<?php

namespace App\Console\Commands;

use App\Exports\IASCertificaeExportMultiSheet;
use App\Exports\IASCertificateRetrieveExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use File;

class CertificateRetrieve extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'certificate:retrieve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Certification Body Retrieve All Certificate in RICI CMS System';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Excel::store(new IASCertificaeExportMultiSheet(), 'public/iasCertificate/CMS_certificate_sheet.xlsx', 'local');
    }


}
