<?php

namespace App\Console\Commands;

use App\AJStandardStage;
use App\Company;
use App\CompanyStandards;
use App\Exports\IASCertificaeExportMultiSheet;
use App\IAS;
use App\IASCertificateError;
use App\IASCertificateStats;
use App\IASCertificateSuccess;
use App\SchemeInfoPostAudit;
use Illuminate\Console\Command;

//use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use File;

class CertificateUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $finalData = [];
    protected $signature = 'certificate:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Certification Body Upload Certificate in IAS System';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        Excel::store(new IASCertificaeExportMultiSheet(), 'iasCertificate/final.xlsx', 'local');
//        whereIn('id', [1, 9, 120, 136, 165, 190])->
        $companies = Company::whereRaw('name <> ""')->where('is_send_to_ias', false)->with(['region', 'country', 'city', 'primaryContact', 'childCompanies'])
            ->whereHas('companyStandards.certificates', function ($q) {
                $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
            })->get();

        foreach ($companies as $key => $company) {
            $companies[$key]->companyStandards = CompanyStandards::
            where('company_id', $company->id)
                ->whereIn('type', ['base', 'single'])
                ->whereHas('certificates', function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                })->with(['companyStandardCodes', 'companyStandardFoodCodes', 'companyStandardEnergyCodes', 'standard.standardFamily', 'certificates' => function ($q) {
                    $q->where('report_status', 'new')->whereIn('certificate_status', ['certified', 'suspended', 'withdrawn']);
                }])->get();

            $auditTypes = ['surveillance_1'];
            foreach ($companies[$key]->companyStandards as $key1 => $companyStd) {
                $companies[$key]->companyStandards[$key1]->ajStandardStage = AJStandardStage::whereHas('ajStandard', function ($q) use ($companyStd) {
                    $q->where('standard_id', $companyStd->standard_id)
                        ->whereHas('ajJustification.company.companyStandards', function ($q) use ($companyStd) {
                            $q->where('company_id', $companyStd->company_id)
                                ->where('standard_id', $companyStd->standard_id)->whereIn('type', ['base', 'single']);
                        });
                })->whereIn('audit_type', $auditTypes)->get();
            }

            $this->curlRequest($company);
        }
//        $fileName = 'datafile.json';
//        Storage::put('/json/' . $fileName, json_encode($this->finalData));
    }

    public function curlRequest($company)
    {
        $job_number = '';
        $findIAS = false;
        if (!empty($company->companyStandards) && count($company->companyStandards) > 0) {
            foreach ($company->companyStandards as $companyStandard) {
                $isIAS = false;
                if ($companyStandard->is_ims == true) {
                    if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {
                        $job_number = $companyStandard->ajStandardStage[0]->job_number;
                        $imsCompanyStandards = $company->companyStandards()->where('is_ims', true)->get();
                        if (!empty($imsCompanyStandards) && count($imsCompanyStandards) > 0) {
                            $standardData = [];

                            foreach ($imsCompanyStandards as $imsCompanyStandard) {
                                $standardNameData = [];
                                if ($imsCompanyStandard->standard->standardFamily->name == 'Food') {
                                    if (!empty($imsCompanyStandard->companyStandardFoodCodes) && count($imsCompanyStandard->companyStandardFoodCodes) > 0) {

                                        foreach ($imsCompanyStandard->companyStandardFoodCodes as $code) {
                                            if (str_contains($code, 'IAS')) {
                                                $isIAS = true;
                                                if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                    $standardName = 'ISO/IEC 27001:2013';
                                                } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                    $standardName = 'ISO/IEC 27001:2022';
                                                } else {
                                                    $standardName = $imsCompanyStandard->standard->name;
                                                }
                                                $schemaName = $this->standardSchemaName($standardName);
                                                if ($isIAS == true) {
                                                    array_push($standardNameData, [
                                                        "standards_name" => $standardName
                                                    ]);
                                                    $data = [
                                                        "scheme_name" => $schemaName,
                                                        "standard_list" => $standardNameData
                                                    ];
                                                    array_push($standardData, $data);

                                                }
                                            }
                                            break;
                                        }
                                    }

                                } elseif ($imsCompanyStandard->standard->standardFamily->name == 'Energy Management') {
                                    if (!empty($imsCompanyStandard->companyStandardEnergyCodes) && count($imsCompanyStandard->companyStandardEnergyCodes) > 0) {
                                        $standardNameData = [];
                                        foreach ($imsCompanyStandard->companyStandardEnergyCodes as $code) {
                                            if (str_contains($code, 'IAS')) {
                                                $isIAS = true;

                                                if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                    $standardName = 'ISO/IEC 27001:2013';
                                                } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                    $standardName = 'ISO/IEC 27001:2022';
                                                } else {
                                                    $standardName = $imsCompanyStandard->standard->name;
                                                }
                                                $schemaName = $this->standardSchemaName($standardName);
                                                if ($isIAS == true) {
                                                    array_push($standardNameData, [
                                                        "standards_name" => $standardName
                                                    ]);
                                                    $data = [
                                                        "scheme_name" => $schemaName,
                                                        "standard_list" => $standardNameData
                                                    ];
                                                    array_push($standardData, $data);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                } else {
                                    if (!empty($imsCompanyStandard->companyStandardCodes) && count($imsCompanyStandard->companyStandardCodes) > 0) {
                                        $standardNameData = [];
                                        foreach ($imsCompanyStandard->companyStandardCodes as $code) {
                                            if (str_contains($code, 'IAS')) {
                                                $isIAS = true;
                                                if ($imsCompanyStandard->standard->name === 'ISO 27001:2013') {
                                                    $standardName = 'ISO/IEC 27001:2013';
                                                } else if ($imsCompanyStandard->standard->name === 'ISO 27001:2022') {
                                                    $standardName = 'ISO/IEC 27001:2022';
                                                } else {
                                                    $standardName = $imsCompanyStandard->standard->name;
                                                }
                                                $schemaName = $this->standardSchemaName($standardName);
                                                if ($isIAS == true) {
                                                    array_push($standardNameData, [
                                                        "standards_name" => $standardName
                                                    ]);
                                                    $data = [
                                                        "scheme_name" => $schemaName,
                                                        "standard_list" => $standardNameData
                                                    ];
                                                    array_push($standardData, $data);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }


                            }

                        }


                    }
                } else {
                    if ($companyStandard->standard->standardFamily->name == 'Food') {
                        if (!empty($companyStandard->companyStandardFoodCodes) && count($companyStandard->companyStandardFoodCodes) > 0) {
                            foreach ($companyStandard->companyStandardFoodCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                                break;
                            }
                        }

                    } elseif ($companyStandard->standard->standardFamily->name == 'Energy Management') {
                        if (!empty($companyStandard->companyStandardEnergyCodes) && count($companyStandard->companyStandardEnergyCodes) > 0) {
                            foreach ($companyStandard->companyStandardEnergyCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                                break;
                            }
                        }
                    } else {
                        if (!empty($companyStandard->companyStandardCodes) && count($companyStandard->companyStandardCodes) > 0) {
                            foreach ($companyStandard->companyStandardCodes as $code) {
                                if (str_contains($code, 'IAS')) {
                                    $isIAS = true;
                                }
                                break;

                            }
                        }
                    }


                    if ($isIAS === true) {
                        $standardData = [];
                        $standardNameData = [];
                        if ($companyStandard->standard->name === 'ISO 27001:2013') {
                            $standardName = 'ISO/IEC 27001:2013';
                        } else if ($companyStandard->standard->name === 'ISO 27001:2022') {
                            $standardName = 'ISO/IEC 27001:2022';
                        } else {
                            $standardName = $companyStandard->standard->name;
                        }
                        $schemaName = $this->standardSchemaName($standardName);

                        array_push($standardNameData, [
                            "standards_name" => $standardName
                        ]);
                        $data = [
                            "scheme_name" => $schemaName,
                            "standard_list" => $standardNameData
                        ];
                        array_push($standardData, $data);

                    }
                }


                if ($isIAS === true) {
                    if (!empty($companyStandard->ajStandardStage) && count($companyStandard->ajStandardStage) > 0) {

                        $sites = [];
                        $job_number = $companyStandard->ajStandardStage[0]->job_number;
                        $findIAS = true;

                        $status = '';
                        if ($companyStandard->certificates->certificate_status === 'certified') {
                            $status = 'active';
                        } elseif ($companyStandard->certificates->certificate_status === 'suspended') {
                            $status = 'suspended';
                        } elseif ($companyStandard->certificates->certificate_status === 'withdrawn') {
                            $status = 'withdrawn';
                        }
                        $latest_dates = SchemeInfoPostAudit::whereHas('postAudit', function ($q) use ($companyStandard) {
                            $q->where('company_id', $companyStandard->company_id)->where('standard_id', $companyStandard->standard_id);
                        })->latest()->first();
                        if (!is_null($latest_dates)) {
                            $original_issue_date = date('Ymd', strtotime(($latest_dates->original_issue_date)));
                        } else {
                            $original_issue_date = '';
                        }
                        if (!is_null($companyStandard->last_certificate_issue_date)) {
                            $issue_date = date("Ymd", strtotime($companyStandard->last_certificate_issue_date));
                        } else {
                            $issue_date = '';
                        }

                        if (isset($latest_dates) && !is_null($latest_dates) && !is_null($latest_dates->new_expiry_date)) {
                            $expiry_date = date('Ymd', strtotime(($latest_dates->new_expiry_date)));
                        } else {
                            $expiry_date = '';
                        }
                        if (!empty($company->childCompanies) && count($company->childCompanies) > 0) {
                            foreach ($company->childCompanies as $keyData => $child_company) {
                                $data = [
                                    "street" => $child_company->address,
                                    "city" => str_replace(' ', '', $child_company->city->name),
                                    "state" => "-",
                                    "country" => $child_company->country ? $this->countryName($child_company->country->name) : $this->countryName($company->country->name),
                                    "postcode" => $child_company->city->post_code,
                                    "scope_description" => ""
                                ];
                                array_push($sites, $data);
                            }
                        } else {
//                            $data = [
//                                "street" => $company->address,
//                                "city" => $company->city->name,
//                                "state" => "-",
//                                "country" => $this->countryName($company->country->name),
//                                "postcode" => $company->city->post_code,
//                                "scope_description" => "-"
//                            ];
//                            array_push($sites, $data);
                        }

                        $headers = [
                            'x-http-authorization: ' . config('app.IAF')['KEY'],
                            'Content-Type: application/json'
                        ];
                        $curl = curl_init();

                        $postData = [
                            "certificate_number" => $job_number,
                            "certificate_identity_number" => 'CIN-'.$job_number,
                            "certification_status" => $status,
                            "certificate_accreditation_status" => "Accredited",
                            "certification_type" => "Management System",
                            "certification_scope" => $company->scope_to_certify,
                            "certification_original_issue_date" => $original_issue_date,
                            "certification_issue_date" => $issue_date,
                            "certification_expiry_date" => $expiry_date,
                            "certified_entity_name" => $company->name,
                            "certified_entity_english_name" => "-",
                            "certified_entity_trading_name" => "-",
                            "certified_entity_unique_id" => 'CEUID-'.$company->id,
                            "certified_entity_street_address" => $company->address,
                            "certified_entity_street_city" => $company->city->name,
                            "certified_entity_state" => "-",
                            "certified_entity_post_code" => $company->city->post_code,
                            "certified_entity_country" => $this->countryName($company->country->name),
                            "certified_entity_website" => "-",
                            "accreditation_body_name" => "International Accreditation Services",
                            "accreditation_body_acronym_name" => "IAS",
                            "schemes" => $standardData,
                            "sites" => $sites
                        ];

//                        array_push($this->finalData, $postData);

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://api.iafcertsearch.org/api/client/v1/cb/upload-cert',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => json_encode($postData),
                            CURLOPT_HTTPHEADER => $headers,
                        ));

                        $response = curl_exec($curl);

                        curl_close($curl);
//                        echo $response;

                        $response = json_decode($response);
                        if (isset($response) && isset($response->data) && isset($response->data->company_certifications_id) && $response->data->company_certifications_id !== '') {
                            $company = Company::where('id', $company->id)->first();
                            $company->is_send_to_ias = 1;
                            $company->save();

                            $record = IASCertificateStats::whereDate('upload_date', date('Y-m-d'))->first();
                            if (!is_null($record)) {
                                $record->success_count = $record->success_count + 1;
                                $record->save();
                            } else {
                                IASCertificateStats::create([
                                    'success_count' => 1,
                                    'error_count' => 0,
                                    'upload_date' => date('Y-m-d')
                                ]);
                            }
                            IASCertificateSuccess::create([
                                'upload_date' => date('Y-m-d'),
                                'company_id' => $company->id,
                                'standard_id' => $companyStandard->standard_id,
                                'company_standard_id' => $companyStandard->id,
                                'aj_standard_id' => $companyStandard->ajStandardStage[0]->aj_standard_id,
                                'aj_standard_stage_id' => $companyStandard->ajStandardStage[0]->id,
                            ]);
                        } else {
                            $record = IASCertificateStats::whereDate('upload_date', date('Y-m-d'))->first();
                            if (!is_null($record)) {
                                $record->error_count = $record->error_count + 1;
                                $record->save();
                            } else {
                                IASCertificateStats::create([
                                    'success_count' => 0,
                                    'error_count' => 1,
                                    'upload_date' => date('Y-m-d')
                                ]);
                            }
                            IASCertificateError::create([
                                'upload_date' => date('Y-m-d'),
                                'error_response' => json_encode($response),
                                'company_id' => $company->id,
                                'standard_id' => $companyStandard->standard_id,
                                'company_standard_id' => $companyStandard->id,
                                'aj_standard_id' => $companyStandard->ajStandardStage[0]->aj_standard_id,
                                'aj_standard_stage_id' => $companyStandard->ajStandardStage[0]->id,
                            ]);
                        }
                    }
                }
            }
        }


    }


    public function standardSchemaName($standard)
    {
        $name = '';

        if ($standard == 'ISO 9001:2015') {
            $name = 'Quality Management Systems';
        } elseif ($standard == 'ISO 27001:2013' || $standard == 'ISO 27001:2022' || $standard == 'ISO/IEC 27001:2013' || $standard == 'ISO/IEC 27001:2022') {
            $name = 'Information Security Management Systems';
        } elseif ($standard == 'ISO 14001:2015') {
            $name = 'Environmental Management Systems';
        } elseif ($standard == 'ISO 45001:2018') {
            $name = 'Occupational Health and Safety Management Systems';
        } elseif ($standard == 'ISO 22000:2018') {
            $name = 'Food Safety Management Systems';
        } elseif ($standard == 'ISO 50001:2018') {
            $name = 'Energy Management Systems';
        } elseif ($standard == 'ISO 29993:2017') {
            $name = 'Learning Services outside formal Education Service Requirements';
        } elseif ($standard == 'ISO 22301:2019') {
            $name = 'Business Continuity Management Systems';
        }

        return $name;

    }

    public function countryName($countryName)
    {
        $name = '';
        if (strtolower($countryName) == strtolower('Bangladesh')) {
            $name = 'Bangladesh';
        } elseif (strtolower($countryName) == strtolower('Kingdom of Bahrain')) {
            $name = 'Bahrain';
        } elseif (strtolower($countryName) == strtolower('Kingdom of Saudi Arabia')) {
            $name = 'Saudi Arabia';
        } elseif (strtolower($countryName) == strtolower('Kuwait')) {
            $name = 'Kuwait';
        } elseif (strtolower($countryName) == strtolower('Oman')) {
            $name = 'Oman';
        } elseif (strtolower($countryName) == strtolower('Pakistan')) {
            $name = 'Pakistan';
        } elseif (strtolower($countryName) == strtolower('United Arab Emirates')) {
            $name = 'United Arab Emirates';
        } elseif (strtolower($countryName) == strtolower('USA')) {
            $name = 'United States';
        }
        return $name;
    }


}
