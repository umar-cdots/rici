<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditJustification extends Model
{
    use EnumValuesTrait;
    use SoftDeletes;

    protected $table = 'audit_justifications';
    protected $guarded = [];
    
    public function ajStandards(){
        return $this->hasMany(AJStandard::class, 'audit_justification_id', 'id');
    }

    public function company(){
        return $this->belongsTo(Company::class,'company_id','id');
    }
}
