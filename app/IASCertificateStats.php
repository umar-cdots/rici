<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IASCertificateStats extends Model
{
    protected $table = "i_a_s_certificate_stats";
    protected $guarded = [];
}
