<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\EnumValuesTrait;

class CompanyContacts extends Model
{
    use EnumValuesTrait;
	use SoftDeletes;

	protected $fillable = ['title','name', 'position', 'contact', 'landline', 'email', 'type'];
    
    public function company()
    {
    	return $this->belongsTo(Company::class);
    }

    public function isPrimary()
    {
    	return $this->type == 'primary' ? true : false;
    }

    public function getNameWithTitle(){
        return $this->title . '. '.$this->name;
    }
}
