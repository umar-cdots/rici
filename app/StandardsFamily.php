<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StandardsFamily extends Model
{
	use SoftDeletes;
    public function standards()
    {
    	return $this->hasMany(Standards::class);
    }
}
