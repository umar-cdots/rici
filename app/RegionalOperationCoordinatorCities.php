<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class RegionalOperationCoordinatorCities extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $table = "regional_operation_coordinator_cities";
    protected $guarded = [];

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;
}
