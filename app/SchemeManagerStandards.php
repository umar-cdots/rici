<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchemeManagerStandards extends Model
{
    protected $table = 'scheme_manager_standards';
//    protected $guarded = [];

    protected $fillable = ['scheme_manager_id', 'standard_id','standard_family_id'];

    public $timestamps = false;

    public function scheme_manager(){
        return $this->belongsTo(SchemeManager::class,'scheme_manager_id','id');
    }

    public function standard(){
        return $this->belongsTo(Standards::class,'standard_id','id');
    }

    public function standard_family(){
        return $this->belongsTo(StandardsFamily::class,'standard_family_id');
    }
}
