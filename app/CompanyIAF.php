<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyIAF extends Model
{
	use SoftDeletes;

	protected $table = "company_i_a_fs";

	protected $fillable = ['iaf_id'];

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function IAF()
	{
		return $this->belongsTo(IAF::class, 'iaf_id');
	}
}
