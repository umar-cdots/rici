<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Traits\EnumValuesTrait;

class Company extends Model implements HasMedia
{
    use EnumValuesTrait;
    use SoftDeletes;
    use LogsActivity;
    use HasMediaTrait;

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    const CERTIFICATE_TYPES = ['initial_inquiry_form' => "Initial Inquiry Form", 'certification_proposal_form' => "Certification Proposal Form", 'certification_contract_form' => "Certification Contract Form", 'other_documents' => "Other Documents"];

    public function region()
    {
        return $this->belongsTo(Regions::class);
    }

    public function country()
    {
        return $this->belongsTo(Countries::class);
    }

    public function city()
    {
        return $this->belongsTo(Cities::class);
    }

    public function companyIAFs()
    {
        return $this->hasMany(CompanyIAF::class);
    }


    public function companyIASs()
    {
        return $this->hasMany(CompanyIAS::class);
    }

    public function companyIAFCodes()
    {
        return $this->hasMany(CompanyStandardCode::class, 'company_id', 'id');
    }

    public function companyIASCodes()
    {
        return $this->hasMany(CompanyStandardCode::class, 'company_id', 'id');
    }


    public function parentCompany()
    {
        return $this->belongsTo(Company::class);
    }

    public function childCompanies()
    {
        return $this->hasMany(Company::class);
    }

    public function contacts()
    {
        return $this->hasMany(CompanyContacts::class);
    }

    public function primaryContact()
    {
        return $this->hasOne(CompanyContacts::class)->where('type', 'primary');
    }

    // public function accreditation()
    // {
    // 	return $this->belongsTo(Accreditation::class);
    // }

    public function companyStandards()
    {
        return $this->hasMany(CompanyStandards::class,'company_id','id');//->with('standard');
    }

    public function isProfileCompleted()
    {
        return $this->company_form_status == 'completed' ? true : false;
    }

    public function auditJustifications()
    {
        return $this->hasMany(AuditJustification::class, 'company_id', 'id');
    }

    public function postAudit()
    {
        return $this->hasMany(PostAudit::class, 'company_id', 'id');
    }
}
