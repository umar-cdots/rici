<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyStandardAccreditation extends Model
{
    protected $table ='company_standards_accreditations';
    protected $guarded = [];
    public $timestamps = false;
}
