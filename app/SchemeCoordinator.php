<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchemeCoordinator extends Model
{
    use SoftDeletes;

    protected $table = 'scheme_coordinator';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function standard(){
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function scheme_manager(){
        return $this->belongsTo(SchemeManager::class, 'scheme_manager_id', 'id');
    }
}
