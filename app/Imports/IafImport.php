<?php

namespace App\Imports;

use App\IAF;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class IafImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new IAF([
            'code' => $row['code'],
            'name' => $row['name'],
        ]);
    }
}
