<?php

namespace App\Imports;

use App\ManDaySheet;
use App\Standards;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Log;

class MandaysImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $garbageRecord = [];
        foreach ($rows as $data) {
            $row = [];
            foreach ($data as $key => $value) {
                $row[$this->filterHeader($key)] = $value;
            }
            if ($row['standard'] != null):

                $standard = Standards::where('name', $row['standard'])->first();
                if(!is_null($standard)){
                    $sheets[] = ManDaySheet::create(
                        [
                            'standard_id' => $standard->id,
                            'standard_number' => $row['standard'],
                            'effective_employes_on' => $row['effective_employes_from'],
                            'effective_employes_off' => $row['effective_employes_to'],
                            'complexity' => $row['complexity'],
                            'stage1_on' => $row['stage1_on'],
                            'stage1_off' => $row['stage1_off'],
                            'stage2_on' => $row['stage2_on'],
                            'stage2_off' => $row['stage2_off'],
                            'surveillance_on' => $row['surveillance_on'],
                            'surveillance_off' => $row['surveillance_off'],
                            'reaudit_on' => $row['reaudit_on'],
                            'reaudit_off' => $row['reaudit_off'],
                            'frequency' => $row['frequency'],
                        ]
                    );
                }else{
                    array_push($garbageRecord, $row);

                }
            endif;

        }
        Log::info($garbageRecord);
        return $sheets;
    }

    public function filterHeader($header)
    {
        $setHeader = preg_replace('/[^a-zA-Z0-9_]/', '', str_replace(' ', '_', strtolower($header)));
        return $setHeader;
    }
}
