<?php

namespace App\Imports;

use App\EnergyFamilySheet;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EnergyImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $index =>$data) {
            $row = [];
            foreach ($data as $key => $value) {
                $row[$this->filterHeader($key)] = $value;
            }
            $sheets[$index] = EnergyFamilySheet::create([
                'energy_code_id' => $row['code'],
                "from" => $row['from'],
                "to" => $row['to'],
                "complexity" => $row['complexity'],
                "considerations" => $row['considerations']
            ]);

        }
        return $sheets;
    }

    public function filterHeader($header)
    {
        $setHeader = preg_replace('/[^a-zA-Z0-9_]/', '', str_replace(' ', '_', strtolower($header)));
        return $setHeader;
    }
}
