<?php

namespace App\Imports;

use App\FoodCategory;
use App\FoodFamilySheet;
use App\ManDaySheet;
use App\Standards;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FoodImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $data) {
            $row = [];
            foreach ($data as $key => $value) {
                $row[$this->filterHeader($key)] = $value;
            }
            $standard = Standards::where('name', $row['standard'])->first();
            $food_category = FoodCategory::where('code', $row['category'])->first();
            if(!empty($food_category)){
                $sheets[] = FoodFamilySheet::create([
                    'standard_id' => $standard->id,
                    'food_category_id' => $food_category->id,
                    'TD' => $row['td'],
                    "TH" => $row['th'],
                    "TMS" => $row['tms'],
                    "TFTE_from" => $row['tfte_from'],
                    "TFTE_to" => $row['tfte_to'],
                    "TFTE_value" => $row['tfte_value']
                ]);
            }

        }
        return $sheets;
    }

    public function filterHeader($header)
    {
        $setHeader = preg_replace('/[^a-zA-Z0-9_]/', '', str_replace(' ', '_', strtolower($header)));
        return $setHeader;
    }
}
