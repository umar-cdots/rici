<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certificate extends Model
{
    use SoftDeletes;
    protected $table = "certificates";
    protected $guarded = [];


    public function companyStandards()
    {
        return $this->belongsTo(CompanyStandards::class, 'company_standard_id', 'id');
    }

    public function ajStandardStage()
    {
        return $this->belongsTo(AJStandardStage::class, 'aj_standard_id', 'id')->select('id','audit_type','excepted_date');
    }
}
