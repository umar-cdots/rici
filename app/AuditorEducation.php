<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class AuditorEducation extends Model implements HasMedia
{

    use SoftDeletes;
    use HasMediaTrait;


    public function auditor()
    {
        return $this->belongsTo(Auditor::class);
    }
}
