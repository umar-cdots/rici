<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class TechnicalExpert extends Model implements HasMedia
{

    use EnumValuesTrait;
    use SoftDeletes;
    use LogsActivity;
    use HasMediaTrait;


    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    protected  $table='technical_expert';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'technical_expert_status', 'user_id', 'first_name',
        'last_name', 'email', 'phone',
        'landline', 'postal_address', 'region_id',
        'country_id', 'city_id', 'dob',
        'language_id', 'working_experience', 'main_education',
        'nationality_id', 'job_status', 'profile_remarks','profile_pic'
    ];


    public function languages()
    {
        return $this->belongsToMany(Languages::class, 'technical_expert_languages', 'technical_expert_id','language_id');
    }
    public function region()
    {
        return $this->belongsTo(Regions::class);
    }

    public function country()
    {
        return $this->belongsTo(Countries::class);
    }

    public function city()
    {
        return $this->belongsTo(Cities::class);
    }

    public function education()
    {
        return $this->hasMany(TechnicalExpertEducation::class);
    }

    public function confidentialityAgreements()
    {
        return $this->hasMany(TechnicalExpertConfidentialityAgreement::class);
    }


    public function fullName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function nationality()
    {
        return $this->belongsTo(Countries::class);
    }

    public function jobStatusString()
    {
        $jobStatus = $this->job_status;
        $jobStatus = Str::title($jobStatus);
        $jobStatus = str_replace('_',' ', $jobStatus);

        return $jobStatus;
    }


    public function technicalExpertStandards()
    {
        return $this->belongsToMany(Standards::class, 'technical_expert_standards', 'auditor_id', 'standard_id')->withPivot('id');
    }
}
