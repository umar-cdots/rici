<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\EnumValuesTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class CompanyStandards extends Model implements HasMedia
{
    use EnumValuesTrait;
    use HasMediaTrait;
    // use SoftDeletes;

    protected $dates = ['old_cb_certificate_issue_date', 'old_cb_certificate_expiry_date'];

    protected $cast = [
        'accreditation_ids' => "json"
    ];

    function company()
    {
        return $this->belongsTo(Company::class,'company_id','id');
    }

    function standard()
    {
        return $this->belongsTo(Standards::class,'standard_id','id');
    }

    function isNewClient()
    {
        return $this->client_type == 'new' ? true : false;
    }

    function auditActivityStage()
    {
        return $this->belongsTo(AuditActivityStages::class);
    }

    function oldCbAuditActivityStage()
    {
        return $this->belongsTo(AuditActivityStages::class, 'old_cb_audit_activity_stage_id');
    }

    function accreditations()
    {
        return $this->belongsToMany(Accreditation::class, 'company_standards_accreditations', 'company_standard_id', 'accreditation_id');
    }

    function companyStandardsAnswers()
    {
        return $this->hasMany(CompanyStandardsAnswers::class, 'company_standards_id');
    }

    function companyStandardDocuments()
    {
        return $this->hasMany(CompanyStandardDocument::class, 'company_standard_id');
    }

    function oldCBLastAuditReportDocMedia()
    {
        return $this->getMedia()->where('id', $this->old_cb_last_audit_report_doc_media_id)->first();
    }

    function oldCBCertificateCopyDocMedia()
    {
        return $this->getMedia()->where('id', $this->old_cb_certificate_copy_doc_media_id)->first();
    }

    function oldCBOtherDocMedia()
    {
        return $this->getMedia()->where('id', $this->old_cb_others_doc_media_id)->first();
    }


    public function companyStandardCodes()
    {
        return $this->hasMany(CompanyStandardCode::class, 'company_standard_id', 'id');
    }

    public function companyStandardFoodCodes()
    {
        return $this->hasMany(CompanyStandardFoodCode::class, 'company_standard_id', 'id');
    }

    public function companyStandardEnergyCodes()
    {
        return $this->hasMany(CompanyStandardEnergyCode::class, 'company_standard_id', 'id');
    }

    public function certificates()
    {
        return $this->hasOne(Certificate::class, 'company_standard_id', 'id')->latest();
    }


    public function certificateWithdrawn()
    {
        return $this->hasOne(Certificate::class, 'company_standard_id', 'id')->where('certificate_status','withdrawn')->latest();
    }

    public function certificateSuspension()
    {
        return $this->hasOne(Certificate::class, 'company_standard_id', 'id')->where('certificate_status','suspended')->latest();
    }

    public function reportCertificate(){
        return $this->hasOne(Certificate::class, 'company_standard_id', 'id');
    }

}
