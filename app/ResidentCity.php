<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ResidentCity extends Model
{
    protected $table = "resident_cities";
    protected $guarded = [];
    use LogsActivity;

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    public function country()
    {
        return $this->belongsTo(ResidentCountry::class, 'country_id')->withDefault();
    }
}
