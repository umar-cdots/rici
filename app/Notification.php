<?php

namespace App;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $guarded = [];

    public $dates = ['updated_at', 'created_at'];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('notification_order', function (Builder $builder) {
            $builder->orderBy('id');
        });
    }

    public function sentToUser(){
        return $this->belongsTo(User::class, 'sent_to', 'id');
    }

    public function sentByUser(){
        return $this->belongsTo(User::class, 'sent_by', 'id');
    }

    public function getShortBody(){

        return $this->sent_message;
//        return substr(ucfirst($this->sent_message), 0, 100).'...';
    }

    public function read(){
        return ($this->is_read == 0) ? 'no' : 'yes';
    }
    public function getCreatedAtAttribute($date){

//        return \Carbon\Carbon::createFromTimeStamp(strtotime($date))->diffForHumans(null,false,true,2);
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
//        return $this->time_elapsed_string($date);
    }
    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
