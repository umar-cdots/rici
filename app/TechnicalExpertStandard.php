<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicalExpertStandard extends Model
{
    use SoftDeletes;

    protected $table='technical_expert_standards';
    public function technical_expert()
    {
        return $this->belongsTo(TechnicalExpert::class);
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class);
    }

    public function technicalExpertStandardCodes()
    {
        return $this->hasMany(TechnicalExpertStandardCode::class, 'auditor_standard_id');
    }


    public function technicalExpertStandardFoodCode(){
        return $this->hasMany(TechnicalExpertStandardFoodCode::class, 'auditor_standard_id', 'id');
    }

    public function technicalExpertStandardGrades(){
        return $this->hasMany(TechnicalExpertStandardGrade::class, 'auditor_standard_id');
    }

}
