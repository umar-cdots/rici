<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnergyWeightage extends Model
{
    protected $table = 'energy_weightage';
    protected $guarded = [];
}
