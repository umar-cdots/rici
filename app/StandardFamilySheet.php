<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StandardFamilySheet extends Model
{
    protected $table = 'standard_family_sheets';
    protected $guarded=[];
}
