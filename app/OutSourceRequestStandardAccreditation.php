<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutSourceRequestStandardAccreditation extends Model
{
    use SoftDeletes;

    protected $table = 'out_source_request_standard_accreditations';
    protected $guarded = [];

    public function outsource_request_standard()
    {
        return $this->belongsTo(OutSourceRequestStandard::class, 'outsource_request_standard_id', 'id');
    }


    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }
}
