<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateNotUploadedView extends Model
{
    protected $table = "certificate_not_uploaded_views";
}
