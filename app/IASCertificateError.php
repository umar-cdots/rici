<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IASCertificateError extends Model
{
    protected $table = "i_a_s_certificate_errors";
    protected $guarded = [];


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
