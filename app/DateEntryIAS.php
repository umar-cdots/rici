<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DateEntryIAS extends Model
{
    use SoftDeletes;
    protected $table = "data_entry_ias";
    protected $guarded = [];
}
