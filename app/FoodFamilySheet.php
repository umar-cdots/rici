<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodFamilySheet extends Model
{
    protected $table = 'food_family_sheets';
    protected $guarded = [];

    public function food_category(){
        return $this->belongsTo(FoodCategory::class, 'food_category_id', 'id');
    }

    public function standard(){
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
