<?php

namespace App;

use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use SoftDeletes;
    use LogsActivity;
    use HasMediaTrait;
    use HasRoles;

    /**
     * Log only changed columns.
     *
     * @var boolean
     */
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'dob', 'region_id', 'working_experience', 'main_education', 'nationality_id', 'remarks', 'phone_number', 'status', 'last_name', 'username', 'email', 'password', 'country_id', 'city_id', 'address', 'user_type', 'is_enabled'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar')
            ->singleFile();
    }

    public function languages()
    {
        return $this->belongsToMany(Languages::class, 'user_languages', 'user_id', 'language_id');
    }


    public function nationality()
    {
        return $this->hasOne(Nationalities::class);
    }

    public function region()
    {
        return $this->belongsTo(Regions::class, 'region_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Countries::class);
    }

    public function state()
    {
        return $this->hasOne(States::class);
    }

    public function city()
    {
        return $this->belongsTo(Cities::class);
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name;
    }

    public function scheme_manager()
    {
        return $this->hasOne(SchemeManager::class, 'user_id', 'id');
    }

    public function scheme_coordinator()
    {
        return $this->hasOne(SchemeCoordinator::class, 'user_id', 'id');
    }

    public function operation_manager()
    {
        return $this->hasOne(OperationManager::class, 'user_id', 'id');
    }

    public function operation_coordinator()
    {
        return $this->hasOne(OperationCoordinator::class, 'user_id', 'id');
    }

    public function auditor()
    {
        return $this->hasOne(Auditor::class, 'user_id', 'id');
    }

    public function technical_expert()
    {
        return $this->hasOne(TechnicalExpert::class, 'user_id', 'id');
    }

    public function remarks()
    {
        return $this->hasMany(AJRemarks::class);
    }
}
