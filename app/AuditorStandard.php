<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditorStandard extends Model
{
    use SoftDeletes;

    public function auditor()
    {
        return $this->belongsTo(Auditor::class, 'auditor_id', 'id');

    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function auditorStandardCodes()
    {
        return $this->hasMany(AuditorStandardCode::class, 'auditor_standard_id');
    }

    public function auditorStandardGrades()
    {
        return $this->hasMany(AuditorStandardGrade::class, 'auditor_standard_id');
    }

    public function auditorStandardFoodCode()
    {
        return $this->hasMany(AuditorStandardFoodCode::class, 'auditor_standard_id', 'id');
    }

    public function auditorStandardEnergyCode()
    {
        return $this->hasMany(AuditorStandardEnergyCode::class, 'auditor_standard_id', 'id');
    }

    public function auditorStandardWitnessEvaluation()
    {
        return $this->hasMany(AuditorStandardWitnessEvaluation::class, 'auditor_standard_id', 'id');
    }

}
