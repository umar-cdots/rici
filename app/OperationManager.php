<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperationManager extends Model
{
    use SoftDeletes;

    protected $table = 'operation_manager';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function operation_coordinators(){
        return $this->hasMany(OperationCoordinator::class, 'operation_manager_id', 'id');
    }

    public function outsource_requests(){
        return $this->hasMany(OutSourceRequest::class);

    }

    public function full_name(){
        if($this->full_name == '' || $this->full_name == null)
            return $this->user->first_name.' '.$this->user->last_name;
        else
            return $this->full_name;
    }
}
