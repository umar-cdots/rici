<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SchemeManager extends Model
{
    use SoftDeletes;

    protected $table = 'scheme_manager';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function standards() {
        return $this->belongsToMany(Standards::class, 'scheme_manager_standards', 'scheme_manager_id', 'standard_id');
    }

    public function standard_families() {
        return $this->belongsToMany(StandardsFamily::class, 'scheme_manager_standards', 'scheme_manager_id', 'standard_family_id'); //Standards
    }

    public function scheme_coordinators(){
        return $this->hasMany(SchemeCoordinator::class, 'scheme_manager_id', 'id');
    }

    public function full_name(){
        if($this->full_name == '' || $this->full_name == null)
            return $this->user->first_name.' '.$this->user->middle_name.' '.$this->user->last_name;
        else
            return $this->full_name;
    }


    public function outsource_requests(){
        return $this->hasMany(OutSourceRequest::class);

    }

}
