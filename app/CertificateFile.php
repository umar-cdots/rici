<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CertificateFile extends Model
{
    use SoftDeletes;
    protected $table = "certificate_files";
    protected $guarded = [];


    public function postAudit(){
        return $this->belongsTo(PostAudit::class,'post_audit_id','id');
    }
}
