<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostAudit extends Model
{
    use SoftDeletes;
    protected $table = "post_audits";
    protected $guarded = [];


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }

    public function AjStandardStage()
    {
        return $this->belongsTo(AJStandardStage::class, 'aj_standard_stage_id', 'id');
    }
    public function sentToUser(){
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }

    public function sentByUser(){
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function questionPostAudit(){
        return $this->belongsToMany(PostAuditQueestion::class,'questions','post_audit_id','question_id','id');
    }

    public function postAuditSchemeInfo(){
        return $this->hasOne(SchemeInfoPostAudit::class,'post_audit_id','id');
    }

    public function postAuditSchemeInfoLatest(){
        return $this->hasOne(SchemeInfoPostAudit::class,'post_audit_id','id')->latest();
    }

    public function postAuditCertificateFile(){
        return $this->hasMany(CertificateFile::class,'post_audit_id','id');
    }
}
