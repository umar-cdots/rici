<?php

namespace App;

use App\Traits\EnumValuesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditorStandardCode extends Model
{
    use EnumValuesTrait;
    use SoftDeletes;

    public function iaf()
    {
        return $this->belongsTo(IAF::class, 'iaf_id')->withDefault();
    }


    public function ias()
    {
        return $this->belongsTo(IAS::class, 'ias_id')->withDefault();
    }


    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function auditorStandard()
    {
        return $this->belongsTo(AuditorStandard::class);
    }


}
