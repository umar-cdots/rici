<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodCategory extends Model
{
    use SoftDeletes;
    public function foodsubcategories()
    {
        return $this->hasMany(FoodSubCategory::class, 'food_category_id');
    }

    public function foodSheet(){
        return $this->hasMany(FoodFamilySheet::class, 'food_category_id', 'id');
    }

    public function accreditation()
    {
        return $this->belongsTo(Accreditation::class, 'accreditation_id', 'id');
    }

    public function standard()
    {
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
