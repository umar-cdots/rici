<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceNotification extends Model
{
    protected $table = "invoice_notifications";
    protected $guarded = [];
}
