<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class TechnicalExpertConfidentialityAgreement extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
    protected  $table='technical_expert_confidentiality_agreements';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'auditor_id', 'date_signed', 'agreement_document',
    ];
    public function technical_expert()
    {
        return $this->belongsTo(TechnicalExpert::class);
    }

}
