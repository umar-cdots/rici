<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataEntryIAF extends Model
{
    use SoftDeletes;
    protected $table = "data_entry_iaf";
    protected $guarded = [];
}
