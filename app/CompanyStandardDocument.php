<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\EnumValuesTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class CompanyStandardDocument extends Model implements HasMedia
{
    use SoftDeletes;
    use EnumValuesTrait;
    use HasMediaTrait;

    protected $table = "company_standard_documents";
    protected $guarded = [];

    protected $dates = ['date'];

    public function companyStandard()
    {
    	return $this->belongsTo(CompanyStandards::class, 'company_standard_id');
    }

    public function getDocument()
    {
    	return $this->getMedia()->where('id', $this->media_id)->first();
    }
}
