<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyIAS extends Model
{
	use SoftDeletes;

	protected $table = "company_i_a_s";

	protected $fillable = ['ias_id'];

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function IAS()
	{
		return $this->belongsTo(IAS::class, 'ias_id');
	}
}
