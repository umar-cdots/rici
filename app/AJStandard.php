<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AJStandard extends Model
{
    use SoftDeletes;
    protected $table = "aj_standards";
    protected $guarded = [];

    public function ajJustification(){
        return $this->belongsTo(AuditJustification::class, 'audit_justification_id', 'id');
    }
    
    public function ajStandardStages(){
        return $this->hasMany(AJStandardStage::class, 'aj_standard_id', 'id');
    }

    public function standard(){
        return $this->belongsTo(Standards::class, 'standard_id', 'id');
    }
}
