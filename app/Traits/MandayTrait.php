<?php

namespace App\Traits;

use App\EnergyEnms;
use App\EnergyFamilySheet;
use App\EnergyWeightage;
use App\FoodFamilySheet;
use App\ManDaySheet;
use App\Standards;

trait MandayTrait
{

    public static function generateSimpleTable($standard_id = null, $complexity = null, $effective_employees = null, $frequency = '')
    {


        $row = ManDaySheet::with('standard')->where(function ($query) use ($standard_id, $effective_employees, $complexity, $frequency) {
            $query->where('standard_id', $standard_id);
            $query->whereRaw('? between effective_employes_on and effective_employes_off', $effective_employees);
            $query->where('complexity', $complexity);
            $query->where('frequency', $frequency);
        })->first();


        if (!empty($row)) {
            if ($frequency == 'biannual') {
                $data = [
                    'stage_1_onsite' => $row->stage1_on,
                    'stage_1_offsite' => $row->stage1_off,
                    'stage_2_onsite' => $row->stage2_on,
                    'stage_2_offsite' => $row->stage2_off,

                    'surveillance_1_onsite' => $row->surveillance_on,
                    'surveillance_1_offsite' => $row->surveillance_off,

                    'surveillance_2_onsite' => $row->surveillance_on,
                    'surveillance_2_offsite' => $row->surveillance_off,

                    'surveillance_3_onsite' => $row->surveillance_on,
                    'surveillance_3_offsite' => $row->surveillance_off,

                    'surveillance_4_onsite' => $row->surveillance_on,
                    'surveillance_4_offsite' => $row->surveillance_off,

                    'surveillance_5_onsite' => $row->surveillance_on,
                    'surveillance_5_offsite' => $row->surveillance_off,

                    'reaudit_onsite' => $row->reaudit_on,
                    'reaudit_offsite' => $row->reaudit_off
                ];
            } else {
                $data = [
                    'stage_1_onsite' => $row->stage1_on,
                    'stage_1_offsite' => $row->stage1_off,

                    'stage_2_onsite' => $row->stage2_on,
                    'stage_2_offsite' => $row->stage2_off,

                    'surveillance_1_onsite' => $row->surveillance_on,
                    'surveillance_1_offsite' => $row->surveillance_off,

                    'surveillance_2_onsite' => $row->surveillance_on,
                    'surveillance_2_offsite' => $row->surveillance_off,

                    'reaudit_onsite' => $row->reaudit_on,
                    'reaudit_offsite' => $row->reaudit_off
                ];
            }


            return $data;
        } else {
            return [];
        }
    }

    public static function generateFoodSafetyTable($standard_id, $food_category_id = null, $haccp = null, $effective_employees = null, $management = '', $no_of_sites = 0, $frequency = '')
    {
        $tfte_value = FoodFamilySheet::where(function ($query) use ($effective_employees, $standard_id) {
            $query->where('standard_id', $standard_id);
            $query->whereRaw('? between TFTE_from and TFTE_to', (float)$effective_employees);

        })->first();


        $row = FoodFamilySheet::where(function ($query) use ($food_category_id, $standard_id) {
            $query->where('standard_id', $standard_id);
            $query->where('food_category_id', $food_category_id);
        })->first();

        if (!empty($row) && !empty($tfte_value)) {
            if ($management == 'yes') {
                $tms = 0;

            } else {
//                $tms = 0.25;
                $tms = 0;
            }


            if ($haccp > 1) {
                $th = $row->TH * ($haccp - 1);

                $ts = $row->TD + $th + $tms + $tfte_value->TFTE_value;  //ts
            } else {
                $th = 0;
                $ts = $row->TD + $th + $tms + $tfte_value->TFTE_value;  //ts
            }

            if ($no_of_sites > 1) {

                $additional_ts = $ts / 2 * ($no_of_sites - 1);

                $new_ts = $ts + $additional_ts;

            } else {

                $additional_ts = 0;
                $new_ts = $ts + $additional_ts;

//           $new_ts = $ts/2;  // 0 not divide 2
            }
//          $complexity = $request->complexity;e('TH', $haccp);
            if ($frequency == 'biannual') {
                return [
                    'stage_1_onsite' => round($new_ts * 0.3, 2),
                    'stage_1_offsite' => 0,
                    'stage_2_onsite' => round(($new_ts * 0.7), 2),
                    'stage_2_offsite' => 0,
                    'surveillance_1_onsite' => (round((($new_ts / 3) * 2) / 5, 2) >= 1) ? round((($new_ts / 3) * 2) / 5, 2) : 1,
                    'surveillance_1_offsite' => 0,
                    'surveillance_2_onsite' => (round((($new_ts / 3) * 2) / 5, 2) >= 1) ? round((($new_ts / 3) * 2) / 5, 2) : 1,
                    'surveillance_2_offsite' => 0,
                    'surveillance_3_onsite' => (round((($new_ts / 3) * 2) / 5, 2) >= 1) ? round((($new_ts / 3) * 2) / 5, 2) : 1,
                    'surveillance_3_offsite' => 0,
                    'surveillance_4_onsite' => (round((($new_ts / 3) * 2) / 5, 2) >= 1) ? round((($new_ts / 3) * 2) / 5, 2) : 1,
                    'surveillance_4_offsite' => 0,
                    'surveillance_5_onsite' => (round((($new_ts / 3) * 2) / 5, 2) >= 1) ? round((($new_ts / 3) * 2) / 5, 2) : 1,
                    'surveillance_5_offsite' => 0,
                    'reaudit_onsite' => round(($new_ts * 2) / 3, 2),
                    'reaudit_offsite' => 0,
                ];
            } else {
                return [
                    'stage_1_onsite' => round($new_ts * 0.3, 2),
                    'stage_1_offsite' => 0,
                    'stage_2_onsite' => round(($new_ts * 0.7), 2),
                    'stage_2_offsite' => 0,
                    'surveillance_1_onsite' => round($new_ts / 3, 2) >= 1 ? round($new_ts / 3, 2) : 1,
                    'surveillance_1_offsite' => 0,
                    'surveillance_2_onsite' => round($new_ts / 3, 2) >= 1 ? round($new_ts / 3, 2) : 1,
                    'surveillance_2_offsite' => 0,
                    'reaudit_onsite' => round(($new_ts * 2) / 3, 2),
                    'reaudit_offsite' => 0,
                ];
            }
        } else {
            return [];
        }
    }

    public static function generateEnergyTable($standard_id, $annual_energy, $energy_source, $energy_use, $effective_employees, $frequency = '')
    {
        //calcuting the value of C
        $c1 = self::calculateC1($annual_energy);
        $c2 = self::calculateC2($energy_source);
        $c3 = self::calculateC3($energy_use);
        $c = $c1 + $c2 + $c3;

        $enms = EnergyEnms::all();

        if ($c > $enms[0]->from && $c <= $enms[0]->to) {
            $EnMS_complexity = 'Low';
        } elseif ($c > $enms[1]->from && $c <= $enms[1]->to) {
            $EnMS_complexity = 'Medium';
        } elseif ($c > $enms[2]->from && $c <= $enms[2]->to) {
            $EnMS_complexity = 'High';
        } else {
            $EnMS_complexity = 'High';
        }


        $complexity = $EnMS_complexity;


        // session()->put('energy_complexity');

        $row = self::calcEnergyStageManday($standard_id, $effective_employees, $EnMS_complexity, $frequency);
        $type = 'energy';


        if ($frequency == 'biannual') {

            return [
                'stage_1_onsite' => $row->stage1_on,
                'stage_1_offsite' => $row->stage1_off,
                'stage_2_onsite' => $row->stage2_on,
                'stage_2_offsite' => $row->stage2_off,
                'surveillance_1_onsite' => $row->surveillance_on,
                'surveillance_1_offsite' => $row->surveillance_off,
                'surveillance_2_onsite' => $row->surveillance_on,
                'surveillance_2_offsite' => $row->surveillance_off,
                'surveillance_3_onsite' => $row->surveillance_on,
                'surveillance_3_offsite' => $row->surveillance_off,
                'surveillance_4_onsite' => $row->surveillance_on,
                'surveillance_4_offsite' => $row->surveillance_off,
                'surveillance_5_onsite' => $row->surveillance_off,
                'surveillance_5_offsite' => $row->surveillance_off,
                'reaudit_onsite' => $row->reaudit_on,
                'reaudit_offsite' => $row->reaudit_off,
            ];
        } else {

            return [
                'stage_1_onsite' => $row->stage1_on,
                'stage_1_offsite' => $row->stage1_off,
                'stage_2_onsite' => $row->stage2_on,
                'stage_2_offsite' => $row->stage2_off,
                'surveillance_1_onsite' => $row->surveillance_on,
                'surveillance_1_offsite' => $row->surveillance_off,
                'surveillance_2_onsite' => $row->surveillance_on,
                'surveillance_2_offsite' => $row->surveillance_off,
                'reaudit_onsite' => $row->reaudit_on,
                'reaudit_offsite' => $row->reaudit_off,
            ];
        }


    }

    protected static function calcEnergyStageManday($standard, $effective_employees, $EnMS_complexity, $frequency)
    {
        return ManDaySheet::with('standard')->where(function ($query) use ($standard, $effective_employees, $EnMS_complexity, $frequency) {
            $query->where('standard_id', $standard);
            $query->whereRaw("? between `effective_employes_on` and `effective_employes_off`", $effective_employees);
            $query->where('complexity', $EnMS_complexity);
            $query->where('frequency', $frequency);
        })->first();
    }

    protected static function calculateC1($annual_energy)
    {

        $sheet_data = EnergyFamilySheet::where(function ($query) use ($annual_energy) {
            $query->whereRaw("? between `from` and `to`", $annual_energy);
            $query->where('energy_code_id', 'C1');
        })->first();

        if (!is_null($sheet_data)) {
            $complexity = $sheet_data->complexity;

            $how_much_percent = EnergyWeightage::where('name', 'EC')->first();

            return (($complexity * $how_much_percent->weightage) / 100);
        } else {
            return 0;
        }


    }

    protected static function calculateC2($energy_source)
    {
        $sheet_data = EnergyFamilySheet::where(function ($query) use ($energy_source) {
            $query->whereRaw("? between `from` and `to`", $energy_source);
            $query->where('energy_code_id', 'C2');
        })->first();

        if (!is_null($sheet_data)) {
            $complexity = $sheet_data->complexity;

            $how_much_percent = EnergyWeightage::where('name', 'ES')->first();

            return (($complexity * $how_much_percent->weightage) / 100);
        } else {
            return 0;
        }

    }

    protected static function calculateC3($energy_use)
    {
        $sheet_data = EnergyFamilySheet::where(function ($query) use ($energy_use) {
            $query->whereRaw("? between `from` and `to`", $energy_use);
            $query->where('energy_code_id', 'C3');
        })->first();

        if (!is_null($sheet_data)) {
            $complexity = $sheet_data->complexity;

            $how_much_percent = EnergyWeightage::where('name', 'SEU')->first();

            return (($complexity * $how_much_percent->weightage) / 100);
        } else {
            return 0;
        }

    }

    public function generateIMSTable($standards = [], $complexities = [], $effective_employees = null, $frequency = '')
    {
        $i = 0;

        foreach ($standards as $standard) {
            $standard = Standards::findOrFail($standard);
            if (strtolower($standard->standardFamily->name) != 'food safety' || strtolower($standard->standardFamily->name) != 'energy management family') {
                $rows[] = ManDaySheet::where(function ($query) use ($effective_employees, $standard, $complexities, $i, $frequency) {
                    $query->where('standard_id', $standard->id);
                    $query->whereRaw('? between effective_employes_on and effective_employes_off', $effective_employees);
                    $query->where('complexity', $complexities[$i]);
                    $query->where('frequency', $frequency);
                })->get();
            }
            $i++;
        }

        if (!empty($rows)) {

            $stage1_on = 0;
            $stage1_off = 0;
            $stage2_on = 0;
            $stage2_off = 0;
            $surveillance_on = 0;
            $surveillance_off = 0;
            $reaudit_on = 0;
            $reaudit_off = 0;

            foreach ($rows as $row) {
                foreach ($row as $rec) {
                    $stage1_on += $rec->stage1_on;
                    $stage1_off += $rec->stage1_off;
                    $stage2_on += $rec->stage2_on;
                    $stage2_off += $rec->stage2_off;
                    $surveillance_on += $rec->surveillance_on;
                    $surveillance_off += $rec->surveillance_off;
                    $reaudit_on += $rec->reaudit_on;
                    $reaudit_off += $rec->reaudit_off;
                }
            }
            if ($frequency == 'biannual') {
                $data = [
                    'stage_1_onsite' => $stage1_on,
                    'stage_1_offsite' => $stage1_off,
                    'stage_2_onsite' => $stage2_on,
                    'stage_2_offsite' => $stage2_off,
                    'surveillance_1_onsite' => $surveillance_on,
                    'surveillance_1_offsite' => $surveillance_off,
                    'surveillance_2_onsite' => $surveillance_on,
                    'surveillance_2_offsite' => $surveillance_off,
                    'surveillance_3_onsite' => $surveillance_on,
                    'surveillance_3_offsite' => $surveillance_off,
                    'surveillance_4_onsite' => $surveillance_on,
                    'surveillance_4_offsite' => $surveillance_off,
                    'surveillance_5_onsite' => $surveillance_on,
                    'surveillance_5_offsite' => $surveillance_off,
                    'reaudit_onsite' => $reaudit_on,
                    'reaudit_offsite' => $reaudit_off
                ];
            } else {

                $data = [
                    'stage_1_onsite' => $stage1_on,
                    'stage_1_offsite' => $stage1_off,
                    'stage_2_onsite' => $stage2_on,
                    'stage_2_offsite' => $stage2_off,
                    'surveillance_1_onsite' => $surveillance_on,
                    'surveillance_1_offsite' => $surveillance_off,
                    'surveillance_2_onsite' => $surveillance_on,
                    'surveillance_2_offsite' => $surveillance_off,
                    'reaudit_onsite' => $reaudit_on,
                    'reaudit_offsite' => $reaudit_off
                ];
            }


            return $data;
        } else {
            return false;
        }

    }
}
