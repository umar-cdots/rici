<?php

namespace App\Traits;

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-08-18 16:45:15
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-02-19 17:58:18
 */
use Illuminate\Support\Facades\DB;

trait EnumValuesTrait
{
    public static function getEnumValues()
    {
    	$instance 	= new static;
        $fields 	= DB::connection($instance->connection)->select(
            DB::raw("SHOW COLUMNS FROM " . config('database.connections.' . $instance->connection . '.prefix') . $instance->getTable())
        );
        $result = [];
        foreach ($fields as $field) 
        {
            $enum = self::parseEnumValues($field->Type);
            if (!empty($enum))
                $result[$field->Field] = $enum;
        }
        return collect($result);

    }

    private static function parseEnumValues($type)
    {
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        if (empty($matches))
            return null;

        foreach (explode(',', $matches[1]) as $index => $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $index, $v);
        }
        return $enum;
    }
}