<?php 
namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

trait AccessRights {
    public $check_employee_rights;
    public $redirectUrl = null;
    
    public function VerifyRights() {
        $this->getAccRights();
   
        $check = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id . ' and name = "'.explode('/', url()->current())[3].'"')->first();
        if(!$check){
          
                DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id . ' and name = "company" ')->first() ? $this->redirectUrl = "company" : (DB::table('model_has_permissions')->whereRaw('model_id = '. Auth::user()->id)->first() ? $this->redirectUrl = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id)->first()->name : $this->redirectUrl = "logout" );
        
        }      
}


    public function getAccRights() {
        $this->check_employee_rights = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->where('model_id', Auth::user()->id)->get();
    }
}


?>