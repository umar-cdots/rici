<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait GeneralHelperTrait
{

    public function getFirstName(string $name)
    {
        $name = self::explodeName($name);

        return (isset($name[0])) ? $name[0] : '';
    }

    public function getMiddleName(string $name)
    {
        $name = self::explodeName($name);

        return (isset($name[1])) ? $name[1] : '';
    }

    public function getLastName(string $name)
    {
        $name = self::explodeName($name);

        return (isset($name[2])) ? $name[2] : '';
    }


    public static function explodeName(string $full_name)
    {
        $name = explode(' ', $full_name);

        return $name;
    }

    public function jobNumber($country_code): string
    {

        $year = Carbon::now()->format('y');
        $existing_job_number = DB::table('job_numbers')
            ->where('country_code', $country_code)
            ->where('year', $year)
            ->first();

        if ($existing_job_number) {
            $new_job_number = $existing_job_number->country_code . $existing_job_number->year . $existing_job_number->code;
            DB::table('job_numbers')
                ->where('country_code', $country_code)
                ->where('year', $year)
                ->update([
                    'country_code' => $country_code,
                    'year' => $year,
                    'code' => $this->code($existing_job_number->code)
                ]);

        } else {
            DB::table('job_numbers')
                ->insert([
                    'country_code' => $country_code,
                    'year' => $year,
                    'code' => '0002'
                ]);
            $new_job_number = $country_code . $year . '0001';
        }

        return $new_job_number;
    }

    public function code(string $code)
    {
        $convert_into_int = (int)$code;
        $incremented_int = $convert_into_int + 1;
        $integer_length = (int)log10($incremented_int) + 1;

        if ($integer_length === 1) {
            $new_code = '000' . $incremented_int;
        } elseif ($integer_length === 2) {
            $new_code = '00' . $incremented_int;
        } elseif ($integer_length === 3) {
            $new_code = '0' . $incremented_int;
        } else {
            $new_code = $incremented_int;
        }

        return $new_code;
    }
}
