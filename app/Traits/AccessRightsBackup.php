<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Permissions;

trait AccessRights
{
    public $check_employee_rights;
    public $redirectUrl = null;

    public function VerifyRights()
    {
        $this->getAccRights();
        $check = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = ' . Auth::user()->id . ' and name = "/' . explode('/', url()->current())[3] . '"')->first();

        if (!is_null($check)) {
            echo 'null';

        } else {
            $permission_id = \App\Permissions::pluck('id')->toArray();
            $modelHasPermission = \App\ModelHasPermissions::where([['permission_id', $permission_id], ['model_id', Auth::user()->id]])->leftJoin('permissions', 'model_has_permissions.permission_id', '=', 'permissions.id')->select('permissions.id', 'permissions.name')->first();
            if ($modelHasPermission) {
                \App\ModelHasPermissions::where('permission_id', $permission_id)->leftJoin('permissions', 'model_has_permissions.permission_id', '=', 'permissions.id')->select('permissions.name')->first();

            } else {


            }

        }

        //  DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id . ' and name = "company" ')->first() ? $this->redirectUrl = "company" : (DB::table('model_has_permissions')->whereRaw('model_id = '. Auth::user()->id)->first());   


        // $data =  DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id . ' and name = "company" ')->first() ? $this->redirectUrl = "company" : (DB::table('model_has_permissions')->whereRaw('model_id = '. Auth::user()->id)->first() ? $this->redirectUrl = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->whereRaw('model_id = '. Auth::user()->id)->first()->name : $this->redirectUrl = "logout" ); 
    }


    public function getAccRights()
    {
        $this->check_employee_rights = DB::table('model_has_permissions')->leftJoin('permissions', 'model_has_permissions.permission_id', 'permissions.id')->where('model_id', Auth::user()->id)->get();
    }
}


?>