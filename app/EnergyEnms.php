<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnergyEnms extends Model
{
    protected $table = 'energy_enms';
    protected $guarded = [];
}
