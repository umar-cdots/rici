<?php

/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2019-03-08 15:50:44
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-03-08 16:32:11
 */


// Home
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > Company
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('company', function ($trail) {
    $trail->parent('home');
    $trail->push('Company', route('company.index'));
});


// Home > OutSource Request
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('outsource-request', function ($trail) {
    $trail->parent('home');
    $trail->push('Outsource Request', route('outsource.index'));
});


// Home > Dashboard
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('dashboard', function ($trail) {
    $trail->parent('home');
    $trail->push('Dashboard', route('home'));
});

// Home > Scheme Manager
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('schemeManager', function ($trail) {
    $trail->parent('home');
    $trail->push('Scheme Manager', route('scheme-manager.index'));
});

// Home > Auditors
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('auditors', function ($trail) {
    $trail->parent('home');
    $trail->push('Auditors', route('auditors.index'));
});

// Home > Notifications
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('notifications', function ($trail) {
    $trail->parent('home');
    $trail->push('Notifications', route('notifications.index'));
});

// Home > Operation Coordinator
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('operationCoordinator', function ($trail) {
    $trail->parent('home');
    $trail->push('Operation Coordinator', route('operation-coordinator.index'));
});

// Home > Operation Manager
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('operationManager', function ($trail) {
    $trail->parent('home');
    $trail->push('Operation Manager', route('operation-manager.index'));
});

// Home > Permissions
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('permissions', function ($trail) {
    $trail->parent('home');
    $trail->push('Permissions', route('permissions.index'));
});

// Home > Reports
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('reports', function ($trail) {
    $trail->parent('home');
    $trail->push('Reports', route('reports.index'));
});

// Home > Roles
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('home');
    $trail->push('Roles', route('roles.index'));
});

// Home > Permissions Role
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('permission-to-role', function ($trail) {
    $trail->parent('home');
    $trail->push('Permissions Role', route('permissions.role.index'));
});

// Home > Scheme Coordinator
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('scheme-coordinator', function ($trail) {
    $trail->parent('home');
    $trail->push('Scheme Coordinator', route('scheme-coordinator.index'));
});

// Home > Audit Conducted
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('audit-conducted', function ($trail) {
    $trail->parent('home');
    $trail->push('Audit Conducted', route('no-of-audit-conducted'));
});

// Home > Auditor Evaluation Due Standard Wise
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('auditor-evaluation-due-standard-wise', function ($trail) {
    $trail->parent('home');
    $trail->push('Auditor Evaluation Due Standard Wise', route('auditor-evaluation-due-standard-wise'));
});

// Home > No of Auditor Standard wise
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-auditor-standard-wise', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Auditor Standard wise', route('no-of-auditor-standard-wise'));
});

// Home > No of Certified Clients
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-certified-clients', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Certified Clients', route('no-of-certified-clients'));
});

// Home > No of Certified Per Year
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-certified-per-year', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Certified Per Year', route('no-of-certified-per-year'));
});

// Home > No of Suspended Clients
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-suspended-clients', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Suspended Clients', route('no-of-suspended-clients'));
});

// Home > No of Technical Expert Standard Wise
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-technical-expert-standard-wise', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Technical Expert Standard Wise', route('no-of-technical-expert-standard-wise'));
});

// Home > No of Withdrawn Clients
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-withdrawn-clients', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Withdrawn Clients', route('no-of-withdrawn-clients'));
});

// Home > No of Withdrawn Per Year
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('no-of-withdrawn-per-year', function ($trail) {
    $trail->parent('home');
    $trail->push('No of Withdrawn Per Year', route('no-of-withdrawn-per-year'));
});

// Home > Technical Expert
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('technical-expert', function ($trail) {
    $trail->parent('home');
    $trail->push('Technical Experts', route('technical-expert.index'));
});

// Home > Users
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('users', function ($trail) {
    $trail->parent('home');
    $trail->push('Users', route('users.index'));
});


// Home > Profile
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('profile', function ($trail) {
    $trail->parent('home');
    $trail->push('Profile', route('profile.index'));
});

// Home > Documents
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('documents', function ($trail) {
    $trail->parent('home');
    $trail->push('Documents', route('document.index'));
});

// Home > Data-entry
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('data-entry', function ($trail) {
    $trail->parent('home');
    $trail->push('Data Entry', route('data.entry.index'));
});

// Home > Manday Calculator
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('man-day-calculator', function ($trail) {
    $trail->parent('home');
    $trail->push('Manday Calculator', route('mandaycalc.index'));
});


