<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('welcome.index');


Auth::routes(['register' => false]);

Route::get('/cache', function () {
//    \Illuminate\Support\Facades\Artisan::call('key:generate');
//    \Illuminate\Support\Facades\Artisan::call('vendor:publish --provider=Maatwebsite\Excel\ExcelServiceProvider');
//    \Illuminate\Support\Facades\Artisan::call('storage:link');
//    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
//    \Illuminate\Support\Facades\Artisan::call('migrate');

    return 'Commands run successfully.';
});

Route::middleware(['auth'])->group(function () {

    Route::get('/profile', 'ProfileController@index')->name('profile.index');
    Route::post('profile/update', 'ProfileController@update')->name('profile.update');
    Route::middleware(['role:super admin'])->group(function () {
        //Routes for managing permissions
        Route::get('/permissions', 'PermissionController@index')->name('permissions.index');
        Route::get('/permissions/create', 'PermissionController@create')->name('permissions.create');
        Route::post('/permissions', 'PermissionController@store')->name('permissions.store');
        Route::get('/permissions/{id}/edit', 'PermissionController@edit')->name('permissions.edit');
        Route::put('/permissions/{id}', 'PermissionController@update')->name('permissions.update');
        Route::delete('/permissions/destroy/{id}', 'PermissionController@destroy')->name('permissions.destroy');

        //Route for assigning permission to roles
        Route::get('/assign/permission/to/role', 'PermissionRoleController@index')->name('permissions.role.index');
        Route::get('/assign/permission/to/role/create', 'PermissionRoleController@create')->name('permissions.role.create');
        Route::post('/assign/permission/to/role', 'PermissionRoleController@store')->name('permissions.role.store');
        Route::get('/assign/permission/to/role/{id}/edit', 'PermissionRoleController@edit')->name('permissions.role.edit');
        Route::put('/assign/permission/to/role/{id}', 'PermissionRoleController@update')->name('permissions.role.update');


        //Routes for managing roles
        Route::get('/roles', 'RoleController@index')->name('roles.index');
        Route::get('/roles/create', 'RoleController@create')->name('roles.create');
        Route::post('/roles', 'RoleController@store')->name('roles.store');
        Route::get('/roles/{id}/edit', 'RoleController@edit')->name('roles.edit');
        Route::put('/roles/update/{id}', 'RoleController@update')->name('roles.update');
        Route::delete('/roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy');
    });


    Route::middleware(['role:super admin|scheme manager'])->group(function () {
        //Routes for scheme manager
        Route::get('/scheme-manager', 'SchemeManagerController@index')->name('scheme-manager.index');
        Route::get('/scheme-manager/create', 'SchemeManagerController@create')->name('scheme-manager.create')->middleware('permission:create_scheme_manager');
        Route::get('/scheme-manager/{id}/show', 'SchemeManagerController@show')->name('scheme-manager.show')->middleware('permission:show_scheme_manager');
        Route::post('/scheme-manager', 'SchemeManagerController@store')->name('scheme-manager.store')->middleware('permission:create_scheme_manager');
        Route::get('/scheme-manager/{id}/edit', 'SchemeManagerController@edit')->name('scheme-manager.edit')->middleware('permission:edit_scheme_manager');
        Route::post('/scheme-manager/update', 'SchemeManagerController@update')->name('scheme-manager.update')->middleware('permission:edit_scheme_manager');
        Route::delete('/scheme-manager/{id}', 'SchemeManagerController@destroy')->name('scheme-manager.destroy')->middleware('permission:delete_scheme_manager');
    });
    Route::middleware(['role:super admin|scheme manager|scheme coordinator'])->group(function () {
        //Routes for scheme coordinator
        Route::get('/scheme-coordinator', 'SchemeCoordinatorController@index')->name('scheme-coordinator.index');
        Route::get('/scheme-coordinator/create', 'SchemeCoordinatorController@create')->name('scheme-coordinator.create')->middleware('permission:create_scheme_coordinator');
        Route::get('/scheme-coordinator/{id}/show', 'SchemeCoordinatorController@show')->name('scheme-coordinator.show')->middleware('permission:show_scheme_coordinator');
        Route::post('/scheme-coordinator', 'SchemeCoordinatorController@store')->name('scheme-coordinator.store')->middleware('permission:create_scheme_coordinator');
        Route::get('/scheme-coordinator/{id}/edit', 'SchemeCoordinatorController@edit')->name('scheme-coordinator.edit')->middleware('permission:edit_scheme_coordinator');
        Route::post('/scheme-coordinator/update', 'SchemeCoordinatorController@update')->name('scheme-coordinator.update')->middleware('permission:edit_scheme_coordinator');
        Route::delete('/scheme-coordinator/{id}', 'SchemeCoordinatorController@destroy')->name('scheme-coordinator.destroy')->middleware('permission:delete_scheme-coordinator');
        Route::POST('/scheme-manager/standard', 'SchemeCoordinatorController@getSchemeManagerStandards')->name('get.scheme.manager.standard');

    });
    Route::middleware(['role:super admin|scheme manager|operation manager'])->group(function () {
        //Routes for operation manager
        Route::get('/operation-manager', 'OperationManagerController@index')->name('operation-manager.index');
        Route::get('/operation-manager/create', 'OperationManagerController@create')->name('operation-manager.create')->middleware('permission:create_operation_manager');
        Route::get('/operation-manager/{id}/show', 'OperationManagerController@show')->name('operation-manager.show')->middleware('permission:show_operation_manager');
        Route::post('/operation-manager', 'OperationManagerController@store')->name('operation-manager.store')->middleware('permission:create_operation_manager');
        Route::get('/operation-manager/{id}/edit', 'OperationManagerController@edit')->name('operation-manager.edit')->middleware('permission:edit_operation_manager');
        Route::post('/operation-manager/update', 'OperationManagerController@update')->name('operation-manager.update')->middleware('permission:edit_operation_manager');
        Route::delete('/operation-manager/{id}', 'OperationManagerController@destroy')->name('operation-manager.destroy')->middleware('permission:delete_operation_manager');
    });

    Route::middleware(['role:super admin|scheme manager|operation manager|operation coordinator'])->group(function () {
        //Route for operation coordinator
        Route::get('/operation-coordinator', 'OperationCoordinatorController@index')->name('operation-coordinator.index');
        Route::get('/operation-coordinator/create', 'OperationCoordinatorController@create')->name('operation-coordinator.create')->middleware('permission:create_operation_coordinator');
        Route::get('/operation-coordinator/{id}/show', 'OperationCoordinatorController@show')->name('operation-coordinator.show')->middleware('permission:show_operation_coordinator');
        Route::post('/operation-coordinator', 'OperationCoordinatorController@store')->name('operation-coordinator.store')->middleware('permission:create_operation_coordinator');
        Route::get('/operation-coordinator/{id}/edit', 'OperationCoordinatorController@edit')->name('operation-coordinator.edit')->middleware('permission:edit_operation_coordinator');
        Route::post('/operation-coordinator/update', 'OperationCoordinatorController@update')->name('operation-coordinator.update')->middleware('permission:edit_operation_coordinator');
        Route::delete('/operation-coordinator/{id}', 'OperationCoordinatorController@destroy')->name('operation-coordinator.destroy')->middleware('permission:delete_operation_coordinator');
        Route::post('/get/operation-manager/details', 'OperationCoordinatorController@getOperationManagerDetails')->name('getOperationManagerDetails');
    });
    //Routes for outsource request


    include(__DIR__ . '/web/outsourcerequest.php');
    //Routes for Man day calc.
    Route::get('/man-day-calculator', 'ManDayCalculatorController@index')->name('mandaycalc.index');
    Route::post('calculate/single', 'ManDayCalculatorController@calculateSingle')->name('calculate.single');

    Route::post('calculate/multiple', 'ManDayCalculatorController@calculateMultiple')->name('calculate.multiple');


    // Download PDF Request for Energy
    Route::post('download/single/standard', 'ManDayCalculatorController@downloadSingleStandardPDF')->name('download.single.standard');


    //Route for manday table data entry module
    Route::group(['prefix' => '/data-entry'], function () {
        Route::post('/man-day-table', 'ManDayTableController@importExcel')->name('mandaytable.excel');
        Route::post('/manday/sheet/filter', 'ManDayTableController@filterSheet')->name('mandaytable.filter');
        Route::post('/food-family', 'FoodFamilyController@importExcel')->name('foodfamily.excel');
        Route::post('/energy-family', 'EnergyFamilyController@importExcel')->name('energyfamily.excel');
        Route::post('/weightage', 'EnergyFamilyController@storeWeightage')->name('weightage.store');
    });

//Routes for Notifications
    Route::get('notifications', 'NotificationController@index')->name('notifications.index');
    Route::get('sent-notifications', 'NotificationController@index')->name('notifications.sent');
    Route::get('sent-notifications-others', 'NotificationController@sentOthers')->name('notifications.sent.others');
    Route::get('sent-notifications-post-audit', 'NotificationController@sendPostAudit')->name('notifications.sent.post.audit');
    Route::get('sent-notifications-post-audit-approved', 'NotificationController@sendPostAuditApproved')->name('notifications.sent.post.audit.approved');
    Route::get('received-notifications', 'NotificationController@received')->name('notifications.received');
    Route::get('received-notifications-others', 'NotificationController@receivedOthers')->name('notifications.received.others');
    Route::get('received-notifications-post-audit', 'NotificationController@receivedPostAudit')->name('notifications.received.post.audit');
    Route::get('received-notifications-post-audit-approved', 'NotificationController@receivedPostAuditApproved')->name('notifications.received.post.audit.approved');
    Route::delete('/notifications/{id}', 'NotificationController@destroy')->name('notification.destroy');


    /*including routes for Audit Justification module*/
    include(__DIR__ . '/web/auditjustification.php');

    /*including routes for Post Audit Justification module*/
    include(__DIR__ . '/web/postaudit.php');

//Routes for UserRoles
    Route::get('/users', 'UserRoleController@index')->name('users.index');
    Route::get('/users/assign/roles/{id}', 'UserRoleController@create')->name('users.assign.roles');
    Route::post('/users/assign/roles', 'UserRoleController@store')->name('users.assign.roles.store');

// Route for Dashboard Page

    Route::get('/home', 'DashboardController@index')->name('home');

    Route::middleware(['role:super admin|scheme manager|management'])->group(function () {

        Route::get('/no-of-auditor-standard-wise', 'DashboardController@noOfAuditors')->name('no-of-auditor-standard-wise');
        Route::get('/no-of-techical-expert-standard-wise', 'DashboardController@noOfTechnicalExperts')->name('no-of-technical-expert-standard-wise');
        Route::get('/auditor-evaluation-due-standard-wise', 'DashboardController@auditorEvaluationsDue')->name('auditor-evaluation-due-standard-wise');
        Route::post('/no-of-auditor-expert-standard-wise-graph', 'DashboardController@noOfAuditorsGraph')->name('no-of-auditor-expert-standard-wise-graph');
        Route::post('/no-of-technical-expert-standard-wise-graph', 'DashboardController@noOfTechnicalExpertGraph')->name('no-of-technical-expert-standard-wise-graph');
        Route::post('/auditor-evaluation-due-standard-wise-graph', 'DashboardController@auditorEvaluationDueGraph')->name('auditor-evaluation-due-standard-wise-graph');


        //Certified Clients
        Route::get('/no-of-certified-clients', 'DashboardController@certifiedClient')->name('no-of-certified-clients');
        Route::post('/no-of-certified-clients-graph', 'DashboardController@certifiedClientGraph')->name('no-of-certified-clients-graph');

        //Suspended Clients
        Route::get('/no-of-suspended-clients', 'DashboardController@suspendedClient')->name('no-of-suspended-clients');
        Route::post('/no-of-suspended-clients-graph', 'DashboardController@suspendedClientGraph')->name('no-of-suspended-clients-graph');

        //Withdrawn Clients
        Route::get('/no-of-withdrawn-clients', 'DashboardController@withdrawnClient')->name('no-of-withdrawn-clients');
        Route::post('/no-of-withdrawn-clients-graph', 'DashboardController@withdrawnClientGraph')->name('no-of-withdrawn-clients-graph');


        //Certified Clients Per Year
        Route::get('/no-of-certified-per-year', 'DashboardController@certifiedPerYear')->name('no-of-certified-per-year');
        Route::post('/no-of-certified-per-year-graph', 'DashboardController@certifiedPerYearGraph')->name('no-of-certified-per-year-graph');


        //Withdrawn Clients Per Year
        Route::get('/no-of-withdrawn-per-year', 'DashboardController@withdrawnPerYear')->name('no-of-withdrawn-per-year');
        Route::post('/no-of-withdrawn-per-year-graph', 'DashboardController@withdrawnPerYearGraph')->name('no-of-withdrawn-per-year-graph');

        Route::get('/no-of-audit-conducted', 'DashboardController@auditConducted')->name('no-of-audit-conducted');
        Route::post('/no-of-audit-conducted-graph', 'DashboardController@auditConductedGraph')->name('no-of-audit-conducted-graph');


    });


    Route::middleware(['role:super admin|scheme manager|scheme coordinator|operation manager|operation coordinator|management'])->group(function () {
        /*including routes for Company Module*/
        include(__DIR__ . '/web/company.php');
    });

    Route::middleware(['role:super admin|scheme manager|data entry operator'])->group(function () {
        /*including routes for dataentry module*/
        include(__DIR__ . '/web/dataentry.php');
    });


    Route::middleware(['role:super admin|scheme manager|scheme coordinator|operation manager|operation coordinator|auditor|management'])->group(function () {
        /*including routes for auditor module*/
        include(__DIR__ . '/web/auditor.php');
    });

    Route::middleware(['role:super admin|scheme manager|scheme coordinator|operation manager|operation coordinator|management'])->group(function () {

        /*including routes for technical-expert module*/
        include(__DIR__ . '/web/technicalexpert.php');
    });


    /*including routes for reporting module*/
    include(__DIR__ . '/web/reporting.php');

    /*including routes for invoice module*/
    include(__DIR__ . '/web/invoice.php');


    Route::middleware(['role:super admin|scheme manager|scheme coordinator|operation manager|operation coordinator|auditor|marketing|management'])->group(function () {
        /*Documents Routes Start*/
        Route::get('/documents', 'DocumentController@index')->name('document.index');
        Route::post('/documents/create', 'DocumentController@uploadDocument')->name('document.save')->middleware('permission:create_documents');
        Route::post('/documents/media/remove', 'DocumentController@removeMediaDocument')->name('documents.media.remove');

    });

    /*Documents Routes End*/


    //Routes for Filtering Sheets of Manday in data entry module
    Route::post('/data-entry/food-family/filter', 'FoodFamilyController@filter')->name('dataentry.foodfamily.filter');
    Route::post('/data-entry/energy-family/filter', 'EnergyFamilyController@filter')->name('dataentry.energyfamily.filter');

    Route::get('welcomeCertificate/{company_standard_id}/{company_standard_stage_id}', 'CertificatePdfController@welcome')->name('welcomeCertificate');
    Route::get('welcomeCertificate1/{company_standard_id}/{company_standard_stage_id}', 'CertificatePdfController@welcome2')->name('welcomeCertificate1');
    Route::get('suspensionCertificate/{company_standard_id}', 'CertificatePdfController@suspension')->name('suspensionCertificate');
    Route::get('withdrawCertificate/{company_standard_id}', 'CertificatePdfController@withdraw')->name('withdrawCertificate');
    Route::get('certificate-letter/{company_standard_id}/{company_standard_stage_id}', 'CertificatePdfController@certificate')->name('certificate-letter');
    Route::get('certificate-letter-others/{company_standard_id}/{company_standard_stage_id}', 'CertificatePdfController@certificateOthers')->name('certificate-letter-others');

//Ajax Routes
    Route::prefix('ajax')->name('ajax.')->group(function () {
        Route::get('region_countries', 'AjaxController@regionCountries')->name('regionCountries');
        Route::get('country_cities', 'AjaxController@countryCities')->name('countryCities');
        Route::get('country_cities_residential', 'AjaxController@countryCitiesResidentials')->name('countryCitiesResidentials');
        Route::get('resident_country_cities', 'AjaxController@residentCountryCities')->name('residentCountryCities');
        Route::get('company_name_exist', 'AjaxController@companyNameExist')->name('companyNameExist');
        Route::get('ias', 'AjaxController@ias')->name('iasByIAF');
        Route::post('company_progress', 'AjaxController@companyProgress')->name('companyProgressUpdate');
        Route::get('ip_info', 'AjaxController@ipInfo')->name('ipInfo');
        Route::get('region/countries', 'AjaxController@regionCountriesCities')->name('regionCountriesCities');
        Route::get('/foodCategoryByAuditorStandardId', 'AjaxController@foodCategory')->name('foodCategoryByAuditorStandardId');
        Route::get('/foodSubCategoryByFoodCategory', 'AjaxController@foodSubCategory')->name('foodSubCategoryByFoodCategory');
        Route::get('/families/standards', 'SchemeManagerController@getFamiliesStandards')->name('families.standards');
        Route::post('/upload/iaf/file', 'AjaxController@uploadIAFFile')->name('upload.iaf.file');


        Route::get('/foodCategoryByAccreditation', 'AjaxController@foodCategoryByAccreditation')->name('foodCategoryByAccreditation');
        Route::get('/energyByAccreditation', 'AjaxController@energyByAccreditation')->name('energyByAccreditation');


        Route::post('data-entry/save-iaf', 'AjaxController@saveDataEntryIAF')->name('data.entry.iaf.save')->middleware('permission:create_data_entry');
        Route::get('data-entry/delete-iaf/{model_id}', 'AjaxController@deleteDataEntryIAF')->name('data.entry.iaf.delete')->middleware('permission:delete_data_entry');
        Route::get('data-entry/iaf-code/status', 'AjaxController@updateDataEntryIAFStatus')->name('data.entry.iaf.status');
        Route::get('data-entry/edit-modal-iaf/{model_id}', 'AjaxController@getEditDataEntryIAFModal')->name('data.entry.iaf.edit.modal')->middleware('permission:edit_data_entry');
        Route::post('data-entry/update-iaf', 'AjaxController@updateDataEntryIAF')->name('data.entry.iaf.update')->middleware('permission:edit_data_entry');


        Route::post('data-entry/save-ias', 'AjaxController@saveDataEntryIAS')->name('data.entry.ias.save')->middleware('permission:create_data_entry');
        Route::get('data-entry/delete-ias/{model_id}', 'AjaxController@deleteDataEntryIAS')->name('data.entry.ias.delete')->middleware('permission:delete_data_entry');
        Route::get('data-entry/edit-modal-ias/{model_id}', 'AjaxController@getEditDataEntryIASModal')->name('data.entry.ias.edit.modal')->middleware('permission:edit_data_entry');
        Route::post('data-entry/update-ias', 'AjaxController@updateDataEntryIAS')->name('data.entry.ias.update')->middleware('permission:edit_data_entry');


        Route::get('/AccreditationByAuditorStandardId', 'AjaxController@accreditationByAuditorStandardId')->name('accreditationByAuditorStandardId');
        Route::post('/updateAuditorStandardStatus', 'AjaxController@updateAuditorStandardStatus')->name('updateAuditorStandardStatus');


        Route::get('/companyAccreditationByStandardId', 'AjaxController@companyAccreditationByStandardId')->name('companyAccreditationByStandardId');


        Route::get('/companyAccreditationByStandardIdCompanyId', 'AjaxController@companyAccreditationByStandardIdCompanyId')->name('companyAccreditationByStandardIdCompanyId');
        Route::get('/companyIafByAccreditation', 'AjaxController@companyIAFByAccreditation')->name('companyIafByAccreditation');


        Route::get('/mandayCalculatorStandardBasedFoodCode', 'AjaxController@mandayCalculatorStandardBasedFoodCode')->name('mandayCalculatorStandardBasedFoodCode');


        Route::get('/auditorStandaradInformation', 'AjaxController@auditorStandaradInformation')->name('auditorStandaradInformation');
        Route::get('/auditorStandaradTeam', 'AjaxController@auditorStandaradTeam')->name('auditorStandaradTeam');

        Route::post('/deletepermission', 'AjaxController@deletepermission')->name('deletepermission');
        Route::post('/notification/is-read', 'AjaxController@isRead')->name('notification.is-read');


        Route::get('/certificate-standard', 'CertificateController@index')->name('certificate.standard.index');
        Route::post('/certificate-standard/store', 'CertificateController@store')->name('certificate.standard.store');
        Route::post('/certificate-standard/delete', 'CertificateController@delete')->name('certificate.standard.delete');
        Route::post('/directory/company-standard/store', 'CertificateController@directory')->name('directory.company.standard.store');

        //get Ias against iaf code

        Route::get('/iascode', 'AjaxController@getIasCode')->name('getIas');
        Route::get('/iafcode', 'AjaxController@getIafCode')->name('getIaf');
        Route::get('/get-accreditation-by-stanadardid', 'AjaxController@getAccreditationByStanadardId')->name('getAccreditationByStanadardId');
        Route::get('/get/remaining/permissions', 'AjaxController@getRemainingPermissions')->name('get.remaining.permissions');


        //remove additional filese
        Route::get('/remove/additional/files', 'AjaxController@removeAdditionalFile')->name('remove.additionalfiles');


        //Get Standards for company Standard
        Route::get('/get/company/standard', 'AjaxController@getCompanyStandard')->name('get.standards');


//        Post Audit Question File

        Route::post('/post-audit/delete/question/file', 'AjaxController@deletePostAuditQuestionFile')->name('delete.post.audit.question');


        // Aj Approved Status for Reports

        Route::post('/aj-standard-stage/approved-status', 'AjaxController@approvedAjStandardStageStatus')->name('aj-standard-stage.approved.status');


        Route::get('/update-original-issue-date', 'AjaxController@updateOriginalIssueDate')->name('scheme_manager_post_audit.index');
        Route::post('/update-original-issue-date/store', 'AjaxController@updateOriginalIssueDateStore')->name('scheme_manager_post_audit.store');

        Route::get('/update/company-standard-remarks', 'AjaxController@companyStandardRemarks')->name('company_standard_remarks');
        Route::post('/update/company-standard-remarks/store', 'AjaxController@companyStandardRemarksStore')->name('company_standard_remarks.store');


        Route::get('update/notification/bell', 'AjaxController@updateNotificationBell')->name('update.notification.bell');
        Route::get('update/notification/bell/aj', 'AjaxController@updateNotificationBellAJ')->name('update.notification.bell.aj');
        Route::get('update/notification/bell/post-audit', 'AjaxController@updateNotificationBellPostAudit')->name('update.notification.bell.post.audit');

        Route::post('/company-standard/add-grade', 'AjaxController@addGradeToCompanyStandard')->name('company-standard.add-grade');
        Route::post('/company-standard/update/contract-price', 'AjaxController@companyStandardUpdateContractPrice')->name('company-standard.update.contract-price');


    });
});

