<?php


/*Technical Expert routes START*/

//Technical Expert start
Route::get('technical-expert', 'TechnicalExpertController@index')->name('technical-expert.index');
Route::post('technical-expert/finish', 'TechnicalExpertController@finish')->name('technical-expert.finish');
Route::get('technical-expert/create-technical-expert', 'TechnicalExpertController@create')->name('technical-expert.create')->middleware('permission:create_technical_experts');
Route::post('store-technical-expert', 'TechnicalExpertController@store')->name('technical-expert.store');
Route::get('technical-expert/show-technical-expert/{auditor_id}', 'TechnicalExpertController@show')->name('technical-expert.show')->middleware('permission:show_technical_experts');
Route::get('technical-expert/edit-technical-expert/{auditor_id}', 'TechnicalExpertController@edit')->name('technical-expert.edit')->middleware('permission:edit_technical_experts');
Route::get('delete-technical-expert/{auditor_id}', 'TechnicalExpertController@deleteTechnicalExpert')->name('technical-expert.delete')->middleware('permission:delete_technical_experts');
Route::get('update-technical-expert-status/{auditor_id}', 'TechnicalExpertController@updateTechnicalExpertStatus')->name('technical-expert.update.status');

//Technical Expert end


//Formal Education Routes Start
Route::get('/technical-expert/form-education-view/{auditor_id?}', 'TechnicalExpertController@getTechnicalExpertFormalEducationView')->name('technical-expert.formal.education.index');
Route::post('save-technical-expert-education', 'TechnicalExpertController@saveTechnicalExpertEducation')->name('technical-expert.education.save');
Route::get('get-technical-expert-education-modal/{education_id}', 'TechnicalExpertController@getEditTechnicalExpertEducationModal')->name('technical-expert.education.edit.modal');
Route::post('update-technical-expert-education', 'TechnicalExpertController@updateTechnicalExpertEducation')->name('technical-expert.education.update');
Route::get('delete-technical-expert-education/{education_id}', 'TechnicalExpertController@deleteTechnicalExpertEducation')->name('technical-expert.education.delete');
//Formal Education Routes End


//Confidentially Agreement Routes Start
Route::get('technical-expert-agreement-view/{auditor_id?}', 'TechnicalExpertController@getTechnicalExpertAgreementView')->name('technical-expert.agreement.index');
Route::post('technical-expert/save-technical-expert-agreement', 'TechnicalExpertController@saveTechnicalExpertAgreement')->name('technical-expert.confidentially-agreement.save');
Route::get('delete-technical-expert-agreement/{document_id}', 'TechnicalExpertController@deleteTechnicalExpertAgreement')->name('technical-expert.agreement.delete');
Route::get('get-technical-expert-agreement-modal/{agreement_id}', 'TechnicalExpertController@getEditTechnicalExpertAgreementModal')->name('technical-expert.agreement.edit.modal');
Route::post('update-technical-expert-agreement', 'TechnicalExpertController@updateTechnicalExpertAgreement')->name('technical-expert.agreement.update');
//Confidentially Agreement Routes End


/*Technical Expert Standards Routes*/
Route::get('get-add-technical-expert-standards-modal/{auditor_id}', 'TechnicalExpertStandardController@getAddTechnicalExpertStandardsModal')->name('technical-expert.standards.modal.index');
Route::post('save-technical-expert-standards', 'TechnicalExpertStandardController@saveTechnicalExpertStandards')->name('technical-expert.standards.save');
Route::get('get-technical-expert-standards-tabs-view/{auditor_id}', 'TechnicalExpertStandardController@getTechnicalExpertStandardsTabs')->name('technical-expert.standards.view');


/*Auditor Standards Grades Routes*/
Route::get('technical-expert--standard-grades/{auditor_standard_id?}', 'TechnicalExpertStandardController@technicalExpertStandardGradesView')->name('technical-expert.standards.grades.index');
Route::post('save-technical-expert-standard-grade', 'TechnicalExpertStandardController@saveTechnicalExpertStandardGrade')->name('technical-expert.standards.grade.save');
Route::get('delete-technical-expert-standard-grade/{audStdGradeId}', 'TechnicalExpertStandardController@deleteTechnicalExpertStandardGrade')->name('technical-expert.standards.grade.delete');
Route::get('get-technical-expert-standard-grade-modal/{audStdGradeId}', 'TechnicalExpertStandardController@getEditTechnicalExpertStdGradeModal')->name('technical-expert.standards.grade.edit.modal');
Route::post('update-technical-expert-standard-grade', 'TechnicalExpertStandardController@updateTechnicalExpertStandardGrade')->name('technical-expert.standards.grade.update');
Route::post('remove-technical-expert-standard-grade', 'TechnicalExpertStandardController@removeTechnicalExpertStandardGrade')->name('technical-expert.standards.grade.remove');


//
///*Technical Expert Standards Witness Evaluation routes*/
//Route::get('technical-expert-standard-witness-eval/{auditor_standard_id?}', 'TechnicalExpertStandardController@technicalExpertStandardWitnessEvalView')->name('technical-expert.standards.witness.eval.index');
//Route::post('save-technical-expert-standard-witness-eval', 'TechnicalExpertStandardController@saveTechnicalExpertStandardWitnessEval')->name('technical-expert.standards.witness.eval.save');
//Route::get('delete-technical-expert-standard-witness-eval/{audStdWitnessEvalId}', 'TechnicalExpertStandardController@deleteTechnicalExpertStandardWitnessEval')->name('technical-expert.standards.witness.eval.delete');
//Route::get('get-technical-expert-standard-witness-eval-modal/{audStdWitnessEvalId}', 'TechnicalExpertStandardController@getEditTechnicalExpertStdWitnessEvalModal')->name('technical-expert.standards.witness.eval.edit.modal');
//Route::post('update-technical-expert-standard-witness-eval', 'TechnicalExpertStandardController@updateTechnicalExpertStandardWitnessEval')->name('technical-expert.standards.witness.eval.update');
///*Technical Expert routes END*/


/*Technical Expert Standards Codes routes*/
Route::get('technical-expert-standard-codes/{auditor_standard_id?}', 'TechnicalExpertStandardController@technicalExpertStandardCodesView')->name('technical-expert.standards.codes.index');
Route::post('save-technical-expert-standard-code', 'TechnicalExpertStandardController@saveTechnicalExpertStandardCode')->name('technical-expert.standards.code.save');
Route::get('delete-technical-expert-standard-code/{model_id}', 'TechnicalExpertStandardController@deleteTechnicalExpertStandardCode')->name('technical-expert.standards.code.delete');
Route::get('get-technical-expert-standard-code-modal/{model_id}', 'TechnicalExpertStandardController@getEditCodesModal')->name('technical-expert.standards.code.edit.modal');
Route::post('update-technical-expert-standard-code', 'TechnicalExpertStandardController@updateTechnicalExpertStandardCode')->name('technical-expert.standards.code.update');

/*Technical Expert Standards Food Codes routes*/
Route::get('technical-expert-standard-food-codes/{auditor_standard_id?}', 'TechnicalExpertStandardController@technicalExpertStandardFoodCodesView')->name('technical-expert.standards.food.codes.index');
Route::post('save-technical-expert-standard-food-code', 'TechnicalExpertStandardController@saveTechnicalExpertStandardFoodCode')->name('technical-expert.standards.food.code.save');
Route::get('delete-technical-expert-standard-food-code/{model_id}', 'TechnicalExpertStandardController@deleteTechnicalExpertStandardFoodCode')->name('technical-expert.standards.food.code.delete');
Route::get('get-technical-expert-standard-food-code-modal/{model_id}', 'TechnicalExpertStandardController@getEditFoodCodesModal')->name('technical-expert.standards.food.code.edit.modal');
Route::post('update-technical-expert-standard-food-code', 'TechnicalExpertStandardController@updateTechnicalExpertStandardFoodCode')->name('technical-expert.standards.food.code.update');


/*Technical Expert Standards Energy Codes routes*/
Route::get('technical-expert-standard-energy-codes/{auditor_standard_id?}', 'TechnicalExpertStandardController@technicalExpertStandardEnergyCodesView')->name('technical-expert.standards.energy.codes.index');
Route::post('save-technical-expert-standard-energy-code', 'TechnicalExpertStandardController@saveTechnicalExpertStandardEnergyCode')->name('technical-expert.standards.energy.code.save');
Route::get('delete-technical-expert-standard-energy-code/{model_id}', 'TechnicalExpertStandardController@deleteTechnicalExpertStandardEnergyCode')->name('technical-expert.standards.energy.code.delete');
Route::get('get-technical-expert-standard-energy-code-modal/{model_id}', 'TechnicalExpertStandardController@getEditEnergyCodesModal')->name('technical-expert.standards.energy.code.edit.modal');
Route::post('update-technical-expert-standard-energy-code', 'TechnicalExpertStandardController@updateTechnicalExpertStandardEnergyCode')->name('technical-expert.standards.energy.code.update');


/*Auditor Standards Notification routes*/
Route::get('technical-expert-standard-notification', 'TechnicalExpertStandardController@technicalExpertStandardNotification')->name('technical-expert.getNotifications');
Route::post('technical-expert/send-for-approval', 'TechnicalExpertController@sendForApproval')->name('technical-expert.send.for.approval');
