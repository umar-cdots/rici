<?php
Route::resource('company', 'CompanyController');
Route::post('/company/update', 'CompanyController@update')->name('company.update');
Route::post('/company/send/to/ias', 'CompanyController@sendToIAS')->name('company.send.to.ias');
Route::post('/company/standard', 'CompanyController@viewStandard')->name('company.standard');
Route::post('/company/media/remove', 'CompanyController@removeMediaDocument')->name('company.media.remove');
Route::get('company/{id}/continue', 'CompanyController@continue')->name('company.continue');


Route::post('company/check/standard', 'CompanyController@checkStandard')->name('company.checkStandard');

Route::post('company/check/standard/approved', 'CompanyController@checkStandardApproved')->name('company.checkStandardApproved');





/*Company Standards Codes routes*/
Route::get('company-standard-codes', 'CompanyStandardController@companyStandardCodesView')->name('company.standards.codes.index');
Route::post('save-company-standard-code', 'CompanyStandardController@saveCompanyStandardCode')->name('company.standards.code.save');
Route::get('get-company-standard-code-modal/{model_id}', 'CompanyStandardController@getEditCodesModal')->name('company.standards.code.edit.modal');
Route::post('update-company-standard-code', 'CompanyStandardController@updateCompanyStandardCode')->name('company.standards.code.update');
Route::get('delete-company-standard-code/{model_id}', 'CompanyStandardController@deleteCompanyStandardCode')->name('company.standards.code.delete');


/*Company Standards Food Codes routes*/
Route::get('company-standard-food-codes', 'CompanyStandardController@companyStandardFoodCodesView')->name('company.standards.food.codes.index');
Route::post('save-company-standard-food-code', 'CompanyStandardController@saveCompanyStandardFoodCode')->name('company.standards.food.code.save');
Route::get('delete-company-standard-food-code/{model_id}', 'CompanyStandardController@deleteCompanyStandardFoodCode')->name('company.standards.food.code.delete');
Route::get('get-company-standard-food-code-modal/{model_id}', 'CompanyStandardController@getEditFoodCodesModal')->name('company.standards.food.code.edit.modal');
Route::post('update-company-standard-food-code', 'CompanyStandardController@updateCompanyStandardFoodCode')->name('company.standards.food.code.update');


/*Company Standards Energy Codes routes*/
Route::get('company-standard-energy-codes', 'CompanyStandardController@companyStandardEnergyCodesView')->name('company.standards.energy.codes.index');
Route::post('save-company-standard-energy-code', 'CompanyStandardController@saveCompanyStandardEnergyCode')->name('company.standards.energy.code.save');
Route::get('delete-company-standard-energy-code/{model_id}', 'CompanyStandardController@deleteCompanyStandardEnergyCode')->name('company.standards.energy.code.delete');
Route::get('get-company-standard-energy-code-modal/{model_id}', 'CompanyStandardController@getEditEnergyCodesModal')->name('company.standards.energy.code.edit.modal');
Route::post('update-company-standard-energy-code', 'CompanyStandardController@updateCompanyStandardEnergyCode')->name('company.standards.energy.code.update');

Route::post('/store-ims-standards', 'CompanyStandardController@storeImsStandards')->name('store.ims.standard');

