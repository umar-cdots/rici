<?php


Route::get('reports', 'ReportingController@index')->name('reports.index');
Route::any('reports/auditors/list', 'ReportingController@auditorsList')->name('reports.auditors.list');


//List of Audit Routes
Route::prefix('reports')->group(function () {

    Route::get('/list-of-auditor', 'Reports\ListOfAuditorController')->name('reports.list-of-index');


    //routes for auditor food reports
    Route::get('/auditor-food', 'Reports\AuditorFoodController')->name('reports.auditor-food');
    Route::get('/get-food-auditor-of-country', 'Reports\ReportAjaxController@CountryFoodAuditors')->name('food.auditor.country');

    //route for auditor evaluation schedule
    Route::get('/auditor-evaluation-schedule', 'Reports\AuditorEvaluationScheduleController')->name('reports.auditor-evaluation-schedule');
    Route::get('/get-auditor-standard', 'Reports\ReportAjaxController@getAuditorStandards')->name('get.auditor.standard');


    //Technical Expert List
    Route::get('/list-of-technical-expert', 'Reports\ListOfTechnicalExpertController')->name('reports.list-of-technical-expert-index');
    Route::get('/list-of-technical-expert-with-approval-date', 'Reports\ListOfTechnicalExpertController@listOfTechnicalExpertWithApprovalDate')->name('reports.list-of-technical-expert-with-approval-date');


    //routes for auditor energy reports
    Route::get('/auditor-energy', 'Reports\AuditorEnergyController')->name('reports.auditor-energy');
    Route::get('/get-energy-auditor-of-country', 'Reports\ReportAjaxController@CountryEnergyAuditors')->name('energy.auditor.country');


    //    Company Standard Reports

    //Generic Company Standard Report
    Route::get('/list-of-generic-company-standard', 'Reports\GenericCompanyStandardController')->name('reports.list-of-generic-company-standard-index');


    //Food Company Standard Report
    Route::get('/list-of-food-company-standard', 'Reports\FoodCompanyStandardController')->name('reports.list-of-food-company-standard-index');


    //Energy Company Standard Report
    Route::get('/list-of-energy-company-standard', 'Reports\EnergyCompanyStandardController')->name('reports.list-of-energy-company-standard-index');


    //List of Companies Report
    Route::get('/list-of-companies', 'Reports\ListOfCompanyController')->name('reports.list-of-companies-index');


    //List of Certificates Post Report
    Route::get('/list-of-certificates', 'Reports\ListOfCertificateController@index')->name('reports.list-of-certificates-index');

    //List of Audit Planning Sheet Report
    Route::get('/audit-planning-sheet', 'Reports\AuditPlanningSheetController')->name('reports.audit-planning-sheet-index');

    //List of Certificate Not Uploaded Report
    Route::get('/certificate-not-uploaded', 'Reports\AuditPlanningSheetController@certificateNotUploaded')->name('reports.certificate-not-uploaded-index');

    //List of Audit surveillance-and-reaudit-due
    Route::get('/surveillance-and-reaudit-due', 'Reports\SurveillanceAndReauditDueController')->name('reports.surveillance-and-reaudit-due-index');

    //List of Audit surveillance-and-reaudit-due
    Route::get('/surveillance-and-reaudit-due-index-with-actual-audit', 'Reports\SurveillanceAndReauditDueController@newReport')->name('reports.surveillance-and-reaudit-due-index-with-actual-audit');

    //List of Audit surveillance-and-reaudit-due withdrawn report
    Route::get('/surveillance-and-reaudit-due-withdrawn-with-actual-audit', 'Reports\SurveillanceAndReauditDueController@withdrawnReport')->name('reports.surveillance-and-reaudit-due-withdrawn-with-actual-audit');


    //Audit Justification Not Certified Clients
    Route::get('/audit-justification-not-certified-clients', 'Reports\SurveillanceAndReauditDueController@notCertifiedClients')->name('reports.audit-justification-not-certified-clients');

    //routes for auditor energy reports
    Route::get('/auditor-log', 'Reports\AuditorLogController')->name('reports.auditor-log');

    //List of Company Job Number Report
    Route::get('/company-job-number', 'Reports\CompanyJobNumberController')->name('reports.company-job-number-index');

    //List of Audit surveillance-and-reaudit-due Conducted Report
    Route::get('/surveillance-and-reaudit-due-index-with-actual-audit-conducted_report', 'Reports\SurveillanceAndReauditDueController@newReportApprovedTrue')->name('reports.surveillance-and-reaudit-due-index-with-actual-audit-conducted-report');
    Route::get('/surveillance-and-reaudit-due-index-with-actual-audit-conducted-special-report', 'Reports\SurveillanceAndReauditDueController@newReportApprovedTrueSpecial')->name('reports.surveillance-and-reaudit-due-index-with-actual-audit-conducted-special-report');


    //List of Stats Count
    Route::get('/statistics/count', 'Reports\StatisticsController@statisticsCount')->name('reports.statistics.count');
    Route::get('/statistics/error', 'Reports\StatisticsController@statisticsError')->name('reports.statistics.error');
    Route::get('/statistics/success', 'Reports\StatisticsController@statisticsSuccess')->name('reports.statistics.success');
    //Invoice Report
    Route::get('/invoice-and-letters-report', 'Reports\InvoiceController@index')->name('reports.invoice.and.letter.index');


});
