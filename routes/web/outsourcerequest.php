<?php
Route::get('/outsource-request', 'OutsourceRequestController@index')->name('outsource.index')->middleware('permission:show_outsource_request');
Route::get('/outsource-request/create', 'OutsourceRequestController@create')->name('outsource.create')->middleware('permission:create_outsource_request');
Route::post('/search-auditor', 'OutsourceRequestController@searchAuditor')->name('outsource.search.auditor');
Route::post('/search-ims-auditor', 'OutsourceRequestController@searchIMSAuditor')->name('outsource.search.auditor.ims');
Route::get('/outsource-request/{id}/show', 'OutsourceRequestController@show')->name('outsource.show')->middleware('permission:show_outsource_request');

//outsource request submittion
Route::post('/outsource-request/store', 'OutsourceRequestController@makeOutsourceRequest')->name('outsource.request.store')->middleware('permission:create_outsource_request');
Route::post('/outsource-request/multi-standard-store', 'OutsourceRequestController@makeMultiStandardOutsourceRequest')->name('multi.outsource.request.store')->middleware('permission:create_outsource_request');
Route::post('/outsource-request/update', 'OutsourceRequestController@outSourceRequestApprovedOrReject')->name('outsource.request.update')->middleware('permission:edit_outsource_request');

