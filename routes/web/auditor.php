<?php
/*Auditor routes START*/

Route::get('auditors', 'AuditorController@index')->name('auditors.index');
Route::post('auditors/finish', 'AuditorController@finish')->name('auditors.finish');
Route::get('auditors/create-auditor', 'AuditorController@create')->name('auditors.create')->middleware('permission:create_auditors');
Route::post('store-auditor', 'AuditorController@store')->name('auditors.store');
Route::get('auditors/show-auditor/{auditor_id}', 'AuditorController@show')->name('auditors.show')->middleware('permission:show_auditors');
Route::get('auditors/edit-auditor/{auditor_id}', 'AuditorController@edit')->name('auditors.edit')->middleware('permission:edit_auditors');
Route::get('delete-auditor/{auditor_id}', 'AuditorController@deleteAuditor')->name('auditors.delete')->middleware('permission:delete_auditors');
Route::get('update-auditor-status/{auditor_id}', 'AuditorController@updateAuditorStatus')->name('auditors.update.status');

Route::get('form-education-view/{auditor_id?}', 'AuditorController@getAuditorFormalEducationView')->name('auditors.formal.education.index');
Route::post('save-auditor-education', 'AuditorController@saveAuditorEducation')->name('auditors.education.save');
Route::get('get-auditor-education-modal/{education_id}', 'AuditorController@getEditAuditorEducationModal')->name('auditors.education.edit.modal');
Route::post('update-auditor-education', 'AuditorController@updateAuditorEducation')->name('auditors.education.update');
Route::get('delete-auditor-education/{education_id}', 'AuditorController@deleteAuditorEducation')->name('auditors.education.delete');
Route::get('auditor-documents/{auditor_id?}', 'AuditorController@getAuditorDocumentsView')->name('auditors.documents.index');
Route::post('save-auditor-document', 'AuditorController@saveAuditorDocument')->name('auditors.documents.save');
Route::get('delete-auditor-document/{document_id}', 'AuditorController@deleteAuditorDocument')->name('auditors.document.delete');
Route::get('get-auditor-document-modal/{document_id}', 'AuditorController@getEditAuditorDocumentModal')->name('auditors.document.edit.modal');
Route::post('update-auditor-document', 'AuditorController@updateAuditorDocument')->name('auditors.document.update');
Route::get('auditor-agreement-view/{auditor_id?}', 'AuditorController@getAuditorAgreementView')->name('auditors.agreement.index');
Route::get('auditor-emp-history-view/{auditor_id?}', 'AuditorController@getAuditorEmpHistoryView')->name('auditors.emp.history.index');
Route::post('save-auditor-agreement', 'AuditorController@saveAuditorAgreement')->name('auditors.agreement.save');
Route::get('delete-auditor-agreement/{document_id}', 'AuditorController@deleteAuditorAgreement')->name('auditors.agreement.delete');
Route::get('get-auditor-agreement-modal/{agreement_id}', 'AuditorController@getEditAuditorAgreementModal')->name('auditors.agreement.edit.modal');
Route::post('update-auditor-agreement', 'AuditorController@updateAuditorAgreement')->name('auditors.agreement.update');
Route::post('save-auditor-emp-history', 'AuditorController@saveAuditorEmpHistory')->name('auditors.emp.history.save');
Route::get('delete-auditor-emp-history/{document_id}', 'AuditorController@deleteAuditorEmpHistory')->name('auditors.emp.history.delete');
Route::get('get-auditor-emp-history-modal/{employment_id}', 'AuditorController@getEditAuditorEmploymentHistoryModal')->name('auditors.emp.history.edit.modal');
Route::post('update-auditor-emp-history', 'AuditorController@updateAuditorEmpHistory')->name('auditors.emp.history.update');


/*Auditor Standards Routes*/
Route::get('get-add-auditor-standards-modal/{auditor_id}', 'AuditorStandardController@getAddAuditorStandardsModal')->name('auditors.standards.modal.index');
Route::post('save-auditor-standards', 'AuditorStandardController@saveAuditorStandards')->name('auditors.standards.save');
Route::get('get-auditor-standards-tabs-view/{auditor_id}', 'AuditorStandardController@getAuditorStandardsTabs')->name('auditors.standards.view');

/*Auditor Standards Training Course Routes*/
Route::get('auditor-standard-training-courses/{auditor_standard_id?}', 'AuditorStandardController@auditorStandardTrainingCoursesView')->name('auditors.standards.training.index');
Route::post('save-auditor-standard-training-courses', 'AuditorStandardController@saveAuditorStandardTrainingCourse')->name('auditors.standards.training.save');
Route::get('delete-auditor-standard-training-course/{audStdTrainingCourseId}', 'AuditorStandardController@deleteAuditorStandardTrainingCourse')
    ->name('auditors.standards.training.delete');
Route::get('get-auditor-training-course-modal/{audStdTrainingCourseId}', 'AuditorStandardController@getEditTrainingCourseModal')->name('auditors.standards.training.edit.modal');
Route::post('update-auditor-standard-training-courses', 'AuditorStandardController@updateAuditorStandardTrainingCourse')->name('auditors.standards.training.update');

/*Auditor Standards Grades Routes*/
Route::get('auditor-standard-grades/{auditor_standard_id?}', 'AuditorStandardController@auditorStandardGradesView')->name('auditors.standards.grades.index');
Route::post('save-auditor-standard-grade', 'AuditorStandardController@saveAuditorStandardGrade')->name('auditors.standards.grade.save');
Route::get('delete-auditor-standard-grade/{audStdGradeId}', 'AuditorStandardController@deleteAuditorStandardGrade')->name('auditors.standards.grade.delete');
Route::get('get-auditor-standard-grade-modal/{audStdGradeId}', 'AuditorStandardController@getEditAuditorStdGradeModal')->name('auditors.standards.grade.edit.modal');
Route::post('update-auditor-standard-grade', 'AuditorStandardController@updateAuditorStandardGrade')->name('auditors.standards.grade.update');
Route::post('remove-auditor-standard-grade', 'AuditorStandardController@removeAuditorStandardGrade')->name('auditors.standards.grade.remove');


/*Auditor Standards Witness Evaluation routes*/
Route::get('auditor-standard-witness-eval/{auditor_standard_id?}', 'AuditorStandardController@auditorStandardWitnessEvalView')->name('auditors.standards.witness.eval.index');
Route::post('save-auditor-standard-witness-eval', 'AuditorStandardController@saveAuditorStandardWitnessEval')->name('auditors.standards.witness.eval.save');
Route::get('delete-auditor-standard-witness-eval/{audStdWitnessEvalId}', 'AuditorStandardController@deleteAuditorStandardWitnessEval')->name('auditors.standards.witness.eval.delete');
Route::get('get-auditor-standard-witness-eval-modal/{audStdWitnessEvalId}', 'AuditorStandardController@getEditAuditorStdWitnessEvalModal')->name('auditors.standards.witness.eval.edit.modal');
Route::post('update-auditor-standard-witness-eval', 'AuditorStandardController@updateAuditorStandardWitnessEval')->name('auditors.standards.witness.eval.update');
/*Auditor routes END*/


/*Auditor Standards Codes routes*/
Route::get('auditor-standard-codes/{auditor_standard_id?}', 'AuditorStandardController@auditorStandardCodesView')->name('auditors.standards.codes.index');
Route::post('save-auditor-standard-code', 'AuditorStandardController@saveAuditorStandardCode')->name('auditors.standards.code.save');
Route::get('delete-auditor-standard-code/{model_id}', 'AuditorStandardController@deleteAuditorStandardCode')->name('auditors.standards.code.delete');
Route::get('get-auditor-standard-code-modal/{model_id}', 'AuditorStandardController@getEditCodesModal')->name('auditors.standards.code.edit.modal');
Route::post('update-auditor-standard-code', 'AuditorStandardController@updateAuditorStandardCode')->name('auditors.standards.code.update');

/*Auditor Standards Food Codes routes*/
Route::get('auditor-standard-food-codes/{auditor_standard_id?}', 'AuditorStandardController@auditorStandardFoodCodesView')->name('auditors.standards.food.codes.index');
Route::post('save-auditor-standard-food-code', 'AuditorStandardController@saveAuditorStandardFoodCode')->name('auditors.standards.food.code.save');
Route::get('delete-auditor-standard-food-code/{model_id}', 'AuditorStandardController@deleteAuditorStandardFoodCode')->name('auditors.standards.food.code.delete');
Route::get('get-auditor-standard-food-code-modal/{model_id}', 'AuditorStandardController@getEditFoodCodesModal')->name('auditors.standards.food.code.edit.modal');
Route::post('update-auditor-standard-food-code', 'AuditorStandardController@updateAuditorStandardFoodCode')->name('auditors.standards.food.code.update');


/*Auditor Standards Energy Codes routes*/
Route::get('auditor-standard-energy-codes/{auditor_standard_id?}', 'AuditorStandardController@auditorStandardEnergyCodesView')->name('auditors.standards.energy.codes.index');
Route::post('save-auditor-standard-energy-code', 'AuditorStandardController@saveAuditorStandardEnergyCode')->name('auditors.standards.energy.code.save');
Route::get('delete-auditor-standard-energy-code/{model_id}', 'AuditorStandardController@deleteAuditorStandardEnergyCode')->name('auditors.standards.energy.code.delete');
Route::get('get-auditor-standard-energy-code-modal/{model_id}', 'AuditorStandardController@getEditEnergyCodesModal')->name('auditors.standards.energy.code.edit.modal');
Route::post('update-auditor-standard-energy-code', 'AuditorStandardController@updateAuditorStandardEnergyCode')->name('auditors.standards.energy.code.update');


/*Auditor Standards Notification routes*/
Route::get('auditor-standard-notification', 'AuditorStandardController@auditorStandardNotification')->name('auditors.getNotifications');
Route::post('auditors/send-for-approval', 'AuditorController@sendForApproval')->name('auditors.send.for.approval');



