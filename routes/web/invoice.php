<?php

//List of Invoice Routes
Route::prefix('reports')->group(function () {

    Route::get('/invoice/letter/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}', 'Invoice\InvoiceController@index')->name('invoice.letter.index');
    Route::get('/invoice/letter/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}/{letter_type}', 'Invoice\InvoiceController@letterCreate')->name('invoice.letter.create');
    Route::post('/invoice/letter/paid/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}', 'Invoice\InvoiceController@letterPaid')->name('invoice.letter.paid');
    Route::post('/invoice/letter/store/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}/{letter_type}', 'Invoice\InvoiceController@letterStore')->name('invoice.letter.store');
    Route::get('/invoice/letter/edit/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}/{letter_type}/{invoice_id}/{type}', 'Invoice\InvoiceController@letterEdit')->name('invoice.letter.edit');
    Route::get('/invoice/letter/print/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}/{letter_type}/{invoice_id}', 'Invoice\InvoiceController@printLetter')->name('invoice.letter.print');
    Route::post('/invoice/send/email', 'Invoice\InvoiceController@sendInvoiceEmail')->name('invoice.letter.email.sent');

    Route::post('/invoice/letter/update/{company_id}/{company_standard_id}/{aj_standard_id}/{aj_standard_stage_id}/{letter_type}/{invoice_id}', 'Invoice\InvoiceController@letterUpdate')->name('invoice.letter.update');


});
