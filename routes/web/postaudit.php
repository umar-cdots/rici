<?php
//Routes for Post Audit Single Audit Justification


Route::get('/post-audit/create/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@index')->name('post.audit.create');
Route::post('/post-audit/store', 'PostAuditController@store')->name('post.audit.store');
Route::get('/post-audit/edit/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@edit')->name('post.audit.edit');
Route::post('/post-audit/update', 'PostAuditController@update')->name('post.audit.update');
Route::get('/post-audit/show/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@show')->name('post.audit.show');
Route::get('/post-audit/delete/{company_id}/{post_audit_id}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@delete')->name('post.audit.delete');


Route::get('/post-audit/scheme/edit/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@postAuditScheme')->name('post.audit.scheme.edit');
Route::post('/post-audit/scheme/store', 'PostAuditController@postAuditSchemeStore')->name('post.audit.scheme.store');
Route::get('/post-audit/scheme/print/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@postAuditSchemePrint')->name('post.audit.scheme.print');


Route::get('/post-audit/draft-print/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditController@postAuditDraftPrint')->name('post.audit.draft.print');
Route::post('/post-audit/draft-print/store', 'PostAuditController@postAuditDraftPrintStore')->name('post.audit.draft.print.store');
Route::post('/post-audit/draft-print/update', 'PostAuditController@postAuditDraftPrintUpdate')->name('post.audit.draft.print.update');


//Routes for Post Audit IMS Audit Justification


Route::get('/ims-post-audit/create/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@index')->name('ims.post.audit.create');
Route::post('/ims-post-audit/store', 'PostAuditIMSController@store')->name('ims.post.audit.store');
Route::get('/ims-post-audit/edit/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@edit')->name('ims.post.audit.edit');
Route::post('/ims-post-audit/update', 'PostAuditIMSController@update')->name('ims.post.audit.update');
Route::get('/ims-post-audit/show/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@show')->name('ims.post.audit.show');
Route::get('/ims-post-audit/delete/{company_id}/{post_audit_id}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@delete')->name('ims.post.audit.delete');

Route::get('/ims-post-audit/scheme/print/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@postAuditSchemePrint')->name('ims.post.audit.scheme.print');


Route::get('/ims-post-audit/scheme/edit/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@postAuditScheme')->name('ims.post.audit.scheme.edit');
Route::post('/ims-post-audit/scheme/store', 'PostAuditIMSController@postAuditSchemeStore')->name('ims.post.audit.scheme.store');

Route::get('/ims-post-audit/draft-print/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'PostAuditIMSController@postAuditDraftPrint')->name('ims.post.audit.draft.print');
Route::post('/ims-post-audit/draft-print/store', 'PostAuditIMSController@postAuditDraftPrintStore')->name('ims.post.audit.draft.print.store');
Route::post('/ims-post-audit/draft-print/update', 'PostAuditIMSController@postAuditDraftPrintUpdate')->name('ims.post.audit.draft.print.update');

