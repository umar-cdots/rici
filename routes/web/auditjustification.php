<?php
//Routes for Audit Justification
Route::get('/audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationController@index')->name('justification.create');
Route::get('/edit/audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationController@edit')->name('justification.edit');
Route::get('/show/audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationController@show')->name('justification.show');
Route::post('/audit-justification/store_multiple', 'AuditJustificationController@store_multiple')->name('justification.store_multiple');
Route::post('/audit-justification/store', 'AuditJustificationController@store')->name('justification.store');
Route::post('/audit-justification/update', 'AuditJustificationController@update')->name('justification.update');
Route::post('/audit-justification/addremarks', 'AuditJustificationController@addRemarks')->name('justification.remarks.store');
Route::post('/audit-justification/updateremarks', 'AuditJustificationController@updateRemarks')->name('justification.remarks.update');
Route::post('/audit-justification/updateStageManday', 'AuditJustificationController@updateStageManday')->name('justification.update.stage.manday');
Route::post('/audit-justification/partial/remarks', 'AuditJustificationController@partialRemarks')->name('justification.partial.remarks');
Route::get('/delete/audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationController@deleteAJ')->name('justification.delete');


//Routes for Special Audit Justification
Route::get('/special/audit-justification/{company_id}/{standard_number?}', 'AuditJustificationSpecialController@specialAJ')->name('justification.special');
Route::post('/special/audit-justification/add', 'AuditJustificationSpecialController@addSpecialAJ')->name('justification.add.special');
Route::get('/special/audit-justification/edit/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationSpecialController@editSpecialAJ')->name('justification.edit.special');
Route::get('/special/audit-justification/show/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationSpecialController@showSpecialAJ')->name('justification.show.special');
Route::get('/special/audit-justification/delete/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationSpecialController@deleteSpecialAJ')->name('justification.delete.special');
Route::post('/special/audit-justification/update', 'AuditJustificationSpecialController@updateSpecialAJ')->name('justification.special.update');
Route::post('/special/audit-justification/updateremarks', 'AuditJustificationSpecialController@updateSpecialAJRemarks')->name('justification.special.remarks.update');


//Routes for Transfer Audit Justification
Route::get('/transfer-audit-justification/{company_id}/{audit_type}/{frequency}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationTransferController@transferAJ')->name('justification.transfer.create');
Route::get('/add/transfer-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationTransferController@addTransferAJ')->name('justification.transfer.add');
Route::get('/edit/transfer-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationTransferController@editTransferAJ')->name('justification.transfer.edit');
Route::get('/show/transfer-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationTransferController@showTransferAJ')->name('justification.transfer.show');
Route::get('/delete/transfer-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationTransferController@deleteTransferAJ')->name('justification.transfer.delete');
Route::post('/update/transfer-audit-justification/update', 'AuditJustificationTransferController@updateTransferAJ')->name('justification.transfer.update');
Route::post('/transfer-audit-justification/updateremarks', 'AuditJustificationTransferController@updateTransferAJRemarks')->name('justification.transfer.remarks.update');
Route::post('/transfer-audit-justification/addchangeversioncomapnystandard', 'AuditJustificationTransferController@addChangeVersionComapnyStandard')->name('justification.transfer.addchangeversioncomapnystandard');


//Routes for Audit Justification Ims
Route::get('/ims-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationImsController@index')->name('ims.justification.create');
Route::get('/edit/ims-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationImsController@edit')->name('ims.justification.edit');
Route::get('/show/ims-audit-justification/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationImsController@show')->name('ims.justification.show');
Route::post('/ims-audit-justification/store_multiple', 'AuditJustificationImsController@store_multiple')->name('ims.justification.store_multiple');
Route::post('/ims-audit-justification/store', 'AuditJustificationImsController@store')->name('ims.justification.store');
Route::post('/ims-audit-justification/update', 'AuditJustificationImsController@update')->name('ims.justification.update');
Route::post('/ims-audit-justification/addremarks', 'AuditJustificationImsController@addRemarks')->name('ims.justification.remarks.store');
Route::post('/ims-audit-justification/updateremarks', 'AuditJustificationImsController@updateRemarks')->name('ims.justification.remarks.update');
Route::post('/ims-audit-justification/updateStageManday', 'AuditJustificationImsController@updateStageManday')->name('ims.justification.update.stage.manday');
Route::post('/ims-audit-justification/partial/remarks', 'AuditJustificationImsController@partialRemarks')->name('ims.justification.partial.remarks');
Route::get('/ims-audit-justification/combine/delete/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationImsController@deleteIMSAJ')->name('ims.justification.delete');




//Routes for Special Audit Justification IMS
Route::get('/special-ims/audit-justification/{company_id}/{standard_number?}', 'AuditJustificationSpecialIMSController@specialAJ')->name('ims.justification.special');
Route::post('/special-ims/audit-justification/add', 'AuditJustificationSpecialIMSController@addSpecialAJ')->name('ims.justification.add.special');
Route::get('/special-ims/audit-justification/edit/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationSpecialIMSController@editSpecialAJ')->name('ims.justification.edit.special');
Route::get('/special-ims/audit-justification/show/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationSpecialIMSController@showSpecialAJ')->name('ims.justification.show.special');
Route::get('/special-ims/audit-justification/delete/{company_id}/{audit_type}/{standard_number?}/{aj_standard_id?}/{aj_id?}', 'AuditJustificationSpecialIMSController@deleteSpecialAJ')->name('ims.justification.delete.special');
Route::post('/special-ims/audit-justification/update', 'AuditJustificationSpecialIMSController@updateSpecialAJ')->name('ims.justification.special.update');
Route::post('/special-ims/audit-justification/updateremarks', 'AuditJustificationSpecialIMSController@updateSpecialAJRemarks')->name('ims.justification.special.remarks.update');

Route::post('/ims-transfer-audit-justification/addchangeversioncomapnystandard', 'AuditJustificationImsController@addChangeVersionComapnyStandard')->name('ims.justification.transfer.addchangeversioncomapnystandard');


