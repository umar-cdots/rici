<?php
/*Data Entry Routes START*/

Route::get('data-entry', 'DataEntryController@index')->name('data.entry.index');
Route::post('save-standard-family', 'DataEntryController@saveStandardFamily')->name('data.entry.standard.family.save')->middleware('permission:create_data_entry');
Route::post('update-standard-family', 'DataEntryController@updateStandardFamily')->name('data.entry.standard.family.update')->middleware('permission:edit_data_entry');
Route::get('list-standard-family', 'DataEntryController@getStandFamilyList')->name('data.entry.standard.family.index')->middleware('permission:show_data_entry');
Route::get('edit-modal-standard-family/{standard_family_id}', 'DataEntryController@getEditStandardFamilyModal')->name('data.entry.standard.family.edit.modal')->middleware('permission:edit_data_entry');
Route::get('delete-standard-family/{standard_family_id}', 'DataEntryController@deleteStandardFamily')->name('data.entry.standard.family.delete')->middleware('permission:delete_data_entry');

Route::post('save-standard', 'DataEntryController@saveStandard')->name('data.entry.standard.save')->middleware('permission:create_data_entry');
Route::get('list-standards', 'DataEntryController@getStandsList')->name('data.entry.standards.index')->middleware('permission:show_data_entry');
Route::get('delete-standard/{standard_id}', 'DataEntryController@deleteStandard')->name('data.entry.standards.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-standard/{standard_id}', 'DataEntryController@getEditStandardModal')->name('data.entry.standard.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-standard', 'DataEntryController@updateStandard')->name('data.entry.standard.update')->middleware('permission:edit_data_entry');

Route::post('save-accreditation', 'DataEntryController@saveAccreditation')->name('data.entry.accreditation.save')->middleware('permission:create_data_entry');
Route::get('list-accreditation', 'DataEntryController@getAccreditationList')->name('data.entry.accreditation.index')->middleware('permission:show_data_entry');
Route::get('delete-accreditation/{standard_id}', 'DataEntryController@deleteAccreditation')->name('data.entry.accreditation.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-accreditation/{accreditation_id}', 'DataEntryController@getEditAccreditationModal')->name('data.entry.accreditation.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-accreditation', 'DataEntryController@updateAccreditation')->name('data.entry.accreditation.update')->middleware('permission:edit_data_entry');


Route::post('save-iaf', 'DataEntryController@saveIAF')->name('data.entry.iaf.save')->middleware('permission:create_data_entry');
Route::get('list-iaf', 'DataEntryController@getIAFList')->name('data.entry.iaf.index')->middleware('permission:show_data_entry');
Route::get('delete-iaf/{model_id}', 'DataEntryController@deleteIAF')->name('data.entry.iaf.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-iaf/{model_id}', 'DataEntryController@getEditIAFModal')->name('data.entry.iaf.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-iaf', 'DataEntryController@updateIAF')->name('data.entry.iaf.update')->middleware('permission:edit_data_entry');

Route::post('save-ias', 'DataEntryController@saveIAS')->name('data.entry.ias.save')->middleware('permission:create_data_entry');
Route::get('list-ias', 'DataEntryController@getIASList')->name('data.entry.ias.index')->middleware('permission:show_data_entry');
Route::get('get-ias-codes-list/{model_id}', 'DataEntryController@getIASCodesList')->name('data.entry.ias.codes.list')->middleware('permission:show_data_entry');
Route::get('delete-ias/{model_id}', 'DataEntryController@deleteIAS')->name('data.entry.ias.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-ias/{model_id}', 'DataEntryController@getEditIASModal')->name('data.entry.ias.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-ias', 'DataEntryController@updateIAS')->name('data.entry.ias.update')->middleware('permission:edit_data_entry');

Route::post('save-food-category', 'DataEntryController@saveFoodCategory')->name('data.entry.food.category.save')->middleware('permission:create_data_entry');
Route::get('list-food-category', 'DataEntryController@getFoodCategoryList')->name('data.entry.food.category.index')->middleware('permission:show_data_entry');
Route::get('delete-food-category/{model_id}', 'DataEntryController@deleteFoodCategory')->name('data.entry.food.category.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-food-category/{model_id}', 'DataEntryController@getEditFoodCategoryModal')->name('data.entry.food.category.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-food-category', 'DataEntryController@updateFoodCategory')->name('data.entry.food.category.update')->middleware('permission:edit_data_entry');

Route::post('save-food-sub-category', 'DataEntryController@saveFoodSubCategory')->name('data.entry.food.sub.category.save')->middleware('permission:create_data_entry');
Route::get('list-food-sub-category', 'DataEntryController@getFoodSubCategoryList')->name('data.entry.food.sub.category.index')->middleware('permission:show_data_entry');
Route::get('delete-food-sub-category/{model_id}', 'DataEntryController@deleteFoodSubCategory')->name('data.entry.food.sub.category.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-food-sub-category/{model_id}', 'DataEntryController@getEditFoodSubCategoryModal')->name('data.entry.food.sub.category.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-food-sub-category', 'DataEntryController@updateFoodSubCategory')->name('data.entry.food.sub.category.update')->middleware('permission:edit_data_entry');

Route::post('save-energy-code', 'DataEntryController@saveEnergyCode')->name('data.entry.energy.code.save')->middleware('permission:create_data_entry');
Route::get('list-energy-codes', 'DataEntryController@getEnergyCodesList')->name('data.entry.energy.code.index')->middleware('permission:show_data_entry');
Route::get('delete-energy-code/{model_id}', 'DataEntryController@deleteEnergyCode')->name('data.entry.energy.code.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-energy-code/{model_id}', 'DataEntryController@getEditEnergyCodeModal')->name('data.entry.energy.code.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-energy-code', 'DataEntryController@updateEnergyCode')->name('data.entry.energy.code.update')->middleware('permission:edit_data_entry');

Route::post('save-country', 'DataEntryController@saveCountry')->name('data.entry.country.save')->middleware('permission:create_data_entry');
Route::get('list-countries', 'DataEntryController@getCountriesList')->name('data.entry.countries.index')->middleware('permission:show_data_entry');
Route::get('delete-country/{model_id}', 'DataEntryController@deleteCountry')->name('data.entry.country.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-country/{model_id}', 'DataEntryController@getEditCountryModal')->name('data.entry.country.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-country', 'DataEntryController@updateCountry')->name('data.entry.country.update')->middleware('permission:edit_data_entry');

Route::post('save-city', 'DataEntryController@saveCity')->name('data.entry.city.save')->middleware('permission:create_data_entry');
Route::get('list-cities', 'DataEntryController@getCitiesList')->name('data.entry.cities.index')->middleware('permission:show_data_entry');
Route::get('delete-city/{model_id}', 'DataEntryController@deleteCity')->name('data.entry.city.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-city/{model_id}', 'DataEntryController@getEditCityModal')->name('data.entry.city.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-city', 'DataEntryController@updateCity')->name('data.entry.city.update')->middleware('permission:edit_data_entry');


Route::post('save-region', 'DataEntryController@saveRegion')->name('data.entry.region.save')->middleware('permission:create_data_entry');
Route::get('list-regions', 'DataEntryController@getRegionsList')->name('data.entry.regions.index')->middleware('permission:show_data_entry');
Route::get('delete-region/{model_id}', 'DataEntryController@deleteRegion')->name('data.entry.region.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-region/{model_id}', 'DataEntryController@getEditRegionModal')->name('data.entry.region.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-region', 'DataEntryController@updateRegion')->name('data.entry.region.update')->middleware('permission:edit_data_entry');


Route::post('save-zone', 'DataEntryController@saveZone')->name('data.entry.zone.save')->middleware('permission:create_data_entry');
Route::get('list-zones', 'DataEntryController@getZonesList')->name('data.entry.zones.index')->middleware('permission:show_data_entry');
Route::get('delete-zones/{model_id}', 'DataEntryController@deleteZone')->name('data.entry.zone.delete')->middleware('permission:delete_data_entry');
Route::get('edit-modal-zones/{model_id}', 'DataEntryController@getEditZoneModal')->name('data.entry.zone.edit.modal')->middleware('permission:edit_data_entry');
Route::post('update-zones', 'DataEntryController@updateZone')->name('data.entry.zone.update')->middleware('permission:edit_data_entry');


Route::post('save-effective-employees', 'DataEntryController@saveEffectiveEmployees')->name('effective.employees.save');
Route::post('save-letter-names', 'DataEntryController@saveLetterNames')->name('letter.names.save');
Route::post('invoice-settings', 'DataEntryController@invoiceSettings')->name('invoice.settings.save');

/*Data Entry Routes END*/
